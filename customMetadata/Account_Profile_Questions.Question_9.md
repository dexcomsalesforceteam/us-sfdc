<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Question 9</label>
    <protected>false</protected>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Order__c</field>
        <value xsi:type="xsd:double">9.0</value>
    </values>
    <values>
        <field>Question__c</field>
        <value xsi:type="xsd:string">Medicare has decided to cover CGM when otherwise qualified beneficiaries use ONLY the Dexcom G5 Receiver to view their glucose data. If a beneficiary otherwise qualified for Medicare coverage for a Dexcom G5 therapeutic CGM uses another device (smartphone, tablet, etc.), either alone or in combination with the G5 Receiver to display their glucose information, Medicare will NOT cover their CGM supplies. Dexcom has no ability to override this requirement, and beneficiaries should reach out to 1-800-MEDICARE with any questions. If you&apos;d like to continue to use your phone or other smart device in conjunction with your Dexcom CGM, there may be options for you on a cash pay basis, outside of Medicare. If that is of interest to you, I can transfer you now to a sales specialist to discuss further.</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Medicare</value>
    </values>
</CustomMetadata>
