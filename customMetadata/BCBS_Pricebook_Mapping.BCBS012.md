<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>BCBS012</label>
    <protected>false</protected>
    <values>
        <field>Billing_State__c</field>
        <value xsi:type="xsd:string">HI</value>
    </values>
    <values>
        <field>Criteria_Field__c</field>
        <value xsi:type="xsd:string">Plan_Type__c</value>
    </values>
    <values>
        <field>Criteria_Object__c</field>
        <value xsi:type="xsd:string">Benefits__c</value>
    </values>
    <values>
        <field>Criteria_Value__c</field>
        <value xsi:type="xsd:string">PPO</value>
    </values>
    <values>
        <field>Is_Med_Adv__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Payor_Code__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Pricebook_Oracle_Id__c</field>
        <value xsi:type="xsd:string">76859</value>
    </values>
    <values>
        <field>Selective_Criteria__c</field>
        <value xsi:type="xsd:string">Benefit_Hierarchy__c=&apos;Primary&apos;</value>
    </values>
</CustomMetadata>
