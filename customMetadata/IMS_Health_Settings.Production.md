<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Production</label>
    <protected>false</protected>
    <values>
        <field>App_Id__c</field>
        <value xsi:type="xsd:string">12</value>
    </values>
    <values>
        <field>Client_Id__c</field>
        <value xsi:type="xsd:string">DEXCOM_PROD</value>
    </values>
    <values>
        <field>Password__c</field>
        <value xsi:type="xsd:string">DEXCOM!PROD20151102</value>
    </values>
    <values>
        <field>Webservice_Endpoint__c</field>
        <value xsi:type="xsd:string">http://daas.imshealth.com/das/hcr/websvc/ascs_daas_api.asmx</value>
    </values>
</CustomMetadata>
