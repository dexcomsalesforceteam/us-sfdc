<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Connection Details Production</label>
    <protected>false</protected>
    <values>
        <field>Client_Id__c</field>
        <value xsi:type="xsd:string">d9punqhidajz1widti7jgb0y</value>
    </values>
    <values>
        <field>Client_Secret__c</field>
        <value xsi:type="xsd:string">lWtuRDa45sprwGe4sqGf61KG</value>
    </values>
    <values>
        <field>MO_Url__c</field>
        <value xsi:type="xsd:string">https://www.exacttargetapis.com/sms/v1/queueMO</value>
    </values>
    <values>
        <field>Marketing_cloud_instance__c</field>
        <value xsi:type="xsd:string">S7</value>
    </values>
    <values>
        <field>Message__c</field>
        <value xsi:type="xsd:string">JOIN</value>
    </values>
    <values>
        <field>Password__c</field>
        <value xsi:type="xsd:string">4^ZbcA%R</value>
    </values>
    <values>
        <field>Short_Code__c</field>
        <value xsi:type="xsd:string">48629</value>
    </values>
    <values>
        <field>Token_Url__c</field>
        <value xsi:type="xsd:string">https://auth.exacttargetapis.com/v1/requestToken</value>
    </values>
    <values>
        <field>Username__c</field>
        <value xsi:type="xsd:string">SundogSFMC-DEV-Dexcom</value>
    </values>
</CustomMetadata>
