<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Question 1</label>
    <protected>false</protected>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Order__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
    <values>
        <field>Question__c</field>
        <value xsi:type="xsd:string">Have you been diagnosed with Type 1 or Type 2 Diabetes?</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Medicare</value>
    </values>
</CustomMetadata>
