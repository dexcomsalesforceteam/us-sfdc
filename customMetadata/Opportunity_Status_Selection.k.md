<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>11</label>
    <protected>false</protected>
    <values>
        <field>Is_BI_Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Is_CMN_Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Is_Credit_Card_Active__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Is_Pre_Authorized__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Stages__c</field>
        <value xsi:type="xsd:string">1. New Opportunity</value>
    </values>
    <values>
        <field>Status__c</field>
        <value xsi:type="xsd:string">1.1A Initial Opporutnity Review</value>
    </values>
</CustomMetadata>
