<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Question 6</label>
    <protected>false</protected>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Order__c</field>
        <value xsi:type="xsd:double">6.0</value>
    </values>
    <values>
        <field>Question__c</field>
        <value xsi:type="xsd:string">Have you seen your Health Care Provider in the past 6 months (HCP) and determined that criteria (1-4) have been met?</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Medicare</value>
    </values>
</CustomMetadata>
