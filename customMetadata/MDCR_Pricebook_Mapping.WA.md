<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>WA</label>
    <protected>false</protected>
    <values>
        <field>Pricebook_Name__c</field>
        <value xsi:type="xsd:string">M/C - Region 21</value>
    </values>
    <values>
        <field>State__c</field>
        <value xsi:type="xsd:string">WA</value>
    </values>
</CustomMetadata>
