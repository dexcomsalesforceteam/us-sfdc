<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>RGA</label>
    <protected>false</protected>
    <values>
        <field>Approval_Reminder__c</field>
        <value xsi:type="xsd:double">5.0</value>
    </values>
    <values>
        <field>Object_Identifier__c</field>
        <value xsi:type="xsd:string">a0G</value>
    </values>
</CustomMetadata>
