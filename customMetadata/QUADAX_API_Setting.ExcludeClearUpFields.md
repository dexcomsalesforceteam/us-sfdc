<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ExcludeClearUpFields</label>
    <protected>false</protected>
    <values>
        <field>FieldSet__c</field>
        <value xsi:type="xsd:string">Account__c,Benefit_Hierarchy__c,Insurer__c,MEMBER_ID__c,Payor__c,Plan_Name__c,Medicare_Beneficiary_Identifier__c</value>
    </values>
    <values>
        <field>Field_Mapping__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Exclude_Clear_Out</value>
    </values>
</CustomMetadata>
