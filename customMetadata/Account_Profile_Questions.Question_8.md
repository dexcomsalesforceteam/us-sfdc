<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Question 8</label>
    <protected>false</protected>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Order__c</field>
        <value xsi:type="xsd:double">8.0</value>
    </values>
    <values>
        <field>Question__c</field>
        <value xsi:type="xsd:string">Medicare coverage for therapeutic CGM requires that you only use the Dexcom receiver to display your CGM glucose data, and not a phone, tablet or other device, even if you are also using your Dexcom receiver to view that data. Therefore, do you agree to follow Medicare criteria and only view your CGM glucose readings on the covered Dexcom receiver and to not use any other device to view your CGM glucose data?</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Medicare</value>
    </values>
</CustomMetadata>
