<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>SmartyStreetsZipCode</label>
    <protected>false</protected>
    <values>
        <field>Auth_Id__c</field>
        <value xsi:type="xsd:string">72ff78d4-c4d6-8870-2934-845747084e4a</value>
    </values>
    <values>
        <field>Auth_Token__c</field>
        <value xsi:type="xsd:string">EMFAEs4rzf5UVoJlOUNX</value>
    </values>
    <values>
        <field>URL__c</field>
        <value xsi:type="xsd:string">https://us-zipcode.api.smartystreets.com/lookup?</value>
    </values>
</CustomMetadata>
