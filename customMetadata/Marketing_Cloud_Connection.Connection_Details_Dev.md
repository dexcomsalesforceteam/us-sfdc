<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Connection Details Dev</label>
    <protected>false</protected>
    <values>
        <field>Client_Id__c</field>
        <value xsi:type="xsd:string">2l241yc8ap82ct56zox225ox</value>
    </values>
    <values>
        <field>Client_Secret__c</field>
        <value xsi:type="xsd:string">u84gFdJ33eZdX9L9gDndahzc</value>
    </values>
    <values>
        <field>MO_Url__c</field>
        <value xsi:type="xsd:string">https://www.exacttargetapis.com/sms/v1/queueMO</value>
    </values>
    <values>
        <field>Marketing_cloud_instance__c</field>
        <value xsi:type="xsd:string">S7</value>
    </values>
    <values>
        <field>Message__c</field>
        <value xsi:type="xsd:string">TESTJOIN</value>
    </values>
    <values>
        <field>Password__c</field>
        <value xsi:type="xsd:string">7a!oyD56HxXJ</value>
    </values>
    <values>
        <field>Short_Code__c</field>
        <value xsi:type="xsd:string">48629</value>
    </values>
    <values>
        <field>Token_Url__c</field>
        <value xsi:type="xsd:string">https://auth.exacttargetapis.com/v1/requestToken</value>
    </values>
    <values>
        <field>Username__c</field>
        <value xsi:type="xsd:string">dexcom.mc.us.dev</value>
    </values>
</CustomMetadata>
