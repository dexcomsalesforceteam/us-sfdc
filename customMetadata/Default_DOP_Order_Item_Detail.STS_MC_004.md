<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>STS-MC-004</label>
    <protected>false</protected>
    <values>
        <field>Item_Description__c</field>
        <value xsi:type="xsd:string">G5 Medicare Sensor Kit 4-Pack, Dexcom</value>
    </values>
</CustomMetadata>
