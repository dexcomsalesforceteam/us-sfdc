<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>QuarterlyQuantityLimit03</label>
    <protected>false</protected>
    <values>
        <field>Duration__c</field>
        <value xsi:type="xsd:string">Quarterly</value>
    </values>
    <values>
        <field>KCode_Setting_Type__c</field>
        <value xsi:type="xsd:string">Testing Supplies Qty Limits</value>
    </values>
    <values>
        <field>K_Code_Insurer_Id__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Product_Names__c</field>
        <value xsi:type="xsd:string">301936586218</value>
    </values>
    <values>
        <field>Quantity_Boxes__c</field>
        <value xsi:type="xsd:double">3.0</value>
    </values>
</CustomMetadata>
