<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Setting04</label>
    <protected>false</protected>
    <values>
        <field>Audit_Area__c</field>
        <value xsi:type="xsd:string">Benefit</value>
    </values>
    <values>
        <field>Audit_Field__c</field>
        <value xsi:type="xsd:string">Benefit - Prior Auth Check</value>
    </values>
    <values>
        <field>Audit_Item_Field__c</field>
        <value xsi:type="xsd:string">Transmitter Info Check</value>
    </values>
</CustomMetadata>
