<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>RequiredFieldSet</label>
    <protected>false</protected>
    <values>
        <field>FieldSet__c</field>
        <value xsi:type="xsd:string">Payor__r.Quadax_Payor_Code__c,Account__r.FirstName,Account__r.LastName,Account__r.DOB__c,Account__r.Gender__c,MEMBER_ID__c,Account__r.Prescribers__r.NPI_Number__c</value>
    </values>
    <values>
        <field>Field_Mapping__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Required</value>
    </values>
</CustomMetadata>
