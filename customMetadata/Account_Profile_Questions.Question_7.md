<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Question 7</label>
    <protected>false</protected>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Order__c</field>
        <value xsi:type="xsd:double">7.0</value>
    </values>
    <values>
        <field>Question__c</field>
        <value xsi:type="xsd:string">Will you have an in-person appointment with your HCP every 6 months to make sure you are following their CMN and diabetes treatment plan?</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Medicare</value>
    </values>
</CustomMetadata>
