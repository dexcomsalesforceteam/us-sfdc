<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>BCBS028</label>
    <protected>false</protected>
    <values>
        <field>Billing_State__c</field>
        <value xsi:type="xsd:string">MO</value>
    </values>
    <values>
        <field>Criteria_Field__c</field>
        <value xsi:type="xsd:string">County__c</value>
    </values>
    <values>
        <field>Criteria_Object__c</field>
        <value xsi:type="xsd:string">Address__c</value>
    </values>
    <values>
        <field>Criteria_Value__c</field>
        <value xsi:type="xsd:string">Atchison, Nodaway, Work, Gentry, Harrison, Mercer, Holt, Andrew, DeKalb, Daviess, Grundy, Buchanan, Clinton, Caldwell, Livingston, Platte, Clay, Ray, Carroll, Jackson, Lafayette, Saline, Cass, Johnson, Pettis, Bates, Henry, Benton, St. Clair, Vernon, Johnson, Wyandot</value>
    </values>
    <values>
        <field>Is_Med_Adv__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Payor_Code__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Pricebook_Oracle_Id__c</field>
        <value xsi:type="xsd:string">70118</value>
    </values>
    <values>
        <field>Selective_Criteria__c</field>
        <value xsi:type="xsd:string">Address_Type__c=&apos;BILL_TO&apos; and Inactive__c = false and Primary_Flag__c = true</value>
    </values>
</CustomMetadata>
