trigger UserTrigger on User (after update)
{
/*******************************************************************************************************************
@Author         : Jagan Periyakaruppan
@Date Created   : 03/06/2018
@Description    : Trigger invokes handler class to handle User updates
********************************************************************************************************************/ 
    if(trigger.isAfter)
    {
        if(trigger.isUpdate)
        {
            ClsUserTriggerHandler.CheckFieldUpdates(trigger.newmap, trigger.oldmap);
        }
    }
    
}