/*
 * Trigger created to handle events on MDCR_Followup__c
 * @CreatedBy : LTI
 * trigger updated for CRMSF-4697
*/
trigger MdcrFollowupTrigger on MDCR_Followup__c (after insert, before insert, before update) {
	//is After routine
    if(trigger.isAfter){
        
        if(trigger.isInsert){
            ClsMdcrFollowupTriggerHandler.handleAfterInsert(trigger.new);
        }
    }
    
    //is before routine
    if(trigger.isBefore){
        if(trigger.isInsert){
            ClsMdcrFollowupTriggerHandler.handleBeforeInsert(trigger.new);
        }
        
        if(trigger.isUpdate){
            ClsMdcrFollowupTriggerHandler.handleBeforeUpdate(trigger.newMap, trigger.oldMap);
        }
    }
    
    
}