trigger ProcessPrescribertTerritoryEventTrigger on Prescriber_Territory_Event__e (after insert) {
  
    //Author : Jagan 02/01/2018
    //Description : This Trigger will be invoked when a new event is entered when the Prescriber territory is changed
    //BHU 2019/05/06 Added Secondary Territory logic
    
    //Map holds the Consumers which are to be updated with Prescriber Territory change
    Map<String, String> prescribersWithUpdatedTerritoryMap = new Map<String, String>();
    
    //BHU 2019-04-09 Secondary Territory
    Map<String, String> prescribersWithUpdatedTerritory2Map = new Map<String, String>();

    //Map holds reference to Territory Id roster object
    Map<String, Id> terrIdLookupMap = new Map<String, Id> ();
    
    //List holds the account that are to be updated with the territory value published via event
    List<Account> accountsToBeUpdatedWithTerritoryList = new List<Account>();
    
    for(Prescriber_Territory_Event__e newEvent : Trigger.New)
    {
        prescribersWithUpdatedTerritoryMap.put(newEvent.Prescriber_Id__c, newEvent.Territory_Name__c);
        //BHU 19/05/03
        prescribersWithUpdatedTerritory2Map.put(newEvent.Prescriber_Id__c, newEvent.Territory_Name_SF__c);
    }
    //Get the Territory Id map
    if(!prescribersWithUpdatedTerritoryMap.isEmpty())
    {
        terrIdLookupMap = AccountTerritoryUpdate.getTerritoryLookupId(prescribersWithUpdatedTerritoryMap.values());
    }
    
    //Loop through Consumers, which need to be updated with Prescriber's territory
    //BHU 19/05/03
    //for(Account accnt : [SELECT Id, Territory_Code__c, Prescribers__c FROM Account WHERE Prescribers__c IN : prescribersWithUpdatedTerritoryMap.keySet() FOR UPDATE])
    for(Account accnt : [SELECT Id, Territory_Code__c, Territory_ID_SF__c, Prescribers__c FROM Account WHERE Prescribers__c IN : prescribersWithUpdatedTerritoryMap.keySet() FOR UPDATE])
    {
        String territory = prescribersWithUpdatedTerritoryMap.get(accnt.Prescribers__c);
        //BHU 19/05/03
        String territorySF = prescribersWithUpdatedTerritory2Map.get(accnt.Prescribers__c);
        
        accnt.Territory_Code__c = territory;
        //BHU 19/05/03
        accnt.Territory_ID_SF__c = territorySF;
        
        accnt.Territory_ID_Lookup__c = terrIdLookupMap.get(territory);
        accountsToBeUpdatedWithTerritoryList.add(accnt);
    }
    
    //Proceed with updates if the list has values
    if(!accountsToBeUpdatedWithTerritoryList.isEmpty())
    {
        Database.SaveResult[] savedResult = Database.update(accountsToBeUpdatedWithTerritoryList, false); 
        for(Database.SaveResult sr : savedResult)
        {
            if(!sr.isSuccess()){
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Fields that affected this error: ' + err.getFields());
                }
                // Ensure we don't retry the trigger more than 4 times
                if (EventBus.TriggerContext.currentContext().retries < 4) {
                    // Condition isn't met, so try again later.
                    throw new EventBus.RetryableException('Error happened, so retrying the trigger again.');
                } 
            }
        }
    }
    
}