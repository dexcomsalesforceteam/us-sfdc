/***********************************************************************************************************************
    @Author        : Jagan Periyakaruppan
    @Date Modified    : 12/19/2017
    @Description    : Added static variable to execute the code only once before and after
************************************************************************************************************************/
trigger OpportunityTrigger on Opportunity (after insert, after update, before update, before insert) {
     UtilityClass.runOrderHeaderTriggerOnce();
    if(trigger.IsBefore){
        if(UtilityClass.runBeforeTriggerOnce()){
            if(trigger.isInsert){
                OpportunityTriggerHandler.beforeInsert(trigger.new);
                OpportunityTriggerHandler.validatePrescribers(Trigger.NEW, null);
            }
            //Jagan 10/4/2017 - Added below if block
            if(trigger.isUpdate){
                OpportunityTriggerHandler.beforeUpdate(trigger.new);
                OpportunityTriggerHandler.calculateCopayOnUpdate(trigger.new, trigger.oldMap);
                OpportunityTriggerHandler.validatePrescribers(Trigger.NEW, Trigger.OldMap);
            }
        }
    }
    
    if(trigger.IsAfter){
        if(UtilityClass.runAfterTriggerOnce()){
            if(trigger.isInsert){
                OpportunityTriggerHandler.afterInsert(trigger.new);
            }
            
            if(trigger.isUpdate){
                //commenting afterUpdate for Auto QC deployment
                //OpportunityTriggerHandler.afterUpdate(trigger.new, trigger.oldMap);
                //OpportunityTriggerHandler.deleteOliOnPricebookChange(trigger.new, trigger.oldMap);
            }
        }
    }
}