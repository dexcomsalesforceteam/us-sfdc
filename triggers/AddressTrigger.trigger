/*******************************************************************************************************************
@Author        : Jagan Periyakaruppan
@Date Created    : 4/15/2019
@Description    : Trigger invokes handler class to update Address information on parent Account
@Modification : Date May 20,2020, Author : Tanay Kulkarni, LTI, User Story - CRMSF-5189
********************************************************************************************************************/    
trigger AddressTrigger on Address__c (before insert, before update, after insert,after update) {
    
    if(trigger.isBefore){
        if(trigger.isInsert){
            //ClsAddressTriggerHandler.formatPhone(trigger.new, null);
        }
        
        if(trigger.isUpdate){
            ClsAddressTriggerHandler.formatPhone(trigger.new, trigger.oldMap);
        }
    }
    
    
    if(trigger.isAfter){
        system.debug('Value of the static class is ' + ClsAccountAddressTriggerStaticClass.addressChangeInvokedFromAddress);
        //Proceed to make changes to Address object only when the address change (Bill To or Ship To) happened from Address object        
        if(trigger.isInsert){
            if(ClsAccountAddressTriggerStaticClass.addressChangeInvokedFromAddress)
                system.debug('#### IS AFTER INSERT Address trigger');
                ClsAddressTriggerHandler.ProcessAddressInsert(trigger.new);
        }
        if(trigger.isUpdate){
            if(ClsAccountAddressTriggerStaticClass.addressChangeInvokedFromAddress){
                system.debug('#### IS AFTER UPDATE Address trigger');
                ClsAddressTriggerHandler.ProcessAddressUpdate(trigger.newmap, trigger.oldmap);
                //added below line for CRMSF-5189
                ClsAddressTriggerHandler.publishAddressOutEvent(trigger.new, trigger.oldmap);
            }
        }
    }
    
}