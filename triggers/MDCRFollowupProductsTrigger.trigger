/***************************************************************************************
* @author      : Jagan Periyakaruppan
* @date        : Sep 10 2018
* @description : Trigger will invoke the process to check the Testing supplies count
**********************************************************************************************/
trigger MDCRFollowupProductsTrigger on MDCR_Followup_Products__c (before insert,after insert,before delete) {
    if(trigger.isBefore){
        if(trigger.isInsert){
            ClsMDCRFollowupProductsTriggerHandler.restrictDupProdcts(trigger.new);
        }
    }
    if(trigger.isAfter){
        if(trigger.isInsert){
            ClsMDCRFollowupProductsTriggerHandler.ProcessMFRProductInsert(trigger.new);//Method will process the MFR Products after they are inserted
        }
    }
    
}