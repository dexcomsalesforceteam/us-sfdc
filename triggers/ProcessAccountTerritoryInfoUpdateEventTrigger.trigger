trigger ProcessAccountTerritoryInfoUpdateEventTrigger on Update_Territory_Values_On_Account__e (after Insert) {
    
    Map<String,String> accountTerritoryMap = new Map<String,String>();
    Map<String,Territory_Alignment__c> territoryAlignmentMap = new Map<String,Territory_Alignment__c>();
    System.debug('ProcessAccountTerritoryInfoUpdateEventTrigger ....');  
    for(Update_Territory_Values_On_Account__e accountTerritory : Trigger.New){
        accountTerritoryMap.put(accountTerritory.Account_Id__c,accountTerritory.Territory_Code__c);
        //System.debug('accountTerritory ....'+accountTerritory);  
    }
    
    List<Account> acountList = [Select Id,Territory_Fax__c,Territory_Code__c,Territory_AS__c,Territory_PSS_RingDNA__c,Territory_RSS_RingDNA__c,Territory_RSS_Supervisor__c,Territory_RSS__c,
                                Territory_PSS__c,Territory_Supervisor__c,Territory_ID_Lookup__c from account where id In : accountTerritoryMap.keySet()];
    if(accountTerritoryMap.values().size() >0){                        
        for(Territory_Alignment__c territory:[Select Id,Name, Fax__c,PCS__c, PSS_RingDNA_Number__c,RSS_RingDNA_Number__c,RSS_Supervisor__c,RSS__c,SA__c,Supervisor__c,Territory_Alignment_18_Digit_ID__c from Territory_Alignment__c where name in : accountTerritoryMap.values()]){
            territoryAlignmentMap.put(territory.Name,territory);
        } 
    }
    for(Account acc:acountList){
        
        if(string.IsBLANK(acc.Territory_Code__c)){
            acc.Territory_Fax__c = null;
            acc.Territory_AS__c= null;
            acc.Territory_PSS_RingDNA__c= null;
            acc.Territory_RSS_RingDNA__c= null;
            acc.Territory_RSS_Supervisor__c= null;
            acc.Territory_RSS__c= null;
            acc.Territory_PSS__c= null;
            acc.Territory_Supervisor__c= null;
            acc.Territory_ID_Lookup__c = null;       
        }
        else{
            acc.Territory_Fax__c = territoryAlignmentMap.get(acc.Territory_Code__c).Fax__c;
            acc.Territory_AS__c = territoryAlignmentMap.get(acc.Territory_Code__c).PCS__c;
            acc.Territory_PSS_RingDNA__c= territoryAlignmentMap.get(acc.Territory_Code__c).PSS_RingDNA_Number__c;
            acc.Territory_RSS_RingDNA__c= territoryAlignmentMap.get(acc.Territory_Code__c).RSS_RingDNA_Number__c;
            acc.Territory_RSS_Supervisor__c= territoryAlignmentMap.get(acc.Territory_Code__c).RSS_Supervisor__c;
            acc.Territory_RSS__c= territoryAlignmentMap.get(acc.Territory_Code__c).RSS__c;
            acc.Territory_PSS__c= territoryAlignmentMap.get(acc.Territory_Code__c).SA__c;
            acc.Territory_Supervisor__c= territoryAlignmentMap.get(acc.Territory_Code__c).Supervisor__c;
            acc.Territory_ID_Lookup__c = territoryAlignmentMap.get(acc.Territory_Code__c).Territory_Alignment_18_Digit_ID__c;  
        }
        
    }   
    
    if(!acountList.isEmpty())
    {
        Database.SaveResult[] savedResult = Database.update(acountList, false); 
        for(Database.SaveResult sr : savedResult)
        {
            if(!sr.isSuccess()){
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Fields that affected this error: ' + err.getFields());
                }
                // Ensure we don't retry the trigger more than 4 times
                if (EventBus.TriggerContext.currentContext().retries < 4) {
                    // Condition isn't met, so try again later.
                    throw new EventBus.RetryableException('Error happened, so retrying the trigger again.');
                } 
            }
        }  
    }                
}