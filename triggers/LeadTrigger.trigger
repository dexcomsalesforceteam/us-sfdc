trigger LeadTrigger on Lead (before insert, before update, before delete, after insert, after update, after delete, after undelete) {

    if(trigger.isBefore){
        if(trigger.isInsert){
            LeadTriggerHandler.onBeforeInsert(trigger.new);
     //       LeadTriggerHandler.StonewallLeadConversionOninsert(trigger.new);

        }
        if(trigger.isUpdate){
            LeadTriggerHandler.onBeforeUpdate(trigger.new);
      //    LeadTriggerHandler.StonewallLeadConversionOnUpdate(trigger.new,trigger.oldmap);
            LeadTriggerHandler.doubleOptInBefore(trigger.new,trigger.oldMap);
        }
    }

    if(trigger.isAfter){
        if(trigger.isUpdate){
            LeadTriggerHandler.onAfterUpdate(trigger.new);
            LeadTriggerHandler.StonewallLeadConversionOnUpdate(trigger.new,trigger.oldmap);
            LeadTriggerHandler.doubleOptInAfter(trigger.new,trigger.oldMap);
            LeadTriggerHandler.SetLeadPriorityFlag(trigger.newMap,trigger.oldMap);
        }
        if(trigger.isInsert){
            LeadTriggerHandler.onAfterInsert(trigger.new);
            LeadTriggerHandler.StonewallLeadConversionOninsert(trigger.new);
            LeadTriggerHandler.doubleOptInAfter(trigger.new,trigger.oldMap);
            LeadTriggerHandler.SetLeadPriorityFlag(trigger.newMap,null);
        }
    }
}