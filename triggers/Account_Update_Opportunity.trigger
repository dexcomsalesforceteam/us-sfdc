/*revision : Kingsley Tumaneng (10/6/2015) add Payor__c to be updated*/
trigger Account_Update_Opportunity on Account (after update) {
    /***************************************************************
    //Summary: Update mobile phone in opporunity after account is updated.
    //Programmer: BHU
    //Change Log
    //  04/22/2015 BHU Initial Version
    //
    //-- Modified by Noy De Goma@CSHERPAS on 11.03.2015
    //--Added Prescribers__c and Medical_Facility__c to be updated
    /***************************************************************
    @Author        : Jagan Periyakaruppan
    @Date Modified    : 06/12/2017
    @Description    : Added logic to carry over Medicare Pricebook
    ***************************************************************/
    /***************************************************************
    @Author        : Jagan Periyakaruppan
    @Date Modified    : 10/03/2017
    @Description    : Added logic to handle Open opportunities 
    with products when the customer type changes 
    on account
    ***************************************************************
     @Author        : Priyanka Kajawe
    @Date Modified    : 05/06/2018
    @Description    : Deactivated this Trigger,
    This trigger logic moved in AccountTrigger and AccountTriggerHandler class, Please refer them.
    ***************************************************************/
        
    //  Get the IDs of the Accounts
    Set<ID> accIDs = Trigger.newMap.keySet();
    
    // Added by Jagan on 10.03.2017 - Capture the account Ids, which are affected with customer type
    Set<Id> accountsWithMedicareChangesSet = new Set<Id>();
    
    // Added by Jagan on 10.03.2017 - Capture the account Ids, which are affected with customer type
    Set<Id> openOpptyWithLineItemsToBeDeletedSet = new Set<Id>();
    
    // Added by Jagan on 10.03.2017 - Find the accounts, which have customer type changes from or to Medicare values
    for(Account acc : Trigger.new)
    {
        //Find if the customer type value has been changed 
        if(acc.Customer_Type__c != Trigger.oldMap.get(acc.Id).Customer_Type__c)
        {
            system.debug('***********Code from Oppty trigger Account pricebook is ' + acc.Default_Price_Book__r.Cash_Price_Book__c);
            //If old or new Customer Type is Medicare, ignore cash pricebook
            if(acc.Customer_Type__c != null)
            {
                if(acc.Default_Price_Book__c != null)
                {
                    if(acc.Default_Price_Book__r.Cash_Price_Book__c == false)
                        accountsWithMedicareChangesSet.add(acc.Id);
                }
                else
                    accountsWithMedicareChangesSet.add(acc.Id);
            }
        }
    }
    //  Get all Opportunities associated with the Account in this Trigger
    List<Opportunity> OppoList = [
        SELECT Id, Account.Id, RecordtypeId, Account.PersonContact.MobilePhone, Pricebook2Id, prescribers__c, Medical_Facility__c, Mobile_Phone__c, Account.Payor__c, Account.Prescribers__c, Account.Medical_Facility__c, Account.Last_MDCR_Reorder_Follow_up_Type__c, First_MDCR_Order_Non_Direct__c, First_MDCR_Order_Non_Direct_Channel__c, Account.Last_MDCR_Reorder_Follow_up_Date__c, Account.MDCR_Communication_Preference__c, Account.Default_Price_Book__c, Account.First_MDCR_Order_Non_Direct__c,Account.First_MDCR_Order_Non_Direct_Channel__c,Account.Consumer_Payor_Code__c, Last_MDCR_Reorder_Follow_up_Type__c, Last_MDCR_Reorder_Follow_up_Date__c, MDCR_Communication_Preference__c, Count_Of_Oppty_Products__c, Payor_Code__c, Estimated_Cost__c, Account.Estimated_Cost__c, Opp_Territory_AS__c, Account.Territory_AS__c, Opp_Territory_Fax__c, Account.Territory_Fax__c, Opp_Territory_PSS_RingDNA__c, Account.Territory_PSS_RingDNA__c, Opp_Territory_PSS__c, Account.Territory_PSS__c, Opp_Territory_RSS_RingDNA__c, Account.Territory_RSS_RingDNA__c, Opp_Territory_RSS_Supervisor__c, Account.Territory_RSS_Supervisor__c, Opp_Territory_RSS__c, Account.Territory_RSS__c, Opp_Territory_Supervisor__c, Account.Territory_Supervisor__c, Opp_Territory_ID__c, Account.Territory_ID_Lookup__c//Jagan - added additional fields
        FROM Opportunity
        WHERE IsClosed = false
        AND (StageName != '61. Quote Approved' AND StageName != '10. Cancelled')
        AND Account.Id IN :accIDs ] ;
    
    //Opportunities to be updated
    List<Opportunity> oppsToBeUpdated = new List<Opportunity>();
    
    //  Loop through them to copy Number of Partners from parent (Account) to the Opportunity
    for(Opportunity opps: OppoList)
    {
        // Added by Anuj Patel on 10.20.2017
        if(opps.RecordtypeId == Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('EMEA CH Opportunity').getRecordTypeId() &&
            (
            opps.Account.Prescribers__c != trigger.oldMap.get(opps.AccountId).Prescribers__c ||
            opps.Account.Medical_Facility__c != trigger.oldMap.get(opps.AccountId).Medical_Facility__c
            )
        )
        {
        if(opps.Account.Prescribers__c != trigger.oldMap.get(opps.AccountId).Prescribers__c){
                opps.Prescribers__c = opps.Account.Prescribers__c;
            }
        if(opps.Account.Medical_Facility__c != trigger.oldMap.get(opps.AccountId).Medical_Facility__c){
                opps.Medical_Facility__c = opps.Account.Medical_Facility__c;
            }
    
       oppsToBeUpdated.add(opps); 
           
        }
        
        system.debug('***********Code from Oppty trigger Oppty pricebook is ' + opps.Pricebook2Id);
        if(opps.RecordtypeId == Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('US Opportunity').getRecordTypeId() &&
            (opps.Account.Payor__c != trigger.oldMap.get(opps.AccountId).Payor__c ||
            opps.Mobile_Phone__c != trigger.oldMap.get(opps.AccountId).PersonContact.MobilePhone ||
            opps.Account.Prescribers__c != trigger.oldMap.get(opps.AccountId).Prescribers__c ||
            opps.Account.Medical_Facility__c != trigger.oldMap.get(opps.AccountId).Medical_Facility__c ||
            opps.Last_MDCR_Reorder_Follow_up_Type__c != opps.Account.Last_MDCR_Reorder_Follow_up_Type__c ||
            opps.Last_MDCR_Reorder_Follow_up_Date__c != opps.Account.Last_MDCR_Reorder_Follow_up_Date__c ||
            opps.MDCR_Communication_Preference__c != opps.Account.MDCR_Communication_Preference__c ||
            opps.First_MDCR_Order_Non_Direct__c != opps.Account.First_MDCR_Order_Non_Direct__c ||
            opps.First_MDCR_Order_Non_Direct_Channel__c != opps.Account.First_MDCR_Order_Non_Direct_Channel__c ||
            opps.Pricebook2Id != opps.Account.Default_Price_Book__c ||
            opps.Payor_Code__c != opps.Account.Consumer_Payor_Code__c ||
            opps.Estimated_Cost__c != opps.Account.Estimated_Cost__c ||
            opps.Prescribers__c != opps.Account.Prescribers__c ||
            opps.Medical_Facility__c != opps.Account.Medical_Facility__c ||
            opps.Opp_Territory_AS__c != opps.Account.Territory_AS__c ||
            opps.Opp_Territory_Fax__c != opps.Account.Territory_Fax__c ||
            opps.Opp_Territory_PSS_RingDNA__c != opps.Account.Territory_PSS_RingDNA__c ||
            opps.Opp_Territory_PSS__c != opps.Account.Territory_PSS__c ||
            opps.Opp_Territory_RSS_RingDNA__c != opps.Account.Territory_RSS_RingDNA__c ||
            opps.Opp_Territory_RSS_Supervisor__c != opps.Account.Territory_RSS_Supervisor__c ||
            opps.Opp_Territory_RSS__c != opps.Account.Territory_RSS__c ||
            opps.Opp_Territory_Supervisor__c != opps.Account.Territory_Supervisor__c ||
            opps.Opp_Territory_ID__c != opps.Account.Territory_ID_Lookup__c
            //opps.G6_Program__c != opps.Account.G6_Program__c
            )
        )
        {
            Boolean shouldUpdatePricebook = false;
            if(opps.Pricebook2Id != opps.Account.Default_Price_Book__c){
                shouldUpdatePricebook = true;
            }
            
            if(opps.Account.Payor__c != trigger.oldMap.get(opps.AccountId).Payor__c){
                opps.Payor__c = opps.Account.Payor__c;
            }
            if(opps.Mobile_Phone__c != trigger.oldMap.get(opps.AccountId).PersonContact.MobilePhone)
            {
                opps.Mobile_Phone__c = opps.Account.PersonContact.MobilePhone ;
            }
            // Added by Noy De Goma@CSHERPAS on 11.03.2015
            if(opps.Account.Prescribers__c != trigger.oldMap.get(opps.AccountId).Prescribers__c){
                opps.Prescribers__c = opps.Account.Prescribers__c;
            }
            // Added by Noy De Goma@CSHERPAS on 11.03.2015
            if(opps.Account.Medical_Facility__c != trigger.oldMap.get(opps.AccountId).Medical_Facility__c){
                opps.Medical_Facility__c = opps.Account.Medical_Facility__c;
            }
            //Added by Jagan on 06.12.2017
            //Altered by Jagan on 10.03.2017 Included logic for customer type change on Account
            if(opps.Count_Of_Oppty_Products__c == 0 || accountsWithMedicareChangesSet.contains(opps.AccountId) || shouldUpdatePricebook){
                system.debug('***********Entering the block to update the Oppty pricebook');
                opps.Pricebook2Id = opps.Account.Default_Price_Book__c;
            }
            //Added by Jagan on 06.12.2017
            if(opps.Last_MDCR_Reorder_Follow_up_Type__c != opps.Account.Last_MDCR_Reorder_Follow_up_Type__c){
                opps.Last_MDCR_Reorder_Follow_up_Type__c = opps.Account.Last_MDCR_Reorder_Follow_up_Type__c;
            }
            //Added by Jagan on 06.12.2017
            if(opps.Last_MDCR_Reorder_Follow_up_Date__c != opps.Account.Last_MDCR_Reorder_Follow_up_Date__c){
                opps.Last_MDCR_Reorder_Follow_up_Date__c = opps.Account.Last_MDCR_Reorder_Follow_up_Date__c;
            }
            //Added by Jagan on 06.12.2017
            if(opps.MDCR_Communication_Preference__c != opps.Account.MDCR_Communication_Preference__c){
                opps.MDCR_Communication_Preference__c = opps.Account.MDCR_Communication_Preference__c;
            }
            //Added by Jagan on 06.12.2017
            if(opps.First_MDCR_Order_Non_Direct__c != opps.Account.First_MDCR_Order_Non_Direct__c){
                opps.First_MDCR_Order_Non_Direct__c = opps.Account.First_MDCR_Order_Non_Direct__c;
            }
            //Added by Jagan on 06.12.2017
            if(opps.First_MDCR_Order_Non_Direct_Channel__c != opps.Account.First_MDCR_Order_Non_Direct_Channel__c){
                opps.First_MDCR_Order_Non_Direct_Channel__c = opps.Account.First_MDCR_Order_Non_Direct_Channel__c;
            }
            //Added by Jagan on 10.03.2017
            if((opps.Count_Of_Oppty_Products__c > 0 && accountsWithMedicareChangesSet.contains(opps.AccountId)) || shouldUpdatePricebook){
                openOpptyWithLineItemsToBeDeletedSet.add(opps.Id);
            }
            //Added by Jagan on 10.04.2017
            if(opps.Payor_Code__c != opps.Account.Consumer_Payor_Code__c){
                opps.Payor_Code__c = opps.Account.Consumer_Payor_Code__c;
            }
            //Added by Jagan on 10.19.2017
            if(opps.Estimated_Cost__c != opps.Account.Estimated_Cost__c){
                opps.Estimated_Cost__c = opps.Account.Estimated_Cost__c;
            }
            //Added by Jagan on 10.20.2017
            //Below lines for Roster updates
            if(opps.Opp_Territory_AS__c != opps.Account.Territory_AS__c){
                opps.Opp_Territory_AS__c = opps.Account.Territory_AS__c;
            }
            if(opps.Opp_Territory_Fax__c != opps.Account.Territory_Fax__c){
                opps.Opp_Territory_Fax__c = opps.Account.Territory_Fax__c;
            }
            if(opps.Opp_Territory_PSS_RingDNA__c != opps.Account.Territory_PSS_RingDNA__c){
                opps.Opp_Territory_PSS_RingDNA__c = opps.Account.Territory_PSS_RingDNA__c;
            }
            if(opps.Opp_Territory_PSS__c != opps.Account.Territory_PSS__c){
                opps.Opp_Territory_PSS__c = opps.Account.Territory_PSS__c;
            }
            if(opps.Opp_Territory_RSS_RingDNA__c != opps.Account.Territory_RSS_RingDNA__c){
                opps.Opp_Territory_RSS_RingDNA__c = opps.Account.Territory_RSS_RingDNA__c;
            }
            if(opps.Opp_Territory_RSS_Supervisor__c != opps.Account.Territory_RSS_Supervisor__c){
                opps.Opp_Territory_RSS_Supervisor__c = opps.Account.Territory_RSS_Supervisor__c;
            }
            if(opps.Opp_Territory_RSS__c != opps.Account.Territory_RSS__c){
                opps.Opp_Territory_RSS__c = opps.Account.Territory_RSS__c;
            }
            if(opps.Opp_Territory_Supervisor__c != opps.Account.Territory_Supervisor__c){
                opps.Opp_Territory_Supervisor__c = opps.Account.Territory_Supervisor__c;
            }
            if(opps.Opp_Territory_ID__c != opps.Account.Territory_ID_Lookup__c){
                opps.Opp_Territory_ID__c = opps.Account.Territory_ID_Lookup__c;
            }
            
            //BHU 4/3/2018
            //if(opps.G6_Program__c != opps.Account.G6_Program__c){
            //    opps.G6_Program__c = opps.Account.G6_Program__c;
            //}
            
            //Add to the list to be updated
            oppsToBeUpdated.add(opps);
        }
    }
    
    //Added by Jagan on 10.03.2017
    //Removes the Opp products for the Open opportunitied tied to the accounts, which were affected by Medicare customer type logic change
    if(!openOpptyWithLineItemsToBeDeletedSet.isEmpty())
    {
        //  List holds the opportunity line items, which needs to be removed
        List<OpportunityLineItem> oppLineItemsToBeRemoved = new List<OpportunityLineItem>([SELECT Id FROM OpportunityLineItem WHERE OpportunityId IN : openOpptyWithLineItemsToBeDeletedSet]);
        if(!oppLineItemsToBeRemoved.isEmpty())
        {
            try{
                delete oppLineItemsToBeRemoved;
            }catch(Exception e){
                system.debug('***Opp Line Items Deletion Error = ' + e.getMessage());
            }
        }
    }
    
    //Added logic to not update Opportunity if we dont need to
    if(!oppsToBeUpdated.isEmpty())
    {
        //Updates the Opportunity
        try{
            update oppsToBeUpdated;
        }catch(Exception e){
            system.debug('***Opp Update Error = ' + e.getMessage());
        }
    }
}