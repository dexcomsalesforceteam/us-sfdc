trigger OrderTrigger on Order (before insert, before update, after insert,after update){
/*******************************************************************************************************************
@Author         : Jagan Periyakaruppan
@Date Created   : 02/26/2018
@Description    : Trigger invokes handler class to calculate Copay and Tax, Order Header creation process
********************************************************************************************************************/    
   
    if(trigger.isAfter)
    {        
        if(UtilityClass.runAfterTriggerOnce() || test.isRunningTest()){
        //if(true){
        
            //Update or Insert the customer Order Header and Order Item Detail records
            //List to gather Orders that are to be processed
            List<Order> processOrders = new List<Order>();
            //Order Headers will only be created for Comp Product Orders. Rest Orders Header are still created through Process Builder and Flow.
            for(Order thisOrder :trigger.new)
            {
                 if(thisOrder.status == 'Activated' && 
                     trigger.oldmap.get(thisOrder.id).status != thisOrder.status &&
                     (thisOrder.Sub_Type__c == 'Comp Product Order' || thisOrder.Sub_Type__c == 'SSIP Order')){ 
                     processOrders.add(thisOrder);
                 }
            }
            if(!processOrders.isEmpty())
            {
                ClsOrderTriggerHandler.createOrderHeaderFromOrder(processOrders);  
            }
            if(trigger.isUpdate)
            {
                //System.Debug('** TPS:EH 1.1 OrderTrigger after update trigger.newmap='+ trigger.newmap + ' trigger.oldmap=' + trigger.oldmap);
                ClsOrderTriggerHandler.CalculateCopayOrTax(trigger.newmap, trigger.oldmap);
                //ClsOrderTriggerHandler.afterUpdate(trigger.new, trigger.oldmap);
                //ClsOrderTriggerHandler.CalculateCopay(trigger.newMap, trigger.oldmap);
                //ClsOrderTriggerHandler.CalculateTax(trigger.newMap, trigger.oldmap);
                ClsOrderTriggerHandler.closeOptyOnOrderActivation(trigger.new, trigger.oldmap);
            }
            if(trigger.isInsert){
                ClsOrderTriggerHandler.addAuditAndAuditItems(trigger.new);
                ClsOrderTriggerHandler.invokeBICheck(trigger.new);              
                //ClsOrderTriggerHandler.processDefaultSku(trigger.new);
                //ClsOrderTriggerHandler.CalculateCopay(trigger.newMap, null);                
            }
        }
    } 
    //Added By Shikha, To defaulting the value to 3-days shipping   
    if(trigger.isBefore){
        
        if(trigger.isInsert){
            ClsOrderTriggerHandler.onBeforeInsert(trigger.new);
            ClsOrderTriggerHandler.setPricebookId(trigger.new, null);//added for INC0261240
        }
        
        if(trigger.isUpdate){
            ClsOrderTriggerHandler.setPricebookId(trigger.new, trigger.oldmap);//added for INC0261240
        }
    }
}