trigger OrderItemTrigger on OrderItem (before insert, before update, after insert,after update, before delete) {
    
    if(trigger.isBefore){
       if(trigger.isInsert){
         OrderItemTriggerHandler.beforeInsert(trigger.new);
         OrderItemTriggerHandler.preventOrderItemBundleProducts(trigger.new);
         OrderItemTriggerHandler.preventTestingSuppliesProducts(trigger.new);
         OrderItemTriggerHandler.restrictDupProdcts(trigger.new); 
       }
       if(trigger.isDelete){                        
            ClsOrderTriggerHandler.CalcCoPay=true;
       }
    }
    if(trigger.isafter){
        //CRMSF-3785  : insert default SKU On Order
        if(trigger.isInsert)
        {
            if(UtilityClass.runOnce() || test.isRunningTest()){ 
                //System.debug('** In Adding method ** ');
                OrderItemTriggerHandler.addIfuSKUOnOrder(trigger.new);
                ClsOrderTriggerHandler.CalcCoPay=true;                              
            }
        }
        if(trigger.isUpdate){ 
            system.debug('Well the Trigger was there next is method');
            OrderItemTriggerHandler.updateUnitPriceOnPriceBookChange(trigger.new, trigger.oldMap);            
            ClsOrderTriggerHandler.CalcCoPay=true;            
        }        
               
    }
   
}