/* V1 */
trigger UpdateNotes on Organization_Data__c (before Update) {
    if(Trigger.isUpdate && Trigger.IsBefore){
        OrgDataTriggerHandler.onBeforeUpdate(Trigger.New, Trigger.OldMap);
    }

}