trigger TerritoryAlignmentTrigger on Territory_Alignment__c (before insert, before update, after insert, after update)
{
/*******************************************************************************************************
    @Author        : Jagan Periyakaruppan
    @Date Modified    : 10/19/2017
    @Description    : Trigger invoke trigger handler to update accounts upon Territory alignment update
********************************************************************************************************/
	if(trigger.isAfter)
	{
		if(trigger.isUpdate)
		{
			//TerritoryAlignmentTriggerHandler.afterUpdate(trigger.new, trigger.oldMap);
		}
		
	}
}