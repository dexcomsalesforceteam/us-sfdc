trigger ProcessAccountPrimaryShipToAddressChangeEvent on Account_Primary_ShipTo_Address_Event__e (after insert) {

    //Author : Priyanka 07/24/2018
    //Description : This Trigger will be invoked when a new platform event is entered on Account primary ship to address change
        
    //Map holds the account that are to be updated with the values published via event
    Map<Id, Id> accountsWithAddressChange = new  Map<Id, Id>();
    List<SSIP_Schedule__c> SSIPScheduleToUpdate= new List<SSIP_Schedule__c>(); // schedules to be updated with correct addresses
    
    for(Account_Primary_ShipTo_Address_Event__e newEvent : Trigger.New)
    {
        accountsWithAddressChange.put(newEvent.Account_Id__c, newEvent.Primary_Ship_To_Address__c);        
    }
    
    if(!accountsWithAddressChange.isEmpty()){
        for(SSIP_Schedule__c SSIPSchedule : [SELECT id, Status__c, Shipping_Address__c, Account__c from SSIP_Schedule__c
                                        where Account__c IN :accountsWithAddressChange.KeySet() and Status__c != 'Closed' and Address_Override__c = false ]){
            SSIPSchedule.Shipping_Address__c = accountsWithAddressChange.get(SSIPSchedule.Account__c); //to update respective primary ship to address updated on Account
            SSIPScheduleToUpdate.add(SSIPSchedule);
        }
        
        //Proceed with updates
        if(!SSIPScheduleToUpdate.isEmpty())
        {
            Database.SaveResult[] savedResult = Database.update(SSIPScheduleToUpdate, false); 
            for(Database.SaveResult sr : savedResult)
            {
                if(!sr.isSuccess()){
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Fields that affected this error: ' + err.getFields());
                    }
                    // Ensure we don't retry the trigger more than 4 times
                    if (EventBus.TriggerContext.currentContext().retries < 4) {
                        // Condition isn't met, so try again later.
                        throw new EventBus.RetryableException('Error happened, so retrying the trigger again.');
                    } 
                }
            }                       
        }
     }
}