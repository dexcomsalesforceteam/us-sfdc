/***************************************************************************************
* @Description : Trigger will invoke the process to handle Order Header trigger contexts
**********************************************************************************************/
trigger OrderHeaderMasterTrigger on Order_Header__c (before insert, after update, before update,after insert) {
 
    UtilityClass.executeAccountTrigger = false;//Set the flag to not fire the account trigger logic
    system.debug('inside trigger::::'+system.now());
    system.debug('inside trigger is insert:::'+trigger.isInsert);
    system.debug('inside trigger is update:::'+ trigger.isUpdate);

    //Check to see if the account code to be executed or not
    if(trigger.isBefore && (UtilityClass.runOrderHeaderBeforeTriggerOnce() || test.isRunningTest())){
        if(trigger.isInsert){
            OrderHeaderTriggerHandler.onBeforeInsert(trigger.new);
            OrderHeaderTriggerHandler.beforeInsertPayorAssociate(trigger.new);
        }
    }
    
    //added before Upsert Routine for CRMSF-5319 
    if(trigger.isBefore && trigger.isUpdate && (UtilityClass.runOrderHeaderBeforeUpdateTriggerOnce() || test.isRunningTest())){
        OrderHeaderTriggerHandler.handleBeforeUpdate(trigger.new, trigger.oldMap);
    }
    
    if(trigger.isAfter && (UtilityClass.runOrderHeaderAfterTriggerOnce() || test.isRunningTest())){
        if(trigger.isUpdate){
           // OrderHeaderTriggerHandler.processToSendG6AuthCode(trigger.new, trigger.oldmap); (Commented as CRMSF-4438)
            OrderHeaderTriggerHandler.UpdateShippedDate(Trigger.newMap, Trigger.oldMap);
            //Added by Proficient
            OrderHeaderTriggerHandler.handleShippedUpdate(trigger.newMap, trigger.oldMap);
            OrderHeaderTriggerHandler.handleOrderConfirmation(Trigger.newMap, Trigger.oldMap);
            OrderHeaderTriggerHandler.handleCustomerOnboarding(Trigger.newMap, Trigger.oldMap);
            OrderHeaderTriggerHandler.afterUpdateCreateTask(Trigger.newMap, Trigger.oldMap);//added for CRMSF-4714
        }
    }
}