trigger HealthcareProfessionalLeadTrigger on Healthcare_Professional_Lead__c (after insert) 
{ 

//Jagan 02/27/2017
//Description - Created this trigger to automatically sent the newly created HCP Lead record to Veeva
  
    // Get the S2S connection Id we have setup for connecting to Veeva Org
    List<S2S_Connection__mdt> s2sConnections = new List<S2S_Connection__mdt>([SELECT Connection_Id__c FROM S2S_Connection__mdt WHERE Label = 'Veeva Connection']);
    if(s2sConnections.size() > 0){
        Id networkId = s2sConnections[0].Connection_Id__c;
        
        //Loop through all the HCP Lead Records 
        List<PartnerNetworkRecordConnection> hcpLeadConnections =  new  List<PartnerNetworkRecordConnection>(); 
            
        for (Healthcare_Professional_Lead__c hcpLead : Trigger.new) 
        { 
            PartnerNetworkRecordConnection newHCPLeadConnection = 
              new PartnerNetworkRecordConnection( 
                  ConnectionId = networkId, 
                  LocalRecordId = hcpLead.Id); 
            hcpLeadConnections.add(newHCPLeadConnection); 
        } 

        if (hcpLeadConnections.size() > 0 ) 
        { 
               database.insert(hcpLeadConnections); 
        } 
    }       
}