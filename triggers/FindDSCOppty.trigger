trigger FindDSCOppty on Opportunity (after insert,after update)
{
  List<PartnerNetworkRecordConnection> recordConns = new List<PartnerNetworkRecordConnection>();
  List<Opportunity> opports = new List<Opportunity>();
  Set<Id> OpptyRecIds = new Set<Id>();
  Set<Id> LocalRecIds = new Set<Id>();
  Set<Id> LocalRecIds1 = new Set<Id>();
  List<Opportunity> OppListSent = new List<Opportunity>();
  List<Opportunity> OppListSent1 = new List<Opportunity>();
  
  // BHU 2018-03-22
  Map<Id,Id> mapPartnerConn = new Map<Id, Id>();
  String ConnName;
  
  if(Recursive1.firstrun){
                
             for(Opportunity opp: Trigger.new){
             OpptyRecIds.add(opp.Id);
             }  
              If(OpptyRecIds.size()>0)
                recordConns = [select Id, Status, ConnectionId, LocalRecordId from PartnerNetworkRecordConnection where LocalRecordId in :OpptyRecIds];
             If(recordConns.size()>0)
             {
                for(PartnerNetworkRecordConnection recordConn : recordConns)
                {  
                   //BHU 2018-03-22
                   mapPartnerConn.put(recordConn.LocalRecordId, recordConn.ConnectionId);
            
                   System.Debug('#### LocalRecordID' + recordConn.LocalRecordId);
                   System.Debug('#### ConnectionID' + recordConn.LocalRecordId);       
                    
                   if(recordConn.Status.equalsignorecase('sent') || recordConn.Status.equalsignorecase('pending (sent)'))
                   { 
                          LocalRecIds.add(recordConn.LocalRecordId);
                          System.debug('Record ='+recordConn);
                   }
                   //if(recordConn.Status.equalsignorecase('Inactive') || recordConn.Status.equalsignorecase('deleted'))
                   if(recordConn.Status.equalsignorecase('Inactive') || recordConn.Status.equalsignorecase('deleted') || recordConn.Status.equalsignorecase('converted'))
                   { 
                          LocalRecIds1.add(recordConn.LocalRecordId);
                          System.debug('Record ='+recordConn);
                   }
                }
             }

OppListSent = [select Id,Received_Connection_Name__c from Opportunity where Id in : LocalRecIds AND Received_Connection_Name__c = NULL];
if(OppListSent.size()>0){
     for(Opportunity Opp: OppListSent){
                //BHU 2018-03-22
                if(mapPartnerConn.get(Opp.Id) == '04P400000008trVEAQ'){
                    ConnName = 'DSC';
                }
                else
                {
                    ConnName = 'SOL';
                }
                //Opp.Received_Connection_Name__c = 'DSC';
                
                System.Debug('#### ID' + Opp.Id);
                System.Debug('#### ConnName' + ConnName);
                
                Opp.Received_Connection_Name__c = ConnName;
                
                system.debug('Opp.Received_ConnectionName__c'+Opp.Received_Connection_Name__c);
               
     }
} 
OppListSent1 = [select Id,Received_Connection_Name__c from Opportunity where Id in : LocalRecIds1 AND Received_Connection_Name__c != NULL];
if(OppListSent1.size()>0){
     for(Opportunity Opp: OppListSent1){
                Opp.Received_Connection_Name__c = '';
                system.debug('Opp.Received_ConnectionName__c'+Opp.Received_Connection_Name__c);
               
     }
} 

try{
If(OppListsent.size()>0)
update OppListSent;
If(OppListsent1.size()>0)
update OppListSent1;
}

catch(Exception e) {System.debug('FindDSCOppty Exception'+e.getMessage());}
}
Recursive1.firstrun=false;  
}