trigger TaskTrigger on Task (before insert,before update, before delete, after insert, after update) {
/********************************************************************************
@author Abhishek Parghi
@date 01/29/2016
@description:Task Trigger
*******************************************************************************/
    if(trigger.isBefore){
        if(trigger.isInsert){
            TaskTriggerHandler.TaskFieldUpdates(trigger.new);
        }
        
        //Jagan 08/14/2018 - Invoke the process to handle task deletion
        if(Trigger.isDelete)   TaskTriggerHandler.ProcessTaskDeletion(trigger.old);
    }
    if(trigger.isafter){
        if(trigger.isInsert){    
            TaskTriggerHandler.AutoshareTasks(trigger.new);              
        }if(trigger.isUpdate){
            TaskTriggerHandler.AutoshareTasks(trigger.new);
            TaskTriggerHandler.updateEscalationStatus(trigger.new, trigger.oldMap);
        }
        
    }
}