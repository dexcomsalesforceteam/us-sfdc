/*
  @Author           : LTI
  @Date Created     : 31/07/2017
  @Description      : Invokes the trigger handler class
*/
trigger EscalationTrigger on Escalation__c (after insert, after update, before update) {

    if(trigger.isAfter){
        if(trigger.isInsert){
            EscalationTriggerHandler.handleAfterInsert(trigger.new);
        }
        
        if(trigger.isUpdate){
            EscalationTriggerHandler.handleAfterUpdate(trigger.oldMap, trigger.new);
        }
    }
    
    if(trigger.isBefore){
        if(trigger.isUpdate){
            EscalationTriggerHandler.handleBeforeUpdate(trigger.new, trigger.oldMap, trigger.newMap);
        }
    }
    
}