/*
 * Created for CRMSF-5189
 * New Account will be created in OCE org and it will publish
 * an event to Kakfa layer. Kafka will read the event and process
 * to publish CRM_Account_Event__e in US SF
 * @Author - Tanay, LTI
 * @Creation Date - May 15, 2020
 * @Modification - Implemented batch process to limit records for further processing. Date - June 29, 2020. By - Tanay, LTI
*/
trigger AccountEventInTrigger on CRM_Account_Event__e (after insert) {
    if(Boolean.valueOf(Label.Run_Account_Event_In_Trigger)){//run the trigger logic based on boolean custom label value
        
        List<CRM_Account_Event__e> accountInProcessList = new List<CRM_Account_Event__e>(); //process list used to pass to handler class
        Integer batchProcessListCounter = ClsApexConstants.ZERO_INTEGER;
        
        for(CRM_Account_Event__e accountInEvent : trigger.new){
            batchProcessListCounter++;
            if(batchProcessListCounter > Integer.valueOf(Label.Account_In_Event_Batch_Size)){
                break;
            }else{
                accountInProcessList.add(accountInEvent);
                EventBus.TriggerContext.currentContext().setResumeCheckpoint(accountInEvent.ReplayId);
            }
        }
        
        if(!accountInProcessList.isEmpty()){
            ClsAccountAddrInEvtTriggerHandler.handleAccountEventAfterInsert(accountInProcessList);
        }
    }
}