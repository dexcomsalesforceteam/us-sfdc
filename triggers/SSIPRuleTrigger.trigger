/*
  @Author           : LTI
  @Date Created     : 08/11/2018
  @Description      : Invokes the trigger handler class
*/
trigger SSIPRuleTrigger on SSIP_Rule__c (before insert, before update , after insert, after update) {
 
    ClsSSIPRuleTriggerHandler triggerHandler = new ClsSSIPRuleTriggerHandler();//Invokes the trigger handler class
    
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            triggerHandler.beforeInsert(trigger.new);
            ClsSSIPRuleTriggerHandler.preventUnPriceBookProducts(trigger.new);
        }
    }
    if(Trigger.isAfter){
        if(Trigger.isInsert){
            triggerHandler.afterInsert(trigger.new);
        }
        
        if(Trigger.isUpdate){
            triggerHandler.afterUpdate(trigger.newMap, trigger.oldMap);
            triggerHandler.handleAfterUpdate(trigger.new, trigger.oldMap);
        }
    }
}