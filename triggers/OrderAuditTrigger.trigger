trigger OrderAuditTrigger on Order_Audit__c (after update) {

    if(trigger.isUpdate){
        OrderAuditTriggerHandler.updateOrderForQcholdChecks(trigger.newMap);
    }
}