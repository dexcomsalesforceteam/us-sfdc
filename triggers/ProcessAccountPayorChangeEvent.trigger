trigger ProcessAccountPayorChangeEvent on Account_Payor_Change__e (after insert) {

    //Author : Priyanka 07/24/2018
    //Description : This Trigger will be invoked when a new platform event is entered on Account Payor change
        
    //List holds the account that are to be updated with the territory value published via event
    List<Id> accountsWithPayorChange = new List<Id>();
    List<Id> SSIPReadyToSendEmail = new List<Id>();
    List<SSIP_Rule__c> SSIPRuleToUpdate= new List<SSIP_Rule__c>();
    List<Id> updatedRecords = new List<Id>();
    
    for(Account_Payor_Change__e newEvent : Trigger.New)
    {
        accountsWithPayorChange.add(newEvent.Account_Id__c);
    }
    
    if(!accountsWithPayorChange.isEmpty()){
        for(SSIP_Rule__c SSIPRule : [SELECT id,Rule_End_Date__c,Rule_Start_Date__c,Account__c, First_Shipment_Date__c from SSIP_Rule__c 
                                        where Account__c IN :accountsWithPayorChange and (Rule_End_Date__c = null or Rule_End_Date__c > Today) ]){
            //if First Shipment Date does not fall in between Rule Start and End dates
                if(!String.isEmpty(String.valueOf(SSIPRule.First_Shipment_Date__c)) && SSIPRule.First_Shipment_Date__c > System.Today()){
                    SSIPRule.Rule_End_Date__c  = SSIPRule.First_Shipment_Date__c; //to avoid validation rule error
                }else{                                      
                    //to handle the future rules
                    if(SSIPRule.Rule_Start_Date__c > System.Today()) SSIPRule.Rule_End_Date__c = SSIPRule.Rule_Start_Date__c;
                    else SSIPRule.Rule_End_Date__c = System.Today();
                }
            SSIPRule.Close_Reason__c = 'Account Payor Changed';
            SSIPRuleToUpdate.add(SSIPRule);
        }
        
        //Proceed with updates
        if(!SSIPRuleToUpdate.isEmpty())
        {
            Database.SaveResult[] savedResult = Database.update(SSIPRuleToUpdate, false); 
            for(Database.SaveResult sr : savedResult)
            {
                if(!sr.isSuccess()){
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Fields that affected this error: ' + err.getFields());
                    }
                    // Ensure we don't retry the trigger more than 4 times
                    if (EventBus.TriggerContext.currentContext().retries < 4) {
                        // Condition isn't met, so try again later.
                        throw new EventBus.RetryableException('Error happened, so retrying the trigger again.');
                    } 
                }else
                        updatedRecords.add(sr.getId());
            }                       
        }
         //insert Marketing Interaction
        if(!updatedRecords.isEmpty()){
            //create List to insert marketing interaction records
            List<Marketing_Interaction__c> marketingInteractionList = new List<Marketing_Interaction__c>();
            for(SSIP_Rule__c SSIPRule :SSIPRuleToUpdate){
                if(updatedRecords.contains(SSIPRule.Id)){
                    Marketing_Interaction__c thisMI = new Marketing_Interaction__c();
                            thisMI.Source_Record_Id__c = SSIPRule.Id;  // Add Source Record Id as Opty Id
                            thisMI.Account__c = SSIPRule.Account__c; //Account Id
                            thisMI.Communication_Type__c =   'SSIP Cancellation Payor Change';
                            marketingInteractionList.add(thisMI); //add Record to List to Insert
                }
            }
            if(!marketingInteractionList.isEmpty()){
                System.debug('final List >> ' + marketingInteractionList);
                        Database.insert(marketingInteractionList,false);  //Insert Marketing Interaction Records
            }            
        }
     }
}