/********************************************************************************
@author Abhishek Parghi
@date 1/27/2017
@description: Apex Trigger for Comp Product Requests Workflow.
@Modified By: 20 Sep: Priyanka Kajawe : Comp Product Order Generation 
*******************************************************************************/
trigger CompProductRequestsTrigger on Comp_Product_Request__c (before insert,after insert, before update, after update) {
       Map<Id,Comp_Product_Request__c> compProdOldMap = new Map<Id,Comp_Product_Request__c>();

      if(trigger.isBefore){
          if(trigger.isInsert){
            CompProductTriggerHandler.validateTestingSuplies(trigger.new,compProdOldMap);
          }
        if(trigger.isUpdate){
            CompProductTriggerHandler.NewDXNotesAfterUpdate(trigger.new);   
            CompProductTriggerHandler.validateTestingSuplies(trigger.new, trigger.oldMap);
        }
      }
      
      /**START: Comp Product Order Generation **/
      if(trigger.isAfter){
        if(trigger.isUpdate){
            //if(UtilityClass.runOnce() || test.isRunningTest()){
                Set<Id> approvedCompProductRequests = new Set<Id>(); //Set of Id to store approved Comp Product requests
                for(Comp_Product_Request__c compProductRequest : trigger.new){ //check for Approved Comp Product Requests
                    if((compProductRequest.Generate_Order__c != trigger.oldMap.get(compProductRequest.id).Generate_Order__c && 
                        compProductRequest.Generate_Order__c == true) || (compProductRequest.Approval_Status__c =='Approved' &&
                        compProductRequest.Approval_Status__c != trigger.oldMap.get(compProductRequest.id).Approval_Status__c &&
                        compProductRequest.createdDate >= Date.valueOf(System.Label.Comp_Product_Order_Generation_Date))){
                              approvedCompProductRequests.add(compProductRequest.Id);   
                        }
                    }
            		system.debug('approved comp request list size::::'+approvedCompProductRequests.size());
                    //invoke Order Generation with Approved Comp Product Requests
                    if(!approvedCompProductRequests.isEmpty())  CompProductTriggerHandler.generateCompProductOrder(approvedCompProductRequests); 
           // }       
        }
      }
     /**END: Comp Product Order Generation **/
}