/*
 * This event is created for CRMSF-5189
 * New Account will be created in OCE org and it will publish
 * an event to Kakfa layer. Kafka will read the event and process
 * to publish CRM_Address_Event__e in US SF
 * @Author - Tanay, LTI
 * @Creation Date - May 15, 2020
 * @Modification - Implemented batch process to limit records for further processing. Date - June 29, 2020. By - Tanay, LTI
*/
trigger AddressEventInTrigger on CRM_Address_Event__e (after insert) {
    if(Boolean.valueOf(Label.Run_Address_Event_In_Trigger)){
        List<CRM_Address_Event__e> addressInProcessList = new List<CRM_Address_Event__e>(); //process list used to pass to handler class
        Integer batchProcessListCounter = ClsApexConstants.ZERO_INTEGER;
        
        for(CRM_Address_Event__e addressInEvent : trigger.new){
            batchProcessListCounter++;
            if(batchProcessListCounter > Integer.valueOf(Label.Account_In_Event_Batch_Size)){
                break;
            }else{
                addressInProcessList.add(addressInEvent);
                EventBus.TriggerContext.currentContext().setResumeCheckpoint(addressInEvent.ReplayId);
            }
        }
                    
        if(!addressInProcessList.isEmpty()){
            ClsAccountAddrInEvtTriggerHandler.handleAddressEventAfterInsert(addressInProcessList);
        }
    }
}