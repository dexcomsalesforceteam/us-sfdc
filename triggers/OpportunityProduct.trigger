trigger OpportunityProduct on OpportunityLineItem (before insert,after insert, before delete) {
    if(trigger.isBefore){
        if(trigger.isInsert){
            OpportuntiyLineItemTriggerHandler.beforeInsert(trigger.new);
            OpportuntiyLineItemTriggerHandler.preventBundleProducts(trigger.new);
            OpportuntiyLineItemTriggerHandler.restrictDupProdcts(trigger.new); 
        }        
    }
    
    if(trigger.isAfter){
        if(trigger.isInsert){
            OpportuntiyLineItemTriggerHandler.addIfuSkuOnOpportunity(trigger.new);
        }   
    }
}