/***************************************************************************************
* @author      : Jagan Periyakaruppan
* @date        : Feb 13 2019
* @description : Trigger will invoke the process to handle Order Item trigger contexts
**********************************************************************************************/
trigger OrderItemDetailMasterTrigger on Order_Item_Detail__c (before insert, before update,after insert, after update){
    if(trigger.isBefore){
        if(trigger.isInsert){
            OrderItemDetailTriggerHandler.UpdateGenerationInfo(trigger.new);
            //PCSCallList.OIDAccountIdUpdate(trigger.new); - commented for CRMSF-4583/4584 
        }   
        if(trigger.isUpdate){
            OrderItemDetailTriggerHandler.UpdateGenerationInfo(trigger.new);
           //PCSCallList.OIDAccountIdUpdate(trigger.new); - commented for CRMSF-4583/4584
        }
    }
    if(trigger.isAfter){
        if(trigger.isUpdate){
            //PCSCallList.OIDInitialCallList(trigger.new); - commented for CRMSF-4583/4584
            OrderItemDetailTriggerHandler.handleUpdateMIGeneration(Trigger.newMap, Trigger.oldMap);
        } else if (Trigger.isInsert) {
            OrderItemDetailTriggerHandler.generateMarketingInteractions(Trigger.new);
        }
    }
}