//Author : Anuj 11/28/2018
//Description : This Trigger will be invoked when a website order is created with MDCR Followup field as null. 
trigger ProcessToAssociateOrderHeaderToMFR on Associate_Order_Header_To_MFR_Event__e (after insert) {
    
    //Initiate collections to be worked on
    Map<String, Id> orderNumberToOrderIdMap = new Map<String, Id>();//Map between the 'Order Number' to the 'Order Header Id' 
    Map<Id, Id> orderHeaderIdToMFRIdMap = new Map<Id, Id>();//Map between the 'Order Header Id' to the 'MFR Id'
    Map<Id, Order_Header__c> orderHeadersToBeUpdatedMap = new Map<Id, Order_Header__c>();//Map holds the unique Order Headers that are to be updated with MFR Reference

    //Prepare the map between the Order Number and the Order Id
    for(Associate_Order_Header_To_MFR_Event__e newEvent : Trigger.New)
    {
        system.Debug('newEvent.Order_Number__c'+newEvent.Order_Number__c);
        system.Debug('newEvent.Order_Id__c'+newEvent.Order_Id__c);
        orderNumberToOrderIdMap.put(newEvent.Order_Number__c, newEvent.Order_Id__c );
    }
    //Prepare the map between the Order Id and the MFR Id
    for(MDCR_Followup__c mfr : [SELECT Id,Order_Number__c FROM MDCR_Followup__c WHERE Order_Number__c IN: orderNumberToOrderIdMap.keySet()])
    {
        system.Debug('orderNumberToOrderIdMap.get(mfr.Order_Number__c)'+orderNumberToOrderIdMap.get(mfr.Order_Number__c));
        If(orderNumberToOrderIdMap.get(mfr.Order_Number__c) != null) 
        {
            system.debug('orderNumberToOrderIdMap.get(mfr.Order_Number__c)' +orderNumberToOrderIdMap.get(mfr.Order_Number__c));
            system.debug('mfr.Id'+mfr.Id);
            orderHeaderIdToMFRIdMap.put(orderNumberToOrderIdMap.get(mfr.Order_Number__c), mfr.Id);
        }
            
    }
    //Process all the Orders, which need to be linked to the MFRs
    for(Order_Header__c oh : [SELECT Id FROM Order_Header__c WHERE Id IN :orderHeaderIdToMFRIdMap.keySet()])
    {
        system.debug('orderHeaderIdToMFRIdMap.get(oh.Id)'+orderHeaderIdToMFRIdMap.get(oh.Id)); 
        system.debug('oh'+oh); 
        oh.MDCR_Followup__c = orderHeaderIdToMFRIdMap.get(oh.Id);
        orderHeadersToBeUpdatedMap.put(oh.Id, oh);
    }
    //Update Order Headers with MFR Reference
    if(!orderHeadersToBeUpdatedMap.isEmpty())
    {
        Database.SaveResult[] savedResult = Database.update(orderHeadersToBeUpdatedMap.values(), false); 
        for(Database.SaveResult sr : savedResult)
        {
            if(!sr.isSuccess()){
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Fields that affected this error: ' + err.getFields());
                }
                // Ensure we don't retry the trigger more than 4 times
                if (EventBus.TriggerContext.currentContext().retries < 4) {
                    // Condition isn't met, so try again later.
                    throw new EventBus.RetryableException('Error happened, so retrying the trigger again.');
                } 
            }
        }
    }
}