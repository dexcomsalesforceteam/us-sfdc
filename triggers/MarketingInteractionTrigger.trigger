/*********************************************************
* Author : LTI (Priyanka Kajawe)  03/09/2020
* Description : Marketing Interaction Trigger
**********************************************************/

trigger MarketingInteractionTrigger on Marketing_Interaction__c (before insert) {
 ClsMarketingInteractionTriggerHandler.populateMarketingAccount(trigger.new);
}