trigger ProcessAccountGenerationChange on Account_Generation_Change__e (after insert) {
    //Author : Priyanka 11/02/2018
    //Description : This Trigger will be invoked when a new platform event is entered on Account Generation change
        
    //Map holds the account data that are to be updated with the value published via event
    Map<Id, Integer> accountsGenerationChangeMap = new Map<Id, Integer>();
    List<SSIP_Rule__c> SSIPRulesHigherGen= new List<SSIP_Rule__c>(); //LIst holds SSIP Rule to update
    List<SSIP_Rule__c> SSIPRuleToUpdate= new List<SSIP_Rule__c>();
    List<Id> updatedRecords = new List<Id>();
    
    //Create Map for Account Id and its generation Number
    for(Account_Generation_Change__e newEvent : Trigger.New){
        Integer accountGeneration = Integer.valueOf(newEvent.New_Generation__c.right(1)); //Get Generation Number
        accountsGenerationChangeMap.put(newEvent.Account_Id__c,accountGeneration);
    }
    System.debug('accountsGenerationChangeMap>> ' + accountsGenerationChangeMap);
    if(!accountsGenerationChangeMap.keySet().isEmpty()){
        for(SSIP_Rule__c thisSSIPRule : [SELECT id,Rule_End_Date__c,Product__c,Account__c,First_Shipment_Date__c,Rule_Start_Date__c from SSIP_Rule__c 
                                        where Account__c IN :accountsGenerationChangeMap.keySet() and (Rule_End_Date__c = null or Rule_End_Date__c > Today) ]){
            System.debug('thisSSIPRule.Product__c >> '+ thisSSIPRule.Product__c);
            Integer productGeneration = Integer.valueOf(thisSSIPRule.Product__c.deleteWhitespace().Split('\\|')[0].right(1));
            if(productGeneration < accountsGenerationChangeMap.get(thisSSIPRule.Account__c)) //check if SSIP Rule Generation is lesser than Account generation
                SSIPRulesHigherGen.add(thisSSIPRule);
        }
        //Cancel SSIP Rules   
        if(!SSIPRulesHigherGen.isEmpty()){
            for(SSIP_Rule__c SSIPRule : SSIPRulesHigherGen){
                //if First Shipment Date does not fall in between Rule Start and End dates
                if(!String.isEmpty(String.valueOf(SSIPRule.First_Shipment_Date__c)) && SSIPRule.First_Shipment_Date__c > System.Today()){
                    SSIPRule.Rule_End_Date__c  = SSIPRule.First_Shipment_Date__c; //to avoid validation rule error
                }else{                                      
                    //to handle the future rules
                    if(SSIPRule.Rule_Start_Date__c > System.Today()) SSIPRule.Rule_End_Date__c = SSIPRule.Rule_Start_Date__c;
                    else SSIPRule.Rule_End_Date__c = System.Today();
                }
                SSIPRule.Close_Reason__c = 'Account Generation Changed';
                SSIPRuleToUpdate.add(SSIPRule);
             }
             
            Database.SaveResult[] savedResult = Database.update(SSIPRuleToUpdate, false); 
                for(Database.SaveResult sr : savedResult){
                    if(!sr.isSuccess()){
                        for(Database.Error err : sr.getErrors()) {
                            System.debug('The following error has occurred.');                    
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            System.debug('Fields that affected this error: ' + err.getFields());
                        }
                        // Ensure we don't retry the trigger more than 4 times
                        if (EventBus.TriggerContext.currentContext().retries < 4) {
                            // Condition isn't met, so try again later.
                            throw new EventBus.RetryableException('Error happened, so retrying the trigger again.');
                        } 
                    }else
                        updatedRecords.add(sr.getId());
                }              
        }
        //insert Marketing Interaction
        if(!updatedRecords.isEmpty()){
            //create Map to insert marketing interaction records
            List<Marketing_Interaction__c> marketingInteractionList = new List<Marketing_Interaction__c>();
            for(SSIP_Rule__c SSIPRule :SSIPRuleToUpdate){
                if(updatedRecords.contains(SSIPRule.Id)){
                    Marketing_Interaction__c thisMI = new Marketing_Interaction__c();
                            thisMI.Source_Record_Id__c = SSIPRule.Id;  // Add Source Record Id as Opty Id
                            thisMI.Account__c = SSIPRule.Account__c; //Account Id
                            thisMI.Communication_Type__c =   'SSIP Cancellation Gen Switch';
                            marketingInteractionList.add(thisMI); //add Record to List to Insert
                }
            }
            if(!marketingInteractionList.isEmpty()){
                System.debug('final List >> ' + marketingInteractionList);
                    Database.insert(marketingInteractionList,false);  //Insert Marketing Interaction Records
            }
        }
    }
}