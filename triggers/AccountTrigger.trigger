trigger AccountTrigger on Account (before insert, before update, after update,before delete, after delete, after insert) {
    /*******************************************************************************************************************
@Description    : Cleaned up commented code and trailing comments
********************************************************************************************************************/  
    /************************************************Trigger current Implementation Start*******************************************************/
    system.debug('*****UtilityClass.executeAccountTrigger' + UtilityClass.executeAccountTrigger); 
    if(UtilityClass.executeAccountTrigger)//Check to see if the account code to be executed or not
    {
        if(trigger.isBefore && (UtilityClass.runAccountBeforeTriggerOnce() || test.isRunningTest())){
            if(trigger.isInsert){
                AccountTriggerHandler.onBeforeInsert(trigger.new);
                ClsAccountAddressTriggerHandler.ProcessAccountShippingAddressOnAccountInsert(trigger.new);
                AccountTerritoryUpdate.ConsumersTerritoryBeforeInsert(trigger.new);
                AccountTriggerHandler.updateDefaultPricebookOnInsert(trigger.new);
                AccountPrescriberUpdate.PrescriberAddressBeforeUpdate(trigger.new,null);
				AccountTriggerHandler.updateUniqueIntegrationId(trigger.new,null);//added for HUB - 5592
            }
            if(trigger.isUpdate){
                AccountTriggerHandler.onBeforeUpdate(trigger.new, trigger.oldMap); 
                ClsAccountAddressTriggerHandler.processAccountAddressBeforeUpdate(trigger.new,trigger.oldMap);
                AccountTerritoryUpdate.PrescribersTerritoryBeforeUpdate(Trigger.new);
                AccountTerritoryUpdate.ConsumersTerritoryBeforeUpdate(trigger.new,trigger.oldmap);
                AccountTerritoryUpdate.ConsumersOwnerupdate(Trigger.new);
                AccountTerritoryUpdate.EMEAConsumerTerritoryUpdate(trigger.new);    //to be commented
                AccountTerritoryUpdate.EMEAConsumerOwnerUpdate(trigger.new); //to be commented
                AccountTerritoryUpdate.CAConsumerTerritoryUpdate(trigger.new);    //to be commented
                AccountTerritoryUpdate.CAConsumerOwnerUpdate(trigger.new); //to be commented
                AccountTriggerHandler.updateDefaultPricebookOnUpdate(trigger.new, trigger.oldMap);   
                //AccountTriggerHandler.calculateCopayOnUpdate(trigger.new, trigger.oldMap); 
                AccountTriggerHandler.doubleOptInBefore(trigger.new,trigger.oldMap);
                AccountPrescriberUpdate.PrescriberAddressBeforeUpdate(trigger.new,trigger.oldMap);  
				AccountTriggerHandler.updateUniqueIntegrationId(trigger.new,trigger.oldMap);//added for HUB - 5592
            }
            
            // Added By Sudheer Vemula (CRMSF-5165) : If the patient account date of death is populated then closing the ssip rueles of the patient.
            if(trigger.isUpdate){
                AccountTriggerHandler.cancleSSIPrules(Trigger.New,trigger.oldmap);
             }
        }
        if(trigger.isAfter && (UtilityClass.runAccountAfterTriggerOnce() || test.isRunningTest())){
            if(trigger.isInsert){
                AccountTriggerHandler.smsCall(null,trigger.newmap);
                AccountTriggerHandler.doubleOptInAfter(trigger.new,null);
				AccountTriggerHandler.createOpptyOnTerrCodeUpdate(trigger.new,null);//added for HUB - 5592
                //Proceed to make changes to Address object only when the address change (BILL_TO or SHIP_TO) happened from Account object       
                if(ClsAccountAddressTriggerStaticClass.addressChangeInvokedFromAccount)
                    ClsAccountAddressTriggerHandler.ProcessAccountRelatedAddressOnAccountInsert(trigger.new);
            }
            if(trigger.isUpdate){
                AccountTriggerHandler.smsCall(trigger.oldMap,trigger.newmap);
                AccountTriggerHandler.doubleOptInAfter(trigger.new,trigger.oldMap);
                AccountTerritoryUpdate.TaskFieldUpdate(trigger.new,trigger.oldMap,trigger.newmap);
                AccountTriggerHandler.updateOpenMFRs(trigger.new, trigger.oldMap);  
                AccountTriggerHandler.UpdateOpportunities(trigger.new,trigger.newmap,trigger.oldMap);
                AccountTriggerHandler.handleAfterUpdate(trigger.new, trigger.oldMap);
                //AccountTriggerHandler.adjustSSIPFirstShipments(trigger.new, trigger.oldMap); //CRMSF-4521
                AccountTriggerHandler.updateOpenOpptyNames(trigger.new, trigger.oldMap); //added for CRMSF-5309
				AccountTriggerHandler.createOpptyOnTerrCodeUpdate(trigger.new,trigger.oldMap);//added for HUB - 5592
                //AccountTriggerHandler.publishPlatformEvent(trigger.new, trigger.oldMap);
                //Proceed to make changes to Address object only when the address change (Bill To or Ship To) happened from Account object 
                if(ClsAccountAddressTriggerStaticClass.addressChangeInvokedFromAccount)
                    ClsAccountAddressTriggerHandler.ProcessAccountRelatedAddressOnAccountUpdate(trigger.newmap, trigger.oldmap); 
            }  
            
        }
    }       
}

/************************************************Trigger current Implementation END*******************************************************/