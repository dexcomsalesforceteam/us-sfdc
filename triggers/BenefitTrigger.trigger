trigger BenefitTrigger on Benefits__c (before insert, after update, before update) {

    if(trigger.isBefore){
        if(trigger.isInsert){
            BenefitTriggerHandler.beforeInsert(trigger.new);
            BenefitTriggerHandler.updateClaimAddress(trigger.new, null); //Update claim mailing address upon Benefit entry with Payor association
            BenefitTriggerHandler.populateAuthProducts(trigger.new, null); //Update the Prior Auth information upon Benefit entry with Payor association--SHOULD ACTIVATE AFTER QUADAX QC HOLD ROLLOUT
        }        
        if(trigger.isUpdate){
            if(utilityclass.runBeforeTriggerOnce()){
                BenefitTriggerHandler.beforeUpdate(trigger.new, trigger.oldMap);
                BenefitTriggerHandler.updateTheHierarchyAndRID(trigger.newMap,trigger.oldMap);
                BenefitTriggerHandler.updatePlanNamePlanTypeValues(trigger.new, trigger.oldMap);//Copy plan name and plan type values on update
                BenefitTriggerHandler.cleanUpBenefitDataForBICheck(trigger.new, trigger.oldMap);//Clear up field values for BI Check 
                BenefitTriggerHandler.updateClaimAddress(trigger.new, trigger.oldMap); //Update claim mailing address upon Payor change
                BenefitTriggerHandler.populateAuthProducts(trigger.new, trigger.oldMap); //Update the Prior Auth information based on the Payor change--SHOULD ACTIVATE AFTER QUADAX QC HOLD ROLLOUT
            }     
        }
    }
    
    if(trigger.isAfter){
        if(trigger.isUpdate){
            if(utilityclass.runBenefitAfterTriggerOnce()){
                BenefitTriggerHandler.afterUpdate(trigger.new, trigger.oldMap);
                BenefitTriggerHandler.processBIVerification(trigger.new, trigger.oldMap);//BI Verification call when the Payor is updated
                BenefitTriggerHandler.updateOrderHeaderOnBenefitUpdate(trigger.new, trigger.oldMap); //added for CRMSF-4613
                BenefitTriggerHandler.updateAccLastMemberIDDate(trigger.new, trigger.oldMap); // added for  CRMSF - 5176 
            }
        }
        if(trigger.isInsert){}
    }
}