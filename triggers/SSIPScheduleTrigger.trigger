/*
Auther      : Kruti bhusan Mohanty
Date        : 07/26/18
Description : If 'Scheduled Shipment Date' is updated on any schedule, Update next schedules according to Frequency on Rule
*/
trigger SSIPScheduleTrigger on SSIP_Schedule__c (before insert, before update , after insert, after update) {
    ClsSSIPScheduleTriggerHandler triggerHandler = new ClsSSIPScheduleTriggerHandler();//Invokes the trigger handler class
    
    if(Trigger.isAfter){
        if(Trigger.isUpdate){           
            if(ClsSSIPApexUtility.checkFirstRun)
                triggerHandler.afterUpdate(trigger.newMap, trigger.oldMap);              
        }
    } else{
                 
        triggerHandler.ssipScheduleUpdateShippingMethod(trigger.new, trigger.oldmap); // this will update Shipping method on Insert & Update
        
        //Added by Sudheer Vemula CRMSF-4906 : If bypass SSIP checked Then the BI, Auth, CMN, and credit card updated to checked and opportunity closed.  
        if(trigger.isUpdate){
            ClsSSIPScheduleTriggerHandler.ssipByPassCheckedcloseOpps(trigger.new, trigger.oldmap);
        }
    }  
}