/*
 * @Description - This platform event updates Next_Possible_Date_For_Sensor_Order__c on account
 * based on the payor rules matrix for sensor packs
 * 
*/
trigger ProcessNextPossibleDateForSensorOrder on Account_Next_Date_For_Sensor_Order_Event__e (after insert) {
    
    //batch event list holds 200 records and passes to helper method to carry out business process
    List<Account_Next_Date_For_Sensor_Order_Event__e> batchProcessEventList = new List<Account_Next_Date_For_Sensor_Order_Event__e>();
    //apex debug log list to track the entry list records
    List<Apex_Debug_Log__c> apexDebugLogList = new List<Apex_Debug_Log__c>();
    //batch process event list counter variable
    Integer batchProcessListCounter = ClsApexConstants.ZERO_INTEGER;
    for(Account_Next_Date_For_Sensor_Order_Event__e newEvent : Trigger.New){
        Apex_Debug_Log__c apexDebugLog = new Apex_Debug_Log__c();
        apexDebugLog.Apex_Class__c = 'NextPossibleDateSensorPlatformEvent';
        apexDebugLog.Method__c = 'Entry Point in Platform Trigger at Time : '+system.today()+' Total List size is: '+Trigger.New.Size();
        apexDebugLog.Message__c = 'Account Entry with Id : '+newEvent.Account_Id__c+' Payor Id is : '+newEvent.Payor_Id__c;
        apexDebugLog.Record_Id__c = newEvent.Account_Id__c;
        apexDebugLog.Type__c = 'Information';
        apexDebugLogList.add(apexDebugLog);
        //increment the counter
        batchProcessListCounter++; 
        //if counter is greater than 200 then break the loop
        if(batchProcessListCounter > Integer.valueOf(system.label.Next_Possible_Date_Sensor_Order_Batch_Size)){
            break;
        }else{
            //if counter is not greater than 200 then add event record in the batch list
            //setResumeCheckpoint as current record's replay Id, for next prorcess the trigger resumes from 
            //last check point in the process
            batchProcessEventList.add(newEvent);
            EventBus.TriggerContext.currentContext().setResumeCheckpoint(newEvent.ReplayId);
        }
    }
    
    //if the batchProcessEventList is not empty then pass list to helper class to process the business logic
    if(!batchProcessEventList.isEmpty()){
        ClsPlatformEvtTriggerHelper.processToUpdateAccounts(batchProcessEventList);
    }
    //insert into debug log for tracking records
    if(!apexDebugLogList.isEmpty()){
        Database.insert(apexDebugLogList,false);
    }
}