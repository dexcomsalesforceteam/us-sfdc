trigger ProcessAccountFieldsUpdateEventTrigger on Account_Fields_Update_Event__e (after insert) {
    
    //Author : Jagan 11/4/2018
    //Description : This Trigger will be invoked when a new event is entered upon Account field update action
    
    //Prepare the account id set when any one of the Medicare fields are updated from Account object
    Set<String> medicareAccountIdsForUpdateSet = new Set<String>();
    //Prepare the account id set when any one of the Medicare fields are updated from Opportunity object
    Set<String> medicareOpptyAccountIdsForUpdateSet = new Set<String>();
    //Collections used in the process
    List<Opportunity> openOppsToBeUpdated = new List<Opportunity>();
    List<Account> accntsToBeUpdated = new List<Account>();
    
    //Map holds the accounts, which are updated
    Map<Id, Account_Fields_Update_Event__e> accountFieldsUpdateMap = new Map<Id, Account_Fields_Update_Event__e>();
    
    //For each published event segregate the data to be processed for updates
    for(Account_Fields_Update_Event__e newEvent : Trigger.New)
    {
        if(newEvent.Updated_From__c == 'Account')
        {
            medicareAccountIdsForUpdateSet.add(newEvent.Account_Id__c);
            accountFieldsUpdateMap.put(newEvent.Account_Id__c, newEvent);
        }
        if(newEvent.Updated_From__c == 'Opportunity')
        {
            medicareOpptyAccountIdsForUpdateSet.add(newEvent.Account_Id__c);
            accountFieldsUpdateMap.put(newEvent.Account_Id__c, newEvent);
        }
    }
    
    //Process the updates happened from Account
    if(!medicareAccountIdsForUpdateSet.isEmpty())
    {
        for(Opportunity oppToBeUpdated : [SELECT Id, AccountId, MDCR_Communication_Preference__c, First_MDCR_Order_Non_Direct__c, First_MDCR_Order_Non_Direct_Channel__c FROM Opportunity where AccountId IN : medicareAccountIdsForUpdateSet AND IsClosed = false AND (StageName != '61. Quote Approved' AND StageName != '10. Cancelled')])
        {
            Account_Fields_Update_Event__e updatedAccountFieldEvent = accountFieldsUpdateMap.get(oppToBeUpdated.AccountId);
            oppToBeUpdated.MDCR_Communication_Preference__c = updatedAccountFieldEvent.MDCR_Communication_Preference__c;
            oppToBeUpdated.First_MDCR_Order_Non_Direct__c = updatedAccountFieldEvent.First_MDCR_Order_Non_Direct__c;
            oppToBeUpdated.First_MDCR_Order_Non_Direct_Channel__c = updatedAccountFieldEvent.First_MDCR_Order_Non_Direct_Channel__c;
            oppToBeUpdated.G6_Program__c = updatedAccountFieldEvent.G6_Program__c;
            openOppsToBeUpdated.add(oppToBeUpdated);
        }
        
        Database.SaveResult[] savedResult = Database.update(openOppsToBeUpdated, false); 
        for(Database.SaveResult sr : savedResult)
        {
            if(!sr.isSuccess()){
                // Ensure we don't retry the trigger more than 4 times
                if (EventBus.TriggerContext.currentContext().retries < 4) {
                    // Condition isn't met, so try again later.
                    throw new EventBus.RetryableException('Error happened, so retrying the trigger again.');
                }
                else{
                    //Write the error log
                    new ClsApexDebugLog().createLog(
                            new ClsApexDebugLog.Error(
                                'ProcessAccountFieldsUpdateEventTrigger',
                                'None',
                                sr.getId(),
                                sr.getErrors()[0].getFields() + ':' + sr.getErrors()[0].getMessage()
                            )
                        );
                }   
            }
        }   
    }
 
    //Process the updates happened from Opportunity
    if(!medicareOpptyAccountIdsForUpdateSet.isEmpty())
    {
        for(Account accnt : [SELECT Id, MDCR_Communication_Preference__c, First_MDCR_Order_Non_Direct__c, First_MDCR_Order_Non_Direct_Channel__c FROM Account where Id IN : medicareOpptyAccountIdsForUpdateSet])
        {
            Account_Fields_Update_Event__e updatedAccountFieldEvent = accountFieldsUpdateMap.get(accnt.Id);
            accnt.MDCR_Communication_Preference__c = updatedAccountFieldEvent.MDCR_Communication_Preference__c;
            accnt.First_MDCR_Order_Non_Direct__c = updatedAccountFieldEvent.First_MDCR_Order_Non_Direct__c;
            accnt.First_MDCR_Order_Non_Direct_Channel__c = updatedAccountFieldEvent.First_MDCR_Order_Non_Direct_Channel__c;
            //accnt.G6_Program__c = updatedAccountFieldEvent.G6_Program__c;           
            accntsToBeUpdated.add(accnt);
        }
        Database.SaveResult[] savedResult = Database.update(accntsToBeUpdated, false); 
        for(Database.SaveResult sr : savedResult)
        {
            if(!sr.isSuccess()){
                // Ensure we don't retry the trigger more than 4 times
                if (EventBus.TriggerContext.currentContext().retries < 4) {
                    // Condition isn't met, so try again later.
                    throw new EventBus.RetryableException('Error happened, so retrying the trigger again.');
                }
                else{
                    //Write the error log
                    new ClsApexDebugLog().createLog(
                            new ClsApexDebugLog.Error(
                                'ProcessAccountFieldsUpdateEventTrigger',
                                'None',
                                sr.getId(),
                                sr.getErrors()[0].getFields() + ':' + sr.getErrors()[0].getMessage()
                            )
                        );
                }   
            }
        }   
    }
}