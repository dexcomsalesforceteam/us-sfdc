trigger DocuClassDocumentTrigger on DocuClass_Documents__c (after insert, after update, before update) {
	/*Start - Amol 11/15/17 - This code needs to be removed with code cleanup for ProcessMultipleOpportunity batch references.
    if(trigger.isAfter){
        if(trigger.isInsert){
           if(UtilityClass.runOnce()){
               DocuClassDocumentTriggerHandler.afterInsert(trigger.new); 
           }
        }
    }*/
	//End - Amol 11/15/17 - This code needs to be removed with code cleanup for ProcessMultipleOpportunity batch references.
    if(trigger.isBefore){
        if(trigger.isUpdate){
          if(UtilityClass.runOnce()){
            DocuClassDocumentTriggerHandler.beforeUpdate(trigger.newMap, trigger.oldMap);
          }
        }
    }
}