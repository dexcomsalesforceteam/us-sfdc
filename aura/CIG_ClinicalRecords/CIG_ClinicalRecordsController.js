({    
	doInit : function(component, event, helper) {        
        component.set('v.columns', [
            {label: 'Title', fieldName: 'linkName', editable:'false', type: 'url', typeAttributes: {label: { fieldName: 'Note_Title__c' }, target: '_self'}},           
            {label: 'Product', fieldName: 'ProductName', editable:'false', type: 'text', sortable: true},
            {label: 'Trial Start Date', fieldName: 'Trial_Start_Date__c', editable:'true', type: 'date'},
            {label: 'Trial End Date', fieldName: 'Trial_End_Date__c', editable:'true', type: 'date'},           
            {label: '# Participants', fieldName: 'Num_of_Participants__c', editable:'true', type: 'number'},
            {label: 'Results', fieldName: 'Results_posted__c', editable:'true', type: 'boolean'},            
            {label: 'Last Modified', fieldName: 'LastModifiedDate', editable:'false', type: 'date', sortable: true}
            
        ]);        
        helper.getClinicalData(component, helper);
        console.log('***** Clinical Org Id=' + component.get("v.recordId"));       
    },
    
    onSave : function (component, event, helper) {
        helper.saveDataTable(component, event, helper);
    },
    
    //Method gets called by onsort action,
    handleSort : function(component,event,helper){
        //Returns the field which has to be sorted
        var sortBy = event.getParam("fieldName");
        //returns the direction of sorting like asc or desc
        var sortDirection = event.getParam("sortDirection");
        //Set the sortBy and SortDirection attributes
        component.set("v.sortBy",sortBy);
        component.set("v.sortDirection",sortDirection);
        // call sortData helper function
        helper.sortData(component,sortBy,sortDirection);
    },    
    openModel: function(component, event, helper) {
      // for Display Model,set the "isOpen" attribute to "true"
      component.set("v.isOpen", true);
   },
   
   closeModel: function(component, event, helper) {
      // for Hide/Close Model,set the "isOpen" attribute to "Fasle"     
      component.set("v.isOpen", false);
   },
   handleCloseModal : function(component, event, helper) {
       var value = event.getParam("param");
       component.set("v.isOpen", false);
   },
   SubmitFormData : function(component, event, helper) {
       var attribute1 = component.get('v.ModalOrgType');
       var clinicalForm = component.find('CIG_AddClinical');
       clinicalForm.mySubmitOrgData(attribute1);       
   }, 
   handleClinicalHeaderClick: function (component, event, helper) {
   	var evt = $A.get("e.force:navigateToComponent");
    evt.setParams({
        componentDef : "c:CIG_ClinicalRecords",
        componentAttributes: {            
            recordId : component.get("v.recordId")
        }
    });
    evt.fire(); 
   }
   
})