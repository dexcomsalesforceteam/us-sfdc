({
	loadEligibilityDetails : function(component) {
        
        console.log('Helper js called for eligibility matrix');
        
        
        var action = component.get("c.getAccountEligibilityMatrixDetails");
        action.setParams({"accountId": component.get("v.accountId")});
        action.setCallback (this, function(response){
        var state = response.getState();
            if(component.isValid() && state == "SUCCESS")
            {
                
                console.log('data passed to controller from helper in eligibility matrix');
                var accountEligibilityMatrixDetails = response.getReturnValue();
                console.log('accountEligibilityMatrixDetails' + accountEligibilityMatrixDetails);
                
                if(accountEligibilityMatrixDetails != null){
                   
                    component.set("v.currentReceiverGen" , accountEligibilityMatrixDetails["currentReceiverGen"]);
                    //component.set("v.qtyOfReceiverShipped" , accountEligibilityMatrixDetails["QtyOfReceiverShipped"]);
                    component.set("v.receiverQtyRemaining" , accountEligibilityMatrixDetails["receiverQtyRemaining"]);
                    
                    component.set("v.currentSensorGen" , accountEligibilityMatrixDetails["currentSensorGen"]);
                    //component.set("v.qtyOfSensorShipped" , accountEligibilityMatrixDetails["QtyOfSensorShipped"]);
                    component.set("v.sensorQtyRemaining" , accountEligibilityMatrixDetails["sensorQtyRemaining"]);
                    
                    component.set("v.currentTransmitterGen" , accountEligibilityMatrixDetails["currentTransmitterGen"]); 
                    //component.set("v.qtyOfTransmitterShipped" , accountEligibilityMatrixDetails["QtyOfTransmitterShipped"]);
                    component.set("v.transmitterQtyRemaining" , accountEligibilityMatrixDetails["transmitterQtyRemaining"]);
                    
                    component.set("v.receiverInW" , accountEligibilityMatrixDetails["receiverInW"]);
                    component.set("v.transmitterInW" , accountEligibilityMatrixDetails["transmitterInW"]); 
                    component.set("v.transmitterOption", accountEligibilityMatrixDetails["transmitterOption"]);
                    component.set("v.G6ForceOrderAuthCode", accountEligibilityMatrixDetails["G6ForceOrderAuthCode"]);
                    component.set("v.G6ForcePurchaseRTxS", accountEligibilityMatrixDetails["G6ForcePurchaseRTxS"]);
                    component.set("v.G6ForcePurchaseTxS", accountEligibilityMatrixDetails["G6ForcePurchaseTxS"]);
                    component.set("v.receiverOption", accountEligibilityMatrixDetails["receiverOption"]);
                    component.set("v.warrentyLeftOverDaysTransmitter", accountEligibilityMatrixDetails["warrentyLeftOverDaysTransmitter"]);
                    component.set("v.warrentyLeftOverDaysReceiver", accountEligibilityMatrixDetails["warrentyLeftOverDaysReceiver"]);
                    component.set("v.numOfDaysRemainingForNextSensorsOrder" , parseInt(accountEligibilityMatrixDetails["numOfDaysRemainingForNextSensorsOrder"]));
                    
                 }
                
                
            }
            else{
                console.log('Problem getting main topic values, response state: ' + state);
            }
        });
        $A.enqueueAction(action);  
    }
})