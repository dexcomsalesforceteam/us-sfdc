({
    fetchComproductsHelper : function(component, event, helper) {
        component.set('v.mycolumns', [
            {label: 'Comp Product Request Name', fieldName: 'linkName', type: 'url', 
             typeAttributes: {label: { fieldName: 'Name' }, target: '_blank'}},
            
            {label: 'Approval Status', fieldName: 'Approval_Status__c', type: 'text'},
            {label: 'Estimated Amount', fieldName: 'Estimated_Amount', type: 'text'},
            {label: 'Rep Making the Request', fieldName: 'Repmaking_Request_Name', type: 'text'},
            {label: 'Date Comp Requested', fieldName: 'Date_Comp_Requested__c', type: 'date',typeAttributes: {  
                timeZone: 'America/Los_Angeles',
                
                day: '2-digit',  
                month: '2-digit',  
                year: 'numeric',  
                hour: '2-digit',  
                minute: '2-digit',  
                second: '2-digit',  
                hour12: true}}, 
        ]);
            var action = component.get("c.getCompProducts");
            var accId = component.get("v.accountId");
            action.setParams({
            accountId : accId
            });
            action.setCallback(this, function(response){
            var state = response.getState(); 
            var compProductsData = response.getReturnValue();
            if (state === "SUCCESS") {
            for (var i = 0; i < compProductsData.length; i++) {
            var compProduct = compProductsData[i];
                      // alert(compProduct.Name);
                      
      if (compProduct.Repmaking_Request__c){
            compProduct.Repmaking_Request_Name = compProduct.Repmaking_Request__r.Name;
        }
        compProduct.linkName = '/'+compProduct.Id;
        compProduct.Estimated_Amount = '$'+compProduct.Estimated_Amount__c;
        
    }
    
    component.set("v.compProductList", response.getReturnValue());
}
 });
$A.enqueueAction(action);
}
})