({
    loadAccountDetails : function(component) {
        //call apex class method by passing in the orderId parameter value
        var action = component.get("c.getOrderAmountDetails");
        action.setParams({
            "orderId": component.get("v.orderId")
        });
        
        //Retrieve and process the response
        action.setCallback (this, function(response){
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS")
            {
                //retrieve the response value (map) from the method
                var orderAmountDetails = response.getReturnValue();
                // set the attribute values for the component from the values retrieved from the map
                if(orderAmountDetails != null){
                    component.set("v.orderType" , orderAmountDetails["orderType"]);
                    component.set("v.orderAmount" , orderAmountDetails["orderAmount"]);
                    component.set("v.taxAmount" , orderAmountDetails["taxAmount"]);
                    component.set("v.shippingCharges" , orderAmountDetails["shippingCharges"]);
                    //component.set("v.copayAmount" , orderAmountDetails["copayAmount"]);
                    component.set("v.copayAmount" , Number(orderAmountDetails["copayAmount"]).toFixed(2));
                    component.set("v.totalAmount" , orderAmountDetails["totalAmount"]);
                    component.set("v.isCashOrder" , orderAmountDetails["isCashOrder"]);
                    component.set("v.priceBookName" , orderAmountDetails["priceBookName"]);
                    component.set("v.errorMessage" , orderAmountDetails["errorMessage"]);
                }
            }
            else {
                console.log('Problem getting main topic values, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    }
})