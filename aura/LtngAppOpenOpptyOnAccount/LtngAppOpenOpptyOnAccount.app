<!--
@Description - This app will be called by OpenOpportunitiesOnAccountPage
This app hosts LtngCompOpenOpptyOnAccount component
@Created Date - 30 Aug 2019
@Created By - Tanay Kulkarni LTI
-->
<aura:application access="GLOBAL" extends="ltng:outApp" >
	<aura:dependency resource="c:LtngCompOpenOpptyOnAccount"/>
</aura:application>