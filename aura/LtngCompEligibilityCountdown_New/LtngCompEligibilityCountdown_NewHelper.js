({
    loadAccountDetails : function(component) {
        //call apex class method by passing in the accountid parameter value
        var action = component.get("c.getAccountEligibilityCountDownDetails");
        action.setParams({"accountId": component.get("v.accountId")});
        
        //Retrieve and process the response
        action.setCallback (this, function(response){
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS")
            {
                //retrieve the response value (map) from the method
                var accountEligibilityCountDownDetails = response.getReturnValue();
                console.log('accountEligibilityCountDownDetails' + accountEligibilityCountDownDetails);
                // set the attribute values for the component from the values retrieved from the map
                if(accountEligibilityCountDownDetails != null){
                    component.set("v.isPriorAuthRequired" , accountEligibilityCountDownDetails["IsPriorAuthRequired"]);
                    
                    component.set("v.cmnExpirationDate" , "CMN Expiration Date : " + accountEligibilityCountDownDetails["CMNExpirationDate"]);
                    component.set("v.benefitCalendarYearEndDate" , "Calendar Year End Date : " + accountEligibilityCountDownDetails["BenefitCalendarYearEndDate"]);
                    
                    component.set("v.latestReceiverShipDate" , accountEligibilityCountDownDetails["LatestReceiverShipDate"]);
					component.set("v.qtyOfReceiverShipped" , accountEligibilityCountDownDetails["QtyOfReceiverShipped"]);
                    component.set("v.nextPossibleReceiverShipDate" , accountEligibilityCountDownDetails["NextPossibleReceiverShipDate"]);
                    component.set("v.numOfDaysRemainingForNextReceiverOrder" , accountEligibilityCountDownDetails["NumOfDaysRemainingForNextReceiverOrder"]);
                    
                    component.set("v.latestSensorsShipDate" , accountEligibilityCountDownDetails["LatestSensorsShipDate"]);
                    component.set("v.qtyOfSensorsShipped" , accountEligibilityCountDownDetails["QtyOfSensorsShipped"]);
                    component.set("v.nextPossibleSensorsShipDate" , accountEligibilityCountDownDetails["NextPossibleSensorsShipDate"]);
                    component.set("v.numOfDaysRemainingForNextSensorsOrder" , accountEligibilityCountDownDetails["NumOfDaysRemainingForNextSensorsOrder"]);
                    
                    component.set("v.latestTransmitterShipDate" , accountEligibilityCountDownDetails["LatestTransmitterShipDate"]);
                    component.set("v.qtyOfTransmitterShipped" , accountEligibilityCountDownDetails["QtyOfTransmitterShipped"]);
                    component.set("v.nextPossibleTransmitterShipDate" , accountEligibilityCountDownDetails["NextPossibleTransmitterShipDate"]);
                    component.set("v.numOfDaysRemainingForNextTransmitterOrder" , accountEligibilityCountDownDetails["NumOfDaysRemainingForTransmitterOrder"]);
                    
                    component.set("v.LatestGlucometerShipDate" , accountEligibilityCountDownDetails["LatestGlucometerShipDate"]);
                    component.set("v.qtyofGlucometerShipped" , accountEligibilityCountDownDetails["qtyofGlucometerShipped"]);
                    component.set("v.nextPossibleGlucometerShipDate" , accountEligibilityCountDownDetails["nextPossibleGlucometerShipDate"]);
                    component.set("v.numOfDaysRemainingForNextGlucometerOrder" , accountEligibilityCountDownDetails["numOfDaysRemainingForNextGlucometerOrder"]);
                
                	component.set("v.latestSolutionShipDate" , accountEligibilityCountDownDetails["latestSolutionShipDate"]);
                    component.set("v.qtyofSolutionShipped" , accountEligibilityCountDownDetails["qtyofSolutionShipped"]);
                    component.set("v.nextPossibleSolutionShipDate" , accountEligibilityCountDownDetails["nextPossibleSolutionShipDate"]);
                    component.set("v.numOfDaysRemainingForNextSolutionOrder" , accountEligibilityCountDownDetails["numOfDaysRemainingForNextSolutionOrder"]);
                
                    component.set("v.currentReceiverGen" , accountEligibilityCountDownDetails["currentReceiverGen"]);
                    component.set("v.currentTransmitterGen" , accountEligibilityCountDownDetails["currentTransmitterGen"]);
                    
                }
                
            }
            else {
                console.log('Problem getting main topic values, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    }
})