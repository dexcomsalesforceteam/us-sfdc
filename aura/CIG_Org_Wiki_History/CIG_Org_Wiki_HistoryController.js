({
	doInit : function(component, event, helper) {        
        component.set('v.columns', [
            {label: 'Title', fieldName: 'linkName', editable:'false', type: 'url', typeAttributes: {label: { fieldName: 'Name' }, target: '_self'}},           
            {label: 'Field Changed', fieldName: 'Field_Changed__c', editable:'false', type: 'text', sortable: true},
            {label: 'Last Modified By', fieldName: 'LastModifiedById', editable:'false', type: 'text'},                       
            {label: 'Last Modified Date', fieldName: 'LastModifiedDate', editable:'false', type: 'date-local', sortable: true, typeAttributes:{year:'numeric',month:'short',day:'2-digit'}}            
        ]);        
        helper.getOrgWikiHistData(component, helper);
        console.log('***** Org Id=' + component.get("v.recordId"));
    },
    
    //Method gets called by onsort action,
    handleSort : function(component,event,helper){
        //Returns the field which has to be sorted
        var sortBy = event.getParam("fieldName");
        //returns the direction of sorting like asc or desc
        var sortDirection = event.getParam("sortDirection");
        //Set the sortBy and SortDirection attributes
        component.set("v.sortBy",sortBy);
        component.set("v.sortDirection",sortDirection);
        // call sortData helper function
        helper.sortData(component,sortBy,sortDirection);
    },
    
})