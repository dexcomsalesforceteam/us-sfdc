({
	validate : function(component, event, helper) {
		//validation logic
		//
		//
       	
        console.log('validate called');
        component.set('v.errors', {
            rows: {
                1: {
                    title: 'We found 2 errors.',
                    messages: [
                        'Enter a valid amount.',
                        'Verify the email address and try again.'
                    ],
                    fieldNames: ['amount', 'contact']
                }
            }
            ,
            table: {
                title: 'Total of order amounts should match total order value.',
                messages: [
                    'Row 2 amount must be number',
                    'Row 2 email is invalid'
                ]
            }
         });
    }
})