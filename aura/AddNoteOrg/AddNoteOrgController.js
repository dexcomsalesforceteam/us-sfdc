({
    doInit: function(component, event, helper) {
        var recId = component.get("v.recordId");
        var sObjectType = component.get("v.sobjecttype");
        
        var userId = $A.get("$SObjectType.CurrentUser.Id");
        console.log('**** Current user id:' + userId);
        component.set("v.currUserId", userId);
        
        console.log('**** Starting **** RecordId=' + component.get("v.recordId") + '  ** sObjectType=' + sObjectType);
        
        if(recId && sObjectType === 'Organization__c') {
            var action = component.get("c.getOrg");
            action.setParams({ "orgId" : component.get("v.recordId") });
            
            action.setCallback(this, function(response){
        	var state = response.getState();
            if (state === "SUCCESS") {
            	console.log('response-->'+ response.getReturnValue() + '');
                component.set("v.org", response.getReturnValue()); 
                console.log("Org Name: " + component.get("v.org.Id") + " : " + component.get("v.org.Name"));
                console.log("Org Type: " + component.get("v.org.Organization_Type__c"));
            } else {
                    console.log("Failed with state: " + state);
            }
       		});
       $A.enqueueAction(action);
            
            
       }
   },
    
    handleSubmit : function(component, event, helper) {
        var selUsers = component.find("selUsers");
        console.log('**** selUsers=' + selUsers.get("v.value"));
        
    	console.log('**** Starting save prod=' + component.get("v.product"));
        var feedback = component.find("feedbackBody").get("v.value");
        console.log('**** prod note=' + feedback);
        var files1=component.get('v.filesDB');
        var filesDB1=[];
        if(files1 != null){
        	files1.forEach(function(file){
            	filesDB1.push(file.documentId);
        	});
        console.log('Files for DB: '+ filesDB1);    
        }
        
        var noteTitle = component.find("noteTitle");
        if(noteTitle.get("v.validity").valid) {
            // continue processing
        } else {
            noteTitle.showHelpMessageIfInvalid();
        }
        console.log('***** title=' + noteTitle);
        
        if(feedback!='' && noteTitle.get("v.validity").valid){
        	var action = component.get("c.addProductNote");
            action.setParams({ "org" : component.get("v.org"),
                              "productNote" : feedback,
                              "files" : filesDB1,
                              "noteTitle" : noteTitle.get("v.value"),
                              "usersToFollow" : selUsers.get("v.value")
                             });    
          	action.setCallback(this, function(response){
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        console.log('response-->'+ response.getReturnValue() + '');
                        
                        var resultsToast = $A.get("e.force:showToast");
                        resultsToast.setParams({
                            "title": "Success!",
                            "message": "Product note submitted successfully."
                        });
                        $A.get("e.force:closeQuickAction").fire();
                        resultsToast.fire();
                        $A.get("e.force:refreshView").fire();                        
                    }
                	else if (state === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " + 
                                            errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
            	} 
                
        	});
            $A.enqueueAction(action);
        }
        console.log('**** End save ****');
    },
    
    handleUploadFinished : function(component, event, helper) {
        var uploadedFiles = event.getParam("files");
        var documentId = uploadedFiles[0].documentId;
        var fileName = uploadedFiles[0].name;
        
        var files=event.getParam('files');
        console.log('Files uploaded: '+JSON.stringify(files));
        var fileIds=[];
        files.forEach(function(file){
            fileIds.push(file.documentId);
        });
        component.set('v.numFiles', fileIds.length);
		component.set('v.fileIds', fileIds);
        component.set('v.filesDB', files);
        
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": "File "+fileName+" Uploaded successfully."
        });
        toastEvent.fire();
    }
    
})