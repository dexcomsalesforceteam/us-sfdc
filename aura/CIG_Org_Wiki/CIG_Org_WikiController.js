({
	handleSuccess : function(component, event, helper) {
                        component.find('notifLib').showToast({
                            "variant": "success",
                            "title": component.get("v.OrgWikiFieldLabel") + " Updated",
                            "message": "Record ID: " + event.getParam("id")
                        });
    }
})