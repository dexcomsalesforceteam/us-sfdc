({
	doInit: function(component, event, helper) {
        var recId = component.get("v.recordId");
        var orgData = component.get("v.data");
        var sObjectType = component.get("v.sobjecttype");
        
        var userId = $A.get("$SObjectType.CurrentUser.Id");
        //console.log('**** Current user id:' + userId);
        component.set("v.currUserId", userId);        
        component.find("od_organization").set("v.value", recId);
        orgData.Organization__c = recId;
        orgData.RecordTypeId=component.get("v.recordTypeId");
        console.log('**** Starting **** Add Clinica RecordId=' + recId + '  ** orgData=' + JSON.stringify(orgData));
   },
    
   handleUploadFinished : function(component, event, helper) {
        var uploadedFiles = event.getParam("files");
        var documentId = uploadedFiles[0].documentId;
        var fileName = uploadedFiles[0].name;
        
        var files=event.getParam('files');
        console.log('Files uploaded: '+JSON.stringify(files));
        var fileIds=[];
        files.forEach(function(file){
            fileIds.push(file.documentId);
        });
        component.set('v.numFiles', fileIds.length);
		component.set('v.fileIds', fileIds);
        component.set('v.filesDB', files);
        
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": "File "+fileName+" Uploaded successfully."
        });
        toastEvent.fire();
    },
    SubmitOrgData : function (component, event, helper) {
        var params = event.getParam('arguments');
        console.log('Param 1: '+ params.OrgType);
        $A.enqueueAction(component.get('c.handleSubmit'));
    },
    handleSubmit : function(component, event, helper) {
		var noteTitle = component.find("od_Note_Title");
        if(!noteTitle.get("v.validity").valid) {            
        	noteTitle.showHelpMessageIfInvalid();            
            return;
        } 
        
        var orgData = component.get("v.data");
        orgData.RecordTypeId=component.get("v.recordTypeId");
        orgData.Organization__c=component.find("od_organization").get("v.value");
        orgData.Product_Claim__c=component.find("od_Product_Claim").get("v.value");
        
        orgData.Product__c=component.find("od_Product").get("v.value");
        orgData.Collateral_Source__c=component.find("od_Collateral_Source").get("v.value");
        
        orgData.Collateral_Target__c=component.find("od_Collateral_Target").get("v.value");
        
        orgData.Collateral_URL__c=component.find("od_Collateral_URL").get("v.value");
        orgData.Collateral_Target=component.find("od_Collateral_Target").get("v.value");
        orgData.Date_first_observed__c=component.find("od_Date_first_observed").get("v.value");
        
        
        orgData.Note_Title__c=component.find("od_Note_Title").get("v.value");
        orgData.Product_Note__c=component.find("od_Product_Note").get("v.value");
        
        console.log('***** orgData=' + JSON.stringify(orgData));
        
        var selUsers = component.find("selUsers");
        console.log('**** selUsers=' + selUsers.get("v.value"));
        
        var files1=component.get('v.filesDB');
        var filesDB1=[];
        if(files1 != null){
        	files1.forEach(function(file){
            	filesDB1.push(file.documentId);
        	});
        	console.log('Files for DB: '+ filesDB1);
            orgData.Num_Files_Attached__c=filesDB1.length;
            if(filesDB1.length>0){
            	orgData.Files_Attached__c=true;    
            }
        } 
        
        var action = component.get("c.addODRecord");
        action.setParams({ "data" : orgData,                              
                              "files" : filesDB1,                              
                              "usersToFollow" : selUsers.get("v.value")
                             });    
         action.setCallback(this, function(response){
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        console.log('response-->'+ JSON.stringify(response.getReturnValue()) + '');
                        
                        var resultsToast = $A.get("e.force:showToast");
                        resultsToast.setParams({
                            "title": "Success!",
                            "message": "Product note submitted successfully."
                        });
                        //$A.get("e.force:closeQuickAction").fire();
                        //resultsToast.fire();
                        //$A.get("e.force:refreshView").fire();
                        
                        var myEvent = component.getEvent("myRefreshTable");
        				myEvent.setParams({"param": "Marketing table!"});
        				myEvent.fire();
                        
                        var myEvent = component.getEvent("CloseModal");
                        myEvent.setParams({"param": "Clinical"});
                        myEvent.fire();
                    }
                	else if (state === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " + 
                                            errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
            	} 
                
        	});
            $A.enqueueAction(action);
        
        console.log('**** End save ****');
    }
})