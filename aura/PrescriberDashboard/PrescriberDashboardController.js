({
    // adapts view depending on device,
    // gets data related to running user and populates component metrics and datatable
    init : function(component, event, helper){
        if($A.get("$Browser.formFactor") != 'DESKTOP'){
            component.set("v.displayTiles", false);
        }
        component.set("v.isLoading", true);
        $A.util.addClass(component.find('all-orders'), 'current-filter');
        helper.startSessionTimer(component, helper);
        helper.getUserData(component, helper, 'past-3-months');
        component.find('dateRangeCombobox').set("v.value", 'past-3-months');
    },
    
    // triggers search based on prescriber or table row name
	handleSearch : function(component, event, helper) {
	    /*helper.resetSession(component, helper);
        component.set("v.isSearching", true);
        component.set("v.noResultsFound", false);
        helper.handleSearch(component, event, helper);*/
        helper.resetSession(component, helper);
        helper.handleSearch(component, helper, false);
	},
    
    // displays corresponding subset of data when user clicks on dropdown filter menu or metric tiles
    handleFilter : function(component, event, helper){
        /*helper.resetSession(component, helper);
        var filterName;
        helper.removeFilterBackgrounds(component);
        if(event.currentTarget){
            // if click happened on tile
            filterName = event.currentTarget.id;
        }
        else{
            // if click happened on dropdown
            filterName = event.getParam("value");
        }
        component.find('filterCombobox').set("v.value", filterName);
        $A.util.addClass(component.find(filterName), 'current-filter');
        if(filterName == 'all-orders'){
            component.set("v.tableData", component.get("v.allData"));
        }
        else{
            component.set("v.tableData", helper.handleFilter(component, event, helper));
        }
        */
       helper.resetSession(component, helper);
        //component.set("v.isLoadingMetric", true);
        
        component.set("v.tableData", helper.handleFilter(component, event, helper));
    },
    
    // row sorting in every datatable
    sortRows : function(component, event, helper){
        helper.resetSession(component, helper);
        helper.sortRows(component, event, helper);
    },
    
    // displays and hides modal with definition of metrics
    // displays and hides modal with definition of metrics
    handleInfoModal : function(component, event, helper){
        helper.resetSession(component, helper);
        var action = event.getSource().getLocalId();
        if(action == "openMetricsModal"){
            component.set("v.displayMetricsModal", true);
        }
        else if(action == "closeMetricsModal"){
            component.set("v.displayMetricsModal", false);
        }
        else if(action == "openStatusesModal"){
            component.set("v.displayStatusesModal", true);
        }
        else if(action == "closeStatusesModal"){
            component.set("v.displayStatusesModal", false);
        }
        
    },

    logOutUser : function(component, event, helper){
        helper.logOutUser();
    },

    closeLogOutModal : function(component, event, helper){
        helper.resetSession(component, helper);
    },
    
    handleDateChange : function(component, event, helper){
        component.set("v.isLoading", true);
        // metrics should get reset
        helper.removeFilterBackgrounds(component);
        component.find('filterCombobox').set("v.value", 'all-orders');
        $A.util.addClass(component.find('all-orders'), 'current-filter');
        // searchbox is reset
        component.set("v.noResultsFound", false);
        component.find('searchbar').set("v.value", '');
        // retrieve data in the date range
        var dateRange = component.find('dateRangeCombobox').get("v.value");
        helper.getUserData(component, helper, dateRange);
    }
   
})