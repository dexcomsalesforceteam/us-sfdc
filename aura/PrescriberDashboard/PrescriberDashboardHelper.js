({
    sessionManager : {
        sessionMinutes : 600000,
        warningMinutes : 30000
    },

    startSessionTimer : function(component, helper){
        helper.sessionManager.sessionTimer = window.setTimeout(
            $A.getCallback(
                function(){
                   helper.startLogoutTimer(component, helper);
                }
            ),
            helper.sessionManager.sessionMinutes
        );
    },

    startLogoutTimer : function(component, helper){
        component.set("v.displayLogoutModal", true);
        helper.startLogoutCountdown(component, helper);
        helper.sessionManager.logoutTimer = window.setTimeout(
                $A.getCallback(
                    function(){
                        helper.logOutUser();
                    }
                ),
                helper.sessionManager.warningMinutes
            );
    },

    startLogoutCountdown : function(component, helper){
        var timePast = 0;
        helper.sessionManager.countdown = setInterval(function(){
            var timeLeft = (helper.sessionManager.warningMinutes - timePast)/1000;
            component.set("v.secondsLeft", timeLeft);
            timePast += 1000;
        },
        1000);
    },

    resetSession : function(component, helper){
                component.set("v.displayLogoutModal", false);
                component.set("v.secondsLeft", 30);
                clearTimeout(helper.sessionManager.sessionTimer);
                clearTimeout(helper.sessionManager.logoutTimer);
                clearInterval(helper.sessionManager.countdown);
                helper.registerActivityAtServerLevel(component);
                helper.startSessionTimer(component, helper);
            },

    logOutUser : function(){
            window.location.replace(window.location.href + "login/");
    },

    registerActivityAtServerLevel : function(component){
        return ltngUtils.callApexPromise(component.get("c.resetSessionTimer"), {

        }).then(
            function(result){
            }
        );
    },


    // makes call to server to obtain user information and related opportunities and order headers
	getUserData : function(component, helper, dateRangeString) {
		// get the name, party Id  and account Id from the contact associated to the running user
        return ltngUtils.callApexPromise(component.get("c.getUserInfo"), {
            // no parameters needed
        }).then(
            function(prescriber){
                component.set("v.prescriberName", prescriber.Name);
                component.set("v.prescriberNumber", prescriber.PersonAccountId__r.Party_ID__c);
                component.set("v.prescriberAccountId", prescriber.PersonAccountId__c); // this is the related to the Person Account
                return ltngUtils.callApexPromise(component.get("c.getOpportunitiesAndOrders"),{
                    dateRange : dateRangeString
                });
            }
        ).then(
            function(result){
                var formattedOpps = helper.formatData(result.orderHeaderMap, result.opportunities, result.orders, helper);
                helper.populateMetrics(component, formattedOpps);
                component.set("v.allData", formattedOpps);
                component.set("v.tableData", formattedOpps);
                component.set("v.isLoading", false);
                
            }
        ).catch(
            function(error){
                component.set("v.isLoading", false);
                console.error(error);
            }
        );
	},
    
    // uniforms opportunity and order header records into wrapper objects to fit the datatable fields and acceptance criteria
    formatData : function(orderHeaderMap, opportunities, orders, helper){
        var distributorStatuses = ['2.1B Distributor - BI Pending', '2.1F Distributor - Unable to Verify', '2.2B Distributor - Verified', '2.3B Dist Verified - Pending Customer OOP Discussion',
                                  '3.1 Pending Distributor Referral Confirmation', '3.2 Distributor Received, Pending Initial Customer Call', '3.3 Distributor Received, Pending Final Customer Call',
                                  '4.5 Distributor-Collecting Docs', '4.9A Dist - Difficult to Collect Docs', '5.2 Distributor-Auth Submitted to Payor', '6.4 Distributor Quote Pending'];
        var newOrderOppTypes = ['NEW SYSTEM', 'Physician Referral', 'Tandem Transmitter', 'Vibe Transmitter'];
        var formattedOpps = [];
        
        // it's gonna get a little ugly here but it's worth it trust me
        opportunities.forEach(function(opp){
            var wrapperOpp = {
                Id: opp.Id,
                FirstName: opp.Patient_First_Name__c,
                LastName : opp.Patient_Last_Name__c,
                Name : opp.Patient_First_Name__c + ' ' + opp.Patient_Last_Name__c,
                DateOfBirth : opp.Account.DOB__c,
                Payor : (opp.Payor__r != null) ? opp.Payor__r.Name : '',
                Status : helper.translateStageName(opp),
                CollectedDocuments : helper.getCollectedDocuments(opp),
                AwaitingDocuments : helper.getAwaitingDocuments(helper.getCollectedDocuments(opp), opp),
                DateSubmitted : opp.CreatedDate.includes('T') ? opp.CreatedDate.split('T')[0] : opp.CreatedDate,
                IsActivePatient : (opp.Sales_Channel__c == 'Dexcom' && opp.StageName != '10. Cancelled' && opp.StageName != '61. Quote Approved') ? true : false,
                IsPendingDocuments : ((opp.Sales_Channel__c == 'Dexcom' || opp.Sales_Channel__c == 'Distributor') && opp.StageName == '4. Doc Collection') ? true : false,
                IsOver15Days : ((opp.StageName != '10. Cancelled' && opp.StageName != '61. Quote Approved') && (Date.now() - new Date(opp.CreatedDate))/(3600000 * 24) > 15) ? true : false,
                IsPatientWithDistributor : (opp.Sales_Channel__c == 'Distributor' && (distributorStatuses.includes(opp.Status__c))) || (opp.DME_Distributor__c) ? true : false,
                IsPatientWithPharmacy : (opp.Sales_Channel__c == 'Pharmacy' && opp.StageName == '13. Pharma Retail') ? true : false,
                IsCancelled : (opp.StageName == '10. Cancelled') ? true : false,
                IsNewOrder : (opp.StageName == '61. Quote Approved' && (newOrderOppTypes.includes(opp.Type)) && ((Date.now() - new Date(opp.CreatedDate))/(3600000 * 24) < 30)) ? true : false,
                IsTotalOrder : (opp.StageName == '61. Quote Approved' && ((Date.now() - new Date(opp.CreatedDate))/(3600000 * 24) < 365)) ? true : false
            };
            if(opp.Order_NUM__c){
                if(orderHeaderMap[opp.Order_NUM__c]){
                    wrapperOpp.Status += '\n on ' + orderHeaderMap[opp.Order_NUM__c];
                }
            }
            formattedOpps.push(wrapperOpp);
        });
        
        orders.forEach(function(o){
            var wrapper = {
                Id: o.Id,
                FirstName: o.Account__r.FirstName,
                LastName : o.Account__r.LastName,
                Name : o.Account__r.FirstName + ' ' + o.Account__r.LastName,
                DateOfBirth : o.Account__r.DOB__c,
                Payor : (o.Account__r.Payor__r != null) ? o.Account__r.Payor__r.Name : '',
                Status : o.Status__c,
                CollectedDocuments : '',
                AwaitingDocuments : '', 
                DateSubmitted : o.Booked_Date__c,
                IsActivePatient : false,
                IsPendingDocuments : false,
                IsOver15Days : false,
                IsPatientWithDistributor : false,
                IsPatientWithPharmacy : false,
                IsCancelled : false,
                IsNewOrder : ((Date.now() - new Date(o.Booked_Date__c))/(3600000 * 24) < 30) ? true : false,
                IsTotalOrder : ((Date.now() - new Date(o.Booked_Date__c))/(3600000 * 24) < 365) ? true : false
            };
            
            if(o.Order_ID__c){
                if(orderHeaderMap[o.Order_ID__c]){
                    wrapper.Status += '\n on ' + orderHeaderMap[o.Order_ID__c];
                }
            }
            formattedOpps.push(wrapper);
        });
        formattedOpps = helper.presort(formattedOpps, helper);
        return(formattedOpps);
    },
    
    // translates wording of Status field on Opportunity to meet acceptance criteria
    translateStageName: function(opp){
        var stageName = opp.StageName;
        var status;
        switch(stageName){
            case '1. New Opportunity':
                status = 'Pending Patient Contact';
                break;
            case '2. Verification':
                status = 'Checking Patients Coverage';
                break;
            case '3. Referred to Channel Partner':
                status = 'Sent to Distributor';
                
                break;
            case '4. Doc Collection':
                status = 'Pending Documentation';
                break;
            case '5. Prior-Authorization':
                status = 'Pending Prior Authorization';
                break;
            case '6. Quote Pending':
                status = 'Pending Order Confirmation';
                break;
            case '61. Quote Approved':
                status = 'Shipped';
                break;
            case '10. Cancelled':
                status = 'Cancelled';
                if(opp.Close_Reason__c){
                    status += ': \n' + opp.Close_Reason__c;
                }
                break;
            case '13. Pharma Retail':
                status = 'At a retail pharmacy';
                break;
        }
        if(opp.DME_Distributor__c){
            status += ': \n' + opp.DME_Distributor__r.Name + ' \n' + opp.DME_Distributor__r.Phone;
        }
        return(status);
    },
    
    // formats the value displayed in the Collected Documents column for each opportunity
    getCollectedDocuments: function(opp){

        var collectedDocuments;
        if(opp.StageName == '10. Cancelled' || opp.StageName == '61. Quote Approved'){
            collectedDocuments = '';
        }
        else if(!opp.HasAllDocs__c){
            var isDoc1Collected = this.isDocCollected(opp.Doc_1_Status__c);
            var isDoc2Collected = this.isDocCollected(opp.Doc_2_Status__c);
            if(isDoc1Collected == true && isDoc2Collected == true){
                collectedDocuments = 'AOB, CMN';
            }
            else if(isDoc1Collected == true){
                collectedDocuments = 'AOB';
            }
            else if(isDoc2Collected == true){
                collectedDocuments = 'CMN';
            }
            else{
                collectedDocuments = '';
            }
        }
        else{
            collectedDocuments = 'AOB, CMN';
        }
        return(collectedDocuments);
    },
    
    // based on the value of the Collected Documents column, formats the value to be displayed on the Awaiting Documents column
    getAwaitingDocuments: function(collectedDocs, opp){
        var awaitingDocuments;
        if(opp.StageName == '10. Cancelled' || opp.StageName == '61. Quote Approved'){
            awaitingDocuments = '';
        }
        else{
            switch(collectedDocs){
                case 'AOB, CMN':
                    awaitingDocuments = '';
                    break;
                case 'AOB':
                    awaitingDocuments = 'CMN';
                    break;
                case 'CMN':
                    awaitingDocuments = 'AOB';
                    break;
                case '':
                    awaitingDocuments = 'AOB, CMN';
                    break;
            }
        }
        return(awaitingDocuments);
    },
    
    // determines whether a document can be marked as collected depending on its status
    isDocCollected: function(docStatus){
        var isCollected;
        if(docStatus == 'New' || docStatus == 'Requested' || docStatus == 'Verbal to use Dist.' || docStatus == 'Rejected - AS' || docStatus == 'Rejected - QC'){
            isCollected = false;
        }else if(docStatus == 'Received' || docStatus == 'Submitted' || docStatus == 'Ready to Submit' || docStatus == 'Verified'){
            isCollected = true;
        }
        return(isCollected);
    },

    // presort orders and opportunities
    presort: function(data, helper){
        var sortedData = [];
        var namesOnly = [];
        // extract a single instance of each last + first name into an array
        data.forEach(function(row){
            if(!namesOnly.includes(row.LastName + ' ' + row.FirstName)){
                namesOnly.push(row.LastName + ' ' + row.FirstName);
            }
        });
        // primary sort by patient name
        namesOnly.sort();
        namesOnly.forEach(function(name){
            var toSort = [];
            data.forEach(function(row){
                if(row.LastName + ' ' + row.FirstName == name){
                    toSort.push(row);
                }
            });
            // sort each group of orders of the same patient by date
            helper.sortByDate(toSort).forEach(function(sortedRow){
                sortedData.push(sortedRow);
            });
            //sortedData.push(helper.sortByDate(toSort));
        });
        // iterate through entire list of data and make sub-arrays of the entire rows that match one of the names
        return sortedData;
    },

    sortByDate: function(data){
        // extract DateSubmitted from each row into an array
        var datesOnly = [];
        var datesReversed = [];
        var sortedData = [];
        data.forEach(function(row){
            datesOnly.push(row.DateSubmitted);
        });

        // sort dates, then reverse so the newest date is first
        datesOnly.sort();
        for(var i = datesOnly.length - 1; i >= 0; i--){
            datesReversed.push(datesOnly[i]);
        }

        // match sorted dates to their respective row
        datesReversed.forEach(function(date){
            var index;
            data.forEach(function(row){
                if(row.DateSubmitted == date){
                    index = data.indexOf(row);
                }
            });
            sortedData.push(data[index]);
            data.splice(index, 1);
        });
        return sortedData;
    },
    
    // takes formatted data and calculates metrics based on the properties of each wrapper object
    populateMetrics : function(component, opportunities){
        var activePatientsCounter = 0;
        var pendingDocumentsCounter = 0;
        var over15DaysCounter = 0;
        var patientsWithDistributorCounter = 0;
        var patientsWithPharmacyCounter = 0;
        var cancelledCounter = 0;
        var newOrdersCounter = 0;
        var totalOrdersCounter = 0;
        opportunities.forEach(function(opp){
            if(opp.IsActivePatient){
                activePatientsCounter++;
            }
            if(opp.IsPendingDocuments){
                pendingDocumentsCounter++;
            }
            if(opp.IsOver15Days){
                over15DaysCounter++;
            }
            if(opp.IsPatientWithDistributor){
                patientsWithDistributorCounter++;
            }
            if(opp.IsPatientWithPharmacy){
                patientsWithPharmacyCounter++;
            }
            if(opp.IsCancelled){
                cancelledCounter++;
            }
            if(opp.IsNewOrder){
                newOrdersCounter++;
            }
            if(opp.IsTotalOrder){
                totalOrdersCounter++;
            }
        });
        
        component.set("v.activePatientsMetric", activePatientsCounter);
        component.set("v.pendingDocumentsMetric", pendingDocumentsCounter);
        component.set("v.over15DaysMetric", over15DaysCounter);
        component.set("v.patientsWithDistributorMetric", patientsWithDistributorCounter);
        component.set("v.patientsWithPharmacyMetric", patientsWithPharmacyCounter);
        component.set("v.cancelledMetric", cancelledCounter);
        component.set("v.newOrdersMetric", newOrdersCounter);
        component.set("v.totalOrdersMetric", totalOrdersCounter);
        component.set("v.allOrdersMetric", opportunities.length);
    },
    
    // handles client side action to filter displayed data by prescriber or table row name
    handleSearch : function(component, helper, returnResults){
        // save and remove any filters before searching
        //helper.removeFilterBackgrounds(component);
        if(!returnResults){
            helper.removeFilterBackgrounds(component);
            setTimeout($A.getCallback(
                function(){
                    $A.util.addClass(component.find('all-orders'), 'current-filter');
                    component.find('filterCombobox').set("v.value", 'all-orders');
                }
            ));
        }

        component.set("v.isSearching", true);
        
        var searchString = component.find('searchbar').get("v.value")
        
        if(searchString == ''){ // if user deleted search value just reset all data
            component.set("v.tableData", component.get("v.allData"));
            component.set("v.noResultsFound", false);
        }
        else{
            var searchResultData = [];
            var allData = component.get("v.allData");
            allData.forEach(function(row){
                if(row.Name.toLowerCase().includes(searchString.toLowerCase())){
                    searchResultData.push(row);
                }
            });
            if(searchResultData.length == 0){
                component.set("v.noResultsFound", true);
                //component.set("v.tableData", []);
            }
            if(returnResults){
                component.set("v.isSearching", false);
                return searchResultData;
            }
            else{
                component.set("v.tableData", searchResultData);
            }
        }
        component.set("v.isSearching", false);
    },

    // handles client side action to reset data to the appropriate subset based on the filter searched
    handleFilter: function(component, event, helper){
        var filter;
        helper.removeFilterBackgrounds(component);
        if(event.currentTarget){
            // if click happened on tile
            filter = event.currentTarget.id;
        }
        else{
            // if click happened on dropdown
            filter = event.getParam("value");
        }
        component.find('filterCombobox').set("v.value", filter);
        
        $A.util.addClass(component.find(filter), 'current-filter');
        var data;
        var filteredData = [];
        if(component.find('searchbar').get("v.value") && component.find('searchbar').get("v.value") != ''){
            if(component.get("v.noResultsFound")){
                component.set("v.tableData", filteredData);
                return;
            }
            data = helper.handleSearch(component, helper, true);

        }
        else{
            data = component.get("v.allData");
        }
        if(filter == 'all-orders'){
            component.find('searchbar').set("v.value", '');
            filteredData = component.get("v.allData");
            component.set("v.isLoadingMetric", false);
        }
        else if(filter == 'active-patients'){
            data.forEach(function(row){
                if(row.IsActivePatient){
                    filteredData.push(row);
                }
            });
           
        }
        else if(filter == 'pending-documents'){
            data.forEach(function(row){
                if(row.IsPendingDocuments){
                    filteredData.push(row);
                }
            });
        }
        else if(filter == 'over-15-days'){
            data.forEach(function(row){
                if(row.IsOver15Days){
                    filteredData.push(row);
                }
            });
        }
        else if(filter == 'patients-with-distributor'){
            data.forEach(function(row){
                if(row.IsPatientWithDistributor){
                    filteredData.push(row);
                }
            });
        }
        else if(filter == 'patients-with-pharmacy'){
            data.forEach(function(row){
                if(row.IsPatientWithPharmacy){
                    filteredData.push(row);
                }
            });
        }
        else if(filter == 'cancelled'){
            data.forEach(function(row){
                if(row.IsCancelled){
                    filteredData.push(row);
                }
            });
        }
        else if(filter == 'new-orders'){
            data.forEach(function(row){
                if(row.IsNewOrder){
                    filteredData.push(row);
                }
            });
        }
        else if(filter == 'total-orders'){
            data.forEach(function(row){
                if(row.IsTotalOrder){
                    filteredData.push(row);
                }
            });
        }
        return(filteredData);
    },
    
    /*
    // handles client side action to reset data to the appropriate subset based on the filter selected
    handleFilter : function(component, filter){
        var allData = component.get("v.allData");
        var filteredData = [];
        if(filter == 'all-orders'){
            allData.forEach(function(row){
                filteredData.push(row);
            });           
        }
        else if(filter == 'active-patients'){
            allData.forEach(function(row){
                if(row.IsActivePatient){
                    filteredData.push(row);
                }
            });
           
        }
        else if(filter == 'pending-documents'){
            allData.forEach(function(row){
                if(row.IsPendingDocuments){
                    filteredData.push(row);
                }
            });
        }
        else if(filter == 'over-15-days'){
            allData.forEach(function(row){
                if(row.IsOver15Days){
                    filteredData.push(row);
                }
            });
        }
        else if(filter == 'patients-with-distributor'){
            allData.forEach(function(row){
                if(row.IsPatientWithDistributor){
                    filteredData.push(row);
                }
            });
        }
        else if(filter == 'patients-with-pharmacy'){
            allData.forEach(function(row){
                if(row.IsPatientWithPharmacy){
                    filteredData.push(row);
                }
            });
        }
        else if(filter == 'cancelled'){
            allData.forEach(function(row){
                if(row.IsCancelled){
                    filteredData.push(row);
                }
            });
        }
        else if(filter == 'new-orders'){
            allData.forEach(function(row){
                if(row.IsNewOrder){
                    filteredData.push(row);
                }
            });
        }
        else if(filter == 'total-orders'){
            allData.forEach(function(row){
                if(row.IsTotalOrder){
                    filteredData.push(row);
                }
            });
        }
        return(filteredData);
        
    },*/
    
    // removes styling from any selected tile 
    removeFilterBackgrounds: function(component){
        var allMetrics = ['active-patients', 'pending-documents', 'over-15-days', 'patients-with-distributor', 
                           'patients-with-pharmacy', 'cancelled', 'new-orders', 'total-orders', 'all-orders'];
        allMetrics.forEach(function(metric){
            $A.util.removeClass(component.find(metric), 'current-filter');
        });
    },
    
    // handles client side action to sort all table rows inside each accordion section
    sortRows : function(component, event, helper){
        var column = event.currentTarget.id;
        var sortDirection = component.get("v." + column + "SortDirection");
        var filter = component.find('filterCombobox').get("v.value");
        var tableData = helper.handleFilter(component, filter);
        var columnValues = [];
        var sortedData = [];
        tableData.forEach(function(row){
            columnValues.push(row[column]);
        });
        columnValues.sort();
        if(sortDirection == 'asc'){
            var descValues = [];
            // reverse sorting order of column values
            for(var i = (columnValues.length - 1); i >= 0; i--){
                descValues.push(columnValues[i]);
            }
            descValues.forEach(
                function(value){
                    var matchIndex;
                    tableData.forEach(
                        function(row){
                            if(row[column] == value){
                                matchIndex = tableData.indexOf(row);
                                if(!sortedData.includes(row)){
                                    sortedData.push(row);
                                }
                            }
                        }
                    );
                    tableData.splice(matchIndex, 1);
                }
            );
            component.set("v." + column + "SortDirection", 'desc');
        }
        else if(sortDirection == 'desc'){
            // sort in ascending order and update the sort direction to asc
            columnValues.forEach(
                function(value){
                    var matchIndex;
                    tableData.forEach(
                        function(row){
                            if(row[column] == value){
                                matchIndex = tableData.indexOf(row);
                                if(!sortedData.includes(row)){
                                    sortedData.push(row);
                                }
                            }
                        }
                    );
                    tableData.splice(matchIndex, 1);
                }
            );
            component.set("v." + column + "SortDirection", 'asc');
        }
        component.set("v.tableData", sortedData);
    },
    
})