({
	saveBICheckInfo : function(component,event) {
		console.log('BI Component invoked');
        var biCheckDataMap = event.getParam("benefitDetailsMap");
        component.set("v.BICheckDataMap", biCheckDataMap);
	},
    saveOrderCheckInfo : function(component,event) {
		console.log('Order Component invoked');
        var orderCheckDataMap = event.getParam("orderDetailsMap");
        component.set("v.orderCheckDataMap", orderCheckDataMap);
	},
    savePayorCheckInfo : function(component,event) {
		console.log('Payor Component invoked');
        var payorCheckDataMap = event.getParam("payorDetailsMap");
        component.set("v.payorCheckDataMap", payorCheckDataMap);
	},
    saveDocumentCheckInfo : function(component,event) {
		console.log('Document event invoked');
        var documentCheckDataMap = event.getParam("documentDetailsMap");
        component.set("v.documentCheckDataMap", documentCheckDataMap);
	},
    getOrderType : function(component,event){
		var action = component.get("c.getOrderType");
        action.setParams({
            "orderId": component.get("v.orderId"),
            "accountId": component.get("v.accountId")
        });
        
        //Retrieve and process the response
        action.setCallback (this, function(response){
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS")
            {
				var resOrderType = response.getReturnValue();
				console.log('Data fetched @@@@@ ' + resOrderType);
				component.set("v.initCallbackReceived",true);
				component.set("v.orderType", resOrderType.OrderType);
                var canBypass = resOrderType.AllowBypassCheck == 'true' ? false : true;
                
                console.log('allowBypassCheck @@@@@ ' + canBypass);
				component.set("v.allowBypassCheck", canBypass);
                
                var exceptionPB = resOrderType.ExceptionPricebook == 'true' ? true : false;
                component.set("v.isExceptionPB", exceptionPB);
                console.log('***exceptionPB ***' + exceptionPB);
                
                var isSkipped = resOrderType.SkippedPayorMatrix == 'true' ? true : false;
                component.set("v.isPayorMatrixSkipped", isSkipped);
                
                var isSkippedMedAdv = resOrderType.SkippedPayorMatrixForMedAdv == 'true' ? true : false;
                component.set("v.isPayorMatrixSkippedForMedAdv", isSkippedMedAdv);
                
                component.set("v.payorName", resOrderType.thisPayorName);
                
                console.log('***isSkipped ***' + isSkipped);
                
			}else {
                console.log('Problem getting main topic values, response state: ' + state);
            }
        });
            $A.enqueueAction(action); 
	},
    saveCheckInfo : function(component,event) {
        console.log('@@@@ saveCheckInfo @@@@@ ');
        
        var action = component.get("c.saveOrderDetails");
        action.setParams({
            "orderId": component.get("v.orderId"),
            "benefitMap": component.get("v.BICheckDataMap"),
            "payorMap": component.get("v.payorCheckDataMap"),
            "orderMap": component.get("v.orderCheckDataMap"),
            "documentMap": component.get("v.documentCheckDataMap")
        });
        
        //Retrieve and process the response
        action.setCallback (this, function(response){
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS")
            {
				var resStatus = response.getReturnValue();
                console.log('Data Saved @@@@@ ' + resStatus);
                component.set("v.displayMessage",true);
                var notifDiv = component.find('notifDiv');
                if(resStatus == true){
                    component.set("v.resultMessage", "QC Hold details are saved!");
                    if(notifDiv !=undefined && notifDiv !=null){
                        $A.util.addClass(notifDiv, 'slds-theme_success');
                        $A.util.removeClass(notifDiv, 'slds-theme_error');
                    }
                }else{
                    component.set("v.resultMessage", "Please contact system administrator, QC Hold details are not saved!");
                    if(notifDiv !=undefined && notifDiv !=null){
                        $A.util.addClass(notifDiv, 'slds-theme_error');
                        $A.util.removeClass(notifDiv, 'slds-theme_success');
                    }
                }
                setTimeout(function(){component.set("v.displayMessage",false);},3000);
			}else {
                console.log('Problem getting main topic values, response state: ' + state);
            }
        });
            $A.enqueueAction(action); 
    }    
})