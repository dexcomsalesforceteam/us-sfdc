({
    doInit : function(component, event, helper) {
        console.log('##@##' + component.get("v.orderId"));
        helper.getOrderType(component, event);
       // alert('##@Order Type ##' + component.get("v.orderType"));
    },
    saveBiCheckData : function(component, event, helper) {
        console.log('##saving BI Check @##');
        helper.saveBICheckInfo(component, event );
    },
    saveOrderCheckData : function(component, event, helper) {
        console.log('##saving Order Check @##');
        helper.saveOrderCheckInfo(component, event );
    },
    savePayorCheckData : function(component, event, helper) {
        console.log('##saving Payor Check @##');
        helper.savePayorCheckInfo(component, event );
    },
    saveDocumentCheckData  : function(component, event, helper) {
        console.log('##saving Document Check @##');
        helper.saveDocumentCheckInfo(component, event );
    },
    saveData : function(component, event, helper) {
        console.log('##saving All Check @##');
        helper.saveCheckInfo(component, event );
    },
    showSpinner: function(component, event, helper) {
        component.set("v.Spinner", true); 
    },
    
    hideSpinner : function(component,event,helper){
        component.set("v.Spinner", false);
    },
    
    handleClose: function(component, event,helper){
        component.set("v.displayMessage",false);
    }
})