({  
   
    recordUpdate: function(component, event, helper){
        var accRecType = component.get("v.addressRecord.Account_Record_Type__c");        
        if(accRecType === "Prescriber"){
            component.set("v.isPrescriber", true);
        }
    },

    closeModel: function(component, event, helper) {
        var accountId=component.get("v.addressRecord.Account__c");
        component.set("v.isOpen", false);
        var eUrl= $A.get("e.force:navigateToURL");
        eUrl.setParams({
            "url": '/'+accountId 
        });
        eUrl.fire();
        
        var workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function(response) {
            var focusedTabId = response.tabId;
            workspaceAPI.closeTab({tabId: focusedTabId});
        })
        .catch(function(error) {
            console.log(error);
        });
    },
    
    updateAddress : function(component, event, helper) {
        var accountId=component.get("v.addressRecord.Account__c");
        component.find("recordHandler").saveRecord($A.getCallback(function(saveResult) {
            if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                console.log('recordSaved');
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type" : "success",
                    "title": "Success!",
                    "message": "The record has been updated successfully."
                });
                toastEvent.fire();   
                var eUrl= $A.get("e.force:navigateToURL");
                eUrl.setParams({
                    "url": '/'+accountId 
                });
                eUrl.fire();
                
                var workspaceAPI = component.find("workspace");
                workspaceAPI.getFocusedTabInfo().then(function(response) {
                    var focusedTabId = response.tabId;
                    workspaceAPI.closeTab({tabId: focusedTabId});
                })
                .catch(function(error) {
                    console.log(error);
                });
                
            } else if (saveResult.state === "INCOMPLETE") {
                console.log("User is offline, device doesn't support drafts.");
            } else if (saveResult.state === "ERROR") {
                console.log('Problem saving record, error: ' + JSON.stringify(saveResult.error));
            } else {
                console.log('Unknown problem, state: ' + saveResult.state + ', error: ' + JSON.stringify(saveResult.error));
            }
        }));
    },
})