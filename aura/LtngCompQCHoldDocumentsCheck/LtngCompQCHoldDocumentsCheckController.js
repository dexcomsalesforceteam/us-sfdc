({
	getQCHoldDocuments : function(component, event, helper) {
	   helper.getQCHoldDocumentsCheckService(component, event);
	},
    fireChangeEvent : function(component, event, helper) {
       
		helper.updateDocumentCheckData(component, event);	
	}
})