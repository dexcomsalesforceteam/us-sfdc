({
    getQCHoldDocumentsCheckService : function(component, event) {
        var selectedDoc = [];
        var action = component.get("c.getDocumentDetails");
        // alert('** order type doc ** ' + component.get("v.orderType"));
        action.setParams({ 
            "orderId": component.get("v.orderId"),
            "accountId": component.get("v.accountId"),
            "orderType": component.get("v.orderType"),
            "isOrderActivated": component.get("v.isOrderActivated")
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                component.set('v.documentCheckMap',response.getReturnValue());
                
                var documentCheckMap = response.getReturnValue();
                var bypassDocumentCheck = documentCheckMap.bypassCheck == 'false' ? false : true ;
             // alert('HourTrial..'+documentCheckMap.HourTrial);
                component.set("v.bypassDocumentCheck" , bypassDocumentCheck);
                
                component.set("v.CMNExpirationDate" , documentCheckMap.CMNExpirationDate );
                component.set("v.ChartNotesExpirationDate" , documentCheckMap.ChartNotesExpirationDate);
                
                component.set("v.AOB" , documentCheckMap.AOB == 'true' ? true : false);
                if("AOB" in documentCheckMap) selectedDoc.push('AOB');    
                
                component.set("v.BGLogs" , documentCheckMap.BGLogs == 'true' ? true : false);
                if("BGLogs" in documentCheckMap) selectedDoc.push('BGLogs');
                
                component.set("v.LabResults" , documentCheckMap.LabResults == 'true' ? true : false);
                if("LabResults" in documentCheckMap) selectedDoc.push('LabResults');
                
                component.set("v.HourTrial" , documentCheckMap.HourTrial == 'true' ? true : false);
                if("HourTrial" in documentCheckMap) selectedDoc.push('HourTrial');
                
                component.set("v.OOWLetter" , documentCheckMap.OOWLetter == 'true' ? true : false);
                if("OOWLetter" in documentCheckMap) selectedDoc.push('OOWLetter');
                
                component.set("v.HypoglycemicQuestionnaire" , documentCheckMap.HypoglycemicQuestionnaire == 'true' ? true : false);
                if("HypoglycemicQuestionnaire" in documentCheckMap) selectedDoc.push('HypoglycemicQuestionnaire');
                
                component.set("v.ABN" , documentCheckMap.ABN == 'true' ? true : false);
                if("ABN" in documentCheckMap) selectedDoc.push('ABN');
                
                //handling additional docs - start
                component.set("v.LMN" , documentCheckMap.LMN == 'true' ? true : false);
                if("LMN" in documentCheckMap) selectedDoc.push('LMN');
                
                component.set("v.RecentNotesWithCompliance" , documentCheckMap.RecentNotesWithCompliance == 'true' ? true : false);
                if("RecentNotesWithCompliance" in documentCheckMap) selectedDoc.push('RecentNotesWithCompliance');
                //handling additional docs - end
                
                component.set("v.OriginalDateofPurchase" , documentCheckMap.OriginalDateofPurchase == 'true' ? true : false);
                if("OriginalDateofPurchase" in documentCheckMap) selectedDoc.push('OriginalDateofPurchase');
                
                component.set("v.TechSupportNotes" , documentCheckMap.TechSupportNotes == 'true' ? true : false);
                if("TechSupportNotes" in documentCheckMap) selectedDoc.push('TechSupportNotes');
                
               // alert('documentCheckMap.ChartNotes..'+documentCheckMap.ChartNotes);
              //  alert('documentCheckMap.ChartNotesExpirationDate..'+documentCheckMap.ChartNotesExpirationDate);
                component.set("v.CMN" , documentCheckMap.CMN  == 'true' ? true : false);
                component.set("v.ChartNotes" , documentCheckMap.ChartNotes  == 'true' ? true : false);
                if("ChartNotes" in documentCheckMap) selectedDoc.push('ChartNotes');
                console.log('*** Selected Doc *** ' + selectedDoc);
                component.set("v.NADocument" , selectedDoc);
                this.updateCss(component);
                
                var thisEvent = component.getEvent("documentsCheckEvent");
                thisEvent.setParams({"documentDetailsMap": component.get("v.documentCheckMap")});
                thisEvent.fire();
                
            }else{
                console.log('Error getting document data >> ');
            }
            
        });
        
        $A.enqueueAction(action);
    },
    updateCss : function(component) {
        var checkStatus = true;
        var lineComponent = component.find("highlightDiv");
        $A.util.removeClass(lineComponent, 'border');
        var selectedDocs =  component.get("v.NADocument");
        
        
        if( component.get("v.CMN") == null || component.get("v.CMN") == false) checkStatus = false;
        if( selectedDocs.includes('ChartNotes') && (component.get("v.ChartNotes") == false)) checkStatus = false;
        if( selectedDocs.includes('AOB') && (component.get("v.AOB") == false)) checkStatus = false;
        if( selectedDocs.includes('BGLogs') && (component.get("v.BGLogs") == null || component.get("v.BGLogs") == false)) checkStatus = false;
        if( selectedDocs.includes('LabResults') && (component.get("v.LabResults") == null || component.get("v.LabResults") == false)) checkStatus = false;
        if( selectedDocs.includes('HourTrial') && (component.get("v.HourTrial") == null || component.get("v.HourTrial") == false)) checkStatus = false;
        if( selectedDocs.includes('OOWLetter') && (component.get("v.OOWLetter") == null || component.get("v.OOWLetter") == false)) checkStatus = false;
        if( selectedDocs.includes('HypoglycemicQuestionnaire') && (component.get("v.HypoglycemicQuestionnaire" ) == null || component.get("v.HypoglycemicQuestionnaire" ) == false)) checkStatus = false;
        if( selectedDocs.includes('ABN') && (component.get("v.ABN") == null || component.get("v.ABN") == false)) checkStatus = false;
        if( selectedDocs.includes('OriginalDateofPurchase') && (component.get("v.OriginalDateofPurchase") == null || component.get("v.OriginalDateofPurchase") == false)) checkStatus = false;
        if( selectedDocs.includes('TechSupportNotes') && (component.get("v.TechSupportNotes") == null || component.get("v.TechSupportNotes") == false)) checkStatus = false;
        if(checkStatus || component.get("v.bypassDocumentCheck" ) == true){ 
            $A.util.removeClass(lineComponent, 'slds-border-incomplete');            
            $A.util.addClass(lineComponent , 'slds-border-complete');
        }else{ 
            $A.util.removeClass(lineComponent, 'slds-border-complete'); 
            $A.util.addClass(lineComponent , 'slds-border-incomplete');
        }
    },
    
    updateDocumentCheckData : function(component, event) {
        
        var bypassDocumentCheck = component.get("v.bypassDocumentCheck" );
        var CMN 				=  component.get("v.CMN");
        var CMNExpirationDate 	=   component.get("v.CMNExpirationDate" );
        var ChartNotes			 =  component.get("v.ChartNotes");
        var ChartNotesExpirationDate =  component.get("v.ChartNotesExpirationDate");
        var AOB 				=  component.get("v.AOB");
        var BGLogs				 = component.get("v.BGLogs");
        var LabResults 			=   component.get("v.LabResults");
        var HourTrial 			=   component.get("v.HourTrial");
        var OOWLetter			 =  component.get("v.OOWLetter");
        var HypoglycemicQuestionnaire =  component.get("v.HypoglycemicQuestionnaire" );
        var ABN 				=  component.get("v.ABN");
        var OriginalDateofPurchase =  component.get("v.OriginalDateofPurchase");
        var TechSupportNotes 	=  component.get("v.TechSupportNotes");
        var LMN 				=  component.get("v.LMN"); 
        var RecentNotesWithCompliance = component.get("v.RecentNotesWithCompliance");
        
        var documentCheckMap 	= component.get("v.documentCheckMap");
        
        //update CSS 
        this.updateCss(component);
        
        if(documentCheckMap["bypassCheck"] != null) documentCheckMap["bypassCheck"] = bypassDocumentCheck;
        if(documentCheckMap["CMN"] != null) documentCheckMap["CMN"] = CMN;
        if(documentCheckMap["CMNExpirationDate"] != null) documentCheckMap["CMNExpirationDate"] = CMNExpirationDate;
        if(documentCheckMap["ChartNotes"] != null)  documentCheckMap["ChartNotes"] = ChartNotes;
        if(documentCheckMap["ChartNotesExpirationDate"] != null) documentCheckMap["ChartNotesExpirationDate"] = ChartNotesExpirationDate;
        if(documentCheckMap["AOB"] != null ) documentCheckMap["AOB"] = AOB;
        if(documentCheckMap["BGLogs"] != null) documentCheckMap["BGLogs"] = BGLogs;
        if(documentCheckMap["LabResults"] != null) documentCheckMap["LabResults"] = LabResults;
        if(documentCheckMap["HourTrial"] != null) documentCheckMap["HourTrial"] = HourTrial;
        if(documentCheckMap["OOWLetter"] != null) documentCheckMap["OOWLetter"] = OOWLetter;
        if(documentCheckMap["HypoglycemicQuestionnaire"] != null) documentCheckMap["HypoglycemicQuestionnaire"] = HypoglycemicQuestionnaire;
        if(documentCheckMap["ABN"] != null) documentCheckMap["ABN"] = ABN;
        if(documentCheckMap["OriginalDateofPurchase"] != null) documentCheckMap["OriginalDateofPurchase"] = OriginalDateofPurchase;
        if(documentCheckMap["TechSupportNotes"] != null) documentCheckMap["TechSupportNotes"] = TechSupportNotes;
        if(documentCheckMap["LMN"] != null) documentCheckMap["LMN"] = LMN;//added to handle new documents
        if(documentCheckMap["RecentNotesWithCompliance"] != null) documentCheckMap["RecentNotesWithCompliance"] = RecentNotesWithCompliance;//added to handle new documents
        
        console.log('documentCheckMap',documentCheckMap);
        
        var thisEvent = component.getEvent("documentsCheckEvent");
        thisEvent.setParams({"documentDetailsMap": documentCheckMap});
        thisEvent.fire();
    }
})