({
	doInit : function(component, event, helper) {
		helper.getInitData(component, event);
	},
    
    handleApprove : function(component, event, helper){
        var isValid = helper.validateBeforeProcess(component, event);
        var errorDiv = component.find('errorDiv');
        if(isValid){
            $A.util.addClass(errorDiv, 'slds-hide');
            helper.handleApprovalProcess(component, event,true);
        }else{
            //show message
            $A.util.removeClass(errorDiv, 'slds-hide');
        }
    },
    
    handleReject : function(component, event, helper){
        var isValid = helper.validateBeforeProcess(component, event);
        var errorDiv = component.find('errorDiv');
        if(isValid){
            $A.util.addClass(errorDiv, 'slds-hide');
            helper.handleApprovalProcess(component, event,false);
        }else{
            //show message
            $A.util.removeClass(errorDiv, 'slds-hide');
        }
    },
    
    handleBaseCheckboxChange : function(component, event, helper){
        helper.handleBaseCheckChange(component,event);
    }
})