({
	getInitData : function(cmp,event) {
		var action = cmp.get("c.getPendingCompProdRequests");
        cmp.set("v.loadSpinner",true);
        action.setParams({ });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                cmp.set("v.loadSpinner",false);
                cmp.set("v.responseReceived",true);
                var compProductList = response.getReturnValue();
                if(compProductList != null && compProductList != undefined && compProductList.length < 1){
                    cmp.set("v.listSizeGreater",false);
                }
                cmp.set("v.compProductList",compProductList);
                cmp.set("v.loadSpinner",false);
            }else if(state === "INCOMPLETE"){
                cmp.set("v.loadSpinner",false);
            }else if (state === "ERROR"){
                cmp.set("v.loadSpinner",false);
            }
            
        });
        
        $A.enqueueAction(action);
	},
    
    handleApprovalProcess : function(cmp,event, approveOrReject){
        cmp.set("v.loadSpinner",true);
        var action = cmp.get("c.approveRejectRecords");
        var selectedRecords = cmp.find('boxItems');
        var idListOfSelectedRecords = [];
        if(!Array.isArray(selectedRecords)){//is not array and a single record in entire table
            if(selectedRecords.get("v.checked")){
                idListOfSelectedRecords.push(selectedRecords.get("v.name"));
            }
        }else{//selection is array
            for(var i=0; i<selectedRecords.length;i++){
                if(selectedRecords[i].get("v.checked")){
                    idListOfSelectedRecords.push(selectedRecords[i].get("v.name"));
                }
            }          
        }
        console.log('idListOfSelectedRecords::::',idListOfSelectedRecords)
        console.log('approveOrReject::::',approveOrReject);
        action.setParams({ approveOrReject : approveOrReject,
                          approveRejectRecordIdList : idListOfSelectedRecords});
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                cmp.set("v.loadSpinner",false);
                var returnValue = response.getReturnValue();
                console.log('response is::::',returnValue);
                this.getInitData(cmp,event);
            }else if(state === "INCOMPLETE"){
                cmp.set("v.loadSpinner",false);
            }else if (state === "ERROR"){
                cmp.set("v.loadSpinner",false);
            }
        });
        
        $A.enqueueAction(action);
    },
    //this function handles select-deselect all for the component
    handleBaseCheckChange : function(cmp,event){
        if(cmp.find('basecheck').get("v.checked")){
            const allRecords = cmp.find('boxItems');
            let chk = (allRecords.length == null) ? [allRecords] : allRecords;
            chk.forEach(checkbox => checkbox.set('v.checked', true));
        }else{
            const allRecords = cmp.find('boxItems');
            let chk = (allRecords.length == null) ? [allRecords] : allRecords;
            chk.forEach(checkbox => checkbox.set('v.checked', false));
        }
    },
    
    validateBeforeProcess : function(component, event){
        console.log('inside validate before process')
        var isValid = false;
        var selectedRecords = component.find('boxItems');
        console.log('selectedRecords::::',selectedRecords);
        if(!Array.isArray(selectedRecords)){//not an array
            if(selectedRecords.get("v.checked")){
                isValid = true;
            }
        }else{
            for(var i=0; i<selectedRecords.length; i++){
                if(selectedRecords[i].get("v.checked")){
                    isValid = true;
                }
            }            
        }
        return isValid;
    }
    
})