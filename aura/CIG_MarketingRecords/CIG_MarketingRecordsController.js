({    
	doInit : function(component, event, helper) {        
        component.set('v.columns', [
            {label: 'Title', fieldName: 'linkName', editable:'false', type: 'url', typeAttributes: {label: { fieldName: 'Note_Title__c' }, target: '_self'}},           
            {label: 'Product', fieldName: 'ProductName', editable:'false', type: 'text', sortable: true},
            {label: 'Product Claim', fieldName: 'Product_Claim__c', editable:'false', type: 'text'},
            {label: 'Source', fieldName: 'Collateral_Source__c', editable:'false', type: 'text'},           
            {label: 'Target', fieldName: 'Collateral_Target__c', editable:'true', type: 'text'},
            {label: 'URL', fieldName: 'Collateral_URL__c', editable:'false', type: 'text'},            
            {label: 'Date First Observed', fieldName: 'Date_first_observed__c', editable:'false', type: 'date-local', sortable: true, typeAttributes:{year:'numeric',month:'short',day:'2-digit'}}
            
            
        ]);        
        helper.getMarketingData(component, helper);
        console.log('***** Clinical Org Id=' + component.get("v.recordId"));       
    },
    
    onSave : function (component, event, helper) {
        helper.saveDataTable(component, event, helper);
    },
    
    //Method gets called by onsort action,
    handleSort : function(component,event,helper){
        //Returns the field which has to be sorted
        var sortBy = event.getParam("fieldName");
        //returns the direction of sorting like asc or desc
        var sortDirection = event.getParam("sortDirection");
        //Set the sortBy and SortDirection attributes
        component.set("v.sortBy",sortBy);
        component.set("v.sortDirection",sortDirection);
        // call sortData helper function
        helper.sortData(component,sortBy,sortDirection);
    },    
    openModel: function(component, event, helper) {
      // for Display Model,set the "isOpen" attribute to "true"
      component.set("v.isOpen", true);
   },
   
   closeModel: function(component, event, helper) {
      // for Hide/Close Model,set the "isOpen" attribute to "Fasle"     
      component.set("v.isOpen", false);
   },
   handleCloseModal : function(component, event, helper) {
       var value = event.getParam("param");
       component.set("v.isOpen", false);
   },
   SubmitFormData : function(component, event, helper) {
       var attribute1 = component.get('v.ModalOrgType');
       var marketingForm = component.find('CIG_AddMarketing');
       marketingForm.mySubmitOrgData(attribute1);       
   }, 
   handleClinicalHeaderClick: function (component, event, helper) {
   	var evt = $A.get("e.force:navigateToComponent");
    evt.setParams({
        componentDef : "c:CIG_MarketingRecords",
        componentAttributes: {            
            recordId : component.get("v.recordId")
        }
    });
    evt.fire(); 
   }
   
})