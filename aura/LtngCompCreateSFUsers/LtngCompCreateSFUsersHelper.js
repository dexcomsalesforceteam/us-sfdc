({
    createObjectData: function(component, event) {
        // get the userList from component and add(push) New Object to List  
        var RowItemList = component.get("v.userList");
        console.log('RowItemList::::',RowItemList)
        RowItemList.push({
            'sobjectType': 'User',
            'FirstName': '',
            'LastName': '',
            'Alias': '',
            'ProfileId':'',
            'UserRoleId':'',
            'Dexcom_Title__c':'',
            'User_Location__c':'',
            'Oracle_User_Name__c':''
        });
        // set the updated list to attribute (userList) again    
        component.set("v.userList", RowItemList);
    },
    // helper function for check if first Name is not null/blank on save  
    validateRequired: function(component, event) {
        var isValid = true;
        var allUserRows = component.get("v.userList");
        for (var indexVar = 0; indexVar < allUserRows.length; indexVar++) {
            if (allUserRows[indexVar].FirstName == '') {
                isValid = false;
                alert('First Name Can\'t be Blank on Row Number ' + (indexVar + 1));
            }
        }
        return isValid;
    },
})