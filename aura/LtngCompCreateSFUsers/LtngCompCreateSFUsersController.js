({
 
    // function call on component Load
    doInit: function(component, event, helper) {
        // create a Default RowItem [User Instance] on first time Component Load
        // by call this helper function  
        helper.createObjectData(component, event);
    },
 
    // function for save the Records 
    Save: function(component, event, helper) {
        // first call the helper function in if block which will return true or false.
        // this helper function check the "first Name" will not be blank on each row.
        if (helper.validateRequired(component, event)) {
            // call the apex class method for save the User List
            // with pass the user List attribute to method param. 
            component.set("v.displaySpinner",true);
            var action = component.get("c.saveUsers");
            action.setParams({
                "userList": JSON.stringify(component.get("v.userList"))
            });
            // set call back 
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    component.set("v.displaySpinner",false);
                    // if response if success then reset/blank the 'userList' Attribute 
                    // and call the common helper method for create a default Object Data to User List 
                    component.set("v.userList", []);
                    helper.createObjectData(component, event);
                }else{
                    component.set("v.displaySpinner",false);
                }
            });
            // enqueue the server side action  
            $A.enqueueAction(action);
        }
    },
 
    // function for create new object Row in User List 
    addNewRow: function(component, event, helper) {
        // call the comman "createObjectData" helper method for add new Object Row to List  
        helper.createObjectData(component, event);
    },
 
    // function for delete the row 
    removeDeletedRow: function(component, event, helper) {
        // get the selected row Index for delete, from Lightning Event Attribute  
        var index = event.getParam("indexVar");
        var AllRowsList = component.get("v.userList");
        AllRowsList.splice(index, 1);
        // set the userList after remove selected row element  
        component.set("v.userList", AllRowsList);
    },
})