({
    getOrderStatusDetails : function(component) {
        var action = component.get("c.getOrderStatus");
        var orderNumber=component.get("v.orderNumber");
        if(orderNumber !=null && orderNumber !=''){
            action.setParams({
                "orderNumber": orderNumber
            });
            
            action.setCallback (this, function(response){
                var state = response.getState();
                
                if(component.isValid() && state == "SUCCESS")
                {
                    var orderStatusData = response.getReturnValue();
                    
                    component.set('v.objOrderStatusWrapper', orderStatusData);
                    
                    var orderStatus = orderStatusData.OrderStatus;
                    
                    var i;
                    for (i = 0; i < orderStatus.length; i++) {
                        if(orderStatus[i].Invoice_Number !='' && orderStatus[i].Invoice_Number){
                            component.set('v.orderInvoiceTable', true);
                        }
                    }
                    
                    for (i = 0; i < orderStatus.length; i++) {
                        if(orderStatus[i].Hold_name !='' && orderStatus[i].Hold_name){
                            component.set('v.orderQcHoldTable', true);
                        }
                    }
                    
                }
            });
        }
        $A.enqueueAction(action);             
        
        
    }
})