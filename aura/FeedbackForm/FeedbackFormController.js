({
    doInit : function(component) {
      // Initialize input select options for chatter group ids
        var opts = [
            { "class": "optionClass", label: "News", value: "0F94B0000008lWHSAY", selected: "true" },
            { "class": "optionClass", label: "Issues", value: "0F94B0000008lWMSAY" },
            { "class": "optionClass", label: "Feature Requests", value: "0F94B0000008lWRSAY" }
        ];
        
        var utilityAPI = component.find("utilitybar");
        
        utilityAPI.getEnclosingUtilityId().then(function(utilityId) {
        	component.set("v.isInUtilityBar",true);
            component.set("v.showFeedbackWindow",true);
            component.set("v.recordURL", "/" + component.get("v.recordId"));
            
            // Populate chatter group ids select box.
            component.find("feedbackGroupId").set("v.options", opts);
        })
        .catch(function(error) {
            console.log(error);
        });
        
        var navService = component.find("navService");
        // Sets the route to /lightning/o/Account/home
        var pageReference = {
            "type": "standard__namedPage",
            "attributes": {
                "pageName": "chatter"    
            }
        };
        component.set("v.pageReference", pageReference);
        // Set the URL on the link or use the default if there's an error
        var defaultUrl = "#";
        navService.generateUrl(pageReference)
            .then($A.getCallback(function(url) {
                cmp.set("v.url", url ? url : defaultUrl);
            }), $A.getCallback(function(error) {
                cmp.set("v.url", defaultUrl);
            }));
        
    },
    
    onChange: function(cmp) {
		 var selChatter = cmp.find("feedbackGroupId");
        
         if(cmp.get("v.isInUtilityBar")){
         	cmp.set("v.chatterGroupID", selChatter.get("v.value"));
         }
	 },    
    
    closeComponent : function(component, event) {
        if(!component.get("v.isInUtilityBar")){
           component.set("v.showFeedbackWindow",false);
        } else {
            //close utility bar if in utility bar
            var utilityAPI = component.find("utilitybarclose");
           //clear feedback
           component.find("feedbackBody").set("v.value","");
           utilityAPI.minimizeUtility();
        }
    },
    
    submitFeedback : function(component,event) {
        console.log('clicked button to submit form');
        if($A.util.isEmpty(component.find("feedbackBody").get("v.value"))){
            alert('Please enter feedback before submitting');
        }else{
            var feedback = component.find("feedbackBody").get("v.value");
            var subject = component.get("v.subject");
            //var chatterGroupID = component.get("v.chatterGroupID");
            var chatterGroupID = component.find("feedbackGroupId").get("v.value");
            
            //set recordURL again if in utility bar
            if(component.get("v.isInUtilityBar")){
                component.set("v.recordURL", "/" + component.get("v.recordId"));
                component.set("v.chatterGroupID", "/" + chatterGroupID);
            }
            var recordURL = component.get("v.recordURL");
            if(feedback!=''){
                var action = component.get("c.processFeedback");
                console.log('feedbackBody is-->' + feedback + '');
                action.setParams({ "feedback" : feedback,
                                  "subject" : subject,
                                "chatterGroupID" : chatterGroupID,
                                "recordURL" : recordURL});
                action.setCallback(this, function(response){
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        console.log('response-->'+ response.getReturnValue() + '');
                        component.set("v.feedbackSubmitted", response.getReturnValue());
                        if(!component.get("v.isInUtilityBar")){
                           component.set("v.showFeedbackWindow",false);
                        } else {
                            //clear feedback
                            component.find("feedbackBody").set("v.value","");
                            //close utility bar if in utility bar
                            var utilityAPI = component.find("utilitybarclose");
                            //utilityAPI.minimizeUtility();
                        }
                        
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Success!",
                            "message": "Feedback submitted successfully."
                        });
                        toastEvent.fire();
                        
        if(component.get("v.isInUtilityBar")){
           var utilityAPI = component.find("utilitybarclose");
           //clear feedback
           component.find("feedbackBody").set("v.value","");
           utilityAPI.minimizeUtility();
        } 
                        
                    }
                });
            }
            $A.enqueueAction(action);
        }
    	
	},
    
    openSRPage: function(cmp, event, helper) {
       
        var navService = cmp.find("navService");
        // Uses the pageReference definition in the init handler
        var pageReference = cmp.get("v.pageReference");
        event.preventDefault();
        navService.navigate(pageReference);
        var utilityAPI = cmp.find("utilitybarclose");
        utilityAPI.minimizeUtility();
   },
})