({
	doInit : function(component, event, helper) {
		helper.loadOrderCheckData(component);	
	},
    fireChangeEvent : function(component, event, helper) {
		helper.updateOrderCheckData(component, event);	
	}
})