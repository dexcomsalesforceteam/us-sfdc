({
	loadOrderCheckData : function(component) {
        var prodEligible = false;
        var prodEligibleCMN = false;
        var prodEligibleAuth = false;
        var bypassCheck = false;
        
		console.log('##@account##' + component.get("v.accountId"));
		 //call apex class method by passing in the orderid parameter value
        var action = component.get("c.getOrderDetails");
        action.setParams({
            orderId : component.get("v.orderId"),
            accountId : component.get("v.accountId"),
            orderType : component.get("v.orderType"),
            isOrderActivated : component.get("v.isOrderActivated")
        });
        
        //Retrieve and process the response
        action.setCallback (this, function(response){
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS")
            {
                //retrieve the response value (map) from the method
                var orderDetails = response.getReturnValue();
                
                component.set("v.orderCheckMap", response.getReturnValue());                
            	console.log('orderDetails >>' + orderDetails);
                
                // set the attribute values for the component from the values retrieved from the map
                if(orderDetails != null){
                    //bypass check
                    if(orderDetails["bypassCheck"] == 'true')
                    	bypassCheck = true;
                    
                    component.set("v.bypassOrderCheck", bypassCheck);
                    
                    if(orderDetails["productsEligible"] == 'true')
                        prodEligible = true;
                    
                    //alert(orderDetails["transmitterEligible"]);
                    component.set("v.productsEligible" , prodEligible);
                    component.set("v.sensorEligible" , orderDetails["sensorEligible"]);
                    component.set("v.transmitterEligible" , orderDetails["transmitterEligible"]);
                    component.set("v.receiverEligible" , orderDetails["receiverEligible"]);
                  
                    if(orderDetails["productsWithInPrescribedLimits"] == 'true')
                        prodEligibleCMN = true;
                    component.set("v.productsWithInPrescribedLimits" , prodEligibleCMN);
                    component.set("v.prescribedRemainingSensorUnits" , orderDetails["prescribedRemainingSensorUnits"]);
                    component.set("v.prescribedRemainingTransmitterUnits" , orderDetails["prescribedRemainingTransmitterUnits"]);
                    component.set("v.prescribedRemainingReceiverUnits" , orderDetails["prescribedRemainingReceiverUnits"]);
                      
                    if(orderDetails["productsWithInAuthorizedLimits"] == 'true')
                        prodEligibleAuth = true;
                    component.set("v.productsWithInAuthorizedLimits" , prodEligibleAuth);
                    component.set("v.authorizedRemainingSensorUnits" , orderDetails["authorizedRemainingSensorUnits"]);
                    component.set("v.authorizedRemainingTransmitterUnits" , orderDetails["authorizedRemainingTransmitterUnits"]);
                    component.set("v.authorizedRemainingReceiverUnits" , orderDetails["authorizedRemainingReceiverUnits"]);
                   // alert( "prodEligibleAuth ::" + prodEligibleAuth);
                    //alert( "authorizedRemainingSensorUnits ::" + orderDetails["authorizedRemainingSensorUnits"]);
                    //alert( "authorizedRemainingTransmitterUnits ::" + orderDetails["authorizedRemainingTransmitterUnits"]);
                    //alert( "authorizedRemainingReceiverUnits ::" + orderDetails["authorizedRemainingReceiverUnits"]);
                    if(prodEligibleAuth == true &&
                      	orderDetails["authorizedRemainingSensorUnits"] == 'N/A' &&
                      	orderDetails["authorizedRemainingTransmitterUnits"] == 'N/A' && 
                      	orderDetails["authorizedRemainingReceiverUnits"] == 'N/A'){
                       // alert('***Hiding the section ***');
                        component.set("v.priorAuthCheck", true);
                       // prodEligibleAuth = true;
                       // component.set("v.productsWithInAuthorizedLimits" , true);
                    }
                    //alert("prodEligibleAuth ::" + prodEligibleAuth);
                }
                if((prodEligibleAuth && prodEligibleCMN && prodEligible) || bypassCheck)
                    this.updateCss(component, true);
                else 
                    this.updateCss(component, false);
                
                var thisEvent = component.getEvent("orderComponentEvent");
                    thisEvent.setParams({"orderDetailsMap": component.get("v.orderCheckMap")});
                    thisEvent.fire();
            }else {
                console.log('Problem getting main topic values, response state: ' + state);
            }
	});
        $A.enqueueAction(action);
	},
    
    updateCss : function(component, checkStatus) {
        var lineComponent = component.find("highlightDiv");
        $A.util.removeClass(lineComponent, 'border');
        
    	console.log('checkStatus >>' +checkStatus);
        if(checkStatus){ 
            $A.util.removeClass(lineComponent, 'slds-border-incomplete');            
            $A.util.addClass(lineComponent , 'slds-border-complete');
        }else{ 
             $A.util.removeClass(lineComponent, 'slds-border-complete'); 
            $A.util.addClass(lineComponent , 'slds-border-incomplete');
        }
	},
    
    updateOrderCheckData : function(component, event) {
        console.log('fireChangeEvent >> ');
        var bypassCheck = component.get("v.bypassOrderCheck");
        
        var productsEligible = component.get("v.productsEligible");
        var prescribedLimits = component.get("v.productsWithInPrescribedLimits"); 
        var authorizedLimits = component.get("v.productsWithInAuthorizedLimits");
        
        var orderDataMap = component.get("v.orderCheckMap");
        
        //update CSS 
        if((productsEligible && prescribedLimits && authorizedLimits) || bypassCheck){
            this.updateCss(component, true);
        }else{
            this.updateCss(component, false);
        }
        //fire change event
        if(productsEligible != null){
           orderDataMap["productsEligible"] = productsEligible; 
        }
        if(prescribedLimits != null){
           orderDataMap["productsWithInPrescribedLimits"] = prescribedLimits; 
        }
        if(authorizedLimits != null){
           orderDataMap["productsWithInAuthorizedLimits"] = authorizedLimits; 
        }
        if(bypassCheck != null){
            orderDataMap["bypassCheck"] =  bypassCheck;
        }
        //alert('authorizedLimits :: ' + authorizedLimits);
    	var thisEvent = component.getEvent("orderComponentEvent");
        thisEvent.setParams({"orderDetailsMap": orderDataMap});
        thisEvent.fire();
	}
    
})