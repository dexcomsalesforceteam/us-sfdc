({
    loadCCData : function(component) {
        //call apex class method to get all Credit Card details by 
        //passing in the accountnumber as Parameter
        var action = component.get("c.getCreditCardDetails");
        action.setParams({
            "accountNumber": component.get("v.accountNumber")
        });
        
        //Retrieve the list of Credit Cards and process the response
        action.setCallback (this, function(response){
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS")
            {
                var ccData = response.getReturnValue();
                component.set('v.data', ccData);
                console.log('data..',ccData);
            }
        });
        $A.enqueueAction(action);             
    },
    setPrimaryCCForCustomer : function(component, cardId) {
        //call apex class method to set the Primary Credit Card
        var action = component.get("c.setPrimaryCardForCustomer");
        action.setParams({
            "accountNumber": component.get("v.accountNumber"),
            "cardId" : cardId
        });
        
        //Retrieve and process the response
        action.setCallback (this, function(response){
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS")
            {
                var message = response.getReturnValue();
                
                this.loadCCData(component);
            }
        });
        $A.enqueueAction(action);             
    },
    updateCardData : function(component,cardId) {
        //call apex class method to get all Credit Card details by 
        //passing in the accountnumber as Parameter
        var action = component.get("c.updateCreditCard");
        action.setParams({
            "orderId": component.get("v.orderRecId"),
            "cardNumber": cardId
        });
        
        //Retrieve the list of Credit Cards and process the response
        action.setCallback (this, function(response){
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS")
            {
          var myEvent = $A.get("e.c:CreditCardEvent");
              myEvent.fire();
             //  alert(state);
            }
        });
        $A.enqueueAction(action);             
    }
    
    
    
})