({
    init : function(component, event, helper) {
      var orderRecId=  component.get("v.orderRecId");
        var actions;
 
        if(orderRecId !=undefined){
         actions = [            
            { label: 'Add To Order', name: 'Add_To_Order' }
        ];
        }
        
        if(orderRecId == undefined){
         actions =  [
            { label: 'Make Primary', name: 'make_primary' }
           ];
        }
     component.set('v.columns', [
            { label: 'Name', fieldName: 'name', type: 'text' },
            { label: 'Card Type', fieldName: 'cardType', type: 'text' },
            { label: 'Last 4 Digits', fieldName: 'maskedNumber', type: 'text' },
            { label: 'Expiration Date', fieldName: 'expirationDate', type: 'text' },
            { label: 'Order of Preference', fieldName: 'orderOfPreference', type: 'number', cellAttributes: { alignment: 'left' } },
            { type: 'action', typeAttributes: { rowActions: actions } }
        ]);
        if(component.get("v.accountNumber") != null)
        	helper.loadCCData(component);
    },
    handleRowAction: function (component, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');
        if(action.name == "make_primary"){
          
          helper.setPrimaryCCForCustomer(component, row.cardID);
        }
        if(action.name == "Add_To_Order"){
          helper.updateCardData(component,row.cardID)
         }
    }
})