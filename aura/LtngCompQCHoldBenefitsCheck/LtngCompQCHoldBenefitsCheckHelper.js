({
	loadBenefitCheckData : function(component) {
        var benefitVerified = false;
        var bypassCheck = false;
       
        //call apex class method by passing in the orderid parameter value
        var action = component.get("c.getBIDetails");
        action.setParams({
            "orderId": component.get("v.orderId"),
            "accountId": component.get("v.accountId"),
            "orderType": component.get("v.orderType"),
            "isOrderActivated": component.get("v.isOrderActivated")
        });
        
        //Retrieve and process the response
        action.setCallback (this, function(response){
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS")
            {
                //retrieve the response value (map) from the method
                var orderBIDetails = response.getReturnValue();
                
                component.set("v.benefitCheckMap", response.getReturnValue());
            	console.log('orderDetails' + orderBIDetails);
                // set the attribute values for the component from the values retrieved from the map
                if(orderBIDetails != null){
                    if(orderBIDetails["bypassCheck"] == 'true')
                    	bypassCheck = true;
                    
                    component.set("v.bypassBICheck", bypassCheck);
                    
                    if(orderBIDetails["biActive"] == 'true')
                    	benefitVerified = true;
                  // alert('##map##' + orderBIDetails["biActive"] );
                    component.set("v.isBIVerified" , benefitVerified);
                    component.set("v.lastBICheckDate" , orderBIDetails["lastBICheckDate"]);
                    component.set("v.lastBIDoneBy" , orderBIDetails["lastBIDoneBy"]);
                    
                    component.set("v.authRequired" , orderBIDetails["authRequired"]);
                    component.set("v.priorAuthProducts" , orderBIDetails["priorAuthProducts"]);
                    
                    if(benefitVerified || bypassCheck){
                        this.updateCss(component, true);
                    }else{
                        this.updateCss(component, false);
                    }
                    var thisEvent = component.getEvent("benefitComponentEvent");
                    thisEvent.setParams({"benefitDetailsMap": component.get("v.benefitCheckMap")});
                    thisEvent.fire();
                    
                } 
            }else {
                console.log('Problem getting main topic values, response state: ' + state);
            }
	});
        $A.enqueueAction(action); 
    },
    
    updateCss : function(component, checkStatus) {
        var lineComponent = component.find("highlightDiv");
        $A.util.removeClass(lineComponent, 'border');
        
    	console.log('checkStatus benefit>>' +checkStatus);
        if(checkStatus){ 
            $A.util.removeClass(lineComponent, 'slds-border-incomplete');            
            $A.util.addClass(lineComponent , 'slds-border-complete');
        }else{ 
             $A.util.removeClass(lineComponent, 'slds-border-complete'); 
            $A.util.addClass(lineComponent , 'slds-border-incomplete');
        }
	},
    
    updateBenefitCheckData : function(component, event) {
        
        var benefitVerified = component.get("v.isBIVerified");
        var bypassCheck = component.get("v.bypassBICheck");
        var benefitDataMap = component.get("v.benefitCheckMap");
        console.log('priorAuthCheck **' + priorAuthCheck);
        //update CSS 
        if(benefitVerified || bypassCheck){
            this.updateCss(component, true);
        }else{
            this.updateCss(component, false);
        }
        //fire change event
        if(benefitVerified != null){
            benefitDataMap["biActive"] = benefitVerified;
        }         
        if(bypassCheck != null){
           benefitDataMap["bypassCheck"] =  bypassCheck;
        }   
            var thisEvent = component.getEvent("benefitComponentEvent");
            thisEvent.setParams({"benefitDetailsMap": benefitDataMap});
            thisEvent.fire();  
    }
    
       
})