({
	doInit : function(component, event, helper) {
		helper.loadBenefitCheckData(component);	
	},
    fireChangeEvent : function(component, event, helper) {
		helper.updateBenefitCheckData(component, event);	
	}
})