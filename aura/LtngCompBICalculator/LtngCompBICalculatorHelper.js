({
    loadAccountDetails : function(component) {
        //call apex class method by passing in the accountid parameter value
        var action = component.get("c.getAccountBIDetails");
        action.setParams({
            "accountId": component.get("v.accountId"),
            "opptyId": component.get("v.opptyId"),
			"generation" : component.get("v.generation"),
            "shipmentDuration" : component.get("v.shipmentDuration")
        });
        
        //Retrieve and process the response
        action.setCallback (this, function(response){
            var state = response.getState();
            if(component.isValid() && state == "SUCCESS")
            {
                //retrieve the response value (map) from the method
                var accountBIDetails = response.getReturnValue();
                console.log('accountBIDetails' + accountBIDetails);
                console.log('Shipment Dur> ' + component.get("v.shipmentDuration"));
                // set the attribute values for the component from the values retrieved from the map
                if(accountBIDetails != null){
                    component.set("v.lastBICheckDate" , accountBIDetails["lastBICheckDate"]);
                    component.set("v.isLastBICheckDateValid" , accountBIDetails["isLastBICheckDateValid"]);
                    component.set("v.priorAuthRequired" , accountBIDetails["priorAuthRequired"]);
                    component.set("v.canShowBICalculator" , accountBIDetails["canShowBICalculator"]);
                    component.set("v.canShowSecondaryBenefitsWarning" , accountBIDetails["secondaryBenefitExist"]);
					console.log(component.get("v.canShowSecondaryBenefitsWarning"));
                    
                    //Coverage formatting with percentage
                    component.set("v.coverage" , Number(accountBIDetails["coverage"]));
                    if(accountBIDetails["coverage"] != null)
                        component.set("v.formattedCoverage" , Number(accountBIDetails["coverage"])+"%");
                    
                    //Copay formatting with $
                    component.set("v.copayDOS" , Number(accountBIDetails["copayDOS"]));
                    if(accountBIDetails["copayDOS"] != null)
                        component.set("v.formattedCopayDOS" , "$" + Number(accountBIDetails["copayDOS"]).toFixed(2));
                    
                    //Copay Line Item formatting with $
                    component.set("v.copayLineItem" , Number(accountBIDetails["copayLineItem"]));
                    if(accountBIDetails["copayLineItem"] != null)
                        component.set("v.formattedCopayLineItem" , "$" + Number(accountBIDetails["copayLineItem"]).toFixed(2));
                    
                    //Individual Deductible formatting with $
                    component.set("v.indDeductible" , Number(accountBIDetails["indDeductible"]));
                    if(accountBIDetails["indDeductible"] != null)
                        component.set("v.formattedIndDeductible" , "$" + Number(accountBIDetails["indDeductible"]).toFixed(2));
                    
                    //Individual Deductible Met formatting with $
                    component.set("v.indDeductibleMet" , Number(accountBIDetails["indDeductibleMet"]));
                    if(accountBIDetails["indDeductibleMet"] != null)
                        component.set("v.formattedIndDeductibleMet" , "$" + Number(accountBIDetails["indDeductibleMet"]).toFixed(2));
                    
                    //Individual OOP Max formatting with $
                    component.set("v.indOOPMax" , Number(accountBIDetails["indOOPMax"]));
                    if(accountBIDetails["indOOPMax"] != null)
                        component.set("v.formattedIndOOPMax" , "$" + Number(accountBIDetails["indOOPMax"]).toFixed(2));
                    
                    //Individual OOP Max Met formatting with $
                    component.set("v.indOOPMaxMet" , Number(accountBIDetails["indOOPMaxMet"]));
                    if(accountBIDetails["indOOPMaxMet"] != null)
                        component.set("v.formattedIndOOPMaxMet" , "$" + Number(accountBIDetails["indOOPMaxMet"]).toFixed(2));
                    
                    
                    //Family Deductible formatting with $
                    component.set("v.famDeductible" , Number(accountBIDetails["famDeductible"]));
                    if(accountBIDetails["famDeductible"] != null)
                        component.set("v.formattedFamDeductible" , "$" + Number(accountBIDetails["famDeductible"]).toFixed(2));
                    
                    //Family Deductible Met formatting with $
                    component.set("v.famDeductibleMet" , Number(accountBIDetails["famDeductibleMet"]));
                    if(accountBIDetails["famDeductibleMet"] != null)
                        component.set("v.formattedFamDeductibleMet" , "$" + Number(accountBIDetails["famDeductibleMet"]).toFixed(2));
                    
                    //Family OOP Max formatting with $
                    component.set("v.famOOPMax" , Number(accountBIDetails["famOOPMax"]));
                    if(accountBIDetails["famOOPMax"] != null)
                        component.set("v.formattedFamOOPMax" , "$" + Number(accountBIDetails["famOOPMax"]).toFixed(2));
                    
                    //Family OOP Max Met formatting with $
                    component.set("v.famOOPMaxMet" , Number(accountBIDetails["famOOPMaxMet"]));
                    if(accountBIDetails["famOOPMaxMet"] != null)
                        component.set("v.formattedFamOOPMaxMet" , "$" + Number(accountBIDetails["famOOPMaxMet"]).toFixed(2));
                    
                    //Get alll the G5 product price values
                    component.set("v.priceBook" , accountBIDetails["priceBook"]);
                    var priceBookExists = accountBIDetails["priceBook"] != null ? "true":"false";
                    component.set("v.priceBookExists", priceBookExists);
                    //alert('pB :: ' + component.get("v.priceBookExists"));
                    //Get the receiver price
                    component.set("v.receiverPrice" , Number(accountBIDetails["receiverPrice"]));
                    if(accountBIDetails["receiverPrice"] != null)
                        component.set("v.formattedReceiverPrice" , "$" + Number(accountBIDetails["receiverPrice"]).toFixed(2));
                    //alert('receiver :: ' + component.get("v.receiverPrice"));
                    //Get the transmitter price	
                    component.set("v.transmitterPrice" , Number(accountBIDetails["transmitterPrice"]));
                    if(accountBIDetails["transmitterPrice"] != null)
                        component.set("v.formattedTransmitterPrice" , "$" + Number(accountBIDetails["transmitterPrice"]).toFixed(2));
                    
                    //Get the sensor price	
                    component.set("v.sensorPrice" , Number(accountBIDetails["sensorPrice"]));
                    if(accountBIDetails["sensorPrice"] != null)
                        component.set("v.formattedSensorPrice" , "$" + Number(accountBIDetails["sensorPrice"]).toFixed(2));
					
					//Get the Medicare Receiver Price
					component.set("v.medicareReceiverPrice" , Number(accountBIDetails["medicareReceiverPrice"]));
                    if(accountBIDetails["medicareReceiverPrice"] != null)
                        component.set("v.formattedMedicareReceiverPrice" , "$" + Number(accountBIDetails["medicareReceiverPrice"]).toFixed(2));
					
					//Get the Medicare Receiver Price
					component.set("v.medicareSubscriptionPrice" , Number(accountBIDetails["medicareSubscriptionPrice"]));
                    if(accountBIDetails["medicareSubscriptionPrice"] != null)
                        component.set("v.formattedMedicareSubscriptionPrice" , "$" + Number(accountBIDetails["medicareSubscriptionPrice"]).toFixed(2));
					
                    //K Code
                    //alert('Kcode>>' + accountBIDetails["kCodeAccount"]);
					component.set("v.bundlePrice" , Number(accountBIDetails["bundlePrice"]));
                    if(accountBIDetails["kCodeAccount"] == "TRUE"){
                        component.set("v.kCodeAccount" , "true");
                        component.set("v.formattedBundlePrice" , "$" + Number(accountBIDetails["bundlePrice"]).toFixed(2));
                    //alert('BundlePrice> ' + component.get("v.formattedBundlePrice"));
                    }else
                        component.set("v.kCodeAccount" , "false");
                    var shipDuration = component.get("v.shipmentDuration");
                    //alert('shipDuration> ' + shipDuration);
                    if(component.get("v.shipmentDuration") === "NULL" || 
                       	(shipDuration != null && shipDuration.length === 0))
						component.set("v.shipmentDuration" , accountBIDetails["shipmentDuration"]);
                    
					//alert('Kcode compo>>' + component.get("v.kCodeAccount"));
                    
                    //Set warning message if last benefit check is not valid
                    if(component.get("v.canShowBICalculator") == "true")
                        component.set("v.customWarningMessage" , "BI Calculator cannot be displayed for any closed or cancelled Opportunity");
                    else if(component.get("v.isLastBICheckDateValid") == "false"){
                        //QUADAX Payor Code to define error message
                        var biCheckDoneBefore = accountBIDetails["biCheckOlderThan"];                        
                        component.set("v.customWarningMessage" , "'Last Benefits Check Date' " + accountBIDetails["lastBICheckDate"] + " is more than " + biCheckDoneBefore);
                    }else if(component.get("v.isLastBICheckDateValid") == "NULL")
                            component.set("v.customWarningMessage" , "'Last Benefits Check Date' does not exist. 'Last Benefits Check Date' is required for BI Calculator to show up in this section.");
                    
                    //Calculate Individual Remaining Deductible
                    if(!isNaN(component.get("v.indDeductible")) && !isNaN(component.get("v.indDeductibleMet")))
                    {
                        if(accountBIDetails["indDeductible"] == "0")
                            component.set("v.indRemainingDeductible" , "$0.00");
                        else
                        {
                            var indRemainingDeductible = component.get("v.indDeductible") - component.get("v.indDeductibleMet");
                            var result = indRemainingDeductible.toFixed(2)
                            component.set("v.indRemainingDeductible" , "$" + result); 
                        }
                    }
                    //Calculate Individual OOP Max
                    if(!isNaN(component.get("v.indOOPMax")) && !isNaN(component.get("v.indOOPMaxMet")))
                    {
                        if(accountBIDetails["indOOPMax"] == "0")
                            component.set("v.indRemainingOOPMax" , "$0.00");
                        else
                        {
                            var indRemainingOOPMax = component.get("v.indOOPMax") - component.get("v.indOOPMaxMet");
                            var result = indRemainingOOPMax.toFixed(2)
                            component.set("v.indRemainingOOPMax" , "$" + result); 
                        }
                    }
                    //Calculate Family Remaining Deductible 
                    if(!isNaN(component.get("v.famDeductible")) && !isNaN(component.get("v.famDeductibleMet")))
                    {
                        if(accountBIDetails["famDeductible"] == "0")
                            component.set("v.famRemainingDeductible" , "$0.00");
                        else
                        {
                            var famRemainingDeductible = component.get("v.famDeductible") - component.get("v.famDeductibleMet");
                            var result = famRemainingDeductible.toFixed(2)
                            component.set("v.famRemainingDeductible" , "$" + result); 
                        }
                    }
                    //Calculate Family OOP Max
                    if(!isNaN(component.get("v.famOOPMax")) && !isNaN(component.get("v.famOOPMaxMet")))
                    {
                        if(accountBIDetails["famOOPMax"] == "0")
                            component.set("v.famRemainingOOPMax" , "$0.00");
                        else
                        {
                            var famRemainingOOPMax = component.get("v.famOOPMax") - component.get("v.famOOPMaxMet");
                            var result = famRemainingOOPMax.toFixed(2)
                            component.set("v.famRemainingOOPMax" , "$" + result); 
                        }
                    }
                    
                 //Call function to calculate the Estimated Costs for Medicare customer or commerical customer
                    if(component.get("v.customerType") == 'Medicare FFS' || (component.get("v.customerType") == 'Medicare Advantage' && component.get("v.customerPayorCode") == 'K')) 
					{
						this.calculateMedicareEstimatedCosts(component);
					}
					else
						//this.calculateEstimatedCosts(component);
                    	this.calculateEstimatedCopay(component);
                    	
                }
                
            }
            else {
                console.log('Problem getting main topic values, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    },
    
    calculateEstimatedCopay : function(component) {
        var kCodeAccount = component.get("v.kCodeAccount");
		//var bundleQty = component.get("v.bundleQty");  
        var bundleQty = kCodeAccount === 'true' ? 1 : 0;       
		var bundlePrice = component.get("v.bundlePrice");
		
        var coverage = component.get("v.coverage");
        var copayDOS = component.get("v.copayDOS");
        var copayLineItem = component.get("v.copayLineItem");
        var receiverQty = component.get("v.receiverQty");
        var transmitterQty = kCodeAccount === 'false' ? component.get("v.transmitterQty") : 0;
        var sensorQty = kCodeAccount === 'false' ? component.get("v.sensorQty") : 0;
        var totalQty = (+receiverQty > 0 ? 1 : 0) + (+transmitterQty > 0 ? 1 : 0) + (+sensorQty > 0 ? 1 : 0) + bundleQty;
        var receiverPrice = component.get("v.receiverPrice");
        var transmitterPrice = component.get("v.transmitterPrice");
        var sensorPrice = component.get("v.sensorPrice");
        var indDeductible = component.get("v.indDeductible");
        var indDeductibleMet = component.get("v.indDeductibleMet");
        var indOOPMax = component.get("v.indOOPMax");
        var indOOPMaxMet = component.get("v.indOOPMaxMet");
        var famDeductible = component.get("v.famDeductible");
        var famDeductibleMet = component.get("v.famDeductibleMet");
        var famOOPMax = component.get("v.famOOPMax");
        var famOOPMaxMet = component.get("v.famOOPMaxMet");
        var ZeroValues = component.get("v.ZeroValues");
        var estimatedCosts = 0;
        
    	var totalReceiverPrice = isNaN(receiverPrice) ? 0 : receiverQty*receiverPrice;
        var totalTransmitterPrice = isNaN(transmitterPrice) ? 0 : transmitterQty*transmitterPrice;
        var totalSensorPrice = isNaN(sensorPrice) ? 0 : sensorQty*sensorPrice;
		var totalBundlePrice = isNaN(bundlePrice) ? 0 : bundleQty*bundlePrice;
        var totalPrice = (totalReceiverPrice + totalTransmitterPrice + totalSensorPrice + totalBundlePrice);
        console.log('**** TPS:EH calculateEstimatedCopay totalPrice=' + totalPrice);
        estimatedCosts=totalPrice;
        
        //Call CoPay method       
        if(totalPrice>0){
            console.log('****	TPS:EH getEstimatedCoPay accountId=' + component.get("v.accountId") + ' totalAmount=' + totalPrice);
            var action = component.get("c.getEstimatedCoPay");
        	action.setParams({
            	"accountId": component.get("v.accountId"),
            	"totalAmount": totalPrice
        	});
            action.setCallback (this, function(response){
                var state = response.getState();
                if(component.isValid() && state == "SUCCESS")
                {
                    var coPay=0;
                    coPay= response.getReturnValue();
                    console.log('****	TPS:EH getEstimatedCoPay coPay=' + coPay);
                    component.set("v.estimatedCosts", "$" + Number(coPay).toFixed(2));
                }
        	});
        	$A.enqueueAction(action);
        }  
    },
    
    calculateEstimatedCosts : function(component) {
		var kCodeAccount = component.get("v.kCodeAccount");
		//var bundleQty = component.get("v.bundleQty");  
        var bundleQty = kCodeAccount === 'true' ? 1 : 0;       
		var bundlePrice = component.get("v.bundlePrice");
		
        var coverage = component.get("v.coverage");
        var copayDOS = component.get("v.copayDOS");
        var copayLineItem = component.get("v.copayLineItem");
        var receiverQty = component.get("v.receiverQty");
        var transmitterQty = kCodeAccount === 'false' ? component.get("v.transmitterQty") : 0;
        var sensorQty = kCodeAccount === 'false' ? component.get("v.sensorQty") : 0;
        var totalQty = (+receiverQty > 0 ? 1 : 0) + (+transmitterQty > 0 ? 1 : 0) + (+sensorQty > 0 ? 1 : 0) + bundleQty;
        var receiverPrice = component.get("v.receiverPrice");
        var transmitterPrice = component.get("v.transmitterPrice");
        var sensorPrice = component.get("v.sensorPrice");
        var indDeductible = component.get("v.indDeductible");
        var indDeductibleMet = component.get("v.indDeductibleMet");
        var indOOPMax = component.get("v.indOOPMax");
        var indOOPMaxMet = component.get("v.indOOPMaxMet");
        var famDeductible = component.get("v.famDeductible");
        var famDeductibleMet = component.get("v.famDeductibleMet");
        var famOOPMax = component.get("v.famOOPMax");
        var famOOPMaxMet = component.get("v.famOOPMaxMet");
        var ZeroValues = component.get("v.ZeroValues");
        var estimatedCosts = 0;
        
        var totalReceiverPrice = isNaN(receiverPrice) ? 0 : receiverQty*receiverPrice;
        var totalTransmitterPrice = isNaN(transmitterPrice) ? 0 : transmitterQty*transmitterPrice;
        var totalSensorPrice = isNaN(sensorPrice) ? 0 : sensorQty*sensorPrice;
		var totalBundlePrice = isNaN(bundlePrice) ? 0 : bundleQty*bundlePrice;
        
        var patientCoverage = isNaN(coverage) ? 0 : (100 - coverage);
        patientCoverage = coverage == 0 ? 0 : patientCoverage;
        
        estimatedCosts = isNaN(copayDOS) ? estimatedCosts : (estimatedCosts + copayDOS);
        estimatedCosts = isNaN(copayLineItem) ? estimatedCosts : estimatedCosts + (totalQty*copayLineItem);
        
        if((!isNaN(indDeductible) && !isNaN(indDeductibleMet) || !isNaN(famDeductible) && !isNaN(famDeductibleMet)))
        {
            var estimatedCostsViaIndDeductible = 0;
            var estimatedCostsViaFamDeductible = 0;
            var totalPrice = (totalReceiverPrice + totalTransmitterPrice + totalSensorPrice + totalBundlePrice);
            
            //Calculate with Individual Deductible
            if(indDeductible > 0)
            {
                var indRemainingDeductible = (indDeductible - indDeductibleMet);
                //estimatedCosts = estimatedCosts + (indRemainingDeductible + ((totalReceiverPrice + totalTransmitterPrice + totalSensorPrice)-indRemainingDeductible)*(patientCoverage/100));
                estimatedCostsViaIndDeductible = (indRemainingDeductible + ((totalPrice)-indRemainingDeductible)*(patientCoverage/100));
            }
            
            //Calculate with Family Deductible
            if(famDeductible > 0)
            {
                var famRemainingDeductible = (famDeductible - famDeductibleMet);
                //estimatedCosts = estimatedCosts + (famRemainingDeductible + ((totalReceiverPrice + totalTransmitterPrice + totalSensorPrice)-famRemainingDeductible)*(patientCoverage/100));
                estimatedCostsViaFamDeductible = (famRemainingDeductible + ((totalPrice)-famRemainingDeductible)*(patientCoverage/100));
            }
            
            //Comare family deductibles and individual deductibles and chose which ever is less
            if(famDeductible > 0 && indDeductible > 0)
            {
                if(estimatedCostsViaFamDeductible <= estimatedCostsViaIndDeductible)
                    estimatedCosts = estimatedCosts + estimatedCostsViaFamDeductible;
                else
                    estimatedCosts = estimatedCosts + estimatedCostsViaIndDeductible;                    
                
            }else
                if(indDeductible > 0 && famDeductible == 0)
                {
                    estimatedCosts = estimatedCosts + estimatedCostsViaIndDeductible;
                }else
                    if(famDeductible > 0 && indDeductible == 0)
                    {
                        estimatedCosts = estimatedCosts + estimatedCostsViaFamDeductible;
                    }else
                        if(indDeductible == 0 && famDeductible == 0)
                        {
                            estimatedCosts = estimatedCosts + totalPrice*(patientCoverage/100);
                        }
        }
        if((!isNaN(indOOPMax) && !isNaN(indOOPMaxMet) || !isNaN(famOOPMax) && !isNaN(famOOPMaxMet)))
        {
            var indOOPMaxRemainingEstimatedCosts = -1;
            var famOOPMaxRemainingEstimatedCosts = -1;
            
            if(indOOPMax > 0 && indOOPMaxMet >=0)
            {
                var indOOPMaxRemaining = (indOOPMax - indOOPMaxMet);
                if(estimatedCosts > indOOPMaxRemaining)
                    indOOPMaxRemainingEstimatedCosts = indOOPMaxRemaining;
            }
            if(famOOPMax > 0 && famOOPMaxMet >=0)
            {
                var famOOPMaxRemaining = (famOOPMax - famOOPMaxMet);
                if(estimatedCosts > famOOPMaxRemaining)
                    famOOPMaxRemainingEstimatedCosts = famOOPMaxRemaining;
            }
            //Choose the OOP Max, which ever is less
            if(indOOPMaxRemainingEstimatedCosts > 0 && famOOPMaxRemainingEstimatedCosts > 0)
                estimatedCosts = (famOOPMaxRemainingEstimatedCosts <= indOOPMaxRemainingEstimatedCosts) ? famOOPMaxRemainingEstimatedCosts : indOOPMaxRemainingEstimatedCosts;
            else if(indOOPMaxRemainingEstimatedCosts > 0)
                estimatedCosts = indOOPMaxRemainingEstimatedCosts;
                else if(famOOPMaxRemainingEstimatedCosts > 0)
                    estimatedCosts = famOOPMaxRemainingEstimatedCosts;
                    else if(indOOPMaxRemainingEstimatedCosts == 0 || famOOPMaxRemainingEstimatedCosts == 0)
                        estimatedCosts = 0;
        }
        estimatedCosts = (totalPrice < estimatedCosts) ? totalPrice : estimatedCosts;
        
        
        if(ZeroValues = 0)
        { 
         component.set("v.estimatedCosts", "$0.00");
        }
        else
		{
         component.set("v.estimatedCosts", "$" + estimatedCosts.toFixed(2));
        }        
    },
	calculateMedicareEstimatedCosts : function(component) {
		var coverage = component.get("v.coverage");
		var receiverPrice = component.get("v.medicareReceiverPrice");
        var subscriptionPrice = component.get("v.medicareSubscriptionPrice");
		var indDeductible = component.get("v.indDeductible");
        var indDeductibleMet = component.get("v.indDeductibleMet");
		var startupCost = 0;
		var receiverCost = 0;
		var ongoingCost = 0;
        var totalReceiverPrice = isNaN(receiverPrice) ? 0 : 1*receiverPrice;
        var totalSubscriptionPrice = isNaN(subscriptionPrice) ? 0 : 1*subscriptionPrice;
		var patientCoverage = isNaN(coverage) ? 0 : (100 - coverage);
        patientCoverage = coverage == 0 ? 0 : patientCoverage;
		if((!isNaN(indDeductible) && !isNaN(indDeductibleMet)))
        {
            var totalPrice = (totalReceiverPrice + totalSubscriptionPrice);//This is to include the Receiver cost and 1 time the subscription cost
            var indRemainingDeductible = (indDeductible - indDeductibleMet);
			startupCost = (indRemainingDeductible + ((totalPrice)-indRemainingDeductible)*(patientCoverage/100));
			startupCost = (totalPrice < startupCost) ? totalPrice : startupCost;
			receiverCost = (indRemainingDeductible + ((totalReceiverPrice)-indRemainingDeductible)*(patientCoverage/100));
			receiverCost = (totalReceiverPrice < receiverCost) ? totalReceiverPrice : receiverCost;
			ongoingCost = (totalSubscriptionPrice) * (patientCoverage/100);
			component.set("v.medicareStartupCost", "$" + startupCost.toFixed(2));
			component.set("v.medicareReceiverCost", "$" + receiverCost.toFixed(2));
			component.set("v.medicareOngoingCost", "$" + ongoingCost.toFixed(2));
			
		}	
	},
    createNewQuoteTask: function(component) {
        var action = component.get("c.createQuoteTask");
        var newTask = component.get("v.newTask");
        var comments = "QUOTE GIVEN TO CUSTOMER\r";
        comments = comments +  ((component.get("v.priorAuthRequired")) == null ? "" : ("Prior Auth Required : " + component.get("v.priorAuthRequired") + "\r"));
        comments = comments +  ((component.get("v.lastBICheckDate")) == null ? "" : ("Last BI Check Date : " + component.get("v.lastBICheckDate") + "\r"));
        comments = comments +  (isNaN(component.get("v.coverage")) ? "" : ("Coverage : " + component.get("v.coverage") + "%\r"));
        comments = comments +  (isNaN(component.get("v.copayDOS")) ? "" : ("CopayDOS : $" + component.get("v.copayDOS") + "\r"));
        comments = comments +  (isNaN(component.get("v.copayLineItem")) ? "" : ("Copay Line Item : $" + component.get("v.copayLineItem") + "\r"));
        comments = comments +  (isNaN(component.get("v.receiverQty")) ? "" : ("Receiver Quantity : " + component.get("v.receiverQty") + "\r"));
        comments = comments +  (isNaN(component.get("v.transmitterQty")) ? "" : ("Transmitter Quantity : " + component.get("v.transmitterQty") + "\r"));
        comments = comments +  (isNaN(component.get("v.sensorQty")) ? "" : ("Sensor Quantity : " + component.get("v.sensorQty") + "\r"));
        comments = comments +  ((component.get("v.priceBook")) == null ? "" : ("Price Book : " + component.get("v.priceBook") + "\r"));
        comments = comments +  (isNaN(component.get("v.receiverPrice")) ? "" : ("Receiver Price : $" + component.get("v.receiverPrice") + "\r"));
        comments = comments +  (isNaN(component.get("v.transmitterPrice")) ? "" : ("Transmitter Price : $" + component.get("v.transmitterPrice") + "\r"));
        comments = comments +  (isNaN(component.get("v.sensorPrice")) ? "" : ("Sensor Price : $" + component.get("v.sensorPrice") + "\r"));
        comments = comments +  (isNaN(component.get("v.indDeductible")) ? "" : ("Individual Deductible : $" + component.get("v.indDeductible") + "\r"));
        comments = comments +  (isNaN(component.get("v.indDeductibleMet")) ? "" : ("Individual Deductible Met : $" + component.get("v.indDeductibleMet") + "\r"));
        comments = comments +  (isNaN(component.get("v.indOOPMax")) ? "" : ("Individual OOP Max : $" + component.get("v.indOOPMax") + "\r"));
        comments = comments +  (isNaN(component.get("v.indOOPMaxMet")) ? "" : ("Individual OOP Max Met : $" + component.get("v.indOOPMaxMet") + "\r"));
        comments = comments +  (isNaN(component.get("v.famDeductible")) ? "" : ("Family Deductible : $" + component.get("v.famDeductible") + "\r"));
        comments = comments +  (isNaN(component.get("v.famDeductibleMet")) ? "" : ("Family Deductible Met : $" + component.get("v.famDeductibleMet") + "\r"));
        comments = comments +  (isNaN(component.get("v.famOOPMax")) ? "" : ("Family OOP Max : $" + component.get("v.famOOPMax") + "\r"));
        comments = comments +  (isNaN(component.get("v.famOOPMaxMet")) ? "" : ("Family OOP Max Met : $" + component.get("v.famOOPMaxMet") + "\r"));
        comments = comments +  (isNaN(component.get("v.famOOPMaxMet")) ? "" : ("Family OOP Max Met : $" + component.get("v.famOOPMaxMet") + "\r"));
        comments = comments + "Estimated Costs through Insurance : " + component.get("v.estimatedCosts");
        console.log('**** component.get("v.opptyId")=' + component.get("v.opptyId") + '  component.get("v.accountId")=' +  component.get("v.accountId"));
        action.setParams({
            //"opptyId": component.get("v.opptyId"),
            "opptyId": (component.get("v.opptyId")!=null ? component.get("v.opptyId") : component.get("v.accountId")),
            
            "comments": comments
        });
        // Configure the response handler for the action
        action.setCallback(this, function(response) {	
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.showQuoteButton", "false");
                let strQuoteMsg=(component.get("v.opptyId")!=null ? "Quote has been Saved, please refresh this Opportunity record to create another Quote" 
                                 : "Quote has been Saved, please refresh this Account record to create another Quote");
                component.set("v.customWarningMessage", strQuoteMsg);
            }
        });
        // Send the request to create the new event
        $A.enqueueAction(action);
    },
	createNewMedicareQuoteTask: function(component) {
        var action = component.get("c.createQuoteTask");
        var newTask = component.get("v.newTask");
        var comments = "QUOTE GIVEN TO CUSTOMER\r";
        comments = comments +  ((component.get("v.lastBICheckDate")) == null ? "" : ("Last BI Check Date : " + component.get("v.lastBICheckDate") + "\r"));
		comments = comments +  (isNaN(component.get("v.coverage")) ? "" : ("Coverage : " + component.get("v.coverage") + "%\r"));
		comments = comments +  ((component.get("v.priceBook")) == null ? "" : ("Price Book : " + component.get("v.priceBook") + "\r"));
		comments = comments +  (isNaN(component.get("v.medicareReceiverPrice")) ? "" : ("Receiver Price : $" + component.get("v.medicareReceiverPrice") + "\r"));
		comments = comments +  (isNaN(component.get("v.medicareSubscriptionPrice")) ? "" : ("Subscription Price : $" + component.get("v.medicareSubscriptionPrice") + "\r"));
		comments = comments +  (isNaN(component.get("v.indDeductible")) ? "" : ("Individual Deductible : $" + component.get("v.indDeductible") + "\r"));
        comments = comments +  (isNaN(component.get("v.indDeductibleMet")) ? "" : ("Individual Deductible Met : $" + component.get("v.indDeductibleMet") + "\r"));
		comments = comments + "Estimated Startup Cost through Insurance : " + component.get("v.medicareStartupCost");
        action.setParams({
            "opptyId": component.get("v.opptyId"),
            "comments": comments
        });
        // Configure the response handler for the action
        action.setCallback(this, function(response) {	
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.showQuoteButton", "false");
                component.set("v.customWarningMessage", "Quote has been Saved, please refresh this Opportunity record to create another Quote");
            }
        });
        // Send the request to create the new event
        $A.enqueueAction(action);
    }
})