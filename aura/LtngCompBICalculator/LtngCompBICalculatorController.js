({
    doInit : function(component, event, helper) {
        helper.loadAccountDetails(component);
    },
    displayBenefitBasedOnGeneration : function(component, event, helper) {
        helper.loadAccountDetails(component);
    },
    displayBenefitBasedOnShipmentDuration : function(component, event, helper) {
        helper.loadAccountDetails(component);
    },
    calculateEstimatedCosts_x : function(component, event, helper) {
        //helper.calculateEstimatedCosts(component);
        helper.calculateEstimatedCopay(component);
    },
    calculateEstimatedCopay : function(component, event, helper) {        
        helper.calculateEstimatedCopay(component);
    },
    createNewQuoteTask : function(component, event, helper){
        helper.createNewQuoteTask(component);
    },
	createNewMedicareQuoteTask : function(component, event, helper){
        helper.createNewMedicareQuoteTask(component);
    }
})