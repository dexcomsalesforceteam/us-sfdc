<!--
@Description - This app will be called by CompProductRequestApproval
This app hosts LtngCompProductCompRequestApproval component
@Created Date - 30 Sep 2019
@Author - Tanay Kulkarni LTI
-->
<aura:application access="GLOBAL" extends="ltng:outApp" >
	<aura:dependency resource="c:LtngCompProductCompRequestApproval"/>
</aura:application>