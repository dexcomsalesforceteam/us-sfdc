({
    // adapts view depending on device and whether user is DBM or TBM, 
    // gets data related to running user and populates component metrics, accordion and datatables
	init : function(component, event, helper) {
        helper.startSessionTimer(component, helper);
        component.set("v.isLoadingData", true);
        if($A.get("$Browser.formFactor") != 'DESKTOP'){
            component.set("v.displayTiles", false);
            $A.util.addClass('prescriber-data', 'slds-show'); 
        }

		helper.getUserData(component, helper).then(function(territory) {
            if(component.get("v.isDBM") === 'false'){
                helper.setTerritory(component, helper, territory);
            }
        });
        component.find('dateRangeCombobox').set("v.value", 'past-3-months');
	},
    
    // reloads new TBM data based on selected territory (only available to DBMs)
    changeTerritory : function(component, event, helper){
        helper.resetSession(component, helper);
        component.set("v.isLoadingData", true);
        var territoryIndex = event.getParam("value");
        
        if (!isNaN(territoryIndex)){
            territoryIndex = parseInt(territoryIndex);
            var territories = component.get("v.territories");
            var territory = territories[territoryIndex];
            helper.setTerritory(component, helper, territory);
        }
    },
    
    handleSearchButton : function(component, event, helper){
        helper.resetSession(component, helper);
        helper.handleSearch(component, helper, false);
    },
    
    handleSearchChange : function(component, event, helper){
        if(component.find('searchbar').get("v.value") == ''){
            component.find('filterCombobox').set("v.value", 'all-orders');
            helper.removeFilterBackgrounds(component);
            $A.util.addClass(component.find('all-orders'), 'current-filter');
            component.set("v.tableData", component.get("v.allData"));
        }
        
    },
    
    // displays corresponding subset of data when user clicks on dropdown filter menu or metric tiles
    handleFilter : function(component, event, helper){
        helper.resetSession(component, helper);
        //component.set("v.isLoadingMetric", true);
        
        helper.handleFilter(component, event, helper);
    },
    
    // row sorting in every datatable
    sortRows : function(component, event, helper){
        helper.resetSession(component, helper);
        helper.sortRows(component, event, helper);
    },
    
    // triggers partner community user creation for an unregistered prescriber
    handleSendInvite : function(component, event, helper){
        helper.resetSession(component, helper);
        if(event.getSource().getLocalId() == 'cancelSendInvite'){
            component.set("v.displaySendInviteModal", false);
        }
        else if(event.getSource().getLocalId() == 'sendInvite'){
            helper.sendInvite(component, event);
            /*.then(
                function(newContactId){
                    console.log('newContactId: ' + newContactId);
                    helper.getNewUser(component, newContactId, component.get("v.prescriberId"));
                }
            );*/
        }
    },
    
    // displays and hides modal with definition of metrics
    handleInfoModal : function(component, event, helper){
        helper.resetSession(component, helper);
        var action = event.getSource().getLocalId();
        if(action == "openMetricsModal"){
            component.set("v.displayMetricsModal", true);
        }
        else if(action == "closeMetricsModal"){
            component.set("v.displayMetricsModal", false);
        }
        else if(action == "openStatusesModal"){
            component.set("v.displayStatusesModal", true);
        }
        else if(action == "closeStatusesModal"){
            component.set("v.displayStatusesModal", false);
        }
        
    },
    
    // stores state of open/collapsed accordion sections
    saveSectionToggle : function(component, event, helper){
        var openSections = event.getParam('openSections');
        if(component.get("v.isSearching") == false){
            component.set("v.expandedPrescribers", openSections);
            var tableData = component.get("v.tableData");
            tableData.forEach(
                function(prescriber){
                    if(openSections.includes(prescriber.account.Id)){
                        prescriber.isExpanded = true;
                    } 
                    else{
                        prescriber.isExpanded = false;
                    }
                }
            );
            component.set("v.tableData", tableData);
        }
        component.find('expandToggle').set("v.checked", (openSections.length == tableData.length) ? true : false);
    },
    
    // handles click events on modals for 'Invite Sent' and 'Registered' prescribers
    handlePrescriberAction : function(component, event, helper){
        helper.resetSession(component, helper);
        var action = event.getSource().getLocalId();
        component.set("v.editingPrescriberInfo", false);
        component.set("v.isPasswordReset", false);
        component.set("v.displayNewUserMessage", false);
        component.set("v.isEmailRequired", false);
        component.set("v.isInviteSentActionsDisabled", false);
        component.set("v.isInviteDisabled", false);
        component.set("v.prescriberId", null);
        component.set("v.prescriberUsername", null);
        component.set("v.presciberEmail", null);
        component.set("v.prescriberName", null);
        component.set("v.lastLoginDate", null);
        component.set("v.inviteSentDate", null);
        component.set("v.emailOnFile", null);
        component.set("v.suggestedUsername", null);
        
        
        // set the prescriber name, username, email, last login date, invite sent date, suggested username, email on file fields
        if(action == 'openRegisteredModal'){
            component.set("v.displayRegisteredModal", true);
            helper.setPrescriberInfo(component, event);
        }
        else if(action == 'openInviteSentModal'){
            component.set("v.displayInviteSentModal", true);
            helper.setPrescriberInfo(component, event);
        }
        else if(action == 'openSendInviteModal'){
            component.set("v.displaySendInviteModal", true);
            helper.setPrescriberInfo(component, event);
        }
        else if(action == 'closeRegisteredModal') {
            component.set("v.displayRegisteredModal", false);
        }
        else if(action == 'closeInviteSentModal'){
            component.set("v.displayInviteSentModal", false);
        }
    },
    
    // changes prescriber modal view to editable
    editUserInfo : function(component, event, helper){
        if(event.getSource().getLocalId().includes('edit')){
            component.set("v.editingPrescriberInfo", true);
        }
        else if(event.getSource().getLocalId().includes('cancel')){
            component.set("v.editingPrescriberInfo", false);
        }
        else if(event.getSource().getLocalId().includes('save')){
            helper.saveUserInfo(component, event);
        }
    },
    
    // makes call to server to verify the uniqueness of a new username
    checkUsernameIsUnique : function(component, event, helper){
        if(event.getSource().get("v.value").includes('@dexcomcommunity.com')){
            helper.checkUsernameIsUnique(component, event);
        }
    },
    
    // makes call to server to reset password for prescriber user
    resetUserPassword : function(component, event, helper){
        helper.resetUserPassword(component, event);
    },
    
    // controls user access to 'Send Invite' button on prescriber modal to enforce all necessary information is filled 
    enableSendInvite : function(component, event, helper){
        if(event.getSource().get("v.validity").valid){
            component.set("v.isInviteDisabled", false);
        }
    },
    
    handleExpandToggle : function(component, event, helper){
        helper.resetSession(component, helper);
        component.set("v.isLoadingData", true);
        if(event.getSource().get("v.checked")){
            var tableData = component.get("v.tableData");
            var accountIds = [];
            tableData.forEach(function(prescriber){
                accountIds.push(prescriber.account.Id);
            });
            component.set("v.expandedPrescribers", accountIds);
        }
        else{
            component.set("v.expandedPrescribers", []);
        }
        component.set("v.isLoadingData", false);
        
    },
    
    logOutUser : function(component, event, helper){
            helper.logOutUser();
        },

    closeLogOutModal : function(component, event, helper){
            helper.resetSession(component, helper);
    },
    
    handleDateChange : function(component, event, helper){
        helper.resetSession(component, helper);
        component.set("v.isLoadingData", true);
        // metrics should get reset
        helper.removeFilterBackgrounds(component);
        component.find('filterCombobox').set("v.value", 'all-orders');
        $A.util.addClass(component.find('all-orders'), 'current-filter');
        // searchbox is reset
        component.set("v.noResultsFound", false);
        component.find('searchbar').set("v.value", '');
        // set expanded toggle to false
        component.find('expandToggle').set("v.checked", false);
        // retrieve data in the date range
        var dateRange = component.find('dateRangeCombobox').get("v.value");
        helper.populateData(component, helper, dateRange);
    }
})