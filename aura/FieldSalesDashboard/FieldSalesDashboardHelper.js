({

    sessionManager : {
            sessionMinutes : 600000,
            warningMinutes : 30000
        },

    startSessionTimer : function(component, helper){
            helper.sessionManager.sessionTimer = window.setTimeout(
                $A.getCallback(
                    function(){
                       helper.startLogoutTimer(component, helper);
                    }
                ),
                helper.sessionManager.sessionMinutes
            );
        },

        startLogoutTimer : function(component, helper){
            component.set("v.displayLogoutModal", true);
            helper.startLogoutCountdown(component, helper);
            helper.sessionManager.logoutTimer = window.setTimeout(
                    $A.getCallback(
                        function(){
                            helper.logOutUser();
                        }
                    ),
                    helper.sessionManager.warningMinutes
                );
        },

        startLogoutCountdown : function(component, helper){
            var timePast = 0;
            helper.sessionManager.countdown = setInterval(function(){
                var timeLeft = (helper.sessionManager.warningMinutes - timePast)/1000;
                component.set("v.secondsLeft", timeLeft);
                timePast += 1000;
                console.log(timeLeft);
            },
            1000);
        },

        resetSession : function(component, helper){
            component.set("v.displayLogoutModal", false);
            component.set("v.secondsLeft", 30);
            clearTimeout(helper.sessionManager.sessionTimer);
            clearTimeout(helper.sessionManager.logoutTimer);
            clearInterval(helper.sessionManager.countdown);
            helper.registerActivityAtServerLevel(component);
            helper.startSessionTimer(component, helper);
        },

        logOutUser : function(){
                window.location.replace(window.location.href + "login/");
        },

        registerActivityAtServerLevel : function(component){
            return ltngUtils.callApexPromise(component.get("c.resetSessionTimer"), {

            }).then(
                function(result){
                }
            );
        },


    // makes call to server to obtain user information (TBM or DBM) and related prescribers
	getUserData : function(component, helper) {
        console.log('getUserData');
        component.set("v.isLoadingData", true);
        return ltngUtils.callApexPromise(component.get("c.getUserTerritories"), {
            // no parameters needed
        }).then(
            function(territories){
                console.log(territories);
                if (territories != null){
                    if (territories.length > 1){
                        component.set("v.isDBM", true);
                    }
                    var options = [];
                    /*{
                        label : 'Select...',
                        value : null
                    }*/
                    var territory = null;
                    for(var i = 0; territories.length > i; i++ ){
                        var t = territories[i];
                        if (typeof(t.TBM__r) == 'undefined') {
                        	t['TBM__r'] = {};
                            t.TBM__r['Name'] = 'TBM Not Set';
                        }
                        if (i == 0) {
                            territory = t;
                        }
                        options.push({label : t.Name + ' - ' + t.TBM__r.Name,
                                      value :  ''+ i+''});
                    }
                    if(component.find("territories-combobox")){
                        //component.find("territories-combobox").set("v.value", "0");
                    }
                    component.set("v.territories", territories);
                    component.set("v.territoryOptions", options);
                    component.find('dateRangeCombobox').set("v.value", 'past-3-months');
                    if(component.get("v.isDBM")){
                        component.set("v.isLoadingData", false);
                    }
                    console.log('done');
                    return territory;
                }
                
            }
        ).catch(
            function(error){
                component.set("v.isLoadingData", false);
            }
        );
	},
    
    // makes call to server to get related opportunities and order headers to the prescribers of the selected territory
    setTerritory : function(component, helper, territory) {
        console.log('setTerritory');
        component.set("v.isLoadingData", true);
        component.set("v.currentTerritoryId", territory.Id);
        component.set("v.salesRepName", territory.TBM__r.Name);
        component.set("v.salesRepNumber", territory.Name);
        component.find('expandToggle').set("v.checked", false);
        helper.populateData(component, helper, 'past-3-months');
        
        /*ltngUtils.callApexPromise(component.get("c.getOpportunitiesAndOrders"),
                                  {territoryId: territory.Id,
                                   dateRange: 'past-3-months'}
                                 ).then(
            function(prescribers){
                if (prescribers != null){
                    component.set("v.prescribersInTerritoryMetric", prescribers.length);
                    console.log('prescribers returned: ' + prescribers.length);
                    var formattedOpps = [];
                    var orderHeaderMap = null;
                    for (var i = 0; i < prescribers.length; i++) {
                        var prescriber = prescribers[i];
                        var prescriberOpps;
                        if (i == 0) {
                            orderHeaderMap = prescriber.orderHeaderMap;
                        }
                        prescriberOpps = helper.formatData(orderHeaderMap, prescriber.opportunities, prescriber.orders, helper);
                        prescriber['formattedOpps'] = prescriberOpps;
                        formattedOpps = formattedOpps.concat(prescriberOpps);
                        prescribers.isExpanded = false;
                        
                    }
                    helper.populateMetrics(component, formattedOpps, prescribers.length);
                    component.set("v.allData", prescribers);
                    component.set("v.tableData", prescribers);
                    component.set("v.isLoadingData", false);
                    component.find("filterCombobox").set("v.value", 'all-orders');
                    helper.removeFilterBackgrounds(component);
                    $A.util.addClass(component.find('all-orders'), 'current-filter');
                    
                } else {
                    helper.populateMetrics(component, [], 0);
                    component.set("v.allData", []);
                    component.set("v.tableData", []);
                    component.set("v.isLoadingData", false);
                }
            }
        ).catch(
            function(error){
                component.set("v.isLoadingData", false);
                console.error(error);
            }
        );*/
    },

    populateData : function(component, helper, dateRangeString){
        console.log('populateData');
        ltngUtils.callApexPromise(component.get("c.getOpportunitiesAndOrders"),
                                  {territoryId: component.get("v.currentTerritoryId"),
                                    dateRange: dateRangeString}
                                   //dateRange: 'past-3-months'}
                                 ).then(
            function(prescribers){
                if (prescribers != null){
                    component.set("v.prescribersInTerritoryMetric", prescribers.length);
                    console.log('prescribers returned: ' + prescribers.length);
                    var formattedOpps = [];
                    var orderHeaderMap = null;
                    for (var i = 0; i < prescribers.length; i++) {
                        var prescriber = prescribers[i];
                        var prescriberOpps;
                        if (i == 0) {
                            orderHeaderMap = prescriber.orderHeaderMap;
                        }
                        prescriberOpps = helper.formatData(orderHeaderMap, prescriber.opportunities, prescriber.orders, helper);
                        prescriber['formattedOpps'] = prescriberOpps;
                        formattedOpps = formattedOpps.concat(prescriberOpps);
                        prescribers.isExpanded = false;
                        
                    }
                    helper.populateMetrics(component, formattedOpps, prescribers.length);
                    component.set("v.allData", prescribers);
                    component.set("v.tableData", prescribers);
                    component.set("v.isLoadingData", false);
                    component.find("filterCombobox").set("v.value", 'all-orders');
                    helper.removeFilterBackgrounds(component);
                    $A.util.addClass(component.find('all-orders'), 'current-filter');
                    
                } else {
                    helper.populateMetrics(component, [], 0);
                    component.set("v.allData", []);
                    component.set("v.tableData", []);
                    component.set("v.isLoadingData", false);
                }
                return prescribers;
            }
        ).catch(
            function(error){
                component.set("v.isLoadingData", false);
                console.error(error);
            }
        );
    },
    
    // uniforms opportunity and order header records into wrapper objects to fit the datatable fields and acceptance criteria
    formatData : function(orderHeaderMap, opportunities, orders, helper){
        var distributorStatuses = ['2.1B Distributor - BI Pending', '2.1F Distributor - Unable to Verify', '2.2B Distributor - Verified', '2.3B Dist Verified - Pending Customer OOP Discussion',
                                  '3.1 Pending Distributor Referral Confirmation', '3.2 Distributor Received, Pending Initial Customer Call', '3.3 Distributor Received, Pending Final Customer Call',
                                  '4.5 Distributor-Collecting Docs', '4.9A Dist - Difficult to Collect Docs', '5.2 Distributor-Auth Submitted to Payor', '6.4 Distributor Quote Pending'];
        var newOrderOppTypes = ['NEW SYSTEM', 'Physician Referral', 'Tandem Transmitter', 'Vibe Transmitter'];
        var formattedOpps = [];
        
        // it's gonna get a little ugly here but it's worth it trust me
        
        opportunities.forEach(function(opp){
            var wrapperOpp = {
                Id: opp.Id,
                FirstName: opp.Patient_First_Name__c,
                LastName : opp.Patient_Last_Name__c,
                Name : opp.Patient_First_Name__c + ' ' + opp.Patient_Last_Name__c,
                DateOfBirth : opp.Account.DOB__c,
                Payor : (opp.Payor__r != null) ? opp.Payor__r.Name : '',
                Status : helper.translateStageName(opp),
                CollectedDocuments : helper.getCollectedDocuments(opp),
                                AwaitingDocuments : helper.getAwaitingDocuments(helper.getCollectedDocuments(opp), opp),
                DateSubmitted : new Date(opp.CreatedDate).toISOString().slice(0,10),
                IsActivePatient : (opp.Sales_Channel__c == 'Dexcom' && opp.StageName != '10. Cancelled' && opp.StageName != '61. Quote Approved') ? true : false,
                IsPendingDocuments : ((opp.Sales_Channel__c == 'Dexcom' || opp.Sales_Channel__c == 'Distributor') && opp.StageName == '4. Doc Collection') ? true : false,
                IsOver15Days : ((opp.StageName != '10. Cancelled' && opp.StageName != '61. Quote Approved') && (Date.now() - new Date(opp.CreatedDate))/(3600000 * 24) > 15) ? true : false,
                IsPatientWithDistributor : (opp.Sales_Channel__c == 'Distributor' && (distributorStatuses.includes(opp.Status__c))) || (opp.DME_Distributor__c) ? true : false,
                IsPatientWithPharmacy : (opp.Sales_Channel__c == 'Pharmacy' && opp.StageName == '13. Pharma Retail') ? true : false,
                IsCancelled : (opp.StageName == '10. Cancelled') ? true : false,
                IsNewOrder : (opp.StageName == '61. Quote Approved' && (newOrderOppTypes.includes(opp.Type)) && ((Date.now() - new Date(opp.CreatedDate))/(3600000 * 24) < 30)) ? true : false,
                IsTotalOrder : (opp.StageName == '61. Quote Approved' && ((Date.now() - new Date(opp.CreatedDate))/(3600000 * 24) < 365)) ? true : false
            };
            
            if(opp.Order_NUM__c){
                if(orderHeaderMap[opp.Order_NUM__c]){
                    wrapperOpp.Status += '\n on ' + orderHeaderMap[opp.Order_NUM__c];
                }
            }
            formattedOpps.push(wrapperOpp);
        });
        
        orders.forEach(function(o){
            var wrapper = {
                Id: o.Id,
                FirstName: o.Account__r.FirstName,
                LastName : o.Account__r.LastName,
                Name : o.Account__r.Name,
                DateOfBirth : o.Account__r.DOB__c,
                Payor : (o.Account__r.Payor__r != null) ? o.Account__r.Payor__r.Name : '',
                Status : o.Status__c,
                CollectedDocuments : '',
                AwaitingDocuments : '', 
                DateSubmitted : o.Booked_Date__c,
                IsActivePatient : false,
                IsPendingDocuments : false,
                IsOver15Days : false,
                IsPatientWithDistributor : false,
                IsPatientWithPharmacy : false,
                IsCancelled : false,
                IsNewOrder : ((Date.now() - new Date(o.Booked_Date__c))/(3600000 * 24) < 30) ? true : false,
                IsTotalOrder : ((Date.now() - new Date(o.Booked_Date__c))/(3600000 * 24) < 365) ? true : false
            };
            
            if(o.Order_ID__c){
                if(orderHeaderMap[o.Order_ID__c]){
                    wrapper.Status += '\n on ' + orderHeaderMap[o.Order_ID__c];
                }
            }
            formattedOpps.push(wrapper);
        });

        //formattedOpps = helper.presort(formattedOpps, helper);
        return(helper.presort(formattedOpps, helper));
    },
    
    // translates wording of Status field on Opportunity to meet acceptance criteria
    translateStageName: function(opp){
        var stageName = opp.StageName;
        var status;
        switch(stageName){
            case '1. New Opportunity':
                status = 'Pending Patient Contact';
                break;
            case '2. Verification':
                status = 'Checking Patients Coverage';
                break;
            case '3. Referred to Channel Partner':
                status = 'Sent to Distributor';
                
                break;
            case '4. Doc Collection':
                status = 'Pending Documentation';
                break;
            case '5. Prior-Authorization':
                status = 'Pending Prior Authorization';
                break;
            case '6. Quote Pending':
                status = 'Pending Order Confirmation';
                break;
            case '61. Quote Approved':
                status = 'Shipped';
                break;
            case '10. Cancelled':
                status = 'Cancelled';
                if(opp.Close_Reason__c){
                    status += ': \n' + opp.Close_Reason__c;
                }
                break;
            case '13. Pharma Retail':
                status = 'At a retail pharmacy';
                break;
        }
        if(opp.DME_Distributor__c){
            status += ': \n' + opp.DME_Distributor__r.Name + ' \n' + opp.DME_Distributor__r.Phone;
            console.log(opp);
        }
        return(status);
    },
    
    // formats the value displayed in the Collected Documents column for each opportunity
        getCollectedDocuments: function(opp){

            var collectedDocuments;
            if(opp.StageName == '10. Cancelled' || opp.StageName == '61. Quote Approved'){
                collectedDocuments = '';
            }
            else if(!opp.HasAllDocs__c){
                var isDoc1Collected = this.isDocCollected(opp.Doc_1_Status__c);
                var isDoc2Collected = this.isDocCollected(opp.Doc_2_Status__c);
                if(isDoc1Collected == true && isDoc2Collected == true){
                    collectedDocuments = 'AOB, CMN';
                }
                else if(isDoc1Collected == true){
                    collectedDocuments = 'AOB';
                }
                else if(isDoc2Collected == true){
                    collectedDocuments = 'CMN';
                }
                else{
                    collectedDocuments = '';
                }
            }
            else{
                collectedDocuments = 'AOB, CMN';
            }
            return(collectedDocuments);
        },

        // based on the value of the Collected Documents column, formats the value to be displayed on the Awaiting Documents column
        getAwaitingDocuments: function(collectedDocs, opp){
            var awaitingDocuments;
            if(opp.StageName == '10. Cancelled' || opp.StageName == '61. Quote Approved'){
                awaitingDocuments = '';
            }
            else{
                switch(collectedDocs){
                    case 'AOB, CMN':
                        awaitingDocuments = '';
                        break;
                    case 'AOB':
                        awaitingDocuments = 'CMN';
                        break;
                    case 'CMN':
                        awaitingDocuments = 'AOB';
                        break;
                    case '':
                        awaitingDocuments = 'AOB, CMN';
                        break;
                }
            }
            return(awaitingDocuments);
        },
    
    // determines whether a document can be marked as collected depending on its status
    isDocCollected: function(docStatus){
        var isCollected;
        if(docStatus == 'New' || docStatus == 'Requested' || docStatus == 'Verbal to use Dist.' || docStatus == 'Rejected - AS' || docStatus == 'Rejected - QC'){
            isCollected = false;
        }else if(docStatus == 'Received' || docStatus == 'Submitted' || docStatus == 'Ready to Submit' || docStatus == 'Verified'){
            isCollected = true;
        }
        return(isCollected);
    },

    // presort orders and opportunities
    presort: function(data, helper){
        var sortedData = [];
        var namesOnly = [];
        // extract a single instance of each last + first name into an array
        data.forEach(function(row){
            if(!namesOnly.includes(row.LastName + ' ' + row.FirstName)){
                namesOnly.push(row.LastName + ' ' + row.FirstName);
            }
        });
        // primary sort by patient name
        namesOnly.sort();
        console.log(namesOnly);
        namesOnly.forEach(function(name){
            var toSort = [];
            data.forEach(function(row){
                if(row.LastName + ' ' + row.FirstName == name){
                    toSort.push(row);
                }
            });
            // sort each group of orders of the same patient by date
            helper.sortByDate(toSort).forEach(function(sortedRow){
                sortedData.push(sortedRow);
            });
            //sortedData.push(helper.sortByDate(toSort));
        });
        // iterate through entire list of data and make sub-arrays of the entire rows that match one of the names
        return sortedData;
    },

    sortByDate: function(data){
        // extract DateSubmitted from each row into an array
        var datesOnly = [];
        var datesReversed = [];
        var sortedData = [];
        data.forEach(function(row){
            datesOnly.push(row.DateSubmitted);
        });

        // sort dates, then reverse so the newest date is first
        datesOnly.sort();
        for(var i = datesOnly.length - 1; i >= 0; i--){
            datesReversed.push(datesOnly[i]);
        }

        // match sorted dates to their respective row
        datesReversed.forEach(function(date){
            var index;
            data.forEach(function(row){
                if(row.DateSubmitted == date){
                    index = data.indexOf(row);
                }
            });
            sortedData.push(data[index]);
            data.splice(index, 1);
        });
        console.log(sortedData);
        return sortedData;
    },

    // takes formatted data and calculates metrics based on the properties of each wrapper object
    populateMetrics : function(component, opportunities, prescribersInTerritory){
        console.log('populateMetrics');
        var activePatientsCounter = 0;
        var pendingDocumentsCounter = 0;
        var over15DaysCounter = 0;
        var patientsWithDistributorCounter = 0;
        var patientsWithPharmacyCounter = 0;
        var cancelledCounter = 0;
        var newOrdersCounter = 0;
        var totalOrdersCounter = 0;
        opportunities.forEach(function(opp){
            if(opp.IsActivePatient){
                activePatientsCounter++;
            }
            if(opp.IsPendingDocuments){
                pendingDocumentsCounter++;
            }
            if(opp.IsOver15Days){
                over15DaysCounter++;
            }
            if(opp.IsPatientWithDistributor){
                patientsWithDistributorCounter++;
            }
            if(opp.IsPatientWithPharmacy){
                patientsWithPharmacyCounter++;
            }
            if(opp.IsCancelled){
                cancelledCounter++;
            }
            if(opp.IsNewOrder){
                newOrdersCounter++;
            }
            if(opp.IsTotalOrder){
                totalOrdersCounter++;
            }
        });
        
        component.set("v.activePatientsMetric", activePatientsCounter);
        component.set("v.pendingDocumentsMetric", pendingDocumentsCounter);
        component.set("v.over15DaysMetric", over15DaysCounter);
        component.set("v.patientsWithDistributorMetric", patientsWithDistributorCounter);
        component.set("v.patientsWithPharmacyMetric", patientsWithPharmacyCounter);
        component.set("v.prescribersInTerritoryMetric", prescribersInTerritory);
        component.set("v.cancelledMetric", cancelledCounter);
        component.set("v.newOrdersMetric", newOrdersCounter);
        component.set("v.totalOrdersMetric", totalOrdersCounter);
        component.set("v.allOrdersMetric", opportunities.length);
    },
    
    // handles client side action to reset data to the appropriate subset based on the filter selected
    handleFilter : function(component, event, helper){
        var filter;
        helper.removeFilterBackgrounds(component);
        if(event.currentTarget){
            // if click happened on tile
            filter = event.currentTarget.id;
        }
        else{
            // if click happened on dropdown
            filter = event.getParam("value");
        }
        component.find('filterCombobox').set("v.value", filter);
        $A.util.addClass(component.find(filter), 'current-filter');
        var accountIds = []; // this is to store the prescriber account Ids if they have any filtered results
        var data;
        var filteredData = [];
        if(component.find('searchbar').get("v.value") && component.find('searchbar').get("v.value") != ''){
            if(component.get("v.noResultsFound")){
                component.set("v.tableData", filteredData);
                return;
            }
            data = helper.handleSearch(component, helper, true);
        }
        else{
            data = component.get("v.allData");
        }
        
        if(filter == 'all-orders'){
            component.find('searchbar').set("v.value", '');
            filteredData = component.get("v.allData");
            component.set("v.isLoadingMetric", false);
        }
        else{
            data.forEach(function(prescriber){
                var allOpps = prescriber.formattedOpps;
                var filteredOpps = [];
                // loop through all the formatted table rows and only add the ones that match the filter
                if(filter == 'active-patients'){
                    allOpps.forEach(function(row){
                        if(row.IsActivePatient){
                            filteredOpps.push(row);
                        }
                    });
                }
                else if(filter == 'pending-documents'){
                    allOpps.forEach(function(row){
                        if(row.IsPendingDocuments){
                            filteredOpps.push(row);
                        }
                    });
                }
                else if(filter == 'over-15-days'){
                    allOpps.forEach(function(row){
                        if(row.IsOver15Days){
                            filteredOpps.push(row);
                        }
                    });
                }
                else if(filter == 'patients-with-distributor'){
                    allOpps.forEach(function(row){
                        if(row.IsPatientWithDistributor){
                            filteredOpps.push(row);
                        }
                    });
                }
                else if(filter == 'patients-with-pharmacy'){
                    allOpps.forEach(function(row){
                        if(row.IsPatientWithPharmacy){
                            filteredOpps.push(row);
                        }
                    });
                }
                else if(filter == 'cancelled'){
                    allOpps.forEach(function(row){
                        if(row.IsCancelled){
                            filteredOpps.push(row);
                        }
                    });
                }
                else if(filter == 'new-orders'){
                    allOpps.forEach(function(row){
                        if(row.IsNewOrder){
                            filteredOpps.push(row);
                        }
                    });
                }
                else if(filter == 'total-orders'){
                    allOpps.forEach(function(row){
                        if(row.IsTotalOrder){
                            filteredOpps.push(row);
                        }
                    });
                }
                // clone the prescriber wrapper but set the formattedOpps to the filtered ones
                var filteredPrescriber = {
                    account : prescriber.account,
                    formattedOpps : filteredOpps,
                    opportunities : prescriber.opportunities,
                    orders : prescriber.orders,
                    registered : prescriber.registered,
                    user : prescriber.user,
                    registered : prescriber.registered,
                    inviteSent : prescriber.inviteSent,
                    sendInvite : prescriber.sendInvite
                };
                filteredData.push(filteredPrescriber);
                if(filteredOpps.length > 0){
                    accountIds.push(prescriber.account.Id);
                }
            
        });
    }
    
    component.set("v.tableData", filteredData);
    setTimeout($A.getCallback(
        function(){
            component.set("v.expandedPrescribers", accountIds);
            if(filter != 'all-orders'){
                    component.find('expandToggle').set("v.checked", true);
                }
                else{
                    component.find('expandToggle').set("v.checked", false);
                }
                component.set("v.isLoadingMetric", false);
            }
        ));
    },
    
    // removes styling from any selected tile 
    removeFilterBackgrounds: function(component){
        console.log('removeFilterBackgrounds');
        var allMetrics = ['active-patients', 'pending-documents', 'over-15-days', 'patients-with-distributor', 
                           'patients-with-pharmacy', 'cancelled', 'new-orders', 'total-orders', 'all-orders'];
        allMetrics.forEach(function(metric){
            $A.util.removeClass(component.find(metric), 'current-filter');
        }); 
    },
    
    // handles client side action to filter displayed data by prescriber or table row name
    handleSearch : function(component, helper, returnResults){
        if(!returnResults){
            helper.removeFilterBackgrounds(component);
        setTimeout($A.getCallback(
            function(){
                $A.util.addClass(component.find('all-orders'), 'current-filter');
                component.find('filterCombobox').set("v.value", 'all-orders');
            }
        ));
        }

        component.set("v.isSearching", true);
        
        component.find('expandToggle').set("v.checked", false);
        var searchString = component.find('searchbar').get('v.value');
        var allData = component.get("v.allData");
        var searchResultData = [];
        var resultAccountIds = [];
        var resultRowIds = [];
        component.set("v.noResultsFound", false);
        
        // search thru all prescriber names first, if they are a match add their entire dashboard
        allData.forEach(function(prescriber){
            if(prescriber.account.Name.toLowerCase().includes(searchString.toLowerCase())){
                searchResultData.push(prescriber);
                resultAccountIds.push(prescriber.account.Id);
                prescriber.formattedOpps.forEach(function(row){
                    resultRowIds.push(row.Id);
                });
            }
        });

        // search thru each prescriber's opps and orders
        allData.forEach(function(prescriber){
            var allOpps = prescriber.formattedOpps;
            allOpps.forEach(function(row){
                // if the name of the table row is a match
                if(row.Name.toLowerCase().includes(searchString.toLowerCase())){
                    // if the row is not already included in the search results
                    if(!resultRowIds.includes(row.Id)){
                        // check if the prescriber is already included in the search results
                        if(resultAccountIds.includes(prescriber.account.Id)){
                            // find the related prescriber in the search results
                            searchResultData.forEach(function(searchResult){
                                if(searchResult.account.Id == prescriber.account.Id){
                                    var resultIndex = searchResultData.indexOf(searchResult);
                                    searchResultData[resultIndex].formattedOpps.push(row);
                                }
                            });
                            resultRowIds.push(row.Id);
                        }
                        // otherwise add the prescriber to the search results with only the matching row
                        else{
                            var prescriberCopy = {
                                account : prescriber.account,
                                formattedOpps : [row],
                                opportunities : prescriber.opportunities,
                                orders : prescriber.orders,
                                registered : prescriber.registered,
                                user : prescriber.user,
                                registered : prescriber.registered,
                                inviteSent : prescriber.inviteSent,
                                sendInvite : prescriber.sendInvite
                            };
                            resultAccountIds.push(prescriber.account.Id);
                            resultRowIds.push(row.Id);
                            searchResultData.push(prescriberCopy);
                        }
                    }
                }
            });
        });
        // if needed, return the search results instead of passing them to the component
        if(returnResults){
            component.set("v.isSearching", false);
            return searchResultData;
        }
        // set all returned accordion sections to be expanded
        if(searchResultData.length > 0){
            component.set("v.tableData", searchResultData);
            /*setTimeout($A.getCallback(
                function(){
                    component.set("v.expandedPrescribers", resultAccountIds);
                }
            ));*/
        }
        else{
            component.set("v.noResultsFound", true);
            component.set("v.tableData", []);
            /*setTimeout($A.getCallback(
                function(){
                    component.set("v.expandedPrescribers", sectionsOpenBeforeSearch);
                }
            ));*/
        }
        component.set("v.isSearching", false);
    },
    
    // handles client side action to sort all table rows inside each accordion section
    sortRows : function(component, event, helper){
        var sectionsOpenBeforeSort = component.get("v.expandedPrescribers")
        var column = event.currentTarget.id;
        var sortDirection = component.get("v." + column + "SortDirection");
        var tableData = component.get("v.tableData");
        var sortedData = [];
        tableData.forEach(function(prescriber){
            var sortedRows = [];
            var columnValues = [];
            var rows = prescriber.formattedOpps;
            // get all the values in that column and sort them according to sortDirection
            rows.forEach(function(row){
                columnValues.push(row[column]);
            });
            columnValues.sort();
            if(sortDirection == 'desc'){
                var descSort = [];
                for(var i = columnValues.length - 1; i >= 0; i--){
                    descSort.push(columnValues[i]);
                }
                columnValues = descSort;
            }
            // find the first matching row and added to the list of sorted rows
            columnValues.forEach(function(value){
                var matchIndex;
                rows.forEach(function(row){
                    if(row[column] == value){
                        matchIndex = rows.indexOf(row);
                    }
                });
                // delete the matching row so it does not get added again
                sortedRows.push(rows[matchIndex]);
                rows.splice(matchIndex, 1);
            });
            
            var prescriberCopy = {
                account : prescriber.account,
                formattedOpps : sortedRows,
                opportunities : prescriber.opportunities,
                orders : prescriber.orders,
                registered : prescriber.registered,
                user : prescriber.user,
                registered : prescriber.registered,
                inviteSent : prescriber.inviteSent,
                sendInvite : prescriber.sendInvite
            };
            sortedData.push(prescriberCopy);
        });
        // change the sort direction for the column
        
        if(sortDirection == 'asc'){
            component.set("v." + column + "SortDirection", 'desc');
        }
        else if(sortDirection == 'desc'){
            component.set("v." + column + "SortDirection", 'asc');
        }
        // set the data and reopen expaned sections
        component.set("v.tableData", sortedData);
        setTimeout($A.getCallback(
            function(){
                component.set("v.expandedPrescribers", sectionsOpenBeforeSort);
            }
        ));
    },
    
    // handles call to server to create a new partner community user for an unregistered prescriber
    sendInvite : function(component, event){
        var currentTerritoryId = component.get("v.currentTerritoryId");
        var personAccountId = component.get("v.prescriberId");
        var email = component.find('SendInviteNewEmail').get("v.value") ? component.find('SendInviteNewEmail').get("v.value") : email = component.get("v.emailOnFile");
        var username = component.find('SendInviteNewUsername').get("v.value") != 'newuser@dexcomcommunity.com' ? component.find('SendInviteNewUsername').get("v.value") : component.get("v.suggestedUsername");
        var allData = component.get("v.allData");
        var tableData = component.get("v.tableData");
        var prescriberIndexAllData;
        var prescriberIndexTableData;
        allData.forEach(function(p){
            if(p.account.Id == personAccountId){
                prescriberIndexAllData = allData.indexOf(p);
            }
        });
        tableData.forEach(function(p){
            if(p.account.Id == personAccountId){
                prescriberIndexTableData = tableData.indexOf(p);
            }
        });
        
        return ltngUtils.callApexPromise(component.get("c.createPrescriberUser"), {
            territoryId: currentTerritoryId,
            personAccountId: personAccountId,
            userEmail : email,
            username : username
        }).then(
            function(newContactId){
                // update the prescriber on both versions of the data, could be refactored to be more elegant
                allData[prescriberIndexAllData].inviteSent = true;
                allData[prescriberIndexAllData].sendInvite = false;
                allData[prescriberIndexAllData].registered = false;
                tableData[prescriberIndexTableData].inviteSent = true;
                tableData[prescriberIndexTableData].sendInvite = false;
                tableData[prescriberIndexTableData].registered = false;
                component.set("v.allData", allData);
                component.set("v.tableData", tableData);
                component.set("v.displaySendInviteModal", false);
            }
        ).catch(
            function(error){
                console.log(error);
            }
        );
    },
    
    // fills fields related to prescriber on prescriber modals
    setPrescriberInfo : function(component, event){
        var personAccountId = event.getSource().get("v.tabindex");
        var action = event.getSource().getLocalId();
        var allData = component.get("v.allData");
        var prescriber;
        component.set("v.prescriberId", personAccountId);
        // find the prescriber that was clicked on
        allData.forEach(function(p){
            if(p.account.Id == personAccountId){
                prescriber = p;
            }
        });
        // set information according to which action was clicked
        component.set("v.prescriberName", prescriber.account.Name);
        if(action.includes('SendInvite')){
            var suggestedUsername;
            return ltngUtils.callApexPromise(component.get("c.suggestUsername"),
                {
                    personAccountId : component.get("v.prescriberId"),
                    territoryId : component.get("v.currentTerritoryId")
                }).then(
                function(result){
                    component.set("v.suggestedUsername", result);
                    if(prescriber.account.PersonEmail){
                        component.set("v.emailOnFile", prescriber.account.PersonEmail);
                    }
                    else{
                        component.set("v.emailOnFile", 'Sorry! No email address on file, please enter one.');
                        component.set("v.isEmailRequired", true);
                        component.set("v.isInviteDisabled", true);
                    }
                }).catch(
                function(error){
                    console.log(error);
                }
            );
        }
        else{
            if(action.includes('InviteSent')){
                if(!prescriber.user){
                    component.set("v.displayNewUserMessage", true);
                    component.set("v.isInviteSentActionsDisabled", true);
                }
                else{
                    component.set("v.inviteSentDate", prescriber.user.CreatedDate.split('T')[0]);
                    component.set("v.prescriberUsername", prescriber.user.Username);
                    component.set("v.prescriberEmail", prescriber.user.Email);
                }
            }
            if(action.includes('Registered')){
                component.set("v.prescriberUsername", prescriber.user.Username);
                component.set("v.prescriberEmail", prescriber.user.Email);
                component.set("v.lastLoginDate", prescriber.user.LastLoginDate.split('T')[0]);
            }
        }
    },
    
    // handles call to server to check for uniqueness of a new username
    checkUsernameIsUnique : function(component, event){
        var input = event.getSource();
        var inputValue = input.get("v.value");
        return ltngUtils.callApexPromise(component.get("c.checkUsernameUniqueness"),
            {
                username : inputValue 
            }).then(
            function(isUnique){
                if(!isUnique){
                    input.setCustomValidity('The username entered is not unique, please select a different one.');
                }
                input.reportValidity();
            }
        );
    },
    
    // handles call to server to save edited user information for a prescriber
    saveUserInfo : function(component, event){
        var userState = event.getSource().getLocalId().split('save')[1];
        var newUsername = component.find(userState + 'NewUsername').get("v.value");
        var newEmail = component.find(userState + 'NewEmail').get("v.value");
        // only make call to server if there are any changes in the user information
        if(newUsername || newEmail){
            //get the userId related to the prescriber Id
            var prescriberId = component.get("v.prescriberId");
            var allData = component.get("v.allData");
            var userId;
            allData.forEach(function(prescriber){
                if(prescriber.account.Id == prescriberId){
                    userId = prescriber.user.Id;
                }
            });
            return ltngUtils.callApexPromise(component.get("c.changeUserInfo"),
                {
                    userId : userId,
                    newUsername : newUsername,
                    newEmail : newEmail,
                    territoryId : component.get("v.currentTerritoryId"),
                    personAccountId : prescriberId
                }).then(
                function(user){
                    allData.forEach(function(prescriber){
                        if(prescriber.account.Id == prescriberId){
                            prescriber.user.Username = user.Username;
                            prescriber.user.Email = user.Email;
                        }
                    });
                    component.set("v.allData", allData);
                    component.set("v.prescriberEmail", user.Email);
                    component.set("v.prescriberUsername", user.Username);
                    component.set("v.editingPrescriberInfo", false);
                }
            ).catch(function(error){
                console.error(error);
            });
        }
        else{
            component.set("v.editingPrescriberInfo", false);
        }
    },
    
    // handles call to server to reset password for prescriber community user
    resetUserPassword : function(component, event){
        var prescriberId = component.get("v.prescriberId");
        var allData = component.get("v.allData");
        var userId;
        allData.forEach(function(prescriber){
            if(prescriber.account.Id == prescriberId){
                userId = prescriber.user.Id;
            }
        });
        return ltngUtils.callApexPromise(
            component.get("c.resetPassword"),
            {
                userId : userId,
                territoryId : component.get("v.currentTerritoryId"),
                personAccountId : prescriberId
            }
        ).then(
            function(result){
                component.set("v.isPasswordReset", true);
            }
        ).catch(
            function(error){
                console.error(error);
            }
        );
    }
})