({
	getPayorCheck : function(component, event, helper) {
		 helper.getQCHoldPayorCheckService(component, event);
	},
    fireChangeEvent : function(component, event, helper) {
		helper.updatePayorCheckData(component, event);	
	}
})