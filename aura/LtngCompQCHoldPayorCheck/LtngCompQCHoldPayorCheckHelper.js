({
    getQCHoldPayorCheckService : function(component, event) {
        var action = component.get("c.getPayorDetails");
        
        action.setParams({ 
            orderId : component.get("v.orderId"),
            accountId : component.get("v.accountId"),
            orderType : component.get("v.orderType"),
            isOrderActivated : component.get("v.isOrderActivated")
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                component.set('v.payorCheckMap',response.getReturnValue());
                var payorCheckMap = response.getReturnValue();
                var byPassPayorCheck = payorCheckMap.bypassCheck == 'false' ? false : true ;
              // alert('payorCheckMap.inContract..'+payorCheckMap.inContract);
                component.set("v.contractEndDate" , payorCheckMap.contractEndDate);
                component.set("v.diagnosisCovered" , payorCheckMap.diagnosisCovered == 'false' ? false : true );
                component.set("v.inContract" , payorCheckMap.inContract == 'false' ? false : true );
                component.set("v.inNetWork" , payorCheckMap.inNetWork == 'false' ? false : true );
                component.set("v.byPassPayorCheck" , byPassPayorCheck);
                component.set("v.diagnosisType" , payorCheckMap.diagnosisType);
				component.set("v.payorDiagnosisType" , payorCheckMap.payorDiagnosisType);
				component.set("v.insulinTherapyRequired" , payorCheckMap.insulinTherapyRequired);
				component.set("v.patientInsulinTherapy" , payorCheckMap.patientInsulinTherapy);
                //alert('byPassPayorCheck..'+ byPassPayorCheck);
                
                if((payorCheckMap.inContract == 'true' && 
                    payorCheckMap.inNetWork  == 'true' && 
                    payorCheckMap.diagnosisCovered == 'true') || 
                   	byPassPayorCheck  == true){
                   this.updateCss(component, true);                    
                }
                else{
                    this.updateCss(component, false);                    
                }
                var thisEvent = component.getEvent("payorComponentEvent");
                    thisEvent.setParams({"payorDetailsMap": component.get("v.payorCheckMap")});
                    thisEvent.fire();
            }
            
        });
        
        $A.enqueueAction(action);
    },
    
    updateCss : function(component, checkStatus) {
        var lineComponent = component.find("highlightDiv");
        $A.util.removeClass(lineComponent, 'border');
        
        console.log('Payor checkStatus >>' +checkStatus);
        if(checkStatus){ 
            $A.util.removeClass(lineComponent, 'slds-border-incomplete');            
            $A.util.addClass(lineComponent , 'slds-border-complete');
        }else{ 
            $A.util.removeClass(lineComponent, 'slds-border-complete'); 
            $A.util.addClass(lineComponent , 'slds-border-incomplete');
        }
    },
    updatePayorCheckData : function(component, event) {
        console.log('fireChangeEvent >> ');
        var inContract = component.get("v.inContract");
        var inNetWork = component.get("v.inNetWork");
        var diagnosisCovered = component.get("v.diagnosisCovered");
        var byPassPayorCheck = component.get("v.byPassPayorCheck");
        var payorCheckMap = component.get("v.payorCheckMap");
        
        console.log(inContract+','+inNetWork+','+diagnosisCovered);
         // alert('byPassPayorCheck.ch.'+ byPassPayorCheck);
        //update CSS 
        if((inContract && inNetWork && diagnosisCovered) || byPassPayorCheck){
            this.updateCss(component, true);                   
        }
        else{
            this.updateCss(component, false);            
        }
        //fire change event
        if(byPassPayorCheck != null){
        	payorCheckMap["bypassCheck"] = byPassPayorCheck;
        }  
        if(inContract != null){
        	payorCheckMap["inContract"] = inContract;
        }
        if(inNetWork != null){
            payorCheckMap["inNetWork"] = inNetWork;
        }
        if(diagnosisCovered != null){
            payorCheckMap["diagnosisCovered"] =  diagnosisCovered;
        }
        var thisEvent = component.getEvent("payorComponentEvent");
        thisEvent.setParams({"payorDetailsMap": payorCheckMap});
        thisEvent.fire();
        
    }
})