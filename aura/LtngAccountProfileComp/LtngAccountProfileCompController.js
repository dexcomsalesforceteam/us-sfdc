({
  doInit: function(component, event, helper) {      
    // Fetch the account list from the Apex controller   
    helper.getAccountMedicareQuestionnaireList(component);
  },
    navigateToAccountRecord:function(component, event, helper) {      
    // Navigate to the account record  
    helper.navigateToAccountRecord(component);
  }   
})