({
    // Fetch the accounts from the Apex controller
    getAccountMedicareQuestionnaireList: function(component) {
        
        //call apex class method by passing in the accountid parameter value
        console.log('Calling Apex');
        var action = component.get('c.getAccountMedicareQuestionnaire');
        action.setParams({
            "accountId": component.get("v.accountId")
        });
        // Set up the callback
        action.setCallback(this, function(actionResult) {
            component.set('v.accountProfile', actionResult.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    navigateToAccountRecord: function(component) {
        var action = component.get("c.saveAccountProfile");
        var context = component.get("v.userContext");
        var sObjectId = component.get("v.opportunityId") == null ? component.get("v.accountId") : component.get("v.opportunityId");
        action.setParams({ 
            'accountProfileList': component.get("v.accountProfile"),
            'accountId': component.get("v.accountId")
        });
       // Set up the redirect
        action.setCallback(this, function(actionResult) {
            window.location.assign('/'+sObjectId);
        });
        $A.enqueueAction(action);
    }
})