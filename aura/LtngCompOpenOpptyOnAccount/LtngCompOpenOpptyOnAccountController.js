({
	init : function(component, event, helper) {
        if(component.get("v.orderStatus") !='Activated')
		helper.getOpenOppty(component, event);
	},
    
    updateOppty : function(component, event, helper){
        helper.updateRecords(component, event);
    },
})