({
	getOpenOppty : function(cmp, event) {
		var action = cmp.get("c.getOpenOpptyDetails");
        cmp.set("v.loadSpinner",true);
        console.log('accountId:::', cmp.get("v.accountId"))
        
        action.setParams({ accountId : cmp.get("v.accountId") });
        
         action.setCallback(this, function(response) {
            var state = response.getState();
             if (state === "SUCCESS") {
                 cmp.set("v.loadSpinner",false);
                 var opportunityList = response.getReturnValue();
                 cmp.set("v.responseReceived",true);
                 //if(opportunityList !=null && opportunityList !=undefined && opportunityList.length > 0){
                     cmp.set("v.opportunityList",opportunityList);
                     console.log('oppty list:::::',cmp.get("v.opportunityList"))
                     var setOpptyList = cmp.get("v.opportunityList");
                 //}
                 cmp.set("v.loadSpinner",false);
             }else if(state === "INCOMPLETE"){
                 cmp.set("v.loadSpinner",false);
             }else if (state === "ERROR"){
                 cmp.set("v.loadSpinner",false);
             }
             
         });
        
        $A.enqueueAction(action);
	},
    
    updateRecords : function(cmp,event){
        var action = cmp.get("c.updateOpptyRecords");
        cmp.set("v.loadSpinner",true);
        action.setParams({ updateOpptyList : cmp.get("v.opportunityList") });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
             if (state === "SUCCESS") {
                 var opportunityList = response.getReturnValue();
                 cmp.set("v.loadSpinner",false);
                 this.getOpenOppty(cmp,event);
             }else if(state === "INCOMPLETE"){
                 cmp.set("v.loadSpinner",false);
             }else if (state === "ERROR"){
                 cmp.set("v.loadSpinner",false);
             }
         });
        
        $A.enqueueAction(action);
    }
})