({
	doInit : function(component) {
        
        // Initialize input select options for request types
        var optsRequestType = [
            { "class": "optionClass", label: "General", value: "General", selected: "true" },
            { "class": "optionClass", label: "Share Update", value: "Share Update" },
            { "class": "optionClass", label: "Technical Request", value: "Technical Request" },
            { "class": "optionClass", label: "Submit Collateral", value: "Submit Collateral" },
            { "class": "optionClass", label: "Press Release", value: "Press Release" },
            { "class": "optionClass", label: "Conference", value: "Conference" },
            { "class": "optionClass", label: "Rumor", value: "Rumor" },
            { "class": "optionClass", label: "CIG Admin", value: "CIG Admin" }
        ];
        
        var optsPriority = [
            { "class": "optionClass", label: "Low", value: "Low", selected: "true" },
            { "class": "optionClass", label: "Medium", value: "Medium" },
            { "class": "optionClass", label: "High", value: "High" },
            { "class": "optionClass", label: "Important", value: "Important" },
            { "class": "optionClass", label: "Escalation Required", value: "Escalation Required" }            
        ];
        
        var utilityAPI = component.find("utilitybar");
        
        utilityAPI.getEnclosingUtilityId().then(function(utilityId) {
        	component.set("v.isInUtilityBar",true);
            component.set("v.showFeedbackWindow",true);
            component.set("v.recordURL", "/" + component.get("v.recordId")); 
            
            // Populate select poxes for request type and priority
            component.find("srRequestType").set("v.options", optsRequestType);
            component.find("srPriority").set("v.options", optsPriority);
        })
        .catch(function(error) {
            console.log(error);
        });
        
        var userId = $A.get("$SObjectType.CurrentUser.Id");
        component.set("v.currUserId", userId);
        console.log('**** Current user id:' + userId);        
    },
    
    onChangeRequestType: function(cmp) {
		 var selRequestType = cmp.find("srRequestType");
        
         if(cmp.get("v.isInUtilityBar")){
         	cmp.set("v.requestType", selRequestType.get("v.value"));
         }
        
	 }, 
    
    onChangePriority: function(cmp) {
		 var selChatter = cmp.find("feedbackGroupId");
        
         if(cmp.get("v.isInUtilityBar")){
         	cmp.set("v.chatterGroupID", selChatter.get("v.value"));
         }
	 }, 
    closeComponent : function(component, event) {
        if(!component.get("v.isInUtilityBar")){
           component.set("v.showFeedbackWindow",false);
        } else {
            //close utility bar if in utility bar
            var utilityAPI = component.find("utilitybarclose");
            component.find("feedbackSubject").set("v.value","");
            component.find("feedbackBody").set("v.value","");
            utilityAPI.minimizeUtility();
        }
    },
    
    submitFeedback : function(component,event) {
        console.log('clicked button to submit form');
        if($A.util.isEmpty(component.find("feedbackBody").get("v.value"))){
            alert('Please enter support request details before submitting');
        }else{
            var feedback = component.find("feedbackBody").get("v.value");
            var subject = component.find("feedbackSubject").get("v.value");
            var requestType = component.find("srRequestType").get("v.value");
            var priority = component.find("srPriority").get("v.value");
            
            var files1=component.get('v.filesDB');
            var filesDB1=[];
            if(files1 != null){
                files1.forEach(function(file){
                    filesDB1.push(file.documentId);
                });
                console.log('Files for DB: '+ filesDB1);    
            }
            
            //set recordURL again if in utility bar
            if(component.get("v.isInUtilityBar")){
                component.set("v.recordURL", "/" + component.get("v.recordId"));               
            }
            var recordURL = component.get("v.recordURL");
            if(feedback!=''){
                var action = component.get("c.processFeedback");
                console.log('** Subject is-->' + subject + '');
                console.log('** Request type is-->' + requestType + '');
                console.log('** Priority is-->' + priority + '');
                console.log('** Details-->' + feedback + '');
                
                action.setParams({ "subject" : subject,
                                   "requestType" : requestType,
                                   "priority" : priority,
                                   "feedback" : feedback,
                                   "files" : filesDB1
                                 });
                action.setCallback(this, function(response){
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        console.log('response-->'+ response.getReturnValue() + '');
                        component.set("v.feedbackSubmitted", response.getReturnValue());
                        if(!component.get("v.isInUtilityBar")){
                           component.set("v.showFeedbackWindow",false);
                        } else {
                            //clear feedback
                            component.find("feedbackBody").set("v.value","");
                            //close utility bar if in utility bar
                            var utilityAPI = component.find("utilitybarclose");
                            
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                "title": "Success!",
                                "message": "Request submitted successfully."
                            });
                            toastEvent.fire();
                            // Clear form 
                            if(component.get("v.isInUtilityBar")){
                               var utilityAPI = component.find("utilitybarclose");
                               //clear feedback
                               component.find("feedbackSubject").set("v.value","");
                               component.find("srRequestType").set("v.value","");
                               component.find("srPriority").set("v.value","");
                               component.find("feedbackBody").set("v.value","");
                               component.set("v.fileIds",null);
                               utilityAPI.minimizeUtility();
        					} 
                            
                        }
                        
                    }
                });
            }
            $A.enqueueAction(action);
        }
	},
    
    handleUploadFinished : function(component, event, helper) {
        var uploadedFiles = event.getParam("files");
        var documentId = uploadedFiles[0].documentId;
        var fileName = uploadedFiles[0].name;
        
        var files=event.getParam('files');
        console.log('Files uploaded: '+JSON.stringify(files));
        var fileIds=[];
        files.forEach(function(file){
            fileIds.push(file.documentId);
        });
        component.set('v.numFiles', fileIds.length);
		component.set('v.fileIds', fileIds);
        component.set('v.filesDB', files);
        
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": "File "+fileName+" Uploaded successfully."
        });
        toastEvent.fire();
    },
    
})