({
    handleHcpChangeFunction : function(cmp,event) {
        var selectValue = cmp.find('hcpSelect').get('v.value');
        console.log('selectValue::::',selectValue);
        if(selectValue != "--None--"){
            cmp.set("v.hcpTypeSelected",selectValue);
        }
        if(selectValue==="Update HCP"){
            cmp.set("v.isUpdateHCP",true);
        }else{
            cmp.set("v.isUpdateHCP",false);
        }
    },
    
    /*
     * send Email function
    */
    saveRecord : function(cmp,event){
        var paramList = [];
        var allParams = [];
        var params = ["hcpTypeSelected","partyId","firstName","lastName","address","city","state","zip","npi","phone","fax","payorId","accountNumber"];
        for(var itr = 0; itr<params.length;itr++){
            var paramName = params[itr];
            var detailObj = {variableKey :paramName, variableValue:cmp.get("v."+params[itr])};
            paramList.push(detailObj);
        }
        
        console.log('paramList::::::',paramList);
        
        var action = cmp.get("c.sendEmail");
        action.setParams({ paramWrapList : paramList });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var context = cmp.get("v.userTheme");
                console.log("From server: " + response.getReturnValue());
                if(context !=''){
                    if(context == 'Theme4t' || context == 'Theme4d') {
                        console.log('VF in S1 or LEX');
                        sforce.one.navigateToSObject('o/Account/home');
                    } else {
                        console.log('VF in Classic');                         
                        window.location.assign('/001/o');
                    }
                }
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
    }
    
})