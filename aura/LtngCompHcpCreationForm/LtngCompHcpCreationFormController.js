({
	handleSave : function(component, event, helper) {
        //get boolean from checkValidity and then call save method upon true
        
        console.log('record id is:::',component.get("v.recordId"))
        console.log('user theme is::::',component.get("v.userTheme"))
        var allValid = component.find('input').reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get('v.validity').valid;
        }, true);
        var selectValue = component.find('hcpSelect').get('v.value');
        var selectPickvalDiv =component.find('hcpSelect');
        var isPickvalValid = false;
        if(selectValue !='--None--'){
            isPickvalValid = true;
            $A.util.removeClass(selectPickvalDiv, 'slds-has-error');
        }else{
            $A.util.addClass(selectPickvalDiv, 'slds-has-error');
        }
        var isValidPayor = false;
        var partyIdDiv = component.find("partyIdDiv");
        var errorMessage1 = component.find("errorMessage1");
        if(isPickvalValid && selectValue==="Update HCP"){
            if(component.get("v.partyId") !='' && component.get("v.partyId") !=null && component.get("v.partyId") !=undefined){
                isValidPayor = true;
                $A.util.removeClass(partyIdDiv, 'slds-has-error');
            }else{
                isValidPayor = false;
                $A.util.addClass(partyIdDiv, 'slds-has-error');
            }
        }else if(isPickvalValid && selectValue !="Update HCP"){
            isValidPayor = true;
            $A.util.removeClass(partyIdDiv, 'slds-has-error');
        }
        if(allValid && isPickvalValid && isValidPayor){
            helper.saveRecord(component,event);
        }
        
	},
    
    onHcpChange: function(component,event, helper){
        helper.handleHcpChangeFunction(component,event);
    },
    
     doAction : function(component) {
        var inputCmp = component.find("inputCmp");
         var inputCmp2 = component.find("inputCmp2");
        var value2 = inputCmp.get("v.value");

        var value = inputCmp.get("v.value");
//alert(value);
        // Is input numeric?
        if (value =='' || value==null) {
            // Set error
            inputCmp.set("v.errors", [{message:"Complete this field."}]);
        } else {
            // Clear error
            inputCmp.set("v.errors", null);
        }
         if (value2 =='' || value2==null) {
            // Set error
            inputCmp2.set("v.errors", [{message:"Complete this field."}]);
        } else {
            // Clear error
            inputCmp2.set("v.errors", null);
        }
     }
})