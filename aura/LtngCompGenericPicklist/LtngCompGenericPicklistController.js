({
	doInit : function(component, event, helper) {
		helper.getInitData(component,event);
	},
    
    handleChange : function(component, event, helper){
        helper.handlePickvalChange(component, event);
    }
})