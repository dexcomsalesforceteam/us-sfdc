({
	getInitData : function(cmp,event) {
		var action = cmp.get("c.getPicklistData");
        action.setParams({ "sobjectApiName" : cmp.get("v.sobjectApiName"),
                          "fieldApiName" : cmp.get("v.fieldApiName")});
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var responseValue = response.getReturnValue();
                console.log('responseValue::::',responseValue);
                if(responseValue != undefined && responseValue !=null){
                    var picklistValue = [];
                    for(var i=0;i<responseValue.length;i++){
                        if(responseValue[i].includes(':')){
                            console.log('split values are:::',responseValue[i].split(':'));
                            console.log('contains hyphen::::')
                            var tempObj = {label : responseValue[i].split(':')[0], idValue : responseValue[i].split(':')[1]};
                            picklistValue.push(tempObj);
                            console.log('picklistValue[i]:::::',picklistValue[i])
                        }else{
                            var tempObj = {label : responseValue[i], idValue : responseValue[i]};
                            picklistValue.push(tempObj);
                        }
                    }
                    console.log('picklistValue:::::',picklistValue);
                    cmp.set("v.listOfValues",picklistValue);
                    console.log('set values are:::::', cmp.get("v.listOfValues"))
                    cmp.set("v.initComplete",true);
                }
            }
            else if (state === "INCOMPLETE") {
                // do something
                console.log('status is incomplete')
            }else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
	},
    
    
    handlePickvalChange : function(cmp, event){
        //selectedValue
        var selectedValue;
        if(cmp.find('select1') != undefined && cmp.find('select1') !=null){
            selectedValue = cmp.find('select1').get("v.value");
        }
        cmp.set("v.selectedValue",selectedValue);
        console.log('selected value is::::',cmp.get("v.selectedValue"))
    }
})