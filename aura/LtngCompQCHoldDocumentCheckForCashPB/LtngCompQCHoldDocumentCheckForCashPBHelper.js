({
    getQCHoldDocumentDetails : function(component, event) {
        var selectedDoc = [];
        var action = component.get("c.getDocumentDetails");
        // alert('** order type doc ** ' + component.get("v.orderType"));
        action.setParams({ 
            "orderId": component.get("v.orderId"),
            "accountId": component.get("v.accountId"),
            "orderType": component.get("v.orderType"),
            "isOrderActivated": component.get("v.isOrderActivated")
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                component.set('v.documentCheckMap',response.getReturnValue());
                
                var documentCheckMap = response.getReturnValue();   
               // for (var i = 0, keys = Object.keys(documentCheckMap), ii = keys.length; i < ii; i++) {
                //    alert(keys[i] + '|' + documentCheckMap[keys[i]]);
                //}
                component.set("v.CMNExpirationDate" , documentCheckMap.CMNExpirationDate );
                component.set("v.CMN" , documentCheckMap.CMN  == 'true' ? true : false); 
                
                //AOB will be conditional
                if("AOB" in documentCheckMap){ 
                    component.set("v.AOB" ,  documentCheckMap.AOB  == 'true' ? true : false);
                    component.set("v.displayAOB" , true);    
                }else{
                   // alert('Document Check Hide!!!');
                    component.set("v.displayAOB" , false);
                }
                
                
                var thisEvent = component.getEvent("documentsCheckEvent");
                thisEvent.setParams({"documentDetailsMap": component.get("v.documentCheckMap")});
                thisEvent.fire();
                
            }else{
                console.log('Error getting document data >> ');
            }
            
        });
        
        $A.enqueueAction(action);
    },
    updateDocumentDetails : function(component, event) {     
        var CMN 				=  component.get("v.CMN");
        var CMNExpirationDate 	=   component.get("v.CMNExpirationDate" );       
        var AOB 				=  component.get("v.AOB");
        var documentCheckMap 	= component.get("v.documentCheckMap");
        
        if(documentCheckMap["CMN"] != null) documentCheckMap["CMN"] = CMN;
        if(documentCheckMap["CMNExpirationDate"] != null) documentCheckMap["CMNExpirationDate"] = CMNExpirationDate;
        if(documentCheckMap["AOB"] != null ) documentCheckMap["AOB"] = AOB;
        
        console.log('documentCheckMap',documentCheckMap);
        
        var thisEvent = component.getEvent("documentsCheckEvent");
        thisEvent.setParams({"documentDetailsMap": documentCheckMap});
        thisEvent.fire();
    }
})