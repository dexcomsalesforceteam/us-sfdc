({    
	doInit : function(component, event, helper) {        
        component.set('v.columns', [
            {label: 'Title', fieldName: 'linkName', editable:'false', type: 'url', typeAttributes: {label: { fieldName: 'Note_Title__c' }, target: '_self'}},           
            {label: 'Form_Number', fieldName: 'Form_Number__c', editable:'false', type: 'text'},
            {label: 'Analyst', fieldName: 'Analyst__c', editable:'false', type: 'text'},
            {label: '# Files', fieldName: 'Num_Files_Attached__c', editable:'false', type: 'number'},
            {label: 'Filed Date', fieldName: 'Filed_Date__c', editable:'false', type: 'date-local', sortable: true, typeAttributes:{year:'numeric',month:'short',day:'2-digit'}}
            
        ]);        
        helper.getFinancialData(component, helper);
        console.log('***** Org Id=' + component.get("v.recordId"));       
    },
    
    onSave : function (component, event, helper) {
        helper.saveDataTable(component, event, helper);
    },
    
    //Method gets called by onsort action,
    handleSort : function(component,event,helper){
        //Returns the field which has to be sorted
        var sortBy = event.getParam("fieldName");
        //returns the direction of sorting like asc or desc
        var sortDirection = event.getParam("sortDirection");
        //Set the sortBy and SortDirection attributes
        component.set("v.sortBy",sortBy);
        component.set("v.sortDirection",sortDirection);
        // call sortData helper function
        helper.sortData(component,sortBy,sortDirection);
    },    
    openModel: function(component, event, helper) {
      // for Display Model,set the "isOpen" attribute to "true"
      component.set("v.isOpen", true);
   },
   
   closeModel: function(component, event, helper) {
      // for Hide/Close Model,set the "isOpen" attribute to "Fasle"     
      component.set("v.isOpen", false);
   },
   handleCloseModal : function(component, event, helper) {
       var value = event.getParam("param");
       component.set("v.isOpen", false);
   },
   SubmitFormData : function(component, event, helper) {
       var attribute1 = component.get('v.ModalOrgType');
       var marketingForm = component.find('CIG_AddFinancial');
       marketingForm.mySubmitOrgData(attribute1);       
   }, 
   handleClinicalHeaderClick: function (component, event, helper) {
   	var evt = $A.get("e.force:navigateToComponent");
    evt.setParams({
        componentDef : "c:CIG_FinancialRecords",
        componentAttributes: {            
            recordId : component.get("v.recordId")
        }
    });
    evt.fire(); 
   }
   
})