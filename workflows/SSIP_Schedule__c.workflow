<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Check_Address_Override_Flag</fullName>
        <field>Address_Override__c</field>
        <literalValue>1</literalValue>
        <name>Check Address Override Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_SSIP_Schedule_Status_To_Closed</fullName>
        <field>Status__c</field>
        <literalValue>Closed</literalValue>
        <name>Update SSIP Schedule Status To Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_On_SSIP_ScheduleTo_Skipped</fullName>
        <field>Status__c</field>
        <literalValue>Skipped</literalValue>
        <name>Update Status On SSIP ScheduleTo Skipped</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Check Address Override Flag On Address Change</fullName>
        <actions>
            <name>Check_Address_Override_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When the address tied to the Schedule is changed then the &apos;Address Override Flag&apos; should be set on the Schedule.</description>
        <formula>ISCHANGED(Shipping_Address__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Close SSIP Schedule On Order Creation Rule</fullName>
        <actions>
            <name>Update_SSIP_Schedule_Status_To_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Close SSIP Schedule On Order Creation Rule</description>
        <formula>AND(
 ISPICKVAL(Status__c, &apos;Eligibility Verified&apos;),
 ISCHANGED( Order__c ),
 NOT( ISBLANK(Order__c ) ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update SSIP Schedule Status To Skipped</fullName>
        <actions>
            <name>Update_Status_On_SSIP_ScheduleTo_Skipped</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SSIP_Schedule__c.Skip__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>When the Skip flag is selected for an SSIP Schedule then the Status on the Schedule should be marked as &apos;Skipped&apos;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
