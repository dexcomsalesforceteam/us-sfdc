<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notify_Creator_when_RGA_Order_Fails</fullName>
        <description>Notify Creator when RGA Order Fails</description>
        <protected>false</protected>
        <recipients>
            <field>Creator_Email_Id__c</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Notify_Creator_in_Case_of_RGA_Order_Failure</template>
    </alerts>
    <alerts>
        <fullName>Oracle_Error_Received_on_Order_Header_Created</fullName>
        <description>Oracle Error Received on Order Header Created</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Error_on_Order_Header</template>
    </alerts>
    <alerts>
        <fullName>RGA_Approved</fullName>
        <description>RGA Approved</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RGA_AP_Emails/RGA_Approved</template>
    </alerts>
    <alerts>
        <fullName>RGA_Recalled</fullName>
        <description>RGA Recalled</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RGA_AP_Emails/RGA_Recalled</template>
    </alerts>
    <alerts>
        <fullName>RGA_Rejected</fullName>
        <description>RGA Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RGA_AP_Emails/RGA_Denied</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approve_RGA_Order</fullName>
        <field>RGA_Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Approve RGA Order</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Integration_Error_Order_Header</fullName>
        <field>Integration_Error_Message__c</field>
        <name>Clear Integration Error Order Header</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Oracle_Status</fullName>
        <field>Oracle_Status__c</field>
        <name>Clear Oracle Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Deny_RGA_Order</fullName>
        <field>RGA_Approval_Status__c</field>
        <literalValue>Denied</literalValue>
        <name>Deny RGA Order</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Header_Set_Desync_flag_to_false</fullName>
        <description>Set the Desync flag on the PersonAccount to be false</description>
        <field>Desynchronize_With_Marketing_Cloud__pc</field>
        <literalValue>0</literalValue>
        <name>Order Header Set Desync flag to false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Account__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RGA_Sent_it_to_Oracle</fullName>
        <field>Oracle_Status__c</field>
        <literalValue>Send to Oracle</literalValue>
        <name>RGA Sent it to Oracle</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Flow_Status_Code</fullName>
        <description>Update Flow Status code to Cancelled</description>
        <field>Flow_Status_Code__c</field>
        <formula>&quot;CANCELLED&quot;</formula>
        <name>Update Flow Status Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Order_Ship_Date</fullName>
        <field>Order_Shipped_Date__c</field>
        <formula>Latest_Fulfillment_Date__c</formula>
        <name>Update Order Ship Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_RGA_Approval_Status</fullName>
        <field>RGA_Approval_Status__c</field>
        <literalValue>Submitted for VP Approval</literalValue>
        <name>Update RGA Approval Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_RGA_Approval_Status_to_Recalled</fullName>
        <field>RGA_Approval_Status__c</field>
        <literalValue>Recalled</literalValue>
        <name>Update RGA Approval Status to Recalled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_RGA_Retrun_Reason_for_Sensor</fullName>
        <field>RGA_Return_Reason__c</field>
        <literalValue>VP Approved</literalValue>
        <name>Update RGA Retrun Reason for Sensor</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_RGA_Return_Reason</fullName>
        <field>RGA_Return_Reason__c</field>
        <literalValue>30-Day Guarantee</literalValue>
        <name>Update RGA Return Reason</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_RGA_Return_Reason_To_VP_Approved</fullName>
        <field>RGA_Return_Reason__c</field>
        <literalValue>VP Approved</literalValue>
        <name>Update RGA Return Reason To VP Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status</fullName>
        <description>Update Status to SHIPPED</description>
        <field>Status__c</field>
        <formula>&quot;SHIPPED&quot;</formula>
        <name>Update Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Create_Order_In_Oracle</fullName>
        <apiVersion>41.0</apiVersion>
        <description>Outbound Message will send Order</description>
        <endpointUrl>https://itapi.dexcom.com/ws/soap/createsalesforceorder;boomi_auth=c2FsZXNmb3JjZUBkZXhjb21pbmMtNk82SjBPOjYyODdmOTZhLWY2NWMtNGRjYi1hYzg4LTM3ZDE0ZWVmYjg2Ng==</endpointUrl>
        <fields>Account_Number__c</fields>
        <fields>Charges__c</fields>
        <fields>Id</fields>
        <fields>Oracle_Address_Id__c</fields>
        <fields>Oracle_Order_Type_Id__c</fields>
        <fields>Oracle_User_Name__c</fields>
        <fields>PO_Number__c</fields>
        <fields>Packing_Instructions__c</fields>
        <fields>Price_List__c</fields>
        <fields>Shipping_Instructions__c</fields>
        <fields>Shipping_Method__c</fields>
        <fields>Waive_Shipping_Charges__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>jp0117@dexcom.com</integrationUser>
        <name>Create Order In Oracle</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Immediate_Outbound_Message</fullName>
        <apiVersion>46.0</apiVersion>
        <endpointUrl>https://iad1.qualtrics.com/triggers/api/v1/event?eventType=SalesforceOutboundMessage&amp;s=SV_1NvoiawteB92tPD&amp;u=UR_6A1VCHhdKu95gVv&amp;t=OC_wLiqSCyz0gRM5RD&amp;b=dexcom</endpointUrl>
        <fields>Account__c</fields>
        <fields>Customer_Email__c</fields>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>rs0216@dexcom.com</integrationUser>
        <name>Immediate Outbound Message</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Send_New_Order_Survey</fullName>
        <apiVersion>46.0</apiVersion>
        <endpointUrl>https://iad1.qualtrics.com/triggers/api/v1/event?eventType=SalesforceOutboundMessage&amp;s=SV_e3YFVtt1GSSQdzT&amp;u=UR_6A1VCHhdKu95gVv&amp;t=OC_3kLpYyQlBJDrJ1X&amp;b=dexcom</endpointUrl>
        <fields>Account_Number__c</fields>
        <fields>Account__c</fields>
        <fields>Assigned_To__c</fields>
        <fields>CreatedById</fields>
        <fields>Customer_Email__c</fields>
        <fields>Id</fields>
        <fields>Order_ID__c</fields>
        <fields>Order_Type__c</fields>
        <fields>PayorName__c</fields>
        <fields>Territory__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>salesforce.admin@dexcom.com.us</integrationUser>
        <name>Send New Order Survey</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Send_Reorder_Survey</fullName>
        <apiVersion>46.0</apiVersion>
        <endpointUrl>https://iad1.qualtrics.com/triggers/api/v1/event?eventType=SalesforceOutboundMessage&amp;s=SV_d76Bq5f463LRJw9&amp;u=UR_6A1VCHhdKu95gVv&amp;t=OC_9sEdXpAhyygjUwt&amp;b=dexcom</endpointUrl>
        <fields>Account_Number__c</fields>
        <fields>Account__c</fields>
        <fields>Assigned_To__c</fields>
        <fields>CreatedById</fields>
        <fields>Customer_Email__c</fields>
        <fields>Id</fields>
        <fields>Order_ID__c</fields>
        <fields>Order_Type__c</fields>
        <fields>PayorName__c</fields>
        <fields>Territory__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>rs0216@dexcom.com</integrationUser>
        <name>Send Reorder Survey</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Clear Integration Error on Order Header</fullName>
        <actions>
            <name>Clear_Integration_Error_Order_Header</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear_Oracle_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow will clear out integration error message when Auth Code is updated</description>
        <formula>AND(
    ISCHANGED(Auth_Code__c) ,
				NOT(ISBLANK(Auth_Code__c)),
				NOT(ISBLANK(Integration_Error_Message__c ))
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Dexcom_CARE_Monthly_Transactional_Survey_New</fullName>
        <active>true</active>
        <formula>AND(
 Account__r.Email_Opt_In__c ,
     Status__c = &apos;SHIPPED&apos;,
				 Flow_Status_Code__c = &apos;CLOSED&apos;,
				 OR(
					                                Order_Type__c = &apos;Standard Sales Order&apos;,
									Order_Type__c = &apos;MC Standard Sales Order&apos;,
									Order_Type__c = &apos;K-Code Commerical&apos;
					),
				 OR(
					    ISBLANK(Account__r.First_Order_Ship_Date__c),
									AND(
									     NOT(ISPICKVAL(Account__r.Customer_Type__c, &apos;Medicare FFS&apos;)),
													 Account__r.Last_Standard_Sales_Order_Ship_Date__c &lt;  (TODAY() - 730)
									),
									AND(
									     ISPICKVAL(Account__r.Customer_Type__c, &apos;Medicare FFS&apos;),
													 Account__r.Last_MC_Order_Shipped_Date__c &lt;  (TODAY() - 730)  
									)
					),
    				  NOT(CONTAINS(UPPER(Account__r.Payor__r.Name), &apos;KAISER&apos;)) ,
				NOT(ISBLANK(Customer_Email__c))
  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Dexcom_CARE_Monthly_Transactional_Survey_Reorder</fullName>
        <active>false</active>
        <formula>AND(
 Account__r.Email_Opt_In__c ,
Status__c = &apos;SHIPPED&apos;,
Flow_Status_Code__c = &apos;CLOSED&apos;,
OR(
Order_Type__c = &apos;Standard Sales Order&apos;,
Order_Type__c = &apos;MC Standard Sales Order&apos;,
Order_Type__c = &apos;K-Code Commerical&apos;,
Order_Type__c = &apos;K-Code Commercial&apos;
),
OR(
NOT(ISBLANK(Account__r.First_Order_Ship_Date__c)),
AND(
NOT(ISPICKVAL(Account__r.Customer_Type__c, &apos;Medicare FFS&apos;)),
Account__r.Last_Standard_Sales_Order_Ship_Date__c &gt; (TODAY() - 730)
),
AND(
ISPICKVAL(Account__r.Customer_Type__c, &apos;Medicare FFS&apos;),
Account__r.Last_MC_Order_Shipped_Date__c &gt; (TODAY() - 730)
)
),
NOT(CONTAINS(UPPER(Account__r.Payor__r.Name), &apos;KAISER&apos;)) ,
NOT(ISBLANK(Customer_Email__c))
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Order header created</fullName>
        <actions>
            <name>Order_Header_Set_Desync_flag_to_false</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order_Header__c.CreatedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Consumers</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Send Notification On RGA Integration Fail</fullName>
        <actions>
            <name>Notify_Creator_when_RGA_Order_Fails</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order_Header__c.RGA_Approval_Status__c</field>
            <operation>equals</operation>
            <value>No Approval Required,Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order_Header__c.Integration_Error_Message__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This Workflow is created to send Email notification and create task when RGA order has error and did not create an order. This workflow is created for CRMSF-4714</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send Order To Oracle</fullName>
        <actions>
            <name>Create_Order_In_Oracle</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Workflow will generate a outbound message to be sent to Oracle for Order creation.</description>
        <formula>OR(
   AND(
       ISNEW(),
       Order_Type__c = &apos;MC Standard Sales Order&apos;,
       NOT(CONTAINS(CreatedBy.FirstName, &apos;Actian&apos;)),
       ISBLANK(Order_ID__c)
      ),
   AND(
      ISCHANGED(Send_To_Oracle__c),
      Send_To_Oracle__c = true,
      ISBLANK(Order_ID__c)
      ), 
   AND(
       ISCHANGED( Oracle_Status__c),
       ISPICKVAL(Oracle_Status__c, &quot;Send to Oracle&quot;),
       ISBLANK(Order_ID__c)
      )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Order Ship Date</fullName>
        <actions>
            <name>Update_Order_Ship_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When the order status is updated to SHIPPED field Order Ship Date will be updated</description>
        <formula>AND( 
	ISCHANGED( Status__c ), 
	Status__c = &apos;SHIPPED&apos;,
	Flow_Status_Code__c != &apos;CANCELLED&apos;
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Return Reason 30 Day Guarantee</fullName>
        <actions>
            <name>Update_RGA_Return_Reason</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order_Header__c.RGA_Approval_Status__c</field>
            <operation>equals</operation>
            <value>Hardware Approval Required</value>
        </criteriaItems>
        <description>This is a temp WF that will be removed when we activate the approval process for RGA Orders in Salesforce.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Return Reason VP Approved</fullName>
        <actions>
            <name>Update_RGA_Retrun_Reason_for_Sensor</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order_Header__c.RGA_Approval_Status__c</field>
            <operation>equals</operation>
            <value>Sensor Approval Required</value>
        </criteriaItems>
        <description>This is a temp WF that will be removed when we activate the approval process for RGA Orders in Salesforce.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Status on Line Cancel</fullName>
        <actions>
            <name>Update_Flow_Status_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order_Header__c.Count_of_Line_Items__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order_Header__c.Quantity__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order_Header__c.Flow_Status_Code__c</field>
            <operation>notEqual</operation>
            <value>CANCELLED</value>
        </criteriaItems>
        <description>If the Order Detail line count is 1 and the quantity is 0, set Status to Shipped and Flow Status Code to Cancelled</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
