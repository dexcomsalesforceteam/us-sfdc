<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_External_Id_WIth_Order_Number</fullName>
        <field>External_Id__c</field>
        <formula>External_Id__c &amp; &quot; - &quot; &amp;  Order_Number__c</formula>
        <name>Update External Id WIth Order Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_MDCR_Followup_Status</fullName>
        <field>MDCR_Followup_Status__c</field>
        <literalValue>Rescheduled - Inpatient Facility</literalValue>
        <name>Update MDCR Followup Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_MFR_Ext_Id</fullName>
        <description>Update MDCR Ext Id.</description>
        <field>External_Id__c</field>
        <formula>Customer__r.Account_18_digit_ID__c + &quot;-&quot; +  TEXT(TODAY())</formula>
        <name>Update MFR Ext Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Next_Follow_up_Date_On_Account</fullName>
        <field>Next_MDCR_Reorder_Follow_up_Date__c</field>
        <formula>Expected_To_Be_Home_On__c +1</formula>
        <name>Update Next Follow-up Date On Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Customer__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Receiver_Attestation_Completed</fullName>
        <field>Attestation_Question_Answered__c</field>
        <literalValue>Yes</literalValue>
        <name>Update_Receiver_Attestation_Completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Attestation_Complete_If_Web_Pending</fullName>
        <actions>
            <name>Update_Receiver_Attestation_Completed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Mark Attestation completed if MFR created with status Web Pending</description>
        <formula>AND(
	RecordType.Name =&apos;US Reorder - G6&apos;,
	ISCHANGED(MDCR_Followup_Status__c),
	ISPICKVAL(MDCR_Followup_Status__c , &apos;Web Pending&apos;),
	Customer__r.Latest_Transmitter_Generation_Shipped__c = &apos;G5&apos;,
	Customer__r.Latest_Receiver_Generation_Shipped__c = &apos;G5 Touch Screen&apos;
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Expected To Be Home On</fullName>
        <active>false</active>
        <criteriaItems>
            <field>MDCR_Followup__c.Patient_Currently_In_Facility__c</field>
            <operation>notEqual</operation>
            <value>None</value>
        </criteriaItems>
        <description>When Patient Currently In Facility? is not None and Expected To Be Home On date is updated, update the MDCR Followup Status to &quot;Rescheduled - Inpatient Facility&quot;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update External Id With Order Number On MFR</fullName>
        <actions>
            <name>Update_External_Id_WIth_Order_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(
 NOT( ISNEW() ),
	NOT(ISBLANK(Order_Number__c)),			
 ISCHANGED(Order_Number__c) ,
	NOT(CONTAINS(External_Id__c, &apos;Sub&apos;)),
	NOT(CONTAINS(External_Id__c, &apos;Admin&apos;))											
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update MDCR Followup Status</fullName>
        <actions>
            <name>Update_MDCR_Followup_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Next_Follow_up_Date_On_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>MDCR_Followup__c.Expected_To_Be_Home_On__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>MDCR_Followup__c.Patient_Currently_In_Facility__c</field>
            <operation>notEqual</operation>
            <value>Not In Facility</value>
        </criteriaItems>
        <description>When Patient Currently In Facility? is not None and Expected To Be Home On date is updated, update the MDCR Followup Status to &quot;Rescheduled - Inpatient Facility&quot;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update MFR External Id</fullName>
        <actions>
            <name>Update_MFR_Ext_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update the External Id when MFR is created manually</description>
        <formula>AND(
								ISBLANK( External_Id__c )

)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
