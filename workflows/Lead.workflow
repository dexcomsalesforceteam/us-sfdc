<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>LDR_Status_Q</fullName>
        <field>LDR_Status__c</field>
        <literalValue>Q Convert: Did Not Contact</literalValue>
        <name>LDR Status Q</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_set_desync_to_false</fullName>
        <field>Desynchronize_With_Marketing_Cloud__c</field>
        <literalValue>0</literalValue>
        <name>Lead set desync to false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Service_Lead_Owner_Update</fullName>
        <field>OwnerId</field>
        <lookupValue>servicelead@dexcom.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Service Lead Owner Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Assign to LDR Queue Phreesia</fullName>
        <actions>
            <name>FU_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Website</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Dex_Campaign_Name__c</field>
            <operation>equals</operation>
            <value>Phreesia (data pass)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Consumer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.IsConverted</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Is_Received_After_Hours__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Assign leads to US LDR record queue for Phreesia</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Convert Qualified Leads</fullName>
        <actions>
            <name>LDR_Status_Q</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Service_Lead_Owner_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Qualified</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Website</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Consumer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.hasAccount__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LDR_Status__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update First Attempt Date</fullName>
        <actions>
            <name>FU_First_Attempt_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LDR_Status__c</field>
            <operation>equals</operation>
            <value>1st Attempt,WAG 1st Attempt</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.First_Attempt_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>When first attempt the value in the LDR Status field, update the First Attempt Date to Today</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>email address changes from blank</fullName>
        <actions>
            <name>Lead_set_desync_to_false</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Email</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Consumer</value>
        </criteriaItems>
        <description>If the email address changes from blank to not blank.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
