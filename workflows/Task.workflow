<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Supervisor_Receives_Email_Alert_On_Assigning_Escalation_Caused_By</fullName>
        <description>Supervisor Receives Email Alert On Assigning Escalation Caused By</description>
        <protected>false</protected>
        <recipients>
            <field>Supervisor_Email_Of_Escalated_User__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Supervisor_Received_Escalation_Mail</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Escalation_Resolved_By</fullName>
        <field>Escalation_Resolved_By__c</field>
        <formula>$User.FirstName +&quot; &quot;+ $User.LastName</formula>
        <name>Update Escalation Resolved By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Owner_Title_On_Task</fullName>
        <description>This field is updated at task creation</description>
        <field>Owner_Title_At_Creation__c</field>
        <formula>Owner:User.Title</formula>
        <name>Update Owner Title On Task</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Supervisor_Email_On_Task</fullName>
        <description>This field update is created to update Supervisor&apos;s email of the person who is marked under Escalation Caused By field. Created for CRMSF-4363</description>
        <field>Supervisor_Email_Of_Escalated_User__c</field>
        <formula>Escalation_Caused_By__r.Manager_Email__c</formula>
        <name>Update Supervisor Email On Task</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>FU Task End Date</fullName>
        <actions>
            <name>FU_Task_End_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Changed workflow under CRMSF-4354, to set the end date only when task is closed</description>
        <formula>AND(
				ISCHANGED(IsClosed),
				IsClosed= TRUE
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Escalation Task Resolved By</fullName>
        <actions>
            <name>Update_Escalation_Resolved_By</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( ISCHANGED( Status ),
(ISPICKVAL( Status , &quot;Completed&quot;) ),
RecordType.DeveloperName = &quot;Escalation_Task&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Owner Title At Task Creation</fullName>
        <actions>
            <name>Update_Owner_Title_On_Task</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow will update the Owner Title in Owner Title At Creation field which is used for reporting</description>
        <formula>ISNEW()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Supervisor Email When Escalation Caused By User Is Set</fullName>
        <actions>
            <name>Supervisor_Receives_Email_Alert_On_Assigning_Escalation_Caused_By</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Supervisor_Email_On_Task</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow is created to update Supervisor&apos;s email when Escalation Caused By Filed is updated. Created for CRMSF-4363</description>
        <formula>AND(RecordType.DeveloperName =&quot;Escalation_Task&quot;,
NOT( ISBLANK( Escalation_Caused_By__c )),
OR( ISNEW() ,ISCHANGED( Escalation_Caused_By__c )))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
