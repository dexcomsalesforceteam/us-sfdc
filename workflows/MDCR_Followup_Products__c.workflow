<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Max_Qty_On_MFR_Products</fullName>
        <description>Update Max Qty on MDCR Products based on Product updates</description>
        <field>Max_Qty__c</field>
        <formula>CASE(Product__r.Name,
&quot;STS-MC-001&quot;, MDCR_Followup__r.Customer__r.MDCR_Sensors_Count__c,
&quot;301936586218&quot;, MDCR_Followup__r.Customer__r.MDCR_Lancets_Count__c,
&quot;301937308505&quot;, MDCR_Followup__r.Customer__r.MDCR_Test_Strips_Count__c,
1)</formula>
        <name>Update Max Qty On MFR Products</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Order_Qty_On_MFR_Products</fullName>
        <description>Rule will update the Order Quantity on the MFR Products based on the Max Qty and Unused Qty</description>
        <field>Order_Qty__c</field>
        <formula>IF(ISBLANK(Max_Qty__c), 0, IF(Unused_Qty__c &gt; Max_Qty__c, 0, Max_Qty__c - Unused_Qty__c))</formula>
        <name>Update Order Qty On MFR Products</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Subscription_Flag_on_MFR_Product</fullName>
        <description>Sets the flag to denote if the product is a subscription product or not.</description>
        <field>Is_Subscription_Product__c</field>
        <literalValue>1</literalValue>
        <name>Update Subscription Flag on MFR Product</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Max Qty On MDCR Followup Products</fullName>
        <actions>
            <name>Update_Max_Qty_On_MFR_Products</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Order_Qty_On_MFR_Products</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Rule updates the Max Qty based on the Product selected for MDCR Followup Products</description>
        <formula>AND ( 	
	CreatedBy.Profile.Name &lt;&gt; &apos;System Administrator&apos;, 	
	OR(ISNEW(), ISCHANGED(Product__c))
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Subscription Flag on MFR Product</fullName>
        <actions>
            <name>Update_Subscription_Flag_on_MFR_Product</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>MDCR_Followup_Products__c.Product_Name__c</field>
            <operation>contains</operation>
            <value>SUB</value>
        </criteriaItems>
        <description>Rule will the flag to denote if this is a subscription product.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
