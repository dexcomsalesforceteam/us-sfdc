<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Marketing_Interaction_Copy</fullName>
        <description>This is to copy our formula field into our text field to stop duplicate communication entries.</description>
        <field>Unique_Shipment_Confirmation__c</field>
        <formula>Unique_Shipment_Confirmation_Formula__c</formula>
        <name>Marketing Interaction Copy</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Shipment Confirmation</fullName>
        <actions>
            <name>Marketing_Interaction_Copy</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Marketing_Interaction__c.Communication_Type__c</field>
            <operation>equals</operation>
            <value>Shipment Confirmation</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
