<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>G6_Launch_UK_and_IE</fullName>
        <description>G6 Launch - UK and IE</description>
        <protected>false</protected>
        <recipients>
            <field>PersonContact2__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK_IE_Emails/Getting_StartedUKIE</template>
    </alerts>
    <alerts>
        <fullName>IE_First_Communication_to_be_sent_out_automatically</fullName>
        <description>IE - First Communication to be sent out automatically</description>
        <protected>false</protected>
        <recipients>
            <field>PersonContact2__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>ie.sales@dexcom.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>UK_IE_Emails/Dexcom_Information_IE</template>
    </alerts>
    <alerts>
        <fullName>Send_15_Days_Reminder_For_Open_IE_Subscription_Opp</fullName>
        <description>Send 15 Days Reminder For Open IE Subscription Opp</description>
        <protected>false</protected>
        <recipients>
            <field>PersonContact2__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>ie.sales@dexcom.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>UK_IE_Emails/G6_15_Days_Reminder_IE</template>
    </alerts>
    <alerts>
        <fullName>Send_15_Days_Reminder_For_Open_UK_Subscription_Opp</fullName>
        <description>Send 15 Days Reminder For Open UK Subscription Opp</description>
        <protected>false</protected>
        <recipients>
            <field>PersonContact2__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>gb.sales@dexcom.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>UK_IE_Emails/G6_15_Days_Reminder_GB</template>
    </alerts>
    <alerts>
        <fullName>Send_23_Days_Reminder_For_Open_IE_Subscription_Opp</fullName>
        <description>Send 23 Days Reminder For Open IE Subscription Opp</description>
        <protected>false</protected>
        <recipients>
            <field>PersonContact2__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>ie.sales@dexcom.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>UK_IE_Emails/G6_23_Days_Reminder_IE</template>
    </alerts>
    <alerts>
        <fullName>Send_23_Days_Reminder_For_Open_UK_Subscription_Opp</fullName>
        <description>Send 23 Days Reminder For Open UK Subscription Opp</description>
        <protected>false</protected>
        <recipients>
            <field>PersonContact2__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>gb.sales@dexcom.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>UK_IE_Emails/G6_23_Days_Reminder_GB</template>
    </alerts>
    <alerts>
        <fullName>UK_Cancelled_Opp_Final_Attempt_Email</fullName>
        <description>UK - Cancelled Opp - Final Attempt Email</description>
        <protected>false</protected>
        <recipients>
            <field>PersonContact2__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>UK_IE_Emails/Final_Attempt_to_Contact_You_Regarding_Your_Recent_Enquiry</template>
    </alerts>
    <alerts>
        <fullName>UK_First_Communication_to_be_sent_out_automatically</fullName>
        <description>UK - First Communication to be sent out automatically</description>
        <protected>false</protected>
        <recipients>
            <field>PersonContact2__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>gb.sales@dexcom.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>UK_IE_Emails/Dexcom_Information_GB</template>
    </alerts>
    <fieldUpdates>
        <fullName>Clear_Opp_Status</fullName>
        <field>Status__c</field>
        <name>Clear Opp Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Set_Desync_to_false</fullName>
        <description>set the Desync flag on the PersonAccount to be false with a field Update</description>
        <field>Desynchronize_With_Marketing_Cloud__pc</field>
        <literalValue>0</literalValue>
        <name>Opportunity Set Desync to false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Document_Status_On_Account</fullName>
        <description>Set the document status to No.</description>
        <field>All_Documents_Received__c</field>
        <literalValue>No</literalValue>
        <name>Set Document Status On Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Medicare_Risk_Flag_On_Account_True</fullName>
        <description>Field will set Medicare Risk on the Account to true when AOB and Chartnotes documents are not verified, but the Order is booked.</description>
        <field>Medicare_Risk__c</field>
        <literalValue>1</literalValue>
        <name>Set Medicare Risk Flag On Account True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Close_Reason_For_Shared_Opps</fullName>
        <field>Close_Reason__c</field>
        <literalValue>Converted to Sale (DME)</literalValue>
        <name>Update Close Reason For Shared Opps</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Close_Reason_to_Cancelled_Age</fullName>
        <field>Close_Reason__c</field>
        <literalValue>Cancelled - Age</literalValue>
        <name>Update Close Reason to Cancelled - Age</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Marketing_Sales_Channel_On_Accnt</fullName>
        <description>Jagan 01/24/2018 - Field update action to update the Marketing Sales Channel on Account</description>
        <field>marketing_sales_channel__c</field>
        <formula>TEXT(Sales_Channel__c)</formula>
        <name>Update Marketing Sales Channel On Accnt</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opp_Close_Date_To_Today</fullName>
        <description>Update Opportunity Closed Date when Opp is Closed</description>
        <field>CloseDate</field>
        <formula>TODAY()</formula>
        <name>Update Opp Close Date To Today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opportunity_Doc1Status_Docusign</fullName>
        <description>Doc 1 Status : Received</description>
        <field>Doc_1_Status__c</field>
        <literalValue>Received</literalValue>
        <name>Update Opportunity Doc1Status Docusign</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opportunity_Stage_For_Docusign</fullName>
        <description>Stage : 4 - Doc Collection</description>
        <field>StageName</field>
        <literalValue>4. Doc Collection</literalValue>
        <name>Update Opportunity Stage For Docusign</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opportunity_Status_For_Docusign</fullName>
        <description>Update Opportunity status to 4.1</description>
        <field>Status__c</field>
        <literalValue>4.1 Identify Docs to Collect</literalValue>
        <name>Update Opportunity Status For Docusign</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_To_Blank</fullName>
        <field>Status__c</field>
        <name>Update Status To Blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_UK_IE_G6_Email_Sub_Flag</fullName>
        <field>UK_IE_G6_Email_Sub_Stop__c</field>
        <literalValue>1</literalValue>
        <name>Update UK IE G6 Email Sub Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Verified_Distributor</fullName>
        <field>Verified_Distributor__c</field>
        <literalValue>1</literalValue>
        <name>Verified Distributor</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Cancel an Opportunity once it hits 100 Days Old</fullName>
        <active>true</active>
        <description>If UK/IE Opportunities is open after 100 days from created date then updating stage, status, close reason values.</description>
        <formula>AND(
OR(
				RecordType.Name  = &apos;IE Opportunity&apos; , 
				RecordType.Name  = &apos;GB Opportunity&apos;
),
				 IsClosed = false
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Clear_Opp_Status</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Opp_Stage_FU_to_10_Cancelled</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_Close_Reason_to_Cancelled_Age</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.CreatedDate</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Cancel_Hold_Opps_After_6_Months</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>US Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>7. On Hold</value>
        </criteriaItems>
        <description>If a patient file is in a 7. Hold Stage after 6 months of the status change date it will be moved to a cancelled stage.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Clear_Opp_Status</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Opp_Stage_FU_to_10_Cancelled</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.Stage_Update_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Docusign_Document_Status_Update_Rule</fullName>
        <actions>
            <name>Update_Opportunity_Doc1Status_Docusign</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Opportunity_Stage_For_Docusign</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Opportunity_Status_For_Docusign</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Opportunity Updates</description>
        <formula>AND(
  ISCHANGED( Docusign_Completed_Envelope_Id__c ),
NOT(ISBLANK(Docusign_Completed_Envelope_Id__c)),			
NOT(
OR(
ISPICKVAL(Status__c , &quot;2.1A BI Pending - MCS&quot;),
ISPICKVAL(Status__c , &quot;4.6 Doc Collection Completed&quot;),
ISPICKVAL(Status__c , &quot;5.1 Auth Submitted to Payor - MCS&quot;),
ISPICKVAL(Status__c , &quot;5.1B Auth Recd by Payor Confirmed - MCS&quot;),
ISPICKVAL(Status__c , &quot;5.4 Insurance Correspondence Received&quot;)
)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>IE - First Communication to be sent out automatically</fullName>
        <actions>
            <name>IE_First_Communication_to_be_sent_out_automatically</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>IE - First Communication to be sent out automatically on opportunity create.</description>
        <formula>RecordTypeId  = &apos;012330000009nsJ&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>IE Subscription Reminder</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>IE Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>G6 Subscription</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.UK_IE_G6_Email_Sub_Stop__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>This workflow will send reminders to IE subscription users</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_23_Days_Reminder_For_Open_IE_Subscription_Opp</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.CreatedDate</offsetFromField>
            <timeLength>23</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Send_15_Days_Reminder_For_Open_IE_Subscription_Opp</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.CreatedDate</offsetFromField>
            <timeLength>15</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>On Create</fullName>
        <actions>
            <name>Opportunity_Set_Desync_to_false</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.CreatedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Consumers</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Onboarding - Verified Distributor</fullName>
        <actions>
            <name>Verified_Distributor</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2 OR 3 OR 4) AND 5</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Status__c</field>
            <operation>equals</operation>
            <value>2.2B Distributor - Verified</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Status__c</field>
            <operation>equals</operation>
            <value>4.5 Distributor-Collecting Docs</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Status__c</field>
            <operation>equals</operation>
            <value>5.2 Distributor-Auth Submitted to Payor</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Status__c</field>
            <operation>equals</operation>
            <value>6.4 Distributor Quote Pending</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Verified_Distributor__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <description>Update Verified Distributor flag on account if Opp is updated to Dist related status</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Medicare Risk Flag On Account</fullName>
        <actions>
            <name>Set_Document_Status_On_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Medicare_Risk_Flag_On_Account_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Workflow will check the Medicare Risk flag if Chartnotes is not filled for the Opportunity. This will fire when Order # is updated on Opportunity.
Jagan 12/8/2017 - Deactivated the Workflow as we are discontinuing the Medicare at Risk process.</description>
        <formula>AND (
ISCHANGED( Order_NUM__c ),
NOT(ISBLANK(Order_NUM__c)),
NOT(TEXT(Type) = &apos;Comp Product&apos;),
TEXT(Account.Customer_Type__c) = &apos;Medicare FFS&apos;,
TEXT(Doc_3_Status__c) != &apos;Verified&apos;
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Stage 6 Stamping</fullName>
        <actions>
            <name>FU_Stage_6_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>6. Quote Pending</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Stage_6_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Existing_Consumer_Opp__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>NEW SYSTEM,Physician Referral,Medicare – New Patient</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>US Opportunity</value>
        </criteriaItems>
        <description>When an opp becomes Stage 6, stamp the Stage 6 Date object with the current date.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>UK - Auto Email for Cancelled Opps</fullName>
        <actions>
            <name>UK_Cancelled_Opp_Final_Attempt_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>GB Opportunity,IE Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Close_Reason__c</field>
            <operation>equals</operation>
            <value>Non responsive</value>
        </criteriaItems>
        <description>Sends an automatic email for UK Cancelled Opps that have the Non Responsive close reason.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>UK - First Communication to be sent out automatically</fullName>
        <actions>
            <name>UK_First_Communication_to_be_sent_out_automatically</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>UK - First Communication to be sent out automatically on opportunity create.</description>
        <formula>RecordTypeId  = &apos;012330000009nsI&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>UK Subscription Reminder</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>GB Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>G6 Subscription</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.UK_IE_G6_Email_Sub_Stop__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>This workflow will send reminders to UK subscription users</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Send_23_Days_Reminder_For_Open_UK_Subscription_Opp</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.CreatedDate</offsetFromField>
            <timeLength>23</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Send_15_Days_Reminder_For_Open_UK_Subscription_Opp</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.CreatedDate</offsetFromField>
            <timeLength>15</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>UK and IE Opp Email for G6 Launch</fullName>
        <actions>
            <name>G6_Launch_UK_and_IE</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>GB Opportunity,IE Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>61. Quote Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>NEW SYSTEM</value>
        </criteriaItems>
        <description>This workflow will send the &quot;Getting Started - UK and IE&quot; G6 email to any Opp&apos;s that are changed to be &quot;NEW SYSTEM&quot; type and in &quot;Quote Approved&quot; Stage.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>UK%2FIE G6 Email Sub Stop</fullName>
        <actions>
            <name>Update_UK_IE_G6_Email_Sub_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>3. Referred to Channel Partner,6. Quote Pending,61. Quote Approved,7. On Hold,8. TSM,81. Field Hold,9. Appeals,10. Cancelled,11. QC Hold,13. Pharma Retail</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>GB Opportunity,IE Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>G6 Subscription</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Marketing Sales Channel On Accnt</fullName>
        <actions>
            <name>Update_Marketing_Sales_Channel_On_Accnt</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Jagan 01/24/18 - Update Marketing Sales Channel on Account based on Sales Channel on Opportunity record.</description>
        <formula>AND( $RecordType.Name = &apos;US Opportunity&apos;, OR( 	AND( 		ISNEW(), 		NOT(ISBLANK(TEXT(Sales_Channel__c))) 	), 	OR( 		ISCHANGED(Sales_Channel__c), 		AND(NOT(ISBLANK(TEXT(PRIORVALUE(Sales_Channel__c)))), ISBLANK(TEXT(Sales_Channel__c))) 	) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Opp Close Date</fullName>
        <actions>
            <name>Update_Opp_Close_Date_To_Today</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Update Opp Close Date when Opportunity is Closed</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update_Status_And_Close_Reason_When_Closed_For_Shared_Opps</fullName>
        <actions>
            <name>Update_Close_Reason_For_Shared_Opps</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Status_To_Blank</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When Received Connection Name is not blank and stage updated to 61.Qoute Approved the delete value in status field and update close reason to Converted to Sale (DME)</description>
        <formula>AND(
    NOT(ISBLANK(Received_Connection_Name__c)),
				ISCHANGED(StageName),
				ISPICKVAL(StageName,&apos;61. Quote Approved&apos;)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
