<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>FU_External_ID</fullName>
        <description>Update the external id using header id and product</description>
        <field>External_ID__c</field>
        <formula>Header_ID__c  &amp;  Item_Number__c</formula>
        <name>FU - External ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FU_Unique_Key</fullName>
        <description>Update the Unique Key field</description>
        <field>Unique_Key__c</field>
        <formula>Order_Item_18_Digit_ID__c</formula>
        <name>FU - Unique Key</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Account_Id_On_Order_Item_Detail</fullName>
        <description>Populate the account id on the Order item detail record upon creation.</description>
        <field>ACC_ID__c</field>
        <formula>Order_Header__r.Account__c</formula>
        <name>Populate Account Id On Order Item Detail</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Receiver_Generation</fullName>
        <field>Latest_Receiver_Generation__c</field>
        <formula>Generation__c</formula>
        <name>Update Receiver Generation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Order_Header__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Sensor_Generation</fullName>
        <field>Latest_Sensor_Generation__c</field>
        <formula>Generation__c</formula>
        <name>Update Sensor Generation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Order_Header__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Transmitter_Generation</fullName>
        <field>Latest_Transmitter_Generation__c</field>
        <formula>Generation__c</formula>
        <name>Update Transmitter Generation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Order_Header__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Create Order Item Unique Key</fullName>
        <actions>
            <name>FU_Unique_Key</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Creates the key for order item to prevent dups</description>
        <formula>$Profile.Name  != &apos;Data Integrator&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Populate Account Id On Order Item Detail</fullName>
        <actions>
            <name>Populate_Account_Id_On_Order_Item_Detail</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order_Item_Detail__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Populate account id on Order item detail</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Order Item External ID</fullName>
        <actions>
            <name>FU_External_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update External ID in Order Item when a Header ID is updated</description>
        <formula>OR(ISCHANGED( Header_ID__c ) ,  ISNEW() )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Receiver Generation</fullName>
        <actions>
            <name>Update_Receiver_Generation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order_Item_Detail__c.Item_Number__c</field>
            <operation>contains</operation>
            <value>STK,STR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order_Item_Detail__c.Shipping_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This will update generation from order item detail to order header.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Sensor Generation</fullName>
        <actions>
            <name>Update_Sensor_Generation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order_Item_Detail__c.Item_Number__c</field>
            <operation>contains</operation>
            <value>STS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order_Item_Detail__c.Shipping_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This will update generation from order item detail to order header.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Transmitter Generation</fullName>
        <actions>
            <name>Update_Transmitter_Generation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order_Item_Detail__c.Item_Number__c</field>
            <operation>contains</operation>
            <value>STT</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order_Item_Detail__c.Shipping_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This will update generation from order item detail to order header.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
