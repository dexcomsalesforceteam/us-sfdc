<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <outboundMessages>
        <fullName>Send_Escalation</fullName>
        <apiVersion>48.0</apiVersion>
        <description>When an escalation moved to resolved, a survey will trigger to the patient.</description>
        <endpointUrl>https://iad1.qualtrics.com/triggers/api/v1/event?eventType=SalesforceOutboundMessage&amp;s=SV_5tEC5m7fttTp1cN&amp;u=UR_6A1VCHhdKu95gVv&amp;t=OC_3ZRVXANaqxNFwdn&amp;b=dexcom</endpointUrl>
        <fields>Account_Name__c</fields>
        <fields>Account_Number__c</fields>
        <fields>Account_Person_Email__c</fields>
        <fields>Assigned_To_Group__c</fields>
        <fields>Assigned_To_user__c</fields>
        <fields>Id</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>salesforce.admin@dexcom.com.us</integrationUser>
        <name>Send Escalation</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Dexcom Escalation Surveys</fullName>
        <actions>
            <name>Send_Escalation</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Escalation__c.Status__c</field>
            <operation>equals</operation>
            <value>Resolved</value>
        </criteriaItems>
        <description>When an escalation moved to resolved, a survey will trigger to the patient.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
