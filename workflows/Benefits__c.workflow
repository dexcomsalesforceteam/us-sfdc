<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>MemberID_Update</fullName>
        <description>Populate MemberID from MBI for Medicare patients.</description>
        <field>MEMBER_ID__c</field>
        <formula>Medicare_Beneficiary_Identifier__c</formula>
        <name>MemberID Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Send_To_Oracle_Flag</fullName>
        <field>Send_to_Oracle__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Send To Oracle Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_BI_Request_Date</fullName>
        <field>BI_Request_Date__c</field>
        <formula>NOW()</formula>
        <name>Update BI Request Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Is_Update_In_Progress</fullName>
        <description>On submission of outbound message to Oracle, change flag to show in Process</description>
        <field>Is_Update_In_Progress__c</field>
        <literalValue>1</literalValue>
        <name>Update Is Update In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Last_Benefits_Check_Done_By</fullName>
        <description>Updated field  Update Last Benefits Check Done By On Benefit</description>
        <field>Last_Benefits_Check_Done_By__c</field>
        <formula>$User.FirstName + &apos; &apos; +  $User.LastName</formula>
        <name>Update Last Benefits Check Done By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Oracle_Relationship_Id_Value</fullName>
        <field>Oracle_Relationship_ID__c</field>
        <formula>Account__r.Party_ID__c + &apos;-&apos; + TEXT(Benefit_Hierarchy__c)</formula>
        <name>Update Oracle Relationship Id Value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Plan_Type_Med_Adv</fullName>
        <field>Plan_Type__c</field>
        <literalValue>MED ADV</literalValue>
        <name>Update_Plan_Type_Med_Adv</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Upsert_Benefits_In_Oracle</fullName>
        <apiVersion>43.0</apiVersion>
        <description>Outbound Message will send Benefits to Oracle</description>
        <endpointUrl>https://itapi-dev.dexcom.com/ws/soap/createsalesforcebenefits;boomi_auth=c2FsZXNmb3JjZUBkZXhjb21pbmMtNk82SjBPOmUxMjUxYTczLTE4NjQtNDRiZi04MjE3LTZiYzlmODYyNGJmZA==</endpointUrl>
        <fields>Account__c</fields>
        <fields>At_Risk_Entity_Name__c</fields>
        <fields>At_Risk_Entity_Type__c</fields>
        <fields>Benefit_Hierarchy__c</fields>
        <fields>CGM_Coverage_Policy__c</fields>
        <fields>CO_PAY__c</fields>
        <fields>Calendar_year_benefits__c</fields>
        <fields>Claims_Mailing_Address__c</fields>
        <fields>Coverage__c</fields>
        <fields>Customer_Service_Phone__c</fields>
        <fields>DEXCOM_IN_OUT_NETWORK__c</fields>
        <fields>DME_MAX__c</fields>
        <fields>DME_MET__c</fields>
        <fields>EFFECTIVE_DATE__c</fields>
        <fields>Employer_Group__c</fields>
        <fields>FAMILY_DEDUCT__c</fields>
        <fields>FAMILY_OOP_MAX__c</fields>
        <fields>FAMILY_OOP_MET__c</fields>
        <fields>FULLTIME_STUDENT__c</fields>
        <fields>Family_Met__c</fields>
        <fields>Host_Plan_or_Network__c</fields>
        <fields>INDIVIDUAL_DEDUCTIBLE__c</fields>
        <fields>INDIVIDUAL_MET__c</fields>
        <fields>INDIVIDUAL_OOP_MAX__c</fields>
        <fields>INDIVIDUAL_OOP_MET__c</fields>
        <fields>Id</fields>
        <fields>Insurer__c</fields>
        <fields>MAX_SENSOR_UNITS_ORDER__c</fields>
        <fields>MEMBER_ID__c</fields>
        <fields>P1_REFERENCE_NUM__c</fields>
        <fields>P2_PRIOR_AUTH_DETERM_FAX_C__c</fields>
        <fields>P2_PRIOR_AUTH_DETERM_PHONE__c</fields>
        <fields>PA_PRIOR_AUTH_DETERM_FAX__c</fields>
        <fields>PA_PRIOR_AUTH_DETERM_PHONE__c</fields>
        <fields>PA_REFERENCE_NUM__c</fields>
        <fields>PA_SPOKE_WITH__c</fields>
        <fields>PBM__c</fields>
        <fields>PRE_DETERMINATION_ALLOWED__c</fields>
        <fields>PRE_EXISTING_CLAUSE__c</fields>
        <fields>PRE_EXISTING_END_DATE__c</fields>
        <fields>PRE_EXISTING_START_DATE__c</fields>
        <fields>PRIOR_AUTH_REQUIRED__c</fields>
        <fields>Party_ID__c</fields>
        <fields>Payor__c</fields>
        <fields>Plan_Name__c</fields>
        <fields>Plan_Phone__c</fields>
        <fields>Plan_Type__c</fields>
        <fields>Policy_Holder_Date_of_Birth__c</fields>
        <fields>Policy_Holder_Name__c</fields>
        <fields>Pre_existing_condition__c</fields>
        <fields>Primary_Insurance_Contact_Member__c</fields>
        <fields>Prior_auth_Denied_or_Approved__c</fields>
        <fields>Provider_Services_Phone__c</fields>
        <fields>RECEIVER_AUTHORIZATION__c</fields>
        <fields>RECEIVER_AUTH_END_DATE__c</fields>
        <fields>RECEIVER_AUTH_START_DATE__c</fields>
        <fields>RECEVIER_UNITS_AUTH__c</fields>
        <fields>RELATIONSHIP_TO_PATIENT__c</fields>
        <fields>RID_IC__c</fields>
        <fields>RID_P1__c</fields>
        <fields>RID_P2__c</fields>
        <fields>RID_PA__c</fields>
        <fields>Rider_or_State_Mandate__c</fields>
        <fields>Risk__c</fields>
        <fields>RxBIN__c</fields>
        <fields>RxGROUP__c</fields>
        <fields>SENSOR_AUTH_END_DATE__c</fields>
        <fields>SENSOR_AUTH_START_DATE__c</fields>
        <fields>Sensor_Unit_Authorization__c</fields>
        <fields>Sensor_Units_Authorized__c</fields>
        <fields>Spoke_With__c</fields>
        <fields>Start_Date__c</fields>
        <fields>TRANSMITTER_AUTH_END_DATE__c</fields>
        <fields>TRANSMITTER_AUTH_NUM__c</fields>
        <fields>TRANSMITTER_AUTH_START_DATE__c</fields>
        <fields>TRANSMITTER_UNITS_AUTH__c</fields>
        <fields>YEAR_END_DATE__c</fields>
        <fields>YEAR_START_DATE__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>dataintegrator@dexcom.com</integrationUser>
        <name>Upsert Benefits In Oracle</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Aetna_Medicare_Advantage _To_update_Plan_Type</fullName>
        <actions>
            <name>Update_Plan_Type_Med_Adv</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Plan type to Med Adv for Aetna patients when Member Id starts with MEB</description>
        <formula>AND(
Payor__r.Name = &apos;AETNA&apos;,
 BEGINS(MEMBER_ID__c, &apos;MEB&apos;) ,
				NOT(ISPICKVAL(Plan_Type__c, &apos;MED ADV&apos;))
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Auto Send to BI</fullName>
        <actions>
            <name>Update_BI_Request_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Auto send for BI if Last Benefit Check Date is older than 60 days and BI Status = success and payer has CHC Approved? field as true</description>
        <formula>AND(  ISCHANGED(Days_Since_Last_Benefit_Check__c),  Payor__r.CHC_Approved__c = TRUE,  BI_Status__c = &apos;SUCCESS&apos;, Days_Since_Last_Benefit_Check__c &gt;= 60 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate MBI_To_MemberID</fullName>
        <actions>
            <name>MemberID_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populate MBI(Medicare Beneficiary Identifier) to MemberID field for medicare Patients if clicked on Save.</description>
        <formula>AND(
 Payor__r.Is_Medicare_Payor__c = true,
				ISCHANGED(Medicare_Beneficiary_Identifier__c),
				NOT(ISBLANK(Medicare_Beneficiary_Identifier__c))
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send Benefits to Oracle</fullName>
        <actions>
            <name>Uncheck_Send_To_Oracle_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Is_Update_In_Progress</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Upsert_Benefits_In_Oracle</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Send Benefits to Oracle</description>
        <formula>AND(
OR(ISNEW(), ISCHANGED( Send_to_Oracle__c)), 
Send_to_Oracle__c  = True
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update BI Request Date</fullName>
        <actions>
            <name>Update_BI_Request_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Update BI Request Date based on CHC Approved and Member ID</description>
        <formula>AND( OR( ISCHANGED(MEMBER_ID__c), AND( ISNEW(), NOT(ISBLANK(MEMBER_ID__c)) ) ), Payor__r.CHC_Approved__c == TRUE )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Last Benefits Check Done By On Benefit</fullName>
        <actions>
            <name>Update_Last_Benefits_Check_Done_By</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Rule will evaluate who modified the Last Benefits Check Date and will stamp the user name to the field Last Benefits Check Done By</description>
        <formula>AND( NOT(CONTAINS($Profile.Name, &apos;Integrator&apos;)), NOT(ISBLANK(Last_Benefits_Check_Date__c)), OR ( ISCHANGED (Last_Benefits_Check_Date__c), ISNEW() ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Oracle Relationship Id</fullName>
        <actions>
            <name>Update_Oracle_Relationship_Id_Value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow updates the Oracle Relationship Id with &quot;PartyId - Hierarchy&quot; value which is used as external id by integration.</description>
        <formula>AND( 
OR( 
ISNEW() , 
ISCHANGED(Benefit_Hierarchy__c ), 
ISCHANGED(Account__c) 
), 
NOT(ISBLANK(Account__c) ), 
NOT( $User.Id == &apos;00533000004B3Fl&apos;) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
