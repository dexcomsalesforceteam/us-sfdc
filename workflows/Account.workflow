<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Assign_Participant_Recordtype</fullName>
        <description>Assign Program Participant Recordtype for the accouont</description>
        <field>RecordTypeId</field>
        <lookupValue>Consumer_Program_Participant</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Assign Participant Recordtype</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_Opt_in_2</fullName>
        <description>Set SMS Send opt in 2 to be true so we can send the third opt in text</description>
        <field>SMS_Send_Opt_In_2__c</field>
        <literalValue>1</literalValue>
        <name>Check Opt in 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Deactivate_Subscription</fullName>
        <field>MDCR_FFS_Subscription__c</field>
        <literalValue>Not Active</literalValue>
        <name>Deactivate Subscription</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Desync_flag_to_false</fullName>
        <field>Desynchronize_With_Marketing_Cloud__pc</field>
        <literalValue>0</literalValue>
        <name>Set Desync flag to false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_CS_Inquiries_Checkbox</fullName>
        <field>CS_Inquiry_Survey__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck CS Inquiries Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Age</fullName>
        <description>Update Age Non formula field</description>
        <field>Age_Number__c</field>
        <formula>Age__c</formula>
        <name>Update Age</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CMN_Expiration_Date</fullName>
        <description>Update CMN Expiration based on other CMN fields</description>
        <field>CMN_or_Rx_Expiration_Date__c</field>
        <formula>IF(TEXT(CMN_Interval__c) == &apos;99&apos;, DATE(2099, 01, 01), (DATE( CMN_Year__c , CMN_Month__c , CMN_Day__c )))</formula>
        <name>Update CMN Expiration Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CMN_Month</fullName>
        <description>Updates CMN Month</description>
        <field>CMN_Month__c</field>
        <formula>IF((MONTH( CMN_Signed_Date__c )+ MOD(VALUE(TEXT(CMN_Interval__c)) , 12))&lt;13,
MONTH( CMN_Signed_Date__c )+ MOD(VALUE(TEXT(CMN_Interval__c)),12) , MONTH( CMN_Signed_Date__c )+ MOD(VALUE(TEXT(CMN_Interval__c)) , 12)-12)</formula>
        <name>Update CMN Month</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_CMN_Year</fullName>
        <description>Update CMN Year</description>
        <field>CMN_Year__c</field>
        <formula>IF((MONTH(CMN_Signed_Date__c )+ VALUE(TEXT(CMN_Interval__c)) )&lt;13, YEAR(CMN_Signed_Date__c), 
YEAR(CMN_Signed_Date__c) + FLOOR((MONTH( CMN_Signed_Date__c )-1 + VALUE(TEXT(CMN_Interval__c)))/12))</formula>
        <name>Update CMN Year</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Customer_Type_On_Account_To_Cash</fullName>
        <description>Jagan 06/13/2017 - Updates the customer type on account to cash.</description>
        <field>Customer_Type__c</field>
        <literalValue>Cash</literalValue>
        <name>Update Customer Type On Account To Cash</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_DOB_Searchable</fullName>
        <field>DOB_Searchable__c</field>
        <formula>TEXT(MONTH(DOB__c))+&quot;/&quot; +TEXT(DAY(DOB__c))+&quot;/&quot; +TEXT(YEAR(DOB__c))</formula>
        <name>Update DOB Searchable</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Days_In_Month</fullName>
        <description>Updates number of days in a given month</description>
        <field>CMN_Days_In_Month__c</field>
        <formula>CASE ( CMN_Month__c ,1,31,2,IF(OR(MOD( CMN_Year__c ,400)=0,AND(MOD(CMN_Year__c,4)=0,MOD(CMN_Year__c,100)&lt;&gt;0)),29,28),3,31,4,30,5,31,6,30,7,31,8,31,9,30,10,31,11,30,12,31,0)</formula>
        <name>Update Days In Month</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Document_Status_Updated_Date</fullName>
        <field>Document_Status_Updated_Date__c</field>
        <formula>TODAY()</formula>
        <name>Update Document Status Updated Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_G4_Exception</fullName>
        <field>G4_Exception_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Update G4 Exception</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_G6_Program</fullName>
        <field>G6_Program__c</field>
        <literalValue>G6 Medicare Transition</literalValue>
        <name>Update G6 Program</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Inactive_Flag_to_True</fullName>
        <field>Inactive__c</field>
        <literalValue>1</literalValue>
        <name>Update Inactive Flag to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Next_MDCR_Reorder_Follow_up_Date</fullName>
        <field>Next_MDCR_Reorder_Follow_up_Date__c</field>
        <formula>DATE(
   YEAR( Last_MDCR_Reorder_Follow_up_Date__c ) + FLOOR(MONTH(Last_MDCR_Reorder_Follow_up_Date__c)/12),
   MOD(MONTH(Last_MDCR_Reorder_Follow_up_Date__c), 12) +1,
   MIN(DAY(Last_MDCR_Reorder_Follow_up_Date__c),CASE(MOD(MONTH(Last_MDCR_Reorder_Follow_up_Date__c) + 1,12), 9, 30, 4, 30, 6, 30, 11, 30, 2,IF(MOD(YEAR(Last_MDCR_Reorder_Follow_up_Date__c) + FLOOR((MONTH(Last_MDCR_Reorder_Follow_up_Date__c) + 1)/12), 400) = 0 || (MOD(YEAR(Last_MDCR_Reorder_Follow_up_Date__c) + FLOOR((MONTH(Last_MDCR_Reorder_Follow_up_Date__c) + 1)/12), 4) = 0 &amp;&amp; MOD(YEAR(Last_MDCR_Reorder_Follow_up_Date__c) + FLOOR((MONTH(Last_MDCR_Reorder_Follow_up_Date__c) + 1)/12), 100) &lt;&gt; 0), 29,28), 31))
)</formula>
        <name>Update Next MDCR Reorder Follow-up Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Patient_Info_For_Simplicity_Progm</fullName>
        <description>Update patient information for simplicity customer</description>
        <field>Important_Patient_Information__c</field>
        <formula>&apos;Do not process at Dexcom, send to Tech Support&apos;</formula>
        <name>Update Patient Info For Simplicity Progm</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Pharmacy_No_Go_CheckBox</fullName>
        <field>Pharmacy_No_Go__c</field>
        <literalValue>0</literalValue>
        <name>Update Pharmacy No Go CheckBox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Pharmacy_No_Go_CheckBox_True</fullName>
        <field>Pharmacy_No_Go__c</field>
        <literalValue>1</literalValue>
        <name>Update Pharmacy No Go CheckBox True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Pharmacy_No_Go_Date</fullName>
        <field>Pharmacy_No_Go_Date__c</field>
        <formula>IF(Pharmacy_No_Go__c = true,  TODAY() , null)</formula>
        <name>Update Pharmacy No Go Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type_Inactive</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Inactive_Consumer</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type Inactive</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_pending_SMS</fullName>
        <description>Update the SMS pending opt in field to be true</description>
        <field>SMS_pending_opt_in__c</field>
        <literalValue>1</literalValue>
        <name>update pending SMS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_pending_SMS_to_false</fullName>
        <description>uncheck the pending SMS checkbox</description>
        <field>SMS_pending_opt_in__c</field>
        <literalValue>0</literalValue>
        <name>update pending SMS to false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_send_opt_in_two_to_false</fullName>
        <description>uncheck the send sms opt in two to be false</description>
        <field>SMS_Send_Opt_In_2__c</field>
        <literalValue>0</literalValue>
        <name>update send opt in two to false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Release_Order_Invoice_Hold_For_Account</fullName>
        <apiVersion>41.0</apiVersion>
        <description>Outbound message will be invoked to release invoice hold on Orders tied to customers when all documents are received.</description>
        <endpointUrl>https://itapi.dexcom.com/ws/soap/updatemedicareaccount;boomi_auth=bWVkaWNhcmVAZGV4Y29taW5jLTZPNkowTzoxM2EwODBmOC1lMTAzLTQzMDItOTJkOC1mZjI0OTk3ZGY2NzM=</endpointUrl>
        <fields>AccountNumber</fields>
        <fields>Id</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>sfdcadmin1@dexcom.com</integrationUser>
        <name>Release Order Invoice Hold For Account</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Send_Account_To_Oracle</fullName>
        <apiVersion>41.0</apiVersion>
        <description>Jagan 01/05/2018 - Outbound message will have acocunt details.</description>
        <endpointUrl>https://itapi.dexcom.com/ws/soap/createsalesforceparty;boomi_auth=c2FsZXNmb3JjZUBkZXhjb21pbmMtNk82SjBPOjYyODdmOTZhLWY2NWMtNGRjYi1hYzg4LTM3ZDE0ZWVmYjg2Ng==</endpointUrl>
        <fields>AccountNumber</fields>
        <fields>Account_Record_Type__c</fields>
        <fields>CMN_or_Rx_Expiration_Date__c</fields>
        <fields>Country_code__c</fields>
        <fields>CreatedDate</fields>
        <fields>DEA__pc</fields>
        <fields>DOB__c</fields>
        <fields>Default_Price_Book__c</fields>
        <fields>Diagnosis_Code_1_Acc__c</fields>
        <fields>Diagnosis_Code_2_Acc__c</fields>
        <fields>Fax</fields>
        <fields>FirstName</fields>
        <fields>Gender__c</fields>
        <fields>HCE_ID__c</fields>
        <fields>IMS_ID__c</fields>
        <fields>Id</fields>
        <fields>Inactive__c</fields>
        <fields>Integrated_Pump_Partner__c</fields>
        <fields>LastName</fields>
        <fields>Medical_Facility_Party_Id__c</fields>
        <fields>Medical_Facility__c</fields>
        <fields>Middle_Name__c</fields>
        <fields>NPI_Number__c</fields>
        <fields>Name</fields>
        <fields>Party_ID__c</fields>
        <fields>PersonEmail</fields>
        <fields>PersonHomePhone</fields>
        <fields>PersonMobilePhone</fields>
        <fields>PersonOtherPhone</fields>
        <fields>Phone</fields>
        <fields>Prescriber_Party_Id__c</fields>
        <fields>Prescribers__c</fields>
        <fields>RecordTypeId</fields>
        <fields>Secondary_Fax__c</fields>
        <fields>ShippingCountry</fields>
        <fields>Type</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>salesforce.admin@dexcom.com.us</integrationUser>
        <name>Send Account To Oracle</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Send_Account_to_qualtrics</fullName>
        <apiVersion>48.0</apiVersion>
        <endpointUrl>https://iad1.qualtrics.com/triggers/api/v1/event?eventType=SalesforceOutboundMessage&amp;s=SV_88QjrnRWWSL8FbD&amp;u=UR_6A1VCHhdKu95gVv&amp;t=OC_0UslEiZflVjOx8N&amp;b=dexcom</endpointUrl>
        <fields>AccountNumber</fields>
        <fields>Id</fields>
        <fields>Name</fields>
        <fields>PersonEmail</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>salesforce.admin@dexcom.com.us</integrationUser>
        <name>Send Account to qualtrics</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>CS Inquiries Survey Check</fullName>
        <actions>
            <name>Uncheck_CS_Inquiries_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Send_Account_to_qualtrics</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Consumers</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.CS_Inquiry_Survey__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Check pending SMS</fullName>
        <actions>
            <name>update_pending_SMS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.SMS_Opt_In_List__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>when sms opt in list changes to true update the sms pending checkbox to be true</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Date Of Death Populated After 60 Days Inactivate Account</fullName>
        <active>false</active>
        <formula>NOT( ISBLANK(Date_of_Death__c) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Inactive_Flag_to_True</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_Record_Type_Inactive</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>60</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Deactivate FFS Sub for Inactive Consumers</fullName>
        <actions>
            <name>Deactivate_Subscription</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( RecordType.Name = &quot;Inactive Consumer&quot;,   ISCHANGED(  Account_Record_Type__c  ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Payor Change for G6 Transition</fullName>
        <actions>
            <name>Update_G6_Program</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(
	ISCHANGED(Payor__c ) ,
	Payor__r.Is_Medicare_Payor__c, 
	Latest_Transmitter_Generation_Shipped__c  = &apos;G6&apos; 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Pharmacy No Go Check</fullName>
        <actions>
            <name>Update_Pharmacy_No_Go_CheckBox_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Program_Status__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Program_Status__c</field>
            <operation>notEqual</operation>
            <value>Document Capture,In Process - ASPN - Receiver Requires DME or Medical</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Pharmacy No Go Date field update</fullName>
        <actions>
            <name>Update_Pharmacy_No_Go_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( Pharmacy_No_Go__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Pharmacy No Go Uncheck</fullName>
        <actions>
            <name>Update_Pharmacy_No_Go_CheckBox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Program_Status__c</field>
            <operation>equals</operation>
            <value>Document Capture,In Process - ASPN - Receiver Requires DME or Medical</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Release Order Invoice Hold For Account</fullName>
        <actions>
            <name>Update_Document_Status_Updated_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Release_Order_Invoice_Hold_For_Account</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Rule will trigger an outbound message when we receive all documents in Salesforce for a customer.</description>
        <formula>AND(
CONTAINS(TEXT(Customer_Type__c), &apos;Medicare&apos;),
ISCHANGED(All_Documents_Received__c),
PRIORVALUE(All_Documents_Received__c) != &apos;Yes&apos;,
ISPICKVAL(All_Documents_Received__c, &apos;Yes&apos;)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SMS Opt In changes to checked</fullName>
        <actions>
            <name>Set_Desync_flag_to_false</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.SMS_Opt_In_List__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Consumers</value>
        </criteriaItems>
        <description>If the &apos;SMS Opt In&apos; changes to be checked/true</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Account To Oracle</fullName>
        <actions>
            <name>Send_Account_To_Oracle</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Jagan 01/05/2018 - WF triggers outbound message on account create or update process.</description>
        <formula>OR ( 	
	
		AND(CONTAINS($RecordType.Name, &apos;Consumer&apos;), ISNEW(), NOT(Was_Lead__c)),  
		AND(  		
			ISCHANGED(Send_To_Oracle__c),  		
			Send_To_Oracle__c = true  	
		),
		ISCHANGED(Date_of_Death__c)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Uncheck Pendng SMS</fullName>
        <actions>
            <name>update_pending_SMS_to_false</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>update_send_opt_in_two_to_false</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.SMS_Opt_In_List__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <description>when the opt in list changes from true we want to uncheck the pending check box</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Age field</fullName>
        <actions>
            <name>Update_Age</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Non-Formula field Age should be updated with Formula field Age</description>
        <formula>OR(ISNEW(), ISCHANGED(Age__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update CMN Expiration Date</fullName>
        <actions>
            <name>Update_CMN_Expiration_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_CMN_Month</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_CMN_Year</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Days_In_Month</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Rule will update the CMN Expiration Date based on the CMN Signed Date and CMN Interval</description>
        <formula>OR( 		AND ( 			NOT(ISBLANK(CMN_Signed_Date__c)), 			OR(ISNEW(), ISCHANGED(CMN_Signed_Date__c )) 		), 		AND ( 			NOT(ISBLANK(TEXT(CMN_Interval__c))), 			OR(ISNEW(), ISCHANGED(CMN_Interval__c )) 		) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update DOB Searchable</fullName>
        <actions>
            <name>Update_DOB_Searchable</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(ISNEW(),ISCHANGED( DOB__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Next MDCR Reorder Follow-up Date On Account</fullName>
        <actions>
            <name>Update_Next_MDCR_Reorder_Follow_up_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Next MDCR Reorder Follow-up Date On Account</description>
        <formula>OR( AND ( ISNEW(), NOT(ISBLANK( Last_MDCR_Reorder_Follow_up_Date__c )) ), AND( ISCHANGED(Last_MDCR_Reorder_Follow_up_Date__c), NOT(ISBLANK( Last_MDCR_Reorder_Follow_up_Date__c )) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Program Participant Record Type</fullName>
        <actions>
            <name>Assign_Participant_Recordtype</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Patient_Info_For_Simplicity_Progm</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update the record type for the customer who are participating in the Dexcom Program.</description>
        <formula>OR(
    AND(ISNEW(), NOT(ISBLANK(TEXT(Dxcm_Program_Name__c)))),
	AND(NOT(ISBLANK(TEXT(Dxcm_Program_Name__c))), ISCHANGED(Dxcm_Program_Name__c))
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Program Participant Record Type for DMP</fullName>
        <actions>
            <name>Assign_Participant_Recordtype</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update the record type for the customer who are participating in the DMP Dexcom Program.</description>
        <formula>OR(AND(NOT(ISBLANK(Party_ID__c)),ISCHANGED(Party_ID__c),  ISPICKVAL(Dxcm_Program_Name__c,&quot;DMP&quot;)),AND(NOT(ISBLANK(Party_ID__c)),ISPICKVAL(Dxcm_Program_Name__c,&quot;DMP&quot;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Program Participant Record Type for Simplicity</fullName>
        <actions>
            <name>Assign_Participant_Recordtype</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Patient_Info_For_Simplicity_Progm</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update the record type for the customer who are participating in the Simplicity Dexcom Program.</description>
        <formula>OR(
    AND(ISNEW(),
NOT(ISPICKVAL(Dxcm_Program_Name__c,&quot;DMP&quot;)),
NOT(ISBLANK(TEXT(Dxcm_Program_Name__c)))),
AND(NOT(ISBLANK(TEXT(Dxcm_Program_Name__c))),
NOT(ISPICKVAL(Dxcm_Program_Name__c,&quot;DMP&quot;)),
ISCHANGED(Dxcm_Program_Name__c))
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update SMS send Opt in 2</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account.SMS_Opt_In_List__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.SMS_Opt_In_Sent__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Check_Opt_in_2</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Account.SMS_Opt_In_Sent__c</offsetFromField>
            <timeLength>24</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update the G4 Exception flag</fullName>
        <actions>
            <name>Update_G4_Exception</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Latest_Transmitter_Generation_Shipped__c</field>
            <operation>equals</operation>
            <value>G4</value>
        </criteriaItems>
        <description>If latest transmitter generation Updated  to G4  then Updating G4 Exception flag to true.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update_Pharmacy_No_ Go_on _Payor_change</fullName>
        <actions>
            <name>Update_Pharmacy_No_Go_CheckBox</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Pharmacy_No_Go_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Payor__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>email address changes from blank</fullName>
        <actions>
            <name>Set_Desync_flag_to_false</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.PersonEmail</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Consumers</value>
        </criteriaItems>
        <description>If the email address changes from blank to not blank.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
