<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Approval_Email</fullName>
        <description>Approval Email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Back_End_Order_Approval_Email/Back_End_Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Back_End_Order_Rejected</fullName>
        <description>Back End Order Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Back_End_Order_Approval_Email/Back_End_Order_Denied</template>
    </alerts>
    <alerts>
        <fullName>sending_Integration_Message_to_Send_Error_Notification_To</fullName>
        <description>sending Integration Message to Send Error Notification To</description>
        <protected>false</protected>
        <recipients>
            <field>Send_Error_Notification_To__c</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Order_Integration_Message_Template</template>
    </alerts>
    <fieldUpdates>
        <fullName>Clear_Copay_On_Order</fullName>
        <description>Clear the copay value on Order</description>
        <field>CoPay__c</field>
        <formula>0</formula>
        <name>Clear Copay On Order</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Order_Shipping_Charge</fullName>
        <description>Clear the shipping charges</description>
        <field>Shipping_Charges__c</field>
        <formula>0</formula>
        <name>Clear Order Shipping Charge</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Tax_On_Order</fullName>
        <description>Clear tax on Order</description>
        <field>Tax__c</field>
        <formula>0</formula>
        <name>Clear Tax On Order</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Unique_Identifier</fullName>
        <field>Unique_Identifier_For_Draft_Orders__c</field>
        <name>Clear Unique Identifier</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_Update_for_Payment_Plans</fullName>
        <field>Amount_to_be_Charged_Final__c</field>
        <formula>PP_Monthly_Payment__c</formula>
        <name>Field Update for Payment Plans</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Move_Order_to_In_Progress</fullName>
        <description>Bill On Back end progress</description>
        <field>Exception_Approval__c</field>
        <literalValue>In Progress</literalValue>
        <name>Move Order to In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Amount_to_be_Charged</fullName>
        <field>Amount_to_be_Charged_Final__c</field>
        <formula>Amount_to_be_Charged__c</formula>
        <name>Update Amount to be Charged</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Amount_to_be_Charged_to_Zero</fullName>
        <field>Amount_to_be_Charged_Final__c</field>
        <formula>0</formula>
        <name>Update Amount to be Charged to Zero</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Type</fullName>
        <field>Bill_on_Backend_Approval_Type__c</field>
        <literalValue>Waive Full Order Amount</literalValue>
        <name>Update Approval Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Billing_Exception</fullName>
        <description>Update Billing Exception as &quot;Bill on back end&quot; for Aetna Payor</description>
        <field>Billing_Exceptions__c</field>
        <literalValue>Dual Coverage</literalValue>
        <name>Update Billing Exception</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Billing_Exception_Approval</fullName>
        <field>Exception_Approval__c</field>
        <literalValue>Approved</literalValue>
        <name>Update Billing Exception Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Billing_Exception_Denial</fullName>
        <field>Exception_Approval__c</field>
        <literalValue>Denied</literalValue>
        <name>Update Billing Exception Denial</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Order_Shipping_Charge</fullName>
        <description>For Insurance Order with no Shipment waiver and Shipment method is selected then system should deduct $15 from the shipment amount. If the amount is less than $15 then the shipment cost will be 0. For Cash Order the amount is as is from Shipping method.</description>
        <field>Shipping_Charges__c</field>
        <formula>IF(Price_Book__r.Cash_Price_Book__c,
CASE(TEXT(Shipping_Method__c),
&quot;000001_FEDEX_A_SAT&quot;,44,
&quot;000001_FEDEX_A_PO&quot;,35,
&quot;000001_FEDEX_A_1DA&quot;,28,
&quot;000001_FEDEX_A_2D&quot;,17,
&quot;000001_FEDEX_A_3DS&quot;,15,
&quot;000001_FEDEX_L_GND&quot;,11.70,
0
) ,
CASE(TEXT(Shipping_Method__c),
&apos;000001_FEDEX_A_SAT&apos;,29,
&apos;000001_FEDEX_A_PO&apos;,20,
&apos;000001_FEDEX_A_1DA&apos;,13,
&apos;000001_FEDEX_A_2D&apos;,IF(TEXT(Shipping_Address__r.State__c) = &apos;AK&apos; || TEXT(Shipping_Address__r.State__c) = &apos;HI&apos;,0,2),
0
) 
)</formula>
        <name>Update Order Shipping Charge</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Order_To_K_Code_Commercial_Order</fullName>
        <field>Type</field>
        <literalValue>K-Code Commercial</literalValue>
        <name>Update Order To K Code Commercial Order</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Order_To_MC_Order_Type</fullName>
        <description>Updated the order type to MC Standard Sales Order</description>
        <field>Type</field>
        <literalValue>MC Standard Sales Order</literalValue>
        <name>Update Order To MC Order Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Order_To_Sample_Order_Type</fullName>
        <description>Update order type to Sample Order</description>
        <field>Type</field>
        <literalValue>Sample Order</literalValue>
        <name>Update Order To Sample Order Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Order_To_Standard_Sales_Order_Typ</fullName>
        <description>Update to Standard Sales Order type.</description>
        <field>Type</field>
        <literalValue>Standard Sales Order</literalValue>
        <name>Update Order To Standard Sales Order Typ</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Shipment_Waiver_To_True</fullName>
        <description>Jagan 02/23/2018 - Workflow updates the Shipment waiver on Order.</description>
        <field>Waive_Shipping_Charges__c</field>
        <literalValue>1</literalValue>
        <name>Update Shipment Waiver To True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Unique_Identifier</fullName>
        <description>Update Unique Identifier field for Draft Orders</description>
        <field>Unique_Identifier_For_Draft_Orders__c</field>
        <formula>AccountId + &apos; &apos; + Price_Book__r.Id</formula>
        <name>Update Unique Identifier</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Wipe_Out_Amount_to_be_Charged</fullName>
        <field>Amount_to_be_Charged_Final__c</field>
        <name>Wipe Out Amount to be Charged</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Clear Order Shipping Charge</fullName>
        <actions>
            <name>Clear_Order_Shipping_Charge</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If Shipping Method is blank or shipping waiver is selcted, order shipping charge should be cleared.</description>
        <formula>OR( 	AND( 		ISCHANGED(Shipping_Method__c), 		ISBLANK(TEXT(Shipping_Method__c)), 		Shipping_Charges__c &gt; 0 	), 	AND( 		ISCHANGED(Waive_Shipping_Charges__c), 		Waive_Shipping_Charges__c = true, 		Shipping_Charges__c &gt; 0 	) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Clear Tax Copay On Order</fullName>
        <actions>
            <name>Clear_Copay_On_Order</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear_Tax_On_Order</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Clear tax and copay on Order when there are no order line items</description>
        <formula>AND( 	OR( 		AND(ISCHANGED(Count_of_Order_Line_Items__c), Count_of_Order_Line_Items__c = 0), 		AND(ISCHANGED(Tax_Exempt__c), Tax_Exempt__c = true) 	),	 	OR(Tax__c &gt; 0, CoPay__c &gt; 0) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Clear Unique Identifier for Activated Orders</fullName>
        <actions>
            <name>Clear_Unique_Identifier</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order.Status</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <description>Clearing the Unique Identifier field once the order is Activated.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send_Email_For_Activation_Failure</fullName>
        <actions>
            <name>sending_Integration_Message_to_Send_Error_Notification_To</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This workflow will send email to Person email in Send Order Notification to field.</description>
        <formula>AND(ISCHANGED(Integration_Message__c),
        NOT(ISBLANK(Integration_Message__c )),
        NOT(ISBLANK(Send_Error_Notification_To__c ) ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set the Billing Exception for Aetna Pricelist</fullName>
        <actions>
            <name>Update_Approval_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Billing_Exception</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Any order that uses either the Aetna Health Insurance or M/C Advantage - Aetna Health Insurance pricelists, we would add Billing Exception on the back end.</description>
        <formula>CONTAINS(Price_Book__r.Name, &apos;AETNA&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Amount to be Charged for Billing Exceptions</fullName>
        <actions>
            <name>Update_Amount_to_be_Charged_to_Zero</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order.Billing_Exceptions__c</field>
            <operation>equals</operation>
            <value>Transfer of Funds,Carecentrix Order,Dual Coverage</value>
        </criteriaItems>
        <description>Update Amount to be Charged for Billing Exceptions - Carecentrix Order and Transfer of Funds</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Amount to be Charged for PP</fullName>
        <actions>
            <name>Field_Update_for_Payment_Plans</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order.Payment_Plan_Order__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <description>Payment Plan WF</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Order Shipping Charge</fullName>
        <actions>
            <name>Update_Order_Shipping_Charge</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates order shipping charge for Cash and Insurance Orders when there is no waiver. For Sample Order the shipping charge is defaulted to 0.</description>
        <formula>NOT(ISBLANK(TEXT(Shipping_Method__c))) &amp;&amp;  NOT(CONTAINS(Price_Book__r.Name, &apos;No Charge&apos;)) &amp;&amp;  Waive_Shipping_Charges__c = FALSE &amp;&amp;  ( 	 OR( 		 	ISNEW(), 		 	ISCHANGED(Shipping_Method__c), 		 	ISCHANGED(Waive_Shipping_Charges__c), 	ISCHANGED(Price_Book__c), 	ISCHANGED(Shipping_Address__c), 	ISCHANGED(Type) )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Order To K Code Commercial Order Type</fullName>
        <actions>
            <name>Update_Order_To_K_Code_Commercial_Order</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update order type to K Code commercial based on the pricebook name tied to the order.</description>
        <formula>AND( 
				NOT(ISBLANK(Price_Book__c)), 
				OR( 
								ISNEW(), 
								ISCHANGED(Price_Book__c) 
				), 
				 Price_Book__r.Oracle_Order_Type_ID__c =&apos;1595&apos;				 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Order To MC Order Type</fullName>
        <actions>
            <name>Update_Order_To_MC_Order_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update order type to MC Order based on the pricebook name tied to the order.</description>
        <formula>AND( 	
	NOT(ISBLANK(Price_Book__c)), 	
	OR( 		
		ISNEW(), 		
		ISCHANGED(Price_Book__c) 	
	), 	
	 Price_Book__r.Oracle_Order_Type_ID__c = &apos;1515&apos;
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Order To Sample Order Type</fullName>
        <actions>
            <name>Update_Order_To_Sample_Order_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update order type to Sample Order based on the pricebook name tied to the order.</description>
        <formula>AND( 	NOT(ISBLANK(Price_Book__c)), 	OR( 		ISNEW(), 		ISCHANGED(Price_Book__c) 	), 	CONTAINS(Price_Book__r.Name, &apos;No Charge&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Order To Standard Sales Order Type</fullName>
        <actions>
            <name>Update_Order_To_Standard_Sales_Order_Typ</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update order type to Standard Sales Order based on the pricebook name tied to the order.</description>
        <formula>AND( 
				NOT(ISBLANK(Price_Book__c)), 
				OR( 
								ISNEW(), 
								ISCHANGED(Price_Book__c) 
				), 
				 Price_Book__r.Oracle_Order_Type_ID__c =&apos;1008&apos;				 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Unique Identifier for Draft Orders</fullName>
        <actions>
            <name>Update_Unique_Identifier</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order.Status</field>
            <operation>equals</operation>
            <value>Draft</value>
        </criteriaItems>
        <criteriaItems>
            <field>Order.Sub_Type__c</field>
            <operation>notEqual</operation>
            <value>Comp Product Order,SSIP Order</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Updated Shipment Waiver For Sample Order</fullName>
        <actions>
            <name>Update_Shipment_Waiver_To_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Rule will set the shipment waiver for Sample order.</description>
        <formula>AND( 	NOT(ISBLANK(Price_Book__c)), 	CONTAINS(Price_Book__r.Name, &apos;No Charge&apos;), 	OR( 		ISNEW(), 		ISCHANGED(Price_Book__c) 	)	 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Wipe Out Amount to be charged Billing Exception</fullName>
        <actions>
            <name>Wipe_Out_Amount_to_be_Charged</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Wipe Out Amount when Billing Exception is wiped out</description>
        <formula>AND(
ISCHANGED(Billing_Exceptions__c),  ISPICKVAL(Billing_Exceptions__c, &quot;&quot;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>