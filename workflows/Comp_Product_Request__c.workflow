<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Comp_Product_Email_Notification_To_The_Requester</fullName>
        <description>Comp Product Email Notification To The Requester</description>
        <protected>false</protected>
        <recipients>
            <field>Repmaking_Request__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Sales/Comp_Product_Request_Approved_Rejected_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approved_Or_Rejected_By_Field_Update</fullName>
        <description>Jagan 02/21/2017 - This field update will update the field Approved Or Rejected By on the Comp product request.</description>
        <field>Approved_Or_Rejected_By__c</field>
        <formula>$User.FirstName +  $User.LastName</formula>
        <name>Approved Or Rejected By Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Comp Product Email Notification Workflow Rule</fullName>
        <actions>
            <name>Comp_Product_Email_Notification_To_The_Requester</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Comp_Product_Request__c.Repmaking_Request__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Comp_Product_Request__c.Approval_Status__c</field>
            <operation>equals</operation>
            <value>Approved,Rejected</value>
        </criteriaItems>
        <description>Jagan 02/16/2017 - Created this Workflow rule to send Comp Product email notification on approval or rejection of the request.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
