<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <outboundMessages>
        <fullName>Create_DxNotes_In_Oracle</fullName>
        <apiVersion>40.0</apiVersion>
        <description>Creates a outbound message to invoke BOOMI service, which creates the DxNotes in Oracle</description>
        <endpointUrl>https://itapi.dexcom.com/ws/soap/createssalesforcenote/notifications;boomi_auth=c2FsZXNmb3JjZUBkZXhjb21pbmMtNk82SjBPOjYyODdmOTZhLWY2NWMtNGRjYi1hYzg4LTM3ZDE0ZWVmYjg2Ng==</endpointUrl>
        <fields>Call_Type__c</fields>
        <fields>Comments__c</fields>
        <fields>Id</fields>
        <fields>Oracle_Notes_Id__c</fields>
        <fields>Party_ID__c</fields>
        <fields>Subject__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>jp0117@dexcom.com</integrationUser>
        <name>Create DxNotes In Oracle</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Send_DXNotes_to_qualtrics</fullName>
        <apiVersion>48.0</apiVersion>
        <endpointUrl>https://iad1.qualtrics.com/triggers/api/v1/event?eventType=SalesforceOutboundMessage&amp;s=SV_ebLWbOuaea6H3mJ&amp;u=UR_6A1VCHhdKu95gVv&amp;t=OC_cVcOnl9KwUL2lQ9&amp;b=dexcom</endpointUrl>
        <fields>ConsumerName__c</fields>
        <fields>CreatedByName__c</fields>
        <fields>CreatedDate</fields>
        <fields>Id</fields>
        <fields>OracleAccountNumber__c</fields>
        <fields>PC_Reason_Code_1__c</fields>
        <fields>PatientDOB__c</fields>
        <fields>PatientEmail__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>salesforce.admin@dexcom.com.us</integrationUser>
        <name>Send DXNotes to qualtrics</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Dexcom PatientCare Survey Check</fullName>
        <actions>
            <name>Send_DXNotes_to_qualtrics</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>DX_Notes__c.PC_Reason_Code_1__c</field>
            <operation>notContain</operation>
            <value>&quot;Call transferred to PCS, non-education&quot;,&quot;Refer to CS, non-education&quot;,&quot;Refer to TS, non-education&quot;,&quot;Ref to other department, non-education&quot;</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>notEqual</operation>
            <value>Data Integrator</value>
        </criteriaItems>
        <criteriaItems>
            <field>DX_Notes__c.PC_Reason_Code_1__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Trigger the patient care qualtrics survey when a patient care note is saved in salesforce.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send DxNotes To Oracle</fullName>
        <actions>
            <name>Create_DxNotes_In_Oracle</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>DX_Notes__c.Comments__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>DX_Notes__c.CreatedById</field>
            <operation>notEqual</operation>
            <value>Actian DataIntegrator</value>
        </criteriaItems>
        <criteriaItems>
            <field>DX_Notes__c.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Coaching Program DX Notes</value>
        </criteriaItems>
        <description>Workflow will trigger the outbound message to send the DxNote information to Oracle when a DxNote is created. 06/21/2017 - Added the constraint to ignore the notes created by Integration.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
