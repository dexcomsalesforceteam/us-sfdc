<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Uncheck_Send_to_Oracle_Flag</fullName>
        <description>Send To Oracle = False</description>
        <field>Send_To_Oracle__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Send to Oracle Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Skip_Apex_Flag</fullName>
        <field>Skip_Apex__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Skip Apex Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Inactive_Flag</fullName>
        <description>Update Address inactive flag</description>
        <field>Inactive__c</field>
        <literalValue>1</literalValue>
        <name>Update Inactive Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Is_Update_in_Progress</fullName>
        <description>Is Update In Progress = True</description>
        <field>Is_Update_In_Progress__c</field>
        <literalValue>1</literalValue>
        <name>Update Is Update in Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Upsert_Address_In_Oracle</fullName>
        <apiVersion>45.0</apiVersion>
        <description>Outbound message will send Address data to Oracle</description>
        <endpointUrl>https://itapi-play.dexcom.com/ws/soap/UpsertAddress;boomi_auth=c2FsZXNmb3JjZUBkZXhjb21pbmMtNk82SjBPOmU4NDQyOTdjLTVhOWEtNDkyZi1hYTAzLWI0OGMzZmRiMzFiZQ==</endpointUrl>
        <fields>AccountNumber__c</fields>
        <fields>Address_18_Digit_ID__c</fields>
        <fields>Address_Type__c</fields>
        <fields>City__c</fields>
        <fields>Country__c</fields>
        <fields>County__c</fields>
        <fields>Id</fields>
        <fields>Inactive__c</fields>
        <fields>Oracle_Address_ID__c</fields>
        <fields>Party_ID__c</fields>
        <fields>Primary_Flag__c</fields>
        <fields>State__c</fields>
        <fields>Street_Address_1__c</fields>
        <fields>Street_Address_2__c</fields>
        <fields>Street_Address_3__c</fields>
        <fields>Street_Address_4__c</fields>
        <fields>Zip_Postal_Code__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>buyeda@dexcom.com</integrationUser>
        <name>Upsert Address In Oracle</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Inactivate on Error</fullName>
        <actions>
            <name>Update_Inactive_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Address__c.Integration_Error_Message__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>If the integration comes back with an error, inactivate the address record.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Address To Oracle</fullName>
        <actions>
            <name>Uncheck_Send_to_Oracle_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Uncheck_Skip_Apex_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Is_Update_in_Progress</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Upsert_Address_In_Oracle</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Jagan 04/23/2019 - Incorporated logic for Lightning conversion
Modified for INC0255977,INC0261695 - added conditions to trigger workflow when street1 is not blank and country is US</description>
        <formula>AND( 
	Account__r.RecordType.DeveloperName = &apos;Consumers&apos;,
        NOT(ISBLANK( Country__c )), 
        UPPER(Country__c ) = &apos;US&apos;,
	OR(
		AND(
			ISNEW(), 
			NOT(ISBLANK(Street_Address_1__c))
		), 
		ISCHANGED(Street_Address_2__c), 
		ISCHANGED(Inactive__c), 
		ISCHANGED(Primary_Flag__c), 
		AND(ISCHANGED(Send_To_Oracle__c), 
		Send_To_Oracle__c = true)
	) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
