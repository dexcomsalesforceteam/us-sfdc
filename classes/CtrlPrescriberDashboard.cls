/**********************************************
 * @author      : Sundog Interactive
 * @date        : JAN 25, 2019
 * @description : Server side controller for PrescriberDashboard.cmp used in the Dexcom Prescriber Community
**********************************************/
public without sharing class CtrlPrescriberDashboard {
    
    /**********************************************************
     **Description: Method called on init to obtain running user information
     **Parameters:  None
     **Returns:     Contact record related to running user
     **Author:      Sundog Interactive
     **Date Created:   JAN 25, 2019
    **********************************************************/
    @AuraEnabled
    public static Contact getUserInfo(){
        String contactId = [SELECT ContactId FROM User WHERE Id = :UserInfo.getUserId()].ContactId;
        return [SELECT Id, Name, PersonAccountId__c, PersonAccountId__r.Party_ID__c FROM Contact WHERE Id =:contactId];
    }       
    
    /*
     * 
     */ 
    /**********************************************************
     **Description: Returns to client certain opportunities and order headers under a prescriber Account
     **Parameters:  None
     **Returns:     Account information, related opportunities and order headers in form of custom wrapper object ClsPrescriberDashboardHelper.OpportunitiesAndOrders
     **Author:      Sundog Interactive
     **Date Created:   JAN 25, 2019
    **********************************************************/
    @AuraEnabled
    public static ClsPrescriberDashboardHelper.OpportunitiesAndOrders getOpportunitiesAndOrders(String dateRange){
        Contact c = getUserInfo();
        Id prescriber = c.PersonAccountId__c;

        if (prescriber != null){
            Account personAccount = new Account(Id = c.PersonAccountId__c);
            return ClsPrescriberDashboardHelper.getOpportunitiesAndOrders(new List<Account>{personAccount}, dateRange);
        }

        return null;
    }

    /**********************************************************
     **Description: An empty call to the server to ensure that the Salesforce login session for the user does not time out.
     *              (Most actions for this component are contained in the client side and the activity is not registered by Salesforce,
     *              therefore we have this empty method in the server side in order to enforce session logout and extend session timers at a Salesforce level)
     **Parameters:  None
     **Returns:     Boolean indicating if security check passed (true) or failed (false)
     **Author:      Sundog Interactive
     **Date Created:   MAY 21, 2019
    **********************************************************/
    @AuraEnabled
    public static void resetSessionTimer(){
        System.debug('resetting session timer');
    }
}