/***
*@Author        : Abdul Mohammed
*@Date Created  : 12/11/2019
*@Description   : Create for Auto QC story: "CRMSF-4614". Used from BclsOrderAutoQCBatch class.
***/

Public class ClsUpdateOfAUtoQBatchController{ 
    
    public static void bclsOrderAutoQCBatchExecuteMothod(List<order> scope){
    
        List<Order> currentOrderList = new List<Order>();        
        Set<string>orderAllCheckPassed = new Set<string>();
        
        if(!scope.isEmpty()){
            for(Order ord : scope){
                if(!(ord.Is_Benefit_Check_Completed__c&& ord.Is_Payor_Check_Completed__c && ord.Is_Document_Check_Completed__c && ord.Is_Order_Check_Completed__c))
                    currentOrderList.add(ord);     
                else  orderAllCheckPassed.add(ord.Id);               
            }  
            if(currentOrderList.size() >0)ClsUpdateOfAUtoQBatchController.saveDetails(currentOrderList);
            //Order Activation, suppose we got a DML exceptions on first time of batch run
            if(orderAllCheckPassed.size() >0)ClsUpdateOfAUtoQBatchController.invokeOrderActivation(orderAllCheckPassed);
            
        }
    }
    
    public static void invokeOrderActivation(Set<string> orderAllCheckPassed){
        
        if(orderAllCheckPassed.size() >0){
            Map<Id,order> orderMap=new  Map<Id,order>([select id,Scheduled_Ship_Date__c,Price_Book__r.Name, Price_Book__r.Oracle_Id__c, 
                AccountId, Account.Last_Order_Pricebook__c, Type_Of_Order__c, status, Is_Cash_Order__c,Account.Payor__r.name,Account.Payor__r.CMN_Waived__c,
                (select Id,Audit_Area__c,Audit_Exception__c,Audit_Exception_Added_By__c,Audit_Exception_Added_Date__c,Order_Number__c
                from Order_Audits__r where Audit_Field__c =:ClsApexConstants.AUDIT_BYPASSCHECK and Audit_Area__c =:ClsApexConstants.AUDITAREA_DOCUMENT order by createddate desc limit 1 )
                from order where id in:orderAllCheckPassed
            ]);
            List<Order_Audit__c> updateByPassCheckAuditRecords = new List<Order_Audit__c>();

            for(order orderRec : orderMap.values()){
                if(orderRec.Order_Audits__r.size() >0 && orderRec.Order_Audits__r != null){
                    Order_Audit__c oa = orderRec.Order_Audits__r[0];
                    oa.Audit_Exception__c = true;
                    oa.Audit_Exception_Added_By__c = System.UserInfo.getUserId();
                    oa.Audit_Exception_Added_Date__c = System.now();
                    updateByPassCheckAuditRecords.add(oa);
                }
            }
            system.debug('======updateByPassCheckAuditRecords======'+updateByPassCheckAuditRecords);
            if(updateByPassCheckAuditRecords.size() >0)Database.update(updateByPassCheckAuditRecords,false);
        }
    }
    
    public static void saveDetails(List<order> orders){
        
        Map<Id,order> orderMap=new  Map<Id,order>([select id,Scheduled_Ship_Date__c,Price_Book__r.Name, Price_Book__r.Oracle_Id__c, 
            AccountId, Account.Last_Order_Pricebook__c,Account.CMN_Expiration_Date_Formula__c, Type_Of_Order__c, status, Is_Cash_Order__c,Account.Payor__r.name,Account.Payor__r.CMN_Waived__c,
            (select Id,Audit_Area__c,Audit_Exception__c,Audit_Exception_Added_By__c,Audit_Exception_Added_Date__c,Order_Number__c
            from Order_Audits__r where Audit_Field__c =:ClsApexConstants.AUDIT_BYPASSCHECK and Audit_Area__c =:ClsApexConstants.AUDITAREA_DOCUMENT order by createddate desc limit 1 )
            from order where id in:orders
        ]);
        
        List<SObject> orderFinalAuditList = new List<SObject>();
        Map<Id,Order>accountOrderMap = new Map<Id,Order>();
        for(order orderRec : orderMap.values()){
             if(orderRec.AccountId != null){
                 accountOrderMap.put(orderRec.AccountId,orderRec);
            }
        }
        
        if(accountOrderMap.keyset().size() >0){
            
            Set<Id> benefitsUpdateTaskId = new Set<Id>();
            List<Benefits__c> benefitsToBeVerified = new List<Benefits__c>();
            for(Benefits__c thisRecord :[SELECT Id, Name,Account__c, 
               Benefit_Hierarchy__c, Payor__c, Start_Date__c, LastModifiedById, CO_PAY__c, INDIVIDUAL_DEDUCTIBLE__c,Last_Benefits_Check_Date__c,
               INDIVIDUAL_MET__c, Family_Met__c, INDIVIDUAL_OOP_MAX__c, FAMILY_DEDUCT__c, FAMILY_OOP_MAX__c,Quadax_Trading_Partner_Id__c,
               Coverage__c FROM Benefits__c WHERE Benefit_Hierarchy__c =:ClsApexConstants.BENEFIT_PRIMARY AND Account__c IN : accountOrderMap.keyset()
            ]){
                Order currentAccountOrder = (accountOrderMap.containskey(thisRecord.Account__c) && accountOrderMap.get(thisRecord.Account__c) != null)? accountOrderMap.get(thisRecord.Account__c) : new Order();
                if(thisRecord.Last_Benefits_Check_Date__c != null && currentAccountOrder.Scheduled_Ship_Date__c != null){
                    Integer numberDaysDue = thisRecord.Last_Benefits_Check_Date__c.daysBetween(currentAccountOrder.Scheduled_Ship_Date__c);
                    system.debug('=== numberDaysDue ===' +numberDaysDue);
                    if(numberDaysDue >30 && thisRecord.Quadax_Trading_Partner_Id__c != null){
                        benefitsToBeVerified.add(thisRecord);
                        system.debug( '=== BI CHECKING ===');
                    }else if(numberDaysDue >30 && thisRecord.Quadax_Trading_Partner_Id__c == null){
                         //benefitsUpdateTaskId.add(thisRecord.id);
                         benefitsUpdateTaskId.add(thisRecord.Account__c);
                         system.debug( '=== No Quadax - Task creating ===');
                    }
                }
            } 
            
            if(!benefitsToBeVerified.isEmpty()){
                system.debug('before calling the queueable job:::: list is:::'+benefitsToBeVerified);
                ID jobID = System.enqueueJob(new ClsInvokeBIVerificationQueueable(benefitsToBeVerified,'order'));
                System.debug('**after* Queueable invoked + Job Id :: ' + benefitsToBeVerified );
            }
            
            if(benefitsUpdateTaskId.size() > 0)OrderAuditTriggerHandler.afterUpdateCreateTask( benefitsUpdateTaskId,label.Task_Description_for_any_check_fail);
        }
        
        List<Order_Audit__c> updateByPassCheckAuditRecords = new List<Order_Audit__c>();
        List<Order> orderACtivationList = new List<Order> ();

        for(order orderRec : orderMap.values()){
            
            Map<String, String>  benefitMap = new Map<String, String> ();
            Map<String, String>  payorMap  = new Map<String, String> ();
            Map<String, String>  thisOrderMap  = new Map<String, String> ();
            Map<String, String>  documentsMap  = new Map<String, String> ();
          
            Boolean isOrderActivated = orderRec.status == ClsApexConstants.STATUS_ACTIVATED ? true : false;
            
            if(String.isNotBlank(orderRec.Account.Payor__c)){
                String payorName = orderRec.Account.Payor__r.name.toUpperCase(); 
            }
            String orderTypeValue = orderRec.Account.Last_Order_Pricebook__c != orderRec.Price_Book__r.Name ? 'New Order' : 'Reorder';
            System.debug('=== Order Trigger Handler : Order Type === ' +orderTypeValue);
            
            benefitMap = ClsQCHoldBenefitsCheckService.getBIDetails(String.valueOf(orderRec.Id), String.valueOf(orderRec.AccountId), orderTypeValue , isOrderActivated);
            payorMap = ClsQCHoldPayorCheckService.getPayorDetails(String.valueOf(orderRec.Id), String.valueOf(orderRec.AccountId), orderTypeValue , isOrderActivated);
            thisOrderMap = ClsQCHoldOrderCheckService.getOrderDetails(String.valueOf(orderRec.Id), String.valueOf(orderRec.AccountId), orderTypeValue , isOrderActivated);
            system.debug('===CMN field Value==='+ orderRec.Account.CMN_Expiration_Date_Formula__c);
            if(orderRec.Order_Audits__r.size() >0 && orderRec.Order_Audits__r != null && orderRec.Account.CMN_Expiration_Date_Formula__c != null && orderRec.Account.CMN_Expiration_Date_Formula__c >=System.today() ){
                system.debug('====CMN Expiration===');
                Order_Audit__c oa = orderRec.Order_Audits__r[0];
                oa.Audit_Exception__c = true;
                oa.Audit_Exception_Added_By__c = System.UserInfo.getUserId();
                oa.Audit_Exception_Added_Date__c = System.now();
                updateByPassCheckAuditRecords.add(oa);
            }
          
            List<SObject> orderCurrentList = ClsUpdateOfAUtoQBatchController.saveOrderDetails(orderRec.Id, benefitMap,payorMap, thisOrderMap,documentsMap);
            system.debug('orderCurrentList'+orderCurrentList);
            if(orderCurrentList.size() > 0 && orderCurrentList != null){
                orderFinalAuditList.addAll(orderCurrentList);
            }
            system.debug('=====orderFinalAuditList========================'+orderFinalAuditList);
        }
        system.debug('=====orderACtivationList=========^^^^^==============='+orderACtivationList);

       
        if(updateByPassCheckAuditRecords.size() >0){
           
            try{ 
                Database.update(updateByPassCheckAuditRecords,false);
            }catch (DmlException de) {
                 new ClsApexDebugLog().createLog(new ClsApexDebugLog.Error('ClsUpdateOfAUtoQBatchController', 'bclsOrderAutoQCBatchExecuteMothod','','Order Audit Bypass Update failed: ' + de.getMessage()));
            }
        }
        if(!orderFinalAuditList.isEmpty()){
            try{
               Database.update(orderFinalAuditList,false);
            }catch(DmlException Ex){ 
                System.debug('Exception to Save : Audit Area : :: Exception :: ' + Ex);
                new ClsApexDebugLog().createLog(new ClsApexDebugLog.Error('ClsUpdateOfAUtoQBatchController', 'bclsOrderAutoQCBatchExecuteMothod','','Order Audit Records Update failed: ' + Ex.getMessage()));
                
            }
        }
     }
  
    public static List<SObject>  saveOrderDetails(String orderId, Map<String, String> benefitMap, Map<String, String> payorMap, Map<String, String> orderMap, Map<String, String> documentMap) {

        List<SObject> availableOrderList = new List<SObject>();
        System.debug('benefitMap :: ' + benefitMap);
        if(benefitMap != null){

            List<SObject>  benefitCheck = saveAuditCheckInfo(orderId, 'Benefit', benefitMap);
            if(benefitCheck.size() >0 && benefitCheck != null)availableOrderList.addAll(benefitCheck);
            System.debug('benefitCheck :: ' + benefitCheck);
        }
        System.debug('payorMap :: ' + payorMap);
        if(payorMap != null){
            List<SObject>  payorCheck  = saveAuditCheckInfo(orderId, 'Payor', payorMap);
            System.debug('payorCheck :: ' + payorCheck);
            if(payorCheck.size() >0 && payorCheck != null)availableOrderList.addAll(payorCheck);
        }
        System.debug('orderMap :: ' + orderMap);
        if(orderMap != null){
            List<SObject>  orderCheck = saveAuditCheckInfo(orderId, 'Order', orderMap);
            if(orderCheck.size() >0 && orderCheck != null)availableOrderList.addAll(orderCheck);
            System.debug('orderCheck :: ' + orderCheck);
        }        
        System.debug('documentMap :: ' + documentMap);
       
        return availableOrderList; 
    }
    
    public static List<SObject> saveAuditCheckInfo(String orderId, String auditArea, Map<String, String> checkDataMap){
        List<SObject> orderCheckList = new List<SObject>();
        
        Map<String, Order_Audit__c> orderAuditMap = new Map<String, Order_Audit__c>();
        Map<String, Order_Audit_Item__c> orderAuditItemMap = new Map<String, Order_Audit_Item__c>();
        Set<Order_Audit__c> updateAuditSet = new Set<Order_Audit__c>();
        List<Order_Audit_Item__c> updateAuditItemList = new List<Order_Audit_Item__c>();
        Set<Id> auditIdSet = new Set<Id>();
        Map<String, String> fieldMappings = new Map<String, String>();
        fieldMappings = ClsOrderAuditHandler.getfieldMappings(auditArea);
        System.debug('checkDataMap >>' + checkDataMap);
        for(Order_Audit__c thisAudit : [Select Id, Audit_Area__c, Audit_Exception__c, Audit_Field__c, Audit_Verified__c, Order_Number__c,
                                        Audit_Exception_Added_By__c, Audit_Exception_Added_Date__c from Order_Audit__c 
                                        where Order_Number__c =:orderId and Audit_Area__c =: auditArea]){
                orderAuditMap.put(thisAudit.Audit_Field__c , thisAudit);
                auditIdSet.add(thisAudit.Id);
        }
        
        for(Order_Audit_Item__c thisAuditItem : [Select Id, Audit_Item_Field__c, Audit_Item_Value__c, Order_Audit__c from Order_Audit_Item__c where Order_Audit__c IN : auditIdSet]){         
            orderAuditItemMap.put(thisAuditItem.Audit_Item_Field__c, thisAuditItem);            
        }
        
        if(!checkDataMap.isEmpty()){
            for(String auditDataRecord : checkDataMap.keySet()){
                for(String thisAuditName : orderAuditMap.keySet()){
                    Order_Audit__c thisAudit = orderAuditMap.get(thisAuditName);
                    
                    if(auditDataRecord != 'bypassCheck' && auditDataRecord == fieldMappings.get(thisAuditName) && thisAudit.Audit_Verified__c != Boolean.valueOf(checkDataMap.get(auditDataRecord))){
                        thisAudit.Audit_Verified__c = Boolean.valueOf(checkDataMap.get(auditDataRecord));
                        updateAuditSet.add(thisAudit);
                    }
                    if(auditDataRecord == 'bypassCheck' && Boolean.valueOf(checkDataMap.get(auditDataRecord)) && 
                        thisAudit.Audit_Field__c == ClsApexConstants.AUDIT_BYPASSCHECK && !thisAudit.Audit_Exception__c){
                        thisAudit.Audit_Exception__c = true;
                        thisAudit.Audit_Exception_Added_By__c = System.UserInfo.getUserId();
                        thisAudit.Audit_Exception_Added_Date__c = System.today();
                        updateAuditSet.add(thisAudit);
                    }
                    if(auditDataRecord == 'bypassCheck' && !Boolean.valueOf(checkDataMap.get(auditDataRecord)) && 
                        thisAudit.Audit_Field__c == ClsApexConstants.AUDIT_BYPASSCHECK && thisAudit.Audit_Exception__c){
                        thisAudit.Audit_Exception__c = false;
                        thisAudit.Audit_Exception_Added_By__c = null;
                        thisAudit.Audit_Exception_Added_Date__c = null;
                        updateAuditSet.add(thisAudit);
                    }
                }
            }
            System.debug('orderAuditItemMap >> ' +orderAuditItemMap );
            for(String auditDataRecord : checkDataMap.keySet()){ 
                for(String thisAuditItemName : orderAuditItemMap.keySet()){ 
                    Order_Audit_Item__c thisAuditItem = orderAuditItemMap.get(thisAuditItemName);                    
                    if(auditDataRecord == fieldMappings.get(thisAuditItemName) && thisAuditItem.Audit_Item_Value__c != checkDataMap.get(auditDataRecord)){
                        thisAuditItem.Audit_Item_Value__c = checkDataMap.get(auditDataRecord);
                        updateAuditItemList.add(thisAuditItem);
                    }
                }
            }
            if(!updateAuditSet.isEmpty()) {
                for(Order_Audit__c thisAudit : updateAuditSet)orderCheckList.add(thisAudit);                
            }
            if(!updateAuditItemList.isEmpty()) orderCheckList.addAll(updateAuditItemList);
        }
        return orderCheckList;
    }
}