/*--------------------------------------------------------------------------------------------------
@Author      - Sudheer Vemula
@Date        - 10/23/2019
@Description - CRMSF-4519:Batch class Create Proactive Document Expired Tasks.
--------------------------------------------------------------------------------------------------*/
public class BclsSSIPOrderCancelBatch implements Database.Batchable<sObject> {
    
    public Database.QueryLocator start(Database.BatchableContext BC){
        
        Date last6MonthsDate = system.today() - 180;
        system.debug('###last6MonthsDate = ' + last6MonthsDate);
        String query ='select id,Close_Reason__c,Rule_End_Date__c,Account__r.Last_Standard_Sales_Order_Ship_Date__c from SSIP_Rule__c where (Account__r.Last_Standard_Sales_Order_Ship_Date__c < :last6MonthsDate OR (Account__r.Last_Standard_Sales_Order_Ship_Date__c = null and First_Shipment_Date__c = TODAY)) and (Rule_End_Date__c > TODAY OR Rule_End_Date__c = null)';
        system.debug('####query = ' + query);
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext Bc,List<SSIP_Rule__c> ssipRulesList){
        Date todayDate = system.today();
        for(SSIP_Rule__c ssip:ssipRulesList){
            ssip.Close_Reason__c = ClsApexConstants.SSIP_CLOSE_REASON;
            if(ssip.Rule_End_Date__c > todayDate || ssip.Rule_End_Date__c == null){
                ssip.Rule_End_Date__c = todayDate;
            }
        }
        
        Database.SaveResult[] saveResultObjList = Database.update(ssipRulesList,false);
        system.debug('Updated SSIP Rules...'+saveResultObjList);
        
        List<Apex_Debug_Log__c> apexDebugLogList = new List<Apex_Debug_Log__c>();
        for(Database.SaveResult saveResultObj : saveResultObjList){
            if (!saveResultObj.isSuccess()){
                for(Database.Error err : saveResultObj.getErrors()){
                    Apex_Debug_Log__c apexDebugLogObj = new Apex_Debug_Log__c();
                    apexDebugLogObj.Apex_Class__c = 'BclsSSIPOrderCancelBatch';
                    apexDebugLogObj.Method__c = 'Execute';
                    apexDebugLogObj.Message__c = err.getMessage();
                    apexDebugLogObj.Type__c = ClsApexConstants.DEBUG_TYPE_ERROR;
                    apexDebugLogList.add(apexDebugLogObj);
                }
            }
        }
        if(!apexDebugLogList.isEmpty()){
            Database.insert(apexDebugLogList, ClsApexConstants.BOOLEAN_FALSE);
        }
        
        
    }
    
    public void finish(Database.BatchableContext Bc){
        system.debug('Finish BclsSSIPOrderCancelBatch Batch...'); 
    }
}