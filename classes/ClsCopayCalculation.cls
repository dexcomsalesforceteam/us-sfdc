public class ClsCopayCalculation
{
    /**********************************************************************************************
    @Author        : Jagan Periyakaruppan
    @Date Modified    : 10/18/2017
    @Description    : This class is used to calculate the BI estimated costs
    1. ProcessAccountsForCopayCalculation - Method Invoked from Account and Benefit objects
    2. GetPriceToProductsForAccounts - Method returns the Price to Product map for each account
    3. ProcessOpportunitiesForCopayCalculation - Method Invoked from Opportunity objects
    4. GetPriceToProductsForOpportunities - Method returns the Price to Product map for each Opportunity
    5. GetPriceToProducts - Method retrieves Price to Product map
    6. CalculateCopay - Calculates copay
    **************************************************************************************************/
    //Method will process account records for calculating Copay
    public static void ProcessAccountsForCopayCalculation (Map<Id, Account>  accountMap, Map<Id, Benefits__c> accountIdToBenefitMap, String invokedFromObject)
    {   
        //Get the Pricebook to its Name mapping
        Map<Id, String> pbIdToNameMap = GetNameForPricebook(accountMap, null, 'Account');       
        //Get the Product to Price mapping as per the default pricebook tied to the accounts 
        Map<Id, Map<String, Decimal>> accountIdToproductToPriceMap = GetPriceToProductsForAccounts(accountMap);

        //Process accounts
        for(Account acc : accountMap.values())
        {
            Decimal receiverPrice = 0;
            Decimal transmitterPrice = 0;
            Decimal sensorPrice = 0;
            Decimal medicareReceiverPrice = 0;
            Decimal medicareSubscriptionPrice = 0;
            system.debug('****Account Id ' + acc.Id);
            //Find the account customer type
            String accountType = pbIdToNameMap.get(acc.Default_Price_Book__c).startsWith('M/C') ? 'Medicare' : 'Non-Medicare'; 
            //String accountType = (acc.Customer_Type__c == 'Medicare FFS' || (acc.Customer_Type__c == 'Medicare Advantage' && acc.Consumer_Payor_Code__c == 'K')) ? 'Medicare' : 'Non-Medicare'; 
            system.debug('****Customer Type ' + accountType);
            //Get all the price values for Benefits calculation
            receiverPrice = accountIdToproductToPriceMap.get(acc.Id).get('STK-SD-001');
            system.debug('****receiverPrice ' + receiverPrice);
            transmitterPrice = accountIdToproductToPriceMap.get(acc.Id).get('BUN-GF-003');
            system.debug('****transmitterPrice ' + transmitterPrice);
            sensorPrice = accountIdToproductToPriceMap.get(acc.Id).get('STS-GL-041');
            system.debug('****sensorPrice ' + sensorPrice);
            medicareReceiverPrice = accountIdToproductToPriceMap.get(acc.Id).get('STK-MC-001');
            system.debug('****medicareReceiverPrice ' + medicareReceiverPrice);
            medicareSubscriptionPrice = accountIdToproductToPriceMap.get(acc.Id).get('MT-MC-SUB');
            system.debug('****medicareSubscriptionPrice ' + medicareSubscriptionPrice);
            //Invoke method to calculate copay 
            Decimal copay = CalculateCopay(accountType, 1, 1, 1, receiverPrice, transmitterPrice, sensorPrice, medicareReceiverPrice, medicareSubscriptionPrice, accountIdToBenefitMap.get(acc.Id
            ));
            acc.Estimated_Cost__c = copay;
        }
        //Issue update statement only when the invocation is from Benefit object
        if(invokedFromObject == 'Benefit')
        {
            try{
                update accountMap.values();
            }catch(Exception e)
            {
                System.debug('***ACCOUNT COPAY UPDATE ERROR = ' + e.getMessage());
            }
        }
    }
    
    //Method will return the Pricebook Id to Name map
    public static Map<Id, String> GetNameForPricebook(Map<Id, Account>  accountMap, Map<Id, Opportunity>  oppMap, String objectName)
    {
        Map<Id, String> pbIdToNameMap = new Map<Id, String>();
        
        Set<Id> pricebookIds = new Set<Id>();
        
        if(objectName == 'Account')
        {
            for(Account acc : accountMap.values())
            {
                pricebookIds.add(acc.Default_Price_Book__c);
            }
        } else if (objectName == 'Opportunity')
        {
            for(Opportunity opp : oppMap.values())
            {
                pricebookIds.add(opp.Pricebook2Id);
            }
        }
        
        for(Pricebook2 pb : [SELECT Id, Name from Pricebook2 WHERE Id IN : pricebookIds])
        {
            pbIdToNameMap.put(pb.Id, pb.Name);
        }
        return pbIdToNameMap;
    }
    
    //Method will return the product to price mapping for each account as a Map
    public static Map<Id, Map<String, Decimal>> GetPriceToProductsForAccounts(Map<Id, Account>  accountMap)
    {
        Map<Id, Map<String, Decimal>> accountIdToproductToPriceMap = new Map<Id, Map<String, Decimal>>();
        
        Set<Id> pricebookIds = new Set<Id>();
        
        for(Account acc : accountMap.values())
        {
            pricebookIds.add(acc.Default_Price_Book__c);
        }
        //Get the product to price mapping for all the Pricebook Ids tied to the accounts 
        Map<Id, Map<String, Decimal>> pbIdToproductToPriceMap = GetPriceToProducts(pricebookIds);
        
        //For each account prepare a map between Account Id and Product to Price map
        for(Account acc : accountMap.values())
        {
            Map<String, Decimal> productToPriceMap = new Map<String, Decimal>();
            productToPriceMap.put('STK-SD-001', pbIdToproductToPriceMap.get(acc.Default_Price_Book__c).get('STK-SD-001'));
            productToPriceMap.put('BUN-GF-003', pbIdToproductToPriceMap.get(acc.Default_Price_Book__c).get('BUN-GF-003'));
            productToPriceMap.put('STS-GL-041', pbIdToproductToPriceMap.get(acc.Default_Price_Book__c).get('STS-GL-041'));
            productToPriceMap.put('STK-MC-001', pbIdToproductToPriceMap.get(acc.Default_Price_Book__c).get('STK-MC-001'));
            productToPriceMap.put('MT-MC-SUB', pbIdToproductToPriceMap.get(acc.Default_Price_Book__c).get('MT-MC-SUB'));
            accountIdToproductToPriceMap.put(acc.Id, productToPriceMap);
        }
        return accountIdToproductToPriceMap;
    }
    
    //Method will process opportunity records when the pricebook changes on Opportunity
    public static void ProcessOpportunitiesForCopayCalculation (Map<Id, Opportunity>  oppMap, Map<Id, Benefits__c> oppIdToBenefitMap)
    {
        //Get the Pricebook to its Name mapping
        Map<Id, String> pbIdToNameMap = GetNameForPricebook(null, oppMap, 'Opportunity');
        //Get the Product to Price mapping as per the default pricebook tied to the accounts 
        Map<Id, Map<String, Decimal>> opptyIdToproductToPriceMap = GetPriceToProductsForOpportunities(oppMap);

        //Process Opportunities
        for(Opportunity opp : oppMap.values())
        {
            Decimal receiverPrice = 0;
            Decimal transmitterPrice = 0;
            Decimal sensorPrice = 0;
            Decimal medicareReceiverPrice = 0;
            Decimal medicareSubscriptionPrice = 0;
            system.debug('****Opp Id ' + Opp.Id);
            //Find the customer type
            String accountType = pbIdToNameMap.get(opp.Pricebook2Id).startsWith('M/C') ? 'Medicare' : 'Non-Medicare'; 
            //String accountType = (opp.Customer_Type__c == 'Medicare FFS' || (opp.Customer_Type__c == 'Medicare Advantage' && opp.Payor_Code__c == 'K')) ? 'Medicare' : 'Non-Medicare'; 
            system.debug('****Customer Type ' + accountType);
            //Get all the price values for Benefits calculation
            receiverPrice = opptyIdToproductToPriceMap.get(opp.Id).get('STK-SD-001');
            system.debug('****receiverPrice ' + receiverPrice);
            transmitterPrice = opptyIdToproductToPriceMap.get(opp.Id).get('BUN-GF-003');
            system.debug('****transmitterPrice ' + transmitterPrice);
            sensorPrice = opptyIdToproductToPriceMap.get(opp.Id).get('STS-GL-041');
            system.debug('****sensorPrice ' + sensorPrice);
            medicareReceiverPrice = opptyIdToproductToPriceMap.get(opp.Id).get('STK-MC-001');
            system.debug('****medicareReceiverPrice ' + medicareReceiverPrice);
            medicareSubscriptionPrice = opptyIdToproductToPriceMap.get(opp.Id).get('MT-MC-SUB');
            system.debug('****medicareSubscriptionPrice ' + medicareSubscriptionPrice);
            //Invoke method to calculate copay 
            Decimal copay = CalculateCopay(accountType, 1, 1, 1, receiverPrice, transmitterPrice, sensorPrice, medicareReceiverPrice, medicareSubscriptionPrice, oppIdToBenefitMap.get(opp.Id));
            opp.Estimated_Cost__c = copay;
        }
    }

    //Method will return the product to price mapping for each opportunity as a Map
    public static Map<Id, Map<String, Decimal>> GetPriceToProductsForOpportunities(Map<Id, Opportunity>  opptyMap)
    {
        Map<Id, Map<String, Decimal>> opptyIdToproductToPriceMap = new Map<Id, Map<String, Decimal>>();
        
        Set<Id> pricebookIds = new Set<Id>();
        
        for(Opportunity opp : opptyMap.values())
        {
            pricebookIds.add(opp.Pricebook2Id);
        }
        //Get the product to price mapping for all the Pricebook Ids tied to the accounts 
        Map<Id, Map<String, Decimal>> pbIdToproductToPriceMap = GetPriceToProducts(pricebookIds);
        
        //For each account prepare a map between Account Id and Product to Price map
        for(Opportunity opp : opptyMap.values())
        {
            Map<String, Decimal> productToPriceMap = new Map<String, Decimal>();
            productToPriceMap.put('STK-SD-001', pbIdToproductToPriceMap.get(opp.Pricebook2Id).get('STK-SD-001'));
            productToPriceMap.put('BUN-GF-003', pbIdToproductToPriceMap.get(opp.Pricebook2Id).get('BUN-GF-003'));
            productToPriceMap.put('STS-GL-041', pbIdToproductToPriceMap.get(opp.Pricebook2Id).get('STS-GL-041'));
            productToPriceMap.put('STK-MC-001', pbIdToproductToPriceMap.get(opp.Pricebook2Id).get('STK-MC-001'));
            productToPriceMap.put('MT-MC-SUB', pbIdToproductToPriceMap.get(opp.Pricebook2Id).get('MT-MC-SUB'));
            opptyIdToproductToPriceMap.put(opp.Id, productToPriceMap);
        }
        return opptyIdToproductToPriceMap;
    }   
    //Method will return the product to price mapping as listed in the pricebook
    public static Map<Id, Map<String, Decimal>> GetPriceToProducts(Set<Id> pricebookIds)
    {
        Map<Id, Map<String, Decimal>> pbIdToproductToPriceMap = new Map<Id, Map<String, Decimal>>();
        
            for(Id pbId : pricebookIds)
            {
                //Map holds product name to its unitprice as listed in the pricebook
                Map<String, Decimal> productToPriceMap = new Map<String, Decimal>();
                for(PriceBookEntry pbe : [SELECT ProductCode, UnitPrice FROM PricebookEntry where ProductCode in ('STK-SD-001', 'BUN-GF-003', 'STS-GL-041', 'STK-MC-001', 'MT-MC-SUB') and IsActive = TRUE and IsDeleted = FALSE and pricebook2Id = :pbId])
                {
                    if(pbe.ProductCode == 'STK-SD-001')
                        productToPriceMap.put('STK-SD-001', pbe.UnitPrice);
                    if(pbe.ProductCode == 'BUN-GF-003')
                        productToPriceMap.put('BUN-GF-003', pbe.UnitPrice);
                    if(pbe.ProductCode == 'STS-GL-041')
                        productToPriceMap.put('STS-GL-041', pbe.UnitPrice);
                    if(pbe.ProductCode == 'STK-MC-001')
                        productToPriceMap.put('STK-MC-001', pbe.UnitPrice);
                    if(pbe.ProductCode == 'MT-MC-SUB')
                        productToPriceMap.put('MT-MC-SUB', pbe.UnitPrice);
                }
                //Map holds the account Id to its Product - Pricebook mapping
                pbIdToproductToPriceMap.put(pbId, productToPriceMap);
            }   
        return pbIdToproductToPriceMap;
    }
    //Calculate the Copay logic for each Benefit
    public static Decimal CalculateCopay (String accountType, Integer receiverQty, Integer transmitterQty, Integer sensorQty, Decimal receiverPrice, Decimal transmitterPrice, Decimal sensorPrice, Decimal medicareReceiverPrice, Decimal medicareSubscriptionPrice,  Benefits__c benefit)
    {
        
        //Added if bloc to avoid null pointer exception
        if(benefit != null)
        {
            //Get all the attributes from Benefit to calculate the Estimated Copay 
            Decimal coverage = benefit.Coverage__c == null ? 0 : benefit.Coverage__c;
            Decimal copayDOS = benefit.CO_PAY__c == null ? 0 : benefit.CO_PAY__c;
            Decimal copayLineItem = benefit.Copay_Line_Item__c == null ? 0 : benefit.Copay_Line_Item__c;
            Decimal indDeductible = benefit.INDIVIDUAL_DEDUCTIBLE__c == null ? 0 : benefit.INDIVIDUAL_DEDUCTIBLE__c;
            Decimal indDeductibleMet = benefit.INDIVIDUAL_MET__c == null ? 0 : benefit.INDIVIDUAL_MET__c;
            Decimal indOOPMax = benefit.INDIVIDUAL_OOP_MAX__c == null ? 0 : benefit.INDIVIDUAL_OOP_MAX__c;
            Decimal indOOPMaxMet = benefit.INDIVIDUAL_OOP_MET__c == null ? 0 : benefit.INDIVIDUAL_OOP_MET__c;
            Decimal estimatedCosts = 0;
            Decimal famDeductible = benefit.FAMILY_DEDUCT__c == null ? 0 : benefit.FAMILY_DEDUCT__c;
            Decimal famDeductibleMet = benefit.Family_Met__c == null ? 0 : benefit.Family_Met__c;
            Decimal famOOPMax = benefit.FAMILY_OOP_MAX__c == null ? 0 : benefit.FAMILY_OOP_MAX__c;
            Decimal famOOPMaxMet = benefit.FAMILY_OOP_MET__c == null ? 0 : benefit.FAMILY_OOP_MET__c;
            Integer totalQty = receiverQty + transmitterQty + sensorQty;
            //Calculate the patient coverage
            Decimal patientCoverage = coverage == null ? 0 : (100 - coverage);
            patientCoverage = coverage == 0 ? 0 : patientCoverage;
            
            if(accountType == 'Non-Medicare')
            {
                //Calculate the Receiver, Transmitter and Sensor price
                Decimal totalReceiverPrice = receiverPrice == null ? 0 : receiverQty*receiverPrice;
                Decimal totalTransmitterPrice = transmitterPrice == null ? 0 : transmitterQty*transmitterPrice;
                Decimal totalSensorPrice = sensorPrice == null ? 0 : sensorQty*sensorPrice;
                Decimal totalPrice = (totalReceiverPrice + totalTransmitterPrice + totalSensorPrice);

                //Calculate Copay 
                estimatedCosts = copayDOS == null ? estimatedCosts : (estimatedCosts + copayDOS);
                estimatedCosts = copayLineItem == null ? estimatedCosts : estimatedCosts + (totalQty*copayLineItem);
                //Deductible calculation
                if((indDeductible != null && indDeductibleMet != null) || (famDeductible != null && famDeductibleMet != null))
                {
                    Decimal estimatedCostsViaIndDeductible = 0;
                    Decimal estimatedCostsViaFamDeductible = 0;
                    
                    //Calculate with Individual Deductible
                    if(indDeductible > 0)
                    {
                        Decimal indRemainingDeductible = (indDeductible - indDeductibleMet);
                        estimatedCostsViaIndDeductible = (indRemainingDeductible + ((totalPrice.setScale(2))-indRemainingDeductible)*(patientCoverage/100));
                    }
                    
                    //Calculate with Family Deductible
                    if(famDeductible > 0)
                    {
                        Decimal famRemainingDeductible = (famDeductible - famDeductibleMet);
                        estimatedCostsViaFamDeductible = (famRemainingDeductible + ((totalPrice.setScale(2))-famRemainingDeductible)*(patientCoverage/100));
                    }
                    
                    //Comare family deductibles and individual deductibles and chose which ever is less
                    if(famDeductible > 0 && indDeductible > 0)
                    {
                        if(estimatedCostsViaFamDeductible <= estimatedCostsViaIndDeductible)
                            estimatedCosts = estimatedCosts + estimatedCostsViaFamDeductible;
                        else
                            estimatedCosts = estimatedCosts + estimatedCostsViaIndDeductible;                    
                        
                    }else
                        if(indDeductible > 0 && famDeductible == 0)
                        {
                            estimatedCosts = estimatedCosts + estimatedCostsViaIndDeductible;
                        }else
                            if(famDeductible > 0 && indDeductible == 0)
                            {
                                estimatedCosts = estimatedCosts + estimatedCostsViaFamDeductible;
                            }else
                                if(indDeductible == 0 && famDeductible == 0)
                                {
                                    estimatedCosts = estimatedCosts + totalPrice*(patientCoverage/100);
                                }
                }
                //OOP Max calculation
                if((indOOPMax != null && indOOPMaxMet != null) || (famOOPMax != null && famOOPMaxMet != null))
                {
                    Decimal indOOPMaxRemainingEstimatedCosts = -1;
                    Decimal famOOPMaxRemainingEstimatedCosts = -1;
                    
                    if(indOOPMax > 0 && indOOPMaxMet >=0)
                    {
                        Decimal indOOPMaxRemaining = (indOOPMax - indOOPMaxMet);
                        if(estimatedCosts > indOOPMaxRemaining)
                            indOOPMaxRemainingEstimatedCosts = indOOPMaxRemaining;
                    }
                    if(famOOPMax > 0 && famOOPMaxMet >=0)
                    {
                        Decimal famOOPMaxRemaining = (famOOPMax - famOOPMaxMet);
                        if(estimatedCosts > famOOPMaxRemaining)
                            famOOPMaxRemainingEstimatedCosts = famOOPMaxRemaining;
                    }
                    //Choose the OOP Max, which ever is less
                    if(indOOPMaxRemainingEstimatedCosts > 0 && famOOPMaxRemainingEstimatedCosts > 0)
                        estimatedCosts = (famOOPMaxRemainingEstimatedCosts <= indOOPMaxRemainingEstimatedCosts) ? famOOPMaxRemainingEstimatedCosts : indOOPMaxRemainingEstimatedCosts;
                    else if(indOOPMaxRemainingEstimatedCosts > 0)
                        estimatedCosts = indOOPMaxRemainingEstimatedCosts;
                        else if(famOOPMaxRemainingEstimatedCosts > 0)
                            estimatedCosts = famOOPMaxRemainingEstimatedCosts;
                            else if(indOOPMaxRemainingEstimatedCosts == 0 || famOOPMaxRemainingEstimatedCosts == 0)
                                estimatedCosts = 0;
                }
                estimatedCosts = (totalPrice < estimatedCosts) ? totalPrice : estimatedCosts;
            }
            else if (accountType == 'Medicare')
            {
                if(indDeductible != null && indDeductibleMet != null)
                {
                    Decimal totalMedicareReceiverPrice = medicareReceiverPrice == null ? 0 : medicareReceiverPrice;
                    Decimal totalMedicareSubscriptionPrice = medicareSubscriptionPrice == null ? 0 : medicareSubscriptionPrice;
                    Decimal totalPrice = (totalMedicareReceiverPrice + totalMedicareSubscriptionPrice);//This is to include the Receiver cost and 1 time the subscription cost
                    Decimal indRemainingDeductible = (indDeductible - indDeductibleMet);
                    estimatedCosts = (indRemainingDeductible + ((totalPrice.setScale(2))-indRemainingDeductible)*(patientCoverage/100));
                    estimatedCosts = (totalPrice < estimatedCosts) ? totalPrice : estimatedCosts;
                }   
            }
            system.debug ('Estimated costs ' + estimatedCosts.setScale(2));     
            return estimatedCosts.setScale(2);
        }
        else
            return 0.00;
    }
 }