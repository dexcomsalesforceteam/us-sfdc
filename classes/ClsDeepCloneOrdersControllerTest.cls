@isTest
public class ClsDeepCloneOrdersControllerTest {
    
        private static testMethod void createRecords(){
        list <Order_Header__c> orderHeaderList;
        list <Order_Item_Detail__c> oidList = new list <Order_Item_Detail__c>();
        Account acc = new Account(Name = 'Test Acc');
        insert acc;
        Order_Header__c oh1 = new Order_Header__c(Account__c = acc.Id, Order_Shipped_Date__c = DATE.TODAY() - 20);
        orderHeaderList = new list <Order_Header__c>{oh1};
        insert orderHeaderList;
        oidList.add(new Order_Item_Detail__c(Order_Header__c = orderHeaderList[0].Id,Quantity__c = 3,Quantity_Shipped__c = -2,Item_Name__c = 'STS', Item_Number__c = 'STS'));
        oidList.add(new Order_Item_Detail__c(Order_Header__c = orderHeaderList[0].Id,Quantity__c = 3,Quantity_Shipped__c = -2,Item_Name__c = 'STT', Item_Number__c = 'STT'));
        Insert oidList;
        
        test.startTest();    
        PageReference pageRef = Page.DeepCloneOrders;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',oh1.id);
        ApexPages.StandardController sc = new ApexPages.standardController(oh1);    
        ClsDeepCloneOrdersController controller = new ClsDeepCloneOrdersController(sc);
        controller.UpdateQuantity();
        test.stopTest();
            
        }
        
    private static testMethod void createNegativeRecords(){
        list <Order_Header__c> orderHeaderList;
        list <Order_Item_Detail__c> oidList = new list <Order_Item_Detail__c>();
        Account acc = new Account(Name = 'Test Acc');
        insert acc;
        Order_Header__c oh1 = new Order_Header__c(Account__c = acc.Id, Order_Shipped_Date__c = DATE.TODAY() - 20,Order_Type__c = 'K-Code Commerical');
       orderHeaderList = new list <Order_Header__c>{oh1};
        insert orderHeaderList;
        oidList.add(new Order_Item_Detail__c(Order_Header__c = orderHeaderList[0].Id,Quantity__c = 3,Quantity_Shipped__c = -5,Item_Name__c = 'STS', Item_Number__c = 'STS'));
        oidList.add(new Order_Item_Detail__c(Order_Header__c = orderHeaderList[0].Id,Quantity__c = 3,Quantity_Shipped__c = 0,Item_Name__c = 'STT', Item_Number__c = 'STT'));
        oidList.add(new Order_Item_Detail__c(Order_Header__c = orderHeaderList[0].Id,Quantity__c = 3,Quantity_Shipped__c = 2,Item_Name__c = 'STT', Item_Number__c = 'STT'));
        
        
        Insert oidList;
        
        test.startTest();    
        PageReference pageRef = Page.DeepCloneOrders;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',oh1.id);
        ApexPages.StandardController sc = new ApexPages.standardController(oh1);    
        ClsDeepCloneOrdersController controller = new ClsDeepCloneOrdersController(sc);
        controller.UpdateQuantity();
        controller.getOrderTypeList();
        controller.getreasonTypeList();
        //controller.error = TRUE;
        //controller.isError = TRUE;
        test.stopTest();   
    }
    //Added by Shikha, To get the Coverage for K-Code Commerical Order Types
     private static testMethod void createKCOdeRecords(){
        list <Order_Header__c> orderHeaderList;
        list <Order_Item_Detail__c> oidList = new list <Order_Item_Detail__c>();
        Account acc = new Account(Name = 'Test Acc');
        insert acc;
        Order_Header__c oh1 = new Order_Header__c(Account__c = acc.Id, Order_Shipped_Date__c = DATE.TODAY() - 20,Order_Type__c = 'K-Code Commerical');
       orderHeaderList = new list <Order_Header__c>{oh1};
        insert orderHeaderList;
        //oidList.add(new Order_Item_Detail__c(Order_Header__c = orderHeaderList[0].Id,Quantity__c = 3,Quantity_Shipped__c = -3,Item_Name__c = 'STS', Item_Number__c = 'STS'));
        oidList.add(new Order_Item_Detail__c(Order_Header__c = orderHeaderList[0].Id,Quantity__c = 3,Quantity_Shipped__c = -3,Item_Name__c = 'STT', Item_Number__c = 'STT'));
        oidList.add(new Order_Item_Detail__c(Order_Header__c = orderHeaderList[0].Id,Quantity__c = 3,Quantity_Shipped__c = -3,Item_Name__c = 'BUN', Item_Number__c = 'BUN'));
        Insert oidList;
        
        test.startTest();    
        PageReference pageRef = Page.DeepCloneOrders;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',oh1.id);
        ApexPages.StandardController sc = new ApexPages.standardController(oh1);    
        ClsDeepCloneOrdersController controller = new ClsDeepCloneOrdersController(sc);
        controller.UpdateQuantity();
        test.stopTest();   
    }
}