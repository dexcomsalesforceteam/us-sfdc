/*
@Author        : LTI
@Date Created    : 22 Oct 2018
@Description    : Wrapper Class to help to make a callout from a button. 
*/
global class ClsG6UpgradeCalloutPayloadWrapper{
    
    global Integer Id;
    global String FirstName;
    global String LastName;
    global string PhoneNumber;
    global String Address1; 
    global String Address2; 
    global String Address3;
    global String City;
    global String ZipCode;    
    global String State;
    global Integer Quantity;
    global String Status;
    global Date DateCreated;
    global String Sku;
    
}