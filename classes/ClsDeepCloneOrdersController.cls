public class ClsDeepCloneOrdersController {
    
    private apexPages.StandardController controller {get;set;}
    private List <Order_Header__c> orderHeaderList {get;set;}
    private ID orderHeaderID {get;set;}
    private Order_Header__c orderHeader {get;set;}
    public String Name {get;set;}
    public String parentOrderType {get;set;}
    public ID ohID {get;set;}
    public List<Order_Item_Detail__c> relatedlist {get;set;}
    public String errorMessage {get;set;}
    public String enteredQuantity {get;set;}
    public String PriceListOracleId {get;set;}
    public String orderTypeSelected {get; set;}
    public String rgaReason {get;set;}
    public String reasonType {get;set;} //Added to add new field RGA reason picklist//
    public String orderType{get; set;}  
    
    public ClsDeepCloneOrdersController(ApexPages.StandardController controller){
        this.controller = controller;
        orderHeaderID = ApexPages.currentpage().getParameters().get('Id');
        orderHeader = (Order_Header__c)controller.getRecord();
        
        //Query Order Header and Line items 
        orderHeader = [SELECT Name,Price_List__c,Price_List_Oracle_ID__c,Order_Shipped_Date__c,Calculated_Shipping_Date__c,Latest_Invoice_Date__c,Latest_Fulfillment_Date__c,Latest_Scheduled_Ship_Date__c,Packing_Instructions__c,PO_Number__c,Ship_To_Address__r.Name,Shipping_Method__c,Account_Number__c,Shipping_Instructions__c, Account__C, Order_Type__c FROM Order_Header__c where ID =:orderHeaderID  ];
        ohID = orderHeader.Id;
        Name = orderHeader.Name;
        parentOrderType = orderHeader.Order_Type__c;
        PriceListOracleId = orderHeader.Price_List_Oracle_ID__c;  
        relatedlist = [SELECT Generation__c,Delivery_Status__c,Parent_Line_ID__c,Line_ID__c,Quantity_Shipped__c,Name,Shipping_Date__c,Item_Name__c,Item_Number__c,Order_Header__c, Quantity__c FROM Order_Item_Detail__c WHERE Order_Header__c =: ohID AND (NOT Item_Number__c LIKE '%INSURANCE%')];
        //Add default to the Bundle SKU if exists for K-Code Commercial order type
        if(parentOrderType == 'K-Code Commerical')
        {
            for(Order_Item_Detail__c oid: relatedlist)
            {
                If(oid.Item_Number__c.contains('BUN'))
                {
                    oid.Quantity_Shipped__c = -1;
                }
            }
        }
        
    }
    
    //Record possible order types to display
    public List<SelectOption> getOrderTypeList()
    {
        
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Return & Credit Order','Return & Credit Order'));
        if(parentOrderType != 'K-Code Commerical')
            options.add(new SelectOption('Return Only Order','Return Only Order'));
        return options;
        
        
    }
    
//Start of code to display RGA Reason for Dropdown Field//
    
    public List<SelectOption> getreasonTypeList(){
        
        //Create a list of select option type
        List<SelectOption> rgareasondropdown = new List<SelectOption>();
        
        //FieldDescription=SObjectName.FieldName.getDescribe()
        schema.DescribeFieldResult fd = Order_Header__c.Reason_for_RGA_Dropdown__c.getdescribe();
        
        //Get all picklist value in a list of scheme.picklistentry type
        list<schema.picklistentry> pc = fd.getpicklistvalues();
        
        //Add each value in the list of selectoption
       // rgareasondropdown.add(new selectoption('','---None---'));
        for(schema.picklistentry f: pc){
        
            rgareasondropdown.add(new selectoption(f.getlabel(),f.getvalue()));
        
        }
    
        return rgareasondropdown;
    }
    
//End of code to display Reason for RGA Dropdown Field//
    
    //Display the order product quantity accordingly
    public pageReference UpdateQuantity()
    {
        
        boolean canProceedInOrderCreation = true;
        Set<String> itemsToBeInsertedSet = new Set<String>();
        integer zeroQuantityOrderLines = 0;
        boolean boolBUNZeroCheck = false;
        boolean boolSKUNegetiveCheck = false;
        // Check to sure they are not ordering more than the ordered quantity or we have any positive qty or any null qty
        for(Order_Item_Detail__c oid: relatedlist)   
        {
            oid.Delivery_Status__c = null; //clear if there are any other delivery status recorded already
            Integer oldQuantity =  oid.Quantity__c.intValue(); 
            Integer newQuantity =  oid.Quantity_Shipped__c.intValue();
            if(parentOrderType == 'K-Code Commercial'){
                if(oid.Item_Number__c.contains('BUN') && newQuantity == 0){
                    boolBUNZeroCheck = true;
                }
                if(!oid.Item_Number__c.contains('BUN') && newQuantity < 0){
                    boolSKUNegetiveCheck = true;
                }
                if(boolBUNZeroCheck && boolSKUNegetiveCheck){
                    oid.Delivery_Status__c = 'You Must RGA The BUN'; 
                    canProceedInOrderCreation = false;
                }
            }
            
            //Added By Shikha, CRMSF -5180
            if(parentOrderType == 'Standard Sales Order' && oid.Item_Number__c.contains('BUN') && newQuantity < 0){
                oid.Delivery_Status__c = 'You Cannot Enter a Negetive Quantity on BUN For Standard Sales Orders'; 
                canProceedInOrderCreation = false;
            }
            if(newQuantity > 0)
            {
                oid.Delivery_Status__c = 'Only negative quantity allowed in Reduction Order.'; 
                canProceedInOrderCreation = false;
            }
            if(newQuantity*-1 > oldQuantity)
            {
                oid.Delivery_Status__c = 'You cannot enter a bigger number than originally Ordered Quantity.';
                canProceedInOrderCreation = false;
            }               
            if(newQuantity == null || newQuantity == 0)
            {
                zeroQuantityOrderLines++;
                if(relatedlist.Size() == zeroQuantityOrderLines){
                    oid.Delivery_Status__c = 'Quantity cannot be Blank/Zero';
                    canProceedInOrderCreation = false;
                }
            }
        }
        
        //Check to see if relevant Bundle SKU combination exist. This is required for K-Code Commercial order type only.
        if(canProceedInOrderCreation == true && parentOrderType == 'K-Code Commerical')
        {
            //Prepare unique set of items that are to be inserted
            for(Order_Item_Detail__c oid: relatedlist)
            {
                if(oid.Quantity_Shipped__c < 0)
                    itemsToBeInsertedSet.add(oid.Item_Number__c.SubString(0,3));
            }
            //Check if STS exist then BUN should be in there
            if(itemsToBeInsertedSet.contains('STS') && !itemsToBeInsertedSet.contains('BUN'))
            {
                canProceedInOrderCreation = false;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Bundle should be returned as Sensor is included in the return'));
                return null;
            }
            //Check if STT exist then BUN should be in there
            if(itemsToBeInsertedSet.contains('STT') && !itemsToBeInsertedSet.contains('BUN'))
            {
                canProceedInOrderCreation = false;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Bundle should be returned as Transmitter is included in the return'));
                return null;
            }
            //Check if BUN exist and either STS or STT exist
            if(itemsToBeInsertedSet.contains('BUN') && (!itemsToBeInsertedSet.contains('STS') && !itemsToBeInsertedSet.contains('STT')))
            {
                canProceedInOrderCreation = false;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'This return must contain a transmitter or sensor due to the Bundle being returned'));
                return null;
            }
        }
        
        //Confirm if we can proceed
        if(canProceedInOrderCreation == true)
        {
            //Insert Order Header
            Order_Header__c newOh = new Order_Header__c();
            List<Order_Item_Detail__c> newOrderItemDetails = new List<Order_Item_Detail__c>();
            newOh = orderheader.clone(false);
            newOh.Order_Type__c = orderType ;
            newOh.Parent_Order__c = orderHeaderID;
            newOh.Price_List__c = PriceListOracleId; 
            newOh.Reason_For_RGA__c = rgaReason;
            newOh.Reason_for_RGA_Dropdown__c = reasonType; //Added to add new field RGA reason dropdown//
            
            Boolean approvalRequired = false;
            //Check to see if Sensors are included in the return then automatically route it for Approval
            for(Order_Item_Detail__c oid: relatedlist)
            {
                if(oid.Item_Number__c.contains('STS') && oid.Quantity_Shipped__c < 0)
                {
                    system.debug('---Found Sensors, so marked the approvalRequired as True');
                    approvalRequired = true;
                }
                
            }
            
            //Insert Order lines
            for(Order_Item_Detail__c oid: relatedlist)
            {
                system.debug('---Entering process of inserting the line items');
                If(oid.Item_Number__c.contains('BUN') || oid.Item_Number__c.contains('STS') || oid.Item_Number__c.contains('STT') || oid.Item_Number__c.contains('STK') || oid.Item_Number__c.contains('STR'))
                {
                    Order_Item_Detail__c oidToInsert = new Order_Item_Detail__c();
                    oidToInsert.Shipping_Date__c = oid.Shipping_Date__c;
                    oidToInsert.Item_Name__c = oid.Item_Name__c;
                    oidToInsert.Item_Number__c = oid.Item_Number__c;
                    oidToInsert.Parent_Line_ID__c = oid.Line_ID__c;
                    //Insert only negative quantity items
                    if(oid.Quantity_Shipped__c < 0)
                    {
                        system.debug('---Item Number is ' + oid.Item_Number__c);
                        system.debug('---Qty Shipped is ' + oid.Quantity_Shipped__c);
                        
                        oidToInsert.Quantity__c = oid.Quantity_Shipped__c;
                        newOrderItemDetails.add(oidToInsert);
                        
                        //If sensor is included then set the status as Approval Required
                        if(oid.Item_Number__c.contains('STS'))
                        {
                            system.debug('---Setting the approval status for Sensors');
                            newOh.RGA_Approval_Status__c = 'Sensor Approval Required';
                        }
                        
                        //If sensor is not included in the list find out the other approvals
                        if(!approvalRequired)
                        {
                            system.debug('---Found no sensor approval, so coming to check for Hardware approval');
                            if((oid.Item_Number__c.contains('STT') || oid.Item_Number__c.contains('STK') ||  oid.Item_Number__c.contains('STR') ) && !approvalRequired)
                            {
                                system.debug('STT and STS explored'); 
                                Date calcShipDate = (newOh.Latest_Invoice_Date__c != null ? newOh.Latest_Invoice_Date__c : (newOh.Latest_Fulfillment_Date__c != null ? newOh.Latest_Fulfillment_Date__c : (newOh.Latest_Scheduled_Ship_Date__c != null ? newOh.Latest_Scheduled_Ship_Date__c : null)));
                                if(calcShipDate != null && calcShipDate  > Date.today() - 30)
                                {
                                    system.debug('---Within 30 days, so no approval required');
                                    newOh.RGA_Return_Reason__c = '30-Day Guarantee';
                                    newOh.RGA_Approval_Status__c = 'No Approval Required';
                                }
                                else
                                {
                                    system.debug('--->30 days, so approval required');
                                    newOh.RGA_Approval_Status__c = 'Hardware Approval Required';
                                    
                                }
                                
                            }
                        }
                        
                    }
                    
                }
                
            }
            //DML Operation to insert orer lines
            try{
                insert newOh;
                //Populate the Order reference for each line items and then insert them
                for(Order_Item_Detail__c oi : newOrderItemDetails)
                    oi.order_header__c = newOh.Id;
                insert newOrderItemDetails;
            }            
            catch(DMLException ex){
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, ex.getDMLMessage(0));
                ApexPages.addMessage(msg);
                return null;
            }
            PageReference detailPage = new ApexPages.StandardController(newOh).view(); 
            detailPage.setRedirect(true);
            detailPage.getParameters().put('id',newOh.Id);
            return detailPage;
        }
        else
        {
            return null;
        }
    }
}