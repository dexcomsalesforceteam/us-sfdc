/****************************************************************************
* Author       : Sudheer Vemula
* Date         : March 12 2019
* Description  : Retrieves the  Order status Data 
/****************************************************************************/
public class CtrlDisplayOrdersStatus {
    
    //Method will retrieve Order status for a given orderNumber
    @auraEnabled
    public static ClsOrderStatusWrapper getOrderStatus(string orderNumber){
        //Initialize variables
        ClsOrderStatusWrapper orderStatuswrap=new ClsOrderStatusWrapper();
        ClsOrderStatusWrapper orderStatusWrapData=new ClsOrderStatusWrapper();
        Set<String> skipInvoiceNumberList = new Set<String>();
        //Make up the Callout URL
        String calloutURL = 'callout:OrderStatusAPI';
        
        if(string.isNotBlank(orderNumber)){
            //Http invocation to call the Order Service
            Http http = new Http();
            HttpRequest httpRequest = new HttpRequest();
            httpRequest.setEndpoint(calloutURL+'/'+orderNumber);
            httpRequest.setMethod('GET');
            
            HttpResponse httpResponse = http.send(httpRequest);
            
            // If the httpRequest is successful, parse the JSON httpResponse.
            if (httpResponse.getStatusCode() == 200) {
                
                System.debug(string.valueof(httpResponse.getBody()));
                
                orderStatuswrap = (ClsOrderStatusWrapper)System.JSON.deserialize(httpResponse.getBody(), ClsOrderStatusWrapper.class);
                Map<string,string> checkForInvoiceDupMap=new Map<string,string>();
                Map<string,string> checkForOrderHoldDupMap=new Map<string,string>();
                list<ClsOrderStatusWrapper.OrderStatus> OrderStatusList=new list<ClsOrderStatusWrapper.OrderStatus>();    
                
                for(ClsOrderStatusWrapper.OrderStatus thisResponseWrapper: orderStatuswrap.OrderStatus){ 
                    ClsOrderStatusWrapper.OrderStatus orderStatusData=new ClsOrderStatusWrapper.OrderStatus();
                    string invoiceCheck=thisResponseWrapper.Invoice_Number+':'+thisResponseWrapper.Invoice_date+':'+thisResponseWrapper.Amount+':'+thisResponseWrapper.Balance+':'+thisResponseWrapper.Payment_required+':'+thisResponseWrapper.CC_Approval_Code;
                    string orderHoldCheck= thisResponseWrapper.Hold_name+':'+thisResponseWrapper.Hold_Release_Flag+':'+thisResponseWrapper.Hold_Applied_date+':'+thisResponseWrapper.Hold_Applied_by+':'+thisResponseWrapper.Hold_Release_Comment+':'+thisResponseWrapper.Hold_Released_by+':'+thisResponseWrapper.Hold_Release_date;
                    
                    if(checkForInvoiceDupMap.get(invoiceCheck) != invoiceCheck ||checkForOrderHoldDupMap.get(orderHoldCheck) !=orderHoldCheck){
                        
                        if(checkForInvoiceDupMap.get(invoiceCheck) != invoiceCheck){
                            
                            orderStatusData.Invoice_Number = thisResponseWrapper.Invoice_Number; 
                            orderStatusData.Invoice_date = thisResponseWrapper.Invoice_date;
                            orderStatusData.Amount = thisResponseWrapper.Amount;
                            orderStatusData.Balance = thisResponseWrapper.Balance;
                            orderStatusData.Payment_required = thisResponseWrapper.Payment_required;
                            orderStatusData.CC_Approval_Code = thisResponseWrapper.CC_Approval_Code;         
                            checkForInvoiceDupMap.put(invoiceCheck,invoiceCheck);
                        }
                        If(checkForOrderHoldDupMap.get(orderHoldCheck) !=orderHoldCheck){
                            orderStatusData.status = thisResponseWrapper.status;
                            orderStatusData.Hold_name = thisResponseWrapper.Hold_name;
                            orderStatusData.Hold_Release_Flag = thisResponseWrapper.Hold_Release_Flag;
                            orderStatusData.Hold_Applied_date = thisResponseWrapper.Hold_Applied_date;
                            orderStatusData.Hold_Applied_by = thisResponseWrapper.Hold_Applied_by;
                            orderStatusData.Hold_Release_Comment = thisResponseWrapper.Hold_Release_Comment; 
                            orderStatusData.Hold_Released_by = thisResponseWrapper.Hold_Released_by;
                            orderStatusData.Hold_Release_date = thisResponseWrapper.Hold_Release_date; 
                            checkForOrderHoldDupMap.put(orderHoldCheck,orderHoldCheck);
                        }
                        OrderStatusList.add(orderStatusData);
                    }
                }				
                
                orderStatusWrapData.Order_Number = orderStatuswrap.Order_Number;
                orderStatusWrapData.OrderlineStatus = orderStatuswrap.OrderlineStatus;
                orderStatusWrapData.OrderStatus =OrderStatusList;                
                
                System.debug('orderStatuswrap :: '+orderStatusWrapData);  
            }
        }
        return orderStatusWrapData; 
    }
}