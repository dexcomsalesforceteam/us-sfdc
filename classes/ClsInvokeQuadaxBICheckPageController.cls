/******************************************************************************************
Description : Apex class controller for InvokeQuadaxBICheck VF Page
*******************************************************************************************/
public class ClsInvokeQuadaxBICheckPageController {
    
    public final Benefits__c benefit;
    public string apiInvocationStatus{get;set;}
    
    public ClsInvokeQuadaxBICheckPageController(ApexPages.StandardController stdController) {
        this.benefit = (Benefits__c)stdController.getRecord();
    }
    
    public void invokeQuadaxAPICall(){
        ClsRequestBICheck.invokeQuadaxAPI(benefit.Id, 'Manually');
        apiInvocationStatus ='<script> reloadData(); </script>';
    }
    
}