/*
 * This test class is created to cover BclsOrderAutoQCBatch
 * @Author - LTI
*/
@isTest(seeAllData = false)
public class BclsOrderAutoQCBatchTest {

    @testSetup
    public static void testSetUp(){
        List<Account> accountInsertList = new List<Account>();
        //prescriber
        Account prescriberAccount=new Account();
        prescriberAccount.lastname='prescriber Account';
        prescriberAccount.recordtypeId= ClsApexUtility.getRecordTypeIdByDeveloperName('Account', 'Prescriber');
        prescriberAccount.Inactive__c =false;        
        accountInsertList.add(prescriberAccount);
        
        //Payor
        Account payorAccount = new Account();
        payorAccount.recordtypeId= ClsApexUtility.getRecordTypeIdByDeveloperName('Account', 'Payor');
        payorAccount.Name = 'Payor';
        payorAccount.Skip_Payor_Matrix__c = true;
        accountInsertList.add(payorAccount);
        Database.insert(accountInsertList);              
        
        //patient
        Account testAccount=TestDataBuilder.testAccount();
        testAccount.Default_Price_Book__c=Test.getStandardPricebookId();
        testAccount.PersonEmail = 'Test@gmail.comQCHoldServices';
        testAccount.BillingStreet='abc';
        testAccount.BillingCity ='Bang';
        testAccount.BillingState ='KN';
        testAccount.BillingPostalCode='560037';
        testAccount.ShippingCountry ='US';
        testAccount.ShippingStreet  ='Bang';
        testAccount.ShippingCity='Bang';
        testAccount.ShippingState='KN';
        testAccount.ShippingPostalCode='560037';
        testAccount.Territory_Code__c = 'US1234';
        testAccount.Party_ID__c ='123344';
        testAccount.BillingCountry ='US';
        testAccount.Prescribers__c = prescriberAccount.Id;
        testAccount.Payor__c = payorAccount.Id;
        testAccount.RecordTypeId = ClsApexUtility.getRecordTypeIdByDeveloperName('Account', 'Consumers');
        insert testAccount;
        
        //Primary_Benefit__c
        Benefits__c benefitObj = new Benefits__c();
        benefitObj.PRIOR_AUTH_REQUIRED__c = 'Y';
        benefitObj.New_Order_Auth_Products__c = 'Receiver;Transmitter;Sensor';
        benefitObj.Reorder_Auth_Products__c = 'Receiver;Transmitter;Sensor';
        benefitObj.Account__c = testAccount.Id;
        benefitObj.Payor__c = payorAccount.Id;
        //benefitObj.Benefit_Hierarchy__c = 'Primary';
        insert benefitObj;
        
        List<Address__c> addrList = new List<Address__c>();
        Address__c addrs1 = TestDataBuilder.getAddressList(testAccount.Id, true,'SHIP_TO', 1)[0];
        addrs1.Oracle_Address_ID__c ='123455';
        addrs1.Address_Verified__c = 'Yes';
        addrList.add(addrs1);
        Address__c addrs2 = TestDataBuilder.getAddressList(testAccount.Id, true,'BILL_TO', 1)[0];
        addrs2.Oracle_Address_ID__c ='123465';
        addrs2.Address_Verified__c = 'Yes';
        addrList.add(addrs2);
        Database.insert(addrList);
        Test.startTest();
        testAccount.Primary_Benefit__c = benefitObj.Id;
        testAccount.Primary_Bill_To_Address__c = addrs2.Id;
        testAccount.Primary_Ship_To_Address__c = addrs1.Id;
        update testAccount;
        
        Order orderObj = new Order();
        orderObj.AccountId = testAccount.Id;
        orderObj.Type = 'Standard Sales Order';
        orderObj.Shipping_Address__c = addrs1.Id;
        orderObj.Status = 'Draft';
        orderObj.EffectiveDate = system.today()-10;
        orderObj.Scheduled_Ship_Date__c = system.today()+1;
        orderObj.Price_Book__c = Test.getStandardPricebookId();
        orderObj.Pricebook2Id = Test.getStandardPricebookId();
        orderObj.Sub_Type__c = 'Future Order';
        insert orderObj;
        Test.stopTest();
    }
    
    @isTest
    public static void getOrderTypeTest_Positive(){
        Order orderRec = [Select Id,AccountId from Order where Account.PersonEmail = 'Test@gmail.comQCHoldServices' and Type = 'Standard Sales Order' limit 1];
        Test.startTest();
        try{
            BclsOrderAutoQCBatch baq = new BclsOrderAutoQCBatch();
            DataBase.executeBatch(baq);
        }catch(Exception e){
            
        }
        
        Test.stopTest();
    }
    
    @isTest
    public static void getOrderTypeTest_Positive1(){
        Order orderRec = [Select Id,AccountId from Order where Account.PersonEmail = 'Test@gmail.comQCHoldServices' and Type = 'Standard Sales Order' limit 1];
        Test.startTest();
        
        orderRec.Is_Benefit_Check_Completed__c = true;
        orderRec.Is_Payor_Check_Completed__c = true;
        orderRec.Is_Document_Check_Completed__c = true ;
        orderRec.Is_Order_Check_Completed__c = true;
        update orderRec;
        
        Task T = new Task();
        T.WhatId = orderRec.ID;
        //T.ownerid = orderRec.Id;
        T.Status = 'Open';
        T.type =ClsApexConstants.AUTOQC; 
        insert T;
        try{
            BclsOrderAutoQCBatch baq1 = new BclsOrderAutoQCBatch();
            DataBase.executeBatch(baq1);
        }catch(Exception e){
            
        }
        
        
        Test.stopTest();
    }
}