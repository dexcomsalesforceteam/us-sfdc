/* V1 */
public without sharing class AddProductCon {
    @AuraEnabled
    public static Organization_Product__c getProduct(Id productId) {
        return [SELECT Id, Name, Organization__c, Organization__r.Name, Product_Stage__c, Product_Type__c FROM Organization_Product__c Where Id=:productId];        
       
    }
    
    @AuraEnabled
    public static Organization__c getOrg(Id orgId) {
        return [SELECT Id, Name, Type__c, Organization_Type__c FROM Organization__c Where Id=:orgId];        
       
    }
    
    @AuraEnabled
    public static Organization_Data__c addProductNote(Organization__c org, Organization_Product__c prod, string productNote, List<Id> files, string noteTitle, string usersToFollow) {
       Organization_Data__c orgData= new Organization_Data__c();
       Id noteRecTypeId = Schema.SObjectType.Organization_Data__c.getRecordTypeInfosByDeveloperName().get('Product_Note').getRecordTypeId();
       
       if(prod != null){
           orgData.Product__c=prod.Id;
           orgData.Note_Organization__c=prod.Organization__c;
           orgData.Organization__c=prod.Organization__c;
           System.Debug('**** prod=' + prod);
       }
       
       if(org != null){           
           orgData.Note_Organization__c=org.Id;
           orgData.Organization__c=org.Id;
       }
      
       if(productNote != null){
           orgData.RecordTypeId=noteRecTypeId;
           orgData.Product_Note__c=productNote;
           orgData.Name=noteTitle.Abbreviate(50);
           orgData.Product_Note_Abbr__c=productNote.Abbreviate(100);
       }
       
       System.Debug('**** productNote=' + productNote);
        
       insert orgData;
       System.Debug('**** Inserted orgData=' + orgData);
       // Move files to organization record.
       System.Debug('**** Inserted files=' + files);
       
       if(files.size() >0){
           List<ContentDocumentLink> lstCDLIns= new List<ContentDocumentLink>();       
           for(ContentDocumentLink cdl : [Select id, ContentDocumentId, LinkedEntityId, ShareType, Visibility 
                           from ContentDocumentLink Where ContentDocumentId IN: files]){
               ContentDocumentLink cdlNew= cdl.Clone();
               cdlNew.LinkedEntityId=orgData.Id;
               lstCDLIns.Add(cdlNew);       
           }
           insert lstCDLIns;         
       }
       
       if(!String.isEmpty(usersToFollow)){
           System.Debug('**** usersToFollow =' + usersToFollow);
           List<String> lstUsers=usersToFollow.split(';');
           System.Debug('**** lstUsers=' + lstUsers);
           
           List<EntitySubscription> lstES= new List<EntitySubscription>();
           for(string uid : lstUsers){
               lstES.Add(new EntitySubscription(SubscriberId = uid, ParentId = orgData.Id));             
           }
           System.Debug('**** lstES=' + lstES);
           insert lstES;
       }
       return orgData;
    }

}