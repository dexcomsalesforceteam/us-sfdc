@istest
public class MDCRFollowupProductsTriggerTest {
  @istest
 public static void testSetUp(){
         Id consumerRecTypeId = ClsApexUtility.getRecordTypeIdByDeveloperName(ClsApexConstants.ACCOUNT_API_NAME, ClsApexConstants.ACCOUNT_REC_TYPE_CONSUMERS_LABEL);
        Account testAccount=TestDataBuilder.testAccount();
        testAccount.Default_Price_Book__c=Test.getStandardPricebookId();
        testAccount.PersonEmail = 'Test@gmail.com1241';
        testAccount.BillingStreet='abc';
        testAccount.BillingCity ='Bang';
        testAccount.BillingState ='KN';
        testAccount.BillingPostalCode='56003';
        testAccount.ShippingStreet  ='Bang';
        testAccount.ShippingCity='Bang';
        testAccount.ShippingState='KN';
        testAccount.ShippingPostalCode='56007';
        testAccount.Party_ID__c ='123344';
        testAccount.Receiver_Quantity_Prescribed__c = 20;
        testAccount.Sensor_Quantity_Prescribed__c = 20;
        testAccount.Transmitter_Quantity_Prescribed__c = 20;
        Database.insert(testAccount);
        
        
        List<Product2> prodList = new List<Product2>();
        Product2 productSku = new Product2();
        productSku.Name = ClsApexConstants.MT25056_SKU;
        productSku.Generation__c = 'G6';
        productSku.Oracle_Product_Id__c = '216946';
        productSku.IsActive = ClsApexConstants.BOOLEAN_TRUE;
        productSku.ProductCode = ClsApexConstants.MT25056_SKU;
        prodList.add(productSku);
        Product2 productSku2 = new Product2();
        productSku2.Name = ClsApexConstants.G6_MDCR_TOUCHSCREEN_KIT;
        productSku2.Generation__c = 'G6';
        productSku2.Oracle_Product_Id__c = '216946';
        productSku2.IsActive = ClsApexConstants.BOOLEAN_TRUE;
        productSku2.ProductCode = ClsApexConstants.G6_MDCR_TOUCHSCREEN_KIT;
        prodList.add(productSku2);
        database.insert(prodList);
        
        List<PricebookEntry> pricebookEntryList = new List<PricebookEntry>();
        PricebookEntry pricebookEntryRec = new PricebookEntry();
        pricebookEntryRec.Product2Id = productSku.Id;
        pricebookEntryRec.Pricebook2Id = Test.getStandardPricebookId();
        pricebookEntryRec.UnitPrice = 10;
        pricebookEntryRec.IsActive = ClsApexConstants.BOOLEAN_TRUE;
        pricebookEntryList.add(pricebookEntryRec);
        
        PricebookEntry pricebookEntryRec2 = new PricebookEntry();
        pricebookEntryRec2.Product2Id = productSku2.Id;
        pricebookEntryRec2.Pricebook2Id = Test.getStandardPricebookId();
        pricebookEntryRec2.UnitPrice = 10;
        pricebookEntryRec2.IsActive = ClsApexConstants.BOOLEAN_TRUE;
        pricebookEntryList.add(pricebookEntryRec2);
        database.insert(pricebookEntryList);
      Id usReorderG6RecType = ClsApexUtility.getRecordTypeIdByDeveloperName(ClsApexConstants.MDCR_FOLLOWUP_OBJECT_API, ClsApexConstants.MFR_US_REORDER_G6_REC_TYPE);
        Account accRec = [Select Id, Latest_Receiver_Generation_Shipped__c, Num_Of_Days_Left_For_Transmitter_Order__c, Latest_Transmitter_Generation_Shipped__c from Account WHERE Party_id__c = '123344' limit 1];
        //created MDCR Follow Up Record 
        MDCR_Followup__c mdcrFollowUpRec = new MDCR_Followup__c();
        mdcrFollowUpRec.Customer__c = accRec.Id;
        mdcrFollowUpRec.Communication_Preference__c = 'Email';
        mdcrFollowUpRec.MDCR_Price_Book__c = Test.getStandardPricebookId();
        mdcrFollowUpRec.Shipping_Method__c = '000001_FEDEX_A_3DS';
        mdcrFollowUpRec.MDCR_Followup_Status__c = 'Open';
        mdcrFollowUpRec.RecordTypeId = usReorderG6RecType;
        Test.startTest();
        Database.insert(mdcrFollowUpRec);
        Test.stopTest();
     
     MDCR_Followup_Products__c mfp = new MDCR_Followup_Products__c();
     mfp.MDCR_Followup__c = mdcrFollowUpRec.id;
     mfp.Product__c = prodList[0].id;
     mfp.Order_Qty__c = 2;
     insert mfp;
     ClsMDCRFollowupProductsTriggerHandler.mfrProductsToProcess.add(mfp);
     ClsMDCRFollowupProductsTriggerHandler.accountIdsToProcess.add(mdcrFollowUpRec.Customer__c);
     ClsMDCRFollowupProductsTriggerHandler.ProcessG6ProductLimits();
 }
    
}