/*
  @Author        : Anuj Patel
  @Date Created    : 30-April-2018
  @Description    : Automatically creates orders for G6 Products when Update Patient is selcted as a G6 program via a Batch class.
*/

global with sharing class BclsG6OrderHeaderCreationBatch implements Database.Batchable<sObject> {
    
	//G6 Program to be looked for processing
	String Picklist = 'Upgrade Patient (Transmitter and Sensors Only)';
	
	//Start Method
    global Database.QueryLocator start(Database.BatchableContext BC)
	{
		Date fourDaysFromNow = Date.Today().addDays(4) ;
        String query = 'SELECT ID,Primary_Ship_To_Address__c,Latest_Sensors_Ship_Date__c,Num_Of_Days_Left_For_Sensors_Order__c,G6_Upgrade_Initial_Ship_Date__c,Default_Price_Book__c,Firstname FROM ACCOUNT WHERE G6_Program__c =: Picklist AND G6_Orders_Generated__c = FALSE AND Primary_Ship_To_Address__c != null  AND G6_Upgrade_Est_Followup_Ship_Date__c	=: fourDaysFromNow';
		system.debug('Query' +query);
        return Database.getQueryLocator(query);
    }
    
	//Execute Method	
	global void execute(Database.BatchableContext BC, List<Account> accountList) 
	{
		system.debug('Starting to Batch process Accounts' +accountList);

		//Initializing variables to be used
		List<Order> ordersToInsertList = new List<order>();//List to hold order entries to be created
		List<OrderItem> orderLinesToInsertList = new List<orderItem>() ;//List holds order lines to be created 

		List<Order_Header__c> orderHeadersToInsertList = new List<Order_Header__c>();//List holds order header entries to be created
		List<Order_Item_Detail__c> orderHeaderLinesToInsertList = new List<Order_Item_Detail__c>();//List holds order header lines entries to be created
		
		Map<Id, Account> accntToBeUpdatedMap = new Map<Id, Account>();//Map to hold Accounts to be updatd with G6_Orders_Generated__c = true

		//Get the record type id for "US Sales Orders"
		Id orderRecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('US Sales Orders').getRecordTypeId(); 

		//Get the Pricebookentry for Product "STT-OR-001" from Pricebook Get "G6 Upgrade Program PriceList"
		PricebookEntry g6TransmitterPBE = [SELECT ID, Pricebook2Id, UnitPrice, Pricebook2.Oracle_Id__c from PricebookEntry where Product2.Name = 'STT-OR-001' and Pricebook2.Name = 'G6 Auto Upgrade Program'];

		//Map between the PricebookId and the Pricebookentry for the G6 Sensor SKU
		Map<Id, PricebookEntry> g6SensorPBEMap = new Map<Id, PricebookEntry>();
		//Map between the Account Id and the Pricebookentry for the G6 Sensor SKU
		Map<Id, PricebookEntry> accountIdToG6SensorPBEMap = new Map<Id, PricebookEntry>();

		//Get the Sensor "STS-OR-003" Pricebookentry for each Pricebook and create a map
		for(PricebookEntry pbe : [SELECT Id, Pricebook2Id, UnitPrice, Pricebook2.Oracle_Id__c FROM PricebookEntry WHERE Product2.Name = 'STS-OR-003'])
		{
			g6SensorPBEMap.put(pbe.Pricebook2Id, pbe);
		}
		
		//Process each account
		for(Account acc : accountList)
		{
		 
            if(acc.Default_Price_Book__c != null)
		   {
			   if(g6SensorPBEMap.get(acc.Default_Price_Book__c) != null)
			   {
				   system.debug('**********************************Starting to create Orders for  ' +acc.Id);
				   system.debug('**********************************Pricebook Id is ' + acc.Default_Price_Book__c);
				   system.debug('**********************************Transmitter Pricebook Id is ' + g6TransmitterPBE.Pricebook2Id);
				   system.debug('**********************************Sensor Pricebook Id is ' + g6SensorPBEMap.get(acc.Default_Price_Book__c).Pricebook2Id);
				   
				   //Prepare Order entries for Transmitter Order and Sensor Order
				   Date currentDate = Date.Today();
                   Date scheduledDate = currentDate.adddays(4);
				   Order transmitterOrder = new order();
				   transmitterOrder.AccountId = acc.Id;
				   transmitterOrder.Type = 'Standard Sales Order';
				   transmitterOrder.RecordTypeId = orderRecordTypeId;
				   transmitterOrder.Status = 'Draft';
				   transmitterOrder.Sub_Type__c = 'G6 Upgrade Orders';
				   transmitterOrder.EffectiveDate = currentDate;
				   transmitterOrder.Scheduled_Ship_Date__c = scheduledDate;
				   transmitterOrder.Shipping_Address__c = acc.Primary_Ship_To_Address__c;
				   transmitterOrder.Shipping_Method__c = '000001_FEDEX_A_3DS';
				   transmitterOrder.Price_Book__c = g6TransmitterPBE.Pricebook2Id;
				   ordersToInsertList.add(transmitterOrder);
				   
				   Order sensorOrder = new order();
				   sensorOrder.AccountId = acc.Id;
				   sensorOrder.Type = 'Standard Sales Order';
				   sensorOrder.RecordTypeId = orderRecordTypeId;
				   sensorOrder.Status = 'Draft';
				   sensorOrder.Sub_Type__c = 'G6 Upgrade Orders';
				   sensorOrder.EffectiveDate = currentDate;
				   sensorOrder.Scheduled_Ship_Date__c = scheduledDate;
				   sensorOrder.Shipping_Address__c = acc.Primary_Ship_To_Address__c;
				   sensorOrder.Shipping_Method__c = '000001_FEDEX_A_3DS';
				   sensorOrder.Price_Book__c = acc.Default_Price_Book__c;
				   ordersToInsertList.add(sensorOrder);
			   } 
		   } 
        
		}
		
		//If there are Orders to process then proceed
        if(!ordersToInsertList.isEmpty())
        {
            //Insert Out of Box Orders
			try{Database.Insert(ordersToInsertList,false);}
            catch (DmlException de) {
                Integer numErrors = de.getNumDml();
                System.debug('getNumDml=' + numErrors);
                for(Integer i=0;i<numErrors;i++) {
                    System.debug('getDmlFieldNames=' + de.getDmlFieldNames(i));
                    System.debug('getDmlMessage=' + de.getDmlMessage(i));
                }
            }
			//Create Custom Order Headers and Order Lines for Inserted Orders	
			for(Order oobOrder : ordersToInsertList)
			{
			   //If the Order was successfully inserted then go ahead and insert the line items	
			   if(oobOrder.Id != null)
			   {				   
				   //If Transmitter Order line or Sensor order line
				   if(oobOrder.Price_Book__c == g6TransmitterPBE.Pricebook2Id)
				   {
						//Insert custom order header for G6 Trasmitter
						system.debug('*******************Adding G6 Transmitter Order for Account ' + oobOrder.AccountId);
						Order_Header__c g6TransmitterOrderHeader = new Order_Header__c();
						g6TransmitterOrderHeader.Account__c = oobOrder.AccountId;
						g6TransmitterOrderHeader.Order__c = oobOrder.Id;
						g6TransmitterOrderHeader.Order_Date__c = oobOrder.Scheduled_Ship_Date__c;
						g6TransmitterOrderHeader.Order_Type__c = 'Standard Sales Order';
						g6TransmitterOrderHeader.Status__c = 'Open';
						g6TransmitterOrderHeader.Shipping_Method__c = '000001_FEDEX_A_3DS';
						g6TransmitterOrderHeader.Ship_To_Address__c = oobOrder.Shipping_Address__c;
						g6TransmitterOrderHeader.Price_List__c = g6TransmitterPBE.Pricebook2.Oracle_Id__c;
						orderHeadersToInsertList.add(g6TransmitterOrderHeader);
						
						//Insert OOB Transmitter Order Line
						system.debug('*******************Adding G6 Transmitter order item for Account ' + oobOrder.AccountId);
						OrderItem g6TransmitterOrderLine = new OrderItem();
						g6TransmitterOrderLine.OrderId = oobOrder.Id;
						g6TransmitterOrderLine.PricebookEntryId = g6TransmitterPBE.Id;
						g6TransmitterOrderLine.Quantity = 1;
						g6TransmitterOrderLine.UnitPrice = g6TransmitterPBE.UnitPrice ;
						orderLinesToInsertList.add(g6TransmitterOrderLine);
				   }
				   else
				   {
						//Insert custom order header for G6 Sensor
						PricebookEntry g6SensorPBE = g6SensorPBEMap.get(oobOrder.Price_Book__c);
						system.debug('*******************Adding G6 Sensor Order for Account ' + oobOrder.AccountId);
						Order_Header__c g6SensorOrderHeader = new Order_Header__c();
						g6SensorOrderHeader.Account__c = oobOrder.AccountId;
						g6SensorOrderHeader.Order__c = oobOrder.Id;
						g6SensorOrderHeader.Order_Date__c = oobOrder.Scheduled_Ship_Date__c;
						g6SensorOrderHeader.Order_Type__c = 'Standard Sales Order';
						g6SensorOrderHeader.Status__c = 'Open';
						g6SensorOrderHeader.Shipping_Method__c = '000001_FEDEX_A_3DS';
						g6SensorOrderHeader.Ship_To_Address__c = oobOrder.Shipping_Address__c;
						g6SensorOrderHeader.Price_List__c = g6SensorPBE.Pricebook2.Oracle_Id__c;
						orderHeadersToInsertList.add(g6SensorOrderHeader);
						
						//Insert OOB Sensor Order Line
						system.debug('*******************Adding G6 Sensor order item for Account ' + oobOrder.AccountId);
						OrderItem g6SensorOrderLine = new OrderItem();
						g6SensorOrderLine.OrderId = oobOrder.Id;
						g6SensorOrderLine.PricebookEntryId = g6SensorPBE.Id;
						g6SensorOrderLine.Quantity = 3;
						g6SensorOrderLine.UnitPrice = g6SensorPBE.UnitPrice ;
						orderLinesToInsertList.add(g6SensorOrderLine); 
				   }
			   }
			}
			 
			//Insert Out of Box Order Lines
			if(!orderLinesToInsertList.isEmpty())
			{
				try{Database.Insert(orderLinesToInsertList,false);}
				catch (DmlException de) {
					Integer numErrors = de.getNumDml();
					System.debug('getNumDml=' + numErrors);
					for(Integer i=0;i<numErrors;i++) {
						System.debug('getDmlFieldNames=' + de.getDmlFieldNames(i));
						System.debug('getDmlMessage=' + de.getDmlMessage(i));
					}
				}
			}
			 //Insert custom order headers
			if(!orderHeadersToInsertList.isEmpty())
			{
				try{Database.Insert(orderHeadersToInsertList,false);}
				catch (DmlException de) {
					Integer numErrors = de.getNumDml();
					System.debug('getNumDml=' + numErrors);
					for(Integer i=0;i<numErrors;i++) {
						System.debug('getDmlFieldNames=' + de.getDmlFieldNames(i));
						System.debug('getDmlMessage=' + de.getDmlMessage(i));
					}
				}
			}

			//Create Order Header Lines for Inserted Order Headers
			for(Order_Header__c customOrder : orderHeadersToInsertList)
			{
			   //If the Order was successfully inserted then go ahead and insert the order header line items	
			   if(customOrder.Id != null)
			   {				   
				   //Update Account List 
				   if(customOrder.Oracle_Status__c != 'Error' && customOrder.Integration_Error_Message__c == null)
				   {
					   Account acc = new Account(Id = customOrder.Account__c);
					   acc.G6_Orders_Generated__c = true;
					   accntToBeUpdatedMap.put(acc.Id, acc);
				   }	   
				   
				   //If Transmitter Order line or Sensor order header line
				   if(customOrder.Price_List__c == g6TransmitterPBE.Pricebook2.Oracle_Id__c)
				   {
						//Insert Transmitter Order header Line
						system.debug('*******************Adding G6 Transmitter order header item for Account ' + customOrder.Account__c);
						Order_Item_Detail__c g6TransmitterOrderHeaderLine = new Order_Item_Detail__c();
						g6TransmitterOrderHeaderLine.Order_Header__c = customOrder.Id;
						g6TransmitterOrderHeaderLine.Item_Name__c = 'STT-OR-001';
						g6TransmitterOrderHeaderLine.Item_Number__c = 'STT-OR-001';
						g6TransmitterOrderHeaderLine.Quantity__c = 1;
						orderHeaderLinesToInsertList.add(g6TransmitterOrderHeaderLine);

				   }
				   else
				   {
						//Insert Sensor Order header Line
						system.debug('*******************Adding G6 Sensor order header item for Account ' + customOrder.Account__c);
						Order_Item_Detail__c g6SensorOrderHeaderLine = new Order_Item_Detail__c();
						g6SensorOrderHeaderLine.Order_Header__c = customOrder.Id;
						g6SensorOrderHeaderLine.Item_Name__c = 'STS-OR-003';
						g6SensorOrderHeaderLine.Item_Number__c = 'STS-OR-003';
						g6SensorOrderHeaderLine.Quantity__c = 3;
						orderHeaderLinesToInsertList.add(g6SensorOrderHeaderLine);

				   }
			   }
			}
			//Insert custom order header lines
			if(!orderHeaderLinesToInsertList.isEmpty())
			{
				try{Database.Insert(orderHeaderLinesToInsertList,false);}
				catch (DmlException de) {
					Integer numErrors = de.getNumDml();
					System.debug('getNumDml=' + numErrors);
					for(Integer i=0;i<numErrors;i++) {
						System.debug('getDmlFieldNames=' + de.getDmlFieldNames(i));
						System.debug('getDmlMessage=' + de.getDmlMessage(i));
					}
				}
			}
			
			// Activate the OOB Orders
			system.debug('*******************Activate Out of box Orders *****');
			for(Order oobOrder :ordersToInsertList )
			{  
			   if(oobOrder.Id != null)
			   {
					oobOrder.status = 'Activated';
			   }

			}
			try{Database.Update(ordersToInsertList,false);}
			catch (DmlException de) {
				Integer numErrors = de.getNumDml();
				System.debug('getNumDml=' + numErrors);
				for(Integer i=0;i<numErrors;i++) {
					System.debug('getDmlFieldNames=' + de.getDmlFieldNames(i));
					System.debug('getDmlMessage=' + de.getDmlMessage(i));
				}
			}
			
			// Send Custom Order Headers to Oracle
			system.debug('*******************Send Custom Order Headers to Oracle *****');
			for(Order_Header__c customOrder :orderHeadersToInsertList )
			{  
			   if(customOrder.Id != null)
			   {
					customOrder.Send_To_Oracle__c = true;
			   }

			}
			try{Database.Update(orderHeadersToInsertList,false);}
			catch (DmlException de) {
				Integer numErrors = de.getNumDml();
				System.debug('getNumDml=' + numErrors);
				for(Integer i=0;i<numErrors;i++) {
					System.debug('getDmlFieldNames=' + de.getDmlFieldNames(i));
					System.debug('getDmlMessage=' + de.getDmlMessage(i));
				}
			}
			
			// Update Account field G6_Orders_Generated__c
			if(!accntToBeUpdatedMap.isEmpty())
			{
				system.debug('*******************Updating Acounts with the G6_Orders_Generated__c field to true*****');
				try{Database.Update(accntToBeUpdatedMap.values(),false);}
				catch (DmlException de) {
					Integer numErrors = de.getNumDml();
					System.debug('getNumDml=' + numErrors);
					for(Integer i=0;i<numErrors;i++) {
						System.debug('getDmlFieldNames=' + de.getDmlFieldNames(i));
						System.debug('getDmlMessage=' + de.getDmlMessage(i));
					}
				}
			}		
		}	
	}	
	
    global void finish(Database.BatchableContext BC){
        
        AsyncApexJob a = [Select Id, Status,ExtendedStatus,NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email    from AsyncApexJob where Id =:BC.getJobId()];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'ap0516@dexcom.com'};
        mail.setToAddresses(toAddresses);
        mail.setSubject('G6 Upgrade Order Records for  ' + system.today().format() + ' ' + a.Status);
        mail.setPlainTextBody('G6 Upgrade Order Records were created. Success :  ' + a.TotalJobItems +   ' batches. Failure : '+ a.NumberOfErrors + ' batches.');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
        
        
    }

}