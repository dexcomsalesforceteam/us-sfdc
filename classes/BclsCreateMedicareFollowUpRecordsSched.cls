//Scheduling Class Creates Medicare Follow Up Records based
global class BclsCreateMedicareFollowUpRecordsSched implements Schedulable {
    // Execute at regular intervals
    global void execute(SchedulableContext ctx){
      BclsCreateMedicareFollowUpRecords batch = new BclsCreateMedicareFollowUpRecords();
      Database.executebatch(batch, 20);
    }
}