/********************************************************************************
@author P Saini
@date June 13, 2019
@description: Test class for AccountPrescriberUpdate
*******************************************************************************/

@istest
public class AccountPrescriberUpdateTest{    
    
    Static testmethod void PrescriberAddressBeforeUpdate_test() {        
       
        Id rtPrescriberId= Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prescriber').getRecordTypeId();
        Account accP1= new Account();
        accP1.RecordTypeId = rtPrescriberId;
        accP1.FirstName = 'PrescriberAddressBeforeUpdate';
        accP1.LastName = 'Prescriber';
        accP1.PersonEmail = 'PrescriberAddressBeforeUpdateP1@test.com';        
        insert accP1;
        
        Account accP2= new Account();
        accP2.RecordTypeId = rtPrescriberId;
        accP2.FirstName = 'PrescriberAddressBeforeUpdate';
        accP2.LastName = 'Prescriber';
        accP2.PersonEmail = 'PrescriberAddressBeforeUpdateP2@test.com';        
        insert accP2;
       
       Address__c addrP1= TestDataBuilder.getAddressList(accP1.Id, true,'SHIP_TO', 1)[0];
       insert addrP1;
       Address__c addrP2= TestDataBuilder.getAddressList(accP2.Id, true,'SHIP_TO', 1)[0];
       //insert addrP2;
       
       Id rtConsumerId= Schema.SObjectType.Account.getRecordTypeInfosByName().get('Consumers').getRecordTypeId();
       Account accC1 = new Account();
       accC1.RecordTypeId = rtConsumerId;
       accC1.FirstName = 'PrescriberAddressBeforeUpdate';
       accC1.LastName = 'Consumer';
       accC1.PersonEmail = 'PrescriberAddressBeforeUpdateC1@gmail.com';
        
       accC1.Prescribers__c=accP1.Id;
       insert accC1;
       
       accC1.Prescribers__c=accP2.Id;
       update accC1;
       
    }
    
}