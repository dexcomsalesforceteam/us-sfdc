/***
*@Author        : Priyanka Kajawe
*@Date Created  : 30-July-2018
*@Description   : Automatically creates orders for SSIP Schedules.
***/

global with sharing class BclsSSIPOrderCreationBatch implements Database.Batchable<sObject> {
    
    //Parameter
    global String batchProcessinvokedFrom;
    global String soqlQueryToProcess;
    
    //Constructor, which accepts the SSIP Ids
    global BclsSSIPOrderCreationBatch(String batchProcessinvokedFrom) {
        this.batchProcessinvokedFrom = batchProcessinvokedFrom;
    }
   
    //Start Method
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        Date last6MonthsDate = system.today() - 180;
        
        if(batchProcessinvokedFrom == 'BclsSSIPOrderCreationBatchSchedule')
            soqlQueryToProcess = 'SELECT ID, Account__c, Product__c, Address_Override__c,Shipping_Address__c, Shipping_Address_Full_Text__c, Account__r.Primary_Ship_To_Address__c, Shipping_Method__c, Waive_Shipping_Charges__c, Price_Book__c, Order_Creation_Date__c, Scheduled_Shipment_Date__c, Order__c FROM SSIP_Schedule__c WHERE Order_Creation_Date__c = TODAY and Order__c = Null and Status__c = \'Eligibility Verified\'';
        else if(batchProcessinvokedFrom == 'BclsProcessSSIPSchedules')
            soqlQueryToProcess = 'SELECT ID, Account__c, Product__c, Address_Override__c,Shipping_Address__c, Shipping_Address_Full_Text__c, Account__r.Primary_Ship_To_Address__c, Shipping_Method__c,Waive_Shipping_Charges__c, Price_Book__c, Order_Creation_Date__c, Scheduled_Shipment_Date__c, Order__c FROM SSIP_Schedule__c WHERE Expedited_Shipment_Required__c = true and Order__c = null and Status__c = \'Eligibility Verified\'';
            system.debug('SOQL Query took effect is ' + soqlQueryToProcess);
            return Database.getQueryLocator(soqlQueryToProcess);
    }
   
    //Execute Method  
    global void execute(Database.BatchableContext BC, List<SSIP_Schedule__c> scheduleList) 
    {
        system.debug('Size of the list is ' + scheduleList.size());
        if(!scheduleList.isEmpty())
        {
            //Initialize Collections
            Map<Id, Order> newOrderCreatedForScheduleMap = new Map<Id, Order>();//Map holds the newly created order for each schedule
            List<SSIP_Schedule__c> scheduleListToBeUpdated = new List<SSIP_Schedule__c>();//List of SSIP Schedules to be updated with Order information
            //List<Order> ordersToBeUpdated = new List<Order>();//List holds the list of orders to be updated with Schedule information
            
            //Get the record type id for "US Sales Orders"
            Id orderRecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('US Sales Orders').getRecordTypeId(); 
            
            System.debug('in Excute');
            //List holds the Order wrapper class that are to be created
            List<ClsOrderWrapper> orderWrapperList = new List<ClsOrderWrapper>();
            Map<Id, Order> createdOrders = new Map<Id, Order>();
            for(SSIP_Schedule__c thisSchedule : scheduleList){
                ClsOrderWrapper thisOrderWrapper = new ClsOrderWrapper();
                thisOrderWrapper.productQuantityMap = new Map<String,Integer>();
                thisOrderWrapper.uniqueIdentifier = thisSchedule.id; //this will be treated as identifier for the order
                thisOrderWrapper.accountId = thisSchedule.Account__c;
                thisOrderWrapper.type =  'Standard Sales Order';
                thisOrderWrapper.recordTypeId = orderRecordTypeId;
                thisOrderWrapper.status = 'Draft';
                thisOrderWrapper.subType = ClsApexConstants.ORDER_SUB_TYPE_SSIP_ORDER;
                thisOrderWrapper.sendErrorNotificationTo = ''; //need to update logic once user decided
                thisOrderWrapper.effectiveDate = batchProcessinvokedFrom == 'BclsSSIPOrderCreationBatchSchedule' ? thisSchedule.Scheduled_Shipment_Date__c : System.Today();//If the process is invoked from Batch class then we take the schedule ship date else we put the current date
                thisOrderWrapper.scheduledShipDate = batchProcessinvokedFrom == 'BclsSSIPOrderCreationBatchSchedule' ? thisSchedule.Scheduled_Shipment_Date__c : System.Today();//If the process is invoked from Batch class then we take the schedule ship date else we put the current date
                //If Override checked on Schedule then take address specified on schedule else take primary ship to address from account.
                thisOrderWrapper.shippingAddress = thisSchedule.Address_Override__c ? 
                    thisSchedule.Shipping_Address__c : thisSchedule.Account__r.Primary_Ship_To_Address__c;
                thisOrderWrapper.shippingMethod = thisSchedule.Shipping_Method__c;
                thisOrderWrapper.priceBook = thisSchedule.Price_Book__c;
                thisOrderWrapper.cmnVerification = true;
                thisOrderWrapper.aobVerification = true;
                thisOrderWrapper.WaiveShippingCharges= thisSchedule.Waive_Shipping_Charges__c;
                String productName = thisSchedule.Product__c.substringBetween('| ').trim();
                List<String> productSplits = thisSchedule.Product__c.split('|');
                Integer quantity = Integer.valueOf(productSplits[productSplits.size()-1].trim());
                
                //Add Product and Quantity in Map
                thisOrderWrapper.productQuantityMap.put(productName, quantity);          
                orderWrapperList.add(thisOrderWrapper);
            }       
            if(!orderWrapperList.isEmpty()){
                System.debug('list1>> '+ orderWrapperList);
                
                //Create the orders for schedules
                newOrderCreatedForScheduleMap = ClsCreateOrder.createOrder(orderWrapperList);
                
                if(!newOrderCreatedForScheduleMap.isEmpty()){
                    //----------------Link the newly created Order record with each Schedule record Start-------------------------
                    for(ClsOrderWrapper orderWrapperRecord :orderWrapperList){
                        if(newOrderCreatedForScheduleMap.containsKey(orderWrapperRecord.uniqueIdentifier)){
                            SSIP_Schedule__c scheduleRecord = new SSIP_Schedule__c(Id = orderWrapperRecord.uniqueIdentifier);
                            //Order newlyCreatedOrder = newOrderCreatedForScheduleMap.get(orderWrapperRecord.uniqueIdentifier);
                            if(newOrderCreatedForScheduleMap.containsKey(orderWrapperRecord.uniqueIdentifier))
                            scheduleRecord.order__c = newOrderCreatedForScheduleMap.get(orderWrapperRecord.uniqueIdentifier).id;//Link Order on the Schedule Record
                            //newlyCreatedOrder.SSIP_Schedule__c = orderWrapperRecord.uniqueIdentifier;//Link Schedule on the Order record
                            scheduleListToBeUpdated.add(scheduleRecord);
                            //ordersToBeUpdated.add(newlyCreatedOrder);
                        }
                    }
                    
                    //Update the Schedules with Order Information
                    if(!scheduleListToBeUpdated.isEmpty()){
                        Database.SaveResult[] saveResult = Database.update(scheduleListToBeUpdated,ClsApexConstants.BOOLEAN_FALSE);
                        for(Database.SaveResult sr : saveResult){
                            if(!sr.isSuccess()){
                                String errorMsg = sr.getErrors().get(0).getFields() + ':' + sr.getErrors().get(0).getMessage();
                                system.debug('####errorMsg - ' + errorMsg);
                                if(errorMsg.Length() >= ClsApexConstants.TWO_HUNDRED_INTEGER)
                                    errorMsg = errorMsg.substring(ClsApexConstants.ZERO_INTEGER,ClsApexConstants.TWO_HUNDRED_INTEGER);
                                //Write the error log
                                new ClsApexDebugLog().createLog(
                                    new ClsApexDebugLog.Error(
                                        'BclsSSIPOrderCreationBatch',
                                        'updateSSIPScheduleRoutine',
                                        ClsApexConstants.EMPTY_STRING,
                                        errorMsg
                                    )); 
                            }
                        }
                    }
                    //Update Orders with Schedule Information
                    /*if(!ordersToBeUpdated.isEmpty()){
                        try{
                            UtilityClass.runAfterTrigger = false;
                            Database.update(ordersToBeUpdated,false);
                        }
                        catch (DmlException de) {
                            Integer numErrors = de.getNumDml();
                            System.debug('getNumDml=' + numErrors);
                            for(Integer i=0;i<numErrors;i++) {
                                System.debug('getDmlFieldNames=' + de.getDmlFieldNames(i));
                                System.debug('getDmlMessage=' + de.getDmlMessage(i));
                            }
                        }
                    }*/
                    //----------------Link the newly created Order record with each Schedule record End-------------------------    
                }               
            }
        }       
    }

    global void finish(Database.BatchableContext BC) {
        //Send an email to the User after your batch completes
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'jagan.periyakaruppan@dexcom.com'};
        mail.setToAddresses(toAddresses);
        mail.setSubject('BclsSSIPOrderCreationBatch Apex Batch Job is complete for ' + System.Today().format() + ' run');
        mail.setPlainTextBody('The batch Apex Job Processed Successfully');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}