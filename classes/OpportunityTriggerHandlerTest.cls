/*********************************************************
* @author      : Kingsley Tumaneng
* @date        : SEPT 9 2015
* @description : Test class for OpportunityTriggerHandler
*********************************************************/
@isTest
private class OpportunityTriggerHandlerTest {
    private static Id recId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
    
    //ADDED BY Noy De Goma@CSHERPAS on 01.07.2016
    //-- START
    @testSetup static void testData(){
        User systAd = TestDataBuilder.testUser();
        system.runAs(systAd){
            Territory_Opportunity_Team_Role__c totc = new Territory_Opportunity_Team_Role__c(
                SA__c               = 'SA - Sales Administrator',
                PCS__c              = 'PCS - Patient Care Specialist',          
                Admin__c            = 'Administrative Assistant',                 
                MCS__c              = 'MCS - Manaaged Care Specialist',          
                DBM__c              = 'DBM - District Sales Manager',         
                RSD__c              = 'RSD - Regional Sales Director',        
                Territory_Rep__c    = 'Temporary TBM'
            );
            insert totc;
            
            Account acc = TestDataBuilder.getAccountList(1, recId)[0];
            acc.Territory_Code__c = '1234';
            insert acc;
            
            list <User> userList = new list <User>();
            for(Integer i = 0; i < 7; i++){
                User u = TestDataBuilder.getUser('System Administrator', 'User' + i + 'Test');
                userList.add(u);
            }
            insert userList;
            
            Territory_Alignment__c territory = TestDataBuilder.getTerritory('1234', userList);
            insert territory;
        }
    }
    
    private static testMethod void kCodeScenario2(){
        test.startTest();
        Id pb = Test.getStandardPricebookID();
        System.debug('$$standard PB >> ' + pb);
       
        //Insert test Account
        id recId = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Consumers'].Id;
        id payorRecId = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Payor'].Id;
        
        Account consumer = TestDataBuilder.getAccountListConsumer(1, recId)[0];
        consumer.Default_Price_Book__c = pb;
        consumer.Next_Possible_Date_For_Sensor_Order__c = System.today()-2;
        
        Account consumerMX = TestDataBuilder.getAccountListConsumer(1, recId)[0];
        consumerMX.Default_Price_Book__c = pb;
        consumerMX.Next_Possible_Date_For_Sensor_Order__c = System.today()-2;
        
        Account acc = TestDataBuilder.getAccountList(1,payorRecId)[0];
        acc.Payor_Code_Commercial__c = 'K';
        acc.Shipment_Duration__c = 'Monthly';
        
        Account accMX = TestDataBuilder.getAccountList(1,payorRecId)[0];
        accMX.Payor_Code_Commercial__c = 'K';
        accMX.Shipment_Duration__c = 'Quarterly';
        
        insert new list <Account>{consumer, consumerMX, acc, accMX};
        consumer.Payor__c = acc.id;
        consumerMX.Payor__c = accMX.id;
        Update new list <Account>{consumer, consumerMX};        
        
        
        //Insert Test Order Headers
        List<Order_Header__c> thisOHList = new List<Order_Header__c>();
        Order_Header__c orderHeaderSTS = new Order_Header__c(Account__c = consumer.Id,
                                                             Order_Id__c = '12340',
                                                             Order_Type__c = 'MC Standard Sales Order'
                                                            );
        thisOHList.add(orderHeaderSTS);
        Order_Header__c orderHeaderSTT = new Order_Header__c(Account__c = consumer.Id,
                                                             Order_Id__c = '12341',
                                                             Order_Type__c = 'MC Standard Sales Order'
                                                            );
        
        thisOHList.add(orderHeaderSTT);
        
        Order_Header__c orderHeaderSTS1 = new Order_Header__c(Account__c = consumerMX.Id,
                                                             Order_Id__c = '12342',
                                                             Order_Type__c = 'MC Standard Sales Order'
                                                            );
        thisOHList.add(orderHeaderSTS1);
        Order_Header__c orderHeaderSTT1 = new Order_Header__c(Account__c = consumerMX.Id,
                                                             Order_Id__c = '12343',
                                                             Order_Type__c = 'MC Standard Sales Order'
                                                            );
        
        thisOHList.add(orderHeaderSTT1);
        
        insert thisOHList;  
        //Insert Test OLI
        List<Order_Item_Detail__c> thisOLIList = new List<Order_Item_Detail__c>();
        Order_Item_Detail__c newOLISTS = new Order_Item_Detail__c(Order_Header__c = orderHeaderSTS.Id, 
                                                                  Item_Number__c = 'STS-GF-003',
                                                                  Shipping_Date__c = System.today()-180, Quantity_Shipped__c = 3);
        thisOLIList.add(newOLISTS);
        Order_Item_Detail__c newOLISTT = new Order_Item_Detail__c(Order_Header__c = orderHeaderSTT.Id, 
                                                                  Item_Number__c = 'STT-GF-001',
                                                                  Shipping_Date__c = System.today()-190 , Quantity_Shipped__c = 3);
        thisOLIList.add(newOLISTT);
        
        Order_Item_Detail__c newOLISTS1 = new Order_Item_Detail__c(Order_Header__c = orderHeaderSTS1.Id, 
                                                                  Item_Number__c = 'STS-GF-003',
                                                                  Shipping_Date__c = System.today()-180, Quantity_Shipped__c = 3);
        thisOLIList.add(newOLISTS1);
        Order_Item_Detail__c newOLISTT1 = new Order_Item_Detail__c(Order_Header__c = orderHeaderSTT1.Id, 
                                                                  Item_Number__c = 'STT-GF-001',
                                                                  Shipping_Date__c = System.today()-190 , Quantity_Shipped__c = 3);
        thisOLIList.add(newOLISTT1);
        
        insert thisOLIList;
        
        
        List<String> prodNames = new List<String>{'BUN-OR-QRT', 'BUN-GF-QRT', 'BUN-GF-MTX', 'BUN-OR-MST', 'BUN-GF-MST', 'BUN-OR-MTX'};
        List<Product2> prodList = new List<Product2>();
        for(String prodName : prodNames){
            Product2 prod = new Product2();
            prod.Name = prodName;
            prod.IsActive = true;
            prod.Generation__c = 'G5';
            prodList.add(prod);
            
        }
        insert prodList;
        
        
        List<PricebookEntry> pricebookEntryList = new List<PricebookEntry>();
        for(Product2 prod : prodList){
            PricebookEntry pbe = new PricebookEntry();
            pbe.UseStandardPrice = false;
            pbe.Pricebook2Id=pb;
            pbe.Product2Id=prod.id;
            pbe.IsActive=true;
            pbe.UnitPrice=100.0;
            pricebookEntryList.add(pbe);
        }
        insert pricebookEntryList;    
        
        List<Opportunity> thisList = new List<Opportunity>();
        Opportunity opp = new Opportunity();
        opp.Pricebook2Id = pb;
        opp.Name = 'Test Opportunity NameX';
        opp.AccountId = consumer.Id;
        opp.StageName = 'New Opportunity';
        opp.CloseDate = date.today();
        opp.Type = 'Comp Product Request';
        opp.Payor__c = acc.Id;
        opp.Expedited_Shipping__c = true;
        opp.Shipping_Method__c = 'ship to';
        thisList.add(opp);
        
        Opportunity opp1 = new Opportunity();
        opp1.Pricebook2Id = pb;
        opp1.Name = 'Test Opportunity NameX';
        opp1.AccountId = consumerMX.Id;
        opp1.StageName = 'New Opportunity';
        opp1.CloseDate = date.today();
        opp1.Type = 'Comp Product Request';
        opp1.Payor__c = accMX.Id;
        opp1.Expedited_Shipping__c = true;
        opp1.Shipping_Method__c = 'ship to';
        thisList.add(opp1);
        
        insert thisList;
        Territory_Opportunity_Team_Role__c totcCS = Territory_Opportunity_Team_Role__c.getOrgDefaults();
        Territory_Alignment__c ta = [SELECT SA__c, PCS__c, Admin__c, MCS__c, DBM__c, RSD__c, Territory_Rep__c, Territory_Code__c
                                     FROM Territory_Alignment__c WHERE Territory_Code__c = '1234'];
        for (Opportunity opportunityRec : [SELECT Id, (SELECT Id, TeamMemberRole, UserId FROM OpportunityTeamMembers) FROM Opportunity]){
            System.assertEquals(7, opportunityRec.OpportunityTeamMembers.size());
            for (OpportunityTeamMember mem : opportunityRec.OpportunityTeamMembers){
                if(mem.TeamMemberRole == totcCS.SA__c){
                    System.assertEquals(ta.SA__c, mem.UserId);
                }
                if(mem.TeamMemberRole == totcCS.PCS__c){
                    System.assertEquals(ta.PCS__c, mem.UserId);
                }
                if(mem.TeamMemberRole == totcCS.Admin__c){
                    System.assertEquals(ta.Admin__c, mem.UserId);
                }
                if(mem.TeamMemberRole == totcCS.MCS__c){
                    System.assertEquals(ta.MCS__c, mem.UserId);
                }
                if(mem.TeamMemberRole == totcCS.DBM__c){
                    System.assertEquals(ta.DBM__c, mem.UserId);
                }
                if(mem.TeamMemberRole == totcCS.RSD__c){
                    System.assertEquals(ta.RSD__c, mem.UserId);
                }
                if(mem.TeamMemberRole == totcCS.Territory_Rep__c){
                    System.assertEquals(ta.Territory_Rep__c, mem.UserId);
                }
            }
        }
    //Insert Rules Matrix
        Rules_Matrix__c thisRule = new Rules_Matrix__c();
        thisRule.Account__c = acc.id; 
        thisRule.Quantity_Boxes__c = 3;
        thisRule.Rule_Criteria__c = 'Days For Next Order';
        thisRule.Is_True_or_False__c = true;
        thisRule.Duration_In_Days__c = 23;
        thisRule.Product_Type__c = 'Sensor';
        thisRule.Generation__c = 'G6';
        insert thisRule;
    
        Rules_Matrix__c thisRule1 = new Rules_Matrix__c();
        thisRule1.Account__c = acc.id; 
        thisRule1.Quantity_Boxes__c = 3;
        thisRule1.Rule_Criteria__c = 'Days For Next Order';
        thisRule1.Is_True_or_False__c = true;
        thisRule1.Duration_In_Days__c = 23;
        thisRule1.Product_Type__c = 'Sensor';
        thisRule1.Generation__c = 'G5';
        insert thisRule1;
        test.stopTest();
    }   
    
    private static testMethod void kCodeScenario3(){
        test.startTest();
        //Pricebook2 pb2 = [select Id, Name, IsActive from PriceBook2 where IsStandard=True LIMIT 1];
        Id pb = Test.getStandardPricebookID();
        System.debug('$$standard PB >> ' + pb);
       
        //Insert test Account
        id recId = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Consumers'].Id;
        id payorRecId = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Payor'].Id;
        
        Account consumer = TestDataBuilder.getAccountListConsumer(1, recId)[0];
        consumer.Default_Price_Book__c = pb;
        consumer.Next_Possible_Date_For_Sensor_Order__c = System.today()-2;
        
        Account consumerMX = TestDataBuilder.getAccountListConsumer(1, recId)[0];
        consumerMX.Default_Price_Book__c = pb;
        consumerMX.Next_Possible_Date_For_Sensor_Order__c = System.today()-2;
        
        Account acc = TestDataBuilder.getAccountList(1,payorRecId)[0];
        acc.Payor_Code_Commercial__c = 'K';
        acc.Shipment_Duration__c = 'Monthly';
        
        Account accMX = TestDataBuilder.getAccountList(1,payorRecId)[0];
        accMX.Payor_Code_Commercial__c = 'K';
        accMX.Shipment_Duration__c = 'Quarterly';
        
        insert new list <Account>{consumer, consumerMX, acc, accMX};
        consumer.Payor__c = acc.id;
        consumerMX.Payor__c = accMX.id;
        Update new list <Account>{consumer, consumerMX};        
        
        
        //Insert Test Order Headers
        List<Order_Header__c> thisOHList = new List<Order_Header__c>();
        Order_Header__c orderHeaderSTS = new Order_Header__c(Account__c = consumer.Id,
                                                             Order_Id__c = '12340',
                                                             Order_Type__c = 'MC Standard Sales Order'
                                                            );
        thisOHList.add(orderHeaderSTS);
        Order_Header__c orderHeaderSTT = new Order_Header__c(Account__c = consumer.Id,
                                                             Order_Id__c = '12341',
                                                             Order_Type__c = 'MC Standard Sales Order'
                                                            );
        
        thisOHList.add(orderHeaderSTT);
        
        Order_Header__c orderHeaderSTS1 = new Order_Header__c(Account__c = consumerMX.Id,
                                                             Order_Id__c = '12342',
                                                             Order_Type__c = 'MC Standard Sales Order'
                                                            );
        thisOHList.add(orderHeaderSTS1);
        Order_Header__c orderHeaderSTT1 = new Order_Header__c(Account__c = consumerMX.Id,
                                                             Order_Id__c = '12343',
                                                             Order_Type__c = 'MC Standard Sales Order'
                                                            );
        
        thisOHList.add(orderHeaderSTT1);
        
        insert thisOHList;  
        //Insert Test OLI
        List<Order_Item_Detail__c> thisOLIList = new List<Order_Item_Detail__c>();
        Order_Item_Detail__c newOLISTS = new Order_Item_Detail__c(Order_Header__c = orderHeaderSTS.Id, 
                                                                  Item_Number__c = 'STS-OR-003',
                                                                  Shipping_Date__c = System.today()-180, Quantity_Shipped__c = 3);
        thisOLIList.add(newOLISTS);
        Order_Item_Detail__c newOLISTT = new Order_Item_Detail__c(Order_Header__c = orderHeaderSTT.Id, 
                                                                  Item_Number__c = 'STT-OR-001',
                                                                  Shipping_Date__c = System.today()-190 , Quantity_Shipped__c = 3);
        thisOLIList.add(newOLISTT);
        
        Order_Item_Detail__c newOLISTS1 = new Order_Item_Detail__c(Order_Header__c = orderHeaderSTS1.Id, 
                                                                  Item_Number__c = 'STS-OR-003',
                                                                  Shipping_Date__c = System.today()-180, Quantity_Shipped__c = 3);
        thisOLIList.add(newOLISTS1);
        Order_Item_Detail__c newOLISTT1 = new Order_Item_Detail__c(Order_Header__c = orderHeaderSTT1.Id, 
                                                                  Item_Number__c = 'STT-OR-001',
                                                                  Shipping_Date__c = System.today()-190 , Quantity_Shipped__c = 3);
        thisOLIList.add(newOLISTT1);
        
        insert thisOLIList;
        
        
        List<String> prodNames = new List<String>{'BUN-OR-QRT', 'BUN-GF-QRT', 'BUN-GF-MTX', 'BUN-OR-MST', 'BUN-GF-MST', 'BUN-OR-MTX'};
        List<Product2> prodList = new List<Product2>();
        for(String prodName : prodNames){
            Product2 prod = new Product2();
            prod.Name = prodName;
            prod.IsActive = true;
            prod.Generation__c = 'G6';
            prodList.add(prod);
            
        }
        insert prodList;
        
        
        List<PricebookEntry> pricebookEntryList = new List<PricebookEntry>();
        for(Product2 prod : prodList){
            PricebookEntry pbe = new PricebookEntry();
            pbe.UseStandardPrice = false;
            pbe.Pricebook2Id=pb;
            pbe.Product2Id=prod.id;
            pbe.IsActive=true;
            pbe.UnitPrice=100.0;
            pricebookEntryList.add(pbe);
        }
        insert pricebookEntryList;    
        
        List<Opportunity> thisList = new List<Opportunity>();
        Opportunity newopp = TestDataBuilder.getOpportunityList(1, consumer.Id, acc.Id)[0];
        newopp.Pricebook2Id = pb;
        thisList.add(newopp);
        
        Opportunity newopp2 = TestDataBuilder.getOpportunityList(1, consumerMX.Id, accMX.Id)[0];
        newopp2.Pricebook2Id = pb;
        thisList.add(newopp2);
        
        Opportunity opp = new Opportunity();
        opp.Pricebook2Id = pb;
        opp.Name = 'Test Opportunity NameX';
        opp.AccountId = consumer.Id;
        opp.StageName = 'New Opportunity';
        opp.CloseDate = date.today();
        opp.Type = 'Comp Product Request';
        opp.Payor__c = acc.Id;
        opp.Expedited_Shipping__c = true;
        opp.Shipping_Method__c = 'ship to';
        thisList.add(opp);
        
        Opportunity opp1 = new Opportunity();
        opp1.Pricebook2Id = pb;
        opp1.Name = 'Test Opportunity NameX';
        opp1.AccountId = consumerMX.Id;
        opp1.StageName = 'New Opportunity';
        opp1.CloseDate = date.today();
        opp1.Type = 'Comp Product Request';
        opp1.Payor__c = accMX.Id;
        opp1.Expedited_Shipping__c = true;
        opp1.Shipping_Method__c = 'ship to';
        thisList.add(opp1);
        
        insert thisList;
        Territory_Opportunity_Team_Role__c totcCS = Territory_Opportunity_Team_Role__c.getOrgDefaults();
        Territory_Alignment__c ta = [SELECT SA__c, PCS__c, Admin__c, MCS__c, DBM__c, RSD__c, Territory_Rep__c, Territory_Code__c
                                     FROM Territory_Alignment__c WHERE Territory_Code__c = '1234'];
        for (Opportunity opportunityRec : [SELECT Id, (SELECT Id, TeamMemberRole, UserId FROM OpportunityTeamMembers) FROM Opportunity]){
            System.assertEquals(7, opportunityRec.OpportunityTeamMembers.size());
            for (OpportunityTeamMember mem : opportunityRec.OpportunityTeamMembers){
                if(mem.TeamMemberRole == totcCS.SA__c){
                    System.assertEquals(ta.SA__c, mem.UserId);
                }
                if(mem.TeamMemberRole == totcCS.PCS__c){
                    System.assertEquals(ta.PCS__c, mem.UserId);
                }
                if(mem.TeamMemberRole == totcCS.Admin__c){
                    System.assertEquals(ta.Admin__c, mem.UserId);
                }
                if(mem.TeamMemberRole == totcCS.MCS__c){
                    System.assertEquals(ta.MCS__c, mem.UserId);
                }
                if(mem.TeamMemberRole == totcCS.DBM__c){
                    System.assertEquals(ta.DBM__c, mem.UserId);
                }
                if(mem.TeamMemberRole == totcCS.RSD__c){
                    System.assertEquals(ta.RSD__c, mem.UserId);
                }
                if(mem.TeamMemberRole == totcCS.Territory_Rep__c){
                    System.assertEquals(ta.Territory_Rep__c, mem.UserId);
                }
            }
        }
        UtilityClass.runBeforeTrigger = true;
        update thisList;
        OpportunityTriggerHandler.deleteDefaultSKUOnOpty(thisList);
        test.stopTest();
    }
}