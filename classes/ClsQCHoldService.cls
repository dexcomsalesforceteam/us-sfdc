/*************************************************
* @author      : Dexcom, LTI
* @date        : April, 2019
* @description : Controller QC Hold :Service Call
*************************************************/
public class ClsQCHoldService {
    //get Order Type
    @AuraEnabled
    public static Map<String, String> getOrderType(String orderId, String accountId){
        Map<String, String> responseMap = new Map<String, String>();
        
        Order thisOrder = [Select Id, Type_Of_Order__c, Price_Book__r.Name, Price_Book__r.Oracle_Id__c, Is_Cash_Order__c, 
                            Account.Payor__r.Skip_Payor_Matrix__c, Account.Payor__r.Name from Order where Id =: orderId];
        System.debug('## Order ** ' + thisOrder );
        
        Account accnt = [SELECT Id, Last_Order_Pricebook__c,Payor__r.name ,Payor__r.CMN_Waived__c, BI_Primary_Plan_Type__c FROM ACCOUNT WHERE Id =:accountId];
        System.debug('## accnt pb ** ' + accnt.Last_Order_Pricebook__c );
        System.debug('## thisOrder.Price_Book__r.Name ** ' + thisOrder.Price_Book__r.Name );
         
        String orderTypeValue = accnt.Last_Order_Pricebook__c != thisOrder.Price_Book__r.Name ? 'New Order' : 'Reorder';
        responseMap.put('OrderType', orderTypeValue);
        
        Boolean canBypass = FeatureManagement.checkPermission('AllowToBypassQcHoldCheck');
        responseMap.put('AllowBypassCheck', String.valueOf(canBypass));
        
        Boolean exceptionPB = false;
        if(thisOrder.Is_Cash_Order__c || 
            thisOrder.Price_Book__r.Oracle_Id__c == '6550' ||
                    thisOrder.Price_Book__r.Oracle_Id__c == '6840' ||
                    thisOrder.Price_Book__r.Oracle_Id__c == '13920')
            exceptionPB = true;
        
        responseMap.put('ExceptionPricebook', String.valueOf(exceptionPB));
        Boolean skipPayor = thisOrder.Account.Payor__r.Skip_Payor_Matrix__c || accnt.BI_Primary_Plan_Type__c == 'MED ADV' ? true : false;
        Boolean medAdvPatient = accnt.BI_Primary_Plan_Type__c == 'MED ADV' ? true : false;
        responseMap.put('SkippedPayorMatrix', String.valueOf(skipPayor));
        responseMap.put('SkippedPayorMatrixForMedAdv', String.valueOf(medAdvPatient));
        responseMap.put('thisPayorName', String.valueOf(thisOrder.Account.Payor__r.Name));
        System.debug('## thisResponse ## ' + responseMap );         
        return responseMap;
    }
  
    //Stores information 
    @AuraEnabled
    public static Boolean saveOrderDetails(String orderId, Map<String, String> benefitMap, Map<String, String> payorMap, Map<String, String> orderMap, Map<String, String> documentMap) {
        Boolean benefitCheck = false;
        Boolean payorCheck = false;
        Boolean orderCheck = false;
        Boolean documentCheck = false;
        
        System.debug('benefitMap :: ' + benefitMap);
        if(benefitMap != null){
            //if Prior Auth Section is hidden, forcefully mark it to true
           /* if(benefitMap.get('priorAuthCheck') == 'false' && 
               benefitMap.get('receiverInfoCheck') == 'N/A' && 
               benefitMap.get('priorAuthInfoCheck') == 'N/A' &&
               benefitMap.get('sensorInfoCheck') == 'N/A' && 
               benefitMap.get('transmitterInfoCheck') == 'N/A' ){
               benefitMap.put('priorAuthCheck', 'true');
            }*/
            benefitCheck = saveAuditCheckInfo(orderId, 'Benefit', benefitMap);
            System.debug('benefitCheck :: ' + benefitCheck);
        }
        System.debug('payorMap :: ' + payorMap);
        if(payorMap != null){
            payorCheck = saveAuditCheckInfo(orderId, 'Payor', payorMap);
            System.debug('payorCheck :: ' + payorCheck);
        }
        System.debug('orderMap :: ' + orderMap);
        if(orderMap != null){
            orderCheck = saveAuditCheckInfo(orderId, 'Order', orderMap);
            System.debug('orderCheck :: ' + orderCheck);
        }        
        System.debug('documentMap :: ' + documentMap);
        if(documentMap != null){
            documentCheck = saveAuditCheckInfo(orderId, 'Document', documentMap);
            System.debug('documentCheck :: ' + documentCheck);
        }
        //return benefitCheck && payorCheck && orderCheck && documentCheck;
        return !benefitCheck && !payorCheck && !orderCheck && !documentCheck; // return true for no exception
    }
    
    public static Boolean saveAuditCheckInfo(String orderId, String auditArea, Map<String, String> checkDataMap){
        List<SObject> orderCheckList = new List<SObject>();
        
        Map<String, Order_Audit__c> orderAuditMap = new Map<String, Order_Audit__c>();
        Map<String, Order_Audit_Item__c> orderAuditItemMap = new Map<String, Order_Audit_Item__c>();
        Set<Order_Audit__c> updateAuditSet = new Set<Order_Audit__c>();
        List<Order_Audit_Item__c> updateAuditItemList = new List<Order_Audit_Item__c>();
        Set<Id> auditIdSet = new Set<Id>();
        Map<String, String> fieldMappings = new Map<String, String>();
        fieldMappings = ClsOrderAuditHandler.getfieldMappings(auditArea);
        System.debug('checkDataMap >>' + checkDataMap);
        for(Order_Audit__c thisAudit : [Select Id, Audit_Area__c, Audit_Exception__c, Audit_Field__c, Audit_Verified__c, Order_Number__c,
                                        Audit_Exception_Added_By__c, Audit_Exception_Added_Date__c from Order_Audit__c 
                                        where Order_Number__c =:orderId and Audit_Area__c =: auditArea]){
                orderAuditMap.put(thisAudit.Audit_Field__c , thisAudit);
                auditIdSet.add(thisAudit.Id);
        }
        
        for(Order_Audit_Item__c thisAuditItem : [Select Id, Audit_Item_Field__c, Audit_Item_Value__c, Order_Audit__c from Order_Audit_Item__c
                                                    where Order_Audit__c IN : auditIdSet]){         
            orderAuditItemMap.put(thisAuditItem.Audit_Item_Field__c, thisAuditItem);            
        }
        
        if(!checkDataMap.isEmpty()){
            for(String auditDataRecord : checkDataMap.keySet()){
                for(String thisAuditName : orderAuditMap.keySet()){
                    Order_Audit__c thisAudit = orderAuditMap.get(thisAuditName);
                    
                    if(auditDataRecord != 'bypassCheck' && auditDataRecord == fieldMappings.get(thisAuditName) && thisAudit.Audit_Verified__c != Boolean.valueOf(checkDataMap.get(auditDataRecord))){
                        thisAudit.Audit_Verified__c = Boolean.valueOf(checkDataMap.get(auditDataRecord));
                        updateAuditSet.add(thisAudit);
                    }
                    if(auditDataRecord == 'bypassCheck' && Boolean.valueOf(checkDataMap.get(auditDataRecord)) && 
                        thisAudit.Audit_Field__c == 'Bypass Check' && !thisAudit.Audit_Exception__c){
                        thisAudit.Audit_Exception__c = true;
                        thisAudit.Audit_Exception_Added_By__c = System.UserInfo.getUserId();
                        thisAudit.Audit_Exception_Added_Date__c = System.today();
                        updateAuditSet.add(thisAudit);
                    }
                    if(auditDataRecord == 'bypassCheck' && !Boolean.valueOf(checkDataMap.get(auditDataRecord)) && 
                        thisAudit.Audit_Field__c == 'Bypass Check' && thisAudit.Audit_Exception__c){
                        thisAudit.Audit_Exception__c = false;
                        thisAudit.Audit_Exception_Added_By__c = null;
                        thisAudit.Audit_Exception_Added_Date__c = null;
                        updateAuditSet.add(thisAudit);
                    }
                }
            }
            System.debug('orderAuditItemMap >> ' +orderAuditItemMap );
            for(String auditDataRecord : checkDataMap.keySet()){ 
                for(String thisAuditItemName : orderAuditItemMap.keySet()){ 
                    Order_Audit_Item__c thisAuditItem = orderAuditItemMap.get(thisAuditItemName);
                    
                    if(auditDataRecord == fieldMappings.get(thisAuditItemName) && thisAuditItem.Audit_Item_Value__c != checkDataMap.get(auditDataRecord)){
                        thisAuditItem.Audit_Item_Value__c = checkDataMap.get(auditDataRecord);
                        updateAuditItemList.add(thisAuditItem);
                    }
                }
            }
            if(!updateAuditSet.isEmpty()) {
                for(Order_Audit__c thisAudit : updateAuditSet){
                    orderCheckList.add(thisAudit);
                }
            }
            if(!updateAuditItemList.isEmpty()) orderCheckList.addAll(updateAuditItemList);
            if(!orderCheckList.isEmpty()){
                try{
                    update orderCheckList;
                    return false;            
                }catch(Exception Ex){
                    System.debug('Exception to Save : Audit Area : ' + auditArea + ' :: Exception :: ' + Ex);
                    return true;
                }
            }
        }
        return false;
    }
}