global class BclsMarketingFieldsUpdateSched implements Schedulable {
	global void execute(SchedulableContext ctx){
      BclsMarketingFieldsUpdate batch = new BclsMarketingFieldsUpdate();
      Database.executebatch(batch, 50);
}
}