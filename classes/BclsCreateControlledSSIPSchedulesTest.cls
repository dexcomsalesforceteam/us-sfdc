/*
 * @description : This test class is created to cover BclsCreateControlledSSIPSchedules
 * and BclsCreateControlledSSIPSchedulesSchd
 * @Author : LTI
*/

@isTest(seeAllData = false)
public class BclsCreateControlledSSIPSchedulesTest {
    
    @testSetUp
    public static void setUpTestData(){
        Id recTypePayorId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
        Id recTypeConsumerId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Consumers').getRecordTypeId();
        
        Account payorAccount=new Account();
        payorAccount.name = 'Test Payor';
        payorAccount.Phone = '123456789';
        payorAccount.DOB__c = Date.newInstance(1992, 04, 23);
        payorAccount.Days_For_Sensor_2_Box_Shipment__c= 2;
        payorAccount.recordtypeid= recTypePayorId;
        payorAccount.Default_Price_Book__c = Test.getStandardPricebookId();
        insert payorAccount;
        
        Account testAccount=TestDataBuilder.testAccount();
        testAccount.Payor__c = payorAccount.id;
        testAccount.Customer_Type__c = 'Commercial';
        testAccount.PersonEmail = 'bclsCreateControllerSSIP@mail.com';
        testAccount.Default_Price_Book__c = Test.getStandardPricebookId();
        testAccount.recordtypeid = recTypeConsumerId;        
        insert testAccount;
        
        Order_Header__c orderHeader=new Order_Header__c();
        orderHeader.Account__c =testAccount.id;
        orderHeader.Order_Type__c = 'Standard Sales Order';
        insert orderHeader;
        
        List<Order_Item_Detail__c> oidList = new List<Order_Item_Detail__c>();
        Order_Item_Detail__c oid1  = new Order_Item_Detail__c(Item_Number__c = 'BUN-GF-003',Generation__c='G6', Order_Header__c = orderHeader.Id,Quantity__c =1,Quantity_Shipped__c =1,Shipping_Date__c =system.today()-850);
        //Order_Item_Detail__c oid2  = new Order_Item_Detail__c(Item_Number__c = 'STS-GL-041',Generation__c='G5', Order_Header__c = orderHeader.Id,Quantity__c =3,Quantity_Shipped__c =3,Shipping_Date__c =system.today()-185);
        oidList.add(oid1);
        //oidList.add(oid2);
        insert oidList;
        
        SSIP_Rule__c ssipRule=new SSIP_Rule__c();
        ssipRule.Account__c = testAccount.Id;
        ssipRule.Rule_Start_Date__c = system.today()-660;
        ssipRule.Rule_End_Date__c = null;
        ssipRule.Product__c = 'G6 | BUN-OR-TX6 | 1';
        ssipRule.First_Shipment_Date__c= system.today()-660;
        ssipRule.Disclaimer__c =true;
        ssipRule.Frequency_In_Days_Number__c = 180;
        insert ssipRule;
        
        SSIP_Rule__c ssirRuleFetchedObj = [Select Id,Schedule_Cycle__c from SSIP_Rule__c where id = : ssipRule.Id ];
        system.debug('ssirRuleFetchedObj:::::'+ssirRuleFetchedObj);
    }
    
    @isTest
    public static void bclsCreateControlledSSIPSchedulesTest(){
        test.startTest();
        Database.executeBatch(new BclsCreateControlledSSIPSchedules(), 100);
        test.stopTest();
    }
    
    @isTest
    public static void bclsCreateControlledSSIPSchedulesSchdTest(){
        Test.startTest();
        BclsCreateControlledSSIPSchedulesSchd scheduler = new BclsCreateControlledSSIPSchedulesSchd();
        String sch = '0 0 23 * * ?';
        system.schedule('BclCreateControlledSSIP Schedules', sch, scheduler);
        Test.stopTest();
    }

}