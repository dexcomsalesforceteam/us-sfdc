/***
*@Author        : Priyanka Kajawe
*@Date Created  : 05-Sept-2018
*@Description   : Schedulable Apex for Batch Class : BclsDocCollectionUpdateBatch 
***/

global class BclsDocCollectionUpdateSched implements Schedulable {
    public final static String CRON_EXP = '0 0 3 ? * THU *';
    global void execute(SchedulableContext sc) {
        BclsDocCollectionUpdateBatch  bCls = new BclsDocCollectionUpdateBatch (); 
        database.executebatch(bCls);
    }
    // to run this batch, copy and execute in dev console:
    // System.Schedule('BclsDocCollectionUpdateBatch', BclsDocCollectionUpdateSched.CRON_EXP, new BclsDocCollectionUpdateSched());
}