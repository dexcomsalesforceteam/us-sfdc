/***
*@Author        : Abdul Mohammed
*@Date Created  : Dec-10-2019
*@Description   : Create for Auto QC story: "CRMSF-4614"
***/
    global with sharing class BclsOrderAutoQCBatch implements Database.Batchable<sObject>{
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        Date daysFromNow = System.today().addDays(3); 
        Date daysFrombefore = System.today().addDays(-4); 
        String soqlQuery;
        soqlQuery = 'SELECT ID,Account.Territory_RSS__c,Account.Territory_Supervisor__c,Scheduled_Ship_Date__c,Type,AccountId,Is_Benefit_Check_Completed__c,Is_Payor_Check_Completed__c,Is_Document_Check_Completed__c,Is_Order_Check_Completed__c FROM Order ';
        soqlQuery += 'WHERE ';
        soqlQuery += 'Scheduled_Ship_Date__c != Null and  Sub_Type__c = \'Future Order\' and id = \'8014F000001BSub\' and status = \'Draft\' limit 1'; 
        return Database.getQueryLocator(soqlQuery);
    }
    
    global void execute(Database.BatchableContext BC, List<Order> scope){      
        
        system.debug('====scope===='+scope );
        ClsUpdateOfAUtoQBatchController.bclsOrderAutoQCBatchExecuteMothod(scope);
    } 
    
    global void finish(Database.BatchableContext BC) {
        
    }
}