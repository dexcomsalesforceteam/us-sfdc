/***
*@Author        : Sundog
*@Date Created  : 04-10-2019
*@Description   : Batch job that creates Marketing_Interaction__c records for Opportunities that have had an
"Pending Auth" value on the Onboarding_Step__c for over 7 business days.
Scheduled by BclsPendingAuthUpdateSched
***/
global with sharing class BclsPendingAuthUpdateBatch implements Database.Batchable<sObject>, Database.Stateful{
    global Set<Id> alreadyProcessedOpptyIds {get;set;}
    global Database.QueryLocator start(Database.BatchableContext BC){
        alreadyProcessedOpptyIds = new Set<Id>();
        Date daysFromNow = addBusinessDays(System.today(), -7);
        String soqlQuery;
        soqlQuery = 'SELECT ID, Onboarding_Steps__c, Onboarding_Step_Update_Date__c,AccountId FROM Opportunity ';
        soqlQuery += 'WHERE ';
        soqlQuery += 'Onboarding_Steps__c = \'Authorization\' and StageName=\'5. Prior-Authorization\' AND ';
        soqlQuery += 'Onboarding_Step_Update_Date__c != Null and Onboarding_Step_Update_Date__c <= :daysFromNow AND';
        soqlQuery += '(Type = \'NEW SYSTEM\' OR Type = \'Medicare – New Patient\' OR Type = \'Physician Referral\' OR Type = \'OOW Receiver Only\' ';
        soqlQuery += 'OR Type = \'OOW SYSTEM\' OR Type = \'OOW Transmitter Only\' OR Type = \'Sensor-Reorder\') AND RecordType.Name = \'US Opportunity\'';
        soqlQuery += 'AND StageName != \'10. Cancelled\' ';
        
        return Database.getQueryLocator(soqlQuery);
    }
    
    global void execute(Database.BatchableContext BC, List<Opportunity> scope){
        for(Opportunity opp : scope){
            System.debug(opp);
        }
        List<Marketing_Interaction__c> marketingInteractionList = new List<Marketing_Interaction__c>();
        if(!scope.isEmpty()){
            List<Id> scopeIds = new List<Id>();
            for(Opportunity thisOpty : scope) {
                scopeIds.add(thisOpty.Id);
            }
            for(Marketing_Interaction__c mic : [SELECT Id, Source_Record_Id__c FROM Marketing_Interaction__c 
                                                WHERE Source_Record_Id__c IN :scopeIds 
                                                 AND Communication_Type__c = 'Authorization Status' 
                                                 AND CreatedDate > :Datetime.now().addDays(-8)]) {
                alreadyProcessedOpptyIds.add(mic.Source_Record_Id__c); // avoid duplicate record creation
            }
            for(Opportunity thisOpty : scope) {
                if(!alreadyProcessedOpptyIds.contains(thisOpty.Id)) {
                    Marketing_Interaction__c thisMI = new Marketing_Interaction__c();
                    thisMI.Source_Record_Id__c = thisOpty.Id;  // Add Source Record Id as Opty Id
                    thisMI.Account__c = thisOpty.AccountId; //Account Id
                    thisMI.Communication_Type__c = 'Authorization Status'; 
                    marketingInteractionList.add(thisMI); //add Record to List to Insert
                    alreadyProcessedOpptyIds.add(thisOpty.Id); // add OpptyId to already processed list to avoid duplicate record creation
                }
            }  
            //Insert Marketing Interaction Records
            if(!marketingInteractionList.isEmpty()){
                try{
                    Database.insert(marketingInteractionList,false);  //Insert Marketing Interaction Records
                }catch (DmlException de) {
                    Integer numErrors = de.getNumDml();
                    System.debug('getNumDml=' + numErrors);
                    for(Integer i=0;i<numErrors;i++) {
                        System.debug('getDmlFieldNames=' + de.getDmlFieldNames(i));
                        System.debug('getDmlMessage=' + de.getDmlMessage(i));
                    }
                }
                for(Marketing_Interaction__c mi : marketingInteractionList){
                    System.debug(mi);
                }
            }   
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
    
    private static Date addBusinessDays(Date startDate, Integer businessDaysToAdd){
        //Add or decrease in BusinessDaysToAdd days 
        Date finalDate = startDate;
        system.debug('finaldate = '+ finalDate);
        integer direction = businessDaysToAdd < 0 ? -1 : 1;
        system.debug('direction = '+ direction);
        while(businessDaysToAdd != 0) {
            finalDate = finalDate.addDays(direction);
            system.debug('businessDaysToAdd = '+ businessDaysToAdd);
            system.debug('finaldate = '+ finalDate);
            if (!isWeekendDay(finalDate)) {
                businessDaysToAdd -= direction;
            }
        }

        return finalDate;
    }
    
    private static Boolean isWeekendDay(Date dateParam) {
        Integer dayOfWeek = getDayOfWeek(dateParam);
        return dayOfWeek == 0 || dayOfWeek == 6 ? true : false;
    }
    
    private static Integer getDayOfWeek(Date dateParam) {
        Date startOfWeek = dateParam.toStartOfWeek();
        return startOfWeek.daysBetween(dateParam);
    }
}