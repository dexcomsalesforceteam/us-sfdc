/****************************************************************************
*@Author        : Jagan Periyakaruppan
*@Date Created  : 31-Aug-2018
*@Description   : Schema class for Credit Card data
****************************************************************************/
public class ClsConvertCCJSONPayloadToApex {

	@AuraEnabled public Integer count;
	@AuraEnabled public List<Result> result;

	public class Result {
		@AuraEnabled public String cardID;
		@AuraEnabled public String name;
		@AuraEnabled public String maskedNumber;
		@AuraEnabled public String expirationDate;
		@AuraEnabled public String cardType;
		@AuraEnabled public String partySiteID;
		@AuraEnabled public String address1;
		@AuraEnabled public String city;
		@AuraEnabled public String state;
		@AuraEnabled public String postalCode;
		@AuraEnabled public String country;
		@AuraEnabled public Integer orderOfPreference;
	}
	public static ClsConvertCCJSONPayloadToApex parse(String json) {
		return (ClsConvertCCJSONPayloadToApex) System.JSON.deserialize(json, ClsConvertCCJSONPayloadToApex.class);
	}
}