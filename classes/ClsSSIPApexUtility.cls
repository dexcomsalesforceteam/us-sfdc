/*
@Author      - Anuj Patel
@Date        - 07/24/2019
@Description - Utility classes to support processing for BclsProcessSSIPSchedules.
@Condition 1
--------------------------------------------------------------------------------------------------*/
public class ClsSSIPApexUtility {
    
    //Static variable, which will be used in SSIPScheduleTrigger
    public static boolean checkFirstRun = true;
    
    //Method will check for Prior Authorization based on the Product type and date
    public static Boolean checkPriorAuth(String productType, Date scheduledShipmentDate, Benefits__c thisBenefit)
    {
        //For each Shedule included in the String list, check the Prior auth
        if(productType == 'Sensor' && 
            thisBenefit.SENSOR_AUTH_END_DATE__c != null && 
                thisBenefit.SENSOR_AUTH_END_DATE__c > scheduledShipmentDate ){
                
                return true;   
            }
        if(productType == 'Transmitter' && 
            thisBenefit.TRANSMITTER_AUTH_END_DATE__c  != null &&  
                thisBenefit.TRANSMITTER_AUTH_END_DATE__c > scheduledShipmentDate ){
                    
                return true;
            }
                        
        return false;
    }

    //Method will check for CMN Expiration
    public static Boolean checkCMNActive(Date cmnOrRxExpirationDate, Date scheduledShipmentDate)  
    {
        if(cmnOrRxExpirationDate != null && 
            cmnOrRxExpirationDate > scheduledShipmentDate ){
                return true;
            }
            
        return false;
    }
    
    /*********************************************************************************
     * @Description : This method will validate Credit Card check based on Order Amount for each SSIP Schedule
     * @Param : Schedule Order Amount, creditCardExpirationDate, scheduledShipmentDate, benefit
     * @Return: Boolean
    *********************************************************************************/
    public static Boolean validateCreditCardCheck(Decimal ssipOrderAmount, Date creditCardExpirationDate, Date scheduledShipmentDate, Benefits__c benefit)  
    {
        
        Decimal indDeductible= benefit.INDIVIDUAL_DEDUCTIBLE__c == null ? 0 : benefit.INDIVIDUAL_DEDUCTIBLE__c;
        Decimal indDeductibleMet= benefit.INDIVIDUAL_MET__c == null ? 0 : benefit.INDIVIDUAL_MET__c;
        Decimal indDeductibleRem= benefit.Individual_Deductible_Remaining__c == null ? 0 : benefit.Individual_Deductible_Remaining__c;            
        
        Decimal indOOPMax= benefit.INDIVIDUAL_OOP_MAX__c == null ? 0 : benefit.INDIVIDUAL_OOP_MAX__c;
        Decimal indOOPMaxMet= benefit.INDIVIDUAL_OOP_MET__c == null ? 0 : benefit.INDIVIDUAL_OOP_MET__c;
        Decimal indOOPMaxRem= benefit.Individual_OOP_Remaining__c == null ? 0 : benefit.Individual_OOP_Remaining__c;
        
        Decimal famDeductible= benefit.FAMILY_DEDUCT__c == null ? 0 : benefit.FAMILY_DEDUCT__c;
        Decimal famDeductibleMet= benefit.Family_Met__c == null ? 0 : benefit.Family_Met__c;
        Decimal famDeductibleRem= benefit.Family_Deductible_Remaining__c== null ? 0 : benefit.Family_Deductible_Remaining__c;
        
        Decimal famOOPMax= benefit.FAMILY_OOP_MAX__c == null ? 0 : benefit.FAMILY_OOP_MAX__c;
        Decimal famOOPMaxMet= benefit.FAMILY_OOP_MET__c == null ? 0 : benefit.FAMILY_OOP_MET__c;
        Decimal famOOPMaxRem= benefit.Family_OOP_Remaining__c == null ? 0 : benefit.Family_OOP_Remaining__c;

        //calculate the amount
        if(ssipOrderAmount > 0 && benefit.Coverage__c > 0){
            Decimal orderAmount = CalculateCoPay.CalcCoPay(ssipOrderAmount,
                                    benefit.Coverage__c, 
                                    indDeductible, indDeductibleMet, indDeductibleRem,
                                    indOOPMax, indOOPMaxMet,indOOPMaxRem,
                                    famDeductible, famDeductibleMet, famDeductibleRem,
                                    famOOPMax, famOOPMaxMet, famOOPMaxRem);
            
            System.debug('orderAmount :: ' + orderAmount); 
            
            if(orderAmount == 0 || 
                    (orderAmount > 0 && creditCardExpirationDate != null && 
                        creditCardExpirationDate > scheduledShipmentDate)){
                return true;  
            }else{
             return false;
            }
        }else{
            System.debug('Order Amount :: '+ ssipOrderAmount  + ' :: Benefit Coverage :: ' + benefit.Coverage__c);
            return false;
        }
    }
    
    /*********************************************************************************
     * @Description : This method will calculate Order Amount for each SSIP Schedule
     * @Param : List(SSIP Schedules)
     * @Return: Map
    *********************************************************************************/
    public static Map<Id, Decimal> calculateTotalOrderAmount(List<SSIP_Schedule__c> ssipSchedules){
        Map<Id, String> productNameMap = new Map<Id, String>(); // Map stores product Name for SSIP Schedule
        Map<Id, Integer> productQuantityMap = new Map<Id, Integer>(); // Map stores product Quantity for SSIP Schedule
        Set<Id> pricebookids = new Set<Id>();//Set holds all the Pricebook Ids included in the process
        Map<String,PricebookEntry> priceBookEntryProductMap = new Map<String,PricebookEntry>();//Map holds the ProductName+Pricebook Id to PricebookentryId
        Map<Id, Decimal> orderAmountMap = new Map<Id, Decimal>(); //Map will hold the Total Order Amount for SSIP Schedule
        
        for(SSIP_Schedule__c thisSchedule : ssipSchedules){
            String productName = thisSchedule.Product__c.substringBetween('| ').trim();
            productNameMap.put(thisSchedule.id, productName);
            
            List<String> productSplits = thisSchedule.Product__c.split('|');
            Integer quantity = Integer.valueOf(productSplits[productSplits.size()-1].trim());
            productQuantityMap.put(thisSchedule.id, quantity);   

            pricebookids.add(thisSchedule.Price_Book__c);
        }
        
         //Get the price book entries for each Product involved in the process       
        if(!productNameMap.isEmpty() || !pricebookids.isEmpty()){
            Set<PricebookEntry> priceBookEntrys = new Set<PricebookEntry>([SELECT ID, Pricebook2Id, UnitPrice, Pricebook2.Oracle_Id__c, Product2.Name from PricebookEntry where Product2.Name IN : productNameMap.Values() and Pricebook2.Id IN :pricebookids]);
            System.debug('priceBookEntrys=' + priceBookEntrys);
            
            //Populate the map priceBookEntryProductMap (ProductName+Pricebook Id to PricebookentryId)            
            for(PricebookEntry thispbEntry :priceBookEntrys){
                priceBookEntryProductMap.put(thispbEntry.Product2.Name+thispbEntry.Pricebook2Id, thispbEntry);
            }  
        }
        System.debug('priceBookEntryProductMap =' + priceBookEntryProductMap);
        
        for(SSIP_Schedule__c thisSSIP : ssipSchedules){
            
            if(!productNameMap.isEmpty() && productNameMap.containsKey(thisSSIP.Id) && 
                !productQuantityMap.isEmpty() && productQuantityMap.containsKey(thisSSIP.Id)){
                
                String key =  productNameMap.get(thisSSIP.Id) + thisSSIP.Price_Book__c;
                System.debug('Key ::' + key);
                
                if(!priceBookEntryProductMap.isEmpty() && priceBookEntryProductMap.containsKey(key)){
                    System.debug('PBE found ::' + priceBookEntryProductMap.get(key));
                    
                    Decimal orderAmount = priceBookEntryProductMap.get(key).UnitPrice * productQuantityMap.get(thisSSIP.Id);
                    orderAmountMap.put(thisSSIP.Id, orderAmount);
                    
                    System.debug('SSIP Schedule Id :: '+ thisSSIP.Id + ' :: Order Amount :: ' + orderAmount);
                }
            }
        }
        
        return orderAmountMap;
    }
    
    //Method will create the Opportunity for the Schedules
    public static Map<Id, Id> createOpportunityforSSIPSch (List<ClsOpptyTaskWrapper> opptyTaskWrapperList)
    {
        system.debug ('Opp Creation');
        Map<Id, Id> ssipScheduleToOptyMap = new Map<Id, Id>();
        List<Opportunity> newOppList = new List<Opportunity>();//Opportunity records to be created
        Set<Id> ssipIdList = new Set<Id>();
        List<ClsOpptyTaskWrapper> existingOpptyTaskWrapperList = new List<ClsOpptyTaskWrapper>();
        List<ClsOpptyTaskWrapper> newOpptyTaskWrapperList = new List<ClsOpptyTaskWrapper>();
        Id oppRecTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('US Opportunity').getRecordTypeId();
        List<Opportunity_Status_Selection__mdt> oppStatusStagesList = new List<Opportunity_Status_Selection__mdt>();
        oppStatusStagesList =  [SELECT DeveloperName,Is_BI_Active__c,Is_Credit_Card_Active__c,Stages__c,Is_CMN_Active__c,Is_Pre_Authorized__c,Status__c FROM Opportunity_Status_Selection__mdt];  // Auth_Expired__c,CMN_Expired__c,
        
        for(ClsOpptyTaskWrapper opptyTaskWrapper : opptyTaskWrapperList){
            ssipIdList.add(opptyTaskWrapper.ssipId);
        }
        
        for(Opportunity existingOptyRecord : [SELECT Id,SSIP_Schedule__c FROM Opportunity WHERE SSIP_Schedule__c IN : ssipIdList]){
            for(ClsOpptyTaskWrapper opptyTaskWrapper : opptyTaskWrapperList){
                if(opptyTaskWrapper.ssipId == existingOptyRecord.SSIP_Schedule__c){
                    opptyTaskWrapper.opportunityId = existingOptyRecord.Id;
                    ssipScheduleToOptyMap.put(existingOptyRecord.SSIP_Schedule__c, existingOptyRecord.Id); // Opty already exists
                }
            }
        }
        for(ClsOpptyTaskWrapper opptyTaskWrapper : opptyTaskWrapperList){
            if(opptyTaskWrapper.opportunityId == null){ //if there is no existing Oppty record for SSIP schedule then only create new
                Opportunity newOpp = new Opportunity();
                newOpp.name = opptyTaskWrapper.accountName+' - '+opptyTaskWrapper.accountTerrirotyCode;
                newOpp.Payor__c = opptyTaskWrapper.payor;
                newOpp.Type = 'SSIP';
                newOpp.RecordTypeId = oppRecTypeId; 
                newOpp.AccountId = opptyTaskWrapper.accountId;
                newOpp.CloseDate = system.today()+30;
                newOpp.SSIP_Schedule__c = opptyTaskWrapper.ssipId;
                //  newOpp.Close_Reason__c =  'SSIP Verified';
                for(Opportunity_Status_Selection__mdt oss :oppStatusStagesList)
                {
                    IF(opptyTaskWrapper.isBiActive == oss.Is_BI_Active__c && opptyTaskWrapper.isCmnActive == oss.Is_CMN_Active__c && opptyTaskWrapper.isCreditCardActive == oss.Is_Credit_Card_Active__c && opptyTaskWrapper.isPreAuthorized == oss.Is_Pre_Authorized__c )
                    {
                        //  newOpp.StageName = '4. Doc Collection';
                        //   newOpp.Status__c = '4.1 Identify Docs to Collect';
                        system.debug('oss.Stages__c' +oss.Stages__c);
                        system.debug('oss.Status__c' +oss.Status__c);
                        newOpp.StageName = oss.Stages__c;
                        newOpp.Status__c = oss.Status__c;
                    }
                }
                newOppList.add(newOpp);  
            }else{
             ////Call Generate Task Method for existing oppty records for schedule
             existingOpptyTaskWrapperList.add(opptyTaskWrapper);
            }
        }
        
        // Loop through new opp list and check SSIP
        try{
            insert newOppList;
            for(Opportunity opp : newOppList){
                for(ClsOpptyTaskWrapper opptyTaskWrapper : opptyTaskWrapperList){
                    if(opptyTaskWrapper.ssipId == opp.SSIP_Schedule__c){
                        opptyTaskWrapper.opportunityId = opp.Id;
                        newOpptyTaskWrapperList.add(opptyTaskWrapper);
                        ssipScheduleToOptyMap.put(opp.SSIP_Schedule__c, opp.Id); //for new Opty
                    }
                }
            }
        }catch (DmlException de) {
            Integer numErrors = de.getNumDml();
            System.debug('getNumDml=' + numErrors);
            for(Integer i=0;i<numErrors;i++) {
                System.debug('getDmlFieldNames=' + de.getDmlFieldNames(i));
                System.debug('getDmlMessage=' + de.getDmlMessage(i));
            }
        }
        //Create Task on Existing Opportunities
        if(!existingOpptyTaskWrapperList.isEmpty()){
            System.debug('****Creating task for existing Oppty************' + opptyTaskWrapperList);
            createTaskOnExistingOpptyRecord(existingOpptyTaskWrapperList);
        }
        //Generate Task for new oppty records on schedule
        if(!newOpptyTaskWrapperList.isEmpty()){//method call to create tasks on newly created opptys
            createTaskOnExistingOpptyRecord(newOpptyTaskWrapperList);
        }
        return ssipScheduleToOptyMap;
    }
    
    //Method will close the Opportunities 
    public static void closeOpportunityforSSIPSch (Set<Id> closeOppIdSet)
    {

        List<Opportunity> closedOpportunitesList = new List<Opportunity>([Select ID,status__c, SSIP_Schedule__c, StageName, CloseDate FROM Opportunity where Id IN: closeOppIdSet]);//List holds the Opportunities, which are closed
        for(Opportunity opp: closedOpportunitesList)
        {
            opp.stagename = '61. Quote Approved';
            opp.status__c = '';
        }
        try{
            update closedOpportunitesList; 
        }
        catch (DmlException de) {
            Integer numErrors = de.getNumDml();
            System.debug('getNumDml=' + numErrors);
            for(Integer i=0;i<numErrors;i++) {
            System.debug('getDmlFieldNames=' + de.getDmlFieldNames(i));
            System.debug('getDmlMessage=' + de.getDmlMessage(i));
            }
        }
        //return closedOpportunitesList;
    }
    
    //Method will reopen the Opportunities 
    public static void reopenOpportunityForSSIP (Set<Id> oppIdSet)
    {
        List<Opportunity> reOpenOpportunitesList = new List<Opportunity>([Select ID,status__c, SSIP_Schedule__c, StageName, CloseDate FROM Opportunity where Id IN: oppIdSet]);//List holds the Opportunities, which are closed
         Set<Id> ssipSchedules = new Set<Id>();
         Map<Id, SSIP_Schedule__c> scheduleDetailsMap = new Map<Id, SSIP_Schedule__c>();
         
         List<Opportunity_Status_Selection__mdt> oppStatusStagesList = new List<Opportunity_Status_Selection__mdt>();
         
         oppStatusStagesList =  [SELECT DeveloperName,Is_BI_Active__c,Is_Credit_Card_Active__c,Stages__c,Is_CMN_Active__c,Is_Pre_Authorized__c,Status__c FROM Opportunity_Status_Selection__mdt];  // Auth_Expired__c,CMN_Expired__c,
                
        System.debug('******* Opty Set ******** ' + oppIdSet);
        for(Opportunity opp: reOpenOpportunitesList){
            ssipSchedules.add(opp.SSIP_Schedule__c);
        }
        for(SSIP_Schedule__c scheduleRec : [SELECT Id, Is_BI_Active__c,Is_Credit_Card_Active__c,Is_CMN_Active__c,Is_Pre_Authorized__c from SSIP_Schedule__c where Id IN :ssipSchedules]){
            scheduleDetailsMap.put(scheduleRec.Id, scheduleRec);
        }
        for(Opportunity opp: reOpenOpportunitesList)
        {
            SSIP_Schedule__c thisSchedule = new SSIP_Schedule__c();
            thisSchedule = scheduleDetailsMap.get(opp.SSIP_Schedule__c);
            
             for(Opportunity_Status_Selection__mdt oss :oppStatusStagesList)
                {
                    if(thisSchedule.Is_BI_Active__c == oss.Is_BI_Active__c && 
                        thisSchedule.Is_CMN_Active__c == oss.Is_CMN_Active__c && 
                        thisSchedule.Is_Credit_Card_Active__c == oss.Is_Credit_Card_Active__c && 
                        thisSchedule.Is_Pre_Authorized__c == oss.Is_Pre_Authorized__c ){
                        
                        system.debug('oss.Stages__c' +oss.Stages__c);
                        system.debug('oss.Status__c' +oss.Status__c);
                        opp.StageName = oss.Stages__c;
                        opp.Status__c = oss.Status__c;
                        opp.CloseDate = System.Today() + 30;
                    }
                }
        }
        try{
            System.debug('****Reopen Opty************' + reOpenOpportunitesList);
            
            update reOpenOpportunitesList; 
        }
        catch (DmlException de) {
            Integer numErrors = de.getNumDml();
            System.debug('getNumDml=' + numErrors);
            for(Integer i=0;i<numErrors;i++) {
            System.debug('getDmlFieldNames=' + de.getDmlFieldNames(i));
            System.debug('getDmlMessage=' + de.getDmlMessage(i));
            }
        }
        
    }
    
    //Create OpptyTaskWrapper
    public static List<ClsOpptyTaskWrapper> getOpptyTaskWrapperList(List<SSIP_Schedule__c> ssipScheduleList, String taskSubject, Id opportunityId,
                                                                   String taskDescription){
        List<ClsOpptyTaskWrapper> opptyTaskWrapperList = new List<ClsOpptyTaskWrapper>();
        
        for(SSIP_Schedule__c ssipScheduleRec : ssipScheduleList){
            if(String.isBlank(taskDescription)){
                taskDescription ='Is BI Active? - '+ ssipScheduleRec.Is_BI_Active__c   +  
                                ', Is CMN Active? - '+ ssipScheduleRec.Is_CMN_Active__c  +
                                ', Is Auth Valid? - '+ ssipScheduleRec.Is_Pre_Authorized__c + 
                                ', Is Credit Card Active? - '+ ssipScheduleRec.Is_Credit_Card_Active__c;
            }
            ClsOpptyTaskWrapper opptyTaskWrapper = new ClsOpptyTaskWrapper();
            opptyTaskWrapper.accountName = ssipScheduleRec.Account__r.Name;
            opptyTaskWrapper.accountTerrirotyCode = ssipScheduleRec.Account__r.Territory_Code__c;
            opptyTaskWrapper.payor = ssipScheduleRec.Payor__c;
            opptyTaskWrapper.accountId = ssipScheduleRec.Account__c;
            opptyTaskWrapper.ssipId = ssipScheduleRec.Id;
            opptyTaskWrapper.isBiActive = ssipScheduleRec.Is_BI_Active__c;
            opptyTaskWrapper.isCmnActive = ssipScheduleRec.Is_CMN_Active__c;
            opptyTaskWrapper.isPreAuthorized = ssipScheduleRec.Is_Pre_Authorized__c;
            opptyTaskWrapper.isCreditCardActive = ssipScheduleRec.Is_Credit_Card_Active__c;
            opptyTaskWrapper.ssipName = ssipScheduleRec.Name;
            opptyTaskWrapper.taskSubject = taskSubject;
            opptyTaskWrapper.opportunityId = opportunityId;
            opptyTaskWrapper.isClosed = ssipScheduleRec.Opportunity__r.isClosed;
            opptyTaskWrapper.taskDescription = taskDescription;
            opptyTaskWrapperList.add(opptyTaskWrapper);
        }
        
        return opptyTaskWrapperList;
    }
    
    /*
     * @Description : This method will create task records on
     * existing opportunity records when Quadax partner Id is not present on
     * benefit or one of the boolean checks - is BI active, Is CMN active, Is preauthorized
     * or is credit Card active is false on ssip
    */
    public static void createTaskOnExistingOpptyRecord(List<ClsOpptyTaskWrapper> opptyTaskWrapperList){
        List<Task> taskListToInsert = new List<Task>();
        Set<Id> openClosedOpty = new Set<Id>(); //it will reopen closed Opportunities.
        Id taskRecTypeId = ClsApexUtility.getRecordTypeIdByDeveloperName(ClsApexConstants.TASK_SOBJECT_API, ClsApexConstants.RECORD_TYPE_DEVELOPER_API_NAME);
        //get wrapper list as well since we are determining the calling point from wrapper
        for(ClsOpptyTaskWrapper opptyTaskWrapper : opptyTaskWrapperList){
            if(opptyTaskWrapper.ssipId != null &&
               opptyTaskWrapper.opportunityId !=null){
                   Task newTask = new Task();
                   newTask.Type = 'SSIP Notes';
                   newTask.RecordTypeId = taskRecTypeId;
                   newTask.WhatId = opptyTaskWrapper.opportunityId;
                   newTask.Subject = opptyTaskWrapper.taskSubject + opptyTaskWrapper.ssipName;
                   newTask.Description = opptyTaskWrapper.taskDescription;
                   taskListToInsert.add(newTask);
                   System.debug('******* Closed Opty ******** ' + opptyTaskWrapper.isClosed);
                   if(opptyTaskWrapper.isClosed){
                       openClosedOpty.add(opptyTaskWrapper.opportunityId);
                   }
               }
        }
        System.debug('****taskListToInsert ************' + taskListToInsert);
        if(!openClosedOpty.isEmpty()){
            System.debug('******* Closed Opty Set ******** ' + openClosedOpty);
            
            reopenOpportunityForSSIP(openClosedOpty);
        }
        if(!taskListToInsert.isEmpty()){
            try{
                insert taskListToInsert;
            }catch(DmlException e){
                Integer numErrors = e.getNumDml();
                System.debug('getNumDml=' + numErrors);
                for(Integer i=0;i<numErrors;i++) {
                    System.debug('getDmlFieldNames=' + e.getDmlFieldNames(i));
                    System.debug('getDmlMessage=' + e.getDmlMessage(i));
                }
            }
        }
    }
    
    public without sharing class ClsOpptyTaskWrapper {
        
        public String accountName;
        public String accountTerrirotyCode;
        public Id payor;
        public Id accountId;
        public Id ssipId;
        public Id opportunityId;
        public String taskSubject;
        public Boolean isBiActive;
        public Boolean isCmnActive;
        public Boolean isPreAuthorized;
        public Boolean isCreditCardActive;
        public String ssipName;
        public String taskDescription;
        public Boolean isClosed;
    }
}