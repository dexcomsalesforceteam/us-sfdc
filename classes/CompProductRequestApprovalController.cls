/*
 * Class modified for CRMSF-4568
*/
public with sharing class CompProductRequestApprovalController {
    
    /*
     * getPendingCompProdRequests - this function will get list of 
     * Comp_Product_Request__c object which are pending for approval
     * @Author - LTI
     * Created for CRMSF-4568
	*/
    @AuraEnabled
    public static List<Comp_Product_Request__c> getPendingCompProdRequests(){
        List<Comp_Product_Request__c> compProductReqList = new List<Comp_Product_Request__c>();
        List<Id> pendingSobjectRecordList = getPendingRecordId();
        compProductReqList = [SELECT Id, Name,Customer_Account_No__c, Error_Source__c,Estimated_Amount__c,Error_Made_By__c, Customer_Account_No__r.Payor__r.Name,
                              Customer_Account_No__r.Payor__c,Customer_Account_No__r.Name,Error_Made_By__r.Name,Territory__c
                              FROM Comp_Product_Request__c WHERE Id IN :pendingSobjectRecordList];
        return compProductReqList;
    }
    
    /*
     * getPendingRecordId - This method will
     * fetch list of Comp Product Request Ids
     * which are pending for approval
     * Created for CRMSF-4568
	*/
    public static List<Id> getPendingRecordId(){
        Set<Id> actorIdSet = new Set<Id>();
        List<Id> pendingSobjectRecordList = new List<Id>();
        actorIdSet = getActorIdSet();
        for(ProcessInstanceWorkItem item : [SELECT ProcessInstance.TargetObjectId FROM ProcessInstanceWorkItem WHERE ProcessInstance.Status = : ClsApexConstants.STATUS_PENDING AND ActorId IN :actorIdSet]){ 
            pendingSobjectRecordList.add(item.ProcessInstance.TargetObjectId);
        }
        return pendingSobjectRecordList;
    }
    
    /*
     * Function to return Set of queue/user Ids
     * who are responsible for approvals
	*/
    public static Set<Id> getActorIdSet(){
        Set<Id> actorIdSet = new Set<Id>();
        Id userId = UserInfo.getUserId();
        actorIdSet.add(userId);
        
        for(GroupMember groupMember : [SELECT Group.Id FROM GroupMember WHERE UserOrGroupId = :userId AND Group.Type = : ClsApexConstants.QUEUE]){
            actorIdSet.add(groupMember.Group.Id);
        }
        return actorIdSet;
    }
    
    /*
     * This method will process the records to approve
     * or reject them based on the boolean value passed from
     * lightning component
     * True - Record List Approve
     * False - Record List Reject
     * Created for CRMSF-4568
	*/
    @AuraEnabled
    public static void approveRejectRecords(Boolean approveOrReject, List<Id> approveRejectRecordIdList){
        List<Approval.ProcessWorkitemRequest> approvalRejectionList = new List<Approval.ProcessWorkitemRequest>();
        Set<Id> actorIdSet = new Set<Id>();
        actorIdSet = getActorIdSet();
        for(ProcessInstanceWorkitem workItem  : [SELECT id FROM ProcessInstanceWorkItem WHERE ProcessInstance.Status = : ClsApexConstants.STATUS_PENDING AND ProcessInstance.TargetObjectId in : approveRejectRecordIdList AND ActorId IN :actorIdSet]){
            if(!approveOrReject){
                Approval.ProcessWorkitemRequest requestItem = new Approval.ProcessWorkitemRequest();
                requestItem.setAction(ClsApexConstants.STRING_REJECT);
                requestItem.setWorkitemId(workItem.id); 
                requestItem.setComments(ClsApexConstants.REJECTION_COMMENTS); 
                approvalRejectionList.add(requestItem);
            }else{
                Approval.ProcessWorkitemRequest requestItem = new Approval.ProcessWorkitemRequest();
                requestItem.setAction(ClsApexConstants.STRING_APPROVE); 
                requestItem.setWorkitemId(workItem.id); 
                requestItem.setComments(ClsApexConstants.APPROVAL_COMMENTS); 
                approvalRejectionList.add(requestItem);
            }
        }
        List<Approval.ProcessResult> processResultList =  Approval.process(approvalRejectionList);
    }
    
}