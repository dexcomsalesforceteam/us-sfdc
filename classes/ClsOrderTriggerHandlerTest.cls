@isTest(seeAllData=false)
public class ClsOrderTriggerHandlerTest {
    
    @testSetup
    public static void setupTestData(){
        List<Account> accountInsertList = new List<Account>();
        //prescriber
        Account prescriberAccount=new Account();
        prescriberAccount.lastname='prescriber Account';
        prescriberAccount.recordtypeId= ClsApexUtility.getRecordTypeIdByDeveloperName('Account', 'Prescriber');
        prescriberAccount.Inactive__c =false;        
        accountInsertList.add(prescriberAccount);
        
        //Payor
        Account payorAccount = new Account();
        payorAccount.recordtypeId= ClsApexUtility.getRecordTypeIdByDeveloperName('Account', 'Payor');
        payorAccount.Name = 'Payor';
        payorAccount.Skip_Payor_Matrix__c = true;
        accountInsertList.add(payorAccount);
        Database.insert(accountInsertList);              
        
        //patient
        Account testAccount=TestDataBuilder.testAccount();
        testAccount.Default_Price_Book__c=Test.getStandardPricebookId();
        testAccount.PersonEmail = 'Test@gmail.comOrderTrigHandler';
        testAccount.BillingStreet='abc';
        testAccount.BillingCity ='Bang';
        testAccount.BillingState ='KN';
        testAccount.BillingPostalCode='56003';
        testAccount.BillingCountry='US';
        testAccount.ShippingStreet  ='Bang';
        testAccount.ShippingCity='Bang';
        testAccount.ShippingState='KN';
        testAccount.ShippingCountry='US';
        testAccount.ShippingPostalCode='56003';
        testAccount.Party_ID__c ='123344';
        testAccount.AccountNumber ='98765';
        testAccount.Prescribers__c = prescriberAccount.Id;
        testAccount.Payor__c = payorAccount.Id;
        testAccount.Receiver_Quantity_Prescribed__c = 10;
        testAccount.Sensor_Quantity_Prescribed__c = 10;
        testAccount.Transmitter_Quantity_Prescribed__c = 10;
        testAccount.RecordTypeId = ClsApexUtility.getRecordTypeIdByDeveloperName('Account', 'Consumers');
        insert testAccount;
        //Primary_Benefit__c
        Benefits__c benefitObj = new Benefits__c();
        benefitObj.PRIOR_AUTH_REQUIRED__c = 'Y';
        benefitObj.New_Order_Auth_Products__c = 'Receiver;Transmitter;Sensor';
        benefitObj.Reorder_Auth_Products__c = 'Receiver;Transmitter;Sensor';
        benefitObj.Account__c = testAccount.Id;
        benefitObj.Payor__c = payorAccount.Id;
        benefitObj.Coverage__c=80;
        benefitObj.INDIVIDUAL_DEDUCTIBLE__c=200; benefitObj.INDIVIDUAL_MET__c=0; benefitObj.Individual_Deductible_Remaining__c=200;
        benefitObj.INDIVIDUAL_OOP_MAX__c=1500; benefitObj.INDIVIDUAL_OOP_MET__c=0; benefitObj.Individual_OOP_Remaining__c=1500;        
        benefitObj.FAMILY_DEDUCT__c=500; benefitObj.Family_Met__c=0; benefitObj.Family_Deductible_Remaining__c=500;
        benefitObj.FAMILY_OOP_MAX__c=2000; benefitObj.FAMILY_OOP_MET__c=0; benefitObj.Family_OOP_Remaining__c=2000;
        benefitObj.CO_PAY__c = 10;
        insert benefitObj;
        
        
        List<Address__c> addrList = new List<Address__c>();
        Address__c addrs1 = TestDataBuilder.getAddressList(testAccount.Id, true,'SHIP_TO', 1)[0];
        addrs1.Oracle_Address_ID__c ='123455';
        addrs1.Address_Verified__c = 'Yes';
        addrList.add(addrs1);
        Address__c addrs2 = TestDataBuilder.getAddressList(testAccount.Id, true,'BILL_TO', 1)[0];
        addrs2.Oracle_Address_ID__c ='123465';
        addrs2.Address_Verified__c = 'Yes';
        addrList.add(addrs2);
        Database.insert(addrList);
        
        testAccount.Primary_Benefit__c = benefitObj.Id;
        testAccount.Primary_Bill_To_Address__c = addrs2.Id;
        testAccount.Primary_Ship_To_Address__c = addrs1.Id;
        update testAccount;
        Test.startTest();
        //create pb
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        //create product
        Product2 prod1 = new Product2();
        prod1.Name = 'STT-MC-001';
        prod1.Description = 'STT-MC-001';
        prod1.IsActive = TRUE;
        insert prod1;
        //create pricebook entry
        List<PricebookEntry> pricebookEntryList = new List<PricebookEntry>();
        /*PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.Product2Id = prod1.Id;
        pbEntry.Pricebook2Id = customPB.Id;
        pbEntry.IsActive = true;
        pbEntry.UnitPrice = 10;
        pbEntry.UseStandardPrice = false;
        pricebookEntryList.add(pbEntry);
        */
        Id standardId= Test.getStandardPricebookId();
        PricebookEntry pbEntry2 = new PricebookEntry();
        pbEntry2.Product2Id = prod1.Id;
        pbEntry2.Pricebook2Id = standardId;
        pbEntry2.IsActive = true;
        pbEntry2.UnitPrice = 100;
        pbEntry2.UseStandardPrice = false;
        pricebookEntryList.add(pbEntry2);
        database.insert(pricebookEntryList);
        Test.stopTest();
    }
    
    @isTest
    public static void beforeInsertTest1(){
        Account account1 = [Select Id, Primary_Ship_To_Address__c from Account where PersonEmail = 'Test@gmail.comOrderTrigHandler' limit 1];
        //Address__c addr1 = [Select Id from Address__c where Address_Type__c='SHIP_TO' limit 1];
        Test.startTest();
        Order orderObj = new Order();
        orderObj.AccountId = account1.Id;
        orderObj.Type = 'Standard Sales Order';
        orderObj.Shipping_Address__c = account1.Primary_Ship_To_Address__c;
        orderObj.Status = 'Draft';
        orderObj.EffectiveDate = system.today()-10;
        orderObj.Price_Book__c = Test.getStandardPricebookId();
        insert orderObj;
        
        //ClsOrderTriggerHandler.closeOpportunityforOrder(setOppid);
        Test.stopTest();
    }
    
    @isTest
    public static void testAfterUpdate(){
        Account account1 = [Select Id,Primary_Benefit__c, Primary_Ship_To_Address__c from Account where PersonEmail = 'Test@gmail.comOrderTrigHandler' limit 1];
        //Address__c addr1 = [Select Id from Address__c where Address_Type__c='SHIP_TO' limit 1];
        PricebookEntry pbEntry = [Select Id,Pricebook2Id,Product2Id from PricebookEntry where Pricebook2Id= : Test.getStandardPricebookId() limit 1];
        Test.startTest();
        Order orderObj = new Order();
        orderObj.AccountId = account1.Id;
        orderObj.Type = 'Standard Sales Order';
        orderObj.Shipping_Address__c = account1.Primary_Ship_To_Address__c;
        orderObj.Shipping_Method__c = '000001_FEDEX_A_2D';
        orderObj.Status = 'Draft';
        orderObj.EffectiveDate = system.today()-10;
        orderObj.Price_Book__c = Test.getStandardPricebookId();
        orderObj.Pricebook2Id = Test.getStandardPricebookId();
        orderObj.Scheduled_Ship_Date__c=Date.today();
        orderObj.Tax_Exempt__c = True;
        insert orderObj;
        Benefits__c ben= [SELECT Id, Name, Benefit_Hierarchy__c, Payor__c, Start_Date__c, LastModifiedById, CO_PAY__c, INDIVIDUAL_DEDUCTIBLE__c, INDIVIDUAL_MET__c, Family_Met__c, INDIVIDUAL_OOP_MAX__c, FAMILY_DEDUCT__c, FAMILY_OOP_MAX__c, Coverage__c FROM Benefits__c Where Id=:account1.Primary_Benefit__c Limit 1];
        
        OrderItem orderItem = new OrderItem();
        orderItem.Quantity = 1;
        orderItem.OrderId = orderObj.Id;
        orderItem.PricebookEntryId = pbEntry.Id;
        orderItem.Product2Id = pbEntry.Product2Id;
        orderItem.UnitPrice = 100;
        insert orderItem;
        
        Set<Id> orderIdSet = new set<Id>(); orderIdSet.add(orderObj.Id);
        Set<Id> setAccId = new set<Id>();   setAccId.add(account1.Id);
        ClsOrderTriggerHandler.CalculateCopay1(orderIdSet, setAccId);
        CalculateCoPay.CalcCoPayByAccount(account1.Id, 700);        
        
        ben.INDIVIDUAL_DEDUCTIBLE__c=0; ben.INDIVIDUAL_MET__c=0; ben.Individual_Deductible_Remaining__c=0;
        ben.INDIVIDUAL_OOP_MAX__c=1500; ben.INDIVIDUAL_OOP_MET__c=0; ben.Individual_OOP_Remaining__c=1500;
        ben.FAMILY_DEDUCT__c=0; ben.Family_Met__c=0; ben.Family_Deductible_Remaining__c=500;
        ben.FAMILY_OOP_MAX__c=0; ben.FAMILY_OOP_MET__c=0; ben.Family_OOP_Remaining__c=2000; ben.Is_Update_In_Progress__c =false;
        update ben;
        CtrlBICalculator.getEstimatedCoPay(account1.Id, 700);
        ClsSvcEstimatedCoPayReq.parse('{"req": {"products":[{"sku":"1111","qty":1,"price":300},{"sku":"2222","qty":1,"price":400}],"accountId":"0013300001bLWi6"}}');
        ClsSvcEstimatedCoPayRes.parse('{"status":null,"message":"ok","data":{"estimatedCost":140.00}}');
           
        //ClsOrderTriggerHandler.CalculateTax(orderIdSet);
        orderObj.Status = 'Activated';
        orderObj.Sub_Type__c = 'Comp Product Order';
        orderObj.Unique_Identifier__c = '12346789567uhgf';
        orderObj.Force_Calc_Copay_Tax__c=true;
        orderObj.Tax_Exempt__c = False;
        orderObj.Scheduled_Ship_Date__c=Date.today().addDays(+2);
         ClsOrderTriggerHandler.isExecuting = false;
        update orderObj;
        Test.stopTest();
    }
}