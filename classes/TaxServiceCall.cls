/***************************************
* @author      : Noy De Goma
* @date        : FEB 08, 2015
* @description : Class to invoke webservice that calculates tax amount
***************************************/
public class TaxServiceCall {
    public static map<String, String> getCalloutResponseContents(String endpoint, String reqBody){
        map<String, String> results;   
        try{
            
            system.debug('Request body is ' + reqBody);
            //Make up the Callout URL
            String calloutURL = 'callout:TaxCalculator';
            calloutURL = calloutURL;
            system.debug('Callout URL is ' + calloutURL);

            //Http invocation to call the TaxCalculator service
            Http http = new Http();
            HttpRequest httpRequest = new HttpRequest();
            httpRequest.setEndpoint(calloutURL);
            httpRequest.setMethod('POST');
            httpRequest.setbody(reqBody);
            httpRequest.setTimeout(120000);
            httpRequest.setHeader('Content-Type', 'application/json');
            httpRequest.setHeader('Content-Length',String.valueof(reqBody.length()));
            System.debug('***\n httpRequest = ' + httpRequest + '\n');
            HttpResponse res = http.send(httpRequest);
            System.debug(res.toString());
            System.debug('STATUS:'+res.getStatus());
            System.debug('STATUS_CODE:'+res.getStatusCode()); 
            System.debug('BODY:'+res.getBody()); 
            
            if(res.getStatus() == 'OK'){
                results = (map<String, String>)JSON.deserialize(res.getBody(), map<String, String>.class);
                if(results.get('ResponseCode') == '1'){
                    
                }else{
                    System.debug('===========================>>>>>>>>>>>>>>>>>>>>>tax ' + results.get('Message'));
                }
            }else{
                System.debug('==== No Results Were Found ====');
                results = new map<String, String>();
                results.put('STATUS', res.getStatus());
            }
        }catch(Exception e){
            System.debug('EXCEPTION IN CALLOUT:'+e.getMessage());
        }
        return results;
    }
}