global class BClsProcessTerritoryAlignmentChanges implements Database.Batchable<sobject>{
/*****************************************************************************************************************
@Author        : Jagan Periyakaruppan
@Date Modified    : 11/20/2017
@Description    : This Batch class will be invoked from TerritoryAlignmentTriggerHandler to process Roster changes
@Author        : Andrew Akre
@Date Modified    : 9/10/2018
@Description    : Added and removed certain fields grabbed from the Territory Alignment object
********************************************************************************************************************/   
    //Variable declarations
    public Set<Id> territoryIdsToProcessSet = new Set<Id>(); //Ids to hold Territory Ids to process
    Map<Id, Map<String, String>> terrAlignmentFieldsMap = new Map<Id, Map<String, String>>();////Map holds the details of Territory alignment records
    
    //Constructor
    public BClsProcessTerritoryAlignmentChanges (Set<Id> territoryIds)
    {
        territoryIdsToProcessSet = territoryIds;
        //Get the territory alignment details
        for(Territory_Alignment__c terrAlign : [SELECT Id, Fax__c, AS_Email__c, AS_Direct_Line__c, PCS__c, RSS__c, RSS_RingDNA_Number__c, RSS_Email__c, Medicare_RSS__c, Medicare_RSS_RingDNA_Number__c, Supervisor__c FROM Territory_Alignment__c WHERE ID IN : territoryIdsToProcessSet])
        {
            Map<String, String> tempTerrAlignFieldsMap = new Map<String, String>();
            tempTerrAlignFieldsMap.put('Territory_AS__c', terrAlign.PCS__c);
            tempTerrAlignFieldsMap.put('Territory_Fax__c', terrAlign.Fax__c);
            tempTerrAlignFieldsMap.put('Territory_ISR_RingDNA__c', terrAlign.AS_Direct_Line__c);
            tempTerrAlignFieldsMap.put('Territory_ISR_Email__c', terrAlign.AS_Email__c);
            tempTerrAlignFieldsMap.put('Territory_RSS_RingDNA__c', terrAlign.RSS_RingDNA_Number__c);
            tempTerrAlignFieldsMap.put('Territory_RSS_Email__c', terrAlign.RSS_Email__c);
            tempTerrAlignFieldsMap.put('Territory_RSS__c', terrAlign.RSS__c);
            tempTerrAlignFieldsMap.put('Territory_Medicare_RSS__c', terrAlign.Medicare_RSS__c);
            tempTerrAlignFieldsMap.put('Territory_Medicare_RSS_RingDNA__c', terrAlign.Medicare_RSS_RingDNA_Number__c);
            tempTerrAlignFieldsMap.put('Territory_Supervisor__c', terrAlign.Supervisor__c);
            terrAlignmentFieldsMap.put(terrAlign.Id, tempTerrAlignFieldsMap);
        }
    }
    //Get all the accounts, which are affected by the territory alignment changes on the Roster object
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        String query = 'SELECT Territory_ID_Lookup__c, Territory_AS__c, Territory_Fax__c, Territory_ISR_RingDNA__c, Territory_ISR_Email__c, Territory_RSS_RingDNA__c, Territory_RSS_Email__c, Territory_RSS__c, Territory_Medicare_RSS__c, Territory_Medicare_RSS_RingDNA__c, Territory_Supervisor__c FROM Account WHERE Territory_ID_Lookup__c IN : territoryIdsToProcessSet';
        return Database.getQueryLocator(query);
    }
    
    //Execute the Account records in batch
    global void execute(Database.BatchableContext BC, List<Account> scope)
    {
        //Get the accounts that are to be processed
        List<Account> accountsToBeUpdated = new List<Account>();
        
        //Process Accounts
        for(Account acc : scope)
        {
            acc.Territory_AS__c = terrAlignmentFieldsMap.get(acc.Territory_ID_Lookup__c).get('Territory_AS__c');
            acc.Territory_Fax__c = terrAlignmentFieldsMap.get(acc.Territory_ID_Lookup__c).get('Territory_Fax__c');
            acc.Territory_ISR_RingDNA__c = terrAlignmentFieldsMap.get(acc.Territory_ID_Lookup__c).get('Territory_ISR_RingDNA__c');
            acc.Territory_ISR_Email__c = terrAlignmentFieldsMap.get(acc.Territory_ID_Lookup__c).get('Territory_ISR_Email__c');
            acc.Territory_RSS_RingDNA__c = terrAlignmentFieldsMap.get(acc.Territory_ID_Lookup__c).get('Territory_RSS_RingDNA__c');
            acc.Territory_RSS_Email__c = terrAlignmentFieldsMap.get(acc.Territory_ID_Lookup__c).get('Territory_RSS_Email__c');
            acc.Territory_RSS__c = terrAlignmentFieldsMap.get(acc.Territory_ID_Lookup__c).get('Territory_RSS__c');
            acc.Territory_Medicare_RSS__c = terrAlignmentFieldsMap.get(acc.Territory_ID_Lookup__c).get('Territory_Medicare_RSS__c');
            acc.Territory_Medicare_RSS_RingDNA__c = terrAlignmentFieldsMap.get(acc.Territory_ID_Lookup__c).get('Territory_Medicare_RSS_RingDNA__c');
            acc.Territory_Supervisor__c = terrAlignmentFieldsMap.get(acc.Territory_ID_Lookup__c).get('Territory_Supervisor__c');
            accountsToBeUpdated.add(acc);
        }
        
        if(!accountsToBeUpdated.isEmpty())
        {
            try{
                update accountsToBeUpdated;
            }catch (Exception e){
                system.debug('***ACCOUNT TERRITORY INFORMATION UPDATE ERROR = ' + e.getMessage());
            }
        }
    }
    
    //Finish Method
    global void finish(Database.BatchableContext BC)
    {
      
    }
}