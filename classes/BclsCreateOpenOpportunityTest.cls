@isTest
public class BclsCreateOpenOpportunityTest{
    @isTest
    public static void verifyOppCreation(){
        
        Test.StartTest();   
        
        Account mdcrPayor = new Account();
        mdcrPayor.RecordTypeId = ClsApexUtility.getRecordTypeIdByDeveloperName('Account', 'Payor');
        mdcrPayor.Name = 'MDCR Test Payor';
        mdcrPayor.Payor_Code__c = 'K';
        mdcrPayor.Days_For_Sensor_3_Box_Shipment__c  =8;
        insert mdcrPayor;
        
        Account mdcrAccnt = new Account();
        mdcrAccnt.FirstName = 'MDCR FirstName';
        mdcrAccnt.LastName = 'MDCR LastName';
        mdcrAccnt.Customer_Type__c = 'Medicare Advantage';
        mdcrAccnt.Payor__c = mdcrPayor.id;
        mdcrAccnt.RecordtypeId = ClsApexUtility.getRecordTypeIdByDeveloperName('Account', 'Consumers');
        mdcrAccnt.DOB__c =Date.newInstance(1992, 04, 23);
        mdcrAccnt.Med_Adv_Override__c = true;
        
        mdcrAccnt.Chart_Notes_Expiration_Date__c = system.Today()+5;
        mdcrAccnt.MDCR_FFS_Subscription__c = 'Active';
        insert mdcrAccnt;
        system.debug('***AccountFields***' + mdcrAccnt);
        mdcrAccnt.Chart_Notes_Expiration_Date__c = system.Today()+5;
        update mdcrAccnt;
        
        
        
        list <Account> acc = new list<Account>();
        acc = [SELECT Id, Name,Consumer_Payor_Code__c ,Active_Opportunity__c,Customer_Type__c,Med_Adv_Override__c, Chart_Notes_Expiration_Date__c, Payor__c,Is_Chart_Notes_Actve__c ,Create_Proactive_Med_Docs_Opp__c,Create_Proactive_Prior_Auth_Opp__c,G6_Upgrade_Est_Followup_Ship_Date__c FROM Account WHERE Id =: mdcrAccnt.id];
        system.debug('***List***'+acc);
        
        BclsCreateOpenOpportunity newCreateOpenOpportunity = new BclsCreateOpenOpportunity();
        Database.executeBatch(newCreateOpenOpportunity);
        
        Test.StopTest();
        
    }
    
    @isTest
    private static void bclsCreateOpenOpptySchedTest(){
        Test.startTest();
        BclsCreateOpenOpportunitySched scheduler = new BclsCreateOpenOpportunitySched();
        String sch = '0 0 23 * * ?';
        system.schedule('Bclsopen oppty sched', sch, scheduler);
        Test.stopTest();
    }
}