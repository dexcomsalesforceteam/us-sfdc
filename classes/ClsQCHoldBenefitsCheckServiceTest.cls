@isTest
public class ClsQCHoldBenefitsCheckServiceTest {

    @testSetUp
    public static void setUpTestData(){

        List<Account> accountInsertList = new List<Account>();
        //prescriber
        Account prescriberAccount=new Account();
        prescriberAccount.lastname='prescriber Account';
        prescriberAccount.recordtypeId= ClsApexUtility.getRecordTypeIdByDeveloperName('Account', 'Prescriber');
        prescriberAccount.Inactive__c =false;        
        accountInsertList.add(prescriberAccount);
        
        //Payor
        Account payorAccount = new Account();
        payorAccount.recordtypeId= ClsApexUtility.getRecordTypeIdByDeveloperName('Account', 'Payor');
        payorAccount.Name = 'Payor';
        payorAccount.Skip_Payor_Matrix__c = true;
        accountInsertList.add(payorAccount);
        Database.insert(accountInsertList);              
        
        //patient
        Account testAccount=TestDataBuilder.testAccount();
        testAccount.Default_Price_Book__c=Test.getStandardPricebookId();
        testAccount.PersonEmail = 'Test@gmail.comQCHoldBenCheck';
        testAccount.BillingStreet='abc';
        testAccount.BillingCity ='Bang';
        testAccount.BillingState ='KN';
        testAccount.BillingCountry ='US';
        testAccount.BillingPostalCode='560037';
        testAccount.ShippingStreet  ='Bang';
        testAccount.ShippingCity='Bang';
        testAccount.ShippingState='KN';
        testAccount.ShippingCountry='US';
        testAccount.ShippingPostalCode='560037';
        testAccount.Party_ID__c ='123344';
        testAccount.Prescribers__c = prescriberAccount.Id;
        testAccount.Payor__c = payorAccount.Id;
        testAccount.RecordTypeId = ClsApexUtility.getRecordTypeIdByDeveloperName('Account', 'Consumers');
        insert testAccount;
        
        //Primary_Benefit__c
        Benefits__c benefitObj = new Benefits__c();
        benefitObj.PRIOR_AUTH_REQUIRED__c = 'Y';
        benefitObj.Reorder_Auth_Products__c = 'Receiver;Transmitter;Sensor';
        benefitObj.Account__c = testAccount.Id;
        benefitObj.Payor__c = payorAccount.Id;
        benefitObj.Last_Benefits_Check_Date__c = system.today()-1;
        benefitObj.Last_Benefits_Check_Done_By__c = 'TestPerson';
        benefitObj.PRIOR_AUTH_REQUIRED__c = 'Y';
        insert benefitObj;
        
        List<Address__c> addrList = new List<Address__c>();
        Address__c addrs1 = TestDataBuilder.getAddressList(testAccount.Id, true,'SHIP_TO', 1)[0];
        addrs1.Oracle_Address_ID__c ='123455';
        addrs1.Address_Verified__c = 'Yes';
        addrList.add(addrs1);
        Address__c addrs2 = TestDataBuilder.getAddressList(testAccount.Id, true,'BILL_TO', 1)[0];
        addrs2.Oracle_Address_ID__c ='123465';
        addrs2.Address_Verified__c = 'Yes';
        addrList.add(addrs2);
        Test.startTest();
        Database.insert(addrList);
        
        testAccount.Primary_Benefit__c = benefitObj.Id;
        testAccount.Primary_Bill_To_Address__c = addrs2.Id;
        testAccount.Primary_Ship_To_Address__c = addrs1.Id;
        update testAccount;
        
        Order orderObj = new Order();
        orderObj.AccountId = testAccount.Id;
        orderObj.Type = 'Standard Sales Order';
        orderObj.Shipping_Address__c = addrs1.Id;
        orderObj.Status = 'Draft';
        orderObj.EffectiveDate = system.today()-10;
        insert orderObj;
        Test.stopTest();
    }
    
    @isTest
    public static void getBIDetailsTest1(){
        Order orderRec = [Select Id,AccountId from Order where Account.PersonEmail = 'Test@gmail.comQCHoldBenCheck' and Type = 'Standard Sales Order' limit 1];
        Test.startTest();
        ClsQCHoldBenefitsCheckService.getBIDetails(String.valueOf(orderRec.Id), String.valueOf(orderRec.AccountId), 'New Order', true);
        Test.stopTest();
    }
    
    @isTest
    public static void getBIDetailsTest2(){
        Order orderRec = [Select Id,AccountId from Order where Account.PersonEmail = 'Test@gmail.comQCHoldBenCheck' and Type = 'Standard Sales Order' limit 1];
        Benefits__c benefitObj = [Select Id, PRIOR_AUTH_REQUIRED__c, Prior_auth_Denied_or_Approved__c, PA_REFERENCE_NUM__c, PA_SPOKE_WITH__c, RECEIVER_AUTHORIZATION__c,
                                 TRANSMITTER_AUTH_NUM__c,TRANSMITTER_AUTH_START_DATE__c,RECEIVER_AUTH_END_DATE__c,RECEIVER_AUTH_START_DATE__c, New_Order_Auth_Products__c,
                                 Sensor_Unit_Authorization__c,Is_Update_In_Progress__c, SENSOR_AUTH_START_DATE__c, SENSOR_AUTH_END_DATE__c, Sensor_Units_Authorized__c from Benefits__c
                                 where Account__c = : orderRec.AccountId];
        benefitObj.Prior_auth_Denied_or_Approved__c = 'APPROVED';
        benefitObj.PRIOR_AUTH_REQUIRED__c = 'Y';
        benefitObj.PA_REFERENCE_NUM__c = 'Test12';
        benefitObj.PA_SPOKE_WITH__c = '24124YUT';
        benefitObj.RECEIVER_AUTHORIZATION__c = 'test14';
        benefitObj.TRANSMITTER_AUTH_NUM__c = '235';
        benefitObj.TRANSMITTER_AUTH_END_DATE__c = system.today();
        benefitObj.TRANSMITTER_AUTH_START_DATE__c = system.today();
        benefitObj.RECEIVER_AUTH_END_DATE__c = system.today();
        benefitObj.RECEIVER_AUTH_START_DATE__c = system.today();
        benefitObj.Sensor_Unit_Authorization__c = '235';
        benefitObj.SENSOR_AUTH_START_DATE__c = system.today();
        benefitObj.SENSOR_AUTH_END_DATE__c = system.today();
        benefitObj.Sensor_Units_Authorized__c = 10;
        benefitObj.New_Order_Auth_Products__c = 'Receiver;Transmitter;Sensor';
        benefitObj.Is_Update_In_Progress__c = ClsApexConstants.BOOLEAN_FALSE;
        update benefitObj;
        
        Test.startTest();
        ClsQCHoldBenefitsCheckService.getBIDetails(String.valueOf(orderRec.Id), String.valueOf(orderRec.AccountId), 'New Order', false);
        Test.stopTest();
    }
    
    @isTest
    public static void getBIDetailsTest3(){
        Order orderRec = [Select Id,AccountId from Order where Account.PersonEmail = 'Test@gmail.comQCHoldBenCheck' and Type = 'Standard Sales Order' limit 1];
        Test.startTest();
        ClsQCHoldBenefitsCheckService.getBIDetails(String.valueOf(orderRec.Id), String.valueOf(orderRec.AccountId), 'Reorder', false);
        Test.stopTest();
        
    }
}