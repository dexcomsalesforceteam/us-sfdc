/****************************************************************************
*@Author        : Jagan Periyakaruppan
*@Date Created  : 31-Aug-2018
*@Description   : Retrieves and Process the credit card data for a customer
****************************************************************************/
public class CtrlCustomerCreditCardInformation{
    @AuraEnabled
    //Method will retrieve all active Credit Cards for a given accountnumber
    public static List<Object> getCreditCardDetails  (String accountNumber) {
        //Initialize variables
        List<Object> returnListOfCreditCards = new List<Object>();
        ClsConvertCCJSONPayloadToApex ccJSONToApex = new ClsConvertCCJSONPayloadToApex();
        ClsConvertCCJSONPayloadToApex.Result ccDetail = new ClsConvertCCJSONPayloadToApex.Result();
        
        //Make up the Callout URL
        String calloutURL = 'callout:CustomerCreditCardDetails';
        calloutURL = calloutURL + '?accountNumber=' + accountNumber;
        system.debug('Callout URL is ' + calloutURL);
        
        //Http invocation to call the Credit Card Service
        Http http = new Http();
        HttpRequest httpRequest = new HttpRequest();
        httpRequest.setEndpoint(calloutURL);
        httpRequest.setMethod('GET');
        HttpResponse httpResponse = http.send(httpRequest);
        
        // If the httpRequest is successful, parse the JSON httpResponse.
        if (httpResponse.getStatusCode() == 200) {
            ccJSONToApex = ClsConvertCCJSONPayloadToApex.parse(httpResponse.getBody());
            //List<ClsConvertCCJSONPayloadToApex.Result> resultList = ccJSONToApex.result;
            //system.debug('Name is ' + resultList[0].name);
            //system.debug('Count is ' + ccJSONToApex.count);
            if(ccJSONToApex.count > 0)
                returnListOfCreditCards.addAll(ccJSONToApex.result);
        }
        return returnListOfCreditCards;
    }
    
    @AuraEnabled
    //Method will set the credit card to Primar for an account
    public static String setPrimaryCardForCustomer  (String accountNumber, String cardId) {
        //Initialize variables
        String processComplete;
        
        //Make up the Callout URL
        String calloutURL = 'callout:CustomerCreditCardDetails/';
        calloutURL = calloutURL + cardId + '?accountNumber=' + accountNumber;
        
        system.debug('Callout URL is ' + calloutURL);
        
        //Http invocation to call the Credit Card Service
        Http http = new Http();
        HttpRequest httpRequest = new HttpRequest();
        httpRequest.setEndpoint(calloutURL);
        httpRequest.setMethod('PUT');
        HttpResponse httpResponse = http.send(httpRequest);
        
        // If the httpRequest is successful, parse the JSON httpResponse.
        if (httpResponse.getStatusCode() == 200) {
            processComplete = 'Success';
        }
        return processComplete;
    }
    
    //updating credit card number on order record.
    @auraEnabled
    public static void updateCreditCard(string orderId,string cardNumber){
        
        
        Order_Header__c ordHeader=[select id,Credit_Card_Number_Last_4__c,Order_ID__c,Integration_Error_Message__c,Auth_Code__c,order__r.OrderNumber,order__r.OrderReferenceNumber,Oracle_Address_Id__c from Order_Header__c where id =:orderId];
        
        string  body =  '{"Order": {"OrderNumber": "'+ordHeader.Order_ID__c+'","CardId": "'+cardNumber+'"}}';
        
        String calloutURL = 'callout:RechargeCustomerCreditCard';
        
      //Http invocation to call the Credit Card Service
        Http http = new Http();
        HttpRequest httpRequest = new HttpRequest();
        httpRequest.setEndpoint(calloutURL);
        httpRequest.setMethod('POST');
        httpRequest.setBody(body);
        HttpResponse httpResponse = http.send(httpRequest);
        
      //  System.debug(httpResponse.getBody());
     
        JSONParser parser = JSON.createParser(httpResponse.getBody());
        while (parser.nextToken() != null) {
            if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                String fieldName = parser.getText();
                parser.nextToken();
                if (fieldName == 'Message') {
                  // system.debug('contactId Text() => ' + parser.getText());
                    ordHeader.Integration_Error_Message__c =parser.getText();
                }
                else if(fieldName == 'AuthorizationCode'){
                     ordHeader.Auth_Code__c =parser.getText();
                }
            }
        }
        
       update  ordHeader;
    }
    
}