/*********************************************************************************************************
Description: This class will hold the structure of the API response we get from Smarty Street ZipCode API
**********************************************************************************************************/
public class ClsSmartyStreetZipCodeAPIResponse {

	public Integer input_index;
	public List<City_states> city_states;
	public List<Zipcodes> zipcodes;

	public class Zipcodes {
		public String zipcode;
		public String zipcode_type;
		public String default_city;
		public String county_fips;
		public String county_name;
		public String state_abbreviation;
		public String state;
		public Double latitude;
		public Double longitude;
		public String precision;
	}

	public class City_states {
		public String city;
		public String state_abbreviation;
		public String state;
		public Boolean mailable_city;
	}

    public static List<ClsSmartyStreetZipCodeAPIResponse> parse(String json) {
		return (List<ClsSmartyStreetZipCodeAPIResponse>) System.JSON.deserialize(json, List<ClsSmartyStreetZipCodeAPIResponse>.class);
	}
}