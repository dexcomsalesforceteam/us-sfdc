/*
@Author         : LTI
@Date Created    : 08/11/2018
@Description      : Handles the logic for SSIP Rule Trigger
*/
Public class ClsSSIPRuleTriggerHandler {
    
    public void beforeInsert(List<SSIP_Rule__c> newTrigger)
    {
        Set<Id> accountIds = new Set<Id>(); //List of Account involved in processing the SSIP Rule
        Set<Id> allPayorAccountIds = new Set<Id>(); //Payor Account Ids
        Map<String, Rules_Matrix__c> ruleMatrixMap = new Map<String, Rules_Matrix__c>(); 
        List<SSIP_Rule__c> optInRulesList = new List<SSIP_Rule__c>();
        set<string> priceBookNames=new set<string>();
        List<string> accountDefaultPriceBooks=new List<string>();
        Map<string,PriceBook2> priceBooksMap=new  Map<string,PriceBook2>();
        Map<Id, SSIP_Rule__c> accountSsipRecNewMap = new Map<Id, SSIP_Rule__c>();
        
        for(SSIP_Rule__c ssipRule:newTrigger){
            if(ssipRule.Price_Book_New__c =='Account Default Pricebook'){
                accountDefaultPriceBooks.add(ssipRule.Account_Default_Pricebook__c); 
            }
            
        }
        List<PriceBook2> priceBooksList=[SELECT Id,Name,Cash_Price_Book__c from PriceBook2 where  Name IN:accountDefaultPriceBooks];
        
        for(PriceBook2 priceBook:priceBooksList){
            priceBooksMap.put(priceBook.name,priceBook);
        }
        System.debug('priceBooksMap> ' + priceBooksMap);
        //Get all the account Ids involved in current processing
        for(SSIP_Rule__c ruleRecord : newTrigger){            
            accountIds.add(ruleRecord.Account__c);
        }
        Map<Id, Account> accountMap = new Map<Id, Account>([SELECT Id,Next_Possible_Date_For_Sensor_Order__c, Payor__c,Consumer_Payor_Code__c,                
                                                            Customer_Type__c,Primary_Benefit__r.Plan_Type__c, Next_Possible_Date_For_Transmitter_Order__c,Default_Price_Book__c
                                                            FROM Account WHERE Id IN : accountIds]);//Prepare Account map with the fields needed for our analysis
        for(Account acntRecord : accountMap.values()){            
            allPayorAccountIds.add(acntRecord.Payor__c);
        }                                                   
        System.debug('allPayorAccountIds> ' + allPayorAccountIds);
        for(Rules_Matrix__c ruleMatrix: [SElECT Account__c, Consumer_Type__c, Is_True_or_False__c,Product_Type__c, Quantity_Boxes__c, Duration_In_Days__c, 
                                         SSIP_Opt_In__c, Consumer_Payor_Code__c, Rule_Criteria__c from Rules_Matrix__c
                                         where (Rule_Criteria__c = 'SSIP Opt In' OR (Rule_Criteria__c = 'Days For Next Order'))
                                         and Is_True_or_False__c = true and Account__c IN :allPayorAccountIds]){
                                             System.debug('>> '+ ruleMatrix.Account__c);
                                             System.debug('1>> '+ ruleMatrix.Consumer_Type__c);
                                             System.debug('2>> '+ ruleMatrix.SSIP_Opt_In__c);
                                             if(ruleMatrix.Rule_Criteria__c == 'SSIP Opt In')
                                                 ruleMatrixMap.put(ruleMatrix.Account__c + ruleMatrix.Consumer_Type__c.toLowerCase(), ruleMatrix); 
                                             else{
                                                 ruleMatrixMap.put(ruleMatrix.Account__c + ruleMatrix.Consumer_Payor_Code__c + ruleMatrix.Product_Type__c + ruleMatrix.Quantity_Boxes__c, ruleMatrix); 
                                             }
                                         }
        System.debug('ruleMatrixMap> ' + ruleMatrixMap);
        for(SSIP_Rule__c ruleRecord : newTrigger){ 
            string primaryBenefitPlanType ;
            
            if(accountMap.get(ruleRecord.Account__c).Primary_Benefit__r.Plan_Type__c == 'MED ADV' || accountMap.get(ruleRecord.Account__c).Primary_Benefit__r.Plan_Type__c == 'HMO MEDICARE RISK'){
                primaryBenefitPlanType = 'med adv';
            }
            else{
                primaryBenefitPlanType = 'commercial';
            }
            System.debug('primaryBenefitPlanType :: ' + primaryBenefitPlanType);
            System.debug('Account_Default_Pricebook__c :: ' + ruleRecord.Account_Default_Pricebook__c);
            if(ruleRecord.Price_Book_New__c != 'Account Default Pricebook' || (ruleRecord.Price_Book_New__c == 'Account Default Pricebook' 
                                                                               && priceBooksMap.containsKey(ruleRecord.Account_Default_Pricebook__c) && priceBooksMap.get(ruleRecord.Account_Default_Pricebook__c).Cash_Price_Book__c) ){
                                                                                   optInRulesList.add(ruleRecord);
                                                                                   System.debug('optInRulesList in> ' + optInRulesList);
                                                                               }
            else if(!string.isBlank(primaryBenefitPlanType) && (primaryBenefitPlanType == 'commercial' || primaryBenefitPlanType == 'med adv')){
                if(!ruleMatrixMap.containsKey(accountMap.get(ruleRecord.Account__c).Payor__c + primaryBenefitPlanType) || 
                   ruleMatrixMap.get(accountMap.get(ruleRecord.Account__c).Payor__c + primaryBenefitPlanType).SSIP_Opt_In__c){ //RULE MATRIX DONT HAVE RECORD OR OPTED IN
                       optInRulesList.add(ruleRecord);                    
                   }                  
            }
        }  
        System.debug('optInRulesList> ' + optInRulesList);        
        
        List<SSIP_Rule__c> otherRulesList = new List<SSIP_Rule__c>([SELECT Id, Name, Account__c, Rule_Start_Date__c, Rule_End_Date__c, Product_Type__c
                                                                    FROM SSIP_Rule__c WHERE Account__c IN : accountIds]);//Get the list of all SSIP Rules for the Accounts involved in current Processing
        boolean duplicateRule = ClsApexConstants.BOOLEAN_FALSE;
        
        //Process the current SSIP Rules that are to be inserted
        //Validation Check : Only one active rule can exist for the Customer for a particular Product
        //dedup logic refactor start
        //get new map and transform that into a map of accountId to ssip rule record
        for(SSIP_Rule__c newRecord : newTrigger){
            accountSsipRecNewMap.put(newRecord.Account__c, newRecord);
        }
        //iterate over the fetched map and check for new map values
        duplicateRule = ClsApexConstants.BOOLEAN_FALSE;
        for(SSIP_Rule__c existingRuleRecord : otherRulesList){
            if(accountSsipRecNewMap.containsKey(existingRuleRecord.Account__c) && optInRulesList.contains(accountSsipRecNewMap.get(existingRuleRecord.Account__c))){
                //Only one active rule can be for a given Product
                if(accountSsipRecNewMap.get(existingRuleRecord.Account__c).Product_Type__c == existingRuleRecord.Product_Type__c){
                    //Existing rule's end date cannot be null
                    if(existingRuleRecord.Rule_End_Date__c == null){
                        duplicateRule = ClsApexConstants.BOOLEAN_TRUE;
                    }else if(accountSsipRecNewMap.get(existingRuleRecord.Account__c).Rule_End_Date__c == null && accountSsipRecNewMap.get(existingRuleRecord.Account__c).Rule_Start_Date__c >=  existingRuleRecord.Rule_Start_Date__c && accountSsipRecNewMap.get(existingRuleRecord.Account__c).Rule_Start_Date__c <= existingRuleRecord.Rule_End_Date__c){
                        //New Rule's start date cannot fall between the existing rule's start and end date
                        duplicateRule = ClsApexConstants.BOOLEAN_TRUE;
                    }else if((accountSsipRecNewMap.get(existingRuleRecord.Account__c).Rule_Start_Date__c >=  existingRuleRecord.Rule_Start_Date__c && accountSsipRecNewMap.get(existingRuleRecord.Account__c).Rule_Start_Date__c <= existingRuleRecord.Rule_End_Date__c) || (accountSsipRecNewMap.get(existingRuleRecord.Account__c).Rule_End_Date__c >=  existingRuleRecord.Rule_Start_Date__c && accountSsipRecNewMap.get(existingRuleRecord.Account__c).Rule_End_Date__c <= existingRuleRecord.Rule_End_Date__c)){
                        //New Rule's start and end date cannot fall between the existing rule's start and end date
                        duplicateRule = ClsApexConstants.BOOLEAN_TRUE;
                    }
                }
                //Display error message when there is Duplicate found
                if (duplicateRule)
                    accountSsipRecNewMap.get(existingRuleRecord.Account__c).addError('There is already one active SSIP Rule exist for the customer for the same Product');
            }
        }
        //dedup logic refactor end
        for(SSIP_Rule__c newRuleRecord : newTrigger){
            if(optInRulesList.contains(newRuleRecord)){
                //Get the current account record details
                Account accountRecord = accountMap.get(newRuleRecord.Account__c);
                
                Integer quantitySelected = Integer.valueOf((String.valueOf(newRuleRecord.Product__c).deleteWhitespace()).right(1));
                //Validation Check: 'First shipment Date' Logic
                if(newRuleRecord.First_Shipment_Date__c == null && newRuleRecord.Product_Type__c != null){
                    //If First Shipment Date is not filled then the value should be taken from Account.Next_Possible_Date_For_Sensor_Order__c
                    if(newRuleRecord.Product_Type__c == 'Sensor')
                    {
                        if(accountRecord.Next_Possible_Date_For_Sensor_Order__c != null){
                            if(accountRecord.Next_Possible_Date_For_Sensor_Order__c > newRuleRecord.Rule_Start_Date__c )
                                newRuleRecord.First_Shipment_Date__c = accountRecord.Next_Possible_Date_For_Sensor_Order__c;
                            else
                                newRuleRecord.addError('Please enter First Shipment Date.');
                        }
                        else{
                            //if next possible date for sensor order is null then default first shipment date on ssip rule
                            //as new rule start date+ duration from payor rule matrix
                            if(!ruleMatrixMap.isEmpty() && ruleMatrixMap.containsKey(accountMap.get(newRuleRecord.Account__c).Payor__c + accountMap.get(newRuleRecord.Account__c).Consumer_Payor_Code__c + newRuleRecord.Product_Type__c + quantitySelected) &&
                               null !=ruleMatrixMap.get(accountMap.get(newRuleRecord.Account__c).Payor__c + accountMap.get(newRuleRecord.Account__c).Consumer_Payor_Code__c + newRuleRecord.Product_Type__c + quantitySelected))
                                newRuleRecord.First_Shipment_Date__c = newRuleRecord.Rule_Start_Date__c +Integer.valueOf(ruleMatrixMap.get(accountMap.get(newRuleRecord.Account__c).Payor__c + accountMap.get(newRuleRecord.Account__c).Consumer_Payor_Code__c + newRuleRecord.Product_Type__c + quantitySelected).Duration_In_Days__c);
                        }                        
                    }
                    //If First Shipment Date is not filled then the value should be taken from Account.Next_Possible_Date_For_Transmitter_Order__c
                    else if(newRuleRecord.Product_Type__c == 'Transmitter')
                    {
                        if(accountRecord.Next_Possible_Date_For_Transmitter_Order__c != null){
                            if(accountRecord.Next_Possible_Date_For_Transmitter_Order__c > newRuleRecord.Rule_Start_Date__c )
                                newRuleRecord.First_Shipment_Date__c = accountRecord.Next_Possible_Date_For_Transmitter_Order__c;
                            else 
                                newRuleRecord.addError('Please enter First Shipment Date.');
                        }
                        else{
                            if(!ruleMatrixMap.isEmpty() && ruleMatrixMap.containsKey(accountMap.get(newRuleRecord.Account__c).Payor__c + accountMap.get(newRuleRecord.Account__c).Consumer_Payor_Code__c + newRuleRecord.Product_Type__c + quantitySelected) &&
                               null !=ruleMatrixMap.get(accountMap.get(newRuleRecord.Account__c).Payor__c + accountMap.get(newRuleRecord.Account__c).Consumer_Payor_Code__c + newRuleRecord.Product_Type__c + quantitySelected))
                                newRuleRecord.First_Shipment_Date__c = newRuleRecord.Rule_Start_Date__c +Integer.valueOf(ruleMatrixMap.get(accountMap.get(newRuleRecord.Account__c).Payor__c + accountMap.get(newRuleRecord.Account__c).Consumer_Payor_Code__c + newRuleRecord.Product_Type__c + quantitySelected).Duration_In_Days__c);
                        }
                        
                    }
                    //Force the user to fill in the date if we couldnt find the next possible date
                    else
                    {
                        newRuleRecord.addError('Please enter First Shipment Date.');
                    }
                } 
                
                //Frequency Calculation : 
                if(newRuleRecord.Exception_Type__c == 'No Exceptions'){                    
                    
                    System.debug('Product_Type__c :: '+ newRuleRecord.Product_Type__c);
                    System.debug('Price_Book_New__c :: '+ newRuleRecord.Price_Book_New__c);
                    
                    if(newRuleRecord.Product_Type__c != null && newRuleRecord.Price_Book_New__c == 'Account Default Pricebook' &&
                       !ruleMatrixMap.isEmpty() && ruleMatrixMap.ContainsKey(accountMap.get(newRuleRecord.Account__c).Payor__c + accountMap.get(newRuleRecord.Account__c).Consumer_Payor_Code__c + newRuleRecord.Product_Type__c + quantitySelected)){
                           newRuleRecord.Frequency_In_Days_Number__c = ruleMatrixMap.get(accountMap.get(newRuleRecord.Account__c).Payor__c + 
                                                                                         accountMap.get(newRuleRecord.Account__c).Consumer_Payor_Code__c + newRuleRecord.Product_Type__c + quantitySelected).Duration_In_Days__c;
                       }else if(newRuleRecord.Product_Type__c != null && newRuleRecord.Product_Type__c == 'Sensor'){
                           if(quantitySelected == 1) newRuleRecord.Frequency_In_Days_Number__c = 23; //default for 1 box sensor
                           if(quantitySelected == 3) newRuleRecord.Frequency_In_Days_Number__c = 79; //default for 3 box sensors
                       }else if(newRuleRecord.Product_Type__c != null && newRuleRecord.Product_Type__c == 'Transmitter') newRuleRecord.Frequency_In_Days_Number__c = 180; //be default value (Product = Transmitter/ Bundle)
                    
                }else{
                    if(!String.isBlank(newRuleRecord.Exception_Frequency_In_Days__c))
                        newRuleRecord.Frequency_In_Days_Number__c = Integer.valueOf(newRuleRecord.Exception_Frequency_In_Days__c);
                }
                System.debug('newRuleRecord.Frequency_In_Days_Number__c:: ' + newRuleRecord.Frequency_In_Days_Number__c);
                //Logic for Pricebook update on SSIP Rule when its Empty
                //newRuleRecord.Price_Book__c = newRuleRecord.Price_Book__c != null ? newRuleRecord.Price_Book__c : accountRecord.Default_Price_Book__c;
            }else{
                
                //if(!priceBooksMap.isEmpty() && priceBooksMap.containskey(newRuleRecord.Price_Book_New__c)&& priceBooksMap.get(newRuleRecord.Price_Book_New__c).Cash_Price_Book__c == false){
                newRuleRecord.addError('Payor on Account has not Opted In for SSIP Rule creation.');
                // }
            }   
        } 
    }
    public void afterInsert(List<SSIP_Rule__c> newTrigger){
        
        //Process SSIP Schedule Creation after the SSIP Rule Insert
        List<SSIP_Schedule__c> createSchedulesList = new List<SSIP_Schedule__c>();
        Set<Id> accountIds = new Set<Id>(); //List of Account involved in processing the SSIP Rule
        //Get all the account Ids involved in current processing
        for(SSIP_Rule__c ruleRecord : newTrigger){
            accountIds.add(ruleRecord.Account__c);
        }
        
        PriceBook2 cashPriceBook = new PriceBook2();
        if(!test.isRunningTest()){
            cashPriceBook = [SELECT Id,Name FROM Pricebook2 WHERE Name = 'Cash Price List' AND Cash_Price_Book__c = true limit 1];
        }
        Map<Id, Account> accountMap = new Map<Id, Account>([SELECT Id, Payor__c, Default_Price_Book__c, Primary_Ship_To_Address__c
                                                            from Account where Id IN : accountIds]);//Prepare Account map with the fields needed for our analysis
        
        for(SSIP_Rule__c ruleRecord : newTrigger){
            
            Date scheduledShipmentDate = ruleRecord.First_Shipment_Date__c;//This will be the first Schedule's 'Schedule Shipment Date'
            //modified rule end date logic for CRMSF-4577
            Date ruleEndDate = ruleRecord.Rule_End_Date__c != null ? ruleRecord.Rule_End_Date__c : null;////If the SSIP Rule does not have any End Date populated then we consider it as null
            
            //Prepare the Schedule List to be inserted
            //Note: Logic needs to be altered for Pricebook once Cash pricebook logic is implemented
            //updated under CRMSF-4577
            if(ruleEndDate !=null){
                while(scheduledShipmentDate <= ruleEndDate){
                    createSSIPSchedulesList(createSchedulesList, ruleRecord, scheduledShipmentDate, accountMap, cashPriceBook);
                    scheduledShipmentDate = scheduledShipmentDate.addDays(Integer.ValueOf(ruleRecord.Frequency_In_Days_Number__c));
                }
            }else if(ruleEndDate == null && ruleRecord.Max_Schedule_Shipment_Date__c == null){
                for(Decimal scheduleCycle = ClsApexConstants.ZERO_DECIMAL; scheduleCycle < ruleRecord.Schedule_Cycle__c; scheduleCycle++){
                    createSSIPSchedulesList(createSchedulesList, ruleRecord, scheduledShipmentDate, accountMap, cashPriceBook);
                    scheduledShipmentDate = scheduledShipmentDate.addDays(Integer.ValueOf(ruleRecord.Frequency_In_Days_Number__c));
                }
            }
            //changes end for CRMSF-4577
        }
        //Insert the schedules
        if(!createSchedulesList.isEmpty()){
            Database.insert(createSchedulesList);
        }//removed try catch as part of 4577 as Database methods don't throw exception but return error array
        
        //introduced the call under 4885
        updateMaxRuleEndDateOnAccount(newTrigger);
    }
    
    public void afterUpdate(Map<Id, SSIP_Rule__c> newTriggerRules, Map<Id, SSIP_Rule__c> oldTriggerRules){
        List<Id> deletedRecords = new List<Id>();    
        //Alter the SSIP Schedules based on the Rule End Date change on the SSIP Rule
        Map<Id, SSIP_Rule__c> endDateUpdatedRulesMap = new Map<Id, SSIP_Rule__c>();//Map holds SSIP Rule Id to SSIP Rule Record
        Map<Id, List<SSIP_Schedule__c>> ruleScheduleRecordsMap = new Map<Id, List<SSIP_Schedule__c>>();//Map between the SSIP Rule Ids to its Schedules
        List<SSIP_Schedule__c> deleteScheduleRecords = new List<SSIP_Schedule__c>();//List holds the schedules that are to be deleted
        List<SSIP_Schedule__c> scheduleRecords = new List<SSIP_Schedule__c>();//List holds the schedules currently exist for the SSIP Rule
        
        //Find out the rules where the 'Rule End Date' value is changed
        for(SSIP_Rule__c newRule : newTriggerRules.values()){
            if(newRule.Rule_End_Date__c != oldTriggerRules.get(newRule.Id).Rule_End_Date__c){
                endDateUpdatedRulesMap.put(newRule.Id, newRule);
            }
        }
        //Get all the Open and In Progress Schedule records for the Rules included in the trigger
        scheduleRecords = [SELECT Id, Scheduled_Shipment_Date__c, Account__c, SSIP_Rule__c, Order__c from SSIP_Schedule__c where Status__c IN ('Open', 'Eligibility Verified', 'Eligibility In Review') AND SSIP_Rule__c IN :endDateUpdatedRulesMap.keySet()];
        
        //Populate the map to hold the Rule Id to its Schedules
        for(SSIP_Schedule__c scheduleRecord : scheduleRecords){
            if(ruleScheduleRecordsMap.containsKey(scheduleRecord.SSIP_Rule__c)){
                ruleScheduleRecordsMap.get(scheduleRecord.SSIP_Rule__c).add(scheduleRecord);
            }else{
                ruleScheduleRecordsMap.put(scheduleRecord.SSIP_Rule__c, new List<SSIP_Schedule__c>{scheduleRecord});
            }
        }   
           
        //For each Rule record where the End Date is changed, look at the Schedules' Schedule Shipment Date and if it is greater than the Rule end date then delete them 
        for(SSIP_Rule__c ruleRecord : endDateUpdatedRulesMap.Values()){
            Date ruleEndDate = ruleRecord.Rule_End_Date__c != null ? ruleRecord.Rule_End_Date__c : Date.newInstance(System.Today().year(), 12,31);
            
            if(ruleScheduleRecordsMap.containsKey(ruleRecord.id))
            {
                for(SSIP_Schedule__c schedRecord : ruleScheduleRecordsMap.get(ruleRecord.id)){
                    if(ruleEndDate < schedRecord.Scheduled_Shipment_Date__c ||
                       (ruleEndDate == schedRecord.Scheduled_Shipment_Date__c && schedRecord.Order__c == null)){
                           deleteScheduleRecords.add(schedRecord);
                       }
                }
            }  
        }
        
        if(!deleteScheduleRecords.isEmpty()){
            try{
                Database.DeleteResult[] deletedResult = Database.delete(deleteScheduleRecords, false);
                for(Database.DeleteResult sr : deletedResult){
                    if(!sr.isSuccess()){
                        for(Database.Error err : sr.getErrors()) {
                            System.debug('The following error has occurred.');                    
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            System.debug('Fields that affected this error: ' + err.getFields());
                        }                        
                    }else
                        deletedRecords.add(sr.getId());
                } 
                
                //insert Marketing Interaction
                if(!deletedRecords.isEmpty()){
                    //create Map to insert marketing interaction records
                    List<Marketing_Interaction__c> marketingInteractionList = new List<Marketing_Interaction__c>();
                    for(SSIP_Schedule__c schedRecord :deleteScheduleRecords){
                        if(deletedRecords.contains(schedRecord.Id) && 
                           String.isBlank(newTriggerRules.get(schedRecord.SSIP_Rule__c).Close_Reason__c)){
                               
                               Marketing_Interaction__c thisMI = new Marketing_Interaction__c();
                               thisMI.Source_Record_Id__c = schedRecord.SSIP_Rule__c;  // Add Source Record Id as SSIP Rule ID
                               thisMI.Account__c = schedRecord.Account__c; //Account Id
                               thisMI.Communication_Type__c =  'SSIP Rule Cancelled';
                               marketingInteractionList.add(thisMI); //add Record to List to Insert
                           }
                    }
                    if(!marketingInteractionList.isEmpty()){
                        System.debug('final List >> ' + marketingInteractionList);
                        Database.insert(marketingInteractionList,false);  //Insert Marketing Interaction Records
                    }
                }
            }catch(DMLException e){System.debug('Schedule Deletion Failed .. +e');}
        }
    } 
    
    //Sudheer [CRMSF-4294 19/6/2019] : Prevent the SSIP Rule product if product is not availble on Pricebook List.
    public static void preventUnPriceBookProducts(List<SSIP_Rule__c> ssipRulesList){
        List<string> priceBookIds=new List<string>();
        List<string> accountDefaultPriceBooks=new List<string>();
        Map<string,string> priceBooksWithProductsMap=new  Map<string,string>();
        
        for(SSIP_Rule__c ssipRule:ssipRulesList){
            
            if(ssipRule.Price_Book_New__c =='Account Default Pricebook'){
                accountDefaultPriceBooks.add(ssipRule.Account_Default_Pricebook__c); 
            }
            else{
                priceBookIds.add(ssipRule.Price_Book_New__c); 
            }
        }
        List<PriceBookEntry> priceBookEntrysList=[SELECT Name,id, Product2.Name,Product2.Generation__c,Product2.ProductCode, PriceBook2.Name from PriceBookEntry where PriceBook2.Id in:priceBookIds];
        List<PriceBookEntry> accountDefaultPriceBooksEntrysList=[SELECT Name,id,Product2.Generation__c,Product2.Name,Product2.ProductCode,PriceBook2.Id,PriceBook2.Name from PriceBookEntry where PriceBook2.Name in:accountDefaultPriceBooks];
        
        for(PriceBookEntry priceBook:priceBookEntrysList){
            string productWithGeneration =priceBook.Product2.Name+'-'+priceBook.Product2.Generation__c;
            if(priceBooksWithProductsMap.isEmpty() || !priceBooksWithProductsMap.containskey(priceBook.PriceBook2.Id)){
                priceBooksWithProductsMap.put(priceBook.PriceBook2.Id , productWithGeneration);
            }else{
                string priceBookProducts=priceBooksWithProductsMap.get(priceBook.PriceBook2.Id);
                priceBooksWithProductsMap.put(priceBook.PriceBook2.Id , priceBookProducts+' : '+productWithGeneration);               
            } 
        }
        
        for(PriceBookEntry priceBook:accountDefaultPriceBooksEntrysList){
            string productWithGeneration =priceBook.Product2.Name+'-'+priceBook.Product2.Generation__c;
            if(priceBooksWithProductsMap.isEmpty() || !priceBooksWithProductsMap.containskey(priceBook.PriceBook2.Name)){
                priceBooksWithProductsMap.put(priceBook.PriceBook2.Name , productWithGeneration);
            }else{
                string priceBookProducts=priceBooksWithProductsMap.get(priceBook.PriceBook2.Name);
                priceBooksWithProductsMap.put(priceBook.PriceBook2.Name , priceBookProducts+' : '+productWithGeneration); 
            }              
        }
        //  system.debug('priceBooksWithProductsMap..'+priceBooksWithProductsMap);
        for(SSIP_Rule__c ssipRule:ssipRulesList){
            List<string> ssipRuleProductSplit=ssipRule.Product__c.split(' | ');
            string ssipRuleProduct= ssipRuleProductSplit[2].trim()+'-'+ssipRuleProductSplit[0].trim();
            //   system.debug('ssipRuleProduct..'+ssipRuleProduct);
            
            //   system.debug('priceBooksWithProductsMap.containsKey(ssipRule.Account_Default_Pricebook__c)...'+priceBooksWithProductsMap.containsKey(ssipRule.Account_Default_Pricebook__c));
            if(!priceBooksWithProductsMap.isEmpty() ){
                if(priceBooksWithProductsMap.containsKey(ssipRule.Price_Book_New__c) && !priceBooksWithProductsMap.get(ssipRule.Price_Book_New__c).contains(ssipRuleProduct)){
                    
                    ssipRule.addError('This SSIP Rule Product not contains in the selected PriceBook list');
                }
                if(priceBooksWithProductsMap.containsKey(ssipRule.Account_Default_Pricebook__c) && !priceBooksWithProductsMap.get(ssipRule.Account_Default_Pricebook__c).contains(ssipRuleProduct)){
                    ssipRule.addError('This SSIP Rule Product not contains in the selected PriceBook list');
                }
            }
        }
        
    }
    
    /*
     * @Description : This function creates a list of SSIP Schedule Records
     * Created for CRMSF-4577. Same function is called by three different logics
     * @Author : LTI
    */
    public static void createSSIPSchedulesList(List<SSIP_Schedule__c> createSchedulesList, SSIP_Rule__c ruleRecord, Date scheduledShipmentDate, Map<Id, Account> accountMap, PriceBook2 cashPriceBook){
        SSIP_Schedule__c scheduleRecord = new SSIP_Schedule__c(); 
        scheduleRecord.Account__c = ruleRecord.Account__c;
        
        if(ClsApexConstants.CASH_PRICE_LIST.equalsIgnoreCase(ruleRecord.Price_Book_New__c) && cashPriceBook.Id != null){
            scheduleRecord.Price_Book__c = cashPriceBook.Id;
        }else if(ClsApexConstants.ACCOUNT_DEFAULT_PRICEBOOK.equalsIgnoreCase(ruleRecord.Price_Book_New__c)){
            if(accountMap !=null){
                scheduleRecord.Price_Book__c =  accountMap.get(ruleRecord.Account__c).Default_Price_Book__c;
            }else{
                scheduleRecord.Price_Book__c = ruleRecord.Account__r.Default_Price_Book__c;
            }
        }else{
            scheduleRecord.Price_Book__c =  ruleRecord.Price_Book_New__c;
        }
        
        system.debug('ruleRecord.Price_Book_New__c - ' +  ruleRecord.Price_Book_New__c);
        scheduleRecord.Product__c = ruleRecord.Product__c;
        scheduleRecord.Scheduled_Shipment_Date__c = scheduledShipmentDate;
        if(accountMap !=null){
            scheduleRecord.Payor__c = accountMap.get(ruleRecord.Account__c).Payor__c;    
            scheduleRecord.Shipping_Address__c  = accountMap.get(ruleRecord.Account__c).Primary_Ship_To_Address__c;
        }else{
            scheduleRecord.Payor__c = ruleRecord.Account__r.Payor__c;
            scheduleRecord.Shipping_Address__c  = ruleRecord.Account__r.Primary_Ship_To_Address__c;
        }
        scheduleRecord.SSIP_Rule__c = ruleRecord.Id;
        scheduleRecord.Status__c = ClsApexConstants.STATUS_OPEN;
        createSchedulesList.add(scheduleRecord); 
    }
    
    
    /*
     * Created under 4885
    */
    public void handleAfterUpdate(List<SSIP_Rule__c> newSSIPRecordList, Map<Id, SSIP_Rule__c> oldSSIPRecordMap){
        List<SSIP_Rule__c> ssipRuleProcessList = new List<SSIP_Rule__c>();
        
        for(SSIP_Rule__c ssipObj : newSSIPRecordList){
            if(oldSSIPRecordMap.containsKey(ssipObj.Id) && oldSSIPRecordMap.get(ssipObj.Id) != null
              && ssipObj.Rule_End_Date__c != oldSSIPRecordMap.get(ssipObj.Id).Rule_End_Date__c){
                  //end date is changed so adding to the process list
                ssipRuleProcessList.add(ssipObj);
            }
        }
        if(!ssipRuleProcessList.isEmpty()){
            updateMaxRuleEndDateOnAccount(ssipRuleProcessList);
        }
    }
    
    /*
     * Created under 4885
    */
    public static void updateMaxRuleEndDateOnAccount(List<SSIP_Rule__c> newSSIPRecordList){
        Map<Id, Account> accountMapToUpdate = new Map<Id, Account>();
        Map<Id, SSIPDetailsWrapper> accountSSIPMap = new Map<Id, SSIPDetailsWrapper>();
        Set<Id> accountIdSet = new Set<Id>();
        Date highDate = Date.newInstance(2099, 12, 31);
        for(SSIP_Rule__c ssipRule : newSSIPRecordList){
            accountIdSet.add(ssipRule.Account__c);
        }
        system.debug('accountIdSet::::'+accountIdSet);
        //get max SSIP Rule End Date on account and total rules on the account
        for(Account accountObj : [SELECT Id,Max_SSIP_Rule_End_Date__c, Date_of_Death__c, (SELECT Id, Rule_End_Date__c FROM SSIP_Rules__r ORDER BY Rule_End_Date__c Desc NULLS FIRST LIMIT 1) FROM Account WHERE Id IN: accountIdSet]){
            SSIPDetailsWrapper ssipWrap = new SSIPDetailsWrapper();
            ssipWrap.maxSSIPDate = accountObj.Max_SSIP_Rule_End_Date__c;
            if(accountObj.SSIP_Rules__r[0] !=null){
                ssipWrap.ssipRuleEndDateOfRule = accountObj.SSIP_Rules__r[0].Rule_End_Date__c;
            }
            if(accountObj.Date_of_Death__c == null){
                accountSSIPMap.put(accountObj.Id, ssipWrap);
            }else{
                accountMapToUpdate.put(accountObj.Id, new Account(Id = accountObj.Id, Max_SSIP_Rule_End_Date__c = system.today()));
            }
        }
        
        //iterate over main list and check how many SSIP rule records are there and based on that compare the dates
        for(SSIP_Rule__c ssipRule : newSSIPRecordList){
            if(accountSSIPMap.containsKey(ssipRule.Account__c)){
                if(accountSSIPMap.get(ssipRule.Account__c).maxSSIPDate == null && ssipRule.Rule_End_Date__c != null &&
                   accountSSIPMap.get(ssipRule.Account__c).ssipRuleEndDateOfRule == ssipRule.Rule_End_Date__c){ // max end date on the account is null
                       //and the end date of fetched rule and the one being entered is same then set the rule end date
                       Account accountObject = new Account(Id = ssipRule.Account__c, Max_SSIP_Rule_End_Date__c = ssipRule.Rule_End_Date__c);
                       accountMapToUpdate.put(accountObject.Id, accountObject);
                   }
                if(ssipRule.Rule_End_Date__c == null && (accountSSIPMap.get(ssipRule.Account__c).maxSSIPDate == null || accountSSIPMap.get(ssipRule.Account__c).maxSSIPDate != null)){
                    //update account with null as the inserted/updated rule is having null date and account does not have a null date as max date
                    Account accountObject = new Account(Id = ssipRule.Account__c, Max_SSIP_Rule_End_Date__c = highDate); //set to high as rule end date is null
                    accountMapToUpdate.put(accountObject.Id, accountObject);
                }
                if(ssipRule.Rule_End_Date__c != null && accountSSIPMap.get(ssipRule.Account__c).maxSSIPDate != null){
                    //end date for the rule getting inserted is not null and the existing max date is also not null
                    //then compare the dates and update account accordingly
                    Account accountObject =new Account(Id = ssipRule.Account__c);
                    if(ssipRule.Rule_End_Date__c > accountSSIPMap.get(ssipRule.Account__c).maxSSIPDate){
                        accountObject.Max_SSIP_Rule_End_Date__c = ssipRule.Rule_End_Date__c;
                        accountMapToUpdate.put(accountObject.Id, accountObject);
                    }
                    //if the updated date is in the past of another rule then the other high date is stamped
                    if(ssipRule.Rule_End_Date__c < accountSSIPMap.get(ssipRule.Account__c).maxSSIPDate){
                        Account accountObject1 = new Account(Id = ssipRule.Account__c);
                        //other rule end date is not null and current rule end date is not null and current rule end date is less than other rule end date
                        if(accountSSIPMap.get(ssipRule.Account__c).ssipRuleEndDateOfRule != null && ssipRule.Rule_End_Date__c < accountSSIPMap.get(ssipRule.Account__c).ssipRuleEndDateOfRule){ // other end date is high
                            accountObject1.Max_SSIP_Rule_End_Date__c = accountSSIPMap.get(ssipRule.Account__c).ssipRuleEndDateOfRule;
                        }
                        //other rule's end date is null but current rule's end date is not null then update to highdate
                        if(accountSSIPMap.get(ssipRule.Account__c).ssipRuleEndDateOfRule == null && ssipRule.Rule_End_Date__c != null){
                            accountObject1.Max_SSIP_Rule_End_Date__c = highDate;
                        }
                        //if other rule end date and current rule end date are same the update to the current rule date
                        if(ssipRule.Rule_End_Date__c != null && ssipRule.Rule_End_Date__c == accountSSIPMap.get(ssipRule.Account__c).ssipRuleEndDateOfRule &&
                           ssipRule.Rule_End_Date__c < accountSSIPMap.get(ssipRule.Account__c).maxSSIPDate){
                               accountObject1.Max_SSIP_Rule_End_Date__c = ssipRule.Rule_End_Date__c;
                           }
                        accountMapToUpdate.put(accountObject1.Id, accountObject1);
                    }
                    
                }
            }
        }
        
        if(!accountMapToUpdate.isEmpty()){
            Database.update(accountMapToUpdate.values(), ClsApexConstants.BOOLEAN_FALSE);
        }
    }
    
    
    public class SSIPDetailsWrapper{
        public Date maxSSIPDate;
        public Date ssipRuleEndDateOfRule;
    }
}