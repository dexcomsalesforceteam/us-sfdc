public class TaskTriggerHandler {
    
    Public Static void  TaskFieldUpdates(List<Task> ListTask) { 
        /********************************************************************************
@author Abhishek Parghi
@date 01/29/2016
@description:It retrives Prescribers fields from associated 
Prescriber when creating a new "Log a Call" for Call Records. It's 
part of IMS Project.
*******************************************************************************/ 
        List<Task> TaskList = new List<Task>();
        Map<Id, Task> MapTasks = new Map<Id, Task>();
        Set<Id> PrescriberIds = new Set<Id>();
        List<Task> UpdatedTasks = new List<Task>();
        Map<Id, List<Task>> whatIdsMap = new Map<Id, List<Task>>();     
        
        for(Task t : ListTask){
            if(t.WhatId != null){
                
                if(!whatIdsMap.containsKey(t.WhatId)){
                    List<Task> temp = new List<Task>();
                    temp.add(t);
                    whatIdsMap.put(t.WhatId, temp);
                }else{
                    whatIdsMap.get(t.WhatId).add(t);
                }
            }
        }
        system.debug('@@@@' +  whatIdsMap.keySet());
        for(Account a : [Select prescribers__c,AccountNumber,prescribers__r.Name,prescribers__r.firstname,prescribers__r.LastName,prescribers__r.Middle_Name__c,
                         prescribers__r.IMS_ID__pc,prescribers__r.PersonMailingStreet,prescribers__r.CCV_ID__c,
                         prescribers__r.PersonMailingCity,prescribers__r.PersonMailingPostalCode,prescribers__r.PersonMailingState,prescribers__r.New_Address_ID__c,
                         prescribers__r.Suffix__pc,prescribers__r.Territory_Code__c,prescribers__r.Account_Record_Type__c,prescribers__r.Party_ID__c
                         from Account where ID  in :whatIdsMap.keySet()]){
                             
                             for(Task t :whatIdsMap.get(a.Id)){
                                 t.First_Name__c = a.prescribers__r.firstname;
                                 t.LastName__c = a.prescribers__r.LastName;
                                 t.Middle_Name__c = a.prescribers__r.Middle_Name__c;
                                 t.IMS_ID__c =a.prescribers__r.IMS_ID__pc;
                                 t.Address_Line_1__c = a.prescribers__r.PersonMailingStreet;
                                 t.CCV_ID__c = a.prescribers__r.CCV_ID__c; 
                                 t.City__c = a.prescribers__r.PersonMailingCity;
                                 t.Zip_Code__c = a.prescribers__r.PersonMailingPostalCode;
                                 t.State__c = a.prescribers__r.PersonMailingState;
                                 t.Suffix__c =a.prescribers__r.Suffix__pc;
                                 t.Prescriber_Territory_Code__c = a.prescribers__r.Territory_Code__c;
                                 t.Prescribers_SFDC_ID__c = a.prescribers__c;
                                 t.Prescriber_s_RecordType__c = a.prescribers__r.Account_Record_Type__c;
                                 t.Party_ID__c =a.prescribers__r.Party_ID__c;
                                 t.Territory__c = a.prescribers__r.Territory_Code__c;
                                 t.New_Address_ID__c = a.prescribers__r.New_Address_ID__c; 
                                 t.Prescriber__c = a.prescribers__r.Name;
                                 t.Patient_Account_Number__c = a.AccountNumber;
                             }
                         }
        
    }
    
    Public Static void  AutoshareTasks(List<Task> ListTask) {     
        String temp;
        Opportunity op;
        Map<ID,LIST<Task>> oppIdToTasks = new Map<ID,LIST<Task>>();
        Map<ID,Opportunity> oppIdToOpp = new Map<ID,Opportunity>();
        String oppcode = '006';
        String sub = '';
        List<ID> opportunityIds = new List<ID>();
        
        for(task t : ListTask) {
            if(t.WhatId != null){
                sub = String.valueOf(t.WhatId).substring(0,3);
                if(sub != null){
                    if(sub.contains(oppcode))
                        opportunityIds.add(t.WhatId);
                    System.debug('#OppIDs'+opportunityIds);  
                }
            }
        }
        if(opportunityIds.size()>0){
            for(opportunity o :[SELECT id,Dist_Activity_Notes__c FROM opportunity WHERE id IN :opportunityIds LIMIT 49999]){
                oppIdToOpp.put(o.id, o); 
            }
            
            for(Task t :[SELECT id,subject,WhatId,createdDate,Distributor_To_See__c FROM Task 
                         WHERE WhatId IN :opportunityIds AND Distributor_To_See__c != '' LIMIT 49999]){
                             if(oppIdToTasks.ContainsKey(t.WhatId)){
                                 oppIdToTasks.get(t.WhatId).add(t);
                             }
                             else{
                                 List<Task> tempList = new List<Task>();
                                 tempList.add(t);
                                 oppIdToTasks.put(t.WhatId,tempList);           
                             } 
                         }
        }
        if(oppIdToTasks.size()>0){
            for(ID oppId : oppIdToTasks.keySet()){
                //Opportunity o = oppIdToOpp.get(oppId);
                op = oppIdToOpp.get(oppId);
                temp = ''; 
                
                for(Task t : oppIdToTasks.get(oppId)){
                    temp += t.subject + ' : ' + t.createddate + '\n';
                    System.debug('#Temp'+temp);
                }
                if(temp!=null){
                    op.Dist_Activity_Notes__c = temp; 
                }
                
                oppIdToOpp.put(oppId,op);
            }
        }
        try{ 
            if(oppIdToOpp.size()>0)
                update oppIdToOpp.values(); 
        }
        catch(Exception e) { 
            System.debug('AutoShareTasks Exception' + e.getMessage()); 
        }
        
    }   
    //Jagan 08/15/2018 - Added the below method
    //Method will prevent the task deletion if it is invoked by users other than the admins
    public static void ProcessTaskDeletion(List<Task> tasksToBeDeleted)
    {
        //Get the current user's profile id
        String userProfileId = UserInfo.getProfileId();  
        //Get the list of admin profiles  	
        List<Profile> adminProfiles=[select id from Profile where name='System Administrator' or name='Data Integrator'];
        //For each task involved in deletion check the Profiles and status and then throw the error message
        for (Task a : tasksToBeDeleted)      
        {            
            if(a.Status == 'Completed' && userProfileId != adminProfiles[0].Id && userProfileId != adminProfiles[1].Id)
                a.addError('You can\'t delete this record');
        }          
    }
    
    /*
     * @Description : This method is created for CRMSF-4623
     * When all the tasks related to an escalation record are completed then
     * mark the escalation record to resolve/Open - Changed under 4623
     * @Author : LTI
	*/
    public static void updateEscalationStatus(List<Task> taskUpdateList, Map<Id,Task> oldTaskMap){
        Map<String,Id> recordTypeIdmap = ClsApexUtility.getRecordTypeMap(ClsApexConstants.TASK_SOBJECT_API, ClsApexConstants.BOOLEAN_FALSE);
        Set<Id> escalationRecordIdSetCompleteTask = new Set<Id>();
        Set<Id> reopenedTaskEscalationIdSet = new Set<Id>();
        Set<Id> allEscalationRecordIdSet = new Set<Id>(); // this set holds escalation records of completed and reopened tasks
        Map<Escalation__c, List<Task>> escalationToTaskListMap = new Map<Escalation__c, List<Task>>();
        for(Task newTaskObj : taskUpdateList){
            if(newTaskObj.Status != oldTaskMap.get(newTaskObj.Id).Status && recordTypeIdmap.get(ClsApexConstants.ESCALATION_TASK) == newTaskObj.RecordTypeId
              && String.valueOf(newTaskObj.WhatId).contains(ClsApexConstants.ESCALATION_OBJ_ID_PREFIX)){
                  //if status is marked to complete
                  if(ClsApexConstants.STATUS_COMPLETED.equalsIgnoreCase(newTaskObj.Status)){
                      escalationRecordIdSetCompleteTask.add(newTaskObj.WhatId);
                  }
                  //if status is marked to in progress, Waiting On Someone Else, Not Started then reopen
                  if(ClsApexConstants.STATUS_COMPLETED.equalsIgnoreCase(oldTaskMap.get(newTaskObj.Id).Status) &&
                     (ClsApexConstants.TASK_STATUS_IN_PROGRESS.equalsIgnoreCase(newTaskObj.Status) || ClsApexConstants.TASK_STATUS_WAITING_SOMEONE_ELSE.equalsIgnoreCase(newTaskObj.Status) ||
                      ClsApexConstants.STATUS_NOT_STARTED.equalsIgnoreCase(newTaskObj.Status))){
                          reopenedTaskEscalationIdSet.add(newTaskObj.WhatId);
                      }
               }
        }
        //add what Ids of tasks to base set who's status is marked to complete
        if(!escalationRecordIdSetCompleteTask.isEmpty()){
            allEscalationRecordIdSet.addAll(escalationRecordIdSetCompleteTask);
        }
        if(!reopenedTaskEscalationIdSet.isEmpty()){
            allEscalationRecordIdSet.addAll(reopenedTaskEscalationIdSet);
        }
        //get all task list for escalation record
        for(Escalation__c escalationObj :[SELECT Id, name,status__c,(SELECT Id, Status,IsClosed,recordType.developerName,WhatId FROM Tasks WHERE RecordType.DeveloperName = : ClsApexConstants.ESCALATION_TASK) FROM Escalation__c WHERE Id IN : allEscalationRecordIdSet]){
            for(Task taskObj : escalationObj.Tasks){
                if(escalationToTaskListMap.containsKey(escalationObj)){
                    List<Task> escalationTaskList = escalationToTaskListMap.get(escalationObj);
                    escalationTaskList.add(taskObj);
                }else{
                    escalationToTaskListMap.put(escalationObj, new List<Task>{taskObj});
                }
            }

        }
        if(!escalationRecordIdSetCompleteTask.isEmpty()){
            updateEscalationToResolve(escalationRecordIdSetCompleteTask, escalationToTaskListMap);
        }
        
        if(!reopenedTaskEscalationIdSet.isEmpty()){
            updateEscalationToOpen(reopenedTaskEscalationIdSet, escalationToTaskListMap);
        }
        
    }
    
    /*
     * @Description - This function will mark the escalation status to resolve depending 
     * on the corresponding task statuses
	*/
    public static void updateEscalationToResolve(Set<Id> escalationRecordIdSet, Map<Escalation__c, List<Task>> escalationToTaskListMap){
        List<Escalation__c> escalationUpdateList = new List<Escalation__c>();
        boolean canUpdateToResolve;
        for(Id escalationRecId : escalationRecordIdSet){
            canUpdateToResolve = ClsApexConstants.BOOLEAN_TRUE;
            for(Escalation__c escalationObj : escalationToTaskListMap.keySet()){
                if(escalationObj.Id == escalationRecId){
                    List<Task> allEscalationTaskList = escalationToTaskListMap.get(escalationObj);
                    List<Task> closedInCompleteTaskList = new List<Task>();//this holds a list of tasks which are closed but status is not complete
                    for(Task relatedTask : allEscalationTaskList){
                        if(relatedTask.IsClosed && !ClsApexConstants.STATUS_COMPLETED.equalsIgnoreCase(relatedTask.Status)){
                            closedInCompleteTaskList.add(relatedTask);
                        }else if(!relatedTask.IsClosed){//if tasks are not closed then 
                            canUpdateToResolve = ClsApexConstants.BOOLEAN_FALSE;
                        }
                    }
                    if(closedInCompleteTaskList.size() == allEscalationTaskList.size()){//all associated tasks are closed but not completed
                        canUpdateToResolve = ClsApexConstants.BOOLEAN_FALSE;
                    }
                    
                    //there are not any incomplete Escalation Task associated to record
                    if(canUpdateToResolve){
                        Escalation__c escalationObjUpdate = new Escalation__c(Id = escalationRecId);
                        escalationObjUpdate.Status__c = ClsApexConstants.STATUS_RESOLVED;
                        escalationObjUpdate.Escalation_Resolved_Date__c = system.today();
                        escalationUpdateList.add(escalationObjUpdate);
                    }
                }
            }
        }
        if(!escalationUpdateList.isEmpty()){
            Database.update(escalationUpdateList, ClsApexConstants.BOOLEAN_FALSE);
        }
    }
    
    
    /*
     * @Description - This function will Set the Escalation Record to open from Resolved status
     * it blanks Assigned_To_user__c - if assigned to user is filled then another task would be created by escalation trigger
	*/
    public static void updateEscalationToOpen(Set<Id> reopenedTaskEscalationIdSet,Map<Escalation__c, List<Task>> escalationToTaskListMap){
        List<Escalation__c> escalationReopenList = new List<Escalation__c>();
        for(Id reOpenedEscalationId : reopenedTaskEscalationIdSet){
            for(Escalation__c escalationObj : escalationToTaskListMap.keySet()){
                if(escalationObj.Id == reOpenedEscalationId){
                    Escalation__c escalationObjUpdate = new Escalation__c(Id = reOpenedEscalationId);
                    escalationObjUpdate.Status__c = ClsApexConstants.STATUS_OPEN;
                    escalationObjUpdate.Assigned_To_user__c = null;
                    escalationObjUpdate.Escalation_Resolved_Date__c = null;
                    escalationReopenList.add(escalationObjUpdate);
                }
            }
        }
        
        if(!escalationReopenList.isEmpty()){
            Database.update(escalationReopenList, ClsApexConstants.BOOLEAN_FALSE);
        }
    }
    
}