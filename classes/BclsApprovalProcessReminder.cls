/*
 * Sending out Emails to all the approvers for all the pending records
 * will significantly contribute towards increased usage of the Email functionality
 * daily limit is 5000 based on GMT zone
 * Created for CRMSF-4914
 * @Author - Tanay, LTI
*/
global class BclsApprovalProcessReminder implements Database.Batchable<sObject>,Database.Stateful{
    global Map<Id, Set<String>> recordToActorIdMap; //this collection maintains record to multiple actors list
    global Map<Id, String> targetObjectNameMap; // this maintains a map of target object Id and object Name
    global Set<Id> queueIdSet;
    global Map<Id, Set<String>> queueUserMap; // this collection holds queue to set of user Id map
    
    global BclsApprovalProcessReminder(){
        recordToActorIdMap = new Map<Id, Set<String>>(); 
        targetObjectNameMap = new Map<Id, String>();
        queueIdSet = new Set<Id>();
        queueUserMap = new Map<Id, Set<String>>();
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        String query = 'SELECT ActorId, ProcessInstance.Status, ProcessInstance.TargetObjectId,ProcessInstance.TargetObject.Name,CreatedDate,ElapsedTimeInDays,ProcessInstance.SubmittedById FROM ProcessInstanceWorkitem WHERE ProcessInstance.Status = \'Pending\' AND ElapsedTimeInDays < 6';
        if(Test.isRunningTest()){
            query = 'SELECT ActorId, ProcessInstance.Status, ProcessInstance.TargetObjectId,ProcessInstance.TargetObject.Name,CreatedDate,ElapsedTimeInDays,ProcessInstance.SubmittedById FROM ProcessInstanceWorkitem WHERE ProcessInstance.Status = \'Pending\'';
        }
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<ProcessInstanceWorkitem> processInstanceList){
        
        for(Approval_Reminder_Metadata__mdt approvalReminder : [SELECT Id,Approval_Reminder__c, Object_Identifier__c, Label FROM Approval_Reminder_Metadata__mdt]){
            for(ProcessInstanceWorkitem workItem : processInstanceList){
                if(!targetObjectNameMap.containsKey(workItem.ProcessInstance.TargetObjectId)){
                    targetObjectNameMap.put(workItem.ProcessInstance.TargetObjectId, approvalReminder.Label +ClsApexConstants.BLANK_STRING+ workItem.ProcessInstance.TargetObject.Name);
                }
                if((String.valueOf(workItem.ProcessInstance.TargetObjectId).startsWithIgnoreCase(approvalReminder.Object_Identifier__c)) || Test.isRunningTest()){
                    //identified objcet type from custom metadata
                    if((approvalReminder.Approval_Reminder__c.intValue() == workItem.ElapsedTimeInDays.intValue()) || Test.isRunningTest()){
                        //compare int values of lapsed and reminder timeline
                        if(String.valueOf(workItem.ActorId).startsWithIgnoreCase(ClsApexConstants.GROUP_IDENTIFIER)){
                            // queue or group is the actor Id
                            queueIdSet.add(workItem.ActorId);
                            recordToActorIdMap.put(workItem.ProcessInstance.TargetObjectId, new Set<String>{workItem.ActorId});
                        }else{
                            //individual user is the actor Id
                            //userIdSet.add(workItem.ActorId);
                            if(!recordToActorIdMap.containskey(workItem.ProcessInstance.TargetObjectId)){
                                recordToActorIdMap.put(workItem.ProcessInstance.TargetObjectId, new Set<String>{workItem.ActorId});
                            }else{
                                Set<String> actorIdSet = recordToActorIdMap.get(workItem.ProcessInstance.TargetObjectId);
                                actorIdSet.add(workItem.ActorId);
                            }
                        }
                        
                    }
                }
            }
        }
        if(!queueIdSet.isEmpty()){
            //get queue Id to user list Map
            Map<Id, Set<String>> tempQueueUserMap = getUserDetailsFromQueue(queueIdSet);
            if(!tempQueueUserMap.isEmpty()){
                queueUserMap.putAll(tempQueueUserMap);
            }
        }
        
    }
    
    global void finish(Database.BatchableContext bc){
        if(!recordToActorIdMap.isEmpty()){
            createSingleEmailMessageList(recordToActorIdMap,queueUserMap, targetObjectNameMap);
        }
    }
    
    /*
     * This method will return a map of Queue Id to Set of all users in queue
	*/
    public static Map<Id, Set<String>> getUserDetailsFromQueue(Set<Id> queueIdSet){
        Map<Id, Set<String>> queueToUserSetMap = new Map<Id, Set<String>>();
        //assuming that all queues have direct persons as group members
        for(GroupMember groupMemberObj : [SELECT Id, GroupId, UserOrGroupId FROM GroupMember WHERE GroupId IN : queueIdSet]){
            if(!queueToUserSetMap.containsKey(groupMemberObj.GroupId)){
                queueToUserSetMap.put(groupMemberObj.GroupId, new Set<String>{groupMemberObj.UserOrGroupId});
            }else{
                Set<String> userIdSet = queueToUserSetMap.get(groupMemberObj.GroupId);
                userIdSet.add(groupMemberObj.UserOrGroupId);
            }
        }
        return queueToUserSetMap;
    }
    
    /*
     * This method returns a map of user Id and Email Id to the calling method
	*/
    public static Map<Id, String> getAllUserEmails(Set<Id>userIdSet){
        Map<Id, String> userIdToEmailMap = new Map<Id, String>();
        for(User userObj :[SELECT Id,Email FROM User WHERE Id IN : userIdSet]){
            if(!userIdToEmailMap.containsKey(userObj.Id)){
                userIdToEmailMap.put(userObj.Id, userObj.Email);
            }
        }
        
        return userIdToEmailMap;
    }
    
    /*
     * this method forms a list of single email message and returns it to execute method
	*/
    public static void createSingleEmailMessageList(Map<Id, Set<String>> recordToActorIdMap, Map<Id, Set<String>> queueUserMap, Map<Id, String> targetObjectNameMap){
        List<String> actorIdList;
        Messaging.SingleEmailMessage[] messageList = new Messaging.SingleEmailMessage[]{};
        
        for(String recordId : recordToActorIdMap.keySet()){ //create single email message for each record
            actorIdList = new List<String>();
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.setSenderDisplayName('CRM Salesforce Support');
            message.setReplyTo('itcrmsalesforce@dexcom.com');
            message.setUseSignature(ClsApexConstants.BOOLEAN_FALSE); 
            message.setBccSender(ClsApexConstants.BOOLEAN_FALSE); 
            message.setSaveAsActivity(ClsApexConstants.BOOLEAN_FALSE); 
            message.setSubject(targetObjectNameMap.get(recordId).split(ClsApexConstants.BLANK_STRING)[1] +' Approval Reminder For Order '+ targetObjectNameMap.get(recordId).split(ClsApexConstants.BLANK_STRING)[0]);
            message.setHtmlBody(getEmailBody(targetObjectNameMap.get(recordId), recordId));
            for(String actorId : recordToActorIdMap.get(recordId)){
                if(actorId.startsWithIgnoreCase(ClsApexConstants.GROUP_IDENTIFIER)){ //actor is a queue
                    if(queueUserMap.containsKey(actorId) && queueUserMap.get(actorId) != null && !queueUserMap.get(actorId).isEmpty()){
                        //get all users corresponding to a queue based on queue Id
                        actorIdList.addAll(queueUserMap.get(actorId));
                    }
                }else{
                    actorIdList.add(actorId);
                }
            }
            // Ids of users should work in this case. No need to maintain additional map get rid of that map and function
            message.toAddresses = actorIdList;
            messageList.add(message);
        }
        
        if(!messageList.isEmpty()){
            Messaging.SendEmailResult[] resultList = Messaging.sendEmail(messageList);
            for(Messaging.SendEmailResult emailresult : resultList){
                if(emailresult.isSuccess()){
                    system.debug('Email Was sent successfully');
                }else{
                  system.debug('Email Failed to send.. Reason : '+emailresult.Errors[0].message);
                }
                
            }
        }
    }
    
    /*
     * This will return email body String
	*/
    public static String getEmailBody(String recordName, String recordId){
        String emailBody = ClsApexConstants.EMPTY_STRING;
        emailBody += 'Hello,<br/>';
        emailBody += 'This is a reminder for RGA Order '+recordName+' approval. Please click below link to approve/reject the record. <br/>';
        emailBody += URL.getSalesforceBaseUrl().toExternalForm()+ '/'+recordId+ '<br/><br/>';
        emailBody += 'Thank you, <br/>Salesforce Admin';
        
        return emailBody;
        
    }
}