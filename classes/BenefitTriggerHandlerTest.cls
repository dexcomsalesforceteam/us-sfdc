@isTest
private class BenefitTriggerHandlerTest {
    public static Account accConsumer = new Account();
    public static Account accPayor = new Account();
    public static Benefits__c bnf = new Benefits__c();
    public static Benefits__c bnf2 = new Benefits__c();
    

    @testSetup static void setup() {
        List<Account> accListToInsert = new List<Account>();
        Id recIdConsumer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Consumers').getRecordTypeId();
        accConsumer = TestDataBuilder.getAccountListConsumer(1, recIdConsumer)[0];
            //insert accConsumer;
            accListToInsert.add(accConsumer);
        
        Id recIdPayor = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
        accPayor = TestDataBuilder.getAccountList(1, recIdPayor)[0];
        accPayor.Name = 'Humana';
            //insert accPayor;
            accListToInsert.add(accPayor);
        insert accListToInsert;
        Test.startTest();
        List<Benefits__c> benefitList = new List<Benefits__c>();
        Id recId = Schema.SObjectType.Benefits__c.getRecordTypeInfosByName().get('Benefits').getRecordTypeId();
        bnf = TestDataBuilder.getBenefits(accConsumer.Id, accPayor.Id, 1, 'Primary', recId)[0];
        bnf.Employer_Group__c = 'X3330001';
            benefitList.add(bnf);
        bnf2 = TestDataBuilder.getBenefits(accConsumer.Id, accPayor.Id, 1, 'Secondary', recId)[0];
            benefitList.add(bnf2);
        if(!benefitList.isEmpty()){
            insert benefitList;
        }
        Test.stopTest();
    }
    
    @isTest static void testInsert(){
        test.startTest();
            Account consumer = [SELECT Id, Payor__c FROM Account WHERE RecordType.Name =: 'Consumers' LIMIT 1];
            Account payor = [SELECT Id, Payor__c FROM Account WHERE RecordType.Name =: 'Payor' LIMIT 1];
            Benefits__c benef = [SELECT Id, Payor__c, Account__c FROM Benefits__c WHERE Benefit_Hierarchy__c =: 'Primary' LIMIT 1];
            
            //system.assertEquals(benef.Payor__c, consumer.Payor__c);
        test.stopTest();
    }
    
    @isTest static void testAfterUpdate(){
        /*
         * create a user with Data Integrator profile
         * we are getting a custom exception in updating benefit record
         * Benefit_Update_Hold - validation rule
        */
        Profile p = [SELECT Id FROM Profile WHERE Name='Data Integrator'];
        User dataIntgUser = new User(Alias = 'newUser', Email='newDataIntgUser@testorg.com',
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
        LocaleSidKey='en_US', ProfileId = p.Id,
        TimeZoneSidKey='America/Los_Angeles', UserName='newDataIntgUser@testorg.com');
        
        
            Account consumer = [SELECT Id, Payor__c FROM Account WHERE RecordType.Name =: 'Consumers' LIMIT 1];
            Account payor = [SELECT Id, Payor__c FROM Account WHERE RecordType.Name =: 'Payor' LIMIT 1];
            List<Benefits__c> benefList = [SELECT Id, Payor__c, Account__c FROM Benefits__c WHERE Benefit_Hierarchy__c =: 'Primary' LIMIT 1];
            List<Benefits__c> toBeUpdated = new List<Benefits__c>();
            
            Id recIdPayor = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
            Account accPayorNew = TestDataBuilder.getAccountList(1, recIdPayor)[0];
                insert accPayorNew;
            
            for(Benefits__c benefi : benefList){
                benefi.Payor__c = accPayorNew.Id;
                toBeUpdated.add(benefi);
            }
        test.startTest();
        system.runAs(dataIntgUser){
            update toBeUpdated;
        }

            List<Account> payorNew = [SELECT Id, Payor__c FROM Account WHERE Id =: accPayorNew.Id];
            List<Benefits__c> benef = [SELECT Id, Payor__c, Account__c FROM Benefits__c WHERE Id =: toBeUpdated[0].Id];
            system.assertEquals(benef[0].Payor__c, payorNew[0].Id);
        test.stopTest();
    }
    
    @isTest static void testUpdate(){
        
            try{
            Account consumer = [SELECT Id, Payor__c FROM Account WHERE RecordType.Name =: 'Consumers' LIMIT 1];
            Account payor = [SELECT Id, Payor__c FROM Account WHERE RecordType.Name =: 'Payor' LIMIT 1];
            List<Benefits__c> benefList = [SELECT Id, Payor__c, Account__c FROM Benefits__c WHERE Benefit_Hierarchy__c =: 'Primary' LIMIT 1];
            List<Benefits__c> toBeUpdated = new List<Benefits__c>();
            
            Id recIdPayor = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
            Account accPayorNew = TestDataBuilder.getAccountList(1, recIdPayor)[0];
                insert accPayorNew;
            
            for(Benefits__c benefi : benefList){
                benefi.Payor__c = accPayorNew.Id;
                benefi.Benefit_Hierarchy__c = 'Secondary';
                benefi.Create_New_Benefit_Record__c = true;
                bnf.RID_IC__c = '1';
                bnf.RID_P1__c = '2';
                bnf.RID_P2__c = '3';
                bnf.RID_PA__c = '4';
                toBeUpdated.add(benefi);
            }
            test.startTest();
            update toBeUpdated;
            
            List<Account> payorNew = [SELECT Id, Payor__c FROM Account WHERE Id =: accPayorNew.Id];
            List<Benefits__c> benef = [SELECT Id, Payor__c, Account__c FROM Benefits__c WHERE Id =: toBeUpdated[0].Id];
            system.assertEquals(benef[0].Payor__c, payorNew[0].Id);
            system.assertEquals(benef[0].Create_New_Benefit_Record__c, false);
            system.assertEquals(benef[0].RID_IC__c, null);
            system.assertEquals(benef[0].RID_P1__c, null);
            system.assertEquals(benef[0].RID_P2__c, null);
            system.assertEquals(benef[0].RID_PA__c, null);
            }catch(Exception e){
                Boolean expectedExceptionThrown =  e.getMessage().contains('There is already a benefit record of that Benefit Hierarchy type') ? true : false;
                
                //System.AssertEquals(expectedExceptionThrown, true);
                System.AssertEquals(expectedExceptionThrown, false);
            }
        test.stopTest();
    }
    
    @isTest static void testInsertNotPrimary(){
        test.startTest();
            Account consumer = [SELECT Id, Payor__c FROM Account WHERE RecordType.Name =: 'Consumers' LIMIT 1];
            Account payor = [SELECT Id, Payor__c FROM Account WHERE RecordType.Name =: 'Payor' LIMIT 1];
            Benefits__c benef = [SELECT Id, Is_Update_In_Progress__c, Payor__c, Switch_With__c, Account__c FROM Benefits__c WHERE Benefit_Hierarchy__c =: 'Secondary' LIMIT 1];
            benef.Switch_With__c = 'Primary';
            benef.Is_Update_In_Progress__c = ClsApexConstants.BOOLEAN_FALSE;
            Update benef;
            
            //system.assertEquals(benef.Payor__c, consumer.Payor__c);
        test.stopTest();
    }
}