/*
 * This test class is created to cover oreder audit trigger
 * 
*/
@isTest(seeAlldata = false)
public class OrderAuditTriggerHandlerTest {

    @testSetUp
    public static void testSetUp(){
        List<Account> accountInsertList = new List<Account>();
        //prescriber
        Account prescriberAccount=new Account();
        prescriberAccount.lastname='prescriber Account';
        prescriberAccount.recordtypeId= ClsApexUtility.getRecordTypeIdByDeveloperName('Account', 'Prescriber');
        prescriberAccount.Inactive__c =false;        
        accountInsertList.add(prescriberAccount);
        
        //Payor
        Account payorAccount = new Account();
        payorAccount.recordtypeId= ClsApexUtility.getRecordTypeIdByDeveloperName('Account', 'Payor');
        payorAccount.Name = 'Payor';
        payorAccount.Skip_Payor_Matrix__c = true;
        accountInsertList.add(payorAccount);
        Database.insert(accountInsertList);              
        
        //patient
        Account testAccount=TestDataBuilder.testAccount();
        testAccount.Default_Price_Book__c=Test.getStandardPricebookId();
        testAccount.PersonEmail = 'Test@gmail.comQCHoldServices';
        testAccount.BillingStreet='abc';
        testAccount.BillingCity ='Bang';
        testAccount.BillingState ='KN';
        testAccount.BillingPostalCode='560037';
        testAccount.ShippingStreet  ='Bang';
        testAccount.ShippingCity='Bang';
        testAccount.ShippingState='KN';
        testAccount.ShippingPostalCode='560037';
        testAccount.Party_ID__c ='123344';
        testAccount.Prescribers__c = prescriberAccount.Id;
        testAccount.Payor__c = payorAccount.Id;
        testAccount.ShippingCountry ='US';
        testAccount.BillingCountry ='US';
        testAccount.RecordTypeId = ClsApexUtility.getRecordTypeIdByDeveloperName('Account', 'Consumers');
        insert testAccount;
        
        //Primary_Benefit__c
        Benefits__c benefitObj = new Benefits__c();
        benefitObj.PRIOR_AUTH_REQUIRED__c = 'Y';
        benefitObj.New_Order_Auth_Products__c = 'Receiver;Transmitter;Sensor';
        benefitObj.Reorder_Auth_Products__c = 'Receiver;Transmitter;Sensor';
        benefitObj.Account__c = testAccount.Id;
        benefitObj.Payor__c = payorAccount.Id;
        insert benefitObj;
        List<Address__c> addrList = new List<Address__c>();
        Address__c addrs1 = TestDataBuilder.getAddressList(testAccount.Id, true,'SHIP_TO', 1)[0];
        addrs1.Oracle_Address_ID__c ='123455';
        addrs1.Address_Verified__c = 'Yes';
        addrList.add(addrs1);
        Address__c addrs2 = TestDataBuilder.getAddressList(testAccount.Id, true,'BILL_TO', 1)[0];
        addrs2.Oracle_Address_ID__c ='123465';
        addrs2.Address_Verified__c = 'Yes';
        addrList.add(addrs2);
        Database.insert(addrList);
        
        testAccount.Primary_Benefit__c = benefitObj.Id;
        testAccount.Primary_Bill_To_Address__c = addrs2.Id;
        testAccount.Primary_Ship_To_Address__c = addrs1.Id;
        update testAccount;
        Test.startTest();
        Order orderObj = new Order();
        orderObj.AccountId = testAccount.Id;
        orderObj.Type = 'Standard Sales Order';
        orderObj.Shipping_Address__c = addrs1.Id;
        orderObj.Status = 'Draft';
        orderObj.EffectiveDate = system.today()-10;
        insert orderObj;
        
        Order_Audit__c orderAudit = new Order_Audit__c();
        orderAudit.Order_Number__c = orderObj.Id;
        orderAudit.Audit_Area__c = 'Benefit';
        orderAudit.Audit_Field__c = 'Benefit - BI Verified';
        insert orderAudit;
        Test.stopTest();
    }
    
    @isTest
    public static void afterUpdateEventTest(){
        Order_Audit__c orderAudit = [Select Id, Audit_Area__c, Audit_Field__c, Audit_Verified__c from Order_Audit__c where Audit_Area__c = 'Benefit' and Audit_Field__c = 'Benefit - BI Verified' limit 1];
        Test.startTest();
        orderAudit.Audit_Verified__c = true;
        update orderAudit;
        Test.stopTest();
    }
}