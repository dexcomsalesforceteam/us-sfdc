public with sharing class CtrlBICalculator {
    @AuraEnabled
    public static Map<String, String> getAccountBIDetails  (String accountId, String opptyId, String generation, String shipmentDuration) {
        Map<String, String> fieldValueMap = new Map<String, String>();
        Boolean kCodeAccount = false;//CRMSF-3356 : Kcode
        
        List<Benefits__c> primaryBenefitsList = new List<Benefits__c>([SELECT Coverage__c,Insurer__r.Name, Plan_Name__c, MEMBER_ID__c,Account__r.Primary_Ship_To_Address__r.State__c, Account__r.Authorization_for_BI_Calculator__c, Account__r.Is_MDCR_Patient__c, Account__r.Is_BI_Active__c, CO_PAY__c, Copay_Line_Item__c, INDIVIDUAL_DEDUCTIBLE__c, INDIVIDUAL_MET__c, INDIVIDUAL_OOP_MAX__c, INDIVIDUAL_OOP_MET__c, FAMILY_DEDUCT__c, Family_Met__c, FAMILY_OOP_MAX__c, FAMILY_OOP_MET__c, Last_Benefits_Check_Date__c, PRIOR_AUTH_REQUIRED__c, Days_Since_Last_Benefit_Check__c FROM Benefits__c WHERE Benefit_Hierarchy__c = 'Primary' AND Account__c = :accountId]);
        List<Benefits__c> secondaryBenefitsList = new List<Benefits__c>([SELECT Id, Account__c FROM Benefits__c WHERE Benefit_Hierarchy__c = 'Secondary' AND Account__c = :accountId]);
        //CRMSF-3356 : Kcode
        List<Account> accountList = new List<Account>([SELECT Id, Payor__r.Shipment_Duration__c, Primary_Plan_Type__c, Payor__r.Shipment_Duration_Med_Adv__c, Customer_Type__c, Consumer_Payor_Code__c, G4_Exception_Flag__c, Payor__r.Quadax_Payor_Code__c FROM Account WHERE Id = :accountId]);
        
        //Prepare a set with Account Ids which have secondary benefit
        Set<Id> secondaryBenefitAccountIds = new Set<Id>();
        for(Benefits__c secondaryBenefit : secondaryBenefitsList)
        {
            secondaryBenefitAccountIds.add(secondaryBenefit.Account__c);
        }
        
        if(!primaryBenefitsList.isEmpty())
        {
            Benefits__c benefit = primaryBenefitsList[0];
            if(benefit.Last_Benefits_Check_Date__c != null)
            {
                system.debug('is it true? ' +benefit.Account__r.Authorization_for_BI_Calculator__c);
                if(benefit.Account__r.Authorization_for_BI_Calculator__c)
                {
                system.debug('This is for Carecentrix - Zero in all values');
                Boolean PrioirAuthRequired; 
                String formattedLastBenefitsCheckDate;
                String formattedIndDeductible = String.valueOf(benefit.INDIVIDUAL_DEDUCTIBLE__c) != null?'$'+benefit.INDIVIDUAL_DEDUCTIBLE__c: '';
                String formattedIndDeductibleMet = String.valueOf(benefit.INDIVIDUAL_MET__c) != null?'$'+benefit.INDIVIDUAL_MET__c: '';
                String formattedIndOOPMax = String.valueOf(benefit.INDIVIDUAL_OOP_MAX__c) != null?'$'+benefit.INDIVIDUAL_OOP_MAX__c: '';
                String formattedIndOOPMaxMet = String.valueOf(benefit.INDIVIDUAL_OOP_MET__c) != null?'$'+benefit.INDIVIDUAL_OOP_MET__c: '';                
                
                formattedLastBenefitsCheckDate = benefit.Last_Benefits_Check_Date__c.month() + '/' + benefit.Last_Benefits_Check_Date__c.day() + '/' + benefit.Last_Benefits_Check_Date__c.year();   
                fieldValueMap.put('lastBICheckDate', formattedLastBenefitsCheckDate);
                Integer noOfDays = benefit.Last_Benefits_Check_Date__c.daysBetween(date.today());
                system.debug('benefit.Account__r.Is_BI_Active__c : ' + benefit.Account__r.Is_BI_Active__c);
                fieldValueMap.put('isLastBICheckDateValid', String.valueOf(benefit.Account__r.Is_BI_Active__c));
                fieldValueMap.put('coverage', '100');
                fieldValueMap.put('ZeroValues',  '0');    // this is to update estimated cost for insurance
                fieldValueMap.put('copayDOS',  '0');
                fieldValueMap.put('copayLineItem', String.valueOf(benefit.Copay_Line_Item__c));
                //Individual Deductible
                fieldValueMap.put('formattedIndDeductible', 'Individual Deductible :' + formattedIndDeductible);
                fieldValueMap.put('indDeductible', '0'); 
                //Individual Deductible Met
                fieldValueMap.put('formattedIndDeductibleMet', 'Individual Deductible Met : ' + formattedIndDeductibleMet); 
                fieldValueMap.put('indDeductibleMet', '0'); 
                //Individual OOP Max
                fieldValueMap.put('formattedIndOOPMax', 'Individual OOP Max : ' + formattedIndOOPMax); 
                fieldValueMap.put('indOOPMax', '0'); 
                //Individual OOP Max
                fieldValueMap.put('formattedIndOOPMaxMet', 'Individual OOP Max Met : ' + formattedIndOOPMaxMet); 
                fieldValueMap.put('indOOPMaxMet', '0'); 
                //Family Deductible
                fieldValueMap.put('famDeductible', '0'); 
                //Family Deductible Met
                fieldValueMap.put('famDeductibleMet', '0'); 
                //Family OOP Max
                fieldValueMap.put('famOOPMax', '0'); 
                //Family OOP Max Met
                fieldValueMap.put('famOOPMaxMet', '0'); 
                
                //Prior Auth Required
                //Added by AP for BI Prior Auth Change
                String Auth = 'N';
                    PrioirAuthRequired = priorAuthRequired(benefit);
                 If(PrioirAuthRequired)
                 {
                     Auth = 'Y';
                     system.debug('we checked if auth required');
                 }
               // fieldValueMap.put('priorAuthRequired', benefit.PRIOR_AUTH_REQUIRED__c); 
                fieldValueMap.put('priorAuthRequired', Auth); 
                //Check if account has secondary benefit
                if(secondaryBenefitAccountIds.contains(accountId))
                    fieldValueMap.put('secondaryBenefitExist', 'TRUE');             
                
                }
                Else
                {
                fieldValueMap.put('ZeroValues',  '1');    // this is to update estimated cost for insurance
                String formattedLastBenefitsCheckDate;
                String formattedIndDeductible = String.valueOf(benefit.INDIVIDUAL_DEDUCTIBLE__c) != null?'$'+benefit.INDIVIDUAL_DEDUCTIBLE__c: '';
                String formattedIndDeductibleMet = String.valueOf(benefit.INDIVIDUAL_MET__c) != null?'$'+benefit.INDIVIDUAL_MET__c: '';
                String formattedIndOOPMax = String.valueOf(benefit.INDIVIDUAL_OOP_MAX__c) != null?'$'+benefit.INDIVIDUAL_OOP_MAX__c: '';
                String formattedIndOOPMaxMet = String.valueOf(benefit.INDIVIDUAL_OOP_MET__c) != null?'$'+benefit.INDIVIDUAL_OOP_MET__c: '';                
                system.debug('NO This is not for Carecentrix - how it use to be....');
                formattedLastBenefitsCheckDate = benefit.Last_Benefits_Check_Date__c.month() + '/' + benefit.Last_Benefits_Check_Date__c.day() + '/' + benefit.Last_Benefits_Check_Date__c.year();   
                fieldValueMap.put('lastBICheckDate', formattedLastBenefitsCheckDate);
                Integer noOfDays = benefit.Last_Benefits_Check_Date__c.daysBetween(date.today());
                system.debug('benefit.Account__r.Is_BI_Active__c : ' + benefit.Account__r.Is_BI_Active__c);
                fieldValueMap.put('isLastBICheckDateValid', String.valueOf(benefit.Account__r.Is_BI_Active__c));
                fieldValueMap.put('coverage', String.valueOf(benefit.Coverage__c));
                fieldValueMap.put('copayDOS', String.valueOf(benefit.CO_PAY__c));
                fieldValueMap.put('copayLineItem', String.valueOf(benefit.Copay_Line_Item__c));
                //Individual Deductible
                fieldValueMap.put('formattedIndDeductible', 'Individual Deductible : ' + formattedIndDeductible);
                fieldValueMap.put('indDeductible', String.valueOf(benefit.INDIVIDUAL_DEDUCTIBLE__c)); 
                //Individual Deductible Met
                fieldValueMap.put('formattedIndDeductibleMet', 'Individual Deductible Met : ' + formattedIndDeductibleMet); 
                fieldValueMap.put('indDeductibleMet', String.valueOf(benefit.INDIVIDUAL_MET__c)); 
                //Individual OOP Max
                fieldValueMap.put('formattedIndOOPMax', 'Individual OOP Max : ' + formattedIndOOPMax); 
                fieldValueMap.put('indOOPMax', String.valueOf(benefit.INDIVIDUAL_OOP_MAX__c)); 
                //Individual OOP Max
                fieldValueMap.put('formattedIndOOPMaxMet', 'Individual OOP Max Met : ' + formattedIndOOPMaxMet); 
                fieldValueMap.put('indOOPMaxMet', String.valueOf(benefit.INDIVIDUAL_OOP_MET__c)); 
                //Family Deductible
                fieldValueMap.put('famDeductible', String.valueOf(benefit.FAMILY_DEDUCT__c)); 
                //Family Deductible Met
                fieldValueMap.put('famDeductibleMet', String.valueOf(benefit.Family_Met__c)); 
                //Family OOP Max
                fieldValueMap.put('famOOPMax', String.valueOf(benefit.FAMILY_OOP_MAX__c)); 
                //Family OOP Max Met
                fieldValueMap.put('famOOPMaxMet', String.valueOf(benefit.FAMILY_OOP_MET__c)); 
                //Prior Auth Required
                
                fieldValueMap.put('priorAuthRequired', benefit.PRIOR_AUTH_REQUIRED__c); 
                //Check if account has secondary benefit
                if(secondaryBenefitAccountIds.contains(accountId))
                    fieldValueMap.put('secondaryBenefitExist', 'TRUE'); 
            
                }               
                
                //START: Code to distinguish Error Message for displaying BI Calculator depending on Quadax Payor Code
                if(!benefit.Account__r.Is_BI_Active__c){
                    if(!benefit.Account__r.Is_MDCR_Patient__c && 
                        !String.isBlank(accountList[0].Payor__r.Quadax_Payor_Code__c) && 
                        benefit.Days_Since_Last_Benefit_Check__c >= 3){
                            fieldValueMap.put('biCheckOlderThan', '3 days');
                        }else if(!benefit.Account__r.Is_MDCR_Patient__c && 
                                    String.isBlank(accountList[0].Payor__r.Quadax_Payor_Code__c) && 
                                    benefit.Days_Since_Last_Benefit_Check__c >= 30){
                                    fieldValueMap.put('biCheckOlderThan', '30 days');
                                }else{
                                    fieldValueMap.put('biCheckOlderThan', '120 days'); // default condition
                                }
                }
            
                //END
            }
            else
                fieldValueMap.put('isLastBICheckDateValid', 'NULL');
            
            
            //START: CRMSF-3356 :K Code
            Account primaryBenefitAccount = accountList[0]; 
            
            if(String.isBlank(shipmentDuration)){
                if(primaryBenefitAccount.Primary_Plan_Type__c != 'MED ADV' &&
                     primaryBenefitAccount.Primary_Plan_Type__c != 'HMO Medicare Risk'){ 
                      shipmentDuration = primaryBenefitAccount.Payor__r.Shipment_Duration__c;
                    } 
                else if(primaryBenefitAccount.Primary_Plan_Type__c == 'MED ADV' ||
                     primaryBenefitAccount.Primary_Plan_Type__c == 'HMO Medicare Risk'){
                        shipmentDuration = primaryBenefitAccount.Payor__r.Shipment_Duration_Med_Adv__c;
                }   
                fieldValueMap.put('shipmentDuration', shipmentDuration);
                
            }
            if(primaryBenefitAccount.Customer_Type__c == 'Commercial' && 
                primaryBenefitAccount.Consumer_Payor_Code__c == 'K' &&
                primaryBenefitAccount.G4_Exception_Flag__c == false){
                fieldValueMap.put('kCodeAccount', 'TRUE'); 
                generation = 'G6';     
                kCodeAccount = true;                    
                }else{
                   fieldValueMap.put('kCodeAccount', 'FALSE');  
                } //END: CRMSF-3356 :K Code         
            
            Map<String, String> productToPriceMap = getPriceForProducts(accountId); 
            //Get the prices for all G5 products 'STK-GF-001', 'BUN-GF-003', 'STS-GL-041', //Jagan 10/17/17 - Added 'STK-SD-001'
            //Get the prices for all G6 products 'STK-OR-001', 'BUN-OR-TX6', 'STS-OR-003'  //Jagan 04/24/18
            
            if(!productToPriceMap.isEmpty())
            {
                fieldValueMap.put('medicareReceiverPrice', productToPriceMap.get('STK-MD-001'));
                fieldValueMap.put('medicareSubscriptionPrice', productToPriceMap.get('MT-MC-SUB'));
                if(generation == 'G5')
                {
                    fieldValueMap.put('priceBook', productToPriceMap.get('priceBook'));
                    fieldValueMap.put('receiverPrice', productToPriceMap.get('STK-SD-001'));
                    fieldValueMap.put('transmitterPrice', productToPriceMap.get('BUN-GF-003'));
                    fieldValueMap.put('sensorPrice', productToPriceMap.get('STS-GL-041'));
                
                }
                 if(generation == 'G6')
                {
                    fieldValueMap.put('priceBook', productToPriceMap.get('priceBook'));
                    fieldValueMap.put('receiverPrice', productToPriceMap.get('STK-OR-001'));
                    fieldValueMap.put('transmitterPrice', productToPriceMap.get('BUN-OR-TX6'));
                    fieldValueMap.put('sensorPrice', productToPriceMap.get('STS-OR-003'));
                }
                //START: CRMSF-3356 :K Code
                if(kCodeAccount){                   
                    if(shipmentDuration == 'Quarterly' || 
                        shipmentDuration == 'Both'){
                        if(productToPriceMap.containskey('BUN-OR-QRT'))
                            fieldValueMap.put('bundlePrice', productToPriceMap.get('BUN-OR-QRT'));
                        else
                           fieldValueMap.put('bundlePrice', '0');  //Need to check  
                    }if(shipmentDuration == 'Monthly'){
                        if(productToPriceMap.containskey('BUN-OR-MTX'))
                            fieldValueMap.put('bundlePrice', productToPriceMap.get('BUN-OR-MTX'));
                        else
                           fieldValueMap.put('bundlePrice', '0');  //Need to check 
                    }
                    if(String.isBlank(shipmentDuration))
                      fieldValueMap.put('bundlePrice', '0');  //Need to check 
                }//END: CRMSF-3356 :K Code  
            }   
            //Get Oppty Status
            if(opptyId==null){
            	 fieldValueMap.put('canShowBICalculator', string.valueOf(false));     
            } else {
            	fieldValueMap.put('canShowBICalculator', string.valueOf(getOpptyStatus(opptyId)));        
            }
            
         }
        return fieldValueMap;
    }
    
    @AuraEnabled
    public static Boolean priorAuthRequired(Benefits__c benefit)
    {
     // Query data from Custom Meta Data
        String   insurarName = Benefit.Insurer__r.Name;
        Boolean Result = False;
        Prior_Auth_Rule__mdt[] checkPayorMatrix = [SELECT MasterLabel,Auth_Required__c,Member_ID_constraints__c,Plan_Type_Constraints__c FROM Prior_Auth_Rule__mdt where MasterLabel =: insurarName LIMIT 1] ;
        if( !checkPayorMatrix.isEmpty()) // if Auth required is Not TRUE, we look for contraints
        {
            if(!checkPayorMatrix[0].Auth_Required__c)
            {
                if(checkPayorMatrix[0].Plan_Type_Constraints__c != null && Benefit.Plan_Name__c.contains(checkPayorMatrix[0].Plan_Type_Constraints__c) )
                {
                    Result = TRUE; 
                }
                if(checkPayorMatrix[0].Member_ID_constraints__c != null && Benefit.MEMBER_ID__c.startsWithIgnoreCase(checkPayorMatrix[0].Member_ID_constraints__c))
                {
                    Result = TRUE; 
                }
                
            }
            Else // if it is true, Auth will set to Y. 
            {
                Result = TRUE;    
            }
        }
        else{
            Result = TRUE; //  if insurar is not part of Custom Metadata with Carecentrix Payor, It should be Y CRMSF- 3538
        }
     return Result;
    }
    
    
    
    //Method returns the map containing prices for each of the G5 products 'STK-GF-001', 'BUN-GF-003', 'STS-GL-041', 'STK-MD-001', 'MT-MC-SUB'//Jagan 10/17/17 - Added 'STK-SD-001'
    @AuraEnabled
    public static Map<String, String> getPriceForProducts(String accountId)
    {
        Map<String, String> productToPriceMap = new Map<String, String>();
        String priceBookId;
        List<Account> accntList = new List<Account>([SELECT Id, Default_Price_Book__c, Default_Price_Book__r.Name FROM Account WHERE Id = :accountId]);
        if(!accntList.isEmpty())
        {
            Account accnt = accntList[0];
            priceBookId = accnt.Default_Price_Book__c;
            productToPriceMap.put('priceBook', accnt.Default_Price_Book__r.Name);
            if(priceBookId != null && priceBookId != '')
            {
                for(PriceBookEntry pbe : [SELECT ProductCode, UnitPrice FROM PricebookEntry where ProductCode in ('STK-SD-001', 'BUN-GF-003', 'STS-GL-041', 'STK-OR-001', 'BUN-OR-TX6', 'STS-OR-003', 'STK-MD-001', 'MT-MC-SUB', 'BUN-OR-QRT','BUN-OR-MTX') and IsActive = TRUE and IsDeleted = FALSE and pricebook2Id = :priceBookId])
                {
                   /* if(pbe.ProductCode == 'STK-SD-001')
                        productToPriceMap.put('STK-SD-001', String.valueOf(pbe.UnitPrice));
                    if(pbe.ProductCode == 'BUN-GF-003')
                        productToPriceMap.put('BUN-GF-003', String.valueOf(pbe.UnitPrice));
                    if(pbe.ProductCode == 'STS-GL-041')
                        productToPriceMap.put('STS-GL-041', String.valueOf(pbe.UnitPrice));

                    if(pbe.ProductCode == 'STK-OR-001')
                        productToPriceMap.put('STK-OR-001', String.valueOf(pbe.UnitPrice));
                    if(pbe.ProductCode == 'BUN-OR-TX6')
                        productToPriceMap.put('BUN-OR-TX6', String.valueOf(pbe.UnitPrice));
                    if(pbe.ProductCode == 'STS-OR-003')
                        productToPriceMap.put('STS-OR-003', String.valueOf(pbe.UnitPrice));


                    if(pbe.ProductCode == 'STK-MD-001')
                        productToPriceMap.put('STK-MD-001', String.valueOf(pbe.UnitPrice));
                    if(pbe.ProductCode == 'MT-MC-SUB')
                        productToPriceMap.put('MT-MC-SUB', String.valueOf(pbe.UnitPrice)); */
                    productToPriceMap.put(pbe.ProductCode , String.valueOf(pbe.UnitPrice));
                    
                    
                }
            }
        }
        return productToPriceMap;
    }
    
    //Method creates a new Task based on the BI Calculator entries
    @AuraEnabled
    public static void createQuoteTask (String opptyId, String comments) {
        Task newTask = new Task();
        newTask.WhatId = opptyId;
        newTask.Subject ='New Quote Created at ' + system.now() + ' by ' + UserInfo.getName();
        newTask.Description = comments;
        newTask.ActivityDate = system.today();
        newTask.OwnerId = UserInfo.getUserId();
        newTask.Type = 'DxNote';
        newTask.Status = 'Completed';
        insert newTask;
    } 
    
    @AuraEnabled
    public static boolean getOpptyStatus  (String opptyId) {
        Boolean isClosed = [SELECT IsClosed FROM Opportunity WHERE Id = :opptyId].IsClosed;
        return isClosed;
    }
    
    @AuraEnabled
    public static decimal getEstimatedCoPay(Id accountId, decimal totalAmount)
    {
    	ClsSvcEstimatedCoPayReq req= new ClsSvcEstimatedCoPayReq();
        req.accountId=accountId;
        req.products=new List<ClsSvcEstimatedCoPayReq.Product>();
        ClsSvcEstimatedCoPayReq.Product p1= new ClsSvcEstimatedCoPayReq.Product();
        p1.sku='1111'; p1.qty=1; p1.price=totalAmount; req.products.Add(p1);

		ClsSvcEstimatedCoPayRes res=ClsSvcEstimatedCoPay.EstimatedCoPayByAccount(req);
		System.Debug('**** response=' + res);
        if(res.data.estimatedCost!=null){
        	return res.data.estimatedCost;    
        }
      	return 0;
    }
}