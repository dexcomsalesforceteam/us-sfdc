/* V1 */
public with sharing class feedbackForm {

    @AuraEnabled
    public static boolean processFeedback(string subject, string feedback, string chatterGroupID, string recordURL) {


        //create chatter post
        if(recordURL == '/null'){
            recordURL = '/one/one.app';
        }

        FeedItem post = new FeedItem();
        post.ParentID = chatterGroupID; //id of chatter group
        post.Body = feedback;
        post.LinkUrl = recordURL;
        post.Title = 'Link to Record';
        post.Type = 'LinkPost';
        insert post;

        return true;
    }
}