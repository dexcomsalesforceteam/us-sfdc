@isTest
private class OpportuntiyLineItemTriggerHandlerTest {
    
    @isTest static void beforeInsertTest(){
        test.startTest();
        try{
            Id recId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
            Account acc = TestDataBuilder.getAccountList(1, recId)[0];
            insert acc;
            
            Opportunity opp = TestDataBuilder.getOpportunityList(1, acc.Id, acc.Id)[0];
            insert opp;
            
            OpportunityLineItem oppLineItem = TestDataBuilder.getOpportunityLineItem('1', 1, opp.Id);
            insert oppLineItem;
            
        }catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('The product you are trying to add is of a different Generation than products already on the Opportunity') ? true : false;
            System.AssertEquals(expectedExceptionThrown, false);
        }
        test.stopTest();
    }
    //Jagan 10/25/2017 - Inactivated this block of code as this needs to be reengineered.
    /*----------------------------------------------------------------------------------------------------------
@isTest static void exisitingLineItemTest(){
test.startTest();
try{
Id recId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
Account acc = TestDataBuilder.getAccountList(1, recId)[0];
insert acc;

Opportunity opp = TestDataBuilder.getOpportunityList(1, acc.Id, acc.Id)[0];
insert opp;

OpportunityLineItem oppLineItem = TestDataBuilder.getOpportunityLineItem('1', 1, opp.Id);
insert oppLineItem;

OpportunityLineItem oppLineItem2 = TestDataBuilder.getOpportunityLineItem('2', 2, opp.Id);
insert oppLineItem2;
}catch(Exception e){
Boolean expectedExceptionThrown =  e.getMessage().contains('The product you are trying to add is of a different Generation than products already on the Opportunity') ? true : false;
System.AssertEquals(expectedExceptionThrown, true);
}    
test.stopTest();
}
----------------------------------------------------------------------------------------------------------*/
    
    @isTest static void sameLineItemTest(){
        test.startTest();
        try{
            Id recId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
            Account acc = TestDataBuilder.getAccountList(1, recId)[0];
            insert acc;
            
            Opportunity opp = TestDataBuilder.getOpportunityList(1, acc.Id, acc.Id)[0];
            insert opp;
            
            OpportunityLineItem oppLineItem = TestDataBuilder.getOpportunityLineItem('1', 1, opp.Id);
            insert oppLineItem;
            
            OpportunityLineItem oppLineItem3 = TestDataBuilder.getOpportunityLineItem('1', 3, opp.Id);
            insert oppLineItem3;
        }catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('The product you are trying to add is of a different Generation than products already on the Opportunity') ? true : false;
            System.AssertEquals(expectedExceptionThrown, false);
        }
        test.stopTest();
    }
    
    @isTest static void kCodeTest(){
        test.startTest();
        
        //Insert Rules Matrix
        Rules_Matrix__c thisRule = new Rules_Matrix__c();
        thisRule.Consumer_Payor_Code__c = 'K'; 
        thisRule.Quantity_Boxes__c = 3;
        thisRule.Rule_Criteria__c = 'Quarterly Limit';
        thisRule.Is_True_or_False__c = true;
        thisRule.Product_Type__c = '301936586218';
        thisRule.Generation__c = 'G6';
        insert thisRule;
        
        Id payorRecId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
        id recId = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Consumers'].Id;
        
        Account testacc = TestDataBuilder.getAccountList(1, payorRecId)[0];
        testacc.Payor_Code_Commercial__c = 'K';
        insert testacc;
        
        Account consumer = TestDataBuilder.getAccountListConsumer(1, recId)[0];
        consumer.Payor__c = testacc.id;
        insert consumer;
        
        Account acc = [Select Id, Consumer_Payor_Code__c from Account where Id =: consumer.Id limit 1];
        System.debug('Final Code >> ' + acc.Consumer_Payor_Code__c);
        Opportunity opp = TestDataBuilder.getOpportunityList(1, acc.Id, acc.Id)[0];
        insert opp;
        
        Product2 prod = new Product2();
        prod.Name = '301936586218';
        prod.IsActive = true;
        prod.Generation__c = 'G6';
        insert prod;
        
        Id pb = Test.getStandardPricebookID();
        
        PricebookEntry pbe = new PricebookEntry();
        pbe.UseStandardPrice = false;
        pbe.Pricebook2Id=pb;
        pbe.Product2Id=prod.id;
        pbe.IsActive=true;
        pbe.UnitPrice=100.0;
        insert pbe;
        try{
            OpportunityLineItem oppLineItem = new OpportunityLineItem();
            oppLineItem.PricebookEntryId = pbe.Id;
            oppLineItem.Quantity = 1;
            oppLineItem.UnitPrice = 1;
            oppLineItem.OpportunityId = opp.Id;
            oppLineItem.Description = 'test decription';
            insert oppLineItem;
        }catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('Payor has opted out for Testing Supplies') ? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
        }    
        test.stopTest();
    }
    
    @isTest static void kCodeTest1(){
        test.startTest();
        Id payorRecId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
        Account testacc = TestDataBuilder.getAccountList(1, payorRecId)[0];
        testacc.Payor_Code_Commercial__c = 'K';
        insert testacc;
        
        //Insert Rules Matrix
        Rules_Matrix__c thisRule = new Rules_Matrix__c();
        thisRule.Consumer_Payor_Code__c = 'K'; 
        thisRule.Quantity_Boxes__c = 1;
        thisRule.Rule_Criteria__c = 'Quarterly Limit';
        thisRule.Is_True_or_False__c = true;
        thisRule.Product_Type__c = '301936586218';
        thisRule.Generation__c = 'G5';
        thisRule.Account__c = testacc.Id;
        thisRule.Rule_Criteria__c = 'Testing Supplies Opt In';
        thisRule.Is_True_or_False__c = true;
        
        insert thisRule;
        id recId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Consumers').getRecordTypeId();

        Account consumer = TestDataBuilder.getAccountListConsumer(1, recId)[0];
        consumer.Payor__c = testacc.id;
        insert consumer;
        
        Account acc = [Select Id, Consumer_Payor_Code__c from Account where Id =: consumer.Id limit 1];
        System.debug('Final Code >> ' + acc.Consumer_Payor_Code__c);
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opportunity NameX';
        opp.AccountId = acc.Id;
        opp.StageName = 'New Opportunity';
        opp.CloseDate = date.today();
        opp.Type = 'Comp Product Request';
        opp.Payor__c = testacc.Id;
        opp.Type = 'RETAIL PRODUCT';
        opp.Expedited_Shipping__c = true;
        opp.Shipping_Method__c = 'ship to';
        insert opp;
        
        Product2 prod = new Product2();
        prod.Name = '301936586218';
        prod.IsActive = true;
        prod.Generation__c = 'G5';
        insert prod;
        
        Id pb = Test.getStandardPricebookID();
        
        PricebookEntry pbe = new PricebookEntry();
        pbe.UseStandardPrice = false;
        pbe.Pricebook2Id=pb;
        pbe.Product2Id=prod.id;
        pbe.IsActive=true;
        pbe.UnitPrice=100.0;
        insert pbe;
        
        OpportunityLineItem oppLineItem = new OpportunityLineItem();
        oppLineItem.PricebookEntryId = pbe.Id;
        oppLineItem.Quantity = 1;
        oppLineItem.UnitPrice = 1;
        oppLineItem.OpportunityId = opp.Id;
        oppLineItem.Description = 'test decription';
        insert oppLineItem;
        
        OpportunityLineItem oLineItem = new OpportunityLineItem();
        oLineItem.PricebookEntryId = pbe.Id;
        oLineItem.Quantity = 1;
        oLineItem.UnitPrice = 1;
        oLineItem.OpportunityId = opp.Id;
        oLineItem.Description = 'test decription';
        insert oLineItem;
        
        test.stopTest();
    }
    
    @istest
    public static void codeCoverForStaticBooleanValues(){
         UtilityClass.runOnce();
         UtilityClass.runBenefitBeforeTriggerOnce();
         UtilityClass.runBenefitAfterTriggerOnce();
         UtilityClass.runAccountTriggerOnce();
         UtilityClass.runOrderHeaderBeforeTriggerOnce();
         UtilityClass.runOrderHeaderBeforeUpdateTriggerOnce();
         UtilityClass.runOrderHeaderAfterTriggerOnce();
        
    }
    
}