/*
 * @Author : LTI
 * @Description : This test class covers EscalationTriggerHandler class
*/
@isTest(seeAlldata= false)
private class EscalationTriggerHandlerTest {
    
    @testSetup static void setup() {
        
        Account payorAccount=new Account();
        payorAccount.name = 'Test Payor';
        payorAccount.Phone = '123456789';
        payorAccount.DOB__c = Date.newInstance(1992, 04, 23);
        payorAccount.Days_For_Sensor_2_Box_Shipment__c= 2;
        payorAccount.recordtypeid= ClsApexUtility.getRecordTypeIdByDeveloperName('Account','Payor');
        insert payorAccount;
        
        Account testAccount=TestDataBuilder.testAccount();
        testAccount.Payor__c = payorAccount.id;
        testAccount.Customer_Type__c = 'Medicare FFS';
        payorAccount.recordtypeid = ClsApexUtility.getRecordTypeIdByDeveloperName('Account','Consumers');
        insert testAccount;
        
    }
    
    /*
     * This method tests after escalation record insert/update
	*/
    @isTest static void testafterInsertMethod() {
        Account account = [select id, FirstName from account where FirstName='Testfirstname' limit 1];
        Test.startTest();
        Escalation__c escalationObj = new Escalation__c();
        escalationObj.Account__c = account.Id;
        escalationObj.Assigned_To_Group__c = 'Teleperformance';
        escalationObj.Assigned_To_user__c = Userinfo.getUserId();
        escalationObj.Status__c = 'Resolved';
        insert escalationObj;
        List<Task> taskList = new List<Task>([select id,WhatId from Task where WhatId  = :escalationObj.Id]);
        User usr = [Select id, name from user limit 1];
        escalationObj.Assigned_To_user__c = usr.id;
        escalationObj.Status__c = 'Open';
        update escalationObj;
        Test.stopTest();
    }
    
    @isTest
    private static void escalationBeforeUpdateValidationTest(){
        Account account = [select id, FirstName from account where FirstName='Testfirstname' limit 1];
        
        Escalation__c escalationRec = new Escalation__c();
        escalationRec.Account__c = account.Id;
        escalationRec.Assigned_To_Group__c = 'Teleperformance';
        escalationRec.Status__c = 'Open';
        escalationRec.Priority__c = 'Low';
        escalationRec.Escalation_reasons__c = 'Delayed Order - Backorder';
        escalationRec.Subject__c = 'testEscalation';
        escalationRec.Channel__c = 'Email';
        insert escalationRec;
        
        test.startTest();
        List<Task> taskRecList = new List<Task>();
        Task taskobj = new Task();
        taskobj.RecordTypeId = ClsApexUtility.getRecordTypeIdByDeveloperName('Task','Escalation_Task');
        taskObj.status = 'Not Started';
        taskObj.Subject = 'testEscalation';
        taskObj.Priority = 'Medium';
        taskObj.WhatId = escalationRec.Id;
        //insert taskObj;
        taskRecList.add(taskObj);
        Task taskobj2 = new Task();
        taskobj2.RecordTypeId = ClsApexUtility.getRecordTypeIdByDeveloperName('Task','Escalation_Task');
        taskObj2.status = 'Not Started';
        taskObj2.Subject = 'testEscalation';
        taskObj2.Priority = 'Medium';
        taskObj2.WhatId = escalationRec.Id;
        taskRecList.add(taskObj2);
        
        Task taskobj3 = new Task();
        taskobj3.RecordTypeId = ClsApexUtility.getRecordTypeIdByDeveloperName('Task','Escalation_Task');
        taskObj3.status = 'Not Started';
        taskObj3.Subject = 'testEscalation';
        taskObj3.Priority = 'Medium';
        taskObj3.WhatId = escalationRec.Id;
        taskRecList.add(taskObj3);
        
        
        insert taskRecList;
        
        
        escalationRec.Status__c = ClsApexConstants.STATUS_RESOLVED;
        
        try{
            update escalationRec;
        }catch(Exception e){
            system.debug('Escalation record should not be updated');
        }
        test.stopTest();
    }
    
    @isTest
    private static void escalationClosureErrorTest(){
        Account account = [select id, FirstName from account where FirstName='Testfirstname' limit 1];
        
        Escalation__c escalationRec = new Escalation__c();
        escalationRec.Account__c = account.Id;
        escalationRec.Assigned_To_Group__c = 'Teleperformance';
        escalationRec.Status__c = 'Open';
        escalationRec.Priority__c = 'Low';
        escalationRec.Escalation_reasons__c = 'Delayed Order - Backorder';
        escalationRec.Subject__c = 'testEscalation';
        escalationRec.Channel__c = 'Email';
        insert escalationRec;
            
        Task taskobj = new Task();
        taskobj.RecordTypeId = ClsApexUtility.getRecordTypeIdByDeveloperName('Task','Escalation_Task');
        taskObj.status = 'Not Started';
        taskObj.Subject = 'testEscalation';
        taskObj.Priority = 'Medium';
        taskObj.WhatId = escalationRec.Id;
        insert taskObj;
        test.startTest();
        try{
            escalationRec.Status__c = ClsApexConstants.STATUS_RESOLVED;
        }catch(Exception e){
            system.debug('exception should have been there');
        }
        
        test.stopTest();
    }

}