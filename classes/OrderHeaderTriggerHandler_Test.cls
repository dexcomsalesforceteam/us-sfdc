@isTest(seeAllData = false)
public class OrderHeaderTriggerHandler_Test {
    @testSetup 
    public static void createRecords(){
        
        id recId = ClsApexUtility.getRecordTypeIdByDeveloperName(ClsApexConstants.ACCOUNT_API_NAME, ClsApexConstants.ACCOUNT_REC_TYPE_CONSUMERS_LABEL);
        id payorRecId = ClsApexUtility.getRecordTypeIdByDeveloperName(ClsApexConstants.ACCOUNT_API_NAME, ClsApexConstants.ACCOUNT_REC_TYPE_PAYOR_LABEL);
        Account consumer = TestDataBuilder.getAccountListConsumer(1, recId)[0];

        
        Account acc = TestDataBuilder.getAccountList(1,payorRecId)[0];
        
        insert new list <Account>{consumer, acc};
        
        list <Opportunity> oppList = TestDataBuilder.getOpportunityList(5, consumer.Id, acc.Id);
        integer ctr = 0;
        for(Opportunity o : oppList){
            o.Order_NUM__c = '1234'+(ctr++);
        }
        insert oppList;
    }
    
    @isTest
    private static void updateOrderOpptySingleTest(){
        Opportunity opp = [SELECT Id FROM Opportunity WHERE Order_NUM__c = '12340'];
        Account acc = [SELECT Id FROM Account limit 1];
        
        Order_Header__c orderHeader = new Order_Header__c(
            Account__c = acc.Id,
            Order_Id__c = '12340'
        );
        
        test.startTest();
            insert orderHeader;
        test.stopTest();
        
        Order_Header__c orderHeaderNew = [SELECT Id, Opportunity__c FROM Order_Header__c WHERE Id = : orderHeader.Id];
        
        System.assertEquals(opp.Id, orderHeaderNew.Opportunity__c);
    }
    
    @isTest
    private static void updateOrderOpptyBulkTest(){
        map <String, Opportunity> oppMap = new map <String, Opportunity>();
        for(Opportunity opp : [SELECT Id, Order_NUM__c FROM Opportunity  ORDER BY Order_NUM__c ASC]){
            oppMap.put(opp.Order_NUM__c, opp);
        }
        
        Account acc = [SELECT Id FROM Account limit 1];
        
        list <Order_Header__c> orderHeaderList = new list <Order_Header__c>();
        for(integer i = 0; i < 5; i++){
            Order_Header__c orderHeader = new Order_Header__c(
                Account__c = acc.Id,
                Order_Id__c = '1234'+i
            );
            orderHeaderList.add(orderHeader);
        }
        
        
        test.startTest();
            insert orderHeaderList;
        test.stopTest();
                
        for(Order_Header__c oh : [SELECT Id, Opportunity__c, Order_Id__c FROM Order_Header__c]){
            System.assertEquals(oppMap.get(oh.Order_Id__c).id, oh.Opportunity__c);
        }
    }

    /* BEGIN: Unit tests for OrderTriggerHandler.handleShippedUpdate 
    *@author        : Kristen (Sundog)
    *@date          : 07.29.2019
    */
    // Test for Order Header record that does not meet criteria for a Marketing Interaction with Communication Type 'GBIE - Engagement Subscription' or 'GBIE - Engagement'.
    // Reason: Order_Type__c must be 'GB Subscription' or 'IE Subscription' or 'GB Standard Sales Order' or 'IE Standard Sales Order'
    @isTest
    private static void engagementMICase1(){
        Account acc = [SELECT Id FROM Account limit 1];
        // verify there are no sibling OH records or existing MI records
        System.assert([SELECT Id FROM Order_Header__c].size() == 0);
        System.assert([SELECT Id FROM Marketing_Interaction__c].size() == 0);

        // create OH record with invalid order type
        Order_Header__c orderHeader = new Order_Header__c(
            Account__c = acc.Id,
            Order_Id__c = '12340',
            Order_Type__c = 'Invalid Subscription',
            Status__c = ''
        );
        insert orderHeader;

        Test.startTest();
        // update status to shipped to enter trigger method
        orderHeader.Status__c = 'SHIPPED';
        update orderHeader;
        Test.stopTest();

        // verify there are no MI records where created
        System.assert([SELECT Id FROM Marketing_Interaction__c].size() == 0);

    }
    // Test for Order Header record that does not meet criteria for a Marketing Interaction with Communication Type 'GBIE - Engagement Subscription'or 'GBIE - Engagement'.
    // Reason: Status__c must be updated to 'SHIPPED'
    @isTest
    private static void engagementMICase2(){
        List<Order_Header__c> ohList = new List<Order_Header__c>();
        Account acc = [SELECT Id FROM Account limit 1];
        // verify there are no sibling OH records or existing MI records
        System.assert([SELECT Id FROM Order_Header__c].size() == 0);
        System.assert([SELECT Id FROM Marketing_Interaction__c].size() == 0);

        // create order header records
        Order_Header__c orderHeaderIE = new Order_Header__c(
            Account__c = acc.Id,
            Order_Id__c = '12340',
            Order_Type__c = 'IE Subscription',
            Status__c = ''
        );
        ohList.add(orderHeaderIE);
        Order_Header__c orderHeaderGB = new Order_Header__c(
            Account__c = acc.Id,
            Order_Id__c = '12345',
            Order_Type__c = 'GB Subscription',
            Status__c = ''
        );
        ohList.add(orderHeaderGB);
        insert ohList;
        
        Test.startTest();
        // update status to shipped to enter trigger method
        for(Order_Header__c oh : ohList){
            oh.Status__c = 'NOT SHIPPED';
        }
        update ohList;
        Test.stopTest();

        // verify there are no MI records where created
        System.assert([SELECT Id FROM Marketing_Interaction__c].size() == 0);
    }
    // Test for Order Header record that does not meet criteria for a Marketing Interaction with Communication Type 'GBIE - Engagement Subscription' or 'GBIE - Engagement'.
    // Reason: The related Account record has existing MI records with Communication_Type__c 'GBIE - Engagement' or 'GBIE - Engagement Subscription'
    @isTest
    private static void engagementMICase3(){
        List<Order_Header__c> ohList = new List<Order_Header__c>();
        Account acc = [SELECT Id FROM Account limit 1];
        // verify there are no sibling OH records or existing MI records
        System.assert([SELECT Id FROM Order_Header__c].size() == 0);
        System.assert([SELECT Id FROM Marketing_Interaction__c].size() == 0);

        // create conflicting Marketing Interaction record on the account
        Marketing_Interaction__c mi = new Marketing_Interaction__c(
            Account__c = acc.Id,
            Communication_Type__c = 'GBIE - Engagement Subscription'
        );
        insert mi;

        // create Order Header records
        Order_Header__c orderHeaderIE = new Order_Header__c(
            Account__c = acc.Id,
            Order_Id__c = '12340',
            Order_Type__c = 'IE Subscription',
            Status__c = ''
        );
        ohList.add(orderHeaderIE);
        Order_Header__c orderHeaderGB = new Order_Header__c(
            Account__c = acc.Id,
            Order_Id__c = '12345',
            Order_Type__c = 'GB Subscription',
            Status__c = ''
        );
        ohList.add(orderHeaderGB);
        insert ohList;

        Test.startTest();
        // update status to shipped to enter trigger method
        for(Order_Header__c oh : ohList){
            oh.Status__c = 'SHIPPED';
        }
        update ohList;
        Test.stopTest();

        // verify there are no additional MI records where created
        System.assert([SELECT Id FROM Marketing_Interaction__c].size() == 1);
    }
    // Test for Order Header record that does not meet criteria for a Marketing Interaction with Communication Type 'GBIE - Engagement Subscription',
    //    but meets criteria for Communication Type 'GBIE - Engagement'.
    // Reason: Under its parent Account, the Order Header record has sibling records with 'SHIPPED' and
    //     Order_Type = ('GB Standard Sales Order' or  'GB Subscription' or 'IE Standard Sales Order' or 'IE Subscription')
    @isTest
    private static void engagementMICase4(){
        List<Order_Header__c> ohList = new List<Order_Header__c>();
        Account acc = [SELECT Id FROM Account limit 1];
        // verify there are no sibling OH records or existing MI records
        System.assert([SELECT Id FROM Order_Header__c].size() == 0);
        System.assert([SELECT Id FROM Marketing_Interaction__c].size() == 0);

        // create conflicting order header record on the account
        Order_Header__c existingOrderHeader = new Order_Header__c(
            Account__c = acc.Id,
            Order_Id__c = '12342',
            Order_Type__c = 'IE Subscription',
            Status__c = 'SHIPPED'
        );
        insert existingOrderHeader;

        // create Order Header records
        Order_Header__c orderHeaderIE = new Order_Header__c(
            Account__c = acc.Id,
            Order_Id__c = '12340',
            Order_Type__c = 'IE Subscription',
            Status__c = ''
        );
        ohList.add(orderHeaderIE);
        Order_Header__c orderHeaderGB = new Order_Header__c(
            Account__c = acc.Id,
            Order_Id__c = '12345',
            Order_Type__c = 'GB Subscription',
            Status__c = ''
        );
        ohList.add(orderHeaderGB);
        insert ohList;

        Test.startTest();
        // update status to shipped to enter trigger method
        for(Order_Header__c oh : ohList){
            oh.Status__c = 'SHIPPED';
        }
        update ohList;
        Test.stopTest();
        
        // verify there are no 'GBIE - Engagement Subscription' MI records where created
        for(Marketing_Interaction__c mi : [SELECT Communication_Type__c FROM Marketing_Interaction__c]){
            System.assertNotEquals('GBIE - Engagement Subscription', mi.Communication_Type__c);
            System.assertEquals('GBIE - Engagement', mi.Communication_Type__c);
        }
    }
    
    // Test for Order Header record that does not meet criteria for a Marketing Interaction with Communication Type 'GBIE - Engagement'
    //    but meets criteria for Communication Type 'GBIE - Engagement Subscription'.
    // Reason: Order_Type__c is 'GB Subscription', Status__c is 'SHIPPED', there are no 'Engagement' Marketing Interactions or qualifying sibling records under account
    @isTest
    private static void engagementMICase5(){
        List<Order_Header__c> ohList = new List<Order_Header__c>();
        List<Marketing_Interaction__c> miList;
        Account acc = [SELECT Id FROM Account limit 1];
        // verify there are no sibling OH records or existing MI records
        System.assert([SELECT Id FROM Order_Header__c].size() == 0);
        System.assert([SELECT Id FROM Marketing_Interaction__c].size() == 0);

        // create Order Header records
        Order_Header__c orderHeaderIE = new Order_Header__c(
            Account__c = acc.Id,
            Order_Id__c = '12340',
            Order_Type__c = 'IE Subscription',
            Status__c = ''
        );
        ohList.add(orderHeaderIE);
        Order_Header__c orderHeaderGB = new Order_Header__c(
            Account__c = acc.Id,
            Order_Id__c = '12345',
            Order_Type__c = 'GB Subscription',
            Status__c = ''
        );
        ohList.add(orderHeaderGB);
        insert ohList;

        Test.startTest();
        // update status to shipped to enter trigger method
        for(Order_Header__c oh : ohList){
            oh.Status__c = 'SHIPPED';
        }
        update ohList;
        Test.stopTest();

        // verify the expected MI records where created
        miList = [SELECT Id, Communication_Type__c FROM Marketing_Interaction__c];
        //System.assert(miList.size() == 2);

        // verify there are no 'GBIE - Engagement Subscription' MI records where created
        for(Marketing_Interaction__c mi : miList){
            System.assertEquals('GBIE - Engagement Subscription', mi.Communication_Type__c);
            System.assertNotEquals('GBIE - Engagement', mi.Communication_Type__c);
        }
    }

    /* END: Unit tests for OrderTriggerHandler.handleShippedUpdate */

    @IsTest
    static void testOrderConfirmation() {
        Id accountId = [SELECT Id FROM Account LIMIT 1].Id;

        Order_Header__c orderHeader = new Order_Header__c();
        orderHeader.Account__c = accountId;
        orderHeader.Order_Type__c = 'Standard Sales Order';

        insert orderHeader;

        Test.startTest();

        orderHeader.Status__c = 'BOOKED';
        update orderHeader;
        orderHeader.Status__c = 'PROCESSING';
        update orderHeader;

        Test.stopTest();

        List<Marketing_Interaction__c> interactions = [
                SELECT Id
                FROM Marketing_Interaction__c
        ];

        System.assertEquals(1, interactions.size());
    }

    @IsTest
    static void testCustomerOnboarding() {
        Id accountId = [SELECT Id FROM Account WHERE RecordType.Name =: 'Consumers' LIMIT 1].Id;

        Marketing_Account__c marketingAccount = new Marketing_Account__c(Account__c = accountId);
        insert marketingAccount;

        Order_Header__c orderHeader = new Order_Header__c();
        orderHeader.Account__c = accountId;
        orderHeader.Order_Type__c = 'Standard Sales Order';
        insert orderHeader;

        Order_Item_Detail__c detail = new Order_Item_Detail__c();
        detail.Item_Number__c = 'BUN-MC-KIT';
        detail.Shipping_Date__c = Date.today();
        detail.Order_Header__c = orderHeader.Id;
        insert detail;

        Test.startTest();

        orderHeader.Status__c = 'SHIPPED';
        orderHeader.Flow_Status_Code__c = 'CLOSED';

        update orderHeader;

        Test.stopTest();

        List<Marketing_Interaction__c> interactions = [
                SELECT Id, Marketing_Account__c
                FROM Marketing_Interaction__c
                WHERE Communication_Type__c = 'Customer Onboarding'
        ];

        System.assertEquals(1, interactions.size());
        System.assertEquals(marketingAccount.Id, interactions.get(0).Marketing_Account__c);
    }

    @IsTest
    static void testCustomerOnboarding2() {

        Id accountId = [SELECT Id FROM Account WHERE RecordType.Name =: 'Consumers' LIMIT 1].Id;

        Order_Header__c orderHeader = new Order_Header__c();
        orderHeader.Account__c = accountId;
        orderHeader.Order_Type__c = 'Standard Sales Order';

        Order_Header__c orderHeader2 = new Order_Header__c();
        orderHeader2.Account__c = accountId;
        orderHeader2.Order_Type__c = 'Standard Sales Order';
        insert new List<Order_Header__c> {orderHeader, orderHeader2};

        Order_Item_Detail__c detail = new Order_Item_Detail__c();
        detail.Item_Number__c = 'STT-GL-003';
        detail.Shipping_Date__c = Date.today();
        detail.Order_Header__c = orderHeader.Id;

        Order_Item_Detail__c detail2 = new Order_Item_Detail__c();
        detail2.Item_Number__c = 'STT-GL-003';
        detail2.Shipping_Date__c = Date.today().addDays(-1);
        detail2.Order_Header__c = orderHeader2.Id;
        insert new List<Order_Item_Detail__c> {detail, detail2};

        orderHeader2.Status__c = 'SHIPPED';
        orderHeader2.Flow_Status_Code__c = 'CLOSED';
        update orderHeader2;

        Test.startTest();

        orderHeader.Status__c = 'SHIPPED';
        orderHeader.Flow_Status_Code__c = 'CLOSED';

        update orderHeader;

        Test.stopTest();

        List<Marketing_Interaction__c> interactions = [
                SELECT Id
                FROM Marketing_Interaction__c
                WHERE Communication_Type__c = 'Transmitter Pairing'
        ];

        System.assertEquals(1, interactions.size());
    }

    @IsTest
    static void testCustomerOnboarding3() {

        Id accountId = [SELECT Id FROM Account WHERE RecordType.Name =: 'Consumers' LIMIT 1].Id;

        Order_Header__c orderHeader = new Order_Header__c();
        orderHeader.Account__c = accountId;
        orderHeader.Order_Type__c = 'Standard Sales Order';

        Order_Header__c orderHeader2 = new Order_Header__c();
        orderHeader2.Account__c = accountId;
        orderHeader2.Order_Type__c = 'Standard Sales Order';
        insert new List<Order_Header__c> {orderHeader, orderHeader2};

        Order_Item_Detail__c detail = new Order_Item_Detail__c();
        detail.Item_Number__c = 'STT-GL-003';
        detail.Shipping_Date__c = Date.today();
        detail.Order_Header__c = orderHeader.Id;

        Order_Item_Detail__c detail2 = new Order_Item_Detail__c();
        detail2.Item_Number__c = 'STT-GF-001';
        detail2.Shipping_Date__c = Date.today().addDays(-1);
        detail2.Order_Header__c = orderHeader2.Id;
        insert new List<Order_Item_Detail__c> {detail, detail2};

        orderHeader2.Status__c = 'SHIPPED';
        orderHeader2.Flow_Status_Code__c = 'CLOSED';
        update orderHeader2;

        Test.startTest();

        orderHeader.Status__c = 'SHIPPED';
        orderHeader.Flow_Status_Code__c = 'CLOSED';

        update orderHeader;

        Test.stopTest();

        List<Marketing_Interaction__c> interactions = [
                SELECT Id
                FROM Marketing_Interaction__c
                WHERE Communication_Type__c = 'Customer Onboarding'
        ];

        System.assertEquals(1, interactions.size());
    }
    
    @isTest
    public static void beforeInsertTest(){
        Id accountId = [SELECT Id FROM Account WHERE RecordType.Name =: 'Consumers' LIMIT 1].Id;
        
        Order_Header__c orderHeader = new Order_Header__c();
        orderHeader.Account__c = accountId;
        orderHeader.Order_Type__c = 'Standard Sales Order';
        orderHeader.RGA_Approval_Status__c = ClsApexConstants.RGA_APPROVAL_NO_APPROVAL_REQ;
        insert orderHeader;
        
        orderHeader.Integration_Error_Message__c = 'Integration failed to Sync RGA Order';
        Test.startTest();
        update orderHeader;
        Test.stopTest();
    }
}