@isTest
private class OrderHeaderTriggerHandler_Test1{
    @testSetUp static void createRecords(){
      
        // Cash Pricebook for account
           Pricebook2 newPricebookCash = new Pricebook2(Name = 'Cash Price List',IsActive = true,Cash_Price_Book__c = TRUE,Customer_Type__c = 'Cash'  );
           Insert newPricebookCash;
           ID standardPBID = Test.getStandardPricebookId(); 
        
        Product2 prod = new Product2(Name = 'TestSTK-GL-001',IsActive = true, Generation__c = 'G4');
        insert prod;
        List<PricebookEntry> pbEntryList = new List<PricebookEntry>();
        PricebookEntry standardPriceEntry = new PricebookEntry(UseStandardPrice = false,Pricebook2Id=standardPBID,Product2Id=prod.id,IsActive=true,UnitPrice=50.0);
        //insert standardPriceEntry;
        pbEntryList.add(standardPriceEntry);
        PricebookEntry pbe = new PricebookEntry(Pricebook2Id=newPricebookCash.Id,Product2Id=prod.id,IsActive=true,UnitPrice=100.0);
        //insert pbe;
        pbEntryList.add(pbe);
        insert pbEntryList;
           
        //create Prescriber Account
        Id recTypeId=ClsApexUtility.getRecordTypeIdByDeveloperName('Account', 'Prescriber');
        Id rtConsumer= ClsApexUtility.getRecordTypeIdByDeveloperName('Account', 'Consumers');
        account prescriberAccount=new account();
        prescriberAccount.lastname='OrderHeaderTriggerHandler_Test1';
        prescriberAccount.recordtypeId=recTypeId;
        prescriberAccount.Inactive__c =false;        
        insert prescriberAccount;
        
        //create Consumer Account
        Account testAccount=TestDataBuilder.testAccount();
        testAccount.Default_Price_Book__c=newPricebookCash.Id;
        testAccount.BillingStreet='abc';
        testAccount.BillingCity ='Bang';
        testAccount.BillingState ='KN';
        testAccount.BillingPostalCode='560037';
        testAccount.ShippingStreet  ='Bang';
        testAccount.ShippingCity='Bang';
        testAccount.ShippingState='KN';
        testAccount.ShippingPostalCode='560037';
        testAccount.Party_ID__c ='123344';
        testAccount.Prescribers__c=prescriberAccount.id;
        testAccount.RecordTypeId=rtConsumer; 
        testAccount.AccountNumber='013118';  
        insert testAccount;
        Test.startTest();
        //create Primary address
        Address__c addrs2 = TestDataBuilder.getAddressList(testAccount.Id, true,'SHIP_TO', 1)[0];
        addrs2.Oracle_Address_ID__c ='123455';
        insert addrs2;
        
        testAccount.Primary_Ship_To_Address__c =addrs2.Id;
        update testAccount;
        system.debug('-------------------------------testAccount'+testAccount.Primary_Ship_To_Address__c);
        system.debug('------------------------------- addrs2.Oracle_Address_ID__c'+addrs2.Oracle_Address_ID__c);
        
        Order o = new Order();
        o.Name = 'OrderHeaderTriggerHandler_Test1';
        o.Status = 'Draft';
        o.EffectiveDate = system.today();
        o.EndDate = system.today() + 4;
        o.AccountId = testAccount.id;
        o.Price_Book__c =  newPricebookCash.Id ;
        o.Shipping_Address__c = addrs2.id;        
        Insert o;
        
        OrderItem oi1 = new OrderItem();
        oi1.OrderId = o.id;
        oi1.Quantity = 2;
        oi1.Product2id = prod.id;
        oi1.PricebookEntryId=pbe.id;
        oi1.unitPrice = 50.0;
        insert oi1;
        
        Order_Header__c orderHeader = new Order_Header__c(
            Account__c = testAccount.Id,
            Order__c = o.Id
        );
        insert orderHeader;
        Test.stopTest();
        
    }
    @isTest
    static void UpdateShippedDateTest(){
        
        Account acc=[Select Id, Name from Account where Name='OrderHeaderTriggerHandler_Test1' Limit 1];
        Order o1=[Select Id, TotalAmount, Name, Status, EndDate,AccountId from Order Where Name='OrderHeaderTriggerHandler_Test1' And Status='Draft' Limit 1];
        system.debug('**** o1=' +o1);
        Test.startTest();
        Order_Header__c oh1= new Order_Header__c(
            Account__c = acc.Id,
            Order__c = o1.Id
        ); insert oh1;
        oh1.Order_Shipped_Date__c=system.today(); update oh1;
        Test.stopTest();
        system.debug('**** oh1=' +oh1); 
    }    
    
}