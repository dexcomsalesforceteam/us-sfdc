public class ClsUpdateOrderSSIPRecs implements Queueable {
    private List<SSIP_Schedule__c> lstSSIP;
    public clsUpdateOrderSSIPRecs(List<SSIP_Schedule__c> lst) {
        this.lstSSIP=lst;
    }
    
    public void execute(QueueableContext context) {
        update lstSSIP;
    }

}