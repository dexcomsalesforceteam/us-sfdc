/*********************************************************
 * @author      : Kingsley Tumaneng
 * @date        : SEPT 28 2015
 * @description : Test class for AddressTriggerHandler
**********************************************************/
@isTest
private class AddressTriggerHandlerTest {
    @isTest static void testInsertBilling() {
        test.startTest();
        ClsAccountAddressTriggerStaticClass.addressChangeInvokedFromAddress = true;
        ClsAccountAddressTriggerStaticClass.addressChangeInvokedFromAccount = false;
        Id recId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Consumers').getRecordTypeId();
        Account acc = TestDataBuilder.getAccountList2(1, recId)[0];
        insert acc;
        Address__c addrs = TestDataBuilder.getAddressList(acc.Id, true,'Other', 1)[0];
        insert addrs;
        
        List<Account> accList2 = [SELECT Id, BillingStreet, BillingState, BillingCity, BillingCountry, BillingPostalCode
                                  FROM Account WHERE Id =: acc.Id];
        Address__c addrs2 = TestDataBuilder.getAddressList(accList2[0].Id, true,'BILL_TO', 1)[0];
        insert addrs2;
        
        List<Account> accList = [SELECT Id, BillingStreet, BillingState, BillingCity, BillingCountry, BillingPostalCode
                                 FROM Account WHERE Id =: acc.Id];
        List<Address__c> addrsList = [SELECT Id, Account__c, City__c, County__c, State__c, Zip_Postal_Code__c, Street_Address_1__c, Primary_Flag__c, Address_Type__c
                                      FROM Address__c WHERE Id =: addrs2.Id];
        
        system.assertEquals(addrsList[0].City__c,  accList[0].BillingCity);
        //system.assertEquals(addrsList[0].County__c,  accList[0].BillingCountry);
        system.assertEquals(addrsList[0].State__c,  accList[0].BillingState);
        system.assertEquals(addrsList[0].Zip_Postal_Code__c,  accList[0].BillingPostalCode);
        system.assertEquals(addrsList[0].Street_Address_1__c,  accList[0].BillingStreet);
        system.assertEquals(addrsList[0].Primary_Flag__c,  true);
        system.assertEquals(addrsList[0].Address_Type__c,  'BILL_TO');
        //system.assertEquals(addrsList[0].Party_ID__c,  '123');
        test.stopTest();
    }
    
    @isTest static void testInsertShipping() {
        
        ClsAccountAddressTriggerStaticClass.addressChangeInvokedFromAddress = true;
        ClsAccountAddressTriggerStaticClass.addressChangeInvokedFromAccount = false;
        Id recId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Consumers').getRecordTypeId();
        List<Address__c> addressInsertList = new List<Address__c>();
        Account acc = TestDataBuilder.getAccountList2(1, recId)[0];
        insert acc;
        Address__c addrs = TestDataBuilder.getAddressList(acc.Id, true,'BILL_TO', 1)[0];
        //insert addrs;
        addressInsertList.add(addrs);
        
        List<Account> accList2 = [SELECT Id, ShippingStreet, ShippingState, ShippingCity, ShippingCountry, ShippingPostalCode, County__c
                                  FROM Account WHERE Id =: acc.Id];
        Address__c addrs2 = TestDataBuilder.getAddressList(accList2[0].Id, true,'SHIP_TO', 1)[0];
        //insert addrs2;
        addressInsertList.add(addrs2);
        
        Database.insert(addressInsertList);
        
        List<Account> accList = [SELECT Id, ShippingStreet, ShippingState, ShippingCity, ShippingCountry, ShippingPostalCode, County__c
                                 FROM Account WHERE Id =: acc.Id];
        List<Address__c> addrsList = [SELECT Id, Account__c, City__c, County__c, State__c, Zip_Postal_Code__c, Street_Address_1__c, Primary_Flag__c, Address_Type__c
                                      FROM Address__c WHERE Id =: addrs2.Id];
        List<Address__c> newAddList = new List<Address__c> {addrs,addrs2};
            Test.startTest();
        AddressTriggerHandler.afterInsert(newAddList);
        addrs.Street_Address_1__c ='Test 123';
        addrs.Admin__c = true;
        update addrs;
        addrs2.Street_Address_1__c ='Test 987';
        addrs2.Admin__c = true;
        update addrs2;
        
        Map<Id,Address__c> addMap = new Map<Id,Address__c>();
        Map<String, Address__c> mapIdAddress = new Map<String, Address__c>();
        addMap.put(addrs.Id,addrs);
        addMap.put(addrs2.Id,addrs2);
        mapIdAddress.put(addrs2.Id,addrs2);
        AddressTriggerHandler.afterUpdate(newAddList,addMap);
        AddressTriggerHandler.runOnce();
        AddressTriggerHandler.ProcessAccount(mapIdAddress,new Set<Id>{addrs.Id},newAddList,new set<id>{acc.Id});
        
        system.assertEquals(addrsList[0].City__c,  accList[0].ShippingCity);
        //     system.assertEquals(addrsList[0].County__c,  accList[0].County__c);
        system.assertEquals(addrsList[0].State__c,  accList[0].ShippingState);
        system.assertEquals(addrsList[0].Zip_Postal_Code__c,  accList[0].ShippingPostalCode);
        system.assertEquals(addrsList[0].Street_Address_1__c,  accList[0].ShippingStreet);
        system.assertEquals(addrsList[0].Primary_Flag__c,  true);
        system.assertEquals(addrsList[0].Address_Type__c,  'SHIP_TO');
        //system.assertEquals(addrsList[0].Party_ID__c,  '123');
        Test.stopTest();
    }
    
    @isTest static void testUpdateBilling() {
        test.startTest();
		ClsAccountAddressTriggerStaticClass.addressChangeInvokedFromAddress = true;
		ClsAccountAddressTriggerStaticClass.addressChangeInvokedFromAccount = false;
            Id recId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Consumers').getRecordTypeId();
            Account acc = TestDataBuilder.getAccountList2(1, recId)[0];
                insert acc;
            Address__c addrs = TestDataBuilder.getAddressList(acc.Id, false, 'SHIP_TO', 1)[0];
                insert addrs;
            AddressTriggerHandler.run = true;
            
            addrs.Address_Type__c = 'BILL_TO';
            addrs.Primary_Flag__c = true;
            addrs.Account__c = acc.id;
                update addrs;
        Address__c addrsB = TestDataBuilder.getAddressList(acc.Id, false, 'BILL_TO', 1)[0];
                insert addrsB;
            AddressTriggerHandler.run = true;
            
            addrsB.Address_Type__c = 'SHIP_TO';
            addrsB.Primary_Flag__c = true;
            addrsB.Account__c = acc.id;
                update addrsB;
        
                
            List<Account> accList = [SELECT Id, BillingStreet, BillingState, BillingCity, BillingCountry, BillingPostalCode
                                     FROM Account WHERE Id =: acc.Id];
            List<Address__c> addrsList = [SELECT Id, Account__c, City__c, County__c, State__c, Zip_Postal_Code__c, Street_Address_1__c, Primary_Flag__c, Address_Type__c
                                          FROM Address__c WHERE Id =: addrs.Id];
            
            system.assertEquals(addrsList[0].City__c,  accList[0].BillingCity);
            //system.assertEquals(addrsList[0].County__c,  accList[0].BillingCountry);
            system.assertEquals(addrsList[0].State__c,  accList[0].BillingState);
            system.assertEquals(addrsList[0].Zip_Postal_Code__c,  accList[0].BillingPostalCode);
            system.assertEquals(addrsList[0].Street_Address_1__c,  accList[0].BillingStreet);
            system.assertEquals(addrsList[0].Primary_Flag__c,  true);
            system.assertEquals(addrsList[0].Address_Type__c,  'BILL_TO');
            //system.assertEquals(addrsList[0].Party_ID__c,  '123');
        test.stopTest();
    }
    
    @isTest static void catchEx(){
     //   Address__c addrs = new Address__c();
    //    insert addrs;
    }
    /*
    @isTest static void deleteRec(){
        test.startTest();
        try{
            Id recId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Consumers').getRecordTypeId();
            Account acc = TestDataBuilder.getAccountList(1, recId)[0];
                insert acc;
           // Address__c addrs = TestDataBuilder.getAddressList(acc.Id, true, 'SHIP_TO', 1)[0];
           Address__c addrs = new Address__c();
           addrs.Account__c = acc.id;
           addrs.County__c = 'Test County';
           addrs.State__c = 'AZ';
           addrs.Zip_Postal_Code__c = '12345';
           addrs.Street_Address_1__c = 'Test Address';
           addrs.Primary_Flag__c = True;
                insert addrs;
            list<Address__c> addrsList = new list<Address__c>();
            addrsList.add(addrs);
            AddressTriggerHandler.isBeforeDeleted(addrsList);
           // List<Address__c> addrsList = [SELECT Id, Account__c FROM Address__c WHERE Id =: addrs.Id];
           // delete addrsList;
        }catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('You cannot delete a Primary Address.');
            System.AssertEquals(expectedExceptionThrown, true);
        }
        test.stopTest();
    }
    */
}