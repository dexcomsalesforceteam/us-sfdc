/*
Auther      : Kruti bhusan Mohanty
Date        : 07/26/18
Description : If 'Scheduled Shipment Date' is updated on any schedule, Update next schedules according to Frequency on Rule
*/
Public class ClsSSIPScheduleTriggerHandler{
    Public void afterUpdate(Map<Id,SSIP_Schedule__c> schedulesNewMap, Map<Id,SSIP_Schedule__c> schedulesOldMap){
        List<SSIP_Schedule__c> schedulesToProcess = new List<SSIP_Schedule__c>();
        Set<Id> deleteOtherSchedules = new Set<Id>();
        Set<SSIP_Schedule__c> updateOtherSchedules = new Set<SSIP_Schedule__c>();
        Set<Id> ruleIds = new Set<Id>();
        
        //This map will store store information for Rule Id and all scheduled mapped with it.
        Map<Id,List<SSIP_Schedule__c>> ruleSchedulesMap = new Map<Id,List<SSIP_Schedule__c>>();
        //Map holds the type of change for each Scheudle    
        Map<Id, String> dateUpdatedSchedules = new Map<Id, String>();
        
        //Divide the Schedule data as per the respective date, which got updated
        for(SSIP_Schedule__c newSchedule : schedulesNewMap.values()){
            if(newSchedule.Actual_Shipment_Date__c != schedulesOldMap.get(newSchedule.Id).Actual_Shipment_Date__c && newSchedule.Actual_Shipment_Date__c != null){
                dateUpdatedSchedules.put(newSchedule.id, 'Actual'); // if actual date updated on schedule
                schedulesToProcess.add(newSchedule);//Add schedule to process
            }
            else if(newSchedule.Scheduled_Shipment_Date__c != schedulesOldMap.get(newSchedule.Id).Scheduled_Shipment_Date__c && newSchedule.Scheduled_Shipment_Date__c != null){
                dateUpdatedSchedules.put(newSchedule.id,'Scheduled'); // if scheduled shipment date updated on Schedule
                schedulesToProcess.add(newSchedule);//Add schedule to process
            }
            ruleIds.add(newSchedule.SSIP_Rule__c);
        }
        System.debug('dateUpdatedSchedules>>'+dateUpdatedSchedules);
        
        //Fetch and create Map of Rule and all its Schedules
        for(SSIP_Schedule__c otherSchdRecord : [SELECT Id, SSIP_Rule__c,SSIP_Rule__r.Rule_End_Date__c,
                                                Scheduled_Shipment_Date__c,Actual_Shipment_Date__c,Status__c
                                                from SSIP_Schedule__c
                                                where SSIP_Rule__c IN :ruleIds ]){
                                                    if(ruleSchedulesMap.containsKey(otherSchdRecord.SSIP_Rule__c)){
                                                        ruleSchedulesMap.get(otherSchdRecord.SSIP_Rule__c).add(otherSchdRecord);
                                                    }else{
                                                        ruleSchedulesMap.put(otherSchdRecord.SSIP_Rule__c, new List<SSIP_Schedule__c>{otherSchdRecord});
                                                    }
                                                }
        System.debug('ruleSchedulesMap>>' +ruleSchedulesMap);
        
        
        for(SSIP_Schedule__c newScheduleRecord : schedulesToProcess){
            Date DateOfShipment; //updated date on Schedule Record
            if(dateUpdatedSchedules.get(newScheduleRecord.id) == 'Actual')
                DateOfShipment = newScheduleRecord.Actual_Shipment_Date__c;
            else
                DateOfShipment = newScheduleRecord.Scheduled_Shipment_Date__c;
            
            //Calculate difference between old Date and Date Updated
            Integer diffBetweendays = schedulesOldMap.get(newScheduleRecord.id).Scheduled_Shipment_Date__c.daysBetween(DateOfShipment);
            System.debug('diffBetweendays > '+diffBetweendays);
            //Loop through all existing schedule records and delete or update them as per the date difference
            for(SSIP_Schedule__c otherScheduleRecord : ruleSchedulesMap.get(newScheduleRecord.SSIP_Rule__c)){
                if(otherScheduleRecord.id != newScheduleRecord.id && 
                   (otherScheduleRecord.Scheduled_Shipment_Date__c > schedulesOldMap.get(newScheduleRecord.id).Scheduled_Shipment_Date__c ||
                    otherScheduleRecord.Status__c == 'Open' || otherScheduleRecord.Status__c == 'Eligibility Verified' ||
                    otherScheduleRecord.Status__c == 'Eligibility In Review' )){
                        //Update Scheduled Shipment Date on the basis of updated Date    
                        otherScheduleRecord.Scheduled_Shipment_Date__c = otherScheduleRecord.Scheduled_Shipment_Date__c.addDays(diffBetweendays);
                        System.debug('otherScheduleRecord> '+otherScheduleRecord);
                        //If schedule date is exceeding Rule End Date, Delete the Schedule
                        if(otherScheduleRecord.SSIP_Rule__r.Rule_End_Date__c != null && otherScheduleRecord.Scheduled_Shipment_Date__c > otherScheduleRecord.SSIP_Rule__r.Rule_End_Date__c != null){
                            deleteOtherSchedules.add(otherScheduleRecord.Id);
                            System.debug('for future > '+otherScheduleRecord.Id); 
                        }else
                            updateOtherSchedules.add(otherScheduleRecord); 
                    }        
            }       
        }
        //Update Schedules
        if(!updateOtherSchedules.isEmpty()){
            try{
                ClsSSIPApexUtility.checkFirstRun = false;
                Database.update(new List<SSIP_Schedule__c>(updateOtherSchedules));
            }catch(DMLException e){System.debug('Schedule Update Failed .. + e');}
        }
        //Delete future Schedules
        if(!deleteOtherSchedules.isEmpty()){
            //Call future method to avoid record lock
            deletePastScheduleRecords(deleteOtherSchedules);
        }            
    }
    
    @Future
    public static void deletePastScheduleRecords(Set<Id> deleteRecords){
        try{
            //Fetch and delete the schedules
            List<SSIP_Schedule__c> deleteSchedules = new List<SSIP_Schedule__c>([SELECT Id from SSIP_Schedule__c 
                                                                                 where Id IN :deleteRecords]);
            Database.delete(deleteSchedules);
            ClsSSIPApexUtility.checkFirstRun = false;
        }catch(Exception e){
            System.debug('Schedule deletion Failed .. + e');
        }   
    }
    
    public  void ssipScheduleUpdateShippingMethod(List<SSIP_Schedule__c> newSSIPSchedules,Map<Id,SSIP_Schedule__c> ssipScheduleOldMap){
        
        //Default Address for AK and HI State
        Set<Id> addressAKHIUpdate = new Set<Id>();
        Set<Id> addressIds = new Set<Id>();
        
        for(SSIP_Schedule__c thisRecord : newSSIPSchedules){
            addressIds.add(thisRecord.Shipping_Address__c);
        }
        
        for(Address__c thisAddress : [Select Id, State__c from Address__c where Id IN :addressIds ]){
            if(thisAddress.State__c == 'AK' || thisAddress.State__c == 'HI') addressAKHIUpdate.add(thisAddress.Id);
        }
        System.debug('addressAKHIUpdate > '+ addressAKHIUpdate);
        
        if(!addressAKHIUpdate.isEmpty())
            for(SSIP_Schedule__c thisRecord : newSSIPSchedules){
                System.debug('this > '+ thisRecord );
                System.debug('this.Shipping_Address__c > '+ thisRecord.Shipping_Address__c );
                if((addressAKHIUpdate.contains(thisRecord.Shipping_Address__c)) && 
                   (Trigger.isInsert || (Trigger.isUpdate && 
                                         thisRecord.Shipping_Address__c != ssipScheduleOldMap.get(thisRecord.id).Shipping_Address__c))){
                                             //TODO:Need to discuss about update condition here commented by sudheer
                                             thisRecord.Shipping_Method__c = '000001_FEDEX_A_2D';
                                             System.debug('this.Shipping_Method__c > '+ thisRecord.Shipping_Method__c );
                                         }
            }  
    }
    
    //Added by Sudheer Vemula CRMSF-4906CRMSF-4906 : If bypass SSIP checked Then the BI, Auth, CMN, and credit card updated to checked and opportunity closed. 
    public static void ssipByPassCheckedcloseOpps(List<SSIP_Schedule__c> newSSIPSchedules,Map<Id,SSIP_Schedule__c> ssipScheduleOldMap){
        Set<Id> opptyIdsToClose = new Set<Id>();
        for(SSIP_Schedule__c ssip_Schedule:newSSIPSchedules){
            if(ssip_Schedule.Bypass_SSIP_Checks__c == true){
                
                    ssip_Schedule.Is_BI_Active__c          = true;
                    ssip_Schedule.Is_CMN_Active__c         = true;
                    ssip_Schedule.Is_Credit_Card_Active__c = true;
                    ssip_Schedule.Is_Pre_Authorized__c     = true;
                    ssip_Schedule.Status__c                = 'Eligibility Verified';
                    
                if(ssip_Schedule.Opportunity__c != null){
                    opptyIdsToClose.add(ssip_Schedule.Opportunity__c);
                }   
            } 
            if(ssipScheduleOldMap.get(ssip_Schedule.Id).Bypass_SSIP_Checks__c == true && ssip_Schedule.Bypass_SSIP_Checks__c == false){
                ssip_Schedule.Bypass_SSIP_Checks__c.adderror('You can not perform this activity once bypass SSIP Check activated.');
            }
            
        }
        
        /************* START OPTY CLOSURE ****************/ 
        if(!opptyIdsToClose.isEmpty())
        {
            system.debug('Order Closure is Here' +opptyIdsToClose);
            ClsSSIPApexUtility.closeOpportunityforSSIPSch(opptyIdsToClose);
        }
        /************* END OPTY CLOSURE ****************/ 
    }
}