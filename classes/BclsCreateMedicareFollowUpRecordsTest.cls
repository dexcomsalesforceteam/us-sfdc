@isTest 
public class BclsCreateMedicareFollowUpRecordsTest 
{
    @testSetup static void setup() {
        //Get Consumers record type  
        Id consumerRecordTypeId = ClsApexUtility.getRecordTypeIdByDeveloperName('Account', 'Consumers');
        //Get Payor record type  
        Id payorRecordTypeId = ClsApexUtility.getRecordTypeIdByDeveloperName('Account', 'Payor');
        //Create Pricebook
        Pricebook2 reOrderPricebook = new Pricebook2();
        reOrderPricebook.Name = 'M/C - Reorder Price List';
        reOrderPricebook.IsActive = true;
        insert reOrderPricebook;
        //Create Medicare Account 
        List<Account> accountInsertList = new List<Account>(); 
        Account mdcrAccnt = new Account();
        mdcrAccnt.FirstName = 'MDCR FirstName';
        mdcrAccnt.LastName = 'MDCR LastName';
        mdcrAccnt.Customer_Type__c = 'Medicare FFS';
        mdcrAccnt.MDCR_Communication_Preference__c = 'Phone';
        mdcrAccnt.Next_MDCR_Reorder_Follow_up_Date__c = Date.today();
        mdcrAccnt.MDCR_FFS_Subscription__c = 'Active';
        mdcrAccnt.MDCR_Sensors_Count__c = 5;
        mdcrAccnt.MDCR_Test_Strips_Count__c = 3;
        mdcrAccnt.MDCR_Lancets_Count__c = 1;
        mdcrAccnt.RecordtypeId = consumerRecordTypeId;
        mdcrAccnt.DOB__c = Date.newInstance(1992, 04, 23);
        //insert mdcrAccnt;
        accountInsertList.add(mdcrAccnt);
        
        //Create Medicare Account - G6 
        Account mdcrAccnt_G6 = new Account();
        mdcrAccnt_G6.FirstName = 'MDCR FirstName G6';
        mdcrAccnt_G6.LastName = 'MDCR LastName G6';
        mdcrAccnt_G6.Customer_Type__c = 'Medicare FFS';
        mdcrAccnt_G6.MDCR_Communication_Preference__c = 'Phone';
        mdcrAccnt_G6.Next_MDCR_Reorder_Follow_up_Date__c = Date.today();
        mdcrAccnt_G6.MDCR_FFS_Subscription__c = 'Active';
        mdcrAccnt_G6.MDCR_Sensors_Count__c = 5;
        mdcrAccnt_G6.MDCR_Test_Strips_Count__c = 3;
        mdcrAccnt_G6.MDCR_Lancets_Count__c = 1;
        mdcrAccnt_G6.RecordtypeId = consumerRecordTypeId;
        mdcrAccnt_G6.G6_Program__c = 'Medicare G6 Upgrade Patient';
        mdcrAccnt_G6.DOB__c = Date.newInstance(1950, 04, 23);
        accountInsertList.add(mdcrAccnt_G6);
        
        //Create a Payor record
        Account mdcrPayor = new Account();
        mdcrPayor.RecordTypeId = payorRecordTypeId;
        mdcrPayor.Name = 'Medicare Test Payor';
        mdcrPayor.Claim_Address__c = '1 Main Street, San Diego, CA - 92127';
        mdcrPayor.Is_Medicare_Payor__c = true;
        //insert mdcrPayor;
        accountInsertList.add(mdcrPayor);
        insert accountInsertList;
        //Create ship to address
        Address__c mdcrAccntAddress = new Address__c();
        mdcrAccntAddress.Account__c = mdcrAccnt.Id;
        mdcrAccntAddress.Street_Address_1__c = '1 Main Street';
        mdcrAccntAddress.City__c = 'San Diego';
        mdcrAccntAddress.State__c = 'CA';
        mdcrAccntAddress.Zip_Postal_Code__c = '92121';
        mdcrAccntAddress.Country__c = 'USA';
        mdcrAccntAddress.Primary_Flag__c = TRUE;
        mdcrAccntAddress.Address_Type__c = 'SHIP_TO';
        insert mdcrAccntAddress;
        
        //Create ship to address - G6
        Address__c mdcrAccnt_G6Address = new Address__c();
        mdcrAccnt_G6Address.Account__c = mdcrAccnt_G6.Id;
        mdcrAccnt_G6Address.Street_Address_1__c = '2 Main Street';
        mdcrAccnt_G6Address.City__c = 'San Diego';
        mdcrAccnt_G6Address.State__c = 'CA';
        mdcrAccnt_G6Address.Zip_Postal_Code__c = '92121';
        mdcrAccnt_G6Address.Country__c = 'USA';
        mdcrAccnt_G6Address.Primary_Flag__c = TRUE;
        mdcrAccnt_G6Address.Address_Type__c = 'SHIP_TO';
        insert mdcrAccnt_G6Address;
        
        //Create Account Profile Qn
        List<Account_Profile__c> medicareQn = new List<Account_Profile__c>();
        Account_Profile__c qn = new Account_Profile__c();
        qn.Account__c = mdcrAccnt.Id;
        qn.Question__c = 'Have you been diagnosed with Type 1 or Type 2 Diabetes?';
        qn.Answer__c = true;
        qn.External_Id__c = mdcrAccnt.Id + '1';
        qn.Type__c = 'Medicare';
        medicareQn.add(qn);
        
        //Create Account Profile Qn - G6
        Account_Profile__c qn_G6 = new Account_Profile__c();
        qn_G6.Account__c = mdcrAccnt_G6.Id;
        qn_G6.Question__c = 'Have you been diagnosed with Type 1 or Type 2 Diabetes?';
        qn_G6.Answer__c = true;
        qn_G6.External_Id__c = mdcrAccnt_G6.Id + '1';
        qn_G6.Type__c = 'Medicare';
        medicareQn.add(qn_G6);
        
        insert medicareQn;
        
        //Create Benefit Record 
        List<Benefits__c> benefitInsertList = new List<Benefits__c>(); 
        Test.startTest();
        Benefits__c mdcrAccntBenefit = new Benefits__c();
        mdcrAccntBenefit.Benefit_Hierarchy__c = 'Primary';
        mdcrAccntBenefit.Account__c = mdcrAccnt.Id;
        mdcrAccntBenefit.Payor__c = mdcrPayor.Id;
        mdcrAccntBenefit.MEMBER_ID__c = '12343';
        benefitInsertList.add(mdcrAccntBenefit);
        //Create Benefit Record -  G6
        Benefits__c mdcrAccnt_G6Benefit = new Benefits__c();
        mdcrAccnt_G6Benefit.Benefit_Hierarchy__c = 'Primary';
        mdcrAccnt_G6Benefit.Account__c = mdcrAccnt_G6.Id;
        mdcrAccnt_G6Benefit.Payor__c = mdcrPayor.Id;
        mdcrAccnt_G6Benefit.MEMBER_ID__c = '123438';
        benefitInsertList.add(mdcrAccnt_G6Benefit);
        insert benefitInsertList;
        String customerType = [Select Customer_Type__c From Account where Id = : mdcrAccnt.Id ].Customer_Type__c;
        String customerType_G6 = [Select Customer_Type__c From Account where Id = : mdcrAccnt_G6.Id ].Customer_Type__c;
        //system.assertequals('Medicare FFS', customerType);
        //system.assertequals('Medicare FFS', customerType_G6);
        
        //Prepare Products
        List<Product2> mdcrProducts = new List<Product2>();
        Product2 mdcrProduct1 = new Product2();
        mdcrProduct1.Name = 'STS-MC-001';
        mdcrProduct1.Description = 'STS-MC-001';
        mdcrProduct1.IsActive = TRUE;
        mdcrProducts.add(mdcrProduct1);
        Product2 mdcrProduct2 = new Product2();
        mdcrProduct2.Name = '301936586218';
        mdcrProduct2.Description = '301936586218';
        mdcrProduct2.IsActive = TRUE;
        mdcrProducts.add(mdcrProduct2);
        Product2 mdcrProduct3 = new Product2();
        mdcrProduct3.Name = '301937308505';
        mdcrProduct3.Description = '301937308505';
        mdcrProduct3.IsActive = TRUE;
        mdcrProducts.add(mdcrProduct3);
        Product2 mdcrProduct4 = new Product2();
        mdcrProduct4.Name = 'STT-MC-001';
        mdcrProduct4.Description = 'STT-MC-001';
        mdcrProduct4.IsActive = TRUE;
        mdcrProducts.add(mdcrProduct4);
        Product2 mdcrProduct5 = new Product2();
        mdcrProduct5.Name = '301937314218';
        mdcrProduct5.Description = '301937314218';
        mdcrProduct5.IsActive = TRUE;
        mdcrProducts.add(mdcrProduct5);
        Product2 mdcrProduct6 = new Product2();
        mdcrProduct6.Name = 'STK-MC-001';
        mdcrProduct6.Description = 'STK-MC-001';
        mdcrProduct6.IsActive = TRUE;
        mdcrProducts.add(mdcrProduct6);
        Product2 mdcrProduct7 = new Product2();
        mdcrProduct7.Name = '301937818011';
        mdcrProduct7.Description = '301937818011';
        mdcrProduct7.IsActive = TRUE;
        mdcrProducts.add(mdcrProduct7);
        Product2 mdcrProduct8 = new Product2();
        mdcrProduct8.Name = 'MT-MC-SUB';
        mdcrProduct8.Description = 'MT-MC-SUB';
        mdcrProduct8.IsActive = TRUE;
        mdcrProducts.add(mdcrProduct8);
        Product2 mdcrProduct9 = new Product2();
        mdcrProduct9.Name = 'STS-MC-001X';
        mdcrProduct9.Description = 'STS-MC-001X';
        mdcrProduct9.IsActive = TRUE;
        mdcrProducts.add(mdcrProduct9);
        Product2 mdcrProduct10 = new Product2();
        mdcrProduct10.Name = 'MT-OM-SUB';
        mdcrProduct10.Description = 'MT-OM-SUB';
        mdcrProduct10.IsActive = TRUE;
        mdcrProducts.add(mdcrProduct10);
        Product2 mdcrProduct11 = new Product2();
        mdcrProduct11.Name = 'STT-OM-001';
        mdcrProduct11.Description = 'STT-OM-001';
        mdcrProduct11.IsActive = TRUE;
        mdcrProducts.add(mdcrProduct11);
        Product2 mdcrProduct12 = new Product2();
        mdcrProduct12.Name = 'STS-OM-003';
        mdcrProduct12.Description = 'STS-OM-003';
        mdcrProduct12.IsActive = TRUE;
        mdcrProducts.add(mdcrProduct12);
        Product2 mdcrProduct13 = new Product2();
        mdcrProduct13.Name = 'STK-OM-001';
        mdcrProduct13.Description = 'STK-OM-001';
        mdcrProduct13.IsActive = TRUE;
        mdcrProducts.add(mdcrProduct13);
        insert mdcrProducts;
        Test.stopTest();
    }
    @isTest static void TestMedicareFollowupCreation() 
    {
        //Query for the Account record types
        /*List<RecordType> rtypes = [Select Name, Id From RecordType 
where sObjectType='Account' and isActive=true];*/
        
        //Create a map between the Record Type Name and Id 
        
        
        //Test Batch Class
        
        Test.startTest();
        
        BclsCreateMedicareFollowUpRecords obj = new BclsCreateMedicareFollowUpRecords();
        DataBase.executeBatch(obj); 
        
        Test.stopTest();
        
        //ClsApexUtility clsApex = new ClsApexUtility();
        //String testString;
        //clsApex.getMedicarePricebookForAccountShippingState(testString);
    }
    
    @isTest
    public static void TestMedicareFollowupCreation1(){
        String testString;
        ClsApexUtility.getMedicarePricebookForAccountShippingState(testString);
    }
    
    @isTest
    private static void createMedicareFollowUpRecSchdTest(){
        Test.startTest();
        BclsCreateMedicareFollowUpRecordsSched scheduler = new BclsCreateMedicareFollowUpRecordsSched();
        String sch = '0 0 23 * * ?';
        system.schedule('Bclscreate medicare followupRec Schd', sch, scheduler);
        Test.stopTest();
    }
}