/**
 * BclsMarketingCloudRecordDesync
 * Batch to update Desynchronize_With_Marketing_Cloud__c field on qualifying Lead and Account records  
 * @author Kristen(Perficient)
 * @date 07/15/2019
 * @date 01/17/2020 (Terry Luschen / Perficient - Do not delete if a recent opportunity exixts)
 */

 global class BclsMarketingCloudRecordDesync implements Database.Batchable<sObject>, Database.Stateful{
     //Implement Database.RaisesPlatformEvents at some point to write out errors
    global enum BatchType{LEAD, ACCOUNT1, ACCOUNT2}
    global BatchType bt {get; set;}
    public static integer batchSize = 10;
    private Date LAST_30_DAYS_DATE = Date.today().addDays(-30);
    private Date LAST_24_MONTHS_DATE = Date.today().addMonths(-24);
    public boolean do24HourTest = false;
    //How to test if you want to test 24 hours in a sandbox.
    //BclsMarketingCloudRecordDesync batchJob = new BclsMarketingCloudRecordDesync(BatchType.ACCOUNT);
    //batchJob.do24HourTest = true;
    //Database.executeBatch(batchJob);
    global BclsMarketingCloudRecordDesync(){
        this.bt = BatchType.LEAD;
    }

    global BclsMarketingCloudRecordDesync(BatchType bt){
        this.bt = bt;
    }

     global Database.QueryLocator start(Database.BatchableContext bc){
         //if a batch run is already processing, abort the job
        Id currentClassId = [SELECT Id FROM ApexClass WHERE Name = 'BclsHourlyDoubleOptInBatch' LIMIT 1].Id;
        List<AsyncApexJob> jobs = [SELECT Id, Status, ApexClassID 
                                   FROM AsyncApexJob 
                                   WHERE ApexClassID = :currentClassId and Status = 'Processing'];
        if (jobs.size() > 0) {
            System.abortJob(bc.getJobId());
        }
        Organization org = [select Id, IsSandbox from Organization limit 1];
        if(org.IsSandbox && !Test.isRunningTest()){
            LAST_24_MONTHS_DATE = Date.today().addMonths(-1);
        }
        if(do24HourTest){
             LAST_24_MONTHS_DATE = Date.today().addDays(-1);
             LAST_30_DAYS_DATE = Date.today().addDays(-1);
        }
        set<string> idsToTest = new set<string>();
         /*
         idsToTest.add('0014F00000bd3yaQAA');
         */
        String queryToRun;// = (bt == BatchType.LEAD) ? LEADS_QUERY_STRING : ACCOUNT_QUERY_STRING;
        if(bt == BatchType.LEAD){
            if(idsToTest.size() > 0){
                queryToRun = 'SELECT Id, Desynchronize_With_Marketing_Cloud__c ' +
                 	'FROM Lead ' +
                    'WHERE (RecordType.Name != \'Consumer\' AND IsConverted = false AND Desynchronize_With_Marketing_Cloud__c = false AND ID = :idsToTest) OR (RecordType.Name = \'Consumer\' AND HasOptedOutOfEmail = false ' + 
                    	'AND (Email = null OR (IsConverted = true AND ConvertedDate < :LAST_30_DAYS_DATE) OR CreatedDate < :LAST_24_MONTHS_DATE) ' +
                    	'AND Desynchronize_With_Marketing_Cloud__c = false AND ID = :idsToTest)';
            }else{
                queryToRun = 'SELECT Id, Desynchronize_With_Marketing_Cloud__c ' +
                 	'FROM Lead ' +
                    'WHERE (RecordType.Name != \'Consumer\' AND IsConverted = false AND Desynchronize_With_Marketing_Cloud__c = false) OR (RecordType.Name = \'Consumer\' AND HasOptedOutOfEmail = false ' + 
                    	'AND (Email = null OR (IsConverted = true AND ConvertedDate < :LAST_30_DAYS_DATE) OR CreatedDate < :LAST_24_MONTHS_DATE) ' +
                    	'AND Desynchronize_With_Marketing_Cloud__c = false)';
            }            
         }else if(bt == BatchType.ACCOUNT1){
             //We did have , (SELECT CreatedDate FROM Opportunities) on these to check opportunities, but it was causing 
             //  First error: Apex CPU time limit exceeded errors
             if(idsToTest.size() > 0){
                 queryToRun = 'SELECT Id, PersonEmail, RecordType.Name, Desynchronize_With_Marketing_Cloud__pc ' +
                    'FROM Account ' +
        			'WHERE RecordType.Name != \'Consumers\' AND IsPersonAccount = true AND Desynchronize_With_Marketing_Cloud__pc = false AND ID = :idsToTest';
             }else{
                 queryToRun = 'SELECT Id, PersonEmail, RecordType.Name, Desynchronize_With_Marketing_Cloud__pc ' +
                    'FROM Account ' +
        			'WHERE RecordType.Name != \'Consumers\' AND IsPersonAccount = true AND Desynchronize_With_Marketing_Cloud__pc = false';
             }             
         }else if(bt == BatchType.ACCOUNT2){
             if(idsToTest.size() > 0){
                 queryToRun = 'SELECT Id, PersonEmail, RecordType.Name, Desynchronize_With_Marketing_Cloud__pc ' +
                 	'FROM Account ' +
                    'WHERE RecordType.Name = \'Consumers\' AND IsPersonAccount = true AND PersonHasOptedOutOfEmail = false AND SMS_Pending_Opt_In__c = false AND ID = :idsToTest AND ' + 
                        'SMS_Opt_In_List__c != \'True\' AND SMS_Opt_Out_List__c != \'True\' AND ' + 
                        '(PersonEmail = null OR (Shipping_Date__c = null AND CreatedDate < :LAST_24_MONTHS_DATE) OR ' +
                        '(Shipping_Date__c != null AND Shipping_Date__c < :LAST_24_MONTHS_DATE))' +
                        'AND Desynchronize_With_Marketing_Cloud__pc = false';
             }else{
                 queryToRun = 'SELECT Id, PersonEmail, RecordType.Name, Desynchronize_With_Marketing_Cloud__pc ' +
                 	'FROM Account ' +
                    'WHERE RecordType.Name = \'Consumers\' AND IsPersonAccount = true AND PersonHasOptedOutOfEmail = false AND SMS_Pending_Opt_In__c = false AND ' + 
                        'SMS_Opt_In_List__c != \'True\' AND SMS_Opt_Out_List__c != \'True\' AND ' + 
                        '(PersonEmail = null OR (Shipping_Date__c = null AND CreatedDate < :LAST_24_MONTHS_DATE) OR ' +
                        '(Shipping_Date__c != null AND Shipping_Date__c < :LAST_24_MONTHS_DATE))' +
                        'AND Desynchronize_With_Marketing_Cloud__pc = false';
             }             
         }
        return Database.getQueryLocator(queryToRun);
     }

     global void execute(Database.BatchableContext bc, List<sObject> scope){
         List<Lead> leadList;
         List<Account> accountList;
         boolean recentOpp = false;
         if(bt == BatchType.LEAD){
             leadList = (List<Lead>) scope;
             for(Lead l : leadList){
                 l.Desynchronize_With_Marketing_Cloud__c = true;
             }
             database.update(leadList, false); //Loop through errors at some point, but let's get the majority saved
         }else{
             accountList = (List<Account>) scope;
             map<string, list<Opportunity>> oppMap = getOpps(accountList);   
             list<Account> updateAccountList = new list<Account>();
             for(Account a : accountList){
                 if(a.RecordType.Name != 'Consumers'){
                     a.Desynchronize_With_Marketing_Cloud__pc = true;
                     updateAccountList.add(a);
                 }else{
                     if(string.isBlank(a.PersonEmail)){
                         a.Desynchronize_With_Marketing_Cloud__pc = true;
                         updateAccountList.add(a);
                     }else{
                         recentOpp = false;
                         if(oppMap.containsKey(a.Id)){
                              for(Opportunity opp : oppMap.get(a.Id)){
                                 //If any opportunity is within the last 24 months then we do NOT want to Desync.
                                if(opp.CreatedDate > LAST_24_MONTHS_DATE){
                                    recentOpp = true;
                                    break;
                                }
                             }
                         }                        
                         if(!recentOpp){
                             a.Desynchronize_With_Marketing_Cloud__pc = true;
                             updateAccountList.add(a);
                         }
                     }
                 }                 
             }
             database.update(updateAccountList, false); //Loop through errors at some point, but let's get the majority saved
         }
     }

     global void finish(Database.BatchableContext bc){
         if(bt == BatchType.LEAD && !Test.isRunningTest()){
             Database.executeBatch(new BclsMarketingCloudRecordDesync(BatchType.ACCOUNT1), batchSize);
         }else if(bt == BatchType.ACCOUNT1 && !Test.isRunningTest()){
             BclsMarketingCloudRecordDesync batchItem = new BclsMarketingCloudRecordDesync(BatchType.ACCOUNT2);
             batchItem.do24HourTest = do24HourTest;
             Database.executeBatch(batchItem, batchSize);
         }
     }
     
     public map<string, list<Opportunity>> getOpps(list<Account> accountList){
         map<string, list<Opportunity>> accountIDToOppListMap = new map<string, list<Opportunity>>();
         set<string> accountIDSet = new set<string>();
         for(Account a : accountList){
             accountIDSet.add(a.Id);
         }
         list<Opportunity> oppList = new list<Opportunity>();
         list<Opportunity> oppTempList = new list<Opportunity>();
         oppList = [Select ID, AccountID, CreatedDate
                    From Opportunity
                    Where AccountID = :accountIDSet];
         for(Opportunity oItem : oppList){
             if(accountIDToOppListMap.containsKey(oItem.AccountID)){
                 oppList = accountIDToOppListMap.get(oItem.AccountID);
             }else{
                 oppList = new list<Opportunity>();
             }
             oppList.add(oItem);
             accountIDToOppListMap.put(oItem.AccountID, oppList);
         }
         return accountIDToOppListMap;
     }
 }