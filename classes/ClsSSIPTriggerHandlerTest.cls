/*
  @Author         : LTI
  @Date Created   : 09/26/2018
  @Description    : Handles test cases for SSIP Triggers
*/
@istest
public class ClsSSIPTriggerHandlerTest {
    public static List<Account> thisAccount;
    public static List<Order_Header__c> thisOrder;
    public static SSIP_Rule__c ssipRule;
    
    /*******
     * @This Method will help to prepare test data Required.
     **/
    public static void testData(){
        system.debug('************** Called ClsSSIPTriggerHandlerTest');  
        Id recTypePayorId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
        Id recTypeConsumerId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Consumers').getRecordTypeId();
        
        Account payorAccount=new Account();
        payorAccount.name = 'Test Payor';
        payorAccount.Phone = '123456789';
        payorAccount.DOB__c = Date.newInstance(1992, 04, 23);
        payorAccount.Days_For_Sensor_2_Box_Shipment__c= 2;
        payorAccount.recordtypeid= recTypePayorId;
        payorAccount.Default_Price_Book__c = Test.getStandardPricebookId();
        insert payorAccount;
        
        Account testAccount=TestDataBuilder.testAccount();
        testAccount.Payor__c = payorAccount.id;
        testAccount.Customer_Type__c = 'Commercial';
        testAccount.Default_Price_Book__c = Test.getStandardPricebookId();
        testAccount.recordtypeid = recTypeConsumerId;        
        insert testAccount;
        
        Order_Header__c orderHeader=new Order_Header__c();
        orderHeader.Account__c =testAccount.id;
        orderHeader.Order_Type__c = 'Standard Sales Order';
        insert orderHeader;
        
        List<Order_Item_Detail__c> oidList = new List<Order_Item_Detail__c>();
        Order_Item_Detail__c oid1  = new Order_Item_Detail__c(Item_Number__c = 'BUN-OR-TX6',Generation__c='G6', Order_Header__c = orderHeader.Id,Quantity__c =1,Quantity_Shipped__c =1,Shipping_Date__c =system.today()-185);
        Order_Item_Detail__c oid2  = new Order_Item_Detail__c(Item_Number__c = 'STS-OR-003',Generation__c='G6', Order_Header__c = orderHeader.Id,Quantity__c =3,Quantity_Shipped__c =3,Shipping_Date__c =system.today()-185);
        oidList.add(oid1);
        oidList.add(oid2);
        insert oidList;
        
        //pull data : to get roll up summary fields 
        thisOrder = new List<Order_Header__c>([SELECT Id, Latest_Transmitter_Ship_Date__c from Order_Header__c where Id =:orderHeader.id]);
        thisAccount = new List<Account>([SELECT id, Latest_Transmitter_Ship_Date__c, Next_Possible_Date_For_Transmitter_Order__c, Payor__c FROM Account where Id =:testAccount.Id]);
        
        //create SSIP Rule
        ssipRule = createSSIPRule(thisAccount[0].Id, System.today(), System.today()+360);
        insert ssipRule;
        
    }
    
    /*******
     * @This Method will help to cover SSIP Rule Trigger 
     **/
    @isTest 
    public static void ssipRuleTriggerTest(){
        
        test.startTest(); 
        //get the test data
        testData();
        
        //Validation 1: create another SSIP Rule : overlapping Start, End Dates
        SSIP_Rule__c ssipRuleDuplicate = createSSIPRule(thisAccount[0].Id, System.today()+180, System.today()+360);
        try {
            insert ssipRuleDuplicate;
        } catch (DmlException e) {
            //Assert Error Message
            System.assert( e.getMessage().contains('Insert failed. First exception on ' +
                                                   'row 0; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, ' +
                                                   'There is already one active SSIP Rule exist for the customer for the same Product: []'),
                          e.getMessage() );
        }   
        
        //validation 2: Please Enter First_Shipment_Date__c
        SSIP_Rule__c ssipRuleBlankFSDate = createSSIPRule(thisAccount[0].Id, System.today()+361, System.today()+365);
        try {
            ssipRuleBlankFSDate.First_Shipment_Date__c = null;
            insert ssipRuleBlankFSDate;
        } catch (DmlException e) {
            //Assert Error Message
            System.assert( e.getMessage().contains('Insert failed. First exception on ' +
                                                   'row 0; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, ' +
                                                   'Please enter First Shipment Date.: []'),
                          e.getMessage() );
        }
        
        //Update SSIP Rule End Date
        ssipRule.Rule_End_Date__c = System.today()+50;
        Update ssipRule;
        
        test.stopTest();
    } 
    
    /*******
     * @This Method will help to cover SSIP Schedule Trigger
     **/
    @isTest 
    public static void ssipScheduleTriggerTest1(){
        
        test.startTest(); 
        //get the test data
        testData();
        
        List<SSIP_Schedule__c> ScheduleList = new List<SSIP_Schedule__c>([SELECT Id, Scheduled_Shipment_Date__c, Actual_Shipment_Date__c from SSIP_Schedule__c where SSIP_Rule__c =: ssipRule.id]);
        System.debug('ScheduleList >>' + ScheduleList);
        System.assertEquals(ScheduleList.size() , 2);
        
        //Update Scheduled Shipment Date
        SSIP_Schedule__c thisSchedule = ScheduleList[0];
        System.debug('thisSchedule >>' + thisSchedule.Scheduled_Shipment_Date__c);
        thisSchedule.Scheduled_Shipment_Date__c = thisSchedule.Scheduled_Shipment_Date__c + 180;
        update thisSchedule;
        
        test.stopTest();
    }   
    
    /*******
     * @This Method will help to cover SSIP Schedule Trigger
     **/
    @isTest 
    public static void ssipScheduleTriggerTest2(){
        
        test.startTest(); 
        //get the test data
        testData();
        
        List<SSIP_Schedule__c> ScheduleList = new List<SSIP_Schedule__c>([SELECT Id, Scheduled_Shipment_Date__c, Actual_Shipment_Date__c from SSIP_Schedule__c where SSIP_Rule__c =: ssipRule.id]);
        System.debug('ScheduleList >>' + ScheduleList);
        System.assertEquals(ScheduleList.size() , 2);
        
        //Update Actual Shipment Date
        SSIP_Schedule__c thisSchedule = ScheduleList[0];
        System.debug('thisSchedule >>' + thisSchedule.Scheduled_Shipment_Date__c);
        thisSchedule.Actual_Shipment_Date__c = thisSchedule.Scheduled_Shipment_Date__c + 1;
        update thisSchedule;
        
        test.stopTest();
    }  
    
     public static SSIP_Rule__c createSSIPRule(string accountId, Date startDate, Date endDate){
        
        SSIP_Rule__c ssipRule=new SSIP_Rule__c();
        ssipRule.Account__c = accountId;
        ssipRule.Rule_Start_Date__c = startDate;
        ssipRule.Rule_End_Date__c = endDate;
        ssipRule.Product__c = 'G6 | BUN-OR-TX6 | 1';
        ssipRule.First_Shipment_Date__c= startDate.addDays(1);
        ssipRule.Disclaimer__c =true;
        ssipRule.Frequency_In_Days_Number__c = 180;
        
        return ssipRule;
    } 
    
    @isTest
    public static void ssipRuleTriggerHandlerBeforeInsertTest1(){
        testData();
        test.startTest();
        Rules_Matrix__c ruleMatrixObj = new Rules_Matrix__c();
        ruleMatrixObj.Account__c = thisAccount[0].payor__c;
        ruleMatrixObj.Rule_Criteria__c = 'SSIP Opt In';
        ruleMatrixObj.Duration_In_Days__c = 180;
        insert ruleMatrixObj;
        
        SSIP_Rule__c ssipRuleDuplicate = createSSIPRule(thisAccount[0].Id, System.today()+500, null);
        ssipRuleDuplicate.Product__c = 'G6 | STS-OR-003 | 3';
        ssipRuleDuplicate.First_Shipment_Date__c = null;
        try{
            insert ssipRuleDuplicate;
        }catch(Exception e){
            //Please enter First Shipment assert
        }
        test.stopTest();
    }
}