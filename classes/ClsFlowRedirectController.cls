public class ClsFlowRedirectController {

    public Object RedirectPage() 
  {
      String oppId = ApexPages.currentPage().getParameters().get('oppId');
        List<Order> orderList = [SELECT Id FROM Order Where OpportunityId = : oppId];
        String url = '';
        system.debug('orderList size is ' + orderList.size());
        if(!orderList.isEmpty())
        {
            url = '/' + orderList[0].Id; 
            return new PageReference(url);  
        }
        else{
            url = '/' + oppId;
        }
             
       return new PageReference(url);  
    }
}