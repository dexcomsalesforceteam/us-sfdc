/**
 * Test class for server side controllers CtrlPrescriberDashboard and ClsPrescriberDashboardHelper
 * @author Sundog
 * @date 01/28/2019
 * 
 */ 


@isTest
private class CtrlPrescriberDashboardTest {
    
    @testSetup
    private static void createTestData(){
        User ownerUser;
        User standardUser;
        Account prescriberCommunityAccount;
        Account consumerAccount;
        Account prescriberAccount;
        Account payorAccount;
        Contact prescriberContact;
        
        List<Account> testAccountList = new List<Account>();
        List<Opportunity> patientOpportunitiesList;
        List<Order_Header__c> patientOrdersList = new List<Order_Header__c>();
        
        // necessary accounts to create opportunity and order records related to a consumer and prescriber
        
        //testAccountList.add(consumerAccount);
        
        prescriberAccount = TestDataBuilder.testAccount();
        prescriberAccount.RecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND Name = 'Prescriber'].Id;
        testAccountList.add(prescriberAccount);
        
                
        payorAccount = new Account(
            Name = 'Test Payor',
            Phone = '123456789',
            DOB__c = Date.newInstance(1992, 04, 23),
            RecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND Name = 'Payor'].Id
        );
        testAccountList.add(payorAccount);
        
        Account medicalFacility = new Account(
            Name = 'Test Hospital',
            RecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND Name = 'Medical Facility'].Id
        );
        testAccountList.add(medicalFacility);
        insert testAccountList;
        
        consumerAccount = TestDataBuilder.testAccount();
        consumerAccount.FirstName = 'Test';
        consumerAccount.LastName = 'Patient';
        consumerAccount.Prescribers__c = prescriberAccount.Id;
        consumerAccount.Age_Number__c = Double.valueOf(24);
        consumerAccount.Generation__c = '';
        consumerAccount.Payor__c = payorAccount.Id;
        consumerAccount.marketing_sales_channel__c = 'Dexcom';
        consumerAccount.Medical_Facility__c = medicalFacility.Id;
        consumerAccount.RecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND Name = 'Consumers'].Id;
        insert consumerAccount;
        /*
        consumerAccount = TestDataBuilder.testAccount();
        consumerAccount.FirstName = 'Test';
        consumerAccount.LastName = 'Patient';
        consumerAccount.Prescribers__c = prescriberAccount.Id;
        consumerAccount.RecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND Name = 'Consumers'].Id;
        insert consumerAccount;
        */
        standardUser = TestDataBuilder.getUser('System Administrator', 'testadmin');
        
        
        System.runAs(standardUser){
            ownerUser = TestDataBuilder.getUser('SERVICE', 'CommunityOwner');
            ownerUser.UserRoleId = [SELECT Id FROM UserRole WHERE Name = 'Training'].Id;
            insert ownerUser;
        }

        System.runAs(ownerUser){
            prescriberCommunityAccount = new Account(
                Name = 'Prescriber Community',
                RecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND Name = 'Business Account'].Id
            );
            insert prescriberCommunityAccount;
            
            prescriberContact = new Contact(
                FirstName = 'Test',
                LastName = 'Contact',
                PersonAccountId__c = prescriberAccount.Id,
                AccountId = prescriberCommunityAccount.Id,
                Email = 'sundog@test.com'
            );
            insert prescriberContact;
            
            User prescriberUser = TestDataBuilder.getUser('Prescriber Community User', 'TestPrescriber');
            prescriberUser.ContactId = prescriberContact.Id;
            insert prescriberUser;
        }
        
        patientOpportunitiesList = TestDataBuilder.getOpportunityList(2, consumerAccount.Id, payorAccount.Id);
        for(Opportunity opp : patientOpportunitiesList){
            opp.Sales_Channel__c = 'Dexcom';
            opp.StageName = '1. New Opportunity';
            opp.Order_NUM__c = String.valueOf(Math.ceil(Math.random() * 1000));
            opp.Prescribers__c = prescriberAccount.Id;
        }
        insert patientOpportunitiesList;
        
        for(Integer i = 0; i < 2; i++){
            Order_Header__c order = new Order_Header__c(
                Account__c = consumerAccount.Id,
                Status__c = 'SHIPPED',
                Order_ID__c = '' + i,
                Booked_Date__c = Date.today().addDays(-5)
            );
            patientOrdersList.add(order);
        }
        insert patientOrdersList;
    }
    
    /*
     * calls PrescriberDashboardCtl.getUserInfo, PrescriberDashboardHelper.getOpportunitiesAndOrders,
     * receives a PrescriberDashboardHelper.OpportunitiesAndOrders object in return
     */ 
    @isTest
    private static void getOpportunitiesAndOrdersPositiveTest(){
        ClsPrescriberDashboardHelper.OpportunitiesAndOrders oppsAndOrders;
        Profile prescriberCommunityUserProfile = [SELECT Id FROM Profile WHERE Name = 'Prescriber Community User'];
        User prescriberUser = [SELECT Id FROM User WHERE ProfileId = :prescriberCommunityUserProfile.Id AND Username = 'TestPrescriber@hesser.com'];
        System.runAs(prescriberUser){
            Test.startTest();
            oppsAndOrders = CtrlPrescriberDashboard.getOpportunitiesAndOrders('past-3-months');
            Test.stopTest();
        }
        System.assertEquals(2, oppsAndOrders.opportunities.size());
        System.assertEquals(2, oppsAndOrders.orders.size());
    }
    
    @isTest
    private static void getOpportunitiesAndOrdersNegativeTest(){
        ClsPrescriberDashboardHelper.OpportunitiesAndOrders oppsAndOrders;
        Profile prescriberCommunityUserProfile = [SELECT Id FROM Profile WHERE Name = 'Prescriber Community User'];
        User prescriberUser = [SELECT Id, ContactId FROM User WHERE ProfileId = :prescriberCommunityUserProfile.Id AND Username = 'TestPrescriber@hesser.com'];
        Contact prescriberContact = [SELECT Id, PersonAccountId__c FROM Contact WHERE Id = :prescriberUser.ContactId];
        prescriberContact.PersonAccountId__c = null;
        update prescriberContact;
        
        System.runAs(prescriberUser){
            Test.startTest();
            oppsAndOrders = CtrlPrescriberDashboard.getOpportunitiesAndOrders('past-3-months');
            Test.stopTest();
        }
        
        System.assert(oppsAndOrders == null);
    }
    
}