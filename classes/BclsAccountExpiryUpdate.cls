/**
 *@Author        : Kristen (Sundog)
 *@Date Created  : 15/30/19
 *@Description   : Batch class to push marketing interaction entries for expiration cases
**/

global class  BclsAccountExpiryUpdate implements Database.Batchable<sObject>, Database.Stateful {
    
    global enum BatchType{
        CREDIT_CARD_EXPIRY,
        PRESCRIPTION_EXPIRY,
        CHART_NOTES_EXPIRY,
        PRIOR_AUTH_EXPIRY
    }
    global BatchType bt { get; set; }

    global BclsAccountExpiryUpdate() {
        bt = BatchType.CREDIT_CARD_EXPIRY;
    }
    global BclsAccountExpiryUpdate(BatchType bt) {
        this.bt = bt;
    }
    
    
    // query locator is silly and doesnt like parenthesis on Date methods, so these have to be variables declared separately
    private Date threeDaysAgo = Date.today().addDays(-3);
    private Date inFourDays = Date.today().addDays(4);
    private Date inOneWeek = Date.today().addDays(7);
    private Date inTwoWeeks = Date.today().addDays(14);
    private Set<Id> accountWithBenefitSet = new Set<Id>();
    @TestVisible
    private static String creditCardExpiry = 'Account - Credit Card Expiry';
    @TestVisible
    private static String prescriptionExpiry = 'Account - Prescription Expiry';
    @TestVisible
    private static String chartExpiry = 'Account - Chart Notes Expiry';
    @TestVisible
    private static String authExpiry = 'Account - Prior Auth Expiry';
    private static Set<Id> relatedAccountSet = new Set<Id>();
    
    global Database.QueryLocator start(Database.BatchableContext BC){

        for(Benefits__c b : [SELECT Account__c FROM Benefits__c WHERE Benefit_Hierarchy__c = 'Primary' AND 
                            ((SENSOR_AUTH_END_DATE__c >= :inOneWeek AND SENSOR_AUTH_END_DATE__c <= :inTwoWeeks) OR
                            (TRANSMITTER_AUTH_END_DATE__c >= :inOneWeek AND TRANSMITTER_AUTH_END_DATE__c <= :inTwoWeeks))]){
            accountWithBenefitSet.add(b.Account__c);
        }
        String query = 'SELECT Id, Name, Credit_Card_Expiration_Date__c, CMN_or_Rx_Expiration_Date__c, Chart_Notes_Expiration_Date__c, CreatedDate ';
        query += 'FROM Account WHERE RecordType.Name = \'Consumers\' AND ';
        if (bt == BatchType.CREDIT_CARD_EXPIRY){
            query += '(Credit_Card_Expiration_Date__c >= :threeDaysAgo AND Credit_Card_Expiration_Date__c <= :inFourDays)';
        }
        else if(bt == BatchType.PRESCRIPTION_EXPIRY){
            query += '(CMN_or_Rx_Expiration_Date__c >= :inOneWeek AND CMN_or_Rx_Expiration_Date__c  <= :inTwoWeeks)';
        }
        else if(bt == BatchType.CHART_NOTES_EXPIRY){
            query += '(Chart_Notes_Expiration_Date__c >= :inOneWeek AND Chart_Notes_Expiration_Date__c <= :inTwoWeeks)';
        }
        else if(bt == BatchType.PRIOR_AUTH_EXPIRY){
            query += '(Id IN :accountWithBenefitSet)';
        }
        
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Account> scope){
        Set<Id> accountIdSet = new Set<Id>();
        Set<String> existingMISet = new Set<String>();
        Map<Id, List<Marketing_Interaction__c>> accountToMIList = new Map<Id, List<Marketing_Interaction__c>>();
        List<Marketing_Interaction__c> miToInsert = new List<Marketing_Interaction__c>();
        Set<String> validCommunicationTypeSet = new Set<String>{creditCardExpiry, prescriptionExpiry, chartExpiry, authExpiry};
        for(Account a : scope){
            accountIdSet.add(a.Id);
            System.debug(a.Name);
        }
        
        for(Marketing_Interaction__c mi : [SELECT Account__c, Account__r.Name, Communication_Type__c, CreatedDate FROM Marketing_Interaction__c 
                                           WHERE Account__c IN :accountIdSet
                                          AND CreatedDate > :Datetime.now().addDays(-8) // less than 8 days ago
                                          AND (Communication_Type__c IN :validCommunicationTypeSet)]){
            existingMISet.add(mi.Account__c + mi.Communication_Type__c);
        }
        
        // DEXCOMOPT-683 : only include accounts that have ordered within the last 9 months
        Set<Id> accountsWithOrders = new Set<Id>();
        for(AggregateResult ar : [SELECT AccountId FROM Order WHERE AccountId IN :accountIdSet AND EffectiveDate >= :Date.today().addMonths(-9) GROUP BY AccountId]) {
            accountsWithOrders.add((Id) ar.get('AccountId'));
        }

        for(Account a : scope){
            List<String> commTypesToInsert = new List<String>();
            if(bt == BatchType.CREDIT_CARD_EXPIRY && !existingMISet.contains(a.Id + creditCardExpiry) && accountsWithOrders.contains(a.Id)){
                commTypesToInsert.add(creditCardExpiry);
            }
            if(bt == BatchType.PRESCRIPTION_EXPIRY && !existingMISet.contains(a.Id + prescriptionExpiry) && accountsWithOrders.contains(a.Id)){
                commTypesToInsert.add(prescriptionExpiry);
            }
            if(bt == BatchType.CHART_NOTES_EXPIRY && !existingMISet.contains(a.Id + chartExpiry) && accountsWithOrders.contains(a.Id)){
                commTypesToInsert.add(chartExpiry);
            }
            if(bt == BatchType.PRIOR_AUTH_EXPIRY && !existingMISet.contains(a.Id + authExpiry) && accountsWithOrders.contains(a.Id)){
                commTypesToInsert.add(authExpiry);
            }
            for(String commType : commTypesToInsert){
                miToInsert.add(createMI(a.Id, commType));
            }
        }
        System.debug('miToInsert: ' + miToInsert.size());
        List<Marketing_Account__c> marketingAccounts = [SELECT Id, Account__c FROM Marketing_Account__c WHERE Account__c IN :relatedAccountSet];
        for(Marketing_Interaction__c mi : miToInsert){
            Id maId;
            for(Marketing_Account__c ma : marketingAccounts){
                if(ma.Account__c == mi.Account__c){
                    maId = ma.Id;
                }
            }
            mi.Marketing_Account__c = maId;
        }
        insert miToInsert;
    }
    
    private Marketing_Interaction__c createMI(Id accountId, String commType){
        relatedAccountSet.add(accountId);
        
        Marketing_Interaction__c mi = new Marketing_Interaction__c(
            Account__c = accountId,
            Communication_Type__c = commType
        );
        return mi;
    }
    
    global void finish(Database.BatchableContext BC) {
		if (bt == BatchType.CREDIT_CARD_EXPIRY && !Test.isRunningTest()) {
            Database.executeBatch(new BclsAccountExpiryUpdate(BatchType.PRESCRIPTION_EXPIRY));
        }
        else if(bt == BatchType.PRESCRIPTION_EXPIRY && !Test.isRunningTest()){
            Database.executeBatch(new BclsAccountExpiryUpdate(BatchType.CHART_NOTES_EXPIRY));
        }
        else if(bt == BatchType.CHART_NOTES_EXPIRY && !Test.isRunningTest()){
            Database.executeBatch(new BclsAccountExpiryUpdate(BatchType.PRIOR_AUTH_EXPIRY));
        }
    }
}