@istest
public class ClsQCHoldPayorCheckServiceTest {
    @testSetup static void createRecords(){
        id recId = ClsApexUtility.getRecordTypeIdByDeveloperName('Account','Consumers');
        id prescriberRecId = ClsApexUtility.getRecordTypeIdByDeveloperName('Account','Prescriber');
        id payorRecId = ClsApexUtility.getRecordTypeIdByDeveloperName('Account','Payor');
        id orderRecId =  ClsApexUtility.getRecordTypeIdByDeveloperName('Order','US_Sales_Orders');
        Account prescriber = TestDataBuilder.getAccountListConsumer(1, prescriberRecId)[0];
        insert prescriber;
        Account payor = TestDataBuilder.getAccountList(1, payorRecId)[0];
        payor.Payor_Contract_End_Date__c =system.today()+3;
        payor.Direct_Contract__c = true;
        insert payor;
        Account consumer = TestDataBuilder.getAccountListConsumer(1, recId)[0];
        consumer.BillingCity ='BNG';
        consumer.BillingCountry ='IND';
        consumer.BillingState ='KN';
        consumer.BillingStreet ='abc';
        consumer.BillingPostalCode ='33146';
        consumer.ShippingCity ='BNG';
        consumer.BillingCountry ='US';
        consumer.ShippingCountry ='US';
        consumer.ShippingState ='KN';
        consumer.ShippingStreet ='abc';
        consumer.ShippingPostalCode ='33146';
        consumer.Party_ID__c = '234235';
        consumer.Prescribers__c = prescriber.id ;
        consumer.Payor__c =payor.id;
        consumer.Type_of_Insulin_Therapy__c = 'Insulin using Pump therapy';
        consumer.Diagnosis_Code_1_Acc__c = 'Type 1';
        insert consumer;
        
        List<Address__c> addresses=new List<Address__c>();
        Address__c shipToaddress = TestDataBuilder.getAddressList(consumer.id,true,'SHIP_TO',1)[0];
        Address__c billToaddress = TestDataBuilder.getAddressList(consumer.id,true,'BILL_TO',1)[0];
        addresses.add(shipToaddress);
        addresses.add(billToaddress);
        Test.startTest();
        insert addresses;
        
        Date todayDate= system.today();
        Order o=new Order();
        o.RecordTypeId = orderRecId;
        o.EffectiveDate =todayDate ;
        o.Scheduled_Ship_Date__c = todayDate+2;
        o.AccountId = consumer.Id;
        o.Status = 'Draft';
        o.Shipping_Method__c = '000001_FEDEX_A_3DS';
        o.Shipping_Address__c = addresses[0].id;
        insert o;
        
        Rules_PayorToDiagnosis__c payorToDiagnosis = new Rules_PayorToDiagnosis__c();
        payorToDiagnosis.Payor__c =Payor.Id ;
        payorToDiagnosis.Covered__c =true ;
        payorToDiagnosis.Diagnosis__c ='Type 1' ; 
        // payorToDiagnosis.Diagnosis_Code_Covered__c ='E10641, E10649';
        payorToDiagnosis.MaximumAge__c =50 ;
        payorToDiagnosis.MinimumAge__c =10 ;
        Insert payorToDiagnosis;
        Test.stopTest();
        
    }
    @istest
    public static void getPayorDetailsTest(){
        account acc=[SELECT Id,Age__c,Diagnosis_Code_1_Acc__c,Diagnosis_Type_Formula__c,Type_of_Insulin_Therapy__c,Payor__r.Payor_Contract_End_Date__c,Payor__r.Direct_Contract__c FROM ACCOUNT where recordtype.DeveloperName = 'Consumers' limit 1];
        order o=[select id,name,Type_Of_Order__c from order limit 1];
        Test.startTest();
        ClsQCHoldPayorCheckService.getPayorDetails(o.id,acc.id,o.Type_Of_Order__c,false);
        Test.stopTest();
    }
}