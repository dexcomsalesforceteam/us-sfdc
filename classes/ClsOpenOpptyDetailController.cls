/*
 * Description : This class is created to operate on open opportunities
 * associated to an account
 * Created Date : Aug 30, 2019
 * Created By : LTI
*/

public inherited sharing class ClsOpenOpptyDetailController {
    
    /*
     * Desc : function to fetch open oppty details corresponding
     * to provided account Id
     * @param accountId
     * @return List<Opportunity>
	*/
    @AuraEnabled
    public Static List<Opportunity> getOpenOpptyDetails(String accountId){
        List<Opportunity> opptyDetailsList = new List<Opportunity>();
        opptyDetailsList = [SELECT Id, name, AccountId, account.Name, Status__c, StageName, Type FROM Opportunity WHERE CreatedBy.Email != : ClsApexConstants.SALESFORCE_ADMIN_EMAIL AND IsClosed = : ClsApexConstants.BOOLEAN_FALSE AND AccountId = : accountId];
        return opptyDetailsList;
    }
    
    
    /*
     * Description : This method is to update
     * opportunity records to closed status
     * @param ist<Opportunity>
     * @return
	*/
    @AuraEnabled
    public Static void updateOpptyRecords(List<Opportunity> updateOpptyList){
        
        if(!updateOpptyList.isEmpty()){
            for(Opportunity opptyRec : updateOpptyList){
                opptyRec.StageName = ClsApexConstants.OPPTY_STAGE_QUOTE_APPROVED;
                opptyRec.Status__c = ClsApexConstants.EMPTY_STRING;
                opptyRec.Close_Reason__c = ClsApexConstants.OPP_CLOSE_REASON_SALE;
            }
            try{
                update updateOpptyList;
            }catch(Exception e){
                throw new AuraHandledException(e.getMessage());
            }
            
        }
    }
}