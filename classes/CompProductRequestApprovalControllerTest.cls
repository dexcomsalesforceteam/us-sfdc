@istest
public class CompProductRequestApprovalControllerTest {
    
    @isTest
    static void methodTestProcessRejection() {
        //Submit Quote for Approval
        Comp_Product_Request__c compProdRequest = [select id,Customer_Account_No__c from Comp_Product_Request__c where Customer_Account_No__r.PersonEmail = 'APTestAP@gmail.com' and Fedex_Notes__c=  'Failed Shipment1' limit 1];
        Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitRequest();
        User testUser = getUserToProcess();
        app.setObjectId(compProdRequest.id);
        Approval.ProcessResult result = Approval.process(app);
        Test.StartTest();
        System.runAs(testUser){
            CompProductRequestApprovalController.approveRejectRecords(false, new List<Id>{compProdRequest.Id});            
        }
        Test.StopTest();
    }
    
    @isTest
    static void methodTestProcessApproval() {
        //Submit Quote for Approval
        Comp_Product_Request__c compProdRequest = [select id,Customer_Account_No__c from Comp_Product_Request__c where Customer_Account_No__r.PersonEmail = 'APTestAP@gmail.com' and Fedex_Notes__c=  'Failed Shipment2' limit 1];
        User testUser = getUserToProcess();
        Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitRequest();
        app.setObjectId(compProdRequest.id);
        Approval.ProcessResult result = Approval.process(app);
        Test.StartTest();
        System.runAs(testUser){
            CompProductRequestApprovalController.approveRejectRecords(true, new List<Id>{compProdRequest.Id});            
        }
        Test.StopTest();
    }
    
    @isTest
    static void getPendingCompProdRequestsTest() {
        Comp_Product_Request__c compProdRequest = [select id,Customer_Account_No__c from Comp_Product_Request__c where Customer_Account_No__r.PersonEmail = 'APTestAP@gmail.com' and Fedex_Notes__c=  'Failed Shipment3' limit 1];
        User testUser = getUserToProcess();
        Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitRequest();
        app.setObjectId(compProdRequest.id);
        Approval.ProcessResult result = Approval.process(app);
        Test.StartTest();
        System.runAs(testUser){
            CompProductRequestApprovalController.getPendingCompProdRequests();
        }
        Test.StopTest();
    }
    @testSetUp
    public static void testdata(){
        //create prescriber
        Account testPrescriber = new Account();      
        testPrescriber.RecordTypeId = ClsApexUtility.getRecordTypeIdByDeveloperName('Account', 'Prescriber');
        testPrescriber.FirstName = 'TestPrescriber_FName';
        testPrescriber.LastName = 'TestPrescriber_LName';
        testPrescriber.PersonEmail = 'Fname.LName@gmail.com';
        Insert testPrescriber;
        //create test Account        
        Account testAccount = new Account();
        testAccount.RecordTypeId = ClsApexUtility.getRecordTypeIdByDeveloperName('Account', 'Consumers');
        testAccount.FirstName = 'APTestfirstname';
        testAccount.LastName = 'APTestlastname';
        testAccount.PersonEmail = 'APTestAP@gmail.com';
        testAccount.Phone = '12345678900';
        testAccount.BillingState = 'CA';
        testAccount.BillingCity = 'APTest City';
        testAccount.BillingStreet = 'APTest Street';
        testAccount.BillingPostalCode =  '95112';
        testAccount.BillingCountry = 'US';
        testAccount.ShippingState = 'CA';
        testAccount.ShippingCity = 'APTest City';
        testAccount.ShippingStreet = 'APTest Street';
        testAccount.ShippingPostalCode =  '95112';
        testAccount.ShippingCountry = 'US';
        testAccount.AccountNumber = '1234';
        testAccount.Party_ID__c = '4444';
        testAccount.Prescribers__c = testPrescriber.Id;
        testAccount.CMN_or_Rx_Expiration_Date__c = Date.newInstance(2099, 1, 1);
        insert testAccount;
        List<Address__c> ShipToAddressList = TestDataBuilder.getAddressList(testAccount.id,true,'SHIP_TO',1);
        insert ShipToAddressList;
        id shipToAddressId;
        for(Address__c shipToAddr : ShipToAddressList ){
            shipToAddressId = shipToAddr.id;
        }
        
        system.debug('*******' + testAccount.Primary_Ship_To_Address__c);
        system.debug('*******' + shipToAddressId);
        List<Comp_Product_Request__c> testComp = new List<Comp_Product_Request__c>();
        Comp_Product_Request__c Comp1 = new Comp_Product_Request__c(Customer_Account_No__c = testAccount.id,
                                                                    G5_Receiver__c = '1',
                                                                    G4_Receiver__c ='1',
                                                                    Error_Source__c = 'Fedex',
                                                                    Fedex_Reasons__c = 'other',
                                                                    Shipping_Address__c = shipToAddressId,
                                                                    Fedex_Notes__c = 'Failed Shipment1',
                                                                    Shipping_Method__c = '000001_FEDEX_L_GND',
                                                                    Repmaking_Request__c = UserInfo.getUserId(),
                                                                    Estimated_Amount__c = 4042);
        
        Comp_Product_Request__c Comp2 = new Comp_Product_Request__c(Customer_Account_No__c = testAccount.id,
                                                                    G5_Receiver__c = '1',
                                                                    G4_Receiver__c ='1',
                                                                    Error_Source__c = 'Fedex',
                                                                    Fedex_Reasons__c = 'other',
                                                                    Fedex_Notes__c = 'Failed Shipment2',
                                                                    Shipping_Address__c = shipToAddressId,
                                                                    Shipping_Method__c = '000001_FEDEX_L_GND',
                                                                    Repmaking_Request__c = UserInfo.getUserId(),
                                                                    Estimated_Amount__c = 4042);
        Comp_Product_Request__c comp3 = new Comp_Product_Request__c(Customer_Account_No__c = testAccount.id,
                                                                    G5_Receiver__c = '1',
                                                                    G4_Receiver__c ='1',
                                                                    Error_Source__c = 'Fedex',
                                                                    Fedex_Reasons__c = 'other',
                                                                    Fedex_Notes__c = 'Failed Shipment3',
                                                                    Shipping_Address__c = shipToAddressId,
                                                                    Shipping_Method__c = '000001_FEDEX_L_GND',
                                                                    Repmaking_Request__c = UserInfo.getUserId(),
                                                                    Estimated_Amount__c = 4042);
        testComp.add(Comp1);
        testComp.add(Comp2);
        testComp.add(comp3);
        
        insert testComp; 
    }
    
    /*
     * This method is to get user to run test
    */
    public static User getUserToProcess(){
        Id testUserId = [SELECT UserOrGroupId FROM GroupMember WHERE Group.Type = 'Queue' AND Group.Name LIKE '%Comp Product Request%' limit 1].UserOrGroupId;
        
        return ([SELECT Id from User where Id =:testUserId limit 1]);
    }
}