/********************************************************************************
@Author         : Jagan Periyakaruppan
@Date Created   : 03/14/2018
@Description    : Test class to test the Order object Functionality
*********************************************************************************/
@isTest
private class ClsTestOrderObject {
    
    public static Map<String, Id> customPricebookMap;
    
    @testSetup public static void orderCreationSetUp(){
        Id payorRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
        Id consumerRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Consumers').getRecordTypeId();
        Id prescriberRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prescriber').getRecordTypeId();
        
        //Create Pricebook
        customPricebookMap = ClsTestDataFactory.createCustomPricebook(new List <String>{'Aetna Health Plan'});
        String customPricebookId = customPricebookMap.get('Aetna Health Plan');
        
        
        //Create a consumer and Payor Account 
        List<Account> accntsToBeInserted = new List<Account>();
        Account consumerAccnt = new Account();
        consumerAccnt.FirstName = 'Med Adv FirstName';
        consumerAccnt.LastName = 'Med Adv LastName';
        consumerAccnt.Party_Id__c = '78666567';
        consumerAccnt.G6_Program__c = 'Limited Launch Participant';
        consumerAccnt.DOB__c =Date.newInstance(1992, 04, 23);
        consumerAccnt.RecordtypeId = consumerRecordTypeId;
         consumerAccnt.BillingStreet = 'abc1';
         consumerAccnt.BillingCity  = 'abc1';
         consumerAccnt.BillingState = 'DE';
         consumerAccnt.BillingPostalCode = '14256';
         consumerAccnt.ShippingStreet = 'abc1';
         consumerAccnt.ShippingCity  = 'abc1';
         consumerAccnt.ShippingState = 'DE';
         consumerAccnt.ShippingPostalCode = '14256';
        accntsToBeInserted.add(consumerAccnt);
		
        
        //Create a Payor record along with the Med Adv details
        Account mdcrPayor = new Account();
        mdcrPayor.RecordTypeId = payorRecordTypeId;
        mdcrPayor.Name = 'Med Adv Test Payor';
        mdcrPayor.Payor_Code__c = 'K';
        mdcrPayor.Default_Price_Book_K__c = customPricebookId;
        mdcrPayor.Claim_Address__c = '1 Main Street, San Diego, CA - 92127';
        mdcrPayor.DOB__c =Date.newInstance(1992, 04, 23);
        accntsToBeInserted.add(mdcrPayor);

        
        //Create a consumer and Prescriber Account 
        Account prescriberAccnt = new Account();
        prescriberAccnt.FirstName = 'Med Adv FirstName';
        prescriberAccnt.LastName = 'Med Adv LastName';
        prescriberAccnt.RecordtypeId = prescriberRecordTypeId;
        prescriberAccnt.DOB__c =Date.newInstance(1992, 04, 23);
        accntsToBeInserted.add(prescriberAccnt);
        insert accntsToBeInserted;
		system.debug('accntsToBeInserted is ' + accntsToBeInserted);
        
        ClsAccountAddressTriggerStaticClass.addressChangeInvokedFromAddress = TRUE;
		ClsAccountAddressTriggerStaticClass.addressChangeInvokedFromAccount = FALSE;
		List<Address__c> addrList = new List<Address__c>();
        Address__c addrs = TestDataBuilder.getAddressList(consumerAccnt.Id, true,'SHIP_TO', 1)[0];
		addrs.Oracle_Address_ID__c = '23145687';
		//insert addrs;
		addrList.add(addrs);
		Address__c addrsBT = TestDataBuilder.getAddressList(consumerAccnt.Id, true,'BILL_TO', 1)[0];
		addrsBT.Oracle_Address_ID__c = '523145687';
		//insert addrsBT;
		addrList.add(addrsBT);
        if(!addrList.isEmpty()){
            database.insert(addrList);
        }
		consumerAccnt.Primary_Bill_To_Address__c = addrsBT.Id;
        consumerAccnt.Primary_Ship_To_Address__c = addrs.Id;
		system.debug('Address addrs.Id' +addrsBT.Id);
        Test.startTest();
		Update consumerAccnt;
         

        Benefits__c benefit = new Benefits__c();
        benefit.Benefit_Hierarchy__c = 'Primary';
        benefit.Account__c = consumerAccnt.Id;
        benefit.Payor__c = accntsToBeInserted[1].Id;
        benefit.Coverage__c = 80;
        benefit.CO_PAY__c = 20;
        benefit.Copay_Line_Item__c = 20;
        benefit.INDIVIDUAL_DEDUCTIBLE__c = 500;
        benefit.INDIVIDUAL_MET__c = 200;
        benefit.INDIVIDUAL_OOP_MAX__c = 1500;
        benefit.INDIVIDUAL_OOP_MET__c = 1000;
        benefit.FAMILY_DEDUCT__c = 1000;
        benefit.Family_Met__c = 850;
        benefit.FAMILY_OOP_MAX__c = 3000;
        benefit.FAMILY_OOP_MET__c = 2500;
        benefit.MEMBER_ID__c ='23423';
        insert benefit;

        //Update Primary Shipping and Benefit on Account
        Account acc = [SELECT Id, Primary_Ship_To_Address__c, Primary_Benefit__c FROM Account WHERE Id = :accntsToBeInserted[0].Id];
        acc.Primary_Benefit__c = benefit.Id;
        acc.Prescribers__c = accntsToBeInserted[2].Id;
        update acc;
        Test.stopTest();
    }
    
    @isTest static void TestOrderCreationAndUpdate(){
        
        Account accountRecord = [select Id, Name, Primary_Ship_To_Address__c from Account where FirstName ='Med Adv FirstName' and Party_ID__c ='78666567' and RecordType.Name ='Consumers'];
        Pricebook2 pricebookRec = [SELECT Id, Name from Pricebook2 where Name = 'Aetna Health Plan' and IsActive = true];
        //Create a new Order
        Id orderRecordType = Schema.SObjectType.Order.getRecordTypeInfosByName().get('US Sales Orders').getRecordTypeId();
        Order customOrder = new Order();
        customOrder.AccountId = accountRecord.Id;
        customOrder.Recordtypeid = orderRecordType;
        customOrder.Status = 'Draft';
        customOrder.EffectiveDate = Date.Today();
        customOrder.Scheduled_Ship_Date__c = Date.Today();
        customOrder.Price_Book__c = pricebookRec.Id;
        customOrder.Shipping_Method__c = '000001_FEDEX_A_SAT';
        customOrder.Shipping_Address__c = accountRecord.Primary_Ship_To_Address__c;
        Test.startTest();
        insert customOrder;
        Test.stopTest();
    }
}