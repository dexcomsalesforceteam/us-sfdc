/**
 * BclsDailyDoubleOptInEmailBatchTest
 * Test for BclsDailyDoubleOptInEmailBatch
 * @author Craig Johnson(Sundog)
 * @date 03/19/19
 */

@isTest
public class BclsDailyDoubleOptInEmailBatchTest {
    
    // Test for hourly job Scheduler
    static testMethod void testDailyEmailScheduledJob() {
       Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock());
       insert TestDataBuilder.testURLExpiryVal();

        List<Account> accountList= new List<Account>();
        Account testAccount = TestDataBuilder.testAccount();
        testAccount.PersonEmail='jane.smith@sundog.net';
        testAccount.Email_Pending_Opt_In__c=true;
        testAccount.Email_Opt_In_Language__c = 'English';
        testAccount.Email_Opt_In_Method__c = 'Via Email';
        accountList.add(testAccount); 
        
        Account testAccount2 = TestDataBuilder.testAccount();
        testAccount2.PersonEmail='john.smith@sundog.net';
        testAccount2.Email_Pending_Opt_In__c=true;
        testAccount2.Email_Opt_In_Language__c = 'English';
        testAccount2.Email_Opt_In_Method__c = 'Via Email';
        accountList.add(testAccount2);
        insert accountList;
        
        List<Lead> leadList= new List<Lead>();
        Lead testLead = TestDataBuilder.testLead();
        testLead.Email_Opt_In_Language__c = 'English';
        testLead.Email_Opt_In_Method__c = 'Via Email';
        testLead.Email='jane.smith@sundog.net';
        testLead.Email_Pending_Opt_In__c=true;
        leadList.add(testLead); 
        
        Lead testlead2 = TestDataBuilder.testlead();
        testLead2.Email_Opt_In_Language__c = 'English';
        testLead2.Email_Opt_In_Method__c = 'Via Email';
        testlead2.Email='john.smith@sundog.net';
        testlead2.Email_Pending_Opt_In__c=true;
        leadList.add(testlead2);      
        insert leadList;
        
        Test.startTest();
        String jobId = System.schedule('testEmailScheduled', '0 0 0 3 9 ? 2052',
                new SclsDailyDoubleOptInEmailScheduled());
        Test.stopTest();
    }
    
    // Test for batch job on Account 
    static testMethod void testCanadaAccountOptIn() {
        //Query 'CA_Consumer' record type
        RecordType recordTypeAccount = [select Id 
                                        FROM RecordType 
                                        where DeveloperName = 'CA_Consumer' and SobjectType = 'Account' limit 1];
        //Create a Test Account
        List<Account> accountList= new List<Account>();
        Account testAccount = TestDataBuilder.testAccount();
        testAccount.PersonEmail = 'jane.smith@sundog.net';
        testAccount.Email_Pending_Opt_In__c = true;
        testAccount.RecordTypeId = recordTypeAccount.Id;
        testAccount.Email_Double_Opt_In_Subscriber__c = Datetime.now();
        testAccount.Email_Opt_In_Language__c = 'English';
        testAccount.Email_Opt_In_Method__c = 'Via Web Form';
        accountList.add(testAccount);
        Account testAccount2 = TestDataBuilder.testAccount();
        testAccount2.PersonEmail = 'jane.smith2@sundog.net';
        testAccount2.Email_Pending_Opt_In__c = true;
        testAccount2.Email_Opt_In_Language__c = 'English';
        testAccount2.Email_Opt_In_Method__c = 'Via Web Form';
        accountList.add(testAccount2); 
        Account testAccount3 = TestDataBuilder.testAccount();
        testAccount3.PersonEmail = 'jane.smith3@sundog.net';
        testAccount3.Email_Pending_Opt_In__c = true;
        testAccount3.Email_Opt_In_Language__c = 'English';
        testAccount3.Email_Opt_In_Method__c = 'Via Web Form';
        accountList.add(testAccount3); 
        insert accountList;
        
        
        // query account record where double opt in pending is true
        Account resultAccount1 = [SELECT PersonContactID,Email_Pending_Opt_In__c,Email_Opt_In_List__c FROM Account where Email_Pending_Opt_In__c = true and PersonEmail = 'jane.smith@sundog.net'];
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock(resultAccount1.PersonContactID));
        
        //Email_Opt_In_List__c field should be false at this point
        System.assertEquals(null, resultAccount1.Email_Opt_In_List__c);
        
        //Run batch job
        Test.startTest(); 
        BclsDailyDoubleOptInEmailBatch dailyEmailBatch = new BclsDailyDoubleOptInEmailBatch(BclsDailyDoubleOptInEmailBatch.BatchType.CA_ACCOUNT);         
       	Id batchInstanceId = Database.executeBatch(dailyEmailBatch, 100);
        Test.stopTest();

        resultAccount1 = [SELECT PersonContactID,Email_Pending_Opt_In__c,Email_Opt_In_List__c FROM Account where Id = :resultAccount1.Id];

        //Email_Pending_Opt_In__c field should be set to false at this point
        System.assertEquals(false, resultAccount1.Email_Pending_Opt_In__c);
        System.assertEquals('True', resultAccount1.Email_Opt_In_List__c);
    }
    
    // Test for batch job on Account 
    static testMethod void testGBAccountOptIn() {
        //Query 'CA_Consumer' record type
        RecordType recordTypeAccount = [select Id 
                                        FROM RecordType 
                                        where DeveloperName = 'GB_Consumer' and SobjectType = 'Account' limit 1];
        //Create a Test Account
        List<Account> accountList= new List<Account>();
        Account testAccount = TestDataBuilder.testAccount();
        testAccount.PersonEmail = 'jane.smith@sundog.net';
        testAccount.Email_Pending_Opt_In__c = true;
        testAccount.RecordTypeId = recordTypeAccount.Id;
        testAccount.Email_Double_Opt_In_Subscriber__c = Datetime.now();
        testAccount.Email_Opt_In_Language__c = 'English';
        testAccount.Email_Opt_In_Method__c = 'Via Web Form';
        accountList.add(testAccount);
        Account testAccount2 = TestDataBuilder.testAccount();
        testAccount2.PersonEmail = 'jane.smith2@sundog.net';
        testAccount2.Email_Pending_Opt_In__c = true;
        testAccount2.Email_Opt_In_Language__c = 'English';
        testAccount2.Email_Opt_In_Method__c = 'Via Web Form';
        accountList.add(testAccount2); 
        Account testAccount3 = TestDataBuilder.testAccount();
        testAccount3.PersonEmail = 'jane.smith3@sundog.net';
        testAccount3.Email_Pending_Opt_In__c = true;
        testAccount3.Email_Opt_In_Language__c = 'English';
        testAccount3.Email_Opt_In_Method__c = 'Via Web Form';
        accountList.add(testAccount3); 
        insert accountList;
        
        
        // query account record where double opt in pending is true
        Account resultAccount1 = [SELECT PersonContactID,Email_Pending_Opt_In__c,Email_Opt_In_List__c FROM Account where Email_Pending_Opt_In__c = true and PersonEmail = 'jane.smith@sundog.net'];
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock(resultAccount1.PersonContactID));
        
        //Email_Opt_In_List__c field should be false at this point
        System.assertEquals(null, resultAccount1.Email_Opt_In_List__c);
        
        //Run batch job
        Test.startTest(); 
        BclsDailyDoubleOptInEmailBatch dailyEmailBatch = new BclsDailyDoubleOptInEmailBatch(BclsDailyDoubleOptInEmailBatch.BatchType.GB_IE_ACCOUNT);         
       	Id batchInstanceId = Database.executeBatch(dailyEmailBatch, 100);
        Test.stopTest();

        resultAccount1 = [SELECT PersonContactID,Email_Pending_Opt_In__c,Email_Opt_In_List__c FROM Account where Id = :resultAccount1.Id];

        //Email_Pending_Opt_In__c field should be set to false at this point
        System.assertEquals(false, resultAccount1.Email_Pending_Opt_In__c);
        System.assertEquals('True', resultAccount1.Email_Opt_In_List__c);
    }
    
    // Test for batch job on Account 
    static testMethod void testIrelandAccountOptIn() {
        //Query 'CA_Consumer' record type
        RecordType recordTypeAccount = [select Id 
                                        FROM RecordType 
                                        where DeveloperName = 'IE_Consumer' and SobjectType = 'Account' limit 1];
        //Create a Test Account
        List<Account> accountList= new List<Account>();
        Account testAccount = TestDataBuilder.testAccount();
        testAccount.PersonEmail = 'jane.smith@sundog.net';
        testAccount.Email_Pending_Opt_In__c = true;
        testAccount.RecordTypeId = recordTypeAccount.Id;
        testAccount.Email_Double_Opt_In_Subscriber__c = Datetime.now();
        testAccount.Email_Opt_In_Language__c = 'English';
        testAccount.Email_Opt_In_Method__c = 'Via Web Form';
        accountList.add(testAccount);
        Account testAccount2 = TestDataBuilder.testAccount();
        testAccount2.PersonEmail = 'jane.smith2@sundog.net';
        testAccount2.Email_Pending_Opt_In__c = true;
        testAccount2.Email_Opt_In_Language__c = 'English';
        testAccount2.Email_Opt_In_Method__c = 'Via Web Form';
        accountList.add(testAccount2); 
        Account testAccount3 = TestDataBuilder.testAccount();
        testAccount3.PersonEmail = 'jane.smith3@sundog.net';
        testAccount3.Email_Pending_Opt_In__c = true;
        testAccount3.Email_Opt_In_Language__c = 'English';
        testAccount3.Email_Opt_In_Method__c = 'Via Web Form';
        accountList.add(testAccount3); 
        insert accountList;
        
        
        // query account record where double opt in pending is true
        Account resultAccount1 = [SELECT PersonContactID,Email_Pending_Opt_In__c,Email_Opt_In_List__c FROM Account where Email_Pending_Opt_In__c = true and PersonEmail = 'jane.smith@sundog.net'];
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock(resultAccount1.PersonContactID));
        
        //Email_Opt_In_List__c field should be false at this point
        System.assertEquals(null, resultAccount1.Email_Opt_In_List__c);
        
        //Run batch job
        Test.startTest(); 
        BclsDailyDoubleOptInEmailBatch dailyEmailBatch = new BclsDailyDoubleOptInEmailBatch(BclsDailyDoubleOptInEmailBatch.BatchType.GB_IE_ACCOUNT);         
       	Id batchInstanceId = Database.executeBatch(dailyEmailBatch, 100);
        Test.stopTest();

        resultAccount1 = [SELECT PersonContactID,Email_Pending_Opt_In__c,Email_Opt_In_List__c FROM Account where Id = :resultAccount1.Id];

        //Email_Pending_Opt_In__c field should be set to false at this point
        System.assertEquals(false, resultAccount1.Email_Pending_Opt_In__c);
        System.assertEquals('True', resultAccount1.Email_Opt_In_List__c);
    }
    
    // Test for batch job on Lead 
    static testMethod void testCanadaLeadOptIn() {
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock());
        //Query 'CA_Consumer' record type
        RecordType recordTypeLead = [select Id 
                                        FROM RecordType 
                                        where DeveloperName = 'CA_Lead' and SobjectType = 'Lead' limit 1];
        //Create a Test Lead
        List<Lead> leadList= new List<Lead>();
        Lead testLead = TestDataBuilder.testLead();
        testLead.Email = 'jane.smith@sundog.net';
        testLead.Email_Opt_In_Language__c = 'English';
        testLead.Email_Opt_In_Method__c = 'Via Email';
        testLead.Email_Pending_Opt_In__c = true;
        testLead.RecordTypeId = recordTypeLead.Id;
        testLead.Email_Double_Opt_In_Subscriber__c = Datetime.now();
        leadList.add(testLead); 
        insert TestDataBuilder.testURLExpiryVal();
        insert leadList;
        
        //query account record where double opt in penbding is true
        Lead resultLead1 = [SELECT id,Email_Pending_Opt_In__c,Email_Opt_In_List__c FROM Lead where Email_Pending_Opt_In__c = true];
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock(resultLead1.Id));
        
        //Email_Opt_In_List__c field should be false at this point
        System.assertEquals(null,resultLead1.Email_Opt_In_List__c );
        
        //Run batch job
        Test.startTest(); 
        BclsDailyDoubleOptInEmailBatch dailyEmailBatch = new BclsDailyDoubleOptInEmailBatch(BclsDailyDoubleOptInEmailBatch.BatchType.CA_Lead);         
       	Id batchInstanceId = Database.executeBatch(dailyEmailBatch, 1);
        Test.stopTest();

        resultLead1 = [SELECT Id,Email_Pending_Opt_In__c,Email_Opt_In_List__c FROM Lead where Id = :resultLead1.Id];

        //Email_Pending_Opt_In__c field should be set to false at this point
        System.assertEquals(false, resultLead1.Email_Pending_Opt_In__c);
        System.assertEquals('True', resultLead1.Email_Opt_In_List__c);
    }
    
    // Test for batch job on Lead 
    static testMethod void testGBLeadOptIn() {
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock());
        //Query 'CA_Consumer' record type
        RecordType recordTypeLead = [select Id 
                                        FROM RecordType 
                                        where DeveloperName = 'GB_Leads' and SobjectType = 'Lead' limit 1];
        //Create a Test Lead
        List<Lead> leadList= new List<Lead>();
        Lead testLead = TestDataBuilder.testLead();
        testLead.Email = 'jane.smith@sundog.net';
        testLead.Email_Opt_In_Language__c = 'English';
        testLead.Email_Opt_In_Method__c = 'Via Email';
        testLead.Email_Pending_Opt_In__c = true;
        testLead.RecordTypeId = recordTypeLead.Id;
        testLead.Email_Double_Opt_In_Subscriber__c = Datetime.now();
        leadList.add(testLead); 
        insert TestDataBuilder.testURLExpiryVal();
        insert leadList;
        
        //query account record where double opt in penbding is true
        Lead resultLead1 = [SELECT id,Email_Pending_Opt_In__c,Email_Opt_In_List__c FROM Lead where Email_Pending_Opt_In__c = true];
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock(resultLead1.Id));
        
        //Email_Opt_In_List__c field should be false at this point
        System.assertEquals(null,resultLead1.Email_Opt_In_List__c );
        
        //Run batch job
        Test.startTest(); 
        BclsDailyDoubleOptInEmailBatch dailyEmailBatch = new BclsDailyDoubleOptInEmailBatch(BclsDailyDoubleOptInEmailBatch.BatchType.GB_IE_LEAD);         
       	Id batchInstanceId = Database.executeBatch(dailyEmailBatch, 1);
        Test.stopTest();

        resultLead1 = [SELECT Id,Email_Pending_Opt_In__c,Email_Opt_In_List__c FROM Lead where Id = :resultLead1.Id];

        //Email_Pending_Opt_In__c field should be set to false at this point
        System.assertEquals(false, resultLead1.Email_Pending_Opt_In__c);
        System.assertEquals('True', resultLead1.Email_Opt_In_List__c);
    }
    
    // Test for batch job on Lead 
    static testMethod void testIrelandLeadOptIn() {
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock());
        //Query 'CA_Consumer' record type
        RecordType recordTypeLead = [select Id 
                                        FROM RecordType 
                                        where DeveloperName = 'IE_Lead' and SobjectType = 'Lead' limit 1];
        //Create a Test Lead
        List<Lead> leadList= new List<Lead>();
        Lead testLead = TestDataBuilder.testLead();
        testLead.Email = 'jane.smith@sundog.net';
        testLead.Email_Opt_In_Language__c = 'English';
        testLead.Email_Opt_In_Method__c = 'Via Email';
        testLead.Email_Pending_Opt_In__c = true;
        testLead.RecordTypeId = recordTypeLead.Id;
        testLead.Email_Double_Opt_In_Subscriber__c = Datetime.now();
        leadList.add(testLead); 
        insert TestDataBuilder.testURLExpiryVal();
        insert leadList;
        
        //query account record where double opt in penbding is true
        Lead resultLead1 = [SELECT id,Email_Pending_Opt_In__c,Email_Opt_In_List__c FROM Lead where Email_Pending_Opt_In__c = true];
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock(resultLead1.Id));
        
        //Email_Opt_In_List__c field should be false at this point
        System.assertEquals(null,resultLead1.Email_Opt_In_List__c );
        
        //Run batch job
        Test.startTest(); 
        BclsDailyDoubleOptInEmailBatch dailyEmailBatch = new BclsDailyDoubleOptInEmailBatch(BclsDailyDoubleOptInEmailBatch.BatchType.GB_IE_LEAD);         
       	Id batchInstanceId = Database.executeBatch(dailyEmailBatch, 1);
        Test.stopTest();

        resultLead1 = [SELECT Id,Email_Pending_Opt_In__c,Email_Opt_In_List__c FROM Lead where Id = :resultLead1.Id];

        //Email_Pending_Opt_In__c field should be set to false at this point
        System.assertEquals(false, resultLead1.Email_Pending_Opt_In__c);
        System.assertEquals('True', resultLead1.Email_Opt_In_List__c);
    }
    
    

    static testMethod void testCanadaConvertedLead() {
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock());
        //Query 'CA_Consumer' record type
        RecordType recordTypeAccount = [select Id 
                                        FROM RecordType 
                                        where DeveloperName = 'CA_Lead' and SobjectType = 'Lead' limit 1];
        //Create a Test Lead
        List<Lead> leadList= new List<Lead>();
        Lead testLead = TestDataBuilder.testLead();
        testLead.Email = 'jane.smith@sundog.net';
        testLead.Email_Pending_Opt_In__c = true;
        testLead.RecordTypeId = recordTypeAccount.Id;
        testLead.Email_Double_Opt_In_Subscriber__c = Datetime.now();
        testLead.Email_Opt_In_Language__c = 'English';
        testLead.Email_Opt_In_Method__c = 'Via Web Form';
        leadList.add(testLead); 
        insert TestDataBuilder.testURLExpiryVal();
        insert leadList;

        Database.LeadConvert lc = new database.LeadConvert();
        lc.setLeadId(testLead.Id);
        lc.convertedStatus = 'Send to Customer Ops';
        lc.setDoNotCreateOpportunity(true);
        Database.LeadConvertResult lcr = Database.convertLead(lc, false);
        //System.assertEquals(null, lcr.getErrors());
        System.assert(lcr.isSuccess());
        
        //query account record where double opt in penbding is true
        Lead resultLead1 = [SELECT Id, Email_Pending_Opt_In__c, Email_Opt_In_List__c, ConvertedAccountId FROM Lead where Email_Pending_Opt_In__c = true];
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock(resultLead1.Id));
        
        //Email_Opt_In_List__c field should be false at this point
        System.assertEquals(null,resultLead1.Email_Opt_In_List__c);
        System.assertNotEquals(null,resultLead1.ConvertedAccountId);
        
        //Run batch job
        Test.startTest(); 
        BclsDailyDoubleOptInEmailBatch dailyEmailBatch = new BclsDailyDoubleOptInEmailBatch(BclsDailyDoubleOptInEmailBatch.BatchType.CA_LEAD);         
        Id batchInstanceId = Database.executeBatch(dailyEmailBatch, 1);
        Test.stopTest();

        resultLead1 = [SELECT Id,Email_Pending_Opt_In__c,Email_Opt_In_List__c, ConvertedAccount.Email_Pending_Opt_In__c, ConvertedAccount.Email_Opt_In_List__c FROM Lead where Id = :resultLead1.Id];

        System.assertEquals(false, resultLead1.ConvertedAccount.Email_Pending_Opt_In__c);
        System.assertEquals('True', resultLead1.ConvertedAccount.Email_Opt_In_List__c);
    }
    
    static testMethod void testGBConvertedLead() {
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock());
        //Query 'CA_Consumer' record type
        RecordType recordTypeAccount = [select Id 
                                        FROM RecordType 
                                        where DeveloperName = 'GB_Leads' and SobjectType = 'Lead' limit 1];
        //Create a Test Lead
        List<Lead> leadList= new List<Lead>();
        Lead testLead = TestDataBuilder.testLead();
        testLead.Email = 'jane.smith@sundog.net';
        testLead.Email_Pending_Opt_In__c = true;
        testLead.RecordTypeId = recordTypeAccount.Id;
        testLead.Email_Double_Opt_In_Subscriber__c = Datetime.now();
        testLead.Email_Opt_In_Language__c = 'English';
        testLead.Email_Opt_In_Method__c = 'Via Web Form';
        leadList.add(testLead); 
        insert TestDataBuilder.testURLExpiryVal();
        insert leadList;

        Database.LeadConvert lc = new database.LeadConvert();
        lc.setLeadId(testLead.Id);
        lc.convertedStatus = 'Send to Customer Ops';
        lc.setDoNotCreateOpportunity(true);
        Database.LeadConvertResult lcr = Database.convertLead(lc, false);
        //System.assertEquals(null, lcr.getErrors());
        System.assert(lcr.isSuccess());
        
        //query account record where double opt in penbding is true
        Lead resultLead1 = [SELECT Id, Email_Pending_Opt_In__c, Email_Opt_In_List__c, ConvertedAccountId FROM Lead where Email_Pending_Opt_In__c = true];
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock(resultLead1.Id));
        
        //Email_Opt_In_List__c field should be false at this point
        System.assertEquals(null,resultLead1.Email_Opt_In_List__c);
        System.assertNotEquals(null,resultLead1.ConvertedAccountId);
        
        //Run batch job
        Test.startTest(); 
        BclsDailyDoubleOptInEmailBatch dailyEmailBatch = new BclsDailyDoubleOptInEmailBatch(BclsDailyDoubleOptInEmailBatch.BatchType.GB_IE_LEAD);         
        Id batchInstanceId = Database.executeBatch(dailyEmailBatch, 1);
        Test.stopTest();

        resultLead1 = [SELECT Id,Email_Pending_Opt_In__c,Email_Opt_In_List__c, ConvertedAccount.Email_Pending_Opt_In__c, ConvertedAccount.Email_Opt_In_List__c FROM Lead where Id = :resultLead1.Id];

        System.assertEquals(false, resultLead1.ConvertedAccount.Email_Pending_Opt_In__c);
        System.assertEquals('True', resultLead1.ConvertedAccount.Email_Opt_In_List__c);
    }
    
    static testMethod void testIrelandConvertedLead() {
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock());
        //Query 'CA_Consumer' record type
        RecordType recordTypeAccount = [select Id 
                                        FROM RecordType 
                                        where DeveloperName = 'IE_Lead' and SobjectType = 'Lead' limit 1];
        //Create a Test Lead
        List<Lead> leadList= new List<Lead>();
        Lead testLead = TestDataBuilder.testLead();
        testLead.Email = 'jane.smith@sundog.net';
        testLead.Email_Pending_Opt_In__c = true;
        testLead.RecordTypeId = recordTypeAccount.Id;
        testLead.Email_Double_Opt_In_Subscriber__c = Datetime.now();
        testLead.Email_Opt_In_Language__c = 'English';
        testLead.Email_Opt_In_Method__c = 'Via Web Form';
        leadList.add(testLead); 
        insert TestDataBuilder.testURLExpiryVal();
        insert leadList;

        Database.LeadConvert lc = new database.LeadConvert();
        lc.setLeadId(testLead.Id);
        lc.convertedStatus = 'Send to Customer Ops';
        lc.setDoNotCreateOpportunity(true);
        Database.LeadConvertResult lcr = Database.convertLead(lc, false);
        //System.assertEquals(null, lcr.getErrors());
        System.assert(lcr.isSuccess());
        
        //query account record where double opt in penbding is true
        Lead resultLead1 = [SELECT Id, Email_Pending_Opt_In__c, Email_Opt_In_List__c, ConvertedAccountId FROM Lead where Email_Pending_Opt_In__c = true];
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock(resultLead1.Id));
        
        //Email_Opt_In_List__c field should be false at this point
        System.assertEquals(null,resultLead1.Email_Opt_In_List__c);
        System.assertNotEquals(null,resultLead1.ConvertedAccountId);
        
        //Run batch job
        Test.startTest(); 
        BclsDailyDoubleOptInEmailBatch dailyEmailBatch = new BclsDailyDoubleOptInEmailBatch(BclsDailyDoubleOptInEmailBatch.BatchType.GB_IE_LEAD);         
        Id batchInstanceId = Database.executeBatch(dailyEmailBatch, 1);
        Test.stopTest();

        resultLead1 = [SELECT Id,Email_Pending_Opt_In__c,Email_Opt_In_List__c, ConvertedAccount.Email_Pending_Opt_In__c, ConvertedAccount.Email_Opt_In_List__c FROM Lead where Id = :resultLead1.Id];

        System.assertEquals(false, resultLead1.ConvertedAccount.Email_Pending_Opt_In__c);
        System.assertEquals('True', resultLead1.ConvertedAccount.Email_Opt_In_List__c);
    }

    static testMethod void testCanadaLeadOptOut() {
        //Query 'CA_Lead' record type
        RecordType recordTypeAccount = [select Id 
                                        FROM RecordType 
                                        where DeveloperName = 'CA_Lead' and SobjectType = 'Lead' limit 1];
        //Create a Test Lead
        List<Lead> leadList= new List<Lead>();
        Lead testLead = TestDataBuilder.testLead();
        testLead.Email = 'jane.smith@sundog.net';
        testLead.Email_Opt_In_List__c = 'True';
        testLead.RecordTypeId = recordTypeAccount.Id;
        testLead.Email_Double_Opt_In_Subscriber__c = Datetime.now();
        leadList.add(testLead); 
        insert TestDataBuilder.testURLExpiryVal();
        insert leadList;
        
        //query account record where double opt in penbding is true
        Lead resultLead1 = [SELECT id,Email_Pending_Opt_In__c,Email_Opt_In_List__c FROM Lead where Id = :testLead.Id];
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock(resultLead1.Id, 'Unsubscribed'));
        
        //Run batch job
        Test.startTest(); 
        BclsDailyDoubleOptInEmailBatch dailyEmailBatch = new BclsDailyDoubleOptInEmailBatch(BclsDailyDoubleOptInEmailBatch.BatchType.CA_LEAD);         
        Id batchInstanceId = Database.executeBatch(dailyEmailBatch, 1);
        Test.stopTest();

        resultLead1 = [SELECT Id,Email_Pending_Opt_In__c,Email_Opt_In_List__c,Email_Double_Opt_In_Subscriber__c FROM Lead where Id = :resultLead1.Id];

        System.assertEquals('False', resultLead1.Email_Opt_In_List__c);
        System.assertEquals(false, resultLead1.Email_Pending_Opt_In__c);
        System.assertEquals(null, resultLead1.Email_Double_Opt_In_Subscriber__c);
    }
    
    static testMethod void testGBLeadOptOut() {
        //Query 'CA_Lead' record type
        RecordType recordTypeAccount = [select Id 
                                        FROM RecordType 
                                        where DeveloperName = 'GB_Leads' and SobjectType = 'Lead' limit 1];
        //Create a Test Lead
        List<Lead> leadList= new List<Lead>();
        Lead testLead = TestDataBuilder.testLead();
        testLead.Email = 'jane.smith@sundog.net';
        testLead.Email_Opt_In_List__c = 'True';
        testLead.RecordTypeId = recordTypeAccount.Id;
        testLead.Email_Double_Opt_In_Subscriber__c = Datetime.now();
        leadList.add(testLead); 
        insert TestDataBuilder.testURLExpiryVal();
        insert leadList;
        
        //query account record where double opt in penbding is true
        Lead resultLead1 = [SELECT id,Email_Pending_Opt_In__c,Email_Opt_In_List__c FROM Lead where Id = :testLead.Id];
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock(resultLead1.Id, 'Unsubscribed'));
        
        //Run batch job
        Test.startTest(); 
        BclsDailyDoubleOptInEmailBatch dailyEmailBatch = new BclsDailyDoubleOptInEmailBatch(BclsDailyDoubleOptInEmailBatch.BatchType.GB_IE_LEAD);         
        Id batchInstanceId = Database.executeBatch(dailyEmailBatch, 1);
        Test.stopTest();

        resultLead1 = [SELECT Id,Email_Pending_Opt_In__c,Email_Opt_In_List__c,Email_Double_Opt_In_Subscriber__c FROM Lead where Id = :resultLead1.Id];

        System.assertEquals('False', resultLead1.Email_Opt_In_List__c);
        System.assertEquals(false, resultLead1.Email_Pending_Opt_In__c);
        System.assertEquals(null, resultLead1.Email_Double_Opt_In_Subscriber__c);
    }
    
    static testMethod void testIrelandLeadOptOut() {
        //Query 'CA_Lead' record type
        RecordType recordTypeAccount = [select Id 
                                        FROM RecordType 
                                        where DeveloperName = 'IE_Lead' and SobjectType = 'Lead' limit 1];
        //Create a Test Lead
        List<Lead> leadList= new List<Lead>();
        Lead testLead = TestDataBuilder.testLead();
        testLead.Email = 'jane.smith@sundog.net';
        testLead.Email_Opt_In_List__c = 'True';
        testLead.RecordTypeId = recordTypeAccount.Id;
        testLead.Email_Double_Opt_In_Subscriber__c = Datetime.now();
        leadList.add(testLead); 
        insert TestDataBuilder.testURLExpiryVal();
        insert leadList;
        
        //query account record where double opt in penbding is true
        Lead resultLead1 = [SELECT id,Email_Pending_Opt_In__c,Email_Opt_In_List__c FROM Lead where Id = :testLead.Id];
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock(resultLead1.Id, 'Unsubscribed'));
        
        //Run batch job
        Test.startTest(); 
        BclsDailyDoubleOptInEmailBatch dailyEmailBatch = new BclsDailyDoubleOptInEmailBatch(BclsDailyDoubleOptInEmailBatch.BatchType.GB_IE_LEAD);         
        Id batchInstanceId = Database.executeBatch(dailyEmailBatch, 1);
        Test.stopTest();

        resultLead1 = [SELECT Id,Email_Pending_Opt_In__c,Email_Opt_In_List__c,Email_Double_Opt_In_Subscriber__c FROM Lead where Id = :resultLead1.Id];

        System.assertEquals('False', resultLead1.Email_Opt_In_List__c);
        System.assertEquals(false, resultLead1.Email_Pending_Opt_In__c);
        System.assertEquals(null, resultLead1.Email_Double_Opt_In_Subscriber__c);
    }

    static testMethod void testCanadaAccountOptOut() {
        //Query 'CA_Consumer' record type
        RecordType recordTypeAccount = [select Id 
                                        FROM RecordType 
                                        where DeveloperName = 'CA_Consumer' and SobjectType = 'Account' limit 1];
        //Create a Test Account
        List<Account> accountList= new List<Account>();
        Account testAccount = TestDataBuilder.testAccount();
        testAccount.PersonEmail = 'jane.smith@sundog.net';
        testAccount.Email_Opt_In_List__c = 'True';
        testAccount.RecordTypeId = recordTypeAccount.Id;
        testAccount.Email_Double_Opt_In_Subscriber__c = Datetime.now();
        testAccount.Email_Opt_In_Language__c = 'English';
        testAccount.Email_Opt_In_Method__c = 'Via Web Form';
        accountList.add(testAccount);
        insert accountList;
        
        // query account record where double opt in penbding is true
        Account resultAccount1 = [SELECT PersonContactID,Email_Pending_Opt_In__c,Email_Opt_In_List__c FROM Account where Id = :testAccount.Id];
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock(resultAccount1.PersonContactID, 'Unsubscribed'));
        
        //Run batch job
        Test.startTest(); 
        BclsDailyDoubleOptInEmailBatch dailyEmailBatch = new BclsDailyDoubleOptInEmailBatch(BclsDailyDoubleOptInEmailBatch.BatchType.CA_ACCOUNT);         
        Id batchInstanceId = Database.executeBatch(dailyEmailBatch, 100);
        Test.stopTest();

        resultAccount1 = [SELECT PersonContactID,Email_Pending_Opt_In__c,Email_Opt_In_List__c,Email_Double_Opt_In_Subscriber__c FROM Account where Id = :resultAccount1.Id];

        System.assertEquals('False', resultAccount1.Email_Opt_In_List__c);
        System.assertEquals(false, resultAccount1.Email_Pending_Opt_In__c);
        System.assertEquals(null, resultAccount1.Email_Double_Opt_In_Subscriber__c);
    }
    
    static testMethod void testGBAccountOptOut() {
        //Query 'CA_Consumer' record type
        RecordType recordTypeAccount = [select Id 
                                        FROM RecordType 
                                        where DeveloperName = 'GB_Consumer' and SobjectType = 'Account' limit 1];
        //Create a Test Account
        List<Account> accountList= new List<Account>();
        Account testAccount = TestDataBuilder.testAccount();
        testAccount.PersonEmail = 'jane.smith@sundog.net';
        testAccount.Email_Opt_In_List__c = 'True';
        testAccount.RecordTypeId = recordTypeAccount.Id;
        testAccount.Email_Double_Opt_In_Subscriber__c = Datetime.now();
        testAccount.Email_Opt_In_Language__c = 'English';
        testAccount.Email_Opt_In_Method__c = 'Via Web Form';
        accountList.add(testAccount);
        insert accountList;
        
        // query account record where double opt in penbding is true
        Account resultAccount1 = [SELECT PersonContactID,Email_Pending_Opt_In__c,Email_Opt_In_List__c FROM Account where Id = :testAccount.Id];
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock(resultAccount1.PersonContactID, 'Unsubscribed'));
        
        //Run batch job
        Test.startTest(); 
        BclsDailyDoubleOptInEmailBatch dailyEmailBatch = new BclsDailyDoubleOptInEmailBatch(BclsDailyDoubleOptInEmailBatch.BatchType.GB_IE_ACCOUNT);         
        Id batchInstanceId = Database.executeBatch(dailyEmailBatch, 100);
        Test.stopTest();

        resultAccount1 = [SELECT PersonContactID,Email_Pending_Opt_In__c,Email_Opt_In_List__c,Email_Double_Opt_In_Subscriber__c FROM Account where Id = :resultAccount1.Id];

        System.assertEquals('False', resultAccount1.Email_Opt_In_List__c);
        System.assertEquals(false, resultAccount1.Email_Pending_Opt_In__c);
        System.assertEquals(null, resultAccount1.Email_Double_Opt_In_Subscriber__c);
    }
    
    static testMethod void testIrelandAccountOptOut() {
        //Query 'CA_Consumer' record type
        RecordType recordTypeAccount = [select Id 
                                        FROM RecordType 
                                        where DeveloperName = 'IE_Consumer' and SobjectType = 'Account' limit 1];
        //Create a Test Account
        List<Account> accountList= new List<Account>();
        Account testAccount = TestDataBuilder.testAccount();
        testAccount.PersonEmail = 'jane.smith@sundog.net';
        testAccount.Email_Opt_In_List__c = 'True';
        testAccount.RecordTypeId = recordTypeAccount.Id;
        testAccount.Email_Double_Opt_In_Subscriber__c = Datetime.now();
        testAccount.Email_Opt_In_Language__c = 'English';
        testAccount.Email_Opt_In_Method__c = 'Via Web Form';
        accountList.add(testAccount);
        insert accountList;
        
        // query account record where double opt in penbding is true
        Account resultAccount1 = [SELECT PersonContactID,Email_Pending_Opt_In__c,Email_Opt_In_List__c FROM Account where Id = :testAccount.Id];
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock(resultAccount1.PersonContactID, 'Unsubscribed'));
        
        //Run batch job
        Test.startTest(); 
        BclsDailyDoubleOptInEmailBatch dailyEmailBatch = new BclsDailyDoubleOptInEmailBatch(BclsDailyDoubleOptInEmailBatch.BatchType.GB_IE_ACCOUNT);         
        Id batchInstanceId = Database.executeBatch(dailyEmailBatch, 100);
        Test.stopTest();

        resultAccount1 = [SELECT PersonContactID,Email_Pending_Opt_In__c,Email_Opt_In_List__c,Email_Double_Opt_In_Subscriber__c FROM Account where Id = :resultAccount1.Id];

        System.assertEquals('False', resultAccount1.Email_Opt_In_List__c);
        System.assertEquals(false, resultAccount1.Email_Pending_Opt_In__c);
        System.assertEquals(null, resultAccount1.Email_Double_Opt_In_Subscriber__c);
    }
    
}