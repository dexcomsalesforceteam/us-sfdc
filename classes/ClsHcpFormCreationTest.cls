/*
  @Author         : LTI
  @Date Created   : 16 Aug 2019
  @Description    : Test Cases for HcpFormCreationCls
*/
@istest
public class ClsHcpFormCreationTest {

    @isTest 
    public static void hcpFormCreationTest(){
        List<ClsHcpFormCreation.HcpCreationWrapper> paramWrapList = new List<ClsHcpFormCreation.HcpCreationWrapper>();
        Map<String,String> paramMap = new Map<String,String>();
        paramMap.put('hcpTypeSelected','Update HCP');
        paramMap.put('partyId','123456');
        paramMap.put('firstName', 'FirstName');
        paramMap.put('lastName','LastName');
        paramMap.put('address','Addr Line 1');
        paramMap.put('city', 'City1');
        paramMap.put('state','State1');
        paramMap.put('zip', '123456');
        paramMap.put('npi','3278795645');
        paramMap.put('phone','1234567897');
        paramMap.put('fax','2547667987809');
        
        for(String paramKey : paramMap.keySet()){
            ClsHcpFormCreation.HcpCreationWrapper wrapper = new ClsHcpFormCreation.HcpCreationWrapper();
            wrapper.variableKey = paramKey;
            wrapper.variableValue = paramMap.get(paramKey);
            paramWrapList.add(wrapper);
        }
        
        Test.startTest();
        ClsHcpFormCreation.sendEmail(paramWrapList);
        Test.stopTest();
    }
}