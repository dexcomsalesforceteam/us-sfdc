/*************************************************
* @author      : Dexcom, LTI
* @date        : April, 2019
* @description : Controller QC Hold : BI Check Service Call
*************************************************/
public class ClsQCHoldBenefitsCheckService{
    //Method fetches the BI information related to Account and returns as a Field Name to Field Value map.
    @AuraEnabled
    public static Map<String, String> getBIDetails(String orderId, String accountId, String orderType, Boolean isOrderActivated) {
        Map<String, String> fieldValueMap = new Map<String, String>();
        
        fieldValueMap = ClsOrderAuditHandler.getAuditDetailsforCheck('Benefit', orderId);        
        System.debug('Fetched Benefit Details >> '+ fieldValueMap);
        system.debug('****OrderType Benefit - ' + orderType );
        
        if(fieldValueMap!= null && !fieldValueMap.isEmpty() && (isOrderActivated || Boolean.valueOf(fieldValueMap.get('bypassCheck')))){
                System.debug(' returning Map : ' + fieldValueMap);
            return fieldValueMap;
        }else{  
            fieldValueMap.put('bypassBICheck', 'false');
            
            List<Account> accntList = new List<Account>([SELECT Is_BI_Active__c, Primary_Benefit__c, Next_Possible_Date_For_Sensor_Order__c, 
                                                         Next_Possible_Date_For_Receiver_Order__c, Next_Possible_Date_For_Transmitter_Order__c 
                                                         FROM ACCOUNT WHERE Id = :accountId]);
            
            if(!accntList.isEmpty()){
                Account accnt = accntList[0];
                List<Benefits__c> benefitList = new List<Benefits__c>([SELECT Last_Benefits_Check_Date__c, Last_Benefits_Check_Done_By__c,          
                                                                    PRIOR_AUTH_REQUIRED__c, New_Order_Auth_Products__c, Reorder_Auth_Products__c
                                                                    FROM Benefits__c 
                                                                    WHERE Id = : accnt.Primary_Benefit__c]);
                
                if(!benefitList.isEmpty()){
                    Benefits__c thisBenefit = benefitList[0]; 
                    
                    if(accnt.Is_BI_Active__c && !String.isBlank(String.valueOf(thisBenefit.Last_Benefits_Check_Date__c)) &&
                        !String.isBlank(String.valueOf(thisBenefit.Last_Benefits_Check_Done_By__c))){
                        //Param 1 : 
                        fieldValueMap.put('biActive', 'true');
                        }else
                         fieldValueMap.put('biActive', 'false'); 
                    
                    //Param 2 : 
                    if(!String.isBlank(String.valueOf(thisBenefit.Last_Benefits_Check_Date__c))){
                        Datetime biCheckDate = Datetime.newInstance(thisBenefit.Last_Benefits_Check_Date__c.year() , thisBenefit.Last_Benefits_Check_Date__c.month() , thisBenefit.Last_Benefits_Check_Date__c.day(), 00, 00, 00); 
                        fieldValueMap.put('lastBICheckDate', String.valueOf(biCheckDate.format('MM/dd/yyyy')));
                    }else 
                        fieldValueMap.put('lastBICheckDate', '');
                    //Param 3 : 
                    fieldValueMap.put('lastBIDoneBy', thisBenefit.Last_Benefits_Check_Done_By__c);
                    
                    string authRequired = thisBenefit.PRIOR_AUTH_REQUIRED__c == 'Y' ? 'Yes':'No';
                    fieldValueMap.put('authRequired', authRequired);
                    
                    string priorAuthProducts ='';
                    if(orderType == 'New Order' && !string.Isblank(thisBenefit.New_Order_Auth_Products__c)){
                      priorAuthProducts = thisBenefit.New_Order_Auth_Products__c.replace(';',',');  
                    }
                    else if(orderType == 'Reorder' && !string.Isblank(thisBenefit.Reorder_Auth_Products__c)){
                      priorAuthProducts = thisBenefit.Reorder_Auth_Products__c.replace(';',',');  
                    }
                    fieldValueMap.put('priorAuthProducts', priorAuthProducts);
                     
                        
                }else{
                        fieldValueMap.put('biActive', 'false');
                        fieldValueMap.put('lastBICheckDate', '');
                        fieldValueMap.put('lastBIDoneBy', '');
                        fieldValueMap.put('authRequired', '');
                        fieldValueMap.put('priorAuthProducts', '');
                    
                        
                }
                System.debug('fieldValueMap >> ' + fieldValueMap);
                
                System.debug('fieldValueMap Benefit Map>>' + fieldValueMap);
                return fieldValueMap;
            }
        }
        return null;         
    }   
}