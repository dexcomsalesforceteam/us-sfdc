/***
*@Author        : Priyanka Kajawe
*@Date Created  : 30-July-2018
*@Description   : wrapper class to create orders.
***/
Public class ClsOrderWrapper {
    public String uniqueIdentifier;
    //fields required to create Order
    public Id accountId;
    public String type; 
    public id recordTypeId; 
    public String status;
    public String subType; 
    public Date effectiveDate; 
    public Date scheduledShipDate; 
    public Id shippingAddress;
    public String shippingMethod;
    public Id priceBook;
    public Id orderId;
    public Id pricebookEntryId;
    public Integer quantity;
    public Double unitPrice;
    public boolean cmnVerification = false;
    public boolean aobVerification = false;
    public boolean signatureRequired = false;
    public String productName;
    public String reasonForApproval;
    public Map<String,Integer> productQuantityMap;
    public String sendErrorNotificationTo;
    public boolean WaiveShippingCharges = false;

}