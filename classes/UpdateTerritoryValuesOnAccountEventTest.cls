@istest
public class UpdateTerritoryValuesOnAccountEventTest {
      
    @istest
    public static void ProcessAccountTerritoryInfoUpdateEventTriggerTest(){
        id recId = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Consumers'].Id;
        id payorRecId = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Payor'].Id;
        
        //Insert terr alignment
       
        List <User> userList = new List <User>();
        for(Integer i = 0; i < 7; i++){
            User u = TestDataBuilder.getUser('System Administrator', 'User' + i + 'Test');
            userList.add(u);
        }
        insert userList;
        
        Territory_Alignment__c territory = TestDataBuilder.getTerritory('1234', userList);
        insert territory;
        
        Account consumer = TestDataBuilder.getAccountListConsumer(1, recId)[0];
        consumer.Territory_Code__c ='';
        insert consumer;
        
        Account consumer2 = TestDataBuilder.getAccountListConsumer(1, recId)[0];
        consumer2.Territory_Code__c =territory.Name;
        insert consumer2;
        Test.startTest();
        
        // Publish a test event
        Update_Territory_Values_On_Account__e event = new Update_Territory_Values_On_Account__e(Account_Id__c = consumer.id, Territory_Code__c=null);
        Database.SaveResult sr = EventBus.publish(event);
        
        Update_Territory_Values_On_Account__e event2 = new Update_Territory_Values_On_Account__e(Account_Id__c = consumer2.id, Territory_Code__c=territory.Name);
        Database.SaveResult sr2 = EventBus.publish(event2);
        // Deliver the initial event message.
        // This will fire the associated event trigger.
        Test.getEventBus().deliver(); 
        
        // Trigger retries event twice, so loop twice               
        for(Integer i=0;i<2;i++) {                           
            // Get info about all subscribers to the event
            EventBusSubscriber[] subscribers = [SELECT Name, Type, Position, Retries, LastError FROM EventBusSubscriber WHERE Topic='Update_Territory_Values_On_Account__e'];
            
            for (EventBusSubscriber sub : subscribers) {  
                System.debug('sub.Retries=' + sub.Retries);
                System.debug('sub.lastError=' + sub.lastError);
                if (sub.Name == 'OrderTriggerRetry') {
                    System.assertEquals(i+1, sub.Retries);
                } 
            }
            
            // Deliver the retried event
            Test.getEventBus().deliver(); 
        }
        
        Test.stopTest();    
        
    }
}