@isTest
private class OrderItemDetailTriggerHandler_Test {
  @testSetUp static void createRecords(){
    list <Order_Header__c> orderHeaderList;
    list <Order_Item_Detail__c> oidList = new list <Order_Item_Detail__c>();
    Account acc = new Account(Name = 'Test Acc');
    insert acc;
    Order_Header__c oh1 = new Order_Header__c(Account__c = acc.Id);
    Order_Header__c oh2 = new Order_Header__c(Account__c = acc.Id);
    Order_Header__c oh3 = new Order_Header__c(Account__c = acc.Id);
    
    orderHeaderList = new list <Order_Header__c>{oh1, oh2, oh3};
    insert orderHeaderList;
    
    oidList.add(new Order_Item_Detail__c(Order_Header__c = orderHeaderList[0].Id, Item_Number__c = 'STS'));
      
      
    // For Event Coverage 
    Order_Header__c oh4 = new Order_Header__c(Account__c = acc.Id, Order_Type__c = 'MC Standard Sales Order', Order_ID__c ='1234567890.00');  
    User dataIntegrator = [SELECT id from User WHERE alias = 'adata' LIMIT 1];  
    system.debug('Dataint' +dataIntegrator);  
    test.startTest();      
    System.runas(dataIntegrator) {
    
    insert oh4;
	Test.getEventBus().deliver() ;
    } 
    test.stopTest();    
    Insert oidList;
  }
  private static testMethod void recalculateOrderHeaderInsert(){
    list<Order_Header__c> ohList = [SELECT Id, Product_Type__c FROM Order_Header__c ORDER BY Name ASC];
 //   System.assertEquals('Sensor',ohList[0].Product_Type__c);
 //   System.assertEquals(null,ohList[1].Product_Type__c);
    System.assertEquals(null,ohList[2].Product_Type__c);
    
    Order_Item_Detail__c oid1  = new Order_Item_Detail__c(Item_Number__c = 'STS', Order_Header__c = ohList[0].Id);
    Order_Item_Detail__c oid2  = new Order_Item_Detail__c(Item_Number__c = 'STS', Order_Header__c = ohList[0].Id);
    Order_Item_Detail__c oid3  = new Order_Item_Detail__c(Item_Number__c = 'STT', Order_Header__c = ohList[0].Id);
    
    Order_Item_Detail__c oid4  = new Order_Item_Detail__c(Item_Number__c = 'STS', Order_Header__c = ohList[1].Id);
    Order_Item_Detail__c oid5  = new Order_Item_Detail__c(Item_Number__c = 'STK', Order_Header__c = ohList[1].Id);
    Order_Item_Detail__c oid6  = new Order_Item_Detail__c(Item_Number__c = 'STT', Order_Header__c = ohList[1].Id);
    
    Order_Item_Detail__c oid7  = new Order_Item_Detail__c(Item_Number__c = 'STS', Order_Header__c = ohList[2].Id);
    Order_Item_Detail__c oid8  = new Order_Item_Detail__c(Item_Number__c = 'STS', Order_Header__c = ohList[2].Id);
    Order_Item_Detail__c oid9  = new Order_Item_Detail__c( Item_Number__c = 'STS', Order_Header__c = ohList[2].Id);
      Order_Item_Detail__c oid10  = new Order_Item_Detail__c( Item_Number__c = 'STS', Order_Header__c = ohList[2].Id);  
    Product2 P = new Product2(Name = 'MT',ProductCode = 'STS',Generation__c = 'G5');
    test.startTest();
      insert new list <Order_Item_Detail__c>{oid1, oid2, oid3, oid4, oid5, oid6, oid7, oid8, oid9};
      insert p;
      insert oid10;
        
      
      
    test.stopTest();
    
    List<Order_Item_Detail__c> OP = [SELECT Id,Generation__c  FROM Order_Item_Detail__c WHERE Id =: oid10.Id];
    ohList = [SELECT Id, Product_Type__c FROM Order_Header__c ORDER BY Name ASC];
  //  System.assertEquals('Transmitter',ohList[0].Product_Type__c);
  //  System.assertEquals('System',ohList[1].Product_Type__c);
  //  System.assertEquals('Sensor',ohList[2].Product_Type__c);
  //    System.assertEquals(p.Generation__c, OP[0].Generation__c);  

  }
  
  private static testMethod void recalculateOrderHeaderUpdate(){
    list<Order_Header__c> ohList = [SELECT Id, Product_Type__c FROM Order_Header__c ORDER BY Name ASC];
    //System.assertEquals('Sensor',ohList[0].Product_Type__c);
   // System.assertEquals(null,ohList[1].Product_Type__c);
   // System.assertEquals(null,ohList[2].Product_Type__c);
    
    Order_Item_Detail__c oid1  = new Order_Item_Detail__c(Item_Number__c = 'STS', Order_Header__c = ohList[0].Id);
    Order_Item_Detail__c oid2  = new Order_Item_Detail__c(Item_Number__c = 'STS', Order_Header__c = ohList[0].Id);
    Order_Item_Detail__c oid3  = new Order_Item_Detail__c(Item_Number__c = 'STT', Order_Header__c = ohList[0].Id);
    
    Order_Item_Detail__c oid4  = new Order_Item_Detail__c(Item_Number__c = 'STS', Order_Header__c = ohList[1].Id);
    Order_Item_Detail__c oid5  = new Order_Item_Detail__c(Item_Number__c = 'STK', Order_Header__c = ohList[1].Id);
    Order_Item_Detail__c oid6  = new Order_Item_Detail__c(Item_Number__c = 'STT', Order_Header__c = ohList[1].Id);
    
    Order_Item_Detail__c oid7  = new Order_Item_Detail__c(Item_Number__c = 'STS', Order_Header__c = ohList[2].Id);
    Order_Item_Detail__c oid8  = new Order_Item_Detail__c(Item_Number__c = 'STS', Order_Header__c = ohList[2].Id);
    Order_Item_Detail__c oid9  = new Order_Item_Detail__c(Item_Number__c = 'STS', Order_Header__c = ohList[2].Id);
    
    list <Order_Item_Detail__c> oidList = new list <Order_Item_Detail__c>{oid1, oid2, oid3, oid4, oid5, oid6, oid7, oid8, oid9};
    insert oidList;
    oidList[2].Item_Number__c = 'STK';
    oidList[5].Item_Number__c = 'STS';
    oidList[7].Item_Number__c = 'STK';
    test.startTest();
      update oidList;
    test.stopTest();
    ohList = [SELECT Id, Product_Type__c FROM Order_Header__c ORDER BY Name ASC];
 //   System.assertEquals('System',ohList[0].Product_Type__c);
 //   System.assertEquals('System',ohList[1].Product_Type__c);
 //   System.assertEquals('System',ohList[2].Product_Type__c);

  }

    @IsTest
    static void testMarketingInteractionGeneration() {
        Id accountId = [SELECT Id FROM Account].Id;

        Order_Header__c orderHeader = new Order_Header__c();
        orderHeader.Order_Type__c = 'Standard Sales Order';
        orderHeader.Account__c = accountId;

        insert orderHeader;

        Order_Item_Detail__c orderItemDetail = new Order_Item_Detail__c();
        orderItemDetail.Order_Header__c = orderHeader.Id;
        orderItemDetail.Tracking_Number__c = '0'.repeat(30);

        Order_Item_Detail__c orderItemDetail2 = new Order_Item_Detail__c();
        orderItemDetail2.Order_Header__c = orderHeader.Id;
        orderItemDetail2.Tracking_Number__c = '0'.repeat(30);


        Test.startTest();
        insert new List<Order_Item_Detail__c> {orderItemDetail, orderItemDetail2};

        List<Marketing_Interaction__c> marketingInteractions = [SELECT Id, Communication_Type__c FROM Marketing_Interaction__c];
        System.debug(marketingInteractions);
        Test.stopTest();

        System.assertEquals(1, marketingInteractions.size());
    }

    @IsTest
    static void testMIUpdateGeneration() {
        Id accountId = [SELECT Id FROM Account].Id;

        Order_Header__c orderHeader = new Order_Header__c();
        orderHeader.Order_Type__c = 'Standard Sales Order';
        orderHeader.Account__c = accountId;

        insert orderHeader;

        Order_Item_Detail__c orderItemDetail = new Order_Item_Detail__c();
        orderItemDetail.Order_Header__c = orderHeader.Id;
        insert orderItemDetail;

        Test.startTest();

        List<Marketing_Interaction__c> initialMarketingInteractions = [
                SELECT Id
                FROM Marketing_Interaction__c
        ];

        orderItemDetail.Tracking_Number__c = '0'.repeat(30);
        update orderItemDetail;

        List<Marketing_Interaction__c> marketingInteractions = [SELECT Id, Communication_Type__c FROM Marketing_Interaction__c];
        System.debug(marketingInteractions);
        Test.stopTest();

        System.assertEquals(0, initialMarketingInteractions.size());
        System.assertEquals(1, marketingInteractions.size());
    }
}