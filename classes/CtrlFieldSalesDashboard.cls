/**********************************************
 * @author      : Sundog Interactive
 * @date        : JAN 25, 2019
 * @description : Server side controller for FieldSalesDashboard.cmp used in the Dexcom Prescriber Community
**********************************************/
public without sharing class CtrlFieldSalesDashboard {
    
    private static final List<String> PROFILE_NAMES = new List<String>{'Prescriber Community User', 'Prescriber Community TBM User', 'Prescriber Community DBM User'};
    private static String PRESCRIBER_PROFILE_ID = null;
    private static String TBM_PROFILE_ID = null;
    private static String DBM_PROFILE_ID = null;

    /**********************************************************
     **Description: Instantiates profile Id variables in lines 9-11
     **Parameters:  None
     **Returns:     None
     **Author:      Sundog Interactive
     **Date Created:   JAN 25, 2019
    **********************************************************/
    private static void setup() {
        for (Profile p : [SELECT Id, Name FROM Profile WHERE Name = :PROFILE_NAMES]) {
            if (p.Name == PROFILE_NAMES[0]) {
                PRESCRIBER_PROFILE_ID = p.Id;
                System.debug('PRESCRIBER_PROFILE_ID:' + PRESCRIBER_PROFILE_ID);
            } else if (p.Name == PROFILE_NAMES[1]) {
                TBM_PROFILE_ID = p.Id;
                System.debug('TBM_PROFILE_ID:' + TBM_PROFILE_ID);
            } else if (p.Name == PROFILE_NAMES[2]) {
                DBM_PROFILE_ID = p.Id;
                System.debug('DBM_PROFILE_ID:' + DBM_PROFILE_ID);
            }
        }
    }
    
    /**********************************************************
     **Description: Queries Territory_Alignment__c record(s) related to the running user
     **Parameters:  None
     **Returns:     List<Territory_Alignment__c>
     **Author:      Sundog Interactive
     **Date Created:   JAN 25, 2019
    **********************************************************/
    @AuraEnabled
    public static List<Territory_Alignment__c> getUserTerritories(){
        if (PRESCRIBER_PROFILE_ID == null){
            setup();
        }

        User u = [SELECT Id, ProfileId FROM User WHERE Id = :UserInfo.getUserId()];

        if (u.ProfileId == TBM_PROFILE_ID) {
            return [SELECT Id, Name, TBM__r.Name FROM Territory_Alignment__c where TBM__c = :u.Id];
        } else if (u.ProfileId == DBM_PROFILE_ID) {
            return [SELECT Id, Name, TBM__r.Name FROM Territory_Alignment__c where DBM__c = :u.Id];
        }

        return null;
    }

    
    /**********************************************************
     **Description: Returns the Account records of all the prescribers related to a Territory_Alignment__c record
     **Parameters:  Id of a Territory_Alignment__c record
     **Returns:     List<Account>
     **Author:      Sundog Interactive
     **Date Created:   JAN 25, 2019
    **********************************************************/
    private static List<Account> getPrescribersInTerritory(Id territoryId){
        System.debug('territoryId:' + territoryId);
        //ensure territory is valid
        Boolean valid = false;

        List<Territory_Alignment__c> territories = getUserTerritories();
        for (Territory_Alignment__c ta : territories){
            if (ta.Id == territoryId){
                valid = true;
                break;
            }
        }

        if (valid){
            return [SELECT Id, Name, PersonEmail, DOB__c FROM Account 
                WHERE Territory_ID_Lookup__c = :territoryId AND RecordType.DeveloperName = 'Prescriber' ORDER BY LastName,FirstName];
        }

        return null;
    }

    /*
     * 
     */ 
    /**********************************************************
     **Description: Returns to client certain opportunities and orders under a prescriber
     **Parameters:  Id of a Territory_Alignment__c record
     **Returns:     Account information, related opportunities and order headers in form of custom wrapper object ClsPrescriberDashboardHelper.OpportunitiesAndOrders
     **Author:      Sundog Interactive
     **Date Created:   JAN 25, 2019
    **********************************************************/
    @AuraEnabled
    public static List<ClsPrescriberDashboardHelper.OpportunitiesAndOrders> getOpportunitiesAndOrders(Id territoryId, String dateRange){
        List<Account> prescribers = getPrescribersInTerritory(territoryId);
        Boolean valid = prescribers.size() > 0;

        if (!valid){
            return null;
        }
        
        Map<Id, User> idToUserLoginMap = new Map<Id, User>();
        Set<Id> hasUser = new Set<Id>();
        for (User u : [SELECT Id, LastLoginDate, Username, Email, CreatedDate, Contact.PersonAccountId__c FROM User WHERE Contact.PersonAccountId__c = :prescribers]){
            hasUser.add(u.Contact.PersonAccountId__c);
            idToUserLoginMap.put(u.Contact.PersonAccountId__c, u);
        }

        System.debug('getOpportunitiesAndOrders-start');
        ClsPrescriberDashboardHelper.OpportunitiesAndOrders data = ClsPrescriberDashboardHelper.getOpportunitiesAndOrders(prescribers, dateRange);
        System.debug('getOpportunitiesAndOrders-end');

        //set up data as maps by prescriber
        Map<Id, List<Opportunity>> opportunityMap = new Map<Id, List<Opportunity>>();
        for (Opportunity o : data.opportunities){
            Id key = o.Prescribers__c;
            if (opportunityMap.containsKey(key)){
                opportunityMap.get(key).add(o);
            } else {
                opportunityMap.put(key, new List<Opportunity>{o});
            }
        }
        System.debug('opportunityMap-end');
        Map<Id, List<Order_Header__c>> orderMap = new Map<Id, List<Order_Header__c>>();
        for (Order_Header__c o : data.orders){
            Id key = o.Account__r.Prescribers__c;
            if (orderMap.containsKey(key)){
                orderMap.get(key).add(o);
            } else {
                orderMap.put(key, new List<Order_Header__c>{o});
            }
        }
        System.debug('orderMap-end');
        Map<Id, Map<String, Date>> prescriberToOHMap = new Map<Id, Map<String, Date>>();
        /*
        for(String orderNum : data.orderHeaderMap.keySet()){
            String prescriberId;
            // find the prescriber to which the order number belongs to if the order is not tied to an opportunity
            for (Order_Header__c o : data.orders){
                if(o.Order_Id__c == orderNum){
                    prescriberId = o.Account__r.Prescribers__c;
                }
            }
            // if the order number is tied to an opportunity
            if(String.isBlank(prescriberId)){
                for (Opportunity o : data.opportunities){
                    if(o.Order_NUM__c == orderNum){
                        prescriberId = o.Prescribers__c;
                    }
                }
            }
            // if the map of maps was already in the wrapper
            if(prescriberToOHMap.containsKey(prescriberId)){
                prescriberToOHMap.get(prescriberId).put(orderNum, data.orderHeaderMap.get(orderNum));
            }
            else{
                Map<String, Date> ohMap = new Map<String, Date>();
                ohMap.put(orderNum, data.orderHeaderMap.get(orderNum));
                prescriberToOHMap.put(prescriberId, ohMap);
            }
        }*/
        System.debug('prescriberToOHMap-end');
        List<ClsPrescriberDashboardHelper.OpportunitiesAndOrders> result = new List<ClsPrescriberDashboardHelper.OpportunitiesAndOrders>();

        Boolean firstPrescriber = true;
        //set maps by prescriber to instance of OpportunitiesAndOrders for each prescriber
        for (Account p : prescribers){
            ClsPrescriberDashboardHelper.OpportunitiesAndOrders accountData = new ClsPrescriberDashboardHelper.OpportunitiesAndOrders();
            
            User relatedUser = idToUserLoginMap.get(p.Id);
            if(relatedUser == null){
                accountData.registered = false;
                accountData.inviteSent = false;
                accountData.sendInvite = true;
            }
            else{
                accountData.sendInvite = false;
                if(relatedUser.LastLoginDate != null){
                    accountData.registered = true;
                    accountData.inviteSent = false;
                }
                else{
                    accountData.registered = false;
                    accountData.inviteSent = true;
                }
            }
            accountData.user = idToUserLoginMap.get(p.Id);
            accountData.account = p;
            /*
            if(prescriberToOHMap.containsKey(p.Id)){
                accountData.orderHeaderMap = prescriberToOHMap.get(p.Id);
            }
			*/

            if (opportunityMap.containsKey(p.Id)){
                accountData.opportunities = opportunityMap.get(p.Id);
            }

            if (orderMap.containsKey(p.Id)){
                accountData.orders = orderMap.get(p.Id);
            }

            if (accountData.opportunities.size() > 0 || accountData.orders.size() > 0 || relatedUser != null) {
                 if (firstPrescriber) {
                    accountData.orderHeaderMap = data.orderHeaderMap;
                    firstPrescriber = false;
                }
                result.add(accountData);
            }
        }
System.debug('result-end');
        return result;
    }
    
    /**********************************************************
     **Description: Creates a contact and relates it to existing person and business accounts. 
     **Parameters:  Id of a Territory_Alignment__c record related to the running user, Id of the person Account record related to the running user, the desired email address and username for the new user 
     **Returns:     ContactId of new partner user
     **Author:      Sundog Interactive
     **Date Created:   JAN 25, 2019
    **********************************************************/
    @AuraEnabled
    public static String createPrescriberUser(Id territoryId, Id personAccountId, String userEmail, String username){
        Boolean valid = prescriberSecurityCheck(territoryId, personAccountId);

        if (!valid){
            throw new AuraHandledException('Prescriber is not valid for territoryId: ' + territoryId);
        }
		
        Account businessAccount = [SELECT Id FROM Account WHERE Name = 'Prescriber Community' limit 1];
        User newUser;
        String result;
        // query necessary fields from the person account because it is more similar to contact than the business account
        Account personAccount = [SELECT Name, Phone, FirstName,LastName, PersonEmail, PersonMobilePhone, ShippingStreet, ShippingCity, 
                                 ShippingState, ShippingPostalCode, ShippingCountry FROM Account WHERE Id = :personAccountId];

        //has contact already been created without a user?
        List<Contact> existingContact = [SELECT Id FROM Contact WHERE PersonAccountId__c = :personAccountId];

        Id existingContactId = null;
        if (!existingContact.isEmpty()) {
            existingContactId = existingContact[0].Id; 
        }

        Contact relatedContact = new Contact(
            Id = existingContactId,
            AccountId = businessAccount.Id,
            PersonAccountId__c = personAccountId,
            Phone = personAccount.Phone,
            FirstName = personAccount.FirstName,
            LastName = personAccount.LastName,
            Email = userEmail,
            MobilePhone = personAccount.PersonMobilePhone,
            MailingStreet = personAccount.ShippingStreet,
            MailingCity = personAccount.ShippingCity,
            MailingState = personAccount.ShippingState,
            MailingPostalCode = personAccount.ShippingPostalCode,
            MailingCountry = personAccount.ShippingCountry
        );
        
        
        Savepoint sp = Database.setSavepoint();
        
        try{
            upsert relatedContact;

            result = relatedContact.Id;
            System.debug(relatedContact);

            User partnerUser = createPartnerUser(relatedContact.Id, username);
            insert partnerUser;
            System.debug('createPrescriberUser');
            System.debug(partnerUser);
            enablePartnerUser(relatedContact.Id, partnerUser.Id);
        }
        catch(Exception e){
            Database.rollback(sp);
            throw new AuraHandledException('[' + e.getLineNumber() + ']  ' + e.getCause() + '  -  ' + e.getMessage() + '  -  ' + e.getStackTraceString()); 
        }
        
        System.debug(result);
        return result;
    }
    
    /**********************************************************
     **Description: Creates a partner user for Dexcom Prescriber Community based on a contact
     **Parameters:  Id of contact to use as ContactId on new user record, desired username for new user
     **Returns:     None, future method
     **Author:      Sundog Interactive
     **Date Created:   JAN 25, 2019
    **********************************************************/
    //@future
    //private static void createPartnerUser(Id contactId, String partnerUserName){
    private static User createPartnerUser(Id contactId, String partnerUserName){
        String partnerAlias;
        setup();
        Contact relatedContact = [SELECT FirstName, LastName, Email FROM Contact WHERE Id = :contactId];
        if(relatedContact.LastName.length() > 3){
            partnerAlias = relatedContact.FirstName.substring(0,1) + relatedContact.LastName.substring(0, 4);
        }
        else{
            partnerAlias = relatedContact.FirstName.substring(0,1) + relatedContact.LastName.substring(0, relatedContact.LastName.length());
            for(Integer i = 0; i < 6 - partnerAlias.length(); i++){
                partnerAlias+= i;
            }
        }
        
        System.debug(partnerAlias);
        User partnerUser = new User( 
            ContactId = contactId,
            FirstName = relatedContact.FirstName,
            LastName = relatedContact.LastName,
            //Email = relatedContact.Email,
            Email = System.UserInfo.getUserEmail(),
            UserName = partnerUserName,
            IsActive = true,
            Alias = partnerAlias,
            TimeZoneSidKey = 'America/Chicago',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            ProfileId = PRESCRIBER_PROFILE_ID
        );
        System.debug('createPartnerUser!');
        System.debug(partnerUser);
        return partnerUser;
        /*
        // sends email to tbm
        insert partnerUser;

        // do update on user and send email to prescriber
        partnerUser = [SELECT IsPortalEnabled, Email FROM User WHERE Id = :partnerUser.Id];
		//partnerUser.Email = relatedContact.Email;
        partnerUser.IsPortalEnabled = true;
        update partnerUser;

        partnerUser.Email = relatedContact.Email;
        update partnerUser;
        System.resetPassword(partnerUser.Id, true);
		*/
    }

    @future
    private static void enablePartnerUser(Id contactId, Id userId){
        System.debug('enablePartnerUser');
        User partnerUser = [SELECT IsPortalEnabled, Email FROM User WHERE Id = :userId];
        partnerUser.IsPortalEnabled = true;
        partnerUser.Email = [SELECT Email FROM Contact WHERE Id = :contactId].Email;
        update partnerUser;
        System.debug(partnerUser);
        System.resetPassword(userId, true);
        
    }
    
    /**********************************************************
     **Description: Queries for matching usernames in this org
     **Parameters:  None
     **Returns:     Boolean indicating whether the username is unique (true) or not (false)
     **Author:      Sundog Interactive
     **Date Created:   JAN 25, 2019
    **********************************************************/
    @AuraEnabled
    public static Boolean checkUsernameUniqueness(String username){
        Boolean isUnique = true;
        Savepoint sp = Database.setSavepoint();
        for(User u : [SELECT Username FROM User WHERE Username = :username LIMIT 1]){
            isUnique = false;
        }
        return isUnique;
    }
    
    /**********************************************************
     **Description: Calls to create a new username string such that the string matches no other existing username in this org
     **Parameters:  Id of related Account to a prescriber, Id of related Territory_Alignment__c record for security check
     **Returns:     Contact record related to running user
     **Author:      Sundog Interactive
     **Date Created:   JAN 25, 2019
    **********************************************************/
    @AuraEnabled
    public static String suggestUsername(String personAccountId, String territoryId){
        Boolean isUserValid = prescriberSecurityCheck(territoryId, personAccountId);
        if(isUserValid){
            Boolean isUnique;
            Account personAccount = [SELECT Name, DOB__c FROM Account WHERE Id = :personAccountId];
            String suggestedUsername = createUsername(personAccount);
            isUnique = checkUsernameUniqueness(suggestedUsername);
            
            while(!isUnique){
                suggestedUsername = createUsername(personAccount);
                isUnique = checkUsernameUniqueness(suggestedUsername);
            }
            return suggestedUsername;
        }
        else{
            throw new AuraHandledException('Prescriber user is not valid for territoryId: ' + territoryId);
        }
    }
    
    /**********************************************************
     **Description: Creates username string based on information of an Account
     **Parameters:  Account
     **Returns:     Username String
     **Author:      Sundog Interactive
     **Date Created:   JAN 25, 2019
    **********************************************************/
    private static String createUsername(Account personAccount){
        String suggestedUsername = '';
        List<String> names = personAccount.Name.split(' ');
        suggestedUsername += String.join(names, '').toLowercase();
        Double seed = Math.random();
        suggestedUsername += Integer.valueOf(Math.ceil(seed * 10000));
        
        suggestedUsername += '@dexcomcommunity.com';
        return suggestedUsername;
    }
    
    /**********************************************************
     **Description: Updates information on a partner user
     **Parameters:  Id of user to be updated, Strings to update username and email fields (either can be null), Id of related Territory_Alignment__c record and Account to user for security check
     **Returns:     Updated User record
     **Author:      Sundog Interactive
     **Date Created:   JAN 25, 2019
    **********************************************************/
    @AuraEnabled
    public static User changeUserInfo(String userId, String newUsername, String newEmail, String territoryId, String personAccountId){
        Boolean isUserValid = prescriberSecurityCheck(territoryId, personAccountId);
        if(isUserValid){
            User prescriberUser = [SELECT Username, Email FROM User WHERE Id = :userId];
            if(newUsername != null && !String.isBlank(newUsername)){
                prescriberUser.Username = newUsername;
            }
            if(newEmail != null && !String.isBlank(newEmail)){
                prescriberUser.Email = newEmail;
            }
            update prescriberUser;
            return prescriberUser;
        }
        else{
            throw new AuraHandledException('Prescriber user is not valid for territoryId: ' + territoryId);
        }
    }
    
    /**********************************************************
     **Description: Resets password on a partner user
     **Parameters:  Id of user to be reset, Id of related Territory_Alignment__c record and Account to user for security check
     **Returns:     None
     **Author:      Sundog Interactive
     **Date Created:   JAN 25, 2019
    **********************************************************/
    @AuraEnabled
    public static void resetPassword(String userId, String territoryId, String personAccountId){
        Boolean isUserValid = prescriberSecurityCheck(territoryId, personAccountId);
        if(isUserValid){
            System.resetPassword(userId, true);
        }
        else{
            throw new AuraHandledException('Prescriber user is not valid for territoryId: ' + territoryId);
        }
    }


    /**********************************************************
     **Description: An empty call to the server to ensure that the Salesforce login session for the user does not time out.
     *              (Most actions for this component are contained in the client side and the activity is not registered by Salesforce,
     *              therefore we have this empty method in the server side in order to enforce session logout and extend session timers at a Salesforce level)
     **Parameters:  None
     **Returns:     Boolean indicating if security check passed (true) or failed (false)
     **Author:      Sundog Interactive
     **Date Created:   MAY 21, 2019
    **********************************************************/
    @AuraEnabled
    public static void resetSessionTimer(){
        System.debug('resetting session timer');
    }
    
    /**********************************************************
     **Description: Performs security check by making sure that a prescriber Account is related to a Territory_Alignment__c record
     **Parameters:  None
     **Returns:     Boolean indicating if security check passed (true) or failed (false)
     **Author:      Sundog Interactive
     **Date Created:   JAN 25, 2019
    **********************************************************/
    private static Boolean prescriberSecurityCheck(String territoryId, String personAccountId){
        List<Account> prescribers = getPrescribersInTerritory(territoryId);

        Boolean valid = false;
        for (Account p : prescribers){
            if (p.Id == personAccountId){
                valid = true;
                break;
            }
        }
        return valid;
    }


}