@isTest
private class BclsPendingAuthUpdateBatchTest {
    
    private final static List<String> typeList = new List<String>{'NEW SYSTEM', 'Medicare – New Patient', 'Physician Referral', 'OOW Receiver Only', 'OOW SYSTEM', 'OOW Transmitter Only', 'Sensor-Reorder'};

    @testSetup
    private static void testSetup(){
        List<Account> toInsert = new List<Account>();
        
        Account payorAccount = new Account();
        payorAccount.Name = 'Test Payor';
        payorAccount.RecordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Account' AND Name = 'Payor'].Id;
        toInsert.add(payorAccount);
        Account consumerAccount = TestDataBuilder.testAccount();
        consumerAccount.FirstName = 'Consumer';
        consumerAccount.LastName = 'Test';
        consumerAccount.RecordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Account' AND Name = 'Consumers'].Id;
        consumerAccount.Territory_Code__c = 'fakecode';
        toInsert.add(consumerAccount);

        insert toInsert;    
    }
    
    @isTest
    private static void testRunBatchPositive(){
        // create 7 opportunities that should generate Marketing_Interaction__c records:
        // all opportunities must have Onboarding_Steps__c = 'Authorization' AND Onboarding_Step_Update_Date__c != Null 
        // AND Onboarding_Step_Update_Date__c > [7 business days] AND RecordType.Name = 'US Opportunity'
        // there must be one opportunity for each one of the 7 Type values in typeList
        List<Opportunity> toInsert = new List<Opportunity>();
        List<Marketing_Interaction__c> marketingInteractionList;
        Set<Id> oppIdSet = new Set<Id>();
        Account payorAccount = [SELECT Id FROM Account WHERE RecordType.Name = 'Payor' LIMIT 1];
        Account consumerAccount = [SELECT Id FROM Account WHERE RecordType.Name = 'Consumers' LIMIT 1];
        Id oppRecordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Opportunity' AND Name = 'US Opportunity'].Id;
        Integer index = 0;
        for(Opportunity o : TestDataBuilder.getOpportunityList(7, consumerAccount.Id, payorAccount.Id)){
            o.Onboarding_Steps__c = 'Authorization';
            o.Onboarding_Step_Update_Date__c = Date.Today().addDays(-20);
            o.RecordTypeId = oppRecordTypeId;
            o.Type = typeList[index];
            o.StageName = '5. Prior-Authorization';
            toInsert.add(o);
            index++;
        }
        
        // before running batch job check that there are no existing Marketing_Interaction__c records
        marketingInteractionList = [SELECT Id FROM Marketing_Interaction__c];
        System.assertEquals(0, marketingInteractionList.size());
        
        // insert test opportunities and run the batch
        Test.startTest();
        insert toInsert;
        for(Opportunity o : toInsert){
            oppIdSet.add(o.Id);
        }
        DataBase.executeBatch(new BclsPendingAuthUpdateBatch());
        Test.stopTest();
        
        // assert that there has been one Marketing_Interaction__c record for each one of the 7 opportunities
        marketingInteractionList = [SELECT Id, Source_Record_Id__c, Account__c, Communication_Type__c FROM Marketing_Interaction__c];
        System.assertEquals(7, marketingInteractionList.size());
        
        // assert that each Marketing_Interaction__c record is correctly related to an opportunity
        for(Marketing_Interaction__c mi : marketingInteractionList){
            System.assert(oppIdSet.contains(mi.Source_Record_Id__c));
            System.assertEquals(consumerAccount.Id, mi.Account__c);
            System.assertEquals('Authorization Status', mi.Communication_Type__c);
        }
    }
    
    @isTest
    private static void testRunBatchNegative(){
        // create opportunities that should NOT generate Marketing_Interaction__c records:
        List<Opportunity> toInsert = new List<Opportunity>();
        List<Marketing_Interaction__c> marketingInteractionList;
        Account payorAccount = [SELECT Id FROM Account WHERE RecordType.Name = 'Payor' LIMIT 1];
        Account consumerAccount = [SELECT Id FROM Account WHERE RecordType.Name = 'Consumers' LIMIT 1];
        Id usOppRecordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Opportunity' AND Name = 'US Opportunity'].Id;
        Id otherOppRecordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Opportunity' AND Name = 'GB Opportunity' LIMIT 1].Id;
        List<String> otherTypeValues = new List<String>();
        List<String> otherOnboardingSteps = new List<String>();
        
        for(Schema.PicklistEntry v : Opportunity.Type.getDescribe().getPicklistValues()){
            if(!typeList.contains(v.getValue())){
                otherTypeValues.add(v.getValue());
            }
        }
        
        for(Schema.PicklistEntry v : Opportunity.Onboarding_Steps__c.getDescribe().getPicklistValues()){
            if(v.getValue() != 'Authorization'){
                otherOnboardingSteps.add(v.getValue());
            }
        }
        // an opportunity for each Onboarding_Steps__c value in the otherOnboardingSteps list but of qualifying Onboarding_Step_Update_Date__c, RecordType and Type
        Integer index = 0;
        for(Opportunity o : TestDataBuilder.getOpportunityList(otherOnboardingSteps.size(), consumerAccount.Id, payorAccount.Id)){
            o.Onboarding_Steps__c = otherOnboardingSteps[index];
            o.Onboarding_Step_Update_Date__c = Date.Today().addDays(-20);
            o.RecordTypeId = usOppRecordTypeId;
            o.Type = typeList[index];
            o.StageName = '5. Prior-Authorization';
            //o.Payor_Code__c = 'A';
            toInsert.add(o);
            index++;
        }
        // an opportunity with Onboarding_Step_Update_Date__c > Date.Today().addDays(-14) but qualifying Onboarding_Steps__c, RecordType and Type
        index = 0;
        for(Opportunity o : TestDataBuilder.getOpportunityList(1, consumerAccount.Id, payorAccount.Id)){
            o.Onboarding_Steps__c = 'Authorization';
            o.Onboarding_Step_Update_Date__c = Date.Today().addDays(-2);
            o.RecordTypeId = usOppRecordTypeId;
            o.Type = typeList[index];
            o.StageName = '5. Prior-Authorization';
            //o.Payor_Code__c = 'A';
            toInsert.add(o);
            index++;
        }
        // an opportunity with RecordType.Name != 'US Opportunity' but with qualifying Onboarding_Steps__c, Onboarding_Step_Update_Date__c and Type
        index = 0;
        for(Opportunity o : TestDataBuilder.getOpportunityList(1, consumerAccount.Id, payorAccount.Id)){
            o.Onboarding_Steps__c = 'Authorization';
            o.Onboarding_Step_Update_Date__c = Date.Today().addDays(-20);
            o.RecordTypeId = otherOppRecordTypeId;
            o.Type = typeList[index];
            o.StageName = '5. Prior-Authorization';
            //o.Payor_Code__c = 'A';
            toInsert.add(o);
            index++;
        }
        // an opportunity for each of the non-qualifying Type values but with qualifying Onboarding_Steps__c, Onboarding_Step_Update_Date__c and RecordType
        index = 0;
        for(Opportunity o : TestDataBuilder.getOpportunityList(otherTypeValues.size(), consumerAccount.Id, payorAccount.Id)){
            o.Onboarding_Steps__c = 'Authorization';
            o.Onboarding_Step_Update_Date__c = Date.Today().addDays(-20);
            o.RecordTypeId = usOppRecordTypeId;
            o.StageName = '5. Prior-Authorization';//o.Payor_Code__c = 'A';
            o.Type = otherTypeValues[index];
            toInsert.add(o);
            index++;
        }
        // an opportunity that qualifies for all query criteria except for StageName
        for(Opportunity o : TestDataBuilder.getOpportunityList(1, consumerAccount.Id, payorAccount.Id)){
            o.Onboarding_Steps__c = 'Authorization';
            o.Onboarding_Step_Update_Date__c = Date.Today().addDays(-20);
            o.RecordTypeId = usOppRecordTypeId;
            o.Type = typeList[0];
            o.StageName = '10. Cancelled';
            o.Close_Reason__c = 'Cancelled - Distributor Denied';
            toInsert.add(o);
            index++;
        }
        
        // before running batch job check that there are no existing Marketing_Interaction__c records
        marketingInteractionList = [SELECT Id FROM Marketing_Interaction__c];
        System.debug(marketingInteractionList);
        System.assertEquals(0, marketingInteractionList.size());
        
        // insert test opportunities and run the batch
        Test.startTest();
        insert toInsert;
        for(Opportunity o : toInsert){
            System.debug(o);
        }
        DataBase.executeBatch(new BclsPendingAuthUpdateBatch());
        Test.stopTest();
        
        // assert that there has been no Marketing_Interaction__c record for each one of the non-qualifying opportunities
        marketingInteractionList = [SELECT Id, Communication_Type__c, Source_Record_Id__c FROM Marketing_Interaction__c];
        
        System.assertEquals(0, marketingInteractionList.size());
    }
    
    @isTest
    private static void testScheduler(){
        List<CronTrigger> scheduledJobs = [SELECT Id FROM CronTrigger];
        
        Test.startTest();
        String jobId = System.Schedule('BclsDocCollectionUpdateBatch'+String.valueOf(system.now()), BclsPendingAuthUpdateSched.CRON_EXP, new BclsPendingAuthUpdateSched());
        Test.stopTest();
        
        // assert correct job has been scheduled with CRON Expression
        System.assertEquals(scheduledJobs.size() + 1, [SELECT Id FROM CronTrigger].size());
        CronTrigger ct = [SELECT CronExpression FROM CronTrigger WHERE Id = :jobId];
        System.assertEquals(BclsPendingAuthUpdateSched.CRON_EXP, [SELECT CronExpression FROM CronTrigger WHERE Id = :jobId].CronExpression);
    }
    
}