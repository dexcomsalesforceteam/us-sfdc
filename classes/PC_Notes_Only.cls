/************************************
 *  PC Notes Only
 * Author : Venkat Kollimerla
 * Description:
 * This class collects the DX Notes to be presented to the TBMs and PCS users that includes only the PCS-generated DX Notes.
**************************************/
public with sharing class PC_Notes_Only{

Public List<DX_Notes__c> notesList { get;set; }
Public id AccRecId {get;set;}
public  string accName {get;set;}
    
 /**   ----- Canada Enhancement           **/
Public List<DX_Notes__c> CAnotesList { get;set; }
Public id CAAccRecId {get;set;}
public  string CAaccName {get;set;}

    public PC_Notes_Only(ApexPages.StandardController controller) 
    { 
         AccRecId = [select id FROM Account where id =: ApexPages.currentPage().getParameters().get('id')].id;
         accName =  [select id,Name FROM Account where id =: ApexPages.currentPage().getParameters().get('id')].Name;
             
         notesList = [select id,Name,Call_Type__c,Comments__c,CreatedById,Oracle_Created_By__c,Oracle_Date_Created__c,CreatedDate,PC_Reasons__c,PC_Reason_Code_1__c,PC_Reason_Code_2__c,PC_Reason_Code_3__c,Interaction_Status__c FROM DX_Notes__c WHERE ( CONSUMER__c = : AccRecId AND Call_Type__c != '' AND RecordType.DeveloperName !='Retention_Notes') ORDER BY CreatedDate DESC];
    
         CAAccRecId = [select id FROM Account where id =: ApexPages.currentPage().getParameters().get('id')].id;
         CAaccName =  [select id,Name FROM Account where id =: ApexPages.currentPage().getParameters().get('id')].Name;
             
         CAnotesList = [select id,Name,Call_Type__c,Comments__c,CreatedById,Oracle_Created_By__c,Oracle_Date_Created__c,CreatedDate,PC_Reasons__c,PC_Reason_Code_1__c,PC_Reason_Code_2__c,PC_Reason_Code_3__c,Interaction_Status__c FROM DX_Notes__c WHERE ( CONSUMER__c = : AccRecId AND Call_Type__c != '' AND RecordType.Name IN('CA PCS Notes')) ORDER BY CreatedDate DESC];    //AND (RecordType.DeveloperName ='CA DX Notes')
    
    
    
    }   
    
}