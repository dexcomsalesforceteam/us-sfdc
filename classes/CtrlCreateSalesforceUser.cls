/*
 * Class is to create users in Salesforce
 * Created by LTI
 * This is consumed by PermitSFLicenses lightning component
*/
public with sharing class CtrlCreateSalesforceUser {
    
    public static final String DEXCOM_EMAIL_ADDRESS= '@dexcom.com';
    
    /*
     * This method saves record in the user object
     * if insertion is failed then it will create a record in the apex debug log object
	*/
    @AuraEnabled
    public static void saveUsers(String userList){
        system.debug('listUser::::'+userList);
        List<User> insertUserList = (List<User>) JSON.deserialize(userList, List<User>.class);
        String oracleUserName;
        for(User userObj : insertUserList){
            oracleUserName = userObj.Oracle_User_Name__c;
            if(String.isNotBlank(oracleUserName)){
                oracleUserName = oracleUserName.deleteWhitespace();
                oracleUserName = oracleUserName.toLowerCase();
            }
            userObj.FederationIdentifier = oracleUserName + DEXCOM_EMAIL_ADDRESS;
            userObj.UserName = oracleUserName + DEXCOM_EMAIL_ADDRESS;
            userObj.Oracle_User_Name__c = oracleUserName;
            userObj.Alias = userObj.FirstName.substring(0,1) + userObj.LastName.substring(0,1);
            userObj.LocaleSidKey = system.label.User_Locale_Sid_Key;
            userObj.TimeZoneSidKey = system.label.Time_Zone_Sid_Key;
            userObj.EmailEncodingKey = system.label.Email_Encoding_Key;
            userObj.LanguageLocaleKey = system.label.User_Locale_Sid_Key;
            system.debug('userObj::::'+userObj);
        }
        if(!insertUserList.isEmpty()){
            Database.SaveResult[] saveResultList = Database.insert(insertUserList,false);
            for(Database.SaveResult saveResultObj : saveResultList){
                if(!saveResultObj.isSuccess()){
                    String errorMsg = saveResultObj.getErrors().get(0).getFields() + ':' + saveResultObj.getErrors().get(0).getMessage();
                    system.debug('####errorMsg - ' + errorMsg);
                    if(errorMsg.Length() >= ClsApexConstants.TWO_HUNDRED_INTEGER)
                        errorMsg = errorMsg.substring(ClsApexConstants.ZERO_INTEGER,ClsApexConstants.TWO_HUNDRED_INTEGER);
                    //Write the error log
                    new ClsApexDebugLog().createLog(
                        new ClsApexDebugLog.Error(
                            'PermitSFLicenseController',
                            'saveUsers',
                            ClsApexConstants.EMPTY_STRING,
                            errorMsg
                        )); 
                }
            }
        }
    }
   	
    
    /*
     * @Description - This will call fetch picklist values and based on object API name
     * and field API name
     * if field api name is blank then we will fetch the data from objects like profiles, roles, products
     * if field api name is present then we will call the picklist value fetch mehods
	*/ 
    @AuraEnabled
    public static List<String> getPicklistData(String sobjectApiName, String fieldApiName){
        List<String> picklistValueList = new List<String>();
        if(String.isNotBlank(fieldApiName)){
            picklistValueList = ClsApexUtility.getPickListValuesIntoList(sobjectApiName, fieldApiName);
        }else{
            if(String.isNotBlank(sobjectApiName)){
                List<sobject> fetchedResults = getSobjectRecordList(sobjectApiName);
                picklistValueList = getObjectIdNameValues(fetchedResults);
            }
        }
        return picklistValueList;
    }
    
    
    /*
     * @Description - This function will fetch Id, Name values from the given
     * sobject
	*/
    public static List<sobject> getSobjectRecordList(String sobjectApiName){
        List<sobject> sobjectList = new List<sobject>();
        String query1 = 'Select Id, Name From ';
        query1 += sobjectApiName;
        sobjectList = Database.query(query1);
       	return sobjectList;
    }
    
    /*
     * @Description - getObjectIdNameValues this will return list of string with
     * name and Id list to the calling method
	*/
    public static List<String> getObjectIdNameValues(List<sobject> sobjectListToProcess){
        List<String> sobjectDetailsList = new List<String>();
        for(sobject sobjectRecord : sobjectListToProcess){
            String objectDetails = String.valueOf(sobjectRecord.get('Name'));
            objectDetails += ':';
            objectDetails += String.valueOf(sobjectRecord.get('Id'));
            sobjectDetailsList.add(objectDetails);
        }
        return sobjectDetailsList;
    }
    
}