@isTest
private class CtrlAccountEligibilityMatrixTest {
   
    @testVisible static List< Eligibility_Upgrade_Matrix__mdt> customMetadata { 
            get {
                if ( customMetadata == null )
                    customMetadata = [ SELECT G6_Force_Purchase_R_TX_S__c FROM Eligibility_Upgrade_Matrix__mdt ]; 
                return customMetadata;
            } set; }
   
    
    static testmethod void testgetAccountEligibilityMatrixDetails(){
        
        Eligibility_Upgrade_Matrix__mdt eum = new Eligibility_Upgrade_Matrix__mdt();
        Account testAccount = TestDataBuilder.testAccount();
        insert testAccount;
        Account testAccount1 = TestDataBuilder.testAccount();
        testAccount1.LastName = 'Testlastname1';
        testAccount1.PersonEmail = 'Test1@gmail.com';
        insert testAccount1;
        
        String currentReceiverGen = 'G5';
        
        list <Order_Header__c> orderHeaderList;
        list <Order_Item_Detail__c> oidList = new list <Order_Item_Detail__c>();
       
        Order_Header__c oh1 = new Order_Header__c(Account__c = testAccount.Id, Order_Type__c ='Standard Sales Order', Latest_Transmitter_Generation__c = 'G5');
        Order_Header__c oh2 = new Order_Header__c(Account__c = testAccount1.Id, Order_Type__c ='Standard Sales Order', Latest_Receiver_Generation__c = 'G5');
        Order_Header__c oh3 = new Order_Header__c(Account__c = testAccount1.Id, Order_Type__c ='Standard Sales Order', Latest_Sensor_Generation__c = 'G5');
        
        orderHeaderList = new list <Order_Header__c>{oh1, oh2, oh3};
        insert orderHeaderList;
        
        oidList.add(new Order_Item_Detail__c(Order_Header__c = orderHeaderList[0].Id, Item_Number__c = 'STS-GL-041', Shipping_Date__c = System.today() - 5));
        oidList.add(new Order_Item_Detail__c(Order_Header__c = orderHeaderList[0].Id, Item_Number__c = 'STT-GF-001', Shipping_Date__c = System.today() - 5));
        oidList.add(new Order_Item_Detail__c(Order_Header__c = orderHeaderList[0].Id, Item_Number__c = 'STK-SD-001', Shipping_Date__c = System.today() - 5));
        //oidList.add(new Order_Item_Detail__c(Order_Header__c = orderHeaderList[1].Id, Item_Number__c = 'STS-GL-041', Shipping_Date__c = System.today() - 545));
        //oidList.add(new Order_Item_Detail__c(Order_Header__c = orderHeaderList[1].Id, Item_Number__c = 'STT-GF-001', Shipping_Date__c = System.today() - 545));
        //oidList.add(new Order_Item_Detail__c(Order_Header__c = orderHeaderList[1].Id, Item_Number__c = 'STK-SD-001', Shipping_Date__c = System.today() - 545));
        insert oidList;
        
        testAccount1.Latest_Transmitter_Order__c = oh1.Id;
        testAccount1.Latest_Receiver_Order__c = oh2.Id;
        testAccount1.Latest_Sensor_Order__c = oh3.Id;
        update testAccount1;
        
        Test.startTest();
			Map<String, String> result = CtrlAccountEligibilityMatrix.getAccountEligibilityMatrixDetails(testAccount.Id);
        	Map<String, String> result1 = CtrlAccountEligibilityMatrix.getAccountEligibilityMatrixDetails(testAccount1.Id);
        Test.stopTest();
        
        system.assertEquals(result.isEmpty(), FALSE);
        system.assertEquals(result1.isEmpty(), FALSE);
	}
}