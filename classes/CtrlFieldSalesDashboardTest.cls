/**
 * Test class for server side controller CtrlFieldSalesDashboard and ClsPrescriberDashboardHelper
 * @author Sundog
 * @date 01/28/2019
 * 
 */ 

@isTest
private class CtrlFieldSalesDashboardTest {
    
    @testSetup
    private static void createTestData(){
        User ownerUser;
        User standardUser;
        User DBMUser;
        User TBMUser;
        User prescriberUser;
        Account TBMAccount;
        Account DBMAccount;
        Account prescriberCommunityAccount;
        Account consumerAccount;
        Account prescriberPersonAccount;
        Account payorAccount;
        Contact TBMContact;
        Contact DBMContact;
        Contact prescriberContact;
        
        List<User> userList = new List<User>();
        List<Account> testAccountList = new List<Account>();
        List<Contact> testContactList = new List<Contact>();
        List<Opportunity> patientOpportunitiesList;
        List<sObject> patientRecordsList = new List<sObject>();
        
        // necessary users for community and user creation
        standardUser = TestDataBuilder.getUser('System Administrator', 'testadmin');
        
        System.runAs(standardUser){
            ownerUser = TestDataBuilder.getUser('SERVICE', 'CommunityOwner');
            ownerUser.UserRoleId = [SELECT Id FROM UserRole WHERE Name = 'Training'].Id;
            insert ownerUser;
        }
        
        // DBM and TBM Community Users
        System.runAs(ownerUser){
            TBMAccount = new Account(
                Name = 'Territory Business Managers',
                RecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND Name = 'Business Account'].Id
            );
            testAccountList.add(TBMAccount);
            
            DBMAccount = new Account(
                Name = 'District Business Managers',
                RecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND Name = 'Business Account'].Id
            );
            testAccountList.add(DBMAccount);

            prescriberCommunityAccount = new Account(
                Name = 'Prescriber Community',
                RecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND Name = 'Business Account'].Id
            );
            testAccountList.add(prescriberCommunityAccount);
            insert testAccountList;

            prescriberCommunityAccount.IsPartner = true;
            update prescriberCommunityAccount;
            
            TBMContact = new Contact(
                FirstName = 'Test',
                LastName = 'TBM',
                AccountId = TBMAccount.Id,
                Email = 'tbm@test.com'
            );
            testContactList.add(TBMContact);
            
            DBMContact = new Contact(
                FirstName = 'Test',
                LastName = 'DBM',
                AccountId = DBMAccount.Id,
                Email = 'dbm@test.com'
            );
            testContactList.add(DBMContact);
            insert testContactList;
            
            
            DBMUser = TestDataBuilder.getUser('Prescriber Community DBM User', 'dbmuser');
            DBMUser.ContactId = DBMContact.Id;
            userList.add(DBMUser);
            
            TBMUser = TestDataBuilder.getUser('Prescriber Community TBM User', 'tbmuser');
            TBMUser.ContactId = TBMContact.Id;
            userList.add(TBMUser);
            insert userList;
        }
        
        testAccountList = new List<Account>();
        
        // Territory_Alignment__c
        Territory_Alignment__c territory = new Territory_Alignment__c(
            Name = 'Test Territory',
            Territory_Name__c = 'Minneapolis W, MN',
            Territory_Code__c = '01333',
            DBM__c = DBMUser.Id,
            TBM__c = TBMUser.Id
        );
        
        insert territory;
        
        // prescriber user
        // second prescriber without user
        
        
        prescriberPersonAccount = TestDataBuilder.testAccount();
        prescriberPersonAccount.RecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND Name = 'Prescriber'].Id;
        prescriberPersonAccount.Territory_ID_Lookup__c = territory.Id;
        testAccountList.add(prescriberPersonAccount);
        
        payorAccount = new Account(
            Name = 'Test Payor',
            Phone = '123456789',
            DOB__c = Date.newInstance(1992, 04, 23),
            RecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND Name = 'Payor'].Id
        );
        testAccountList.add(payorAccount);
        
        Account medicalFacility = new Account(
            Name = 'Test Hospital',
            RecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND Name = 'Medical Facility'].Id
        );
        testAccountList.add(medicalFacility);
        insert testAccountList;
        
        consumerAccount = TestDataBuilder.testAccount();
        consumerAccount.FirstName = 'Test';
        consumerAccount.LastName = 'Patient';
        consumerAccount.Prescribers__c = prescriberPersonAccount.Id;
        consumerAccount.Age_Number__c = Double.valueOf(24);
        consumerAccount.Generation__c = '';
        consumerAccount.Payor__c = payorAccount.Id;
        consumerAccount.marketing_sales_channel__c = 'Dexcom';
        consumerAccount.Medical_Facility__c = medicalFacility.Id;
        consumerAccount.RecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND Name = 'Consumers'].Id;
        insert consumerAccount;

    }

    
    @isTest
    private static void getUserTerritoriesDBMTest(){
        List<Territory_Alignment__c> result;
        User DBM = [SELECT Id FROM User WHERE Profile.Name = 'Prescriber Community DBM User' and IsActive = true LIMIT 1];
        List<Territory_Alignment__c> DBMTerritories = [SELECT Id, Name, TBM__r.Name FROM Territory_Alignment__c where DBM__c = :DBM.Id];
        System.runAs(DBM){
            Test.startTest();
            result = CtrlFieldSalesDashboard.getUserTerritories();
            Test.stopTest();
        }
        System.assertEquals(DBMTerritories.size(), result.size());
        System.assert(DBMTerritories == result);
    }
    
    @isTest
    private static void getUserTerritoriesNegativeTest(){
        List<Territory_Alignment__c> result;
        User invalidUser = [SELECT Id FROM User WHERE Username = 'CommunityOwner@hesser.com' and IsActive = true LIMIT 1];
        System.runAs(invalidUser){
            Test.startTest();
            result = CtrlFieldSalesDashboard.getUserTerritories();
            Test.stopTest();
        }
        System.assert(result == null);
    }
    
    @isTest
    private static void getOpportunitiesAndOrdersOfInviteSentPrescriber(){
        User communityOwner = [SELECT Id FROM User WHERE Username = 'CommunityOwner@hesser.com' and IsActive = true LIMIT 1];
        User TBM = [SELECT Id FROM User WHERE Profile.Name = 'Prescriber Community TBM User' AND Username = 'tbmuser@hesser.com' LIMIT 1];
        Account prescriberPersonAccount = [SELECT Id FROM Account WHERE RecordType.Name = 'Prescriber' LIMIT 1];
        Account consumerAccount = [SELECT Id FROM Account WHERE Prescribers__c = :prescriberPersonAccount.Id LIMIT 1];
        Account payorAccount = [SELECT Id FROM Account WHERE RecordType.Name = 'Payor' AND Name = 'Test Payor' LIMIT 1];
        List<sObject> patientRecordsList = new List<sObject>();
        List<ClsPrescriberDashboardHelper.OpportunitiesAndOrders> oppsAndOrders;
        List<Territory_Alignment__c> TBMTerritories = [SELECT Id, Name, TBM__r.Name FROM Territory_Alignment__c where TBM__c = :TBM.Id LIMIT 1]; // set LIMIT so we know the size here is 1
        Account prescriberCommunityAccount = [SELECT Id FROM Account WHERE Name = 'Prescriber Community' LIMIT 1];
        Contact prescriberContact;
        User prescriberUser;
        
        // create the prescriber user
        System.runAs(communityOwner){
            prescriberContact = new Contact(
                FirstName = 'Test',
                LastName = 'Contact',
                PersonAccountId__c = prescriberPersonAccount.Id,
                AccountId = prescriberCommunityAccount.Id,
                Email = 'sundog@test.com'
            );
            insert prescriberContact;
            
            prescriberUser = TestDataBuilder.getUser('Prescriber Community User', 'TestPrescriber');
            prescriberUser.ContactId = prescriberContact.Id;
            insert prescriberUser;
        }
        
        // insert opportunities and orders. I'm putting this inside the test context to avoid hitting query limits.
        Test.startTest();
        List<Opportunity> patientOpportunitiesList = TestDataBuilder.getOpportunityList(1, consumerAccount.Id, payorAccount.Id);
        for(Opportunity opp : patientOpportunitiesList){
            opp.Sales_Channel__c = 'Dexcom';
            opp.StageName = '1. New Opportunity';
            opp.Order_NUM__c = String.valueOf(Math.ceil(Math.random() * 1000));
            opp.Prescribers__c = prescriberPersonAccount.Id;
            patientRecordsList.add(opp);
        }
        
        Order_Header__c order = new Order_Header__c(
            Account__c = consumerAccount.Id,
            Status__c = 'SHIPPED',
            Order_ID__c = String.valueOf(Math.ceil(Math.random() * 1000)),
            Booked_Date__c = Date.today().addDays(-5)
        );
        patientRecordsList.add(order);
        insert patientRecordsList;
        
        // get consumer records related to the prescriber
        System.runAs(TBM){
            oppsAndOrders = CtrlFieldSalesDashboard.getOpportunitiesAndOrders(TBMTerritories[0].Id, 'past-3-months');
        }
        Test.stopTest();
        
        // verify that prescriber is in correct state and that records where retrieved as expected
        System.assertEquals(1, oppsAndOrders[0].opportunities.size());
        System.assertEquals(1, oppsAndOrders[0].orders.size());
        System.assertEquals(true, oppsAndOrders[0].inviteSent);
        System.assertEquals(false, oppsAndOrders[0].registered);
        System.assertEquals(false, oppsAndOrders[0].sendInvite);
    }
    
    
    @isTest
    private static void getOpportunitiesAndOrdersOfUnregisteredPrescriber(){
        User TBM = [SELECT Id FROM User WHERE Profile.Name = 'Prescriber Community TBM User' AND Username = 'tbmuser@hesser.com' LIMIT 1];
        Account prescriberPersonAccount = [SELECT Id FROM Account WHERE RecordType.Name = 'Prescriber' LIMIT 1];
        Account consumerAccount = [SELECT Id FROM Account WHERE Prescribers__c = :prescriberPersonAccount.Id LIMIT 1];
        Account payorAccount = [SELECT Id FROM Account WHERE RecordType.Name = 'Payor' AND Name = 'Test Payor' LIMIT 1];
        List<sObject> patientRecordsList = new List<sObject>();
        List<ClsPrescriberDashboardHelper.OpportunitiesAndOrders> oppsAndOrders;
        List<Territory_Alignment__c> TBMTerritories = [SELECT Id, Name, TBM__r.Name FROM Territory_Alignment__c where TBM__c = :TBM.Id LIMIT 1]; 
        
        // insert opportunities and orders. I'm putting this inside the test context to avoid hitting query limits.
        Test.startTest();
        List<Opportunity> patientOpportunitiesList = TestDataBuilder.getOpportunityList(1, consumerAccount.Id, payorAccount.Id);
        for(Opportunity opp : patientOpportunitiesList){
            opp.Sales_Channel__c = 'Dexcom';
            opp.StageName = '1. New Opportunity';
            opp.Order_NUM__c = String.valueOf(Math.ceil(Math.random() * 1000));
            opp.Prescribers__c = prescriberPersonAccount.Id;
            patientRecordsList.add(opp);
        }
        
        Order_Header__c order = new Order_Header__c(
            Account__c = consumerAccount.Id,
            Status__c = 'SHIPPED',
            Order_ID__c = String.valueOf(Math.ceil(Math.random() * 1000)),
            Booked_Date__c = Date.today().addDays(-5)
        );
        patientRecordsList.add(order);
        insert patientRecordsList;
        
        // get consumer records related to the prescriber
        System.runAs(TBM){
            oppsAndOrders = CtrlFieldSalesDashboard.getOpportunitiesAndOrders(TBMTerritories[0].Id, 'past-3-months');
        }
        Test.stopTest();
        
        // verify that prescriber is in correct state and that records where retrieved as expected
        System.assertEquals(1, oppsAndOrders[0].opportunities.size());
        System.assertEquals(1, oppsAndOrders[0].orders.size());
        System.assertEquals(false, oppsAndOrders[0].inviteSent);
        System.assertEquals(false, oppsAndOrders[0].registered);
        System.assertEquals(true, oppsAndOrders[0].sendInvite);
    }
    
    @isTest
    private static void createPrescriberUserTest(){
        List<User> usersBeforeNewUserCreation = [SELECT Id FROM User];
        List<Contact> contactsBeforeNewUserCreation = [SELECT Id FROM Contact];
        User TBM = [SELECT Id FROM User WHERE Profile.Name = 'Prescriber Community TBM User' AND Username = 'tbmuser@hesser.com' LIMIT 1];
        List<Territory_Alignment__c> TBMTerritories = [SELECT Id, Name, TBM__r.Name FROM Territory_Alignment__c where TBM__c = :TBM.Id LIMIT 1]; 
        Account prescriberPersonAccount = [SELECT Id FROM Account WHERE RecordType.Name = 'Prescriber' LIMIT 1];
        
        Test.startTest();
        System.runAs(TBM){
            CtrlFieldSalesDashboard.createPrescriberUser(TBMTerritories[0].Id, prescriberPersonAccount.Id, 'testprescriber@email.com', 'testprescriber@dexcomprescribercommunity.com');
        }
        Test.stopTest();
        
        // assert that the total amount of users and contacts has increased by 1
        System.assertEquals(usersBeforeNewUserCreation.size() + 1, [SELECT Id FROM User].size());
        System.assertEquals(contactsBeforeNewUserCreation.size() + 1, [SELECT Id FROM Contact].size());
        
        List<User> newUserList = [SELECT Id FROM User WHERE Id NOT IN :usersBeforeNewUserCreation];
        // assert the new user is the one created during the test
        System.assert(newUserList.size() == 1);
    }
    
    @isTest
    private static void suggestUsernameTest(){
        //suggestUsername(String personAccountId, String territoryId){
        User TBM = [SELECT Id FROM User WHERE Profile.Name = 'Prescriber Community TBM User' AND Username = 'tbmuser@hesser.com' LIMIT 1];
        List<Territory_Alignment__c> TBMTerritories = [SELECT Id, Name, TBM__r.Name FROM Territory_Alignment__c where TBM__c = :TBM.Id LIMIT 1]; 
        Account prescriberPersonAccount = [SELECT Id FROM Account WHERE RecordType.Name = 'Prescriber' LIMIT 1];
        String username;
        
        Test.startTest();
        System.runAs(TBM){
            username = CtrlFieldSalesDashboard.suggestUsername(prescriberPersonAccount.Id, TBMTerritories[0].Id);
        }
        Test.stopTest();
    }
    
    @isTest
    private static void changeUserInfoTest(){
        User communityOwner = [SELECT Id FROM User WHERE Username = 'CommunityOwner@hesser.com' and isActive = true LIMIT 1];
        User TBM = [SELECT Id FROM User WHERE Profile.Name = 'Prescriber Community TBM User' AND Username = 'tbmuser@hesser.com' LIMIT 1];
        Account prescriberPersonAccount = [SELECT Id FROM Account WHERE RecordType.Name = 'Prescriber' LIMIT 1];
        Account consumerAccount = [SELECT Id FROM Account WHERE Prescribers__c = :prescriberPersonAccount.Id LIMIT 1];
        Account payorAccount = [SELECT Id FROM Account WHERE RecordType.Name = 'Payor' AND Name = 'Test Payor' LIMIT 1];
        List<sObject> patientRecordsList = new List<sObject>();
        List<ClsPrescriberDashboardHelper.OpportunitiesAndOrders> oppsAndOrders;
        List<Territory_Alignment__c> TBMTerritories = [SELECT Id, Name, TBM__r.Name FROM Territory_Alignment__c where TBM__c = :TBM.Id LIMIT 1]; // set LIMIT so we know the size here is 1
        Account prescriberCommunityAccount = [SELECT Id FROM Account WHERE Name = 'Prescriber Community' LIMIT 1];
        Contact prescriberContact;
        User prescriberUser;
        String emailBeforeChange;
        String usernameBeforeChange = 'TestPrescriber@hesser.com';
        
        // create the prescriber user
        System.runAs(communityOwner){
            prescriberContact = new Contact(
                FirstName = 'Test',
                LastName = 'Contact',
                PersonAccountId__c = prescriberPersonAccount.Id,
                AccountId = prescriberCommunityAccount.Id,
                Email = 'sundog@test.com'
            );
            insert prescriberContact;
            
            prescriberUser = TestDataBuilder.getUser('Prescriber Community User', 'TestPrescriber');
            prescriberUser.ContactId = prescriberContact.Id;
            insert prescriberUser;
        }
        
        // verify username and email address of user before changing their info
        emailBeforeChange = [SELECT Email FROM User WHERE Id = :prescriberUser.Id].Email;
        
        Test.startTest();
        System.runAs(TBM){
            CtrlFieldSalesDashboard.changeUserInfo(prescriberUser.Id, 'newUsername@dexcomprescribercommunity.com', 'newEmail@email.com', TBMTerritories[0].Id, prescriberPersonAccount.Id);
        }
        Test.stopTest();
        
        System.assertNotEquals([SELECT Username FROM User WHERE Id = :prescriberUser.Id].Username, usernameBeforeChange);
        System.assertNotEquals([SELECT Email FROM User WHERE Id = :prescriberUser.Id].Email, emailBeforeChange);
    }
    
    @isTest
    private static void resetUserPasswordTest(){
        User communityOwner = [SELECT Id FROM User WHERE Username = 'CommunityOwner@hesser.com' and isActive = true LIMIT 1];
        User TBM = [SELECT Id FROM User WHERE Profile.Name = 'Prescriber Community TBM User' AND Username = 'tbmuser@hesser.com' LIMIT 1];
        Account prescriberPersonAccount = [SELECT Id FROM Account WHERE RecordType.Name = 'Prescriber' LIMIT 1];
        Account consumerAccount = [SELECT Id FROM Account WHERE Prescribers__c = :prescriberPersonAccount.Id LIMIT 1];
        Account payorAccount = [SELECT Id FROM Account WHERE RecordType.Name = 'Payor' AND Name = 'Test Payor' LIMIT 1];
        List<sObject> patientRecordsList = new List<sObject>();
        List<ClsPrescriberDashboardHelper.OpportunitiesAndOrders> oppsAndOrders;
        List<Territory_Alignment__c> TBMTerritories = [SELECT Id, Name, TBM__r.Name FROM Territory_Alignment__c where TBM__c = :TBM.Id LIMIT 1]; // set LIMIT so we know the size here is 1
        Account prescriberCommunityAccount = [SELECT Id FROM Account WHERE Name = 'Prescriber Community' LIMIT 1];
        Contact prescriberContact;
        User prescriberUser;
        String emailBeforeChange;
        String usernameBeforeChange = 'TestPrescriber@hesser.com';
        
        // create the prescriber user
        System.runAs(communityOwner){
            prescriberContact = new Contact(
                FirstName = 'Test',
                LastName = 'Contact',
                PersonAccountId__c = prescriberPersonAccount.Id,
                AccountId = prescriberCommunityAccount.Id,
                Email = 'sundog@test.com'
            );
            insert prescriberContact;
            
            prescriberUser = TestDataBuilder.getUser('Prescriber Community User', 'TestPrescriber');
            prescriberUser.ContactId = prescriberContact.Id;
            insert prescriberUser;
        }
        
        // verify username and email address of user before changing their info
        emailBeforeChange = [SELECT Email FROM User WHERE Id = :prescriberUser.Id].Email;
        
        Test.startTest();
        System.runAs(TBM){
            CtrlFieldSalesDashboard.resetPassword(prescriberUser.Id, TBMTerritories[0].Id, prescriberPersonAccount.Id);
        }
        Test.stopTest();
    }
    
}