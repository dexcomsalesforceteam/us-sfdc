/*
 * Helper class created to handle functions
 * from Platform event triggers
 * @Author - LTI
*/
public without sharing class ClsPlatformEvtTriggerHelper {

    /*
     * This function is to log message in Apex Debug Log object
     * to IT CRM Team for failed Account records during
     * ProcessNestPossibleDateForSensorOrder - Platform evt trigger
	*/
    public static void createApexDebugLogRecords(List<Account> skippedAccountList){
        List<Apex_Debug_Log__c> apexDebugLogList = new List<Apex_Debug_Log__c>();
        for(Account skippedRec : skippedAccountList){
            Apex_Debug_Log__c apexdebugLog = new Apex_Debug_Log__c();
            apexdebugLog.Apex_Class__c = 'ProcessNextPossibleDateForSensorOrderTrigger';
            apexdebugLog.Message__c = 'Account Id : '+skippedRec.Id+' with Party Id : '+skippedRec.Party_ID__c+
                ' skipped from ProcessNextPossibleDateForSensorOrder Platform event Trigger';
            apexdebugLog.Method__c ='ProcessNextPossibleDateForSensorOrder.createApexDebugLogRecords';
            apexdebugLog.Type__c = 'Error';
            apexDebugLogList.add(apexdebugLog);
           }
        
        if(!apexDebugLogList.isEmpty()){
            try{
                insert apexDebugLogList;
            }catch(Exception e){//implement logging in debug log
                system.debug('Exception Occrred:::'+e.getMessage());
            }
        }
    }
    
    /*
     * @Description - This method performs the core logic of ProcessNextPossibleDateForSensorOrder platform event 
     * trigger to update the Next_Possible_Date_For_Sensor_Order__c on account based on the rules matrix 
     * it logs error records in the apex debug log by consuming method createApexDebugLogRecords
     * param list size is restricted to 200 by trigger process
     * @return
     * @Author - LTI
	*/
    public static void processToUpdateAccounts(List<Account_Next_Date_For_Sensor_Order_Event__e> batchProcessEventList){
        //Map holds the Consumers which are to be updated with Shipping Date change
        Map<String, Account_Next_Date_For_Sensor_Order_Event__e> accountMap = new Map<String, Account_Next_Date_For_Sensor_Order_Event__e>();
        Set<Id> payorIds = new Set<Id>();
        //this holds map of account payor Id, sensor, consumer payor code, total packs to days interval
        Map<String, Integer> accountRuleMatrix = new Map<String, Integer>();
        //List holds the account that are to be updated with the event
        List<Account> accountsToBeUpdated = new List<Account>();
        //skippedAccountsList holds the account record for which the rules matrix mapping was not found
        List<Account> skippedAccountsList = new List<Account>();
        //fetched account list for update - this is for debugging purposes
        List<Account> fetchedAccountListForDebug = new List<Account>();
        
        for(Account_Next_Date_For_Sensor_Order_Event__e newEvent : batchProcessEventList){
            accountMap.put(newEvent.Account_Id__c, newEvent);     
            payorIds.add(newEvent.Payor_Id__c);
        }
        
        //Loop through Consumers, which need to be updated 
        for(Rules_Matrix__c ruleMtx : [SELECT Account__c, Product_Type__c, Quantity_Boxes__c, Duration_In_Days__c, Consumer_Payor_Code__c from Rules_Matrix__c where
                                       Account__c IN : payorIds and Rule_Criteria__c = 'Days For Next Order' and Is_True_or_False__c = true]){
                                           accountRuleMatrix.put(ruleMtx.Account__c + ruleMtx.Product_Type__c + ruleMtx.Quantity_Boxes__c + ruleMtx.Consumer_Payor_Code__c , ruleMtx.Duration_In_Days__c.intValue());
                                       }
        
        System.debug('##accountRuleMatrix>> ' + accountRuleMatrix);
        for(Account accnt :  [SELECT Id, Party_ID__c, Latest_Transmitter_Generation_Shipped__c, Latest_Qty_Of_Sensors_Shipped__c, payor__c,Consumer_Payor_Code__c,
                              Latest_Sensors_Ship_Date__c, Next_Possible_Date_For_Sensor_Order__c 
                              FROM Account WHERE Id IN : accountMap.keySet() FOR UPDATE]){
            System.debug('##accnt>> ' + accnt);
            fetchedAccountListForDebug.add(accnt);
            String mapKey = accnt.payor__c + 'Sensor' + String.valueOf(accnt.Latest_Qty_Of_Sensors_Shipped__c) + accnt.Consumer_Payor_Code__c;
            System.debug('##mapKey>> ' + mapKey);
            if(accountRuleMatrix.containsKey(mapKey)){
                accnt.Next_Possible_Date_For_Sensor_Order__c = accnt.Latest_Sensors_Ship_Date__c.addDays(accountRuleMatrix.get(mapKey));
                accountsToBeUpdated.add(accnt);
            }else{
                //routine to track skipped records - key was not found
                //skipped record track routine
                skippedAccountsList.add(accnt);
            }
        }
        system.debug('skipped account List is::'+skippedAccountsList);
        //if skip record list is not empty then log records in apex debug log object
        if(!skippedAccountsList.isEmpty()){
            createApexDebugLogRecords(skippedAccountsList);
        }
        System.debug('##accountRuleMatrix>> ' + accountRuleMatrix);
        if(!fetchedAccountListForDebug.isEmpty()){
            createApexDebugRecords(fetchedAccountListForDebug,'afterQueryFetch');
        }
        //pass the list to log records before update
        if(!accountsToBeUpdated.isEmpty()){
            createApexDebugRecords(accountsToBeUpdated, 'beforeUpdate');
        }
        
        //Proceed with updates if the list has values
        if(!accountsToBeUpdated.isEmpty()){
            system.debug('accountsToBeUpdated List size::'+accountsToBeUpdated.size());
            try{
                update accountsToBeUpdated;
            }catch(Exception e){
                Apex_Debug_Log__c apexdebugLog = new Apex_Debug_Log__c();
                apexdebugLog.Apex_Class__c = 'ProcessNextPossibleDateForSensorOrderTrigger';
                apexdebugLog.Message__c = 'Update Account List Size : '+accountsToBeUpdated.size()+' Update account failed with reason : '+e.getMessage() + ' Stack trace is: '+e.getStackTraceString();
                apexdebugLog.Method__c ='ProcessNextPossibleDateForSensorOrder.processToUpdateAccounts';
                apexdebugLog.Type__c = 'Error';
                database.insert(apexdebugLog);
            }
        }
    }
    
    /*
     * @Description - This method will insert the debug records for accounts to be processed
	*/
    public static void createApexDebugRecords(List<Account> accountList, String invoked){
        List<Apex_Debug_Log__c> apexDebugLogList = new List<Apex_Debug_Log__c>();
        for(Account accountObj : accountList){
            Apex_Debug_Log__c apexdebugLog = new Apex_Debug_Log__c();
            apexdebugLog.Apex_Class__c = 'ProcessNextPossibleDateForSensorOrderTrigger';
            apexdebugLog.Message__c = 'Account Id : '+accountObj.Id+' with Party Id : '+accountObj.Party_ID__c+
                ' record tracked from '+invoked;
            apexdebugLog.Method__c ='ProcessNextPossibleDateForSensorOrder.createApexDebugLogRecords';
            apexdebugLog.Type__c = 'Information';
            apexDebugLogList.add(apexdebugLog);
        }
        if(!apexDebugLogList.isEmpty()){
            database.insert(apexDebugLogList,false);
        }
    }
    
    /*
     * @Description - This method is created to publish platform event from Account trigger handler
     * this should be ideally a future per documentation
	*/
    public static void publishPlatformEventFromAccountTrigger(List<Account> accountToProcessList){
        system.debug('accountToProcessList:::: inside function::::'+accountToProcessList);
        List<Account_Next_Date_For_Sensor_Order_Event__e> publishEventList = new List<Account_Next_Date_For_Sensor_Order_Event__e>();
        
        for(Account accountObj : accountToProcessList){
            Account_Next_Date_For_Sensor_Order_Event__e accountNextDateForSensorOrder = new Account_Next_Date_For_Sensor_Order_Event__e();
            accountNextDateForSensorOrder.Account_Id__c = accountObj.Id;
            accountNextDateForSensorOrder.Payor_Id__c = accountObj.Payor__c;
            publishEventList.add(accountNextDateForSensorOrder);
        }
        
        Database.SaveResult[] savedResultList = EventBus.publish(publishEventList);
        
        for(integer i=0;i<savedResultList.size();i++){
            if(!savedResultList[i].isSuccess()){
                String errorMsg = savedResultList[i].getErrors().get(0).getFields() + ':' + savedResultList[i].getErrors().get(0).getMessage();
                system.debug('####errorMsg - ' + errorMsg);
                if(errorMsg.Length() >= ClsApexConstants.TWO_HUNDRED_INTEGER)
                    errorMsg = errorMsg.substring(ClsApexConstants.ZERO_INTEGER,ClsApexConstants.TWO_HUNDRED_INTEGER);
                //Write the error log
                new ClsApexDebugLog().createLog(
                    new ClsApexDebugLog.Error(
                        'ClsPlatformEvtTriggerHelper',
                        'publishPlatformEventFromAccountTrigger',
                        ClsApexConstants.EMPTY_STRING,
                        errorMsg
                    )); 
            }
        }
    }
}