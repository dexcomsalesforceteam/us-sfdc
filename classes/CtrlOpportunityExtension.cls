//This class is a controller to the OpportunityBICalculator VF Page. This will just add the accountid value, which will be used in the Lightning component
public class CtrlOpportunityExtension 
{
	public final Opportunity opty;
	public CtrlOpportunityExtension(ApexPages.StandardController stdCtrl)
	{
		stdCtrl.addFields(new List<String>{'AccountId'});
		this.opty = (Opportunity)stdCtrl.getRecord();
	}
}