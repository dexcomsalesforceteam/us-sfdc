/*
@Author        : LTI
@Date Created    : 22 Oct 2018
@Description    : Class to help to make a callout from a button. 
*/
global class ClsPGICallout {    
    
    webService  static string sendCallout(string callOutRequest){
        
        //For(ClsG6UpgradeCalloutPayloadWrapper payload: payloads)  
        //{
        //system.debug('payload  ' + payload);
        String rowNumber;
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        //  req.setEndpoint('https://test-api.mypropago.com/services/v1/company/mailingrequest/create'); 
        req.setEndpoint('callout:G6ReceiverAuth');
        req.setMethod('POST');
        
        system.debug('******Request' + callOutRequest);
        req.setBody(callOutRequest);
        req.setHeader('Content-Type', 'application/json');
        
        
        try {
            System.debug('About to send the request ');
            res = http.send(req);  
            system.debug('******Response' + res.getStatusCode());   
            if (res.getStatusCode() == 200) {
                
                // deserialize the response
                Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
                String rowNumberFromResponse = String.valueOf(m.get('Data')) ;
                Pattern nonNumeric = Pattern.compile('[^0-9]');
                Matcher matcher = nonNumeric.matcher(rowNumberFromResponse);
                rowNumber = matcher.replaceAll('');
                system.debug('Value of the rownumber is ' + rowNumber);    
            }
            return rowNumber;
            
        }
        
        
        catch(System.CalloutException e) {
            System.debug('Callout error: '+ e);
            System.debug(res.toString());
            return rowNumber;
        }
        //}   
    }
}