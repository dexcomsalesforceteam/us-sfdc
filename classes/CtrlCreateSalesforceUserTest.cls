/*
 * Test class to cover PermitSFLicenseController
*/
@isTest(seeAllData = false)
public class CtrlCreateSalesforceUserTest {
    
    @isTest
    public static void getPicklistDataTest(){
        CtrlCreateSalesforceUser.getPicklistData('User', 'User_Location__c');
        test.startTest();
        CtrlCreateSalesforceUser.getPicklistData('Profile', null);
        test.stopTest();
    }
    
    @isTest
    public static void saveUsersTest(){
        List<User> userInsertList = new List<User>();
        User userRec = new User();
        userRec.FirstName = 'TestCreationUser';
        userRec.LastName = 'TestUserLast';
        userRec.Email = 'Test@testcreationuser.com';
        userInsertList.add(userRec);
        test.startTest();
        CtrlCreateSalesforceUser.saveUsers(Json.serialize(userInsertList));
        test.stopTest();
    }
}