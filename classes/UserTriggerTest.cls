@isTest

Private class UserTriggerTest{

  Public static testMethod void Test(){
      
      List<User> userList = new List<User>();
      List<User> listOfUser = new List<User>();

  
      Profile insideSlsMgmtPrfle = [SELECT Id FROM Profile WHERE Name='Inside Sales Mgmt'];
      
      Profile sysAdminPrfle = [SELECT Id FROM Profile WHERE Name='System Administrator'];
      
      User insideSalsUser = new User(Alias = 'tt', Email='InsideSalesMgmt@dexcom.com', Oracle_User_Name__c = 'tt0717',
                                        EmailEncodingKey='UTF-8', FirstName = 'testFirstName',LastName='TestLastName', LanguageLocaleKey='en_US', 
                                        LocaleSidKey='en_US', ProfileId = insideSlsMgmtPrfle.Id, CommunityNickname =  'testInsideSales',
                                        TimeZoneSidKey='America/Los_Angeles', UserName='InsideSalesMgmt@dexcom.com');
                        
      insert insideSalsUser;
     
                      
      User systemAdminUser = new User(Alias = 'sysAdmin', Email='sysAdminUser@dexcom.com', Title = 'testTitle',Oracle_User_Name__c = 'tt0616',
                                        EmailEncodingKey='UTF-8',FirstName = 'testFirstName', LastName='TestLastName', LanguageLocaleKey='en_US', 
                                        LocaleSidKey='en_US', ProfileId = sysAdminPrfle.Id, CommunityNickname =  'testSysAdmin',
                                        TimeZoneSidKey='America/Los_Angeles', UserName='sysAdminUser@dexcom.com');
                                        
      insert systemAdminUser;
     
     system.runAs(insideSalsUser){    
     
     Test.startTest();  
     
     listOfUser = [select id, Title, Email from User where UserName='sysAdminUser@dexcom.com'];
     //system.debug('***' + listOfUser);
          
      for(User usr : listOfUser){
           usr.Title = 'test';
           //usr.Email = 'test@gmail.com';
           userList.add(usr);
       }
       
       
       
      Update userList;
      
      Test.stopTest();      
    }    
 
   }

}