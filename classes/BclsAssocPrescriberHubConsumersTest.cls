/*
 * @Description - Test class for BclsAssocPrescriberHubConsumers
*/
@istest(seeAllData = false)
public class BclsAssocPrescriberHubConsumersTest {
    
    
    @istest
    public static void testBatch(){
        //Insert test Account
        id consumerRecId = ClsApexUtility.getRecordTypeIdByDeveloperName('Account', 'Consumers');
        id prescriberRecId = ClsApexUtility.getRecordTypeIdByDeveloperName('Account', 'Prescriber');
        Account consumer = TestDataBuilder.getAccountListConsumer(1, consumerRecId)[0];
        consumer.Default_Price_Book__c = Test.getStandardPricebookID();
        consumer.BillingState = 'AZ';
        consumer.BillingCountry = 'US';
        consumer.ShippingCountry = 'US';
        consumer.System_Of_Origin__c = 'gpo';
        consumer.System_Of_Origin_Id__c = '12434554';
        consumer.Unique_Integration_Id__c = 'gpo-12434554';
        consumer.Source_Prescriber_NPI__c = '987766';
        test.startTest();
        Account prescriberAccount = TestDataBuilder.getAccountListConsumer(1,prescriberRecId)[0];
        prescriberAccount.NPI_Number__c = '987766';
        
        insert new list <Account>{consumer, prescriberAccount};
            
        BclsAssocPrescriberHubConsumersSched sh1 = new BclsAssocPrescriberHubConsumersSched();
        String sch = '0 59 23 * * ?'; 
        system.schedule('Test Territory Check'+String.valueOf(system.now()), sch, sh1);
        test.stopTest();
    }
}