/*
 * This test class is created to cover CtrlAccountEligibilityCountdown
 * @Author - LTI
*/
@isTest(seeAllData = false)
public class CtrlAccountEligibilityCountdownTest {

    @testSetup
    public static void setUpTestData(){
        List<Account> accountInsertList = new List<Account>();
        //prescriber
        Account prescriberAccount=new Account();
        prescriberAccount.lastname='prescriber Account';
        prescriberAccount.recordtypeId= ClsApexUtility.getRecordTypeIdByDeveloperName('Account', 'Prescriber');
        prescriberAccount.Inactive__c =false;        
        accountInsertList.add(prescriberAccount);
        
        //Payor
        Account payorAccount = new Account();
        payorAccount.recordtypeId= ClsApexUtility.getRecordTypeIdByDeveloperName('Account', 'Payor');
        payorAccount.Name = 'Payor';
        payorAccount.Skip_Payor_Matrix__c = true;
        accountInsertList.add(payorAccount);
        Database.insert(accountInsertList);              
        
        //patient
        Account testAccount=TestDataBuilder.testAccount();
        testAccount.Default_Price_Book__c=Test.getStandardPricebookId();
        testAccount.PersonEmail = 'Test@gmail.comEligibilityCountDown';
        testAccount.BillingStreet='abc';
        testAccount.BillingCity ='Bang';
        testAccount.BillingState ='KN';
        testAccount.BillingPostalCode='560037';
        testAccount.ShippingStreet  ='Bang';
        testAccount.ShippingCity='Bang';
        testAccount.ShippingState='KN';
        testAccount.ShippingPostalCode='560037';
        testAccount.Party_ID__c ='123344';
        testAccount.Prescribers__c = prescriberAccount.Id;
        testAccount.Payor__c = payorAccount.Id;
        testAccount.Receiver_Quantity_Prescribed__c = 10;
        testAccount.Sensor_Quantity_Prescribed__c = 10;
        testAccount.Transmitter_Quantity_Prescribed__c = 10;
        testAccount.RecordTypeId = ClsApexUtility.getRecordTypeIdByDeveloperName('Account', 'Consumers');
        insert testAccount;
        
        List<Order_Header__c> orderHeaderList = new List<Order_Header__c>();
        Order_Header__c orderHeader=new Order_Header__c();
        orderHeader.Account__c =testAccount.id;
        orderHeader.Order_Type__c = 'Standard Sales Order';
        orderHeaderList.add(orderHeader);
        
        Order_Header__c orderHeader1 = new Order_Header__c();
        orderHeader1.Account__c = testAccount.id;
        orderHeader1.Order_Type__c = 'MC Standard Sales Order';
        orderHeaderList.add(orderHeader1);
        
        Order_Header__c orderHeader2=new Order_Header__c();
        orderHeader2.Account__c =testAccount.id;
        orderHeader2.Order_Type__c = 'MC Standard Sales Order';
        orderHeaderList.add(orderHeader2);
        database.insert(orderHeaderList);
        
        List<Order_Item_Detail__c> orderItemDetail = new List<Order_Item_Detail__c>();
        Order_Item_Detail__c oid1  = new Order_Item_Detail__c(Item_Number__c = 'BUN-GF-003',
                                                              Generation__c='G6', 
                                                              Order_Header__c = orderHeader.Id,
                                                              Quantity__c =1,
                                                              Quantity_Shipped__c =1,
                                                              Shipping_Date__c =system.today());//transmitter
        orderItemDetail.add(oid1);
        Order_Item_Detail__c oid2  = new Order_Item_Detail__c(Item_Number__c = 'STR-MC-001',
                                                              Generation__c='G6', 
                                                              Order_Header__c = orderHeader1.Id,
                                                              Quantity__c =1,
                                                              Quantity_Shipped__c =1,
                                                              Shipping_Date__c =system.today());//receiver
        orderItemDetail.add(oid2);
        Order_Item_Detail__c oid3  = new Order_Item_Detail__c(Item_Number__c = 'STS-OR-003',
                                                              Generation__c='G6',
                                                              Order_Header__c = orderHeader2.Id,
                                                              Quantity__c =1,
                                                              Quantity_Shipped__c =1,
                                                              Shipping_Date__c =system.today());//sensor
        orderItemDetail.add(oid3);
        Order_Item_Detail__c oid4  = new Order_Item_Detail__c(Item_Number__c = '301937314218',
                                                              Generation__c='G6', 
                                                              Order_Header__c = orderHeader2.Id,
                                                              Quantity__c =1,
                                                              Quantity_Shipped__c =1,
                                                              Shipping_Date__c =system.today()-185);//solution
        orderItemDetail.add(oid4);
        Order_Item_Detail__c oid5  = new Order_Item_Detail__c(Item_Number__c = '301937818011',
                                                              Generation__c='G6', 
                                                              Order_Header__c = orderHeader2.Id,
                                                              Quantity__c =1,
                                                              Quantity_Shipped__c =1,
                                                              Shipping_Date__c =system.today()-185);//glucometer
        orderItemDetail.add(oid5);
        database.insert(orderItemDetail); 
        
        testAccount.Latest_Insurance_Transmitter_Order__c = orderHeader.Id;
        testAccount.Latest_Insurance_Receiver_Order__c = orderHeader1.Id;
        testAccount.Latest_Insurance_Sensor_Order__c = orderHeader2.Id;
        update testAccount;
        
        OpportunityTriggerHandler.SKIP_PRESCRIBERS_VALIDATION = true;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test opp';
        opp.AccountId = testAccount.Id;
        opp.StageName = 'Prospecting';
        opp.CloseDate = System.today();
        opp.Prescribers__c = prescriberAccount.Id;
        insert opp;
        
    }
    
    @isTest
    public static void getAccountEligibilityCountDownDetailsTest(){
        Account account = [Select Id,Consumer_Payor_Code__c from Account where PersonEmail = 'Test@gmail.comEligibilityCountDown' limit 1];
        System.debug('Payor Code>>'+account.Consumer_Payor_Code__c);
        
        Account accountPayor = [Select Id from Account where Name = 'Payor' limit 1];
        
        List<Rules_Matrix__c> matrixList = new List<Rules_Matrix__c>();
        
        Rules_Matrix__c ruleMatrix = new Rules_Matrix__c();
        ruleMatrix.Account__c = accountPayor.Id;
        ruleMatrix.Quantity_Boxes__c = 1;
        ruleMatrix.Duration_In_Days__c = 280;
        ruleMatrix.Consumer_Payor_Code__c = 'A';
        ruleMatrix.Product_Type__c = 'Receiver';
        matrixList.add(ruleMatrix);
        
        Rules_Matrix__c ruleMatrix1 = new Rules_Matrix__c();
        ruleMatrix1.Account__c = accountPayor.Id;
        ruleMatrix1.Quantity_Boxes__c = 1;
        ruleMatrix1.Duration_In_Days__c = 280;
        ruleMatrix1.Consumer_Payor_Code__c = 'A';
        ruleMatrix1.Product_Type__c = 'Sensor';
        matrixList.add(ruleMatrix1);
        
        Rules_Matrix__c ruleMatrix2 = new Rules_Matrix__c();
        ruleMatrix2.Account__c = accountPayor.Id;
        ruleMatrix2.Quantity_Boxes__c = 1;
        ruleMatrix2.Duration_In_Days__c = 280;
        ruleMatrix2.Consumer_Payor_Code__c = 'A';
        ruleMatrix2.Product_Type__c = 'Transmitter';
        matrixList.add(ruleMatrix2);
        
        insert matrixList;
        
        Test.startTest();
        	CtrlAccountEligibilityCountdown.getAccountEligibilityCountDownDetails(String.valueOf(account.Id));
        	String id = CtrlAccountEligibilityCountdown.getAccountId([Select Id from Opportunity Limit 1].Id);
        Test.stopTest();
        
        System.assertEquals(account.Id, id);
    }
}