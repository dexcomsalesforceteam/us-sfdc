global class SclsDocCollectionUpdate implements Schedulable {
   global void execute(SchedulableContext sc) {
      BclsDocCollectionUpdateBatch  bCls = new BclsDocCollectionUpdateBatch (); 
      database.executebatch(bCls);
   }
}