/*
 * @Description : Class Created to send out email 
 * from HCP creation form
 * @Author : Tanay Kulkarni LTI
 * @Created Date : 9 Aug 2019
*/
public with sharing class ClsHcpFormCreation {

    /*
     * @Description: method added for 4405
     * @Param - String
     * This method sends out notification
     * Invoked by HCP creation form on prescriber account
	*/
    @AuraEnabled
    public static void sendEmail(List<HcpCreationWrapper> paramWrapList){        
        String mailBody = '<html><body><p> <b>HCP Creation Form:</b> <br>';
        for(HcpCreationWrapper creationWrap : paramWrapList){
            
            if('hcpTypeSelected'.equalsIgnoreCase(creationWrap.variableKey))
                mailBody+='<b>Requested Type: </b>'+creationWrap.variableValue+'   <br>';
            if('partyId'.equalsIgnoreCase(creationWrap.variableKey) && String.isNotBlank(creationWrap.variableValue))
                mailBody+='<b>Party Id: </b>'+creationWrap.variableValue+'   <br>';
            if('firstName'.equalsIgnoreCase(creationWrap.variableKey))
                mailBody+='<b>First Name: </b>'+creationWrap.variableValue+'   ';
            if('lastName'.equalsIgnoreCase(creationWrap.variableKey))
                mailBody+='<b>Last Name: </b>'+creationWrap.variableValue+'<br>';
            if('address'.equalsIgnoreCase(creationWrap.variableKey))
                mailBody+='<b>Address(Physical address-No PO): </b>'+creationWrap.variableValue+'<br>';
            if('city'.equalsIgnoreCase(creationWrap.variableKey))
                mailBody+='<b>City: </b>'+creationWrap.variableValue+'   ';
            if('state'.equalsIgnoreCase(creationWrap.variableKey))
                mailBody+='<b>State: </b>'+creationWrap.variableValue+'   ';
            if('zip'.equalsIgnoreCase(creationWrap.variableKey))
                mailBody+='<b>Zip: </b>'+creationWrap.variableValue+'<br>';
            if('npi'.equalsIgnoreCase(creationWrap.variableKey))
            	mailBody+='<b>NPI#(Required-insert 1234567891 if no NPI is available): </b>'+creationWrap.variableValue+'<br>';
            if('phone'.equalsIgnoreCase(creationWrap.variableKey))
                mailBody+='<b>Phone: </b>'+creationWrap.variableValue+'   ';
            if('fax'.equalsIgnoreCase(creationWrap.variableKey))
                mailBody+='<b>Fax: </b>'+creationWrap.variableValue+'   <br>';
             if('accountNumber'.equalsIgnoreCase(creationWrap.variableKey))
                mailBody+='<b>Account Number: </b>'+creationWrap.variableValue+'   <br>';
            
        }
        mailBody+='</p></body></html>';
        system.debug('mailBody:::'+mailBody);
        Organization org = [SELECT Id, IsSandbox FROM Organization limit 1];
        List<String> addressEmailList = new List<String>();
        if(org.IsSandbox || Test.isRunningTest()){ //Test is running is implemented so that we don't send out notifications to prod users from else block
            if(String.isNotBlank(system.label.HCP_Creation_To_Email_Sandbox)){
                for(String emailValue : system.label.HCP_Creation_To_Email_Sandbox.split(',')){
                    if(String.isNotBlank(emailValue)){
                        addressEmailList.add(emailValue);   
                    }
                }
            }
        }else{
            if(String.isNotBlank(system.label.HCP_Creation_To_Email_Production)){
                for(String emailValue : system.label.HCP_Creation_To_Email_Production.split(',')){
                    if(String.isNotBlank(emailValue)){
                        addressEmailList.add(emailValue);
                    }
                }
            }
        }
        
        string userEmail = [Select Email From User where id =:UserInfo.getUserId() limit 1].Email; 

        system.debug('addressEmailList:::::'+addressEmailList);
        if(!addressEmailList.isEmpty()){
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.setToAddresses(addressEmailList);
            message.setSubject('SalesOps Verification');
            message.setCCAddresses( new String[]{userEmail});
            message.setHtmlBody(mailBody);
            message.setReplyTo(UserInfo.getUserEmail());
            
            Messaging.SendEmailResult[] results = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { message });
            
            if (results[0].success) {
                System.debug('The email was sent successfully.');
            } else {
                System.debug('The email failed to send: '
                             + results[0].errors[0].message);
            }
        }
    }
    
    /*
     * @Description - Wrapper class to receive inputs from
     * Component
	*/
    public without sharing class HcpCreationWrapper{
        @AuraEnabled
        public String variableKey {get; set;}
        
        @AuraEnabled
        public String variableValue {get; set;}
    }
}