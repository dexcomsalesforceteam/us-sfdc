/**
 * AccountOptInScheduled
 * Scheduled method to kick off call to get opt in status from marketing cloud
 * @author Katie Wilson(Sundog)
 * @date 10/15/2018
 */
global class SclsAccountOptInScheduled implements Schedulable {
    global void execute(SchedulableContext sc) {
        //This is here because the tests were hitting an issue with not being able to access the mock class
        if(!Test.isRunningTest()){
            ClsMarketingCloudHelper.futureupdateOptInValues(); 
        }
    }
}