/*--------------------------------------------------------------------------------------------------
@Author      - LTI
@Date        - 07/24/2019
@Description - Batch class updates SSIP Schedule records needs to be processed as well as sends Benefit to Change Health care.
@Condition 
@refactored - 11/04/2019

--------------------------------------------------------------------------------------------------*/
global class BclsProcessSSIPSchedules implements Database.Batchable<sObject>
{   
    Set<Id> accountIdSet = new Set<Id>();    //Set holds all the account Id to process    
    Set<Id> opptyIdsToClose = new Set<Id>();    //Set holds the list of Opportunity Ids, which should be closed    
    Set<Id> cashPriceListSSIPSchedules = new Set<Id>();     //Set of Schedule Ids with Cash Price List    
    Set<Id> updateScheduleId = New Set<Id>();   //Set of SSIP Schedule needs to be updated.    
    Set<Id> scheduleIdKeys = New Set<Id>(); //Set<Id> Schedule Ids on which Opty Created    
    
    Map<Id, Benefits__c> schedAccountIdToPrimaryBenefitMap = new Map<Id, Benefits__c>();   //Map holds the Account Id to Primary Benfit     
    Map<Id, Id> scheduleToOptyMap = new Map<Id, Id>();  //Map holds the Schedule Id to Opportunity Id    
    Map<Id, SSIP_Schedule__c> scheduleMap = new Map<Id, SSIP_Schedule__c>();    //Map holds Schedule Id <> Schedule record for update    
    Map<Id, Id> biFailureScheduleToOptyMap = new Map<Id, Id>(); //Map holds Schedule Id <> Opty Id { Benefit verification Failed }    
    Map<Id, Decimal>  ssipScheduleOrderAmountMap = new Map<Id, Decimal> (); //Map holds SSIP Schedule and Total Order Amount details    
    
    List<Benefits__c> allBenefitsForSchedules = new List<Benefits__c>();    //List of  Benefit records to be updated
    List<Benefits__c> benefitsToBeVerified = new List<Benefits__c>();   //List of  Benefit records to be verified    
    List<SSIP_Schedule__c> opptyForOpenSchedulesList = new List<SSIP_Schedule__c>();    //List holds the schedules for which Oppty needs to be created     
    List<SSIP_Schedule__c> opptyForBenefitQuadaxIdList = new List<SSIP_Schedule__c>();  //List holds the schedules on which Opty needs to be created for QUADAX PARTNER ID    
    
    List<SSIP_Schedule__c> updateSSIPSchedule = new List<SSIP_Schedule__c>();   //List of SSIP schedules needs to be updated with Open Status    
    List<SSIP_Schedule__c> calculateAmountSSIPSchedule = new List<SSIP_Schedule__c>();  //List of SSIP schedules needs to be updated with Total Order Amount    
    List<ClsSSIPApexUtility.ClsOpptyTaskWrapper> opptyTaskWrapperList = new List<ClsSSIPApexUtility.ClsOpptyTaskWrapper>(); //List of ClsOpptyTaskWrapper to process records for oppty and task creation    
    
    String taskDescription = ClsApexConstants.EMPTY_STRING; //String holds the task Description to be passed to task generation method    
    
   //Start Method
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
       system.debug('************** Called BclsProcessSSIPSchedules');
        Date today = Date.today();
        String query = 'SELECT Id,Name, Price_Book__r.Cash_Price_Book__c,Account__c,Status__c,Shipping_Address__c, Shipping_Method__c,Total_Amount__c,';
        query += 'Process_Schedule__c, Scheduled_Shipment_Date__c,Shipping_Address_Full_Text__c,Opportunity__c,Eligibility_Check_Date__c,';
        query += 'Is_BI_Active__c,Is_CMN_Active__c,Is_Credit_Card_Active__c,Is_Pre_Authorized__c,Payor__c,Opportunity__r.isClosed,';
        query += 'Price_Book__c, Product__c,SSIP_Rule__r.Product_Type__c,';
        query += 'Account__r.Primary_Benefit__c,Account__r.Is_BI_Active__c, Account__r.Name,Account__r.Territory_Code__c,';
        query += 'Account__r.CMN_or_Rx_Expiration_Date__c,Account__r.Credit_Card_Expiration_Date__c ';
        query += 'FROM SSIP_Schedule__c ';
        query += 'WHERE ';
        //query += '(Account__r.Last_Standard_Sales_Order_Ship_Date__c != null AND Account__r.Last_Standard_Sales_Order_Ship_Date__c = LAST_N_DAYS:180) AND ';
        query += '(((Eligibility_Check_Date__c =: today OR Force_Eligibility_Check__c = true) AND Status__c  = \'Open\') OR ';
        query += '(Status__c  =  \'Eligibility In Review\') OR ';
        query += 'Process_Schedule__c = true ';
        query += ')'; 
        
        return Database.getQueryLocator(query);
    }

    //Execute Method
    global void execute(Database.BatchableContext BC, List<SSIP_Schedule__c> ssipSchedules)
    {
        system.debug('Query' +ssipSchedules);//Group the 'Open' vs 'Eligibility In Review' Schedules for further processing
        for(SSIP_Schedule__c ssipSch :ssipSchedules)
        {
            if(ssipSch.Price_Book__r.Cash_Price_Book__c) cashPriceListSSIPSchedules.add(ssipSch.Id);
            
            if(ssipSch.Status__c == 'Open')
            {
                System.debug('Open is executed');
                ssipSch.Status__c = 'Eligibility In Review';
                updateScheduleId.add(ssipSch.Id);
                System.debug('***********Adding Schedule :: ' + ssipSch.Id);
                accountIdSet.add(ssipSch.Account__c);
                
                //calculate Total Order Amount, if not Cash Order
                if(!ssipSch.Price_Book__r.Cash_Price_Book__c)
                    calculateAmountSSIPSchedule.add(ssipSch);               
            }
            else
            {
                System.debug('Eligibility In Review is executed');
                accountIdSet.add(ssipSch.Account__c);
                if(!ssipSch.Price_Book__r.Cash_Price_Book__c)
                    ssipScheduleOrderAmountMap.put(ssipSch.Id, ssipSch.Total_Amount__c); 
            } 
            //Create Map of all schedules
            scheduleMap.put(ssipSch.Id, ssipSch);
        }
        system.debug('** cashPriceListSSIPSchedules **' + cashPriceListSSIPSchedules);
        
        //calculate total Order Amount
        Map<Id, Decimal> totalOrderAmountMap = new Map<Id, Decimal>();
        if(!calculateAmountSSIPSchedule.isEmpty()){ 
            totalOrderAmountMap = ClsSSIPApexUtility.calculateTotalOrderAmount(calculateAmountSSIPSchedule);
        }
        //merge Maps for Order Amounts
        if(!totalOrderAmountMap.isEmpty()){
            for(Id thisId : totalOrderAmountMap.keySet())
                ssipScheduleOrderAmountMap.put(thisId, totalOrderAmountMap.get(thisId)); 
        }
        system.debug('** ssipScheduleOrderAmountMap **' + ssipScheduleOrderAmountMap);
        
        // Get all Primary Benefits tied to the Account, which are part of the Open Schedules 
        allBenefitsForSchedules = [Select Id, Account__c, BI_Status__c, Last_Benefits_Check_Date__c, Quadax_Trading_Partner_Id__c,
                                    TRANSMITTER_AUTH_END_DATE__c, SENSOR_AUTH_END_DATE__c, PRIOR_AUTH_REQUIRED__c, 
                                    BI_Request_Invoked_From__c, Invoke_BI_Verification__c,
                                    Coverage__c, INDIVIDUAL_DEDUCTIBLE__c, INDIVIDUAL_MET__c, Individual_Deductible_Remaining__c, 
                                    INDIVIDUAL_OOP_MAX__c, INDIVIDUAL_OOP_MET__c, Individual_OOP_Remaining__c, 
                                    FAMILY_DEDUCT__c, Family_Met__c, Family_Deductible_Remaining__c, 
                                    FAMILY_OOP_MAX__c, FAMILY_OOP_MET__c, Family_OOP_Remaining__c 
                                    FROM Benefits__c WHERE Benefit_Hierarchy__c = 'Primary'  AND Account__c IN: accountIdSet];  
        system.debug('All Benefits' +allBenefitsForSchedules);
                
        //map Account Id <> Benefit Map
        for(Benefits__c thisBenefit : allBenefitsForSchedules){
            schedAccountIdToPrimaryBenefitMap.put(thisBenefit.Account__c, thisBenefit);
        }
        
        
            for(SSIP_Schedule__c ssipSchUpdate : ssipSchedules){
                /************* Update Order Amount ***************/
                if(!ssipScheduleOrderAmountMap.isEmpty() && ssipScheduleOrderAmountMap.containsKey(ssipSchUpdate.Id)
                     && ssipSchUpdate.Total_Amount__c != ssipScheduleOrderAmountMap.get(ssipSchUpdate.Id)){
                    ssipSchUpdate.Total_Amount__c = ssipScheduleOrderAmountMap.get(ssipSchUpdate.Id);
                }
                
                /************* BYPASS CASH ORDERS ****************/
                    if(cashPriceListSSIPSchedules.contains(ssipSchUpdate.Id)){
                    
                        ssipSchUpdate.Is_BI_Active__c = true;   // BI Check will be true for cash orders
                        ssipSchUpdate.Is_Pre_Authorized__c = true; // pre auth Check will be true for cash orders
                        ssipSchUpdate.Is_Credit_Card_Active__c = true; // Credit Card check will be true for cash orders
                    }
                /************* BYPASS CASH ORDERS ****************/
                
                /************* START Benefit Verification ****************/
                    if(!cashPriceListSSIPSchedules.contains(ssipSchUpdate.Id) && 
                        !schedAccountIdToPrimaryBenefitMap.isEmpty() &&
                        schedAccountIdToPrimaryBenefitMap.containsKey(ssipSchUpdate.Account__c)){
                            Benefits__c vBenefit = new Benefits__c();
                            vBenefit = schedAccountIdToPrimaryBenefitMap.get(ssipSchUpdate.Account__c);
                            //if benefit verified in last 30 days
                            if(vBenefit != null && vBenefit.Last_Benefits_Check_Date__c != null && 
                                    ssipSchUpdate.Scheduled_Shipment_Date__c != null && 
                                    vBenefit.Last_Benefits_Check_Date__c.daysBetween(ssipSchUpdate.Scheduled_Shipment_Date__c) <= 30 ){
                                        ssipSchUpdate.Is_BI_Active__c = true;                                        
                                    }else{
                                        if(!String.isBlank(vBenefit.Quadax_Trading_Partner_Id__c)){ //QUADAX Payors
                                            //invoke BI Verification
                                            benefitsToBeVerified.add(vBenefit); 
                                            System.debug('**BI Verification :: ' +ssipSchUpdate.Id);
                                        }else{ 
                                            ssipSchUpdate.Is_BI_Active__c = false;
                                            opptyForBenefitQuadaxIdList.add(ssipSchUpdate); //Create Opty
                                        }
                                    }                                                            
                    }
                /************* END Benefit Verification ****************/
        
                
                /************* START PRE AUTH CHECK ****************/
                    if(!ssipSchUpdate.Is_Pre_Authorized__c && 
                        !cashPriceListSSIPSchedules.contains(ssipSchUpdate.Id) && 
                        !schedAccountIdToPrimaryBenefitMap.isEmpty() &&
                        schedAccountIdToPrimaryBenefitMap.containsKey(ssipSchUpdate.Account__c)){
                        
                        String priorAuthValue = schedAccountIdToPrimaryBenefitMap.get(ssipSchUpdate.Account__c).PRIOR_AUTH_REQUIRED__c;
                        
                        if(priorAuthValue == 'N' || String.isBlank(priorAuthValue)){
                            ssipSchUpdate.Is_Pre_Authorized__c = true;
                        }else if(priorAuthValue == 'Y'){
                                System.debug('**priorAuthValue :: ' + priorAuthValue);
                                ssipSchUpdate.Is_Pre_Authorized__c = ClsSSIPApexUtility.checkPriorAuth(ssipSchUpdate.SSIP_Rule__r.Product_Type__c, 
                                                                                                        ssipSchUpdate.Scheduled_Shipment_Date__c, 
                                                                                                        schedAccountIdToPrimaryBenefitMap.get(ssipSchUpdate.Account__c)); 
                        }   
                    }
                /************* END PRE AUTH CHECK ****************/
                
                /************* START CMN Verification ****************/     
                    if(!ssipSchUpdate.Is_CMN_Active__c)
                        ssipSchUpdate.Is_CMN_Active__c = ClsSSIPApexUtility.checkCMNActive(ssipSchUpdate.Account__r.CMN_or_Rx_Expiration_Date__c, 
                                                                                            ssipSchUpdate.Scheduled_Shipment_Date__c);
                
                /************* END CMN Verification ****************/
                
                /************* START CREDIT CARD Verification ****************/
                    if(ssipSchUpdate.Is_BI_Active__c && !ssipSchUpdate.Is_Credit_Card_Active__c && !cashPriceListSSIPSchedules.contains(ssipSchUpdate.Id) &&
                        !schedAccountIdToPrimaryBenefitMap.isEmpty() && schedAccountIdToPrimaryBenefitMap.containsKey(ssipSchUpdate.Account__c)){
                            Benefits__c benefit = new Benefits__c();
                            benefit = schedAccountIdToPrimaryBenefitMap.get(ssipSchUpdate.Account__c);
                        System.debug('Calculate CoPay Benefit:: ' + benefit);
                            
                        System.debug('orderAmount :: ' + ssipSchUpdate.Total_Amount__c);                      
                        
                        ssipSchUpdate.Is_Credit_Card_Active__c = ClsSSIPApexUtility.validateCreditCardCheck(ssipSchUpdate.Total_Amount__c, 
                                                                                                            ssipSchUpdate.Account__r.Credit_Card_Expiration_Date__c, 
                                                                                                            ssipSchUpdate.Scheduled_Shipment_Date__c, benefit);
                    }
                    System.debug('Schedule Id : ' + ssipSchUpdate.Id + ' : Is_Credit_Card_Active__c :: ' + ssipSchUpdate.Is_Credit_Card_Active__c );
                /************* END CREDIT CARD Verification ****************/
                
                /************* START MARK FOR OPTY CREATION ****************/
                if( ssipSchUpdate.Is_CMN_Active__c == false || 
                   ssipSchUpdate.Is_Pre_Authorized__c == false || 
                   ( ssipSchUpdate.Is_BI_Active__c == true && ssipSchUpdate.Is_Credit_Card_Active__c == false )){
                       
                       opptyForOpenSchedulesList.add(ssipSchUpdate);
                       taskDescription = 'CMN Active '+ssipSchUpdate.Is_CMN_Active__c+
                           'Is Pre Authorized '+ssipSchUpdate.Is_Pre_Authorized__c+
                           'Is Credit Card Active '+ssipSchUpdate.Is_Credit_Card_Active__c;
                   }
                /************* END MARK FOR OPTY CREATION ****************/ 
                 
                /************* START ELIGIBLITY VERIFIED ****************/  
                if(ssipSchUpdate.Is_BI_Active__c && ssipSchUpdate.Is_CMN_Active__c && 
                    ssipSchUpdate.Is_Pre_Authorized__c && ssipSchUpdate.Is_Credit_Card_Active__c ){
                    
                    ssipSchUpdate.Status__c = 'Eligibility Verified'; 
                    if(ssipSchUpdate.Opportunity__c != null){
                        opptyIdsToClose.add(ssipSchUpdate.Opportunity__c);
                       // ssipSchUpdate.Shipping_Method__c = '000001_FEDEX_A_1DA'; // As per Amol, needed only when Opty is closed after schedule ship date
                       // reviewSsipSchNeedstobeUpdated.add(ssipSchUpdate);
                    }
                }
                /************* END ELIGIBLITY VERIFIED ****************/
                updateScheduleId.add(ssipSchUpdate.Id);
                System.debug('***********Adding Schedule1 :: ' + ssipSchUpdate);                 
            }           
        
        
        /************* START BI VERIFICATION ****************/
            if(!benefitsToBeVerified.isEmpty()){
                ID jobID = System.enqueueJob(new ClsInvokeBIVerificationQueueable(benefitsToBeVerified,'SSIP')); //Added Invoke form parameter for #CRMSF-4614
                System.debug('*** Queueable invoked + Job Id :: ' + jobID );
            }
               // ClsSSIPApexUtility.processBIVerification(benefitsToBeVerified);
            
        /************* END BI VERIFICATION ****************/
        
        /************* START OPTY CREATION ****************/
        //QUADAX Partner ID not present
        if(!opptyForBenefitQuadaxIdList.isEmpty()){
                        
            taskDescription = ClsApexConstants.SSIP_TASK_DESC_FOR_QUADAX_FAIL;
            opptyTaskWrapperList = ClsSSIPApexUtility.getOpptyTaskWrapperList(opptyForBenefitQuadaxIdList, 
                                                                                ClsApexConstants.SSIP_TASK_SUBJECT_FOR_QUADAX_FAIL,
                                                                                    null,
                                                                                        taskDescription);//quadax Id is not present, oppty Id is passed null
            biFailureScheduleToOptyMap = ClsSSIPApexUtility.createOpportunityforSSIPSch(opptyTaskWrapperList);
        }
        //When eligibility verification failed
        if(!opptyForOpenSchedulesList.isEmpty()){
            
            opptyTaskWrapperList.clear();  
            taskDescription = '';          
            opptyTaskWrapperList = ClsSSIPApexUtility.getOpptyTaskWrapperList(opptyForOpenSchedulesList,
                                                                                ClsApexConstants.SSIP_TASK_SUBJECT_FOR_ELIGIBILITY_FAIL ,
                                                                                    null,
                                                                                        taskDescription);//quadaxId is present, oppty Id is passed null
            scheduleToOptyMap = ClsSSIPApexUtility.createOpportunityforSSIPSch(opptyTaskWrapperList);
        }
        //combine Opty Created Maps
        scheduleIdKeys.addAll(biFailureScheduleToOptyMap.KeySet());
        scheduleIdKeys.addAll(scheduleToOptyMap.KeySet());
        
        for(SSIP_Schedule__c ssipSchUpdate : ssipSchedules){
            for(Id scheduleId : scheduleIdKeys){
                if(!biFailureScheduleToOptyMap.isEmpty() && biFailureScheduleToOptyMap.containskey(ssipSchUpdate.Id)){
                    ssipSchUpdate.Opportunity__c = biFailureScheduleToOptyMap.get(ssipSchUpdate.id);
                    
                      updateScheduleId.add(ssipSchUpdate.Id); //update this record in next steps.
                      System.debug('***********Adding Schedule2:: ' + ssipSchUpdate.Id);
                    
                }else if(!scheduleToOptyMap.isEmpty() && scheduleToOptyMap.containskey(ssipSchUpdate.Id)){
                    ssipSchUpdate.Opportunity__c = scheduleToOptyMap.get(ssipSchUpdate.id);
                    
                      updateScheduleId.add(ssipSchUpdate.Id); //update this record in next steps.
                      System.debug('***********Adding Schedule3 :: ' + ssipSchUpdate.Id);
                      
                }
            }            
        }
        /************* END OPTY CREATION ****************/
        
        /************* START OPTY CLOSURE ****************/ 
            if(!opptyIdsToClose.isEmpty())
            {
                //Send the Opportunities for closure
                system.debug('Order Closure is Here' +opptyIdsToClose);
                ClsSSIPApexUtility.closeOpportunityforSSIPSch(opptyIdsToClose);
            }
        /************* END OPTY CLOSURE ****************/ 
        
        /************* START SCHEDULE UPDATE ****************/
            try{
                //get Schedules to Update
                if(!updateScheduleId.isEmpty()){
                    for(Id thisScheduleId : updateScheduleId){
                        updateSSIPSchedule.add(scheduleMap.get(thisScheduleId));
                    }
                }
                if(!updateSSIPSchedule.isEmpty())
                    Database.update(updateSSIPSchedule, false); 
            }
            catch (DmlException de) {
                Integer numErrors = de.getNumDml();
                System.debug('getNumDml=' + numErrors);
                for(Integer i=0;i<numErrors;i++) {
                    System.debug('getDmlFieldNames=' + de.getDmlFieldNames(i));
                    System.debug('getDmlMessage=' + de.getDmlMessage(i));
                }
            } 
        
        /************* END SCHEDULE UPDATE ****************/
    
    }
    global void finish(Database.BatchableContext BC)
    {
        //Invoke the Order creation process
        Database.executeBatch(new BclsSSIPOrderCreationBatch('BclsProcessSSIPSchedules'),2);
        //Send an email to the User after your batch completes
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       // String[] toAddresses = new String[] {'itcrmsalesforce@Dexcom.com'};
        String[] toAddresses = new String[] {'priyanka.kajawe@dexcom.com'};
        mail.setToAddresses(toAddresses);
        mail.setSubject('BclsProcessSSIPSchedules Apex Batch Job is complete for ' + System.Today().format() + ' run');
        mail.setPlainTextBody('The batch Apex Job Processed Successfully');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
      
    }
    
}