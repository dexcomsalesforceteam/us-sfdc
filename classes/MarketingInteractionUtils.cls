/**
 * Created by dakotalarson on 8/27/19.
 */

public with sharing class MarketingInteractionUtils {
    public static List<Marketing_Interaction__c> filterExistingInteractions(List<Marketing_Interaction__c> newMarketingInteractions) {
        Set<String> newCommunicationTypes = new Set<String>();
        Set<String> newOrderHeaderIds = new Set<String>();
        Set<Id> newAccounts = new Set<Id>();
        Set<String> newRelatedInformation = new Set<String>();

        for (Marketing_Interaction__c marketingInteraction : newMarketingInteractions) {
            newCommunicationTypes.add(marketingInteraction.Communication_Type__c);
            newOrderHeaderIds.add(marketingInteraction.Source_Record_Id__c);
            newAccounts.add(marketingInteraction.Account__c);
            newRelatedInformation.add(marketingInteraction.Related_Information__c);
        }

        List<Marketing_Interaction__c> existingMarketingInteractions = [
                SELECT Communication_Type__c, Source_Record_Id__c, Account__c, Related_Information__c
                FROM Marketing_Interaction__c
                WHERE Communication_Type__c IN: newCommunicationTypes AND
                Source_Record_Id__c IN: newOrderHeaderIds AND
                Account__c IN: newAccounts AND
                (Related_Information__c IN: newRelatedInformation OR Related_Information__c = NULL)
        ];

        List<Marketing_Interaction__c> filteredMarketingInteractions = new List<Marketing_Interaction__c>();
        for (Marketing_Interaction__c newMarketingInteraction : newMarketingInteractions) {
            Boolean foundExisting = false;

            for (Marketing_Interaction__c existingMarketingInteraction : existingMarketingInteractions) {
                if (newMarketingInteraction.Communication_Type__c == existingMarketingInteraction.Communication_Type__c &&
                        newMarketingInteraction.Source_Record_Id__c == existingMarketingInteraction.Source_Record_Id__c &&
                        newMarketingInteraction.Account__c == existingMarketingInteraction.Account__c &&
                        newMarketingInteraction.Related_Information__c == existingMarketingInteraction.Related_Information__c) {
                    foundExisting = true;
                    break;
                }
            }

            if (!foundExisting) {
                filteredMarketingInteractions.add(newMarketingInteraction);
            }
        }
        return filteredMarketingInteractions;
    }
}