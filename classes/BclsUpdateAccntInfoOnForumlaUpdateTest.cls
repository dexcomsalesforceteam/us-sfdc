@isTest
public class BclsUpdateAccntInfoOnForumlaUpdateTest
{
    @testSetup static void setup() {
        Id customerRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Consumers').getRecordTypeId();
        Id payorRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
        List<Account> accountInsertList = new List<Account>();
        
        //Create a map between the Record Type Name and Id 
        /*Map<String,String> accountRecordTypes = new Map<String,String>{};
		for(RecordType rt: rtypes)
		accountRecordTypes.put(rt.Name,rt.Id);*/
        //Get Consumers record type 
        String consumerRecordTypeId = customerRecordType;//accountRecordTypes.get('Consumers');
        //Get Payor record type 
        String payorRecordTypeId = payorRecordType;//accountRecordTypes.get('Payor');
        //Create Pricebook
        Pricebook2 reOrderPricebook = new Pricebook2();
        reOrderPricebook.Name = 'M/C - Reorder Price List';
        reOrderPricebook.IsActive = true;
        insert reOrderPricebook;
        //Create Medicare Account   
        Account mdcrAccnt = new Account();
        mdcrAccnt.FirstName = 'MDCR FirstName';
        mdcrAccnt.LastName = 'MDCR LastName';
        mdcrAccnt.Customer_Type__c = 'Medicare FFS';
        mdcrAccnt.MDCR_Communication_Preference__c = 'Phone';
        mdcrAccnt.Next_MDCR_Reorder_Follow_up_Date__c = Date.today();
        mdcrAccnt.MDCR_FFS_Subscription__c = 'Active';
        mdcrAccnt.RecordtypeId = consumerRecordTypeId;
        mdcrAccnt.DOB__c = Date.newInstance(1992, 04, 23);
        //insert mdcrAccnt;
        accountInsertList.add(mdcrAccnt);
        //Create a Payor record
        Account mdcrPayor = new Account();
        mdcrPayor.RecordTypeId = payorRecordTypeId;
        mdcrPayor.Name = 'Medicare Test Payor';
        mdcrPayor.Claim_Address__c = '1 Main Street, San Diego, CA - 92127';
        mdcrPayor.Is_Medicare_Payor__c = true;
        mdcrPayor.DOB__c = Date.newInstance(1992, 04, 23);
        accountInsertList.add(mdcrPayor);
        if(!accountInsertList.isEmpty()){
            database.insert(accountInsertList);
        }
        //insert mdcrPayor;
        
        //Create ship to address
        Address__c mdcrAccntAddress = new Address__c();
        mdcrAccntAddress.Account__c = mdcrAccnt.Id;
        mdcrAccntAddress.Street_Address_1__c = '1 Main Street';
        mdcrAccntAddress.City__c = 'San Diego';
        mdcrAccntAddress.State__c = 'CA';
        mdcrAccntAddress.Zip_Postal_Code__c = '92121';
        mdcrAccntAddress.Country__c = 'USA';
        mdcrAccntAddress.Primary_Flag__c = TRUE;
        mdcrAccntAddress.Address_Type__c = 'SHIP_TO';
        
        insert mdcrAccntAddress;
        //Create Account Profile Qn
        Account_Profile__c qn = new Account_Profile__c();
        qn.Account__c = mdcrAccnt.Id;
        qn.Question__c = 'Have you been diagnosed with Type 1 or Type 2 Diabetes?';
        qn.Answer__c = true;
        qn.External_Id__c = mdcrAccnt.Id + '1';
        qn.Type__c = 'Medicare';
        insert qn;
        //Create Benefit Record
        Test.startTest();
        Benefits__c mdcrAccntBenefit = new Benefits__c();
        mdcrAccntBenefit.Benefit_Hierarchy__c = 'Primary';
        mdcrAccntBenefit.Account__c = mdcrAccnt.Id;
        mdcrAccntBenefit.Payor__c = mdcrPayor.Id;
        mdcrAccntBenefit.MEMBER_ID__c = '12343';
        insert mdcrAccntBenefit;
        Account newMDCRAccount = [Select Id, Customer_Type__c, MDCR_FFS_Subscription_Update_Check__c, MDCR_FFS_Subscription_Calculation__c FROM Account Where Id = : mdcrAccnt.Id];
        system.assertEquals(newMDCRAccount.Customer_Type__c, 'Medicare FFS');
        system.assertEquals(newMDCRAccount.MDCR_FFS_Subscription_Calculation__c, 'Not Active');
        system.assertEquals(newMDCRAccount.MDCR_FFS_Subscription_Update_Check__c, true);
        Test.stopTest();
        
    }
    
    @isTest static void TestAccountUpdateFromFormulaUpdate()
    {
        //Query for the Account record types
        /*List<RecordType> rtypes = [Select Name, Id From RecordType 
                  where sObjectType='Account' and isActive=true];*/
        
        //Test Batch Class 
        Test.StartTest();
        BclsUpdateAccntInfoOnForumlaUpdate obj = new BclsUpdateAccntInfoOnForumlaUpdate();
        Database.executeBatch(obj);
        Test.stoptest();
    }
    
    @isTest
    public static void BclsUpdateAccntInfoSchedTest(){
        Test.startTest();
        BclsUpdateAccntInfoOnForumlaUpdateSched scheduler = new BclsUpdateAccntInfoOnForumlaUpdateSched();
        String sch = '0 0 23 * * ?';
        system.schedule('Bcls Update Account info Schd', sch, scheduler);
        Test.stopTest();
    }
}