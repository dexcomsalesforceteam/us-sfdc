/*********************************************************************************************
* @description : Class will handle process required in trigger OrderHeaderMasterTrigger
**********************************************************************************************/

public with sharing class OrderHeaderTriggerHandler {
    private static Boolean ONBOARDING_HAS_RUN = false;
    //Logic to link Opportunity to Order that is inserted
    public static void onBeforeInsert(List<Order_Header__c> newOrderHeaderList){
        List <Order_Header__c> orderList = new List <Order_Header__c>();
        Set <String> orderIdSet = new Set <String>();
        for(Order_Header__c oh : newOrderHeaderList){
            if(!String.isEmpty(oh.Order_Id__c)){
                orderList.add(oh);
                orderIdSet.add(oh.Order_Id__c);
            }   
        }
        if(!orderList.isEmpty()){
            updateOrderOppty(orderIdSet, orderList);
        }
    }
    
    /*
     * @Description - this function associates the payor from account on
     * order header.
     * This function is created for CRMSF-4738 and called by before insert routine
     * modified for CRMSF-4714
     * @Param - List<Order_Header__c>
    */
    public static void beforeInsertPayorAssociate(List <Order_Header__c> newOrderHeaderList){
        Map<Id, Account> accountMap; // holds accound Id to Payor Id mapping
        Set<Id> accountIdSet = new Set<Id>();
        Id consumerRecordTypeId = ClsApexUtility.getRecordTypeIdByDeveloperName(ClsApexConstants.ACCOUNT_API_NAME, ClsApexConstants.ACCOUNT_REC_TYPE_CONSUMERS_LABEL);
        for(Order_Header__c orderHeaderObj : newOrderHeaderList){
            orderHeaderObj.Creator_Email_Id__c = UserInfo.getUserEmail(); // added for CRMSF-4714
            accountIdSet.add(orderHeaderObj.Account__c);
        }
        accountMap = new Map<Id, Account>([SELECT Id,Payor__c FROM account WHERE Id IN : accountIdSet AND RecordTypeId = :consumerRecordTypeId]);
        for(Order_Header__c orderHeaderObj : newOrderHeaderList){
            if(accountMap != null && accountMap.containsKey(orderHeaderObj.Account__c) && accountMap.get(orderHeaderObj.Account__c) != null){
                orderHeaderObj.Payor_At_Creation__c = accountMap.get(orderHeaderObj.Account__c).Payor__c;
            }
        }
    }
    
    private static void updateOrderOppty(Set <String> orderNumSet, List <Order_Header__c> newOrderHeaderList){
        map <String, Opportunity> oppMap = new map<String, Opportunity>();
        for (Opportunity opp : [SELECT Id, Order_NUM__c FROM Opportunity WHERE Order_NUM__c IN: orderNumSet AND Order_NUM__c != NULL AND isDeleted = false]){
            if(!oppMap.containsKey(opp.Order_NUM__c)){
                oppMap.put(opp.Order_NUM__c, opp);
            }
        }
        
        for (Order_Header__c oh : newOrderHeaderList){
            if(oppMap.get(oh.Order_Id__c) != NULL){
                oh.Opportunity__c = oppMap.get(oh.Order_Id__c).Id;
            }
        }
    }
    //START:CRMSF-4438 : Below code not needed.
    //Process logic to send out auth code for customers who have touch screen receiver
  /*  public static void processToSendG6AuthCode(List<Order_Header__c> newOrderHeaderList, Map<Id, Order_Header__c> oldOrderHeaderMap){
        for(Order_Header__c orderHeader : newOrderHeaderList)
        {
            //Set to hold Accounts which are to be processed
            Set<String> accountsToProcessSet = new Set<String>(); 
            
            if(orderHeader.Last_G6_Transmitter_Ship_Date__c != oldOrderHeaderMap.get(orderHeader.Id).Last_G6_Transmitter_Ship_Date__c &&
               orderHeader.MDCR_Followup__c != null && orderHeader.MFR_Type__c == 'G6 Transition from Touchscreen')
            {
                system.debug('--Entered the if block');
                accountsToProcessSet.add(orderHeader.Account__c);
            }
            if(!accountsToProcessSet.isEmpty())
            {
                //Remove the account for which the receiver code is already sent
                for(Task t : [SELECT WhatId FROM Task WHERE Type= 'G6 Receiver Auth Code' AND WhatId IN :accountsToProcessSet])
                {
                    system.debug('--Entered the for block');
                    accountsToProcessSet.remove(t.WhatId);
                }
                //Process accounts to send the receiver code
                if(!accountsToProcessSet.isEmpty())
                {
                    system.debug('--Entered the block to send the code');
                    sendAuthCode(accountsToProcessSet);
                }
            }
        }
    }
    
    //Callout to send to the auth code
    @future(callout=true)
    public static void sendAuthCode (Set<String> accountIds) {
        
        //Initialize variables
        List<account> callOutAccounts = new List<account>();
        List<Task> tasksToBeCreated = new List<Task>();
        List<ClsG6UpgradeCalloutPayloadWrapper> payloadList = new List<ClsG6UpgradeCalloutPayloadWrapper>();
        String userId = UserInfo.getUserId(); 
        
        callOutAccounts = [SELECT Id,Lastname,Firstname,Phone,ShippingStreet, Address_Line_2__c,Address_Line_3__c,ShippingCity,ShippingPostalCode,ShippingState FROM Account WHERE Id IN : accountIds];    
        
        //Prepare payload for call out  
        for (account thisAccount: callOutAccounts)  
        {
            ClsG6UpgradeCalloutPayloadWrapper payload = new ClsG6UpgradeCalloutPayloadWrapper();
            payload.ID = 0;
            payload.firstName = thisAccount.firstname;
            payload.lastName = thisAccount.lastname;
            payload.address1 = thisAccount.ShippingStreet;
            payload.address2 = thisAccount.Address_Line_2__c;
            payload.address3 = thisAccount.Address_Line_3__c;
            payload.city = thisAccount.ShippingCity;
            payload.zipCode = thisAccount.ShippingPostalCode; 
            payload.state = thisAccount.ShippingState;  
            payload.PhoneNumber = thisAccount.phone;  
            payload.quantity = 1;
            payload.status =  'Activated';
            payload.dateCreated = system.today();
            payload.sku = 'DX-AUTHBUNDLE-00'; 
            payloadList.add(payload); 
            
        }
        //Process the return payload
        if(!payloadList.isEmpty()){
            string rowNumber = ClsPGICallout.sendCallout(JSON.serialize(payloadList));
            system.debug('******rowNumber' + rowNumber);
            
            //Process task createion
            for(account acc: callOutAccounts)
            {
                Task t = new Task();
                t.OwnerId = userId;
                t.Subject ='Receiver AuthCode Process Initiated and Raw Number = ' + rowNumber;
                t.Type= 'G6 Receiver Auth Code';
                t.Status = 'Completed';
                t.Priority = 'Normal';
                t.WhatId = acc.Id;
                t.ActivityDate = date.today();
                tasksToBeCreated.add(t);
            } 
            if(!tasksToBeCreated.isEmpty())
            {
                try{
                    insert tasksToBeCreated;
                }catch (Exception e){
                    system.debug('***TASK CANNOT BE CREATED ERROR = ' + e.getMessage());
                }
            }
        }
        
    }*/ //END:CRMSF-4438
    
    @InvocableMethod
    public static void G6UpgradeCodeCallOut (List<Order_Header__c> listOrderHeader)  
    {
        System.debug('OrderHeaderTriggerHandler/G6UpgradeCodeCallOut Called from tigger');
        List<ID> accList = new List<ID>();
        for(Order_Header__c orderHeader : listOrderHeader)
        {
            
            System.debug('About to make a callout');
            accList.add(orderHeader.Account__c); 
        }  
        system.debug('-------------------------------------------------'+accList);
        if(!Test.isRunningTest()){
            ClsG6UpgradeCallout.sendNotification(accList); 
        }   
    }
    
    /*Updates related order [order Shipped Date] with Shipped Date.
Finds all SSIP records related to related order and updates actual shipped date to order shipped date.*/
    
    public static void updateShippedDate(Map<Id, Order_Header__c> newMap, Map<Id, Order_Header__c> oldMap) {     
        Map<Id, Order_Header__c> mapOrderOH= new Map<Id, Order_Header__c>();
        
        // Check for changes to OH Order_Shipped_Date__c
        for (Id ohId : newMap.keySet()) {
            Order_Header__c newOH = newMap.get(ohId);
            Order_Header__c oldOH = oldMap.get(ohId);
            
            // If only OH order shipped date is being changed and it still references an order
            if ((oldOH.Order_Shipped_Date__c != newOH.Order_Shipped_Date__c && newOH.Order_Shipped_Date__c != null) && 
                (oldOH.Order__c == newOH.Order__c && newOH.Order__c != null)
               ) {              
                   if(newOH.Order__c != null){
                       mapOrderOH.put(newOH.Order__c, newOH);
                   }
               }
        }
        System.Debug('**** TPS:EH 1.1 UpdateShippedDate, mapOrderOH=' + mapOrderOH);
        
        if(mapOrderOH.keySet().size()>0){
            
            Map<Id, Order> mapOrders= new Map<Id, Order>([Select Id, Order_Shipped_Date__c, (Select Id, Name, Actual_Shipment_Date__c, Status__c from Order.SSIP_Schedules__r)
                                                          from Order Where ID IN :mapOrderOH.keySet()]);
            
            System.Debug('**** TPS:EH 2.1 UpdateShippedDate, mapOrders=' + mapOrders);
            
            List<Order> lstOrderUpd= new List<Order>();
            List<SSIP_Schedule__c> lstSSIPUpd= new List<SSIP_Schedule__c>();
            Map<Order, List<SSIP_Schedule__c>> mapOrderSSIP= new Map<Order, List<SSIP_Schedule__c>>();
            
            for(Order o : mapOrders.values()){
                o.Order_Shipped_Date__c=mapOrderOH.get(o.Id).Order_Shipped_Date__c;
                lstOrderUpd.Add(o);
                
                if(o.SSIP_Schedules__r!= null){
                    mapOrderSSIP.put(o, o.SSIP_Schedules__r);
                    for(SSIP_Schedule__c ssip : o.SSIP_Schedules__r){
                        ssip.Actual_Shipment_Date__c =o.Order_Shipped_Date__c;
                        ssip.Status__c ='Closed';
                        lstSSIPUpd.Add(ssip);
                    }
                }
            }
            update lstOrderUpd;
            System.Debug('**** TPS:EH 2.2 UpdateShippedDate, lstSSIPUpd=' + lstSSIPUpd);
            
            ClsUpdateOrderSSIPRecs clsUpdateSSIP= new ClsUpdateOrderSSIPRecs(lstSSIPUpd);
            ID jobID = System.enqueueJob(clsUpdateSSIP);
            
        }
        
    }
    
    public static void handleOrderConfirmation(Map<Id, Order_Header__c> newOrderHeaders, Map<Id, Order_Header__c> oldOrderHeaders) {
        
        List<Marketing_Interaction__c> interactions = new List<Marketing_Interaction__c>();
        
        for (Id orderHeaderId : newOrderHeaders.keySet()) {
            Order_Header__c newOrderHeader = newOrderHeaders.get(orderHeaderId);
            Order_Header__c oldOrderHeader = oldOrderHeaders.get(orderHeaderId);
            
            if (newOrderHeader.Status__c != null && newOrderHeader.Status__c == 'BOOKED' || newOrderHeader.Status__c == 'PROCESSING' && newOrderHeader.Status__c != oldOrderHeader.Status__c) {
                if (newOrderHeader.Order_Type__c == 'Standard Sales Order' || newOrderHeader.Order_Type__c == 'MC Standard Sales Order' || newOrderHeader.Order_Type__c == 'Subscription') {
                    Marketing_Interaction__c interaction = new Marketing_Interaction__c();
                    interaction.Communication_Type__c = 'Order Confirmation';
                    interaction.Account__c = newOrderHeader.Account__c;
                    interaction.Source_Record_Id__c = newOrderHeader.Order_Header_18_Digit_ID__c;
                    
                    interactions.add(interaction);
                }
            }
        }
        
        List<Marketing_Interaction__c> filteredInteractions = MarketingInteractionUtils.filterExistingInteractions(interactions);
        insert filteredInteractions;
    }
    
    /**
*@description  : This method creates Marketing Interaction records when an Order Header with certain Order_Type__c values has Status__c udpated to SHIPPED
*@author    : Kristen (Sundog)
*@date      : 07.26.2019
*@param    : trigger.newMap, trigger.oldMap
*@return    : N/A
**/
    public static void handleShippedUpdate(Map<Id, Order_Header__c> newMap, Map<Id, Order_Header__c> oldMap) {
        Set<Id> elegibleOHIdSet = new Set<Id>();
        Set<Id> elegibleAccountIdSet = new Set<Id>();
        List<Order_Header__c> pastOrdersOnElegibleAccounts;
        //List<Marketing_Interaction__c> existingMIOnElegibleAccounts;
        Set<Id> accountIdOnExistingMISet = new Set<Id>();
        Map<Id, Integer> accountIdToOrderCountMap = new Map<Id, Integer>();
        List<Marketing_Interaction__c> miToInsert = new List<Marketing_Interaction__c>();
        List<String> validOrderTypes = new List<String>{
            'GB Standard Sales Order', 'GB Subscription', 'IE Standard Sales Order', 'IE Subscription'
                };
                    List<String> validOrderSubscriptionTypes = new List<String>{
                        'GB Subscription', 'IE Subscription'
                            };
                                for (Id ohId : newMap.keySet()) {
                                    Order_Header__c newOH = newMap.get(ohId);
                                    Order_Header__c oldOH = oldMap.get(ohId);
                                    if (validOrderTypes.contains(newOH.Order_Type__c) && oldOH.Status__c != 'SHIPPED' && newOH.Status__c == 'SHIPPED') {
                                        elegibleAccountIdSet.add(newOH.Account__c);
                                        elegibleOHIdSet.add(ohId);
                                    }
                                }
        
        if (!elegibleOHIdSet.isEmpty()) {
            pastOrdersOnElegibleAccounts = [SELECT Id, Account__c FROM Order_Header__c WHERE Order_Type__c IN :validOrderTypes AND Status__c = 'SHIPPED' AND ID NOT IN :elegibleOHIdSet];
            for (Marketing_Interaction__c mi : [SELECT Account__c FROM Marketing_Interaction__c WHERE Account__c IN :elegibleAccountIdSet AND (Communication_Type__c = 'GBIE - Engagement' OR Communication_Type__c = 'GBIE - Engagement Subscription')]) {
                accountIdOnExistingMISet.add(mi.Account__c);
            }
            
            for (Order_Header__c oh : pastOrdersOnElegibleAccounts) {
                if (accountIdToOrderCountMap.keySet().contains(oh.Account__c)) {
                    Integer count = accountIdToOrderCountMap.get(oh.Account__c);
                    count++;
                    accountIdToOrderCountMap.put(oh.Account__c, count);
                } else {
                    accountIdToOrderCountMap.put(oh.Account__c, 1);
                }
            }
            
            for (Id ohId : elegibleOHIdSet) { // iterate over OH with qualifying Status and Order Type
                Order_Header__c oh = newMap.get(ohId);
                if (!accountIdOnExistingMISet.contains(oh.Account__c)) { // if the account has no existing, conflicting Marketing Interactions
                    if (accountIdToOrderCountMap.keySet().contains(oh.Account__c)) { // if the account has existing eligible order headers
                        // create MI with GBIE - Engagement
                        miToInsert.add(createMIRecord(oh.Account__c, 'GBIE - Engagement', null, null, null));
                    } else { // if the account has no existing eligible order headers
                        if (validOrderSubscriptionTypes.contains(oh.Order_Type__c)) { // if the Order Type is 'GB Subscription' or 'IE Subscription'
                            // create GBIE - Engagement Subscription
                            miToInsert.add(createMIRecord(oh.Account__c, 'GBIE - Engagement Subscription', null, null, null));
                        }
                    }
                }
            }
        }
        insert miToInsert;
    }
    
    public static void handleCustomerOnboarding(Map<Id, Order_Header__c> newOrderHeaders, Map<Id, Order_Header__c> oldOrderHeaders) {
        if (ONBOARDING_HAS_RUN) return;
        Set<Order_Header__c> updatedOrderHeaders = new Set<Order_Header__c>();
        for (Id id : newOrderHeaders.keySet()) {
            Order_Header__c newOrderHeader = newOrderHeaders.get(id);
            Order_Header__c oldOrderHeader = oldOrderHeaders.get(id);
            
            Boolean statusChanged = newOrderHeader.Status__c == 'SHIPPED' && oldOrderHeader.Status__c != 'SHIPPED';
            Boolean flowStatusChanged = newOrderHeader.Flow_Status_Code__c == 'CLOSED' && oldOrderHeader.Flow_Status_Code__c != 'CLOSED';
            if (statusChanged || flowStatusChanged) {
                updatedOrderHeaders.add(newOrderHeader);
            }
        }
        System.debug(updatedOrderHeaders.size());
        Set<Id> accountIds = new Set<Id>();
        Set<Order_Header__c> eligibleOrderHeaders = new Set<Order_Header__c>();
        for (Order_Header__c orderHeader : updatedOrderHeaders) {
            if (orderHeader.Account_Record_Type__c == 'Consumers' && orderHeader.Status__c == 'SHIPPED' &&
                orderHeader.Flow_Status_Code__c == 'CLOSED' && orderHeader.Latest_Transmitter_Ship_Date__c != null) {
                    accountIds.add(orderHeader.Account__c);
                    eligibleOrderHeaders.add(orderHeader);
                }
        }
        if (accountIds.size() > 0) {
            List<Marketing_Interaction__c> interactions = new List<Marketing_Interaction__c>();
            Map<Order_Header__c, Account> accountsByOrderHeaders = new Map<Order_Header__c, Account>();
            Map<Id, Account> accounts = new Map<Id, Account>([
                SELECT Id, Last_Transmitter_Ship_Date__c, Last_G4_Transmitter_Ship_Date__c,
                Last_G5_Transmitter_Ship_Date__c, Last_G6_Transmitter_Ship_Date__c
                FROM Account
                WHERE Id IN:accountIds
            ]);

            List<Marketing_Account__c> marketingAccounts = [
                    SELECT Id, Account__c
                    FROM Marketing_Account__c
                    WHERE Account__c IN: accountIds
            ];

            Map<Id, Id> marketingAccountsByAccounts = new Map<Id, Id>();
            for (Marketing_Account__c marketingAccount : marketingAccounts) {
                marketingAccountsByAccounts.put(marketingAccount.Account__c, marketingAccount.Id);
            }
            
            for (Order_Header__c orderHeader : eligibleOrderHeaders) {
                accountsByOrderHeaders.put(orderHeader, accounts.get(orderHeader.Account__c));
            }
            
            Map<Order_Header__c, Map<String, Boolean>> isFirstDateByOrderHeaders = getIsFirstWithDateByOrderHeaders(eligibleOrderHeaders);
            
            for (Order_Header__c orderHeader : accountsByOrderHeaders.keySet()) {
                Account account = accountsByOrderHeaders.get(orderHeader);
                Id marketingAccountId = marketingAccountsByAccounts.get(account.Id);
                
                System.debug(orderHeader.Last_G4_Transmitter_Ship_Date__c);
                System.debug(account.Last_G4_Transmitter_Ship_Date__c);
                if (account.Last_Transmitter_Ship_Date__c == null) {
                    interactions.add(createMIRecord(orderHeader.Account__c, 'Customer Onboarding', orderHeader.Order_Header_18_Digit_ID__c, orderHeader.Shipping_Method__c, marketingAccountId));
                } else if (isFirstShipDate(orderHeader, isFirstDateByOrderHeaders)) {
                    interactions.add(createMIRecord(orderHeader.Account__c, 'Customer Onboarding', orderHeader.Order_Header_18_Digit_ID__c, orderHeader.Shipping_Method__c, marketingAccountId));
                } else {
                    interactions.add(createMIRecord(orderHeader.Account__c, 'Transmitter Pairing', orderHeader.Order_Header_18_Digit_ID__c, null, null));
                }
            }
            
            List<Marketing_Interaction__c> filteredInteractions = MarketingInteractionUtils.filterExistingInteractions(interactions);
            insert filteredInteractions;
            
            ONBOARDING_HAS_RUN = true;
        }
    }
    
    /**@description  : This method supplements OrderHeaderTriggerHandler.handleShippedUpdate with functionality to create Marketing Interaction records
*@author    : Kristen (Sundog)
*@date      : 07.26.2019
*@param    : Account Id for Marketing_Interaction__c.Account__c, String for Marketing_Interaction__c.Communication_Type__c
*@return    : N/A
**/
    private static Marketing_Interaction__c createMIRecord(Id accountId, String commType, String orderHeaderId, String shippingMethod, Id marketingAccountId) {
        Marketing_Interaction__c interaction = new Marketing_Interaction__c(
            Account__c = accountId,
            Communication_Type__c = commType
        );
        
        if (orderHeaderId != null) {
            interaction.Source_Record_Id__c = orderHeaderId;
        }

        if (shippingMethod != null) {
            interaction.Related_Information__c = shippingMethod;
        }

        if (marketingAccountId != null) {
            interaction.Marketing_Account__c = marketingAccountId;
        }
        
        return interaction;
    }
    
    // Rollup computed before trigger runs.
    private static Map<Order_Header__c, Map<String, Boolean>> getIsFirstWithDateByOrderHeaders(Set<Order_Header__c> updatedOrderHeaders) {
        List<Id> accountIds = new List<Id>();
        List<Id> updatedOrderHeaderIds = new List<Id>();
        for (Order_Header__c orderHeader : updatedOrderHeaders) {
            accountIds.add(orderHeader.Account__c);
            updatedOrderHeaderIds.add(orderHeader.Id);
        }
        
        List<Order_Header__c> otherOrderHeaders = [
            SELECT Last_G4_Transmitter_Ship_Date__c, Last_G5_Transmitter_Ship_Date__c, Last_G6_Transmitter_Ship_Date__c, Account__c
            FROM Order_Header__c
            WHERE Account__c IN: accountIds AND
            Id NOT IN: updatedOrderHeaderIds AND
            (Last_G4_Transmitter_Ship_Date__c != NULL OR
             Last_G5_Transmitter_Ship_Date__c != NULL OR
             Last_G6_Transmitter_Ship_Date__c != NULL)
        ];
        
        Map<Order_Header__c, Map<String, Boolean>> firstWithDateByOrderHeaders = new Map<Order_Header__c, Map<String, Boolean>>();
        for (Order_Header__c orderHeader : updatedOrderHeaders) {
            Map<String, Boolean> firstWithDate = new Map<String, Boolean>();
            firstWithDate.put('G4', true);
            firstWithDate.put('G5', true);
            firstWithDate.put('G6', true);
            firstWithDateByOrderHeaders.put(orderHeader, firstWithDate);
        }
        
        for (Order_Header__c otherOrderHeader : otherOrderHeaders) {
            for (Order_Header__c updatedOrderHeader : firstWithDateByOrderHeaders.keySet()) {
                if (otherOrderHeader.Account__c == updatedOrderHeader.Account__c) {
                    if (otherOrderHeader.Last_G4_Transmitter_Ship_Date__c != null) {
                        firstWithDateByOrderHeaders.get(updatedOrderHeader).put('G4', false);
                    }
                    if (otherOrderHeader.Last_G5_Transmitter_Ship_Date__c != null) {
                        firstWithDateByOrderHeaders.get(updatedOrderHeader).put('G5', false);
                    }
                    if (otherOrderHeader.Last_G6_Transmitter_Ship_Date__c != null) {
                        firstWithDateByOrderHeaders.get(updatedOrderHeader).put('G6', false);
                    }
                }
            }
        }
        
        return firstWithDateByOrderHeaders;
    }
    
    private static Boolean isFirstShipDate(Order_Header__c orderHeader, Map<Order_Header__c, Map<String, Boolean>> isFirstByOrderHeaders) {
        Boolean headerHasG4 = orderHeader.Last_G4_Transmitter_Ship_Date__c != null;
        Boolean headerHasG5 = orderHeader.Last_G5_Transmitter_Ship_Date__c != null;
        Boolean headerHasG6 = orderHeader.Last_G6_Transmitter_Ship_Date__c != null;
        
        Boolean isOnlyG4 = isFirstByOrderHeaders.get(orderHeader).get('G4');
        Boolean isOnlyG5 = isFirstByOrderHeaders.get(orderHeader).get('G5');
        Boolean isOnlyG6 = isFirstByOrderHeaders.get(orderHeader).get('G6');
        
        return headerHasG4 && isOnlyG4 || headerHasG5 && isOnlyG5 || headerHasG6 && isOnlyG6;
    }
    
    
    /*
     * @Description - This method is created for CRMSF-4714
     * this method created tasks at order header level, to notify the order header creator that 
     * it runs after update if the RGA approval status is approved or no approval required and
     * integration error message is not blank
    */
    public static void afterUpdateCreateTask(Map<Id, Order_Header__c> newOrderHeaderMap, Map<Id, Order_Header__c> oldOrderHeaderMap){
        List<Task> taskList = new List<Task>();
        for(Order_Header__c newOrderHeaderRec : newOrderHeaderMap.values()){
            if(oldOrderHeaderMap.containsKey(newOrderHeaderRec.Id) && oldOrderHeaderMap.get(newOrderHeaderRec.Id)!= null){
                if((ClsApexConstants.RGA_APPROVAL_NO_APPROVAL_REQ.equalsIgnoreCase(newOrderHeaderRec.RGA_Approval_Status__c) ||
                    ClsApexConstants.STATUS_APPROVED.equalsIgnoreCase(newOrderHeaderRec.RGA_Approval_Status__c)) &&
                   newOrderHeaderRec.Integration_Error_Message__c != oldOrderHeaderMap.get(newOrderHeaderRec.Id).Integration_Error_Message__c &&
                   String.isNotBlank(newOrderHeaderRec.Integration_Error_Message__c)){
                       Task newNotificationTask = new Task();
                       newNotificationTask.WhatId = newOrderHeaderRec.Id;
                       newNotificationTask.Subject = ClsApexConstants.RGA_TASK_STATUS;
                       newNotificationTask.OwnerId = newOrderHeaderRec.CreatedById;
                       newNotificationTask.Type = ClsApexConstants.TASK_TYPE;
                       newNotificationTask.Status = ClsApexConstants.STATUS_NOT_STARTED;
                       newNotificationTask.Priority = ClsApexConstants.TASK_PRIORITY;
                       newNotificationTask.RecordTypeId = ClsApexUtility.getRecordTypeIdByDeveloperName(ClsApexConstants.TASK_SOBJECT_API, ClsApexConstants.RECORD_TYPE_DEVELOPER_API_NAME);
                       taskList.add(newNotificationTask);
                   }
            }
        }
        
        if(!taskList.isEmpty()){
            database.insert(taskList, ClsApexConstants.BOOLEAN_FALSE);
        }
    }
    
    /*
     * Created to retire workflow Update Order Ship Date
     * added for for CRMSF-5319 to update order shipped date
	*/
    public static void handleBeforeUpdate(List<Order_Header__c> newOrderHeaderList, Map<Id, Order_Header__c> oldOrderHeaderMap){
        for(Order_Header__c orderHeaderObj : newOrderHeaderList){
            if(oldOrderHeaderMap.containsKey(orderHeaderObj.Id) && oldOrderHeaderMap.get(orderHeaderObj.Id) != null
              && orderHeaderObj.Status__c != oldOrderHeaderMap.get(orderHeaderObj.Id).Status__c &&
              ClsApexConstants.STRING_STATUS_SHIPPED.equalsIgnoreCase(orderHeaderObj.Status__c) &&
              !ClsApexConstants.STATUS_CANCELLED.equalsIgnoreCase(orderHeaderObj.Flow_Status_Code__c)){
                orderHeaderObj.Order_Shipped_Date__c = system.today();
            }
        }
    }
}