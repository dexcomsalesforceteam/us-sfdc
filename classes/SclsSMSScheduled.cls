/**
 * SMSScheduled
 * Scheduled method to kick off batch to resend any missed SMS messages
 * @author Katie Wilson(Sundog)
 * @date 09/28/18
 */
global class SclsSMSScheduled implements Schedulable{
    global void execute(SchedulableContext sc) {
        BclsSMSBatch smsBatch = new BclsSMSBatch();   
        //There is not much volume here so using a batch size of 1 keeps the 
        // batch job code simple and more transactional to one record at a time.
        Id batchInstanceId = Database.executeBatch(smsBatch, 1);
    }
}