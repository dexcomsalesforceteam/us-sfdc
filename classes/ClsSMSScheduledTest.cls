/**
* Test Class for SMSScheduled
*
* @author Katie Wilson(Sundog)
* @date 9/28/2019
*/
@IsTest
public class ClsSMSScheduledTest {

    static testMethod void testSMSScheduledJob() {
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperMock());
        List <Account> accountList= new List<Account>();
        Account testAccount = TestDataBuilder.testAccount();
        testAccount.PersonMobilePhone='1234567890';
        testAccount.SMS_Opt_In_List__c='True';
        accountList.add(testAccount);  
        
        Account testAccount2 = TestDataBuilder.testAccount();
        testAccount2.PersonMobilePhone='1234567891';
        testAccount2.SMS_Opt_In_List__c='True'; 
        accountList.add(testAccount2);      
        insert accountList;
        
        Account delacc=[SELECT SMS_Opt_In_Sent__c FROM Account].get(0);
        List <Marketing_Account__c> deleteMAccount= new list<Marketing_Account__c>();
        deleteMAccount=[SELECT id FROM Marketing_Account__c];
        delete deleteMAccount;
        delete delacc;  
        
        Test.startTest();
        String jobId = System.schedule('testSMSScheduled', '0 0 0 3 9 ? 2052',
                new SclsSMSScheduled());
        Test.stopTest();
        //A scheduled job does not really make the batch job run so there is nothing to assert here.
    }
    
}