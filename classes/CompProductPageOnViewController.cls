/********************************************************************************
@author Abhishek Parghi
@date 1/27/2017
@description: Controller Extension for CompProductPageOnView Visualforce Page.
*******************************************************************************/
public class CompProductPageOnViewController{
    
    Public Comp_Product_Request__c obj {get;set;}
    //Constructor
    public CompProductPageOnViewController(ApexPages.StandardController controller) {
        obj = (Comp_Product_Request__c ) controller.getrecord();
    }
    // Method to fetch History
    public list<cHistories> getHistories() {
      list<cHistories> list_ch = new list<cHistories>();
      // Loop through all field history records
      for (Comp_Product_Request__history fh: [select parentID,OldValue,NewValue, IsDeleted,Id,Field,CreatedDate,CreatedById,CreatedBy.Name
                                              From Comp_Product_Request__history where ParentId =:obj.id order by CreatedDate desc]){
      // Create a new wrapper object
      cHistories ch = new cHistories();
      ch.theDate = String.valueOf(fh.createddate);
      ch.who = fh.createdby.name;
 
      if (String.valueOf(fh.Field) == 'created') { // on Creation
      ch.action = 'Created.';
      } else if (fh.OldValue != null && fh.NewValue == null){ // when deleting a value from a field
      try {
      ch.action = 'Deleted ' + Date.valueOf(fh.OldValue).format() + ' in <b>' + String.valueOf(fh.Field) + '</b>.';
      } catch (Exception e){
      ch.action = 'Deleted ' + String.valueOf(fh.OldValue) + ' in <b>' + String.valueOf(fh.Field) + '</b>.';
      }
 
      } else {
      String fromText = '';
      if (fh.OldValue != null) {
      try {
      fromText = ' from ' + Date.valueOf(fh.OldValue).format();
      } catch (Exception e) {
      fromText = ' from ' + String.valueOf(fh.OldValue);
      }
      }
      String toText = '';
      try {
      toText = ' from ' + Date.valueOf(fh.NewValue).format();
      } catch (Exception e) {
      toText = '' + String.valueOf(fh.NewValue);
      }
      ch.action = 'Changed <b>' + String.valueOf(fh.Field) + '</b>' + fromText + ' to <b>' + toText + '</b>.';
      }
      list_ch.add(ch);
      }
      return list_ch;
      }
      public class cHistories {
      // Class properties
        public String theDate {get; set;}
        public String who {get; set;}
        public String action {get; set;} 
      }
       //Jagan 02/16/2017 - Added this pageReference method
       public PageReference doCancel()
       {
            Schema.DescribeSObjectResult compProductSchema = Comp_Product_Request__c.SObjectType.getDescribe();
            String objectIdPrefix = compProductSchema.getKeyPrefix();
            PageReference pageReference = new PageReference('/'+objectIdPrefix+'/o');
            pageReference.setRedirect(true);
            return pageReference;
        }
   
    //Sudheer {CRMSF-4192 6/21/2019} : Fetching the Comp Products for the account to displaying on Component.
    @auraEnabled
    public Static List<Comp_Product_Request__c> getCompProducts(string accountId){
        List<Comp_Product_Request__c> compProductsList=new  List<Comp_Product_Request__c>();
        
        if(!string.isBlank(accountId)){
            compProductsList =[select id,Name,Approval_Status__c,Estimated_Amount__c,Customer_Account_No__r.Name,Repmaking_Request__c,Repmaking_Request__r.Name,Date_Comp_Requested__c from Comp_Product_Request__c where Customer_Account_No__c=:accountId  order by Date_Comp_Requested__c desc];
        }
        return compProductsList;
    }

}