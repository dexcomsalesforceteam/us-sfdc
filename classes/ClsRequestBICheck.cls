/******************************************************************************************
Description : Apex class has logic to process to perform BI Verification using Quadax API
*******************************************************************************************/
public class ClsRequestBICheck {

    //This method will be invoked when a BI Check need to be performed  
    //Parameter will be the Benefit Id and a boolean flag to denote if the verification is a new verification
    @future(callout=true)
    public static void requestBICheckWebService(String benefitId, String biRequestInvokedFrom){
    
        system.debug('====requestBICheckWebService====');
        //Initialize variables and collections
        List<String> requiredFieldList = new List<String>();
        
        //Dynamically query all fields from benefit record                                    
        Benefits__c thisBenefit = new Benefits__c();
        thisBenefit = getBenefitData(benefitId);
        system.debug('====thisBenefit===='+thisBenefit);

        //Query the metadata QUADAX_API_Setting__mdt to get the field details
        for(QUADAX_API_Setting__mdt newMdt : [SELECT MasterLabel, Type__c, FieldSet__c from QUADAX_API_Setting__mdt]){
            if(newMdt.Type__c == 'Required') requiredFieldList.addAll(newMdt.FieldSet__c.split(','));
        } 
        
        //Verify if all required fields for Quadax call is present else update the Benefit
        String missingField = verifyInputFields(thisBenefit, requiredFieldList);
        system.debug('====missingField====' +missingField);
        if(!String.isBlank(missingField))
        {
            system.debug('====missingField==available====');

            thisBenefit.BI_Status__c = 'FAIL';
            thisBenefit.Integration_Error_Message__c = 'Required Field Value Missing : ' + missingField;
            
            //update invocation calls
            thisBenefit.BI_Request_Invoked_From__c = '';
            thisBenefit.Invoke_BI_Verification__c = false;
            
            try{
                update(thisBenefit);
                
                if(biRequestInvokedFrom == 'SSIP'){
                    //Open Task on Opportunity for respective SSIP Schedule
                    insertErrorInfoOnSSIPSchedule(thisBenefit);
                }else if(biRequestInvokedFrom == 'order'){
                    
                    Set<Id> benefitsUpdateTaskId = new Set<Id>();
                    benefitsUpdateTaskId.add(thisBenefit.Account__c);
                    //START for #CRMSF-4614
                    if(benefitsUpdateTaskId.size() > 0)OrderAuditTriggerHandler.afterUpdateCreateTask( benefitsUpdateTaskId,label.Task_Description_for_any_check_fail);
                    //End for #CRMSF-4614
                    system.debug('====Order BI Check====');
                }
            }catch(Exception e){
            
                system.debug('====exception==missingField====');

                //Write the error log
                new ClsApexDebugLog().createLog(
                    new ClsApexDebugLog.Error(
                        'ClsRequestBICheck',
                        'requestBICheckWebService',
                        benefitId,
                        'Benefit staus update failed: ' + e.getMessage()
                    )
                );
            }
        }
        else
            invokeQuadaxAPI(thisBenefit.id, biRequestInvokedFrom);
    }
    
    //Invoke the Quadax API to get the BI data
    //This method will be invoked from a Button or from automated process, which need to invoke the BI Check
    public static void invokeQuadaxAPI(String benefitId, String biRequestInvokedFrom){
        
        system.debug('=====invokeQuadaxAPI=====');

        //Dynamically query all fields from benefit record                                    
        Benefits__c thisBenefit = new Benefits__c();
        thisBenefit = getBenefitData(benefitId);
        
        // Prepare the JSON Body Message based on the Benefit field entries
        String JSONRequest = generateRequest(thisBenefit);
        system.debug('=====JSON Request is===== ' + JSONRequest);
        
        //Invoke the API
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        if(Test.isRunningTest()) 
            request.setEndpoint('https://itapi-play.dexcom.com/ws/rest/quadax/BenefitEligibility/check');
        else
            request.setEndpoint('callout:BOOMI_QUADAX_API');
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/json;charset=UTF-8');
        request.setBody(JSONRequest);
        request.setTimeout(120000); //Setting max timeout value as we are experiencing huge delay for some payors
        HttpResponse response = http.send(request);
        System.debug(response.getBody());
        
        //Check if the HTTP Status code is not 200
        system.debug('====Response body is ====' + response.getBody());
        if (response.getStatusCode() != 200 || response.getBody() == null || response.getBody() == '') 
        {
            System.debug('The status code returned was not expected: ' +
                         response.getStatusCode() + ' ' + response.getStatus());
            //Benefit Update status
            thisBenefit.BI_Status__c = 'ERROR';
            thisBenefit.BI_Received_Date__c = System.Now();
            thisBenefit.Integration_Error_Message__c = 'Error in Accessing Quadax Resources: ' + response.getStatusCode() + ' ' + response.getStatus();
            thisBenefit.BI_Error_Msg__c = '';
            thisBenefit.Invoke_BI_Verification__c = false; 
            thisBenefit.BI_Request_Invoked_From__c = '';            
            
        } 
        else 
        {
            System.debug('Processing the response');
            //Deserialize JSON from the Response message received from Quadax 
            ClsquadaxAPIResponse quadaxAPIResponse = ClsquadaxAPIResponse.parse(response.getBody());
            String qdxReq = quadaxAPIResponse.quadaxRequest;
            String qdxRes = quadaxAPIResponse.quadaxResponse;
            
            if(quadaxAPIResponse.status == 200){
                String deductibleOOPMaxMetErrorMsg;
                Boolean updateBenefitValues = true;
                List<Benefits__c> checkPriorAuthForBIList = new List<Benefits__c>();//List to be passed to check the Prior Auth
                //Get the plantype translation
                Map<String, String> planTypeTranslationMap = getPlantypeTranslation();
                ClsquadaxAPIResponse.Result quadaxAPIResponseResult = quadaxAPIResponse.Result;
                Datetime policyEndDate = quadaxAPIResponseResult.policyEndDate;
                Integer currentYear = System.Today().Year();
                Date policyEndDateCalculated = quadaxAPIResponseResult.policyEndDate == null ? Date.newInstance(currentYear,12,31) : policyEndDate.date();
                system.debug('policyEndDate is ' + policyEndDate);
                String clientInsuranceId = removeBlank(quadaxAPIResponseResult.clientInsuranceId);
                String planTypeValue = removeBlank(quadaxAPIResponseResult.planType);
                if(planTypeValue != null && !planTypeTranslationMap.isEmpty() && planTypeTranslationMap.get(planTypeValue) != null)
                    planTypeValue = planTypeTranslationMap.get(planTypeValue);
                //For Medicare and Med Adv calculate the Individual Deductible Remaining
                Double calculatedIndDedMet = 0.0;
                if((planTypeValue == 'MEDICARE PART A B' || planTypeValue == 'MED ADV') && ignoreNull(quadaxAPIResponseResult.individualDeductible) != 0.0)
                    calculatedIndDedMet = ignoreNull(quadaxAPIResponseResult.individualDeductible) - ignoreNull(quadaxAPIResponseResult.individualDeductibleRemaining);
                else
                    calculatedIndDedMet = ignoreNull(quadaxAPIResponseResult.individualDeductibleMet);
                system.debug('calculatedIndDedMet is ' + calculatedIndDedMet);
                //Check for neagive met values
                if(calculatedIndDedMet < 0 || ignoreNull(quadaxAPIResponseResult.familyDeductibleMet) < 0)
                {
                    deductibleOOPMaxMetErrorMsg = 'Found negative amount in the deductibles met calculation : Individual Deductible Met = ' + calculatedIndDedMet + '; Family Deductible Met = ' + ignoreNull(quadaxAPIResponseResult.familyDeductibleMet);
                    updateBenefitValues = false;
                }
                if(ignoreNull(quadaxAPIResponseResult.individualOOPMaxMet) < 0 || ignoreNull(quadaxAPIResponseResult.familyOOPMaxMet) < 0)
                {
                    deductibleOOPMaxMetErrorMsg = 'Found negative amount in the oop max met calculation : Individual Deductible Met = ' + ignoreNull(quadaxAPIResponseResult.individualOOPMaxMet) + '; Family Deductible Met = ' + ignoreNull(quadaxAPIResponseResult.familyOOPMaxMet);
                    updateBenefitValues = false;
                }
                system.debug('deductibleOOPMaxMetErrorMsg is ' + deductibleOOPMaxMetErrorMsg);
                system.debug('updateBenefitValues is ' + updateBenefitValues);
                
                //Set all the Quadax field values
                if(updateBenefitValues == true)
                {
                    thisBenefit.Quadax_Response__c = (quadaxAPIResponse.quadaxResponse);
                    thisBenefit.Benefit_Status__c = 'A';
                    thisBenefit.P1_REFERENCE_NUM__c = removeBlank(quadaxAPIResponseResult.invocationReferenceNumber);
                    thisBenefit.BI_Client_Insurance_Id__c = clientInsuranceId == null ? 'L06' : clientInsuranceId;
                    thisBenefit.Coverage__c = ignoreNull(quadaxAPIResponseResult.coverage);
                    thisBenefit.CO_PAY__c = ignoreNull(quadaxAPIResponseResult.copay); 
                    thisBenefit.YEAR_END_DATE__c  = policyEndDateCalculated.year() > 4000 ? Date.newInstance(currentYear,12,31) : policyEndDateCalculated; //Noticed Quadax is sending the year as 9999, so want to set the latest possible date from Saelsforce side. Need to check the limiation on Oracle side
                    system.debug('YEAR_END_DATE__c is ' + thisBenefit.YEAR_END_DATE__c);
                    //benefitRecord.End_Date__c = date.newinstance(policyEndDate.year(), policyEndDate.month(), policyEndDate.day());
                    thisBenefit.INDIVIDUAL_DEDUCTIBLE__c = ignoreNull(quadaxAPIResponseResult.individualDeductible);
                    thisBenefit.INDIVIDUAL_MET__c = calculatedIndDedMet;
                    thisBenefit.Individual_Deductible_Remaining__c = ignoreNull(quadaxAPIResponseResult.individualDeductibleRemaining);
                    thisBenefit.INDIVIDUAL_OOP_MAX__c = ignoreNull(quadaxAPIResponseResult.individualOOPMax);
                    thisBenefit.INDIVIDUAL_OOP_MET__c = ignoreNull(quadaxAPIResponseResult.individualOOPMaxMet);
                    thisBenefit.Individual_OOP_Remaining__c = ignoreNull(quadaxAPIResponseResult.individualOOPMaxRemaining);
                    thisBenefit.FAMILY_DEDUCT__c = ignoreNull(quadaxAPIResponseResult.familyDeductible);
                    thisBenefit.Family_Met__c = ignoreNull(quadaxAPIResponseResult.familyDeductibleMet);
                    thisBenefit.Family_Deductible_Remaining__c = ignoreNull(quadaxAPIResponseResult.familyDeductibleRemaining);
                    thisBenefit.FAMILY_OOP_MAX__c = ignoreNull(quadaxAPIResponseResult.familyOOPMax);
                    thisBenefit.FAMILY_OOP_MET__c = ignoreNull(quadaxAPIResponseResult.familyOOPMaxMet);
                    thisBenefit.Family_OOP_Remaining__c = ignoreNull(quadaxAPIResponseResult.familyOOPMaxRemaining);                
                    thisBenefit.RELATIONSHIP_TO_PATIENT__c = removeBlank(quadaxAPIResponseResult.relationshiptoPatient);
                    thisBenefit.Plan_Name__c = thisBenefit.Updated_Plan_Name__c == null ? removeBlank(quadaxAPIResponseResult.planName) : thisBenefit.Updated_Plan_Name__c;
                    //update Start for CRMSF-4949
                    //if updated plan type is blank and plan type is not MED ADV and payor is not Medicare Payor then only update the plan
                    //type value on the benefit record
                    system.debug('###ClsApexConstants.PLAN_TYPE_MED_ADV.equalsIgnoreCase(planTypeValue)' + ClsApexConstants.PLAN_TYPE_MED_ADV.equalsIgnoreCase(planTypeValue));
                    if(String.isBlank(thisBenefit.Updated_Plan_Type__c)){
                        if((thisBenefit.Payor__r.Is_Medicare_Payor__c && !ClsApexConstants.PLAN_TYPE_MED_ADV.equalsIgnoreCase(planTypeValue)) || !thisBenefit.Payor__r.Is_Medicare_Payor__c){
                            thisBenefit.Plan_Type__c = planTypeValue;
                        }                        
                    }else{
                        thisBenefit.Plan_Type__c =thisBenefit.Updated_Plan_Type__c;
                    }
                    //update End for CRMSF-4949
                    //Benefit Update status
                    thisBenefit.BI_Status__c = 'SUCCESS';
                    thisBenefit.BI_Received_Date__c = System.Now();
                    thisBenefit.Last_Benefits_Check_Date__c = System.Today();
                    thisBenefit.Integration_Error_Message__c = '';
                    thisBenefit.BI_Error_Msg__c = '';
                    thisBenefit.Invoke_BI_Verification__c = false;
                    thisBenefit.BI_Request_Invoked_From__c = '';
                    //Update Prior Auth Information after the BI verification
                    checkPriorAuthForBIList.add(thisBenefit);
                    BenefitTriggerHandler.populateAuthProducts(checkPriorAuthForBIList, null); //Update the Prior Auth information----SHOULD ACTIVATE AFTER QUADAX QC HOLD ROLLOUT
                    createDxNote(thisBenefit);                    
                }
                else
                {
                    //Benefit Update status
                    thisBenefit.BI_Status__c = 'REQUIRE MCS FOLLOWUP';
                    thisBenefit.Integration_Error_Message__c = deductibleOOPMaxMetErrorMsg;
                } 
            }
            else{
                String biErrorMsg = 'ERROR : ';
                thisBenefit.BI_Received_Date__c = System.Now();
                if(quadaxAPIResponse.errors != null)
                {
                    for(ClsquadaxAPIResponse.ErrorMsg errorMessage : quadaxAPIResponse.errors)
                    {
                        if(errorMessage.rejectReasonMessage != null)
                            biErrorMsg = biErrorMsg + errorMessage.rejectReasonMessage + ';';    
                    }
                }
                else
                    biErrorMsg = 'Quadax couldn\'t process BI Check with the information provided.';
                //Benefit Update status
                thisBenefit.BI_Status__c = 'FAIL';
                thisBenefit.BI_Received_Date__c = System.Now();
                thisBenefit.Integration_Error_Message__c = '';
                thisBenefit.BI_Error_Msg__c = biErrorMsg;
            }
            
        }
        //SSIP : If Benefit invocation is invoked for SSIP 
        if(biRequestInvokedFrom == 'SSIP'){
            System.debug('******thisBenefit.BI_Status__c******** ' + thisBenefit.BI_Status__c);
            if(thisBenefit.BI_Status__c != 'SUCCESS'){ //Open Opportunity on respective SSIP Schedule
                thisBenefit.Invoke_BI_Verification__c = false;
                thisBenefit.BI_Request_Invoked_From__c = '';
                    
                insertErrorInfoOnSSIPSchedule(thisBenefit); 
            }else{ // Update respective SSIP Schedule
                completeSSIPScheduleBICheck(thisBenefit);
            }
        }else if(biRequestInvokedFrom == 'order'){
            System.debug('******thisBenefit.BI_Status__c******** ' + thisBenefit.BI_Status__c);
            if(thisBenefit.BI_Status__c != 'SUCCESS'){ //Open Opportunity on respective SSIP Schedule
                thisBenefit.Invoke_BI_Verification__c = false;
                thisBenefit.BI_Request_Invoked_From__c = '';
                //insertErrorInfoOnSSIPSchedule(thisBenefit); 

                Set<Id> benefitsUpdateTaskId = new Set<Id>();
                benefitsUpdateTaskId.add(thisBenefit.Account__c);
                //START for #CRMSF-4614
                if(benefitsUpdateTaskId.size() > 0)OrderAuditTriggerHandler.afterUpdateCreateTask( benefitsUpdateTaskId,label.Task_Description_for_any_check_fail);
                //End for #CRMSF-4614
            }          
        }
        //Perfom DML Operation in updating the Benefit with the details
        try{
            update(thisBenefit);
        }catch(Exception e){
            //Write the error log
            new ClsApexDebugLog().createLog(
                new ClsApexDebugLog.Error(
                    'ClsRequestBICheck',
                    'invokeQuadaxAPI',
                    benefitId,
                    'Benefit staus update failed: ' + e.getMessage()
                )
            );
        }   
    }
    
    //----------------------------------------HELPER FUNCTIONS--------------------------------------------------------
    
    //Dynamically Query the Benefit Object Fields
    //added is medicare payor field in query for CRMSF-4949
    public static Benefits__c getBenefitData(String benefitId){
        //Dynamically query all fields from benefit record                                    
        SObjectType objToken = Schema.getGlobalDescribe().get('Benefits__c');
        DescribeSObjectResult benefitsObjDef = objToken.getDescribe();
        Map<String, SObjectField> benefitsFields = benefitsObjDef.fields.getMap();
        
        String SOQLstr = 'SELECT ';
        for(String fieldAPIName : benefitsFields.keySet()){
            SOQLstr += String.valueOf(benefitsFields.get(fieldAPIName));
            SOQLstr += ', ';   
        } 
        //Add Relative Object Fields
        SOQLstr += 'Payor__r.Quadax_Payor_Code__c,Payor__r.Is_Medicare_Payor__c,Account__r.Name,Account__r.Territory_Code__c,Account__r.FirstName, Account__r.LastName, Account__r.DOB__c, Account__r.Gender__c,Account__r.Medicare__c';
        SOQLstr += ' from Benefits__c where ID = : benefitId limit 1';
        System.debug('SOQLstr ::'+SOQLstr);                    
        return Database.query(SOQLstr);  
        
    }
    
    //This method will verify if all required fields for Quadax API Invocation is filled in.
    //Required fields are stored in the Custom Metadata - QUADAX_API_Setting__mdt
    public static String verifyInputFields(Benefits__c benefitRecord, List<String> requiredFieldList){
        for(String fieldName: requiredFieldList){
            if(fieldName.contains('__r.')){
                List<String> subList = new List<String>();
                subList.addAll(fieldName.split('__r.'));
                system.debug('****subList is ' + subList);
                if(subList.size() == 2)
                {
                    if(String.isBlank(String.valueOf(benefitRecord.getSobject(subList[0]+'__r').get(subList[1])))){
                        String ObjectName = subList[0] == 'Payor' ? 'Account' : subList[0];
                        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(ObjectName);
                        String lbl = targetType.getDescribe().fields.getMap().get(subList[1]).getDescribe().getLabel();
                        return lbl + ' on ' + subList[0];
                    }
                }                   
            }else{
                if(String.isBlank(String.valueOf(benefitRecord.get(fieldName))))
                    return fieldName;
            }        
        }
        return null;
    }
    
    //Method will prepare the JSON body for Quadax API Call
    private static String generateRequest(Benefits__c benefitRecord){
        RequestWrapper thisWrapper = new RequestWrapper();
        thisWrapper.payerCode = benefitRecord.Quadax_Trading_Partner_Id__c;
        thisWrapper.subscriberFirstName = benefitRecord.Account__r.FirstName;
        thisWrapper.subscriberLastName = benefitRecord.Account__r.LastName;
        thisWrapper.subscriberDateOfBirth = String.valueOf(benefitRecord.Account__r.DOB__c);
        thisWrapper.subscriberGender = benefitRecord.Account__r.Gender__c;
        thisWrapper.subscriberMemberId = (benefitRecord.Quadax_Trading_Partner_Id__c == '14427' && benefitRecord.Medicare_Beneficiary_Identifier__c != null) ? benefitRecord.Medicare_Beneficiary_Identifier__c :  benefitRecord.MEMBER_ID__c;
        thisWrapper.serviceDateFrom = String.valueOf(System.Today());
        thisWrapper.mrn = benefitRecord.Id;
        thisWrapper.accountNumber = benefitRecord.Id;
        
        String JSONRequest = JSON.serialize(thisWrapper);
        return JSONRequest;
    }
    
    //Method to Check if the API returned any response value else return null value
    private static String removeBlank(String responseValue){
        if(String.isBlank(responseValue))
            return null;
        else return responseValue;
    }
    
    //Method to Check if the API returned any response value else return 0
    private static Decimal ignoreNull(Decimal responseValue){
        if(String.isBlank(String.valueOf(responseValue)))
            return 0;
        else return responseValue;  
    }
    
    //Prepare map between the Quadax plantype values and Oracle plantype values
    private static Map<String, String> getPlantypeTranslation()
    {
        Map<String, String> qdxToOraclePlanTypeMap = new Map<String, String>();
        for(QUADAX_LOV_Mapping__mdt qdxPlanTypeLOV : [SELECT MasterLabel, DeveloperName, Field_Name__c, From_Value__c, To_Value__c FROM QUADAX_LOV_Mapping__mdt WHERE Field_Name__c = 'Plan Type'])
        {
            qdxToOraclePlanTypeMap.put(qdxPlanTypeLOV.From_Value__c, qdxPlanTypeLOV.To_Value__c);
        }
        return qdxToOraclePlanTypeMap;
    }
    //Method creates DxNote upon Successful BI Transaction
    private static void createDxNote(Benefits__c benefitrecord)
    {
        DX_Notes__c dxNote = new DX_Notes__c();
        Date d = Date.today();
        String commentString = 'Note Date - ' + Date.today().format() + '\r\n' +
            'Benefit check for - ' + benefitrecord.Account__r.FirstName + ' ' + benefitRecord.Account__r.LastName + '\r\n' +
            'BI Reference # - ' + benefitrecord.P1_REFERENCE_NUM__c + '\r\n' +
            'Last Benefits Check Date - ' + benefitrecord.Last_Benefits_Check_Date__c + '\r\n' +
            'Last Benefits Check Done By - ' + benefitrecord.Last_Benefits_Check_Done_By__c + '\r\n' +
            'Coverage - ' + benefitrecord.Coverage__c + '\r\n' +
            'Co-Pay$ - ' + benefitrecord.CO_PAY__c + '\r\n' +
            'Copay Line Item - ' + benefitrecord.Copay_Line_Item__c + '\r\n' +
            'Individual Deductible - ' + benefitrecord.INDIVIDUAL_DEDUCTIBLE__c + '\r\n' +
            'Individual Deductible Met - ' + benefitrecord.INDIVIDUAL_MET__c + '\r\n' +
            'Individual Deductible Remaining - ' + benefitrecord.Individual_Deductible_Remaining__c + '\r\n' +
            'Individual OOP MAX - ' + benefitrecord.INDIVIDUAL_OOP_MAX__c + '\r\n' +
            'Individual OOP Met - ' + benefitrecord.INDIVIDUAL_OOP_MET__c + '\r\n' +
            'Individual OOP Remaining - ' + benefitrecord.Individual_OOP_Remaining__c + '\r\n' +
            'Family Deductible - ' + benefitrecord.FAMILY_DEDUCT__c + '\r\n'+
            'Family Deductible Met - ' + benefitrecord.Family_Met__c + '\r\n'+
            'Family Deductible Remaining  - ' + benefitrecord.Family_Deductible_Remaining__c + '\r\n'+
            'Family OOP MAX - ' + benefitrecord.FAMILY_OOP_MAX__c + '\r\n'+
            'Family OOP Met - ' + benefitrecord.FAMILY_OOP_MET__c + '\r\n'+
            'Family OOP Remaining - ' + benefitrecord.Family_OOP_Remaining__c;
        dxNote.Comments__c = commentString;
        dxNote.Consumer__c = benefitrecord.Account__c;
        insert dxNote;
    }
    
    private static void completeSSIPScheduleBICheck(Benefits__c benefitrecord){
        List<SSIP_Schedule__c> inReviewSSIPSchedulesList = new List<SSIP_Schedule__c>();
        Set<Id> closeOptyOnSSIP = new Set<Id>();        
        System.debug('************completeSSIPScheduleBICheck ************** ');
        for(SSIP_Schedule__c thisSchedule : [SELECT Id, Opportunity__c, status__c, Eligibility_Check_Date__c,
                                            Is_BI_Active__c, Is_CMN_Active__c, Is_Credit_Card_Active__c, Is_Pre_Authorized__c
                                            FROM SSIP_Schedule__c WHERE 
                                            Account__c = : benefitrecord.Account__c and 
                                            Eligibility_Check_Date__c <=: System.Today() and Status__c = 'Eligibility In Review']){
            thisSchedule.Is_BI_Active__c = true;
            
            if(thisSchedule.Is_BI_Active__c && thisSchedule.Is_CMN_Active__c &&
                thisSchedule.Is_Credit_Card_Active__c && thisSchedule.Is_Pre_Authorized__c){
                    closeOptyOnSSIP.add(thisSchedule.Opportunity__c);
                    thisSchedule.status__c = 'Eligibility Verified';
                }
            inReviewSSIPSchedulesList.add(thisSchedule);
        }
        if(!closeOptyOnSSIP.isEmpty()){
                ClsSSIPApexUtility.closeOpportunityforSSIPSch(closeOptyOnSSIP);
        }
        if(!inReviewSSIPSchedulesList.isEmpty()){
            try{
                System.debug('************inReviewSSIPSchedulesList ************** '+ inReviewSSIPSchedulesList);
                Database.update(inReviewSSIPSchedulesList);  
            }catch(Exception e){
                //Write the error log
                new ClsApexDebugLog().createLog(
                    new ClsApexDebugLog.Error(
                        'ClsRequestBICheck',
                        'invokeQuadaxAPI',
                        benefitrecord.id,
                        'SSIP Schedule **staus** update failed: ' + e.getMessage()
                    )
                );
            }               
        }   
    }
    
    private static void completeOrderScheduleBICheck(Benefits__c benefitrecord){
       
       List<order> availableOrderList = [select Id,Account.Name,Account.Territory_Code__c from order where AccountId =:benefitrecord.Account__c];
       if(availableOrderList.size() >0){
            Set<string> ordersId = new Set<string>();
            for(order ord : availableOrderList)ordersId.add(ord.Id);
            List<Order_Audit__c> orderAuditList = [Select Id, Audit_Area__c, Audit_Exception__c, Audit_Field__c, Audit_Verified__c, Order_Number__c,
                Audit_Exception_Added_By__c, Audit_Exception_Added_Date__c from Order_Audit__c 
                where Order_Number__c IN:ordersId and Audit_Area__c ='Benefit' and Audit_Field__c = 'Benefit - BI Verified'];
          
           for(Order_Audit__c oa : orderAuditList){
               oa.Audit_Verified__c = true;
           }
    
            if(!orderAuditList.isEmpty()){
                try{
                    System.debug('************orderAuditList ************** '+ orderAuditList);
                    Database.update(orderAuditList);  
                }catch(Exception e){
                    //Write the error log
                    new ClsApexDebugLog().createLog(
                        new ClsApexDebugLog.Error(
                            'ClsRequestBICheck',
                            'completeOrderScheduleBICheck',
                            benefitrecord.id,
                            'Order **staus** update failed: ' + e.getMessage()
                        )
                    );
                }               
            }
       } 
    }
   
    private static void insertErrorInfoOnSSIPSchedule(Benefits__c benefitrecord){
        List<SSIP_Schedule__c> inReviewSSIPScheduleList = new List<SSIP_Schedule__c>();
        List<SSIP_Schedule__c> createTaskOnSSIPSchedules = new List<SSIP_Schedule__c>();
        List<SSIP_Schedule__c> updateSSIPSchedules = new List<SSIP_Schedule__c>();
        Map<Id, Id> tagOptyToSSIPScheduleMap = new Map<Id, Id>();
        
        Map<Id, Opportunity> scheduleIdToOpptyMap = new Map<Id, Opportunity>();
        
        String errMessage = !String.IsBlank(benefitrecord.Integration_Error_Message__c) ? benefitrecord.Integration_Error_Message__c : 
                                !String.IsBlank(benefitrecord.BI_Error_Msg__c) ? benefitrecord.BI_Error_Msg__c : 'UNKNOWN ERROR';
                                
        inReviewSSIPScheduleList = [SELECT Id, Opportunity__c, Opportunity__r.isClosed, Name, Price_Book__r.Cash_Price_Book__c,
                                    Account__c,Status__c,Shipping_Address__c, Shipping_Method__c, Scheduled_Shipment_Date__c,
                                    Shipping_Address_Full_Text__c,Eligibility_Check_Date__c, 
                                    Is_BI_Active__c,Is_CMN_Active__c,Is_Credit_Card_Active__c,Is_Pre_Authorized__c,Payor__c, 
                                    SSIP_Rule__r.Product_Type__c,Account__r.Primary_Benefit__c,Account__r.Is_BI_Active__c, 
                                    Account__r.Name,Account__r.Territory_Code__c,Account__r.Credit_Card_Expiration_Date__c,
                                    Account__r.Primary_Benefit__r.Days_Since_Last_Benefit_Check__c,
                                    Account__r.Primary_Benefit__r.Quadax_Trading_Partner_Id__c
                                    FROM SSIP_Schedule__c WHERE 
                                    Account__c = : benefitrecord.Account__c and 
                                    Eligibility_Check_Date__c <=: System.Today() and Status__c = 'Eligibility In Review'];
                                    
        System.debug('************** Creating Task on Opty For SSIP Schedule ************* '+ inReviewSSIPScheduleList);                            
        //check if Opty is present, if yes, create task else create new Opty on SSIP Schedule
        if(!inReviewSSIPScheduleList.isEmpty()){
            String taskSubject = 'QUADAX ' + ClsApexConstants.SSIP_TASK_SUBJECT_FOR_QUADAX_FAIL; 
            List<ClsSSIPApexUtility.ClsOpptyTaskWrapper> opptyTaskWrapperList = ClsSSIPApexUtility.getOpptyTaskWrapperList(inReviewSSIPScheduleList,taskSubject,null,errMessage);//quadaxId is present, oppty Id is passed null
            tagOptyToSSIPScheduleMap = ClsSSIPApexUtility.createOpportunityforSSIPSch(opptyTaskWrapperList);
        }
        
        
        if(!tagOptyToSSIPScheduleMap.isEmpty()){ //create Opty on SSIP Schedule
        
        for(SSIP_Schedule__c updateSchedule : inReviewSSIPScheduleList){
            if(tagOptyToSSIPScheduleMap.containsKey(updateSchedule.Id) && 
                 String.isBlank(updateSchedule.Opportunity__c)){
                updateSchedule.Opportunity__c = tagOptyToSSIPScheduleMap.get(updateSchedule.Id); // Tag Opty to corresponding Schedule
                updateSSIPSchedules.add(updateSchedule);
            }   
        }   
               
               
        }
        
        if(!updateSSIPSchedules.isEmpty())
            try{
                Database.update(updateSSIPSchedules);
            }catch(Exception e){
                    //Write the error log
                    new ClsApexDebugLog().createLog(
                        new ClsApexDebugLog.Error(
                            'ClsRequestBICheck',
                            'invokeQuadaxAPI',
                            benefitrecord.id,
                            'SSIP Schedule **Opportunity** update failed: ' + e.getMessage()
                        )
                    );
                }
    }   

    //----------------------------------------HELPER FUNCTIONS--------------------------------------------------------
    
    //Wrapper class to prepare the request message  
    public Class RequestWrapper{
        public String payerCode;
        public String subscriberFirstName;
        public String subscriberLastName;
        public String subscriberDateOfBirth;
        public String subscriberGender;
        public String subscriberMemberId;
        public String serviceDateFrom;
        public String mrn;
        public String accountNumber;
    }
}