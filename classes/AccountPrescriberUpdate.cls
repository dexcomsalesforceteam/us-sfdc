/********************************************************************************
@author PSaini
@date June 13, 2019
@description: Apex class for consumer prescriber updates.
*******************************************************************************/

public class AccountPrescriberUpdate{
    
   //Method groups the Consumer accounts, which needs to be updated with Territory information
   public static void PrescriberAddressBeforeUpdate(List<Account> Accounts, Map <Id,Account> AccountsOldMap){
        List<Account> accountList = new List<Account>();
        Map<Id, Id> mapPrescriberLocUpd= new Map<Id, Id>();
        Map<Id, Id> mapPrescriberLocDel= new Map<Id, Id>();
        
        Set<Id> prescribers = new Set<Id>();
        Id rtConsumer= Schema.SObjectType.Account.getRecordTypeInfosByName().get('Consumers').getRecordTypeId(); 
        
        for(Account accnt: Accounts) {
            //Proceed if it is only Consumer account
            if(accnt.RecordTypeId == rtConsumer)
            {
                if(AccountsOldMap != null){
                    Account OldAccount = AccountsOldMap.get(accnt.ID);
                    // If a prescriber is updated
                    if(accnt.Prescribers__c  != null && accnt.Prescribers__c != OldAccount.Prescribers__c ){
                        accountList.add(accnt); 
                        mapPrescriberLocUpd.put(accnt.Id, accnt.Prescribers__c);
                        accnt.Location_Seen_At__c=null;
                    }
                    
                    // If a prescriber is being removed
                    if(accnt.Prescribers__c  == null && OldAccount.Prescribers__c != null ){                   
                        //mapPrescriberLocDel.put(accnt.Id, accnt.Location_Seen_At__c); 
                        accnt.Location_Seen_At__c=null;                       
                    }
                    
                    // If a prescriber is being added
                    if(accnt.Prescribers__c  != null && OldAccount.Prescribers__c == null ){                   
                        mapPrescriberLocUpd.put(accnt.Id, accnt.Prescribers__c);                        
                    }
                } else {
                    if(accnt.Prescribers__c  != null){
                        mapPrescriberLocUpd.put(accnt.Id, accnt.Prescribers__c);
                    }
                
                }
                
            }   
        }
        
        Map<Id, Id> mapAddPres= new Map<Id, Id>();
        system.debug('**** TPS: mapPrescriberLocUpd: ' + mapPrescriberLocUpd);
        if(mapPrescriberLocUpd.size()>0){
            for(Address__c add : [Select Id, Name, Primary_Flag__c, Account__c from Address__c Where Account__c IN :mapPrescriberLocUpd.Values() AND Address_Type__c='SHIP_TO' AND Primary_Flag__c =true ]){
                mapAddPres.put(add.Account__c, add.Id);            
            } 
        }
        
        for(Account accnt: Accounts) {
            if(mapPrescriberLocUpd.containsKey(accnt.Id)){
                if(mapAddPres.containsKey(mapPrescriberLocUpd.get(accnt.Id))){
                    accnt.Location_Seen_At__c=mapAddPres.get(mapPrescriberLocUpd.get(accnt.Id));
                } else{
                    accnt.Location_Seen_At__c=null;
                }  
            }
        }
        
    }
    
}