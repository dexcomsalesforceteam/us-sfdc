@isTest
Private class CreateSSIPControllerExtensionTest {
    
    
   public static list <Product2> getProductList(integer recLimit, String ProductCode){
        list <Product2> prodList =  new list <Product2>();
        for(Integer i = 0 ; i < recLimit ; i++){
            Product2 prod = new Product2(
                Name = ProductCode + i,
                ProductCode = ProductCode + i,
                Oracle_Product_Id__c = (i+1) + '44995'
            );
            prodList.add(prod);
        }
        return prodList;
    }
    public static  Asset getAsset(String name, Id prodId, Id accId){
        Asset ass = new Asset(
            Name = name,
            Product2Id = prodId,
            AccountId = accId
        );
        return ass;
    }
    
    @testSetup static void testData() {
        G5_Product_Code__c g5ProdCodeCS = new G5_Product_Code__c(
            Product_Code__c = 'BUN-GF-003'
        );
        insert g5ProdCodeCS;
        
        list <G5_Transmitter_Oracle_Prod_Id__c> G5Transmitter = new list <G5_Transmitter_Oracle_Prod_Id__c>();
        for(Integer i = 0; i < 5 ; i++){
            G5_Transmitter_Oracle_Prod_Id__c g5 = new G5_Transmitter_Oracle_Prod_Id__c(
                Name = 'G5'+i
            );
            G5Transmitter.add(g5);
        }
        G5Transmitter[0].Name = '144995';
        insert G5Transmitter;
        
        List<Account> prescriberAccList = new List<Account>{new Account(
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prescriber').getRecordTypeId(),
            FirstName = 'APTestfirstname',
            LastName = 'APTestlastname',
            Middle_Name__c = 'Test',
            PersonEmail = 'createSSIPControllerTest@gmail.com',
            Phone = 'Call: 1234567890012345678900',
            BillingState = 'OH',
            BillingCity = 'APTest City',
            BillingStreet = 'APTest Street',
            ShippingPostalCode =  '95112',
            BillingCountry = 'United States',
            Prescriber__c = null,
            IMS_ID__pc = '109',
            Party_ID__c = '111',
            CCV_ID__c = '222'  )}; 
                insert prescriberAccList;
        system.debug('prescriberAccList:::::: after insert::::'+prescriberAccList);
        system.debug('record type prescriber:::::'+prescriberAccList[0].recordTypeId);
        
        Id rTypeId = ClsApexUtility.getRecordTypeIdByDeveloperName('Account', 'Consumers');
        list <Account> testAccList = TestDataBuilder.getAccountListConsumer(10, rTypeId);
        for (Account a: testAccList){
            a.Default_Price_Book__c = Test.getstandardpricebookid();
            a.Prescribers__c = prescriberAccList[0].Id;
        }
        Test.startTest();
        insert testAccList;
        system.debug('testAccList:::::'+testAccList[0]);
        
        list <Product2> testProdList = getProductList(10, 'G5');
        testProdList[0].Name = 'BUN-GF-003';
        testProdList[0].ProductCode = 'BUN-GF-003';
        insert testProdList;
        
        list <Asset> testAssetList = new list<Asset>();
        for(integer i = 0; i < 10 ; i ++){
            Asset ass = getAsset('Asset'+1, testProdList[i].Id, testAccList[0].Id);
            testAssetList.add(ass);
        }
        insert testAssetList;
        
        Account acc = [SELECT Id FROM Account WHERE LastName = 'Test Account Name0' limit 1];
        Product2 p = [SELECT id FROM Product2  WHERE Oracle_Product_Id__c = '144995' limit 1];
        
        Scheduled_Shipments__c ss =  new Scheduled_Shipments__c(
            Name = 'SS 1',
            Account__c = acc.Id,
            Product_Item__c = p.Id,
            Oracle_Update_Status__c = 'In Progress',
            Quantity__c = 1.0,
            Frequency__c = '28',
            Start_Date__c = date.today(),
            First_Shipment_Date__c =  date.today()
        );
        
        insert ss;
        
        Address__C a = new Address__C (
            Oracle_Address_ID__c = '12345' ,
            Street_Address_1__c = 'Test St',
            City__c = 'TestCity' ,
            State__c ='CA',
            Zip_Postal_Code__c = '92123' ,
            County__c = 'United States',
            Account__c = acc.Id
        );
        insert a;
        ID addID = a.Id;
        Test.stopTest();
        Scheduled_Shipments__c ss2 =  new Scheduled_Shipments__c(
            Name = 'SS 1',
            Account__c = acc.Id,
            Product_Item__c = p.Id
        );
    }
   
    @isTest
    static void TestActiveCheck(){
        
        Account acc = [SELECT Id,Default_Price_Book__c, Prescribers__c FROM Account WHERE LastName = 'Test Account Name0' limit 1];
        system.debug('acc::::'+acc);
        acc.Default_Price_Book__c = Test.getStandardPricebookId();
        update acc;
        system.debug('account after update::::'+acc);
        Product2 p = [SELECT id FROM Product2  WHERE Oracle_Product_Id__c = '144995' limit 1];
        //    Product2 p2 = [SELECT id FROM Product2  WHERE Oracle_Product_Id__c = '48884' limit 1];
        List<Scheduled_Shipments__c> scheduleShipmentList = new List<Scheduled_Shipments__c>();
        Scheduled_Shipments__c ss =  new Scheduled_Shipments__c(
            Name = 'SS 1',
            Account__c = acc.Id,
            Product_Item__c = p.Id,
            Oracle_Update_Status__c = 'In Progress',
            Quantity__c = 1.0,
            Frequency__c = '28',
            Start_Date__c = date.today(),
            First_Shipment_Date__c =  date.today()
        );
        
        //insert ss;
        scheduleShipmentList.add(ss);
        
        Scheduled_Shipments__c ss2 =  new Scheduled_Shipments__c(
            Name = 'SS 1',
            Account__c = acc.Id,
            Product_Item__c = p.Id,
            //  Product_Item__c = '01t40000003WmNO' , 
            Oracle_Update_Status__c = 'In Progress',
            Quantity__c = 2.0,
            Frequency__c = '28',
            Start_Date__c = date.today(),
            End_Date__c = date.today(),
            First_Shipment_Date__c =  date.today()
        );
        
        //insert ss2;
        scheduleShipmentList.add(ss2);
        
        Scheduled_Shipments__c ss3 =  new Scheduled_Shipments__c(
            Name = 'SS 3',
            Account__c = acc.Id,
            Product_Item__c = p.Id,
            //  Product_Item__c = '01t40000003WmNO' , 
            Oracle_Update_Status__c = 'Error',
            Quantity__c = 1.0,
            Frequency__c = '28',
            Start_Date__c = date.today() -1,
            //     End_Date__c = date.today() - 1,
            First_Shipment_Date__c =  date.today()
        );
        
        //insert ss3;
        scheduleShipmentList.add(ss3);
        
        if(!scheduleShipmentList.isEmpty()){
            database.insert(scheduleShipmentList);
        }
        
        /**      
* This Gives errors
*  Scheduled_Shipments__c ss4 =  new Scheduled_Shipments__c(
Name = 'SS 4',
Account__c = acc.Id,
Product_Item__c = p2.Id
//      Product_Item__c = '01t3300000Ar3rg' , 
//     Oracle_Update_Status__c = 'Error',
//     Quantity__c = 1.0,
//     Frequency__c = '28',
//      Start_Date__c = date.today() -1,
//     End_Date__c = date.today() - 1,
//     First_Shipment_Date__c =  date.today()
);

insert ss4;  **/
        test.startTest();
        Address__C a = new Address__C (
            Oracle_Address_ID__c = '123459' ,
            Street_Address_1__c = 'Test 2 St',
            City__c = 'TestCity2' ,
            State__c ='CA',
            Zip_Postal_Code__c = '92123' ,
            County__c = 'United States',
            Account__c = acc.Id
        );
        insert a;
        ID addID = a.Id;
        
        PageReference pageRef = Page.CreateSSIP; 
        pageRef.getParameters().put('id',ss.id);
        Test.setCurrentPage(pageRef);
        
        
        
        system.assertEquals(ss.Frequency__c,'28');
        //   PageReference pageRef = Page.CreateSSIP;
        //   Test.setCurrentPage(pageRef);
        //   pageRef.getParameters().put('id',ss.id);
        ApexPages.StandardController sc = new ApexPages.standardController(ss);
        CreateSSIPControllerExtension  controller = new CreateSSIPControllerExtension(sc);
        
        controller.shipToaddress = '12345 - test address added ca 92121';
        controller.address = addID;
        controller.NewSave();
        controller.ActiveCheck(ss);
        controller.ActiveCheck(ss2);
        
        Controller.getAddress();
        controller.setAddress('1323 swquence sr');
        Controller.shipToAddressId = '12345 - 12345';
        controller.address = null;
        controller.NewSave();
        //     controller.IsActiveAvailable = FALSE;
        //      controller.ActiveCheck(ss3);
        //     controller.NewSave();
        //     controller.ActiveCheck(ss4);
        test.stopTest();
    }
      
    @isTest
    static void TestEditSSIP() {
        
        Account acc = [SELECT Id,Default_Price_Book__c, Prescribers__c,recordTypeId  FROM Account WHERE LastName = 'Test Account Name0' limit 1];
        system.debug('acc in test EDIT SSIP::::'+acc);
        Product2 p = [SELECT id FROM Product2  WHERE Oracle_Product_Id__c = '144995' limit 1];
        List<Account> accountListUpdate = new List<Account>();
        acc.Default_Price_Book__c = Test.getStandardPricebookId();
        update acc;
        system.debug('acc after update in test EDIT ssip::::'+acc);
        Scheduled_Shipments__c ss2 =  new Scheduled_Shipments__c(
            Name = 'SS 1',
            Account__c = acc.Id,
            Product_Item__c = p.Id,
            Oracle_Update_Status__c = 'In Progress',
            Quantity__c = 1.0,
            Frequency__c = '28',
            Start_Date__c = date.today(),
            End_Date__c = date.today() - 2,
            First_Shipment_Date__c =  date.today()
        );
        
        insert ss2;
        
        Address__C a = new Address__C (
            Oracle_Address_ID__c = '12865' ,
            Street_Address_1__c = 'SSIPCSt12',
            City__c = 'CrCity1' ,
            State__c ='CA',
            Zip_Postal_Code__c = '92123' ,
            County__c = 'United States',
            Phone_1__c = '1234567893',
            Fax__c = '123235457',
            Account__c = acc.Id
        );
        test.startTest();
        insert a;
        ID addID = a.Id;
        acc.Default_Price_Book__c = Test.getStandardPricebookId();
        update acc;
        Scheduled_Shipments__c ss =  new Scheduled_Shipments__c(
            Name = 'SS 1',
            Account__c = acc.Id,
            Product_Item__c = p.Id,
            Oracle_Update_Status__c = 'Error',
            Quantity__c = 1.0,
            Frequency__c = '28',
            Start_Date__c = date.today(),
            First_Shipment_Date__c =  date.today()
        );
        
        insert ss;
        
        
        
        
        
        PageReference pageRef = Page.EditSSIP;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',ss2.id);
        ApexPages.StandardController sc = new ApexPages.standardController(ss2);
        CreateSSIPControllerExtension  controller = new CreateSSIPControllerExtension(sc);
        
        ss2.End_Date__c = date.today() + 2;
        controller.shipToaddress = '12345 - test address added ca 92121';
        controller.address = addID;
        controller.SaveEdit();
        
        update ss2;
        
        ss2.Oracle_Update_Status__c = 'Error';
        ss2.End_Date__c = date.today() - 2;
        controller.SaveEdit();
        
        update ss2;
        
        //  ss.Oracle_Update_Status__c = 'Error';
        //   update ss;
        //   controller.SaveEdit();
        test.stopTest();
    }
    
    @isTest
    static void TestErrorSSIP() {
        
        Account acc = [SELECT Id,Default_Price_Book__c FROM Account WHERE LastName = 'Test Account Name0' limit 1];
        Product2 p = [SELECT id FROM Product2  WHERE Oracle_Product_Id__c = '144995' limit 1];
        acc.Default_Price_Book__c = Test.getStandardPricebookId();
        update acc;
        Scheduled_Shipments__c ss2 =  new Scheduled_Shipments__c(
            Name = 'SS 1',
            Account__c = acc.Id,
            //Product_Item__c = p.Id,
            Product_Item__c = '01t3300000Ar3rg',
            Oracle_Update_Status__c = 'In Progress',
            Quantity__c = 2.0,
            Frequency__c = '28',
            Start_Date__c = date.today(),
            End_Date__c = date.today() + 5,
            // First_Shipment_Date__c =  date.today()
            First_Shipment_Date__c =  null
        );
        
        insert ss2;
        
        Address__C a = new Address__C (
            Oracle_Address_ID__c = '09643' ,
            Street_Address_1__c = 'SSIP St2',
            City__c = 'ssipCIty1' ,
            State__c ='CA',
            Zip_Postal_Code__c = '92123' ,
            County__c = 'United States',
            Account__c = acc.Id
        );
        test.startTest();
        insert a;
        ID addID = a.Id;
        
        
        
        
        PageReference pageRef = Page.CreateSSIP;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',ss2.id);
        ApexPages.StandardController sc = new ApexPages.standardController(ss2);
        CreateSSIPControllerExtension  controller = new CreateSSIPControllerExtension(sc);
        controller.address = addID;
        controller.NewSave();
        controller.ActiveCheck(ss2);
        
        
        
        
        
        test.stopTest();
        
        
        
    }
    
    /**
    static testMethod void TestDisplayError() {
        
        Account acc = [SELECT Id FROM Account WHERE LastName = 'Test Account Name0' limit 1];
        Product2 p = [SELECT id FROM Product2  WHERE Oracle_Product_Id__c = '144995' limit 1];
      //  Product2 p2 = [SELECT id FROM Product2  WHERE Oracle_Product_Id__c = '48884' limit 1];
        
        Scheduled_Shipments__c ss =  new Scheduled_Shipments__c(
            Name = 'SS 1',
            Account__c = acc.Id,
            Product_Item__c = '01t40000003WmNO',
            Oracle_Update_Status__c = 'Error',
            Quantity__c = 0,
            Frequency__c = '28',
     //       Start_Date__c = date.today(),
            Start_Date__c = null,
            First_Shipment_Date__c =  date.today()
            );
        
         insert ss;
         Address__C a = new Address__C (
            Oracle_Address_ID__c = '12345' ,
            Street_Address_1__c = 'Test St',
            City__c = 'TestCity' ,
            State__c ='CA',
            Zip_Postal_Code__c = '92123' ,
            County__c = 'United States'
            );
        insert a;
        ID addID = a.Id;
        
        
        
        
         test.startTest();
        
        PageReference pageRef = Page.CreateSSIP;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',ss.id);
        ApexPages.StandardController sc = new ApexPages.standardController(ss);
        CreateSSIPControllerExtension  controller = new CreateSSIPControllerExtension(sc);
       controller.address = null;
        controller.shipToaddress = '12345 - test address added ca 92121';
        controller.address = addID;
       controller.saveedit();
       
        
      
        
        
        test.stopTest();
        
        
        
        
    }
    
 **/    
    
}