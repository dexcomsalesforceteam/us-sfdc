/*
 * Test Class created to cover BclsApprovalProcessReminder batch class
 * created for 4914
*/

@isTest(seeAllData = false)
public class BclsApprovalProcessReminderTest {
    
    @testSetup
    public static void setUpTestData(){
        Id recTypePayorId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
        Id recTypeConsumerId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Consumers').getRecordTypeId();
        Id prescriberRecordTypeId = ClsApexUtility.getRecordTypeIdByDeveloperName(ClsApexConstants.ACCOUNT_API_NAME, ClsApexConstants.PRESCRIBER);
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Data Integrator'];
        User dataIntgUser = new User(Alias = 'newUser', Email='newDataIntgUser@testorg.com',
                                     EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                                     LocaleSidKey='en_US', ProfileId = p.Id,ManagerId = UserInfo.getUserId(),
                                     TimeZoneSidKey='America/Los_Angeles', UserName='newDataIntgUser@testorg.com');
        system.runAs(dataIntgUser){
            List<Account> accountList = new List<Account>();
            Account payorAccount=new Account();
            payorAccount.name = 'Test Payor';
            payorAccount.Phone = '123456789';
            payorAccount.DOB__c = Date.newInstance(1992, 04, 23);
            payorAccount.Days_For_Sensor_2_Box_Shipment__c= 2;
            payorAccount.recordtypeid= recTypePayorId;
            payorAccount.Default_Price_Book__c = Test.getStandardPricebookId();
            //insert payorAccount;
            accountList.add(payorAccount);
            
            Account prescriberAccount = new Account();
            prescriberAccount.RecordTypeId = prescriberRecordTypeId;
            prescriberAccount.FirstName = 'ApprovalFirst';
            prescriberAccount.LastName = 'ApprovalLast';
            prescriberAccount.Inactive__c =ClsApexConstants.BOOLEAN_FALSE;
            accountList.add(prescriberAccount);
            
            insert accountList;
            
            Account testAccount=TestDataBuilder.testAccount();
            testAccount.Payor__c = payorAccount.id;
            testAccount.Customer_Type__c = 'Commercial';
            testAccount.PersonEmail = 'bclsCreateControllerSSIP@mail.com';
            testAccount.Default_Price_Book__c = Test.getStandardPricebookId();
            testAccount.recordtypeid = recTypeConsumerId;    
            testAccount.Prescribers__c = prescriberAccount.Id;
            testAccount.BillingStreet = '123 ABC St';
            testAccount.BillingCity ='Topeka';
            testAccount.BillingState ='KS';
            testAccount.BillingCountry = 'US';
            testAccount.BillingPostalCode = '12345';
            testAccount.ShippingStreet = '123 ABC St';
            testAccount.ShippingCity = 'Topeka';
            testAccount.ShippingState = 'KS';
            testAccount.ShippingPostalCode = '12345';
            testAccount.ShippingCountry = 'US';
            insert testAccount;
            
            Order_Header__c orderHeader=new Order_Header__c();
            orderHeader.Account__c =testAccount.id;
            orderHeader.Order_Type__c = 'Return & Credit Order';
            orderHeader.Product_Type__c = 'Trasnmitter';
            orderHeader.RGA_Approval_Status__c = 'Hardware Approval Required';
            orderHeader.RGA_Return_Reason__c = '30-Day Guarantee';
            orderHeader.Reason_For_RGA__c = 'Duplicate Order';
            insert orderHeader;
            
            test.startTest();
            List<Address__c> ShipToAddressList = TestDataBuilder.getAddressList(testAccount.id,true,'SHIP_TO',1);
            insert ShipToAddressList;
            
            Comp_Product_Request__c comp1 = new Comp_Product_Request__c(Customer_Account_No__c = testAccount.id,
                                                                        G5_Receiver__c = '1',
                                                                        G4_Receiver__c ='1',
                                                                        Error_Source__c = 'Distributors',
                                                                        Fedex_Reasons__c = 'other',
                                                                        Shipping_Address__c = ShipToAddressList[0].Id,
                                                                        Fedex_Notes__c = 'Failed Shipment1',
                                                                        Shipping_Method__c = '000001_FEDEX_L_GND',
                                                                        Repmaking_Request__c = UserInfo.getUserId(),
                                                                        Distributor_CMN_Active__c = true,
                                                                        Estimated_Amount__c = 4042);
            insert comp1;
            Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitRequest();
            app.setObjectId(comp1.id);
            Approval.ProcessResult result = Approval.process(app);
            test.stopTest();
        }        
    }
    
    
    @isTest
    public static void testBclsApprovalProcessReminder1(){
        test.startTest();
        try{
            database.executeBatch(new BclsApprovalProcessReminder(), 200);
        }catch(Exception e){
            
        }
        test.stopTest();
    }
    
    @isTest
    public static void testBclsApprovalProcessReminderSched(){
        Test.startTest();
        BclsApprovalProcessRemiderSchd scheduler = new BclsApprovalProcessRemiderSchd();
        String sch = '0 0 23 * * ?';
        system.schedule('BclsApprovalProcessReminder'+String.valueOf(system.now()), sch, scheduler);
        Test.stopTest();
    }

}