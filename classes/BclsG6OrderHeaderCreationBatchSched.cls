//Scheduling Class Creates G6 Orders for upgrades 
// Added by Anuj Patel - 05/25/2018
global class BclsG6OrderHeaderCreationBatchSched implements Schedulable {
    // Execute at regular intervals
    global void execute(SchedulableContext ctx){
      BclsG6OrderHeaderCreationBatch batch = new BclsG6OrderHeaderCreationBatch();
      Database.executebatch(batch, 50);
    }
}