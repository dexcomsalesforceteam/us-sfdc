/********************************************************************************
@author Abhishek Parghi
@date 1/27/2017
@description: Controller Extension for CompProductPageOnEdit Visualforce Page.

@Modified by Jagan Periyakaruppan
@date 2/16/2017
@description: Added more fields to null out when the Error source field is changed

*******************************************************************************/
public class CompProductPageOnEditController  {
    public final Comp_Product_Request__c myobj;
    //Constructor  
    public CompProductPageOnEditcontroller(ApexPages.StandardController stdcontroller) {
       this.myobj= (Comp_Product_Request__c)stdController.getRecord();

    }
    /// Method to Change Picklist Values.
    public void changePickList(){
            //Fields for Error Source = Dexcom
            myobj.Repmaking_Request__c = null;
            myobj.Reasons__c = '';
            myobj.Comp_Request_Notes__c = '';
            myobj.Error_Made_By__c = null;
            myobj.Role__c = '';
            
            //Fields for Error Source = Dexcom
            myobj.Distributors__c = '';
            myobj.Distributors_Notes__c = '';
            myobj.Distributors_Reason__c = '';
    
            //Fields for Fedex
            myobj.Fedex_Reasons__c = '';
            myobj.Fedex_Notes__c = '';

            //Fields for Fedex
            myobj.Medicare_Reasons__c = '';

        /*      
        if(myobj.Fedex_Reasons__c== 'other' ||  myobj.Distributors__c  == 'Other'){
              myobj.Fedex_Reasons__c= '';
              myobj.Distributors__c = '';
            
        }*/
    }
    /// Method to Change Picklist Values.
    public void changeInAddress(){
    System.debug('inside >>>>>> ');
        List<Address__c> thisAddress = new List<Address__c>([SELECT ID, State__c from Address__c where Id = : myobj.Shipping_Address__c limit 1]);
         System.debug('inside thisAddress >>>>>> '+ thisAddress );
         if(!thisAddress.isEmpty() && (thisAddress[0].State__c == 'HI' || thisAddress[0].State__c == 'AK'))
         myobj.Shipping_Method__c = '000001_FEDEX_A_2D';
    }
}