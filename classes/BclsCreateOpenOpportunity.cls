/*****************************************************
* @author          : Kruti bhusan Mohanty
* @date            : SEPT 22 2017
* @description     : Batch apex to create Opportunity for 
*                    parent Account if no Open aopportunity found                  
*****************************************************/
global class BclsCreateOpenOpportunity implements Database.Batchable<sObject> {
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        ///Get all Medicare accounts and Med Advantage accounts with Payor code "K" which has active subscription and no open opportunity of type "Proactive Medicare Docs"
        String query;
        if(!test.isRunningTest()){
            query ='SELECT Id, Name,Create_Proactive_Med_Docs_Opp__c, Create_Proactive_Prior_Auth_Opp__c, Chart_Notes_Expiration_Date__c, Payor__c, Prescribers__c, Medical_Facility__c, PersonLeadSource,Territory_Code__c, Date_of_Death__c FROM Account WHERE Date_of_Death__c = null AND (Create_Proactive_Med_Docs_Opp__c = true OR Create_Proactive_Prior_Auth_Opp__c = true)' ;       
        }
        else{
            query ='SELECT Id, Name,Create_Proactive_Med_Docs_Opp__c, Create_Proactive_Prior_Auth_Opp__c, Chart_Notes_Expiration_Date__c, Payor__c, Prescribers__c, Medical_Facility__c, PersonLeadSource,Territory_Code__c FROM Account' ;       
            
        }
        return DataBase.getQueryLocator(query); 
    }
    
    global void execute(Database.BatchableContext BC, List<Account> scopeAcc) {
        
        List<Opportunity> newOppListToCreate = new List<Opportunity>();            
        Id oppRecTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('US Opportunity').getRecordTypeId();            
        
        for(account ac: scopeAcc){
            if(ac.Create_Proactive_Med_Docs_Opp__c == true){
                Opportunity proactiveMedDocsOpp = new Opportunity();
                proactiveMedDocsOpp.name = ac.name+' - '+ac.Territory_Code__c;
                proactiveMedDocsOpp.Payor__c = ac.Payor__c;
                proactiveMedDocsOpp.Prescribers__c = ac.Prescribers__c;
                proactiveMedDocsOpp.Medical_Facility__c = ac.Medical_Facility__c;
                proactiveMedDocsOpp.StageName = '4. Doc Collection';
                proactiveMedDocsOpp.AccountId = ac.id;
                proactiveMedDocsOpp.CloseDate = system.today()+10;
                proactiveMedDocsOpp.Type = 'PROACTIVE Medicare Docs';
                proactiveMedDocsOpp.LeadSource = ac.PersonLeadSource;
                proactiveMedDocsOpp.Status__c = '4.2 AS-Needs Docs';
                proactiveMedDocsOpp.RecordTypeId = oppRecTypeId;
                
                newOppListToCreate.add(proactiveMedDocsOpp);
                
                //}
            }
            else if(ac.Create_Proactive_Prior_Auth_Opp__c == true || test.isRunningTest()){
                Opportunity proactivePriorAuthOpp = new Opportunity();
                proactivePriorAuthOpp.name = ac.name+' - '+ac.Territory_Code__c;
                proactivePriorAuthOpp.Payor__c = ac.Payor__c;
                proactivePriorAuthOpp.Prescribers__c = ac.Prescribers__c;
                proactivePriorAuthOpp.Medical_Facility__c = ac.Medical_Facility__c;
                proactivePriorAuthOpp.StageName = '4. Doc Collection';
                proactivePriorAuthOpp.AccountId = ac.id;
                proactivePriorAuthOpp.Type = 'PROACTIVE Prior Auth';
                proactivePriorAuthOpp.CloseDate = system.today()+12;
                proactivePriorAuthOpp.Status__c = '4.6 Doc Collection Completed';
                proactivePriorAuthOpp.RecordTypeId = oppRecTypeId;
                
                newOppListToCreate.add(proactivePriorAuthOpp);
                
            }
            
        } // ending For Loop
        
        system.debug('***List***'+newOppListToCreate);
        //This static variable is required to avoid another batch class (ProcessMultipleOpportunity) from Opp trigger after insert
        ClsApexUtility.skipExecutionInOpptyTrigger = true;
        
        Database.insert (newOppListToCreate);
        
    } // ending Execute Method
    
    global void finish(Database.BatchableContext BC){}
    
}