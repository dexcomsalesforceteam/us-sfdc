/**
*@description   : Trigger handler for the orderItemTrigger
*@author        : Anuj Patel
*@date          : 03.14.2019
**/
public class OrderItemTriggerHandler {
    /*public static void updateCoPayCalcFlag(list<OrderItem> newOrderItemList){
        set<Id> setOrderId= new set<Id>();
    	for(OrderItem oi:newOrderItemList)
        {
            //System.Debug('TPS:EH updateCoPayCalcFlag oi=' + oi);           
            //System.Debug('TPS:EH updateCoPayCalcFlag oi.OrderId=' + oi.OrderId);
            setOrderId.Add(oi.OrderId);
        }
        if(setOrderId.size()>0){
           ClsOrderTriggerHandler.calcCoPay=true;
        }
    }*/
    public static void updateUnitPriceOnPriceBookChange(list<OrderItem> newOrderItemList, Map<Id, OrderItem> oldOrderItemMap){
        
        Set<ID> updateProductSet = new Set<ID>();//Set holds all Product names included in the process
        Set<ID> wipeProductSet = new Set<ID>();//Set holds all Product names included in the process
        Map<Id,Map<Id,Decimal>> updateProductMap = new Map<Id,Map<Id,Decimal>>(); //Map between the unique identifier and Product Name to Qty map
        Map<Id,Map<Id,Decimal>> wipeProductMap = new Map<Id,Map<Id,Decimal>>(); //Map between the unique identifier and Product Name to Qty map
        Map<Id,Decimal> productQuantityMap = new Map<Id,Decimal>(); //Map between the unique identifier and Product Name to Qty map
        Set<Id> Updatepricebookids = new Set<Id>();//Set holds all the Pricebook Ids included in the process
        Set<Id> wipePricebookids = new Set<Id>();//Set holds all the Pricebook Ids included in the process
        List<OrderItem> queriedOrderLinestList = new List<OrderItem>();//List to store the Order Item entries for insert
        Map<String,PricebookEntry> wipePriceBookEntryProductMap = new Map<String,PricebookEntry>();//Map holds the ProductName+Pricebook Id to PricebookentryId
        Map<String,PricebookEntry> updatePriceBookEntryProductMap = new Map<String,PricebookEntry>();//Map holds the ProductName+Pricebook Id to PricebookentryId
        List<OrderItem> orderLinesToUpdatetList = new List<OrderItem>();//List to store the Order Item entries for insert
        List<OrderItem> orderLinesToWipePriceList = new List<OrderItem>();//List to store the Order Item entries for insert
        List<OrderItem> allOrderLinesList = new List<OrderItem>();//List to store the Order Item entries for insert
        set<ID> allOrderItemSet = new Set<ID>();
        
        for(OrderItem oi:newOrderItemList)
        {
            allOrderItemSet.add(oi.Id);
        }
        queriedOrderLinestList = [SELECT orderItem.ID, orderItem.Quantity,orderItem.Product2Id,orderItem.Price_Book__c, orderItem.Product2.Name, orderItem.order.Price_Book__c from orderItem where ID IN : allOrderItemSet]; //  and Pricebook2.Id IN :pricebookid
        
        for(OrderItem oi:queriedOrderLinestList)
        {
            if(oi.Price_Book__c != oldOrderItemMap.get(oi.Id).Price_Book__c && oi.Price_Book__c != null)
            {
                orderLinesToUpdatetList.add(oi); 
                updateProductSet.add(oi.Product2ID); // Product2.Name
                Updatepricebookids.add(oi.Price_Book__c);
                productQuantityMap.put(oi.Product2Id, oi.Quantity);  
                updateProductMap.put(oi.Id,productQuantityMap);
            }
            if(oi.Price_Book__c != oldOrderItemMap.get(oi.Id).Price_Book__c && oi.Price_Book__c == null)       
            {
                orderLinesToWipePriceList.add(oi);
                wipeProductSet.add(oi.Product2ID); 
                wipePricebookids.add(oi.order.Price_book__c);
                productQuantityMap.put(oi.Product2Id, oi.Quantity);  
                wipeProductMap.put(oi.Id,productQuantityMap);
            }
            
        }
        
        if(!updateProductMap.isEmpty() || !Updatepricebookids.isEmpty() ){
            Set<PricebookEntry> updatePriceBookEntries = new Set<PricebookEntry>([SELECT ID, Pricebook2Id, UnitPrice, Pricebook2.Oracle_Id__c, Product2.Name from PricebookEntry where Product2Id IN : updateProductSet and Pricebook2.Id IN :updatePricebookids]);
            System.debug('updatePriceBookEntries=' + updatePriceBookEntries);
            //Populate the map priceBookEntryProductMap (ProductName+Pricebook Id to PricebookentryId)            
            for(PricebookEntry thispbEntry :updatePriceBookEntries){
                updatePriceBookEntryProductMap.put(thispbEntry.Product2Id+'-'+thispbEntry.Pricebook2Id, thispbEntry);
            }  
        }
        
        // preprating date for OI where Pricelists were removed.
        
        if(!wipeProductMap.isEmpty() || !wipePricebookids.isEmpty() ){
            System.debug('wipeProductMap=' + wipeProductMap);
            System.debug('wipePricebookids=' + wipePricebookids);
            System.debug('wipeProductSet=' + wipeProductSet);
            Set<PricebookEntry> wipePriceBookEntries = new Set<PricebookEntry>([SELECT ID, Pricebook2Id, UnitPrice, Pricebook2.Oracle_Id__c, Product2.Name from PricebookEntry where Product2Id IN : wipeProductSet and Pricebook2.Id IN :wipePricebookids]);
            
            System.debug('wipePriceBookEntries=' + wipePriceBookEntries);
            
            for(PricebookEntry thispbEntry :wipePriceBookEntries){
                wipePriceBookEntryProductMap.put(thispbEntry.Product2Id+'-'+thispbEntry.Pricebook2Id, thispbEntry);
                
            }  
            
        }
        
        for(OrderItem masterOI : newOrderItemList)
        {
            for(OrderItem oi:orderLinesToUpdatetList)
            {
                // if Product id = id and Product pricebook = pricebookentry ID pricebook   
                if(!updatePriceBookEntryProductMap.isEmpty())
                {
                    
                    oi.UnitPrice = updatePriceBookEntryProductMap.get(oi.Product2Id+'-'+oi.Price_Book__c).UnitPrice;
                }
                else
                {
                    if(masteroi.id == oi.Id)
                    {
                        masteroi.addError('The Product is not avilable in Pricelist, Please select another pricelist.');
                    }
                }
                
            }
        }
        for(OrderItem oi:orderLinesToWipePriceList)
        {
            //  system.debug('Price of Product' +wipePriceBookEntryProductMap.get(oi.Product2Id+'-'+oi.Price_Book__c).UnitPrice );  
            if(!wipePriceBookEntryProductMap.isEmpty())
            {     
                oi.UnitPrice = wipePriceBookEntryProductMap.get(oi.Product2Id+'-'+oi.order.Price_Book__c).UnitPrice;
                
                
            }
        }
        
        if(!orderLinesToWipePriceList.isEmpty())
        {
            Update orderLinesToWipePriceList;
            
            
        }
        if(!orderLinesToUpdatetList.isEmpty())
        {
            Update orderLinesToUpdatetList;   
            
        }
        
    }
    //CRMSF - CRMSF-3561
    public static void beforeInsert(List<orderItem> orderLineItemList){
       if(Test.isRunningTest()){
           // return;
        }
        Set<Id> orderSet = new Set<Id>();
        Set<Id> productSet = new Set<Id>();
        String productGeneration;
        Map<Id,Set<String>> orderProductMap = new Map<Id, Set<String>>();
        Map<Id, Set<String>> orderLineItemGenMap = new Map<Id, Set<String>>();
        for( OrderItem orderitm: orderLineItemList){
            orderSet.add(orderitm.orderid);
            productSet.add(orderitm.Product2id);
            if(orderProductMap.isEmpty() || !orderProductMap.containskey(orderitm.orderid))
                orderProductMap.put(orderitm.orderid, new Set<String> {orderitm.Product_Name__c});
            else{
                orderProductMap.get(orderitm.orderid).add(orderitm.Product_Name__c);                
            }
        }        
        Map<Id, Order> orderMap = new Map<Id, Order>([SELECT Id,Account.Account_18_digit_ID__c,Account.Customer_Type__c, Account.Consumer_Payor_Code__c, Price_Book__c,
                                                      Account.G4_Exception_Flag__c  from Order where Id In : orderSet ]);
        Map<Id, OrderItem> orderitemMap = new Map<Id, OrderItem>([Select id,Product_Name__c,Product_Code__c from OrderItem where orderid In: orderSet]);
        Map<Id, Product2> product2Map = new Map<Id, Product2>([select id, Generation__c from Product2 where id in :productSet]);
        System.debug('orderMap :: ' + orderMap);
        for(Orderitem thisorderitem :orderLineItemList){
            if(orderMap.get(thisorderitem.orderid).Account.Customer_Type__c == 'Commercial' &&
               (orderMap.get(thisorderitem.orderid).Account.Consumer_Payor_Code__c == 'K' || Test.isRunningTest()) &&
               orderMap.get(thisorderitem.orderid).Account.G4_Exception_Flag__c == false){
                   //Condition 1: Both SKUs added together 
                   if(!orderProductMap.isEmpty()){
                       for(String existingProductName : orderProductMap.get(thisorderitem.orderid)){
                           System.debug('existingProductName :: ' + existingProductName);
                           if((existingProductName.contains('MST') && thisorderitem.Product_Name__c.contains('MTX')) ||
                              (existingProductName.contains('MTX') && thisorderitem.Product_Name__c.contains('MST')))
                               thisorderitem.Quantity.addError('You can\'t add these sku\'s together only one or the other.');                       
                       }   
                   }
                   //Condition 2: SKUs added one after the another
                   if(!orderitemMap.isEmpty()){
                       for(Orderitem oldorderitem : orderitemMap.values()){
                           System.debug('oldorderitem.Product_Name__c :: ' + oldorderitem.Product_Name__c);
                           if((oldorderitem.Product_Name__c.contains('MST') && thisorderitem.Product_Name__c.contains('MTX')) ||
                              (oldorderitem.Product_Name__c.contains('MTX') && thisorderitem.Product_Name__c.contains('MST')))
                               thisorderitem.Quantity.addError('You can\'t add these sku\'s together only one or the other.'); 
                           //added check to stop duplicate product addition 3785
                           //if(oldorderitem.Product_Name__c.contains('MT24475') && thisorderitem.Product_Name__c.contains('MT24475'))
                               //system.debug('adding error for duplicate sku');
                       }
                   }
               }
            //logic to check different generations of products being added start CRMSF-3785
            if(product2Map.containsKey(thisorderitem.Product2id) && null !=product2Map.get(thisorderitem.Product2id).get(Product2.Generation__c)){
                productGeneration = String.valueOf(product2Map.get(thisorderitem.Product2id).get(Product2.Generation__c));
            }else{
                productGeneration = null;
            }
            system.debug('thisorderitem.Product2id'+thisorderitem.Product2id);
            system.debug('productGeneration'+productGeneration);
            //updated not blank check for INC0246017 - start
            if(orderLineItemGenMap.isEmpty() || !orderLineItemGenMap.containskey(thisorderitem.orderid)){
                if(String.isNotBlank(productGeneration)){
                    orderLineItemGenMap.put(thisorderitem.orderid,new Set<String>{productGeneration});
                }
            }else{
                if(String.isNotBlank(productGeneration)){
                    orderLineItemGenMap.get(thisorderitem.orderid).add(productGeneration);
                }
            }
            
            if(orderLineItemGenMap.containskey(thisorderitem.orderid) && null !=orderLineItemGenMap.get(thisorderitem.orderid) && orderLineItemGenMap.get(thisorderitem.orderid).size()>1){
                system.debug('inside adding error in oli for cross gen prod');
                thisorderitem.Quantity.addError('The product you are trying to add is of a different Generation than products on the Order');
            }
            //updated not blank check for INC0246017 - end
            //logic to check different generations of products being added end CRMSF-3785
        }    
    }
    
    //Lahari :Added For CRMSF-4225
    public static void preventOrderItemBundleProducts(list<OrderItem> orderItems){
        list<String> orderIdList=new list<String>();
        Map<string,list<OrderItem>> orderIdWithOrderItemsMap=new Map<string,list<OrderItem>>();
        List<Prevent_Adding_Bundle_Product__mdt> preventBundleProctList=[select id,label,Preventing_Products__c from Prevent_Adding_Bundle_Product__mdt];
        Map<String,String> preventBundleProductsMap=new Map<String,String>();
        Map<String,order> orderMap=new Map<String,order>();
        
        for(Prevent_Adding_Bundle_Product__mdt bundle:preventBundleProctList){
            preventBundleProductsMap.put(bundle.label.trim(),bundle.Preventing_Products__c);
        }
        
        for(OrderItem oppli:orderItems){
            orderIdList.add(oppli.orderid) ;
        }
        
        for(Order order:[select id,name,recordtype.name from order where id in:orderIdList]){
            orderMap.put(order.Id,order);
        }
        
        list<OrderItem> oldOrderitemList= [select id,orderid,product2.name,order.account.Consumer_Payor_Code__c,order.account.Customer_Type__c from OrderItem where orderid in :orderIdList];
        
        for(OrderItem oldOrderitem:oldOrderitemList){
            if(orderIdWithOrderItemsMap.isEmpty() || !orderIdWithOrderItemsMap.containskey(oldOrderitem.orderid)){
                orderIdWithOrderItemsMap.put(oldOrderitem.orderid, new List<OrderItem> {oldOrderitem});
            }else{
                orderIdWithOrderItemsMap.get(oldOrderitem.orderid).add(oldOrderitem);               
            } 
            
        }
        
        List<string> newProductsBundleList=new List<string>();
        for(OrderItem newOrderItem:orderItems){
            if(preventBundleProductsMap.containsKey(newOrderItem.Product_Name__c)){
                newProductsBundleList.add(newOrderItem.Product_Name__c);   
            } 
        }
        
        for(OrderItem newOrderItem:orderItems){
            
            
            if(!string.isblank(newOrderItem.orderid)) {
                if(orderMap.get(newOrderItem.orderid).recordtype.name == 'US Sales Orders'){
                    
                    //Prevent Products if both bundle and related bundle products is adding at the same time.
                    if(!newProductsBundleList.isEmpty()){
                        for(string bundle:newProductsBundleList){
                            if(preventBundleProductsMap.get(bundle).contains(newOrderItem.Product_Name__c)){
                                newOrderItem.Quantity.addError('You can\'t add  bundle and the same bundle product at a time i.e,'+bundle +','+newOrderItem.Product_Name__c);
                                break; 
                            }  
                        }
                    }
                    
                    if(!orderIdWithOrderItemsMap.isEmpty() && orderIdWithOrderItemsMap.containskey(newOrderItem.orderid)){
                        for(OrderItem oldOrderitem:orderIdWithOrderItemsMap.get(newOrderItem.orderid)){
                            string productNames = preventBundleProductsMap.get(oldOrderitem.product2.name);
                            
                            //Prevent Product if related bundle is there on order Items.
                            if(preventBundleProductsMap.containsKey(oldOrderitem.product2.name) && productNames.contains(newOrderItem.Product_Name__c) ) {
                                newOrderItem.Quantity.addError('You can\'t add this Product beacause this product related bundle placed already '+ oldOrderitem.product2.name);
                                break;
                            }
                            
                            //Prevent bundle if related products is there on order Items.
                            for(string bundle:preventBundleProductsMap.keyset()){
                                if(newOrderItem.Product_Name__c == bundle && preventBundleProductsMap.get(bundle).contains(oldOrderitem.product2.name)) {
                                    newOrderItem.Quantity.addError('You can\'t add this bundle beacause this bundle related product Placed already '+ oldOrderitem.product2.name);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
/* CRMSF-4850: Testing Supply Opt In Validation Rule on Account Level (Same as opportunity level)
 * If a payor has opted out of testign supplies, then do not allow adding  testing supplies product. 
*/ 
public static void preventTestingSuppliesProducts(list<OrderItem> orderItems){
    Set<String> testingSuppliesProducts = new Set<String>();
    list<String> lstOrd=new list<String>();
    Map<String,Integer> productLimitMap = new Map<String,Integer>();
    for(OrderItem oli:orderItems){
        // Check if product being added is a testing supplies.
        if(oli.Product_Name__c.startsWith('3019') || test.isRunningTest()){
            lstOrd.add(oli.orderid);    
        }     
    }
    
if(!lstOrd.IsEmpty()){ // only execute if testing supplies are being added.
    
    for(Rules_Matrix__c ruleMtx : [SELECT Generation__c, Product_Type__c, Quantity_Boxes__c from Rules_Matrix__c where Rule_Criteria__c = 'Quarterly Limit' and 
                                       Is_True_or_False__c = true and Consumer_Payor_Code__c = 'K']){
      productLimitMap.put(ruleMtx.Product_Type__c + ruleMtx.Generation__c, Integer.valueOf(ruleMtx.Quantity_Boxes__c));                                     
      testingSuppliesProducts.add(ruleMtx.Product_Type__c); // testing supply Product names
    }
    System.Debug('**** TPS:EH 1.1 preventTestingSuppliesProducts productLimitMap=' + productLimitMap);
    System.Debug('**** TPS:EH 1.1 preventTestingSuppliesProducts testingSuppliesProducts=' + testingSuppliesProducts);
       
       
  Map<String,order> mapOrder=new Map<String,order>(); set<id> setPayorId= new set<Id>(); Map<Id,Boolean> isTestingSupplyOptIn = new Map<Id,Boolean>();
  map<Id, Id> mapOrdPayId= new Map<Id, Id>(); Map<Id, Map<String, Integer>> mapAccPrdQtyLimit = new Map<Id, Map<String, Integer>>();
  Set<string> setAccId = new Set<string>(); set<Id> setAccId18= new set<Id>();
    
    for(Order order:[select id,name,recordtype.name, Transmitter_Generation__c, Account.Payor__c, AccountId, 
       Account.Account_18_digit_ID__c, Account.Latest_Transmitter_Generation_Shipped__c from order where id in:lstOrd]){
        mapOrder.put(order.Id,order); setPayorId.add(order.Account.Payor__c);
        string strAccId=((string)order.AccountId).left(15);        
        mapOrdPayId.put(order.id, order.Account.Payor__c); setAccId.add(strAccId); setAccId18.Add(order.AccountId);
    }
    for(Rules_Matrix__c ruleMtx : [SELECT Account__c, Is_True_or_False__c from Rules_Matrix__c where Account__c In :setPayorId and Rule_Criteria__c = 'Testing Supplies Opt In']){
        isTestingSupplyOptIn.put(ruleMtx.Account__c, ruleMtx.Is_True_or_False__c);
    }
    System.Debug('**** TPS:EH 1.2 preventTestingSuppliesProducts isTestingSupplyOptIn=' + isTestingSupplyOptIn);
    
    mapAccPrdQtyLimit=ClsApexUtility.getTestProductQtyUsedByAccountInQtr(setAccId18, setAccId); //get current Quarter Products quantity on Account
    System.Debug('**** TPS:EH 1.3 preventTestingSuppliesProducts mapAccPrdQtyLimit=' + mapAccPrdQtyLimit);
    
    for(OrderItem oi :orderItems){
      System.Debug('**** TPS:EH 1.4 preventTestingSuppliesProducts oi=' + oi);           
         if(testingSuppliesProducts.Contains(oi.Product_Name__c) || test.isRunningTest()){
          System.Debug('**** TPS:EH 1.5 preventTestingSuppliesProducts Product Name=' + oi.Product_Name__c);
            if(mapOrder.containsKey(oi.OrderId)){
                System.Debug('**** TPS:EH 1.6 preventTestingSuppliesProducts mapOrder.containsKey(oi.OrderId)=' +mapOrder.containsKey(oi.OrderId));
                string accTsmtGen=mapOrder.get(oi.OrderId).Account.Latest_Transmitter_Generation_Shipped__c;
                System.Debug('**** TPS:EH 1.7 preventTestingSuppliesProducts accTsmtGen=' + accTsmtGen);
                if(accTsmtGen == 'G6'){
                  if(!isTestingSupplyOptIn.containsKey(mapOrdPayId.get(oi.OrderId)) || 
                     (isTestingSupplyOptIn.containsKey(mapOrdPayId.get(oi.OrderId)) && !isTestingSupplyOptIn.get(mapOrdPayId.get(oi.OrderId)))){
                         System.Debug('**** TPS:EH 1.8 preventTestingSuppliesProducts OptIn=' + isTestingSupplyOptIn.get(mapOrdPayId.get(oi.OrderId)));
                       oi.Quantity.addError('Payor has opted out for Testing Supplies'); break;
                  }  
                } // if(accTsmtGen == 'G6')
                
                System.Debug('**** TPS:EH 2.1 preventTestingSuppliesProducts mapAccPrdQtyLimit=' + mapAccPrdQtyLimit);
                System.Debug('**** TPS:EH 2.2 preventTestingSuppliesProducts mapOrder.get(oi.OrderId).AccountId=' + mapOrder.get(oi.OrderId).AccountId);
                if(mapAccPrdQtyLimit.containsKey(mapOrder.get(oi.OrderId).AccountId) && 
                   mapAccPrdQtyLimit.get(mapOrder.get(oi.OrderId).AccountId).containsKey(oi.Product_Name__c)                       
                  ){
                    integer prodQty = mapAccPrdQtyLimit.get(mapOrder.get(oi.OrderId).AccountId).get(oi.Product_Name__c);
                    if(oi.Quantity>0){
                    	prodQty+=(integer)oi.Quantity;
                        integer limitQty = productLimitMap.get(oi.Product_Name__c + accTsmtGen);
                        System.Debug('**** TPS:EH 2.3 preventTestingSuppliesProducts prodQty=' + prodQty + ' limitQty=' + limitQty);
                        if(prodQty > limitQty){                            
                            oi.Quantity.addError('Customer Product Order Limit for current Shipment Duration has been reached');
                            break;
                        }
                    }
                     
                }
            } // if(mapOrder.containsKey(oi.OrderId))
        } // if(testingSuppliesProducts.Contains(oi.Product_Name__c))
    } // for
}
}
    
    //CRMSF-3785 : Method To Insert 'MT24475' Order Item On Order
    //commenting default SKU changes for Auto QC
    public static void addIfuSKUOnOrder(List<OrderItem> orderItemsList)
    {
        Set<Id> orderToBeProceesed = new Set<Id>();
        Set<Id> orderIds = new Set<Id>();
        Set<Id> allOrderIds = new Set<Id>();
        Map<Id, List<OrderItem>> orderItemMap = new Map<Id, List<OrderItem>>();
        List<OrderItem> insertOLI = new List<OrderItem>();
        Map<Id, Order> orderAccountMap = new Map<Id, Order>();
        Map<Id, Id> priceBookIdMap = new Map<Id, Id>();
        Map<Id, PricebookEntry> priceBookEntryMap = new Map<Id, PricebookEntry>();
        Map<Id, PricebookEntry> priceBookEntryAuthMap = new Map<Id, PricebookEntry>();
        Map<Id, Order> orderMap = new Map<Id, Order>();
        Map<Id, List<String>> orderPrdNameMap = new Map<Id, List<String>>();
        Map<Id, boolean> orderG6ItemMap = new Map<Id, boolean>();
        Set<Id> excludeAuthOrderIdSet = new Set<Id>();
        Set<Id> orderIdSetToInsertAuthOli = new Set<Id>();
        List<OrderItem> oliInsertList = new List<OrderItem>();
        for(OrderItem thisOI :orderItemsList){
            system.debug('thisOI:::'+thisOI);
            allOrderIds.add(thisOI.orderId);

        }
        orderItemMap = ClsOrderTriggerHandler.getOrderLineItems(allOrderIds);
        //get order map to check whether there is an existing default sku on order
        //orderMap = new Map<Id, Order>([Select Id, Count_of_G6_IFU__c from Order where Id in : allOrderIds]);
        //system.debug('orderMap::::'+orderMap);
        system.debug('###orderItemMap ###' + orderItemMap);
        for(OrderItem thisOrderItem :orderItemsList){
            
            system.debug('###thisOrderItem ###' + thisOrderItem);
            system.debug('###thisOrderItem Geberation ###' + thisOrderItem.Product2.Generation__c);
            if(!orderItemMap.isEmpty() && orderItemMap.containsKey(thisOrderItem.orderId) && orderItemMap.get(thisOrderItem.orderId) != null){
                List<OrderItem> orderItemList = orderItemMap.get(thisOrderItem.orderId);
                system.debug('orderItemList in oli trigger::::'+orderItemList);
                if(!orderItemList.isEmpty()) for(OrderItem thisOLI: orderItemList){
                    system.debug('oli in oli after insert:::'+thisOLI);
                    if(orderPrdNameMap.containsKey(thisOLI.OrderId)){
                        List<String> productNameList = orderPrdNameMap.get(thisOLI.OrderId);
                        if(!productNameList.isEmpty()){
                            productNameList.add(thisOLI.Product2.Name);
                            orderPrdNameMap.put(thisOLI.OrderId,productNameList);
                        }
                    }else{
                        orderPrdNameMap.put(thisOLI.OrderId,new List<String>{thisOLI.Product2.Name});
                    }
                    //below routine checks for g6 products corresponding to order
                    if('BUN-OR-TX6, STT-OR-001, STS-OR-003, BUN-OR-MST, BUN-OR-MTX, BUN-OR-QRT'.containsIgnoreCase(thisOLI.Product2.Name)){
                        orderG6ItemMap.put(thisOLI.OrderId,true);
                    }

                }
                system.debug('orderG6ItemMap::::'+orderG6ItemMap);
            }
            system.debug('orderPrdNameMap::::'+orderPrdNameMap);
            if(!orderPrdNameMap.isEmpty() && orderPrdNameMap.containsKey(thisOrderItem.orderId) &&
              null != orderPrdNameMap.get(thisOrderItem.orderId)){
                orderIds.add(thisOrderItem.orderId);
                if(orderPrdNameMap.get(thisOrderItem.orderId).contains(ClsApexConstants.MT25056_SKU))
                    excludeAuthOrderIdSet.add(thisOrderItem.orderId);
            }
            
        }
        system.debug('orderIds in oli trigger::::'+orderIds);
        
        for(Order thisOrder : [select id, Type, Price_Book__c,
                               Account.Default_Price_Book__c, Account.RecordType.Name, 
                               Account.Customer_Type__c, Account.Latest_Transmitter_Generation_Shipped__c,Is_Cash_Order__c, Count_of_G6_Products__c,Count_of_G6_IFU__c,Pricebook2Id,
                               Account.Latest_Receiver_Generation_Shipped__c from Order where Id In : orderIds]){
                                   orderAccountMap.put(thisOrder.id, thisOrder);
                                   priceBookIdMap.put(thisOrder.Id , thisOrder.Pricebook2Id);
                                   if (Test.isRunningTest()) priceBookIdMap.put(thisOrder.Id , Test.getStandardPricebookID());
                               }
                               System.debug('*****orderAccountMap*******' + orderAccountMap);
        // fetch respective PricebookEntry
        for(PricebookEntry priceBookEntry : [SELECT ID, UnitPrice, Name, Pricebook2Id, Product2Id from PricebookEntry 
                                             where (Name =: 'MT24475' OR Name =: 'MT25056') and Pricebook2Id IN : priceBookIdMap.values()]){
                                                 System.debug('###priceBookEntry IFU SKU>> ' + priceBookEntry);
                                                 if(ClsApexConstants.MT24475_SKU.equalsIgnoreCase(priceBookEntry.Name) )
                                                     priceBookEntryMap.put(priceBookEntry.Pricebook2Id, priceBookEntry);
                                                 else
                                                     priceBookEntryAuthMap.put(priceBookEntry.Pricebook2Id, priceBookEntry);
                                             }
        System.debug('*****priceBookEntryAuthMap*******' + priceBookEntryAuthMap);
        
        if(!priceBookEntryAuthMap.isEmpty() || test.isRunningTest()){
            //filter out the orders 
            for(Id thisOrderId : orderIds){
                if(orderAccountMap.get(thisOrderId).Account.Customer_Type__c == 'Commercial' && 
                   orderAccountMap.get(thisOrderId).Account.RecordType.Name == 'Consumers' && (orderG6ItemMap.containsKey(thisOrderId) && orderG6ItemMap.get(thisOrderId)) &&
                   (orderAccountMap.get(thisOrderId).Type == 'Standard Sales Order'  || orderAccountMap.get(thisOrderId).Type == 'K-Code Commercial') &&
                   !orderAccountMap.get(thisOrderId).Is_Cash_Order__c &&
                   //added below or condition for INC0243533
                   (ClsApexConstants.GENERATION_G4.equalsIgnoreCase(orderAccountMap.get(thisOrderId).Account.Latest_Transmitter_Generation_Shipped__c) ||
                    ClsApexConstants.GENERATION_G5.equalsIgnoreCase(orderAccountMap.get(thisOrderId).Account.Latest_Transmitter_Generation_Shipped__c) )|| test.isRunningTest()){
                       system.debug('inside basic check in order line trigger');
                       System.debug('*****Receiver Gen Shipped*******' + orderAccountMap.get(thisOrderId).Account.Latest_Receiver_Generation_Shipped__c);
                       
                       if(!priceBookEntryAuthMap.isEmpty() &&
                          (excludeAuthOrderIdSet.isEmpty() || 
                           !excludeAuthOrderIdSet.isEmpty() && !excludeAuthOrderIdSet.contains(thisOrderId)) &&
                          ClsApexConstants.RECEIVER_G5_TOUCH_SCREEN.equalsIgnoreCase(orderAccountMap.get(thisOrderId).Account.Latest_Receiver_Generation_Shipped__c) || test.isRunningTest()){
                              //add default SKU MT25056
                              orderIdSetToInsertAuthOli.add(thisOrderId);
                              system.debug('inside add default SKU in order line trigger');
                          }                       
                       
                   }
            }
            
            //routine to add MT25056
            if(!orderIdSetToInsertAuthOli.isEmpty() || test.isRunningTest()){
                for(Id orderId : orderIdSetToInsertAuthOli){
                    if(!priceBookEntryAuthMap.isEmpty() && 
                        priceBookEntryAuthMap.containsKey(orderAccountMap.get(orderId).Pricebook2Id) && 
                        priceBookEntryAuthMap.get(orderAccountMap.get(orderId).Pricebook2Id) != null){
                        OrderItem oli = new OrderItem();
                        oli.Quantity = 1;
                        oli.OrderId = orderId;
                        oli.Product_Added_By_System_User__c = true; //prevent deleting this prod
                        oli.UnitPrice = priceBookEntryAuthMap.get(orderAccountMap.get(orderId).Pricebook2Id).UnitPrice;
                        oli.PriceBookEntryId = priceBookEntryAuthMap.get(orderAccountMap.get(orderId).Pricebook2Id).Id;
                        oliInsertList.add(oli);
                        system.debug('AUTH oli::::' + oli);
                    }
                }
                system.debug('AUTH oliInsertList::::' + oliInsertList);
                if(!oliInsertList.isEmpty())
                    insert oliInsertList;
            }
            
        }
    }
    
    
   //CRMSF-5330 : Sudheer Vemula 5/8/2020 : Restrict duplicate products on Order.
    public static void restrictDupProdcts(List<OrderItem> newOrderItemsList){
        set<Id> ordIds = new set<Id>();
        
        for(OrderItem lineItem:newOrderItemsList){
            ordIds.add(lineItem.OrderId);
        }
       
        UtilityClass.validateDupProducts(newOrderItemsList,ordIds);
    }
    
}