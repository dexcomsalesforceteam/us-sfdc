/*
  @Author        : Anuj Patel
  @Date Created    : 30-April-2018
  @Description    : Makes a callout from a button, also invoked by the clsOrderTriggerHandler class. 
*/

global class ClsG6UpgradeCallout {
     
    @Future(callout=true)
  	webservice static void sendNotification(List<ID> accList) {
       //Account acc = [SELECT ID,firstname, lastname, Address_Line_1__c,Address_Line_2__c,Address_Line_3__c,ShippingCity,ShippingPostalCode,ShippingState  FROM ACCOUNT WHERE Id =: AccountID ];    
        system.debug('this is calling the callout man');
        List<Account> callOutAcc = new List<Account>();
        callOutAcc = [SELECT ID,firstname, lastname, ShippingStreet,Address_Line_2__c,Address_Line_3__c,ShippingCity,ShippingPostalCode,ShippingState  FROM ACCOUNT WHERE Id In: accList ];    
		
        
        
      For(Account acc:callOutAcc)  
      {
          system.debug('Addresses' +acc.ShippingStreet);
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
	//	req.setEndpoint('https://test-api.mypropago.com/services/v1/company/mailingrequest/create'); 
        req.setEndpoint('callout:G6ReceiverAuth');
        req.setMethod('POST');
        String sku = 'DX-AUTHBUNDLE-00'; 
        Integer qty = 1;
        Integer passID = 0;
        Date todayDate = system.today();
        String Status =  'Activated';
        String reqBody =   '[{"Id": "' + passID + 
                                 '","FirstName" : "' + acc.FirstName + 
                                 '","LastName": "' + acc.LastName + 
                                 '","Address1": "' + acc.ShippingStreet + 
                                 '","Address2":"' + acc.Address_Line_2__c + 
                                 '","Address3":"' + acc.Address_Line_3__c +
                                 '","City":"' + acc.ShippingCity + 
            					 '","ZipCode":"' + acc.ShippingPostalCode + 
          						 '","State":"' + acc.ShippingState +
            					 '","Quantity":"' + qty +
             					 '","Status":"' + Status +
            					 '","DateCreated":"' + todayDate +
            					 '","Sku":"' + sku +	'"}]';   
          req.setBody(reqBody);
          req.setHeader('Content-Type', 'application/json');
   //     req.setCompressed(true); 
   		
  //      req.setHeader('Host', 'test-api.mypropago.com'); // api.mypropago.com
 //      String username = 'Dexcomm';
  //      String password = '0281be6c-f680-4e96-b823-2ee478b4a8f4';
  //      Blob headerValue = Blob.valueOf(username + ':' + password);
  //      String authorizationHeader = 'Basic ' +
   //         EncodingUtil.base64Encode(headerValue);
   //     req.setHeader('Authorization', authorizationHeader);
         
        try {
             System.debug('About to send the request ');
            res = http.send(req);  
            System.debug(res.getBody());
            if (res.getStatusCode() == 200) {
            
            // deserialize the response
            Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
            String rowNumberFromResponse = String.valueOf(m.get('Data')) ;
            Pattern nonNumeric = Pattern.compile('[^0-9]');
            Matcher matcher = nonNumeric.matcher(rowNumberFromResponse);
            String rowNumber = matcher.replaceAll('');
            system.debug('Value of the rownumber is ' + rowNumber);    
                
                
                
            // we can create a task here.
            String userId = UserInfo.getUserId(); 
          	Task t = new Task();
                t.OwnerId = userId;
                t.Subject ='Receiver AuthCode Process Initiated and Raw Number = ' + rowNumber;
                t.Status = 'Open';
                t.Priority = 'Normal';
                t.WhatId = acc.Id;
                insert t;
               	} 
             }
        
            catch(System.CalloutException e) {
            System.debug('Callout error: '+ e);
            System.debug(res.toString());
        }
      }
    }
     
}