/****************************************************************************************************************
Description    : Test generates data required for test data classes
****************************************************************************************************************/
@isTest
public class ClsTestDataFactory {
    //Method returns the map between Recordtypename and RecordtypeId
    public static Map<String, String> getRecordTypeMap(){
        //Query for the Account record types
        List<RecordType> rtypes = [Select Name, Id From RecordType 
                                   where sObjectType='Account' and isActive=true];
        
        //Create a map between the Record Type Name and Id 
        Map<String,String> accountRecordTypes = new Map<String,String>();
        for(RecordType rt: rtypes)
            accountRecordTypes.put(rt.Name,rt.Id);	
        return accountRecordTypes;
    }
    
    //Method creates accounts based on a particular record type
    public static List<Account> createAccountsWithBillingAddress(Integer numAccts, String accountType, String country) {
        Map<String,String> accountRecordTypes = ClsTestDataFactory.getRecordTypeMap();
        List<Account> accts = new List<Account>();
        String recordTypeId = accountRecordTypes.get(accountType);
        for(Integer i=0;i<numAccts;i++) {
            Account a = new Account();
            a.RecordTypeId = recordTypeId;
            if(accountType.Contains('Consumer') || accountType.Contains('Prescriber'))
            {
                if(accountType.Contains('Prescriber'))
                {
                    a.FirstName='TestPrescFirstName' + i;
                    a.LastName='TestPrescLastName' + i;
                    a.Prescribers__c = String.valueOf(Integer.valueOf('1000') + i);
                }
                else
                    if(accountType.Contains('Consumer'))
                {
                    a.FirstName='TestConsumerFirstName' + i;
                    a.LastName='TestConsumercLastName' + i;
                }	
                
            }
           
            if(country == 'US')
            {
                a.BillingStreet = '6340 Sequence Drive' + i;
                a.BillingCity = 'San Diego';
                a.BillingState = 'CA';				
                a.BillingPostalCode = '92121';
                a.BillingCountry = 'US';
            }
            accts.add(a);
        }
        insert accts;
        return accts;
    }
    
    //Method creates a user reocrd
    public static User createTestUser(Id roleId, Id profID, String fName, String lName)
    {
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        User tuser = new User(  firstname = fName,
                              lastName = lName,
                              email = uniqueName + '@test' + orgId + '.org',
                              Username = uniqueName + '@test' + orgId + '.org',
                              EmailEncodingKey = 'ISO-8859-1',
                              Alias = uniqueName.substring(18, 23),
                              TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',
                              LanguageLocaleKey = 'en_US',
                              ProfileId = profId,
                              UserRoleId = roleId);
        return tuser;
    }
    //Method creates Products for given product lists
    public static Map<String, Id> createProducts(List<String> prodNames) {
        
        List<Product2> productsToBeAdded = new List<Product2>();
        Map<String, Id> productNameToIdMap = new Map<String, Id>();
        for(String prodName : prodNames)
        {
            Product2 newProd = new Product2();
            newProd.Name = prodName;
            newProd.ProductCode = prodName;
            newProd.IsActive = true;
            productsToBeAdded.add(newProd);
        }
        insert productsToBeAdded;
        for(Product2 prod : productsToBeAdded)
        {
            productNameToIdMap.put(prod.Name, prod.Id);
        }
        return productNameToIdMap;
    }
    //Method creates custom pricebook
    public static Map<String, Id> createCustomPricebook(List<String> pricebookNames) {
        
        List<Pricebook2> pricebooksToBeAdded = new List<Pricebook2>();
        Map<String, Id> pricebookNameToIdMap = new Map<String, Id>();
        for(String pricebookName : pricebookNames)
        {
            Pricebook2 newPricebook = new Pricebook2();
            newPricebook.Name = pricebookName;
            newPricebook.IsActive = true;
            pricebooksToBeAdded.add(newPricebook);
        }
        insert pricebooksToBeAdded;
        for(Pricebook2 priceBook : pricebooksToBeAdded)
        {
            pricebookNameToIdMap.put(priceBook.Name, priceBook.Id);
        }
        return pricebookNameToIdMap;
    }
    //Method creates pricebook entries and returns ProductId to Pricebookentry Id
    public static Map<Id, Id> createCustomPricebookEntries(Map<Id, Decimal> productIdToPriceMap, Id customPricebookId) {
        // Get standard price book ID.
        Id standardPricebookId = Test.getStandardPricebookId();
        List<PricebookEntry> standardPricebookEntries = new List<PricebookEntry>();
        List<PricebookEntry> customPricebookEntries = new List<PricebookEntry>();
        Map<Id, Id> productIdToPbeId = new Map<Id, Id>();
        //Insert the products to the standard pricebook
        for(Id productId : productIdToPriceMap.keySet())
        {
            // Insert a price book entry for the standard price book.
            PricebookEntry standardPriceBookEntry = new PricebookEntry(
                Pricebook2Id = standardPricebookId, Product2Id = productId,
                UnitPrice = 1, IsActive = true);
            standardPricebookEntries.add(standardPriceBookEntry);
        }
        try{
            insert standardPricebookEntries;
        }
        catch (Exception e) {}
        //Insert the products to custom pricebook
        for(Id productId : productIdToPriceMap.keySet())
        {
            // Insert a price book entry for the standard price book.
            PricebookEntry customPriceBookEntry = new PricebookEntry(
                Pricebook2Id = customPricebookId, Product2Id = productId,
                UnitPrice = productIdToPriceMap.get(productId), IsActive = true);
            customPricebookEntries.add(customPriceBookEntry);
        }
        try{
            insert customPricebookEntries;
        }
        catch (Exception e) {}
        for(PricebookEntry pe : customPricebookEntries)
        {
            productIdToPbeId.put(pe.Product2Id, pe.Id);
            system.debug('****Product id is ' + pe.Product2Id);
        }
        return productIdToPbeId;
    }
}