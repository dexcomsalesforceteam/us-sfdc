/***
*@Author        : Priyanka Kajawe
*@Date Created  : 12-12-2018
*@Description   : Batch class to push marketing interaction entries for SSIP
***/

global with sharing class  BclsSSIPMarketingInteractions implements Database.Batchable<sObject>, Database.Stateful{
    global String soqlQuery; //Query string
    global Map<String, Date> daysFromNowMap = new Map<String, Date>();
    
    global BclsSSIPMarketingInteractions(){
        for(Marketing_Interaction_Entry__mdt miEntry : [SELECT id, DeveloperName, Label, Days__c, Duration__c, IsActive__c
                                                        from Marketing_Interaction_Entry__mdt where IsActive__c = true]){
                                                            String tempDays = String.valueof(miEntry.Days__c);
                                                            if(miEntry.Duration__c == 'Future')
                                                                tempDays = '-' + tempDays;
                                                            Date daysFromNow = Date.Today().addDays(Integer.valueOf(tempDays)) ; 
                                                            daysFromNowMap.put(miEntry.Label, daysFromNow);  
                                                        }
        System.debug('daysFromNowMap >> ' + daysFromNowMap);
    }
    //Start Method
    global Database.QueryLocator start(Database.BatchableContext BC){ 
        System.debug('daysFromNowMap in start>> ' + daysFromNowMap);
        
        //prepare SOQL 
        soqlQuery = 'SELECT ID, Account__c, Scheduled_Shipment_Date__c, Status__c, Is_Credit_Card_Active__c, Is_CMN_Active__c, Is_Pre_Authorized__c, Is_Scheduled__c, Skip__c, SSIP_Rule__r.Rule_Status__c, Price_Book__r.Cash_Price_Book__c FROM SSIP_Schedule__c ';
        soqlQuery += 'WHERE (';
        Integer appendOR = 0;
        for(String key : daysFromNowMap.keySet()){
            soqlQuery += appendOR != 0 ? ' OR ' : ''; 
            switch on key {
                when 'SSIP Schedule Notification'{
                    Date SSIPNotificationDate = daysFromNowMap.get(key);
                    
                    soqlQuery += '(Scheduled_Shipment_Date__c = :SSIPNotificationDate';
                    soqlQuery += ' and Is_Scheduled__c = true )';
                }when 'SSIP Credit Card Date'{
                    Date CCNotificationDate = daysFromNowMap.get(key);
                    
                    soqlQuery += '(Scheduled_Shipment_Date__c = :CCNotificationDate';
                    soqlQuery += ' and Is_Scheduled__c = true and ';
                    soqlQuery += 'Is_Credit_Card_Active__c = false)';
                }when 'SSIP Delay'{
                    Date DelayNotificationDate = daysFromNowMap.get(key);
                    
                    soqlQuery += '(Scheduled_Shipment_Date__c = :DelayNotificationDate';
                    soqlQuery += ' and Is_Scheduled__c = true) ';
                }
            }
            appendOR++;
        }
        soqlQuery += ')';
        system.debug('SOQL Query took effect is ' + soqlQuery);
        return Database.getQueryLocator(soqlQuery);
    }
    //Execute Method  
    global void execute(Database.BatchableContext BC, List<SSIP_Schedule__c> scheduleList){
        system.debug('Size of the list is ' + scheduleList.size());
        System.debug('daysFromNowMap in execute>> ' + daysFromNowMap);
       
        //List of Marketing Interaction Records to insert
        List<Marketing_Interaction__c> marketingInteractionList = new List<Marketing_Interaction__c>();
        if(!scheduleList.isEmpty()){
            for(SSIP_Schedule__c thisSchedule : scheduleList) {                
                //criteria to create MI with correct communication type
                
                if(thisSchedule.Is_Scheduled__c && !thisSchedule.Is_Credit_Card_Active__c && //criteria 3: Credit Card not active
                      daysFromNowMap.containsKey('SSIP Credit Card Date') &&
                      thisSchedule.Scheduled_Shipment_Date__c == daysFromNowMap.get('SSIP Credit Card Date')){
                        Marketing_Interaction__c thisCCMI = new Marketing_Interaction__c();
                        thisCCMI.Source_Record_Id__c = thisSchedule.Id;  // Add Source Record Id as Opty Id
                        thisCCMI.Account__c = thisSchedule.Account__c; //Account Id
                        thisCCMI.Communication_Type__c = 'SSIP Credit Card Date';
                        marketingInteractionList.add(thisCCMI);
                }
                if(thisSchedule.Is_Scheduled__c && //criteria 4: SSIP Schedule Notification
                       daysFromNowMap.containsKey('SSIP Schedule Notification') &&
                       thisSchedule.Scheduled_Shipment_Date__c == daysFromNowMap.get('SSIP Schedule Notification')){
                        Marketing_Interaction__c thisScheduleMI = new Marketing_Interaction__c();
                        thisScheduleMI.Source_Record_Id__c = thisSchedule.Id;  // Add Source Record Id as Opty Id
                        thisScheduleMI.Account__c = thisSchedule.Account__c; //Account Id
                        thisScheduleMI.Communication_Type__c = 'SSIP Schedule Notification'; 
                        marketingInteractionList.add(thisScheduleMI);
                }
                if(thisSchedule.Is_Scheduled__c && //criteria 5: SSIP Delay
                       daysFromNowMap.containsKey('SSIP Delay') &&
                       thisSchedule.Scheduled_Shipment_Date__c == daysFromNowMap.get('SSIP Delay')){
                        Marketing_Interaction__c thisMI = new Marketing_Interaction__c();
                        thisMI.Source_Record_Id__c = thisSchedule.Id;  // Add Source Record Id as Opty Id
                        thisMI.Account__c = thisSchedule.Account__c; //Account Id
                        thisMI.Communication_Type__c = 'SSIP Delay';
                        marketingInteractionList.add(thisMI);                       
                }                
            }  
            //Insert Marketing Interaction Records
            if(!marketingInteractionList.isEmpty()){
                try{
                    System.debug('final List >> ' + marketingInteractionList);
                     Database.insert(marketingInteractionList,false);  //Insert Marketing Interaction Records
                }catch (DmlException de) {
                    Integer numErrors = de.getNumDml();
                    System.debug('getNumDml=' + numErrors);
                    for(Integer i=0;i<numErrors;i++) {
                        System.debug('getDmlFieldNames=' + de.getDmlFieldNames(i));
                        System.debug('getDmlMessage=' + de.getDmlMessage(i));
                    }
                }
            }   
        }
    }   
    
    //Finish Method
    global void finish(Database.BatchableContext BC) {
    } 
}