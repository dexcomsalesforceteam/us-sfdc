/*
  @Author        : Jagan Periyakaruppan
  @Description    : Includes logic to create Medicare Subscription
*/
public class ClsCreateMFRSubscriptionPageExtension {
    
    public final Account acc;
    
    public clsCreateMFRSubscriptionPageExtension(ApexPages.StandardController stdController) {
        stdController.addFields(new List<String>{'Last_MDCR_Reorder_Follow_up_Date__c', 'Max_MFR_Followup_Date__c', 'Primary_Ship_To_Address__c', 'Is_Chart_Notes_Actve__c'});
        this.acc = (Account)stdController.getRecord();
        system.debug('Accunt' +acc);
        system.debug('Last MDCR Reorder Followup Date is ' + acc.Last_MDCR_Reorder_Follow_up_Date__c);
        
    }
    public pagereference getretrunURL()
    {
        PageReference retPage = new PageReference('/'+apexPages.CurrentPage().getParameters().get('id')); 
        return retPage;
    }

    public boolean getCheckAddress ()
    {
        List<Address__C> addrList = new List<Address__c>();
        addrList = [SELECT Id,Zip_Postal_Code__c,City__c,State__c FROM Address__c WHERE ID =: acc.Primary_Ship_To_Address__c Limit 1 ];
        Boolean result = false;
        if(!addrList.isEmpty())
        {
        	if(addrList[0].Zip_Postal_Code__c == null || addrList[0].State__c == null || addrList[0].City__c == null)
         	   result = true;
        }
        return result;
    }
    
    public boolean getCheckLastMFRSubscriptionDate ()
    {
        Date currentDate = Date.today();
        Boolean result = false;
        //If the date since we last billed for Sub isn't more than 30 days then we cannot let the users to create another sub
        if((acc.Last_MDCR_Reorder_Follow_up_Date__c != null && acc.Last_MDCR_Reorder_Follow_up_Date__c.daysBetween(currentDate) < 30) || 
           (acc.Max_MFR_Followup_Date__c != null && acc.Max_MFR_Followup_Date__c.daysBetween(currentDate) < 30) )   
            result = true;
        return result;
    }
    
    public boolean getCheckForActiveChartNotes ()
    {
        Boolean result = false;
        if(acc.Is_Chart_Notes_Actve__c)    
            result = true;
        return result;
    }
    
}