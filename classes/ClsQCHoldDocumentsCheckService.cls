/*************************************************
* @author      : Dexcom, LTI
* @date        : April, 2019
* @description : Controller QC Hold : Document Check Service Call
*************************************************/
public class ClsQCHoldDocumentsCheckService {
 
    @AuraEnabled
    public static Map<String, String> getDocumentDetails(String orderId, String accountId, String orderType, Boolean isOrderActivated) {
        Map<String, String> fieldValueMap = new Map<String, String>();
        string cmnExpiration;
        string cmnExpirationDate;
        string chartNotes;
        string chartNotesExpirationDate;
        Boolean isExceptionPB = false;
        
        System.debug('orderType Document Details >> '+ orderType);
        
        Account accnt = [SELECT Id,Payor__c,Is_CMN_Active__c,CMN_or_Rx_Expiration_Date__c,
                                        Is_Chart_Notes_Actve__c,Chart_Notes_Expiration_Date__c, Last_Order_Pricebook__c, 
                                        Primary_Benefit__c, Primary_Benefit__r.PRIOR_AUTH_REQUIRED__c FROM ACCOUNT WHERE Id =:accountId];
        
        system.debug('accnt::::'+accnt);
        system.debug('payor is::::'+accnt.Payor__c);
        
        fieldValueMap = ClsOrderAuditHandler.getAuditDetailsforCheck('Document', orderId);        
        System.debug('Fetched Document Details >> '+ fieldValueMap);
        
        if(fieldValueMap == null || fieldValueMap.isEmpty()){
            
            List<Rules_PayorToDoc__c> rulesPayorToDocList = [Select Id,Payor__c,DocumentType__r.Name,OrderType__c,Required__c from Rules_PayorToDoc__c where Payor__c=:Accnt.Payor__c and OrderType__c=: orderType];
            System.debug('rulesPayorToDocList >> '+ rulesPayorToDocList );           
            
            //check for Cash or Sample Orders            
            List<Order> thisOrderDetails = [Select Id, Is_Cash_Order__c, Price_Book__r.Oracle_Id__c from Order where Id = :orderId];
            if(!thisOrderDetails.isEmpty() && 
                (thisOrderDetails[0].Is_Cash_Order__c || 
                    thisOrderDetails[0].Price_Book__r.Oracle_Id__c == '6550' ||
                    thisOrderDetails[0].Price_Book__r.Oracle_Id__c == '6840' ||
                    thisOrderDetails[0].Price_Book__r.Oracle_Id__c == '13920')) 
                isExceptionPB = true;
            
            // on request it will be manually checked and added. 
            cmnExpiration = 'false';
            
            // on request it will be manually checked and added.
            //chartNotes =accnt.Is_Chart_Notes_Actve__c == True?'true':'false';
            chartNotes = 'false';
            
            Datetime  chart_Notes_Expiration_Date = accnt.Chart_Notes_Expiration_Date__c;
            chartNotesExpirationDate = chart_Notes_Expiration_Date != null ? String.valueOf(chart_Notes_Expiration_Date.format('MM/dd/yyyy','GMT')) : null;
            if(!isExceptionPB){
                
                for(Rules_PayorToDoc__c rulesPayorToDoc:rulesPayorToDocList){
                    System.debug('rulesPayorToDoc >>' + rulesPayorToDoc.DocumentType__r.Name);
                   if(rulesPayorToDoc.DocumentType__r.Name == 'Chart Notes'  && rulesPayorToDoc.Required__c){
                        fieldValueMap.put('ChartNotes',chartNotes);
                        fieldValueMap.put('ChartNotesExpirationDate',chartNotesExpirationDate);
                       System.debug('ChartNotesExpirationDate >>' + ChartNotesExpirationDate);
                    } 
                
                    if(rulesPayorToDoc.DocumentType__r.Name == 'BG Logs'  && rulesPayorToDoc.Required__c){
                        fieldValueMap.put('BGLogs','false');
                    }
                    
                    if(rulesPayorToDoc.DocumentType__r.Name == 'Lab Results'  && rulesPayorToDoc.Required__c){
                        fieldValueMap.put('LabResults','false');
                    }
                    
                    if(rulesPayorToDoc.DocumentType__r.Name == '72 Hour Trial'  && rulesPayorToDoc.Required__c){
                        fieldValueMap.put('HourTrial','false');
                    }
                    
                    if(rulesPayorToDoc.DocumentType__r.Name == 'OOW Letter'  && rulesPayorToDoc.Required__c){
                        if(accnt.Primary_Benefit__c != null && accnt.Primary_Benefit__r.PRIOR_AUTH_REQUIRED__c == 'Y')
                            fieldValueMap.put('OOWLetter','false');
                    }
                    
                    if(rulesPayorToDoc.DocumentType__r.Name == 'Hypoglycemic Questionnaire'  && rulesPayorToDoc.Required__c){
                        fieldValueMap.put('HypoglycemicQuestionnaire','false');
                    }
                    
                    if(rulesPayorToDoc.DocumentType__r.Name == 'ABN' && rulesPayorToDoc.Required__c){
                        fieldValueMap.put('ABN','false');
                    }
                    
                    if(rulesPayorToDoc.DocumentType__r.Name == 'Original Date Of Purchase'  && rulesPayorToDoc.Required__c){
                        fieldValueMap.put('OriginalDateofPurchase','false');
                         system.debug('rulesPayorToDoc.DocumentType__r.Name..t..'+fieldValueMap.get('OriginalDateofPurchase'));
                    }
                    
                    if(rulesPayorToDoc.DocumentType__r.Name == 'Tech Support Notes' && rulesPayorToDoc.Required__c){
                        fieldValueMap.put('TechSupportNotes','false');
                    }
                    //added below to handle new documents introduced - start
                    if('Recent Notes With Compliance'.equalsIgnoreCase(rulesPayorToDoc.DocumentType__r.Name) && rulesPayorToDoc.Required__c){
                        fieldValueMap.put('RecentNotesWithCompliance','false');
                    }
                    if('LMN'.equalsIgnoreCase(rulesPayorToDoc.DocumentType__r.Name) && rulesPayorToDoc.Required__c){
                        fieldValueMap.put('LMN','false');
                    }
                    //new document handling- end
                }
            }
            if(!isExceptionPB){
                fieldValueMap.put('bypassCheck','false');
                
            //    fieldValueMap.put('ChartNotes',chartNotes);
            //    fieldValueMap.put('ChartNotesExpirationDate',chartNotesExpirationDate);
            }
            
            if(!thisOrderDetails.isEmpty() && 
                (thisOrderDetails[0].Price_Book__r.Oracle_Id__c != '6550' &&
                    thisOrderDetails[0].Price_Book__r.Oracle_Id__c != '6840' &&
                    thisOrderDetails[0].Price_Book__r.Oracle_Id__c != '13920')){
                fieldValueMap.put('AOB', 'false');
            }
            fieldValueMap.put('CMN',cmnExpiration);
            
        }
        system.debug('Document fieldValueMap..t..'+fieldValueMap);
        if(!isOrderActivated && (fieldValueMap.isEmpty() || (!fieldValueMap.isEmpty() && fieldValueMap.containsKey('bypassCheck') 
                                                                && !Boolean.valueOf(fieldValueMap.get('bypassCheck'))))){ //CMN expiration Date will be always fetched and populated  
            Datetime  cmn_or_Rx_Expiration_Date = accnt.CMN_or_Rx_Expiration_Date__c;
                cmnExpirationDate = cmn_or_Rx_Expiration_Date != null ? String.valueOf(cmn_or_Rx_Expiration_Date.format('MM/dd/yyyy','GMT')) : null;
                
            fieldValueMap.put('CMNExpirationDate',cmnExpirationDate);
        }
        
        return fieldValueMap;
    }
}