@isTest
public class Test_OrderTrigger {
    @istest
    public static void testData(){
        
        test.startTest(); 
        
        //Query for the Account record types
        List<RecordType> rtypes = [Select Name, Id From RecordType 
                                   where sObjectType='Account' and isActive=true];
        //Create a map between the Record Type Name and Id 
        Map<String,String> accountRecordTypes = new Map<String,String>{};
            for(RecordType rt: rtypes)
            accountRecordTypes.put(rt.Name,rt.Id);
        
        //Get Consumers record type 
        String consumerRecordTypeId = accountRecordTypes.get('Consumers');
        
        //Get Prescriber record type 
        String prescriberRecordTypeId = accountRecordTypes.get('Prescriber');
        
        //Get Payor record type 
        String payorRecordTypeId = accountRecordTypes.get('Payor');
        
        //Create Pricebook
        Map<String, Id> customPricebookMap = ClsTestDataFactory.createCustomPricebook(new List <String>{'Aetna Health Plan'});
        String customPricebookId = customPricebookMap.get('Aetna Health Plan');
        
        List<Account> accntsToBeInserted = new List<Account>();
        Account consumerAccnt = TestDataBuilder.testAccount();
        consumerAccnt.FirstName = 'Med Adv FirstName';
        consumerAccnt.LastName = 'Med Adv LastName';
        consumerAccnt.Party_Id__c = '78666567';
        consumerAccnt.G6_Program__c = 'Limited Launch Participant';
        consumerAccnt.DOB__c =Date.newInstance(1992, 04, 23);
        consumerAccnt.RecordtypeId = consumerRecordTypeId;
        accntsToBeInserted.add(consumerAccnt);
        
        //Create a Payor record along with the Med Adv details
        Account mdcrPayor = new Account();
        mdcrPayor.RecordTypeId = payorRecordTypeId;
        mdcrPayor.Name = 'Med Adv Test Payor';
        mdcrPayor.Payor_Code__c = 'K';
        mdcrPayor.Default_Price_Book_K__c = customPricebookId;
        mdcrPayor.Claim_Address__c = '1 Main Street, San Diego, CA - 92127';
        mdcrPayor.DOB__c =Date.newInstance(1992, 04, 23);
        accntsToBeInserted.add(mdcrPayor);
        
        //Create a consumer and Prescriber Account 
        Account prescriberAccnt = TestDataBuilder.testAccount();
        prescriberAccnt.FirstName = 'Med Adv FirstName';
        prescriberAccnt.LastName = 'Med Adv LastName';
        prescriberAccnt.RecordtypeId = prescriberRecordTypeId;
        prescriberAccnt.DOB__c =Date.newInstance(1992, 04, 23);
        accntsToBeInserted.add(prescriberAccnt);
        insert accntsToBeInserted;
        
        List<Address__c> accountAddresses = new List<Address__c>();
        Address__c billTo = new Address__c();
        billTo.Account__c = accntsToBeInserted[0].Id;
        billTo.Street_Address_1__c = '1 Main Street';
        billTo.City__c = 'San Diego';
        billTo.State__c = 'CA';
        billTo.Zip_Postal_Code__c = '92121';
        billTo.Address_Type__c = 'BILL_TO';
        billTo.Primary_Flag__c = true;
        accountAddresses.add(billTo);
        Address__c shipTo = new Address__c();
        shipTo.Account__c = accntsToBeInserted[0].Id;
        shipTo.Street_Address_1__c = '1 Main Street';
        shipTo.City__c = 'San Diego';
        shipTo.State__c = 'CA';
        shipTo.Zip_Postal_Code__c = '92121';
        shipTo.Address_Type__c = 'SHIP_TO';
        shipTo.Primary_Flag__c = true;
        shipTo.Oracle_Address_ID__c = '12345';
        accountAddresses.add(shipTo);
        insert accountAddresses;
        
        //Create Benefit Record
        Benefits__c benefit = new Benefits__c();
        benefit.Benefit_Hierarchy__c = 'Primary';
        benefit.Account__c = accntsToBeInserted[0].Id;
        benefit.Payor__c = accntsToBeInserted[1].Id;
        benefit.Coverage__c = 80;
        benefit.CO_PAY__c = 20;
        benefit.Copay_Line_Item__c = 20;
        benefit.INDIVIDUAL_DEDUCTIBLE__c = 500;
        benefit.INDIVIDUAL_MET__c = 200;
        benefit.INDIVIDUAL_OOP_MAX__c = 1500;
        benefit.INDIVIDUAL_OOP_MET__c = 1000;
        benefit.FAMILY_DEDUCT__c = 1000;
        benefit.Family_Met__c = 850;
        benefit.FAMILY_OOP_MAX__c = 3000;
        benefit.FAMILY_OOP_MET__c = 2500;
        benefit.MEMBER_ID__c ='23423';
        insert benefit;
        
        //Update Primary Shipping and Benefit on Account
        Account acc = [SELECT Id, Primary_Ship_To_Address__c, Primary_Benefit__c FROM Account WHERE Id = :accntsToBeInserted[0].Id];
        acc.Primary_Ship_To_Address__c = accountAddresses[1].Id;
        acc.Primary_Benefit__c = benefit.Id;
        acc.Prescribers__c = accntsToBeInserted[2].Id;
        update acc;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        
        Product2 prod = new Product2(Name = 'STS-OR-003', ProductCode ='STS-OR-003',IsActive = true);
        insert prod;
        
        PricebookEntry standardPrice = new PricebookEntry( Pricebook2Id = pricebookId, Product2Id = prod.Id,  UnitPrice = 100, IsActive = true);
        insert standardPrice;
        
        copayUrl__c copay=copayUrl__c.getOrgDefaults();
        copay.Copay__c  ='https://dexsfp.dexcom.com/api/orders/copay/';
        
        upsert copay copayUrl__c.Id;
        
        TaxComputationUrl__c taxCompUrl = TaxComputationUrl__c.getOrgDefaults();
        taxCompUrl.URL__c  ='https://dexsfp.dexcom.com/api/orders/calculatetax/';
        
        upsert taxCompUrl copayUrl__c.Id;
        
        
        //Create a new Order
        Order customOrder = new Order();
        Id usOrderId = [SELECT Id FROM RecordType WHERE SobjectType = 'Order' AND DeveloperName = 'US_Sales_Orders'].Id;
        customOrder.AccountId = accntsToBeInserted[0].Id;
        customOrder.Recordtypeid = usOrderId;
        customOrder.Status = 'Draft';
        customOrder.Sub_Type__c = 'Comp Product Order';
        customOrder.EffectiveDate = Date.Today();
        customOrder.Scheduled_Ship_Date__c = Date.Today();
        customOrder.Price_Book__c = pricebookId;
        customOrder.Shipping_Method__c = '000001_FEDEX_A_SAT';
        customOrder.Shipping_Address__c = accountAddresses[1].Id;
        customOrder.Tax_Exempt__c = true;
        
        insert customOrder;
        Map<Id, Order> oldTriggerOrders=new  Map<Id, Order>();
        oldTriggerOrders.put(customOrder.id,customOrder);
        
        //  system.debug('Tax_Exempt__c..insert...'+ customOrder.Tax_Exempt__c);
        //    system.debug('Count_of_Order_Line_Items__c..Insert...'+ customOrder.Count_of_Order_Line_Items__c);
        
        OrderItem ordItem=new OrderItem();
        ordItem.OrderId = customOrder.Id;
        ordItem.Product2Id = prod.Id;
        ordItem.Quantity  = 3;
        ordItem.UnitPrice = 100;
        ordItem.PricebookEntryId =standardPrice.id;
        
        insert ordItem;
        ClsOrderTriggerHandler.isExecuting=false;
        
        order ord = [select id,Totalamount,Is_Cash_Order__c, status, Tax_Exempt__c, Force_Calc_Copay_Tax__c from order where id=:customOrder.id limit 1];
        //   system.debug('Totalamount..insert...'+ ord.Totalamount+'..Is_Cash_Order__c..'+ord.Is_Cash_Order__c);
        ord.status = 'Activated';
        ord.Tax_Exempt__c = false;
        ord.Force_Calc_Copay_Tax__c =true;
        ord.Reason_for_Approval__c ='VP Approved';
        
        ord.Type ='Sample Order';
        
        update ord;
        
        //  system.debug('Tax_Exempt__c..update...'+ customOrder.Tax_Exempt__c);
        //   system.debug('Count_of_Order_Line_Items__c..update...'+ customOrder.Count_of_Order_Line_Items__c);
        
        Map<Id, Order> newTriggerOrders=new   Map<Id, Order> ();
        newTriggerOrders.put(ord.id,ord);
        set<Id> accSet=new set<id>();
        
        list<account> accList=[select id from account];
        for(account account:accList){
            accSet.add(account.id);
        }
        
        test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        ClsOrderTriggerHandler.CalculateCopay(newTriggerOrders.keyset(),accSet);
        
        test.stopTest();
    }
    
}