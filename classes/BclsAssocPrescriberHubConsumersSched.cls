/***
*@Author        : Sudheer, LTI
*@Date Created  : 05/28/2020
*@Description   : Schedule class for BclsAddPrescribersToAsembiaAccounts.
***/
public class BclsAssocPrescriberHubConsumersSched implements schedulable{
    public void execute(schedulableContext sc){
        BclsAssocPrescriberHubConsumers bcn = new BclsAssocPrescriberHubConsumers() ;
        ID batchprocessid = Database.executeBatch(bcn);
    }
}