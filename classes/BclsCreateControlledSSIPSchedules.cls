/*
 * Description : This batch is created for story CRMSF-4577
 * The execution of it will create new SSIP Schedules for next year based on the intervals given.
 * It will pick up the SSIP rule records on nightly basis where end date is blank and latest schedule
 * date is blank or equal to batch run date.
 * @Author : Tanay Kulkarni, LTI
*/
global class BclsCreateControlledSSIPSchedules implements Database.Batchable<sObject>{
    
    /*
     * Description : Fetches the records based on the query criteria as below
	*/
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query;
        query = 'SELECT Id, Schedule_Cycle__c, Product__c, Price_Book_New__c, First_Shipment_Date__c, Max_Schedule_Shipment_Date__c, Schedule_Revision_Date__c, Rule_End_Date__c, Frequency_In_Days_Number__c,Account__r.Default_Price_Book__c,Account__c,Account__r.Payor__c,Account__r.Primary_Ship_To_Address__c FROM SSIP_Rule__c WHERE Rule_End_Date__c = null AND (Schedule_Revision_Date__c = null OR Schedule_Revision_Date__c = TODAY OR Force_SSIP_Revision__c = true)';
        if(Test.isRunningTest()){
            query = 'SELECT Id, Schedule_Cycle__c, Product__c, Price_Book_New__c, First_Shipment_Date__c, Max_Schedule_Shipment_Date__c, Schedule_Revision_Date__c, Rule_End_Date__c, Frequency_In_Days_Number__c,Account__r.Default_Price_Book__c,Account__c,Account__r.Payor__c,Account__r.Primary_Ship_To_Address__c FROM SSIP_Rule__c WHERE Rule_End_Date__c = null';
        }
        return DataBase.getQueryLocator(query);
    }
    
    /*
     * @Description : Below chunk Execuets number of times the chunks we have for processing
	*/
    global void execute(Database.BatchableContext bc, List<SSIP_Rule__c> ruleRecordList) {
        PriceBook2 cashPriceBook = new PriceBook2();
        if(!test.isRunningTest()){
            cashPriceBook = [SELECT Id,Name FROM Pricebook2 WHERE Name = : ClsApexConstants.CASH_PRICE_LIST AND Cash_Price_Book__c = : ClsApexConstants.BOOLEAN_TRUE limit 1];
        }
        
        List<SSIP_Schedule__c> ssipScheduleInsertList = new List<SSIP_Schedule__c>();
        for(SSIP_Rule__c ssipRuleRecord : ruleRecordList){
            if(ssipRuleRecord.Schedule_Cycle__c !=null && ssipRuleRecord.Frequency_In_Days_Number__c != null){
                Date scheduleShipDate = ssipRuleRecord.Max_Schedule_Shipment_Date__c == null ? ssipRuleRecord.First_Shipment_Date__c.addDays(Integer.ValueOf(ssipRuleRecord.Frequency_In_Days_Number__c)) : ssipRuleRecord.Max_Schedule_Shipment_Date__c.addDays(Integer.ValueOf(ssipRuleRecord.Frequency_In_Days_Number__c));
                if(ssipRuleRecord.Schedule_Cycle__c !=null){
                    for(Decimal scheduleCycle = ClsApexConstants.ZERO_DECIMAL; scheduleCycle < ssipRuleRecord.Schedule_Cycle__c; scheduleCycle++){
                        ClsSSIPRuleTriggerHandler.createSSIPSchedulesList(ssipScheduleInsertList, ssipRuleRecord, scheduleShipDate, null, cashPriceBook);
                        if(scheduleShipDate != null)
                            scheduleShipDate = scheduleShipDate.addDays(Integer.ValueOf(ssipRuleRecord.Frequency_In_Days_Number__c));
                    }
                }
            }
        }
        //implement error handling mechanism and log in apex debug log object
        Database.SaveResult[] saveResultObjList;
        if(!ssipScheduleInsertList.isEmpty()){
            saveResultObjList = database.insert(ssipScheduleInsertList,ClsApexConstants.BOOLEAN_FALSE);
        }
        List<Apex_Debug_Log__c> apexDebugLogList = new List<Apex_Debug_Log__c>();
        for(Database.SaveResult saveResultObj : saveResultObjList){
            if (!saveResultObj.isSuccess()){
                for(Database.Error err : saveResultObj.getErrors()){
                    Apex_Debug_Log__c apexDebugLogObj = new Apex_Debug_Log__c();
                    apexDebugLogObj.Apex_Class__c = 'BclsCreateControlledSSIPSchedules';
                    apexDebugLogObj.Method__c = 'Execute';
                    apexDebugLogObj.Message__c = err.getMessage();
                    apexDebugLogObj.Type__c = ClsApexConstants.DEBUG_TYPE_ERROR;
                    apexDebugLogList.add(apexDebugLogObj);
                }
            }
        }
        if(!apexDebugLogList.isEmpty()){
            Database.insert(apexDebugLogList, ClsApexConstants.BOOLEAN_FALSE);
        }
        
    }
    
    /*
     * @Description : routine executes at last when all the executions are finished
	*/
    global void finish(Database.BatchableContext bc){
        
    }

}