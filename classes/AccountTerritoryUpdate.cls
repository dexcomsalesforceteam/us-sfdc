/********************************************************************************
@author Abhishek Parghi
@date 11/18/2015
@description: Apex class for account territory update & Benefits party id update.
*******************************************************************************/
/********************************************************************************
@author Jagan Periyakaruppan
@date 02/01/2018
@description: Inactivated method PrescribersOwnerupdate as it is not used
*******************************************************************************/
/********************************************************************************
@author Anuj Patel
@date 02/20/2018
@description: Removed method AccountPartyIDUpdate as it is not used
*******************************************************************************/

/********************************************************************************
@author Brian Uyeda
@date 05/06/2016
@description: Added Secondary Territory logic
*******************************************************************************/

public class AccountTerritoryUpdate {
    
    public static void PrescribersTerritoryBeforeUpdate(List<Account> accountsNew){
        set<string> Zipcodes = new set<string>();  
        set<ID> Accset = new Set<ID>();
        Map<String, Id> terrIdLookupMap = new Map<String, Id> ();
        Id CnRecordTypeId3 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prescriber').getRecordTypeId(); 
        for (Account A: accountsNew) {
            if(A.ShippingPostalCode != null && A.RecordTypeId == CnRecordTypeId3 ){    
                if(A.ShippingPostalCode.length() >= 5){
                    Zipcodes.add(A.ShippingPostalCode.substring(0,5));
                }
            }
        } 
        //query the zip_Ter object to get the zipcode (Name) and zone from the zip ter object  
        map<string,string> territoryMap=new map<string,string>(); 
        // BHU 2019/04/16
        map<string,string> territoryMapSF=new map<string,string>();
          
        for(Zip_Territory__c z : [Select name, Territory_Code__c, Territory_Code_SF__c from Zip_Territory__c WHERE name IN :Zipcodes]) {
            territoryMap.put (z.name, z.Territory_Code__c);
            // BHU 2019/04/16
            territoryMapSF.put (z.name, z.Territory_Code_SF__c);
        }
        if(!territoryMap.isEmpty())
        {
            terrIdLookupMap = getTerritoryLookupId(territoryMap.values());
        }       
        
        for(Account Acc: accountsNew) {  
            if(Acc.ShippingPostalCode != null){
                if(!territoryMap.isEmpty() && Acc.ShippingPostalCode.length() >= 5){
                    String territory = territoryMap.get(Acc.ShippingPostalCode.substring(0,5));
                    // BHU 2019/04/16
                    String territorySF = territoryMapSF.get(Acc.ShippingPostalCode.substring(0,5));
                    if(territory != null) {
                        Acc.Territory_Code__c = territory;
                        // BHU 2019/04/16
                        Acc.Territory_ID_SF__c = territorySF;
                        Acc.Territory_ID_Lookup__c = terrIdLookupMap.get(territory);
                    }
                }
            }
            
        }
    }
    
    //Method returns the Territory Id Lookup for a given Territory Name
    public static Map<String, Id> getTerritoryLookupId (List<String> terrNames)
    {
        Map<String, Id> terrNamesToTerrLookupIdMap = new Map<String, Id>();
        for(Territory_Alignment__c terrAlign : [SELECT Id, Name FROM Territory_Alignment__c WHERE Name IN : terrNames])
        {
            terrNamesToTerrLookupIdMap.put(terrAlign.Name, terrAlign.Id);
        }
        return terrNamesToTerrLookupIdMap;
    }
    
    //Method returns Id of the Prescriber Account - NO ACCOUNT (Do NOT Change/Delete)
    public static Id getPrescriberNoAccountId()
    {
        Id prescriberRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prescriber').getRecordTypeId(); 
        List<Account> adminAccountIds = new List<Account> ([Select Id FROM Account Where RecordTypeId = : prescriberRecordTypeId AND Name = 'NO ACCOUNT (Do NOT Change/Delete)']);
        if(!adminAccountIds.isEmpty())
            return adminAccountIds[0].Id;
        else
            return null;
    }
    
    /*  
public static void PrescribersOwnerupdate(List<Account> accountsNew){
List<String> terCodes = new List<String>();  
Set<ID> Accset = new Set<ID>();    
Id CnRecordTypeId4 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prescriber').getRecordTypeId(); 
for (Account A: accountsNew) {
if(A.Territory_Code__c != null && A.RecordTypeId == CnRecordTypeId4 ) {
terCodes.add(a.Territory_Code__c);
}
}
map<string,ID> territoryalignmentmap =  new map<string,ID>();  
for(Territory_Alignment__c Tz: [Select id,name,RSD__c,Territory_Rep__c from Territory_Alignment__c WHERE Name IN :terCodes]) {
territoryalignmentmap.put (Tz.name, Tz.Territory_Rep__c);
}
for (Account Acc: accountsNew) {
if(Acc.Territory_Code__c != null){
if(!territoryalignmentmap.isEmpty() &&   Acc.Territory_Code__c !=null){
ID   territory1  = territoryalignmentmap.get(Acc.Territory_Code__c);
if(territory1  != null) {
Acc.OwnerID = territory1;
}
}  
}      
}
}    
*/
    //Method groups the Consumer accounts, which needs to be updated with Territory information
    public static void ConsumersTerritoryBeforeInsert(List<Account> Accounts){
        system.debug('*****Entering the process of ConsumersTerritoryBeforeInsert');
		List<Account> accountList = new List<Account>();
        Set<Id> prescribers = new Set<Id>();
		Set<String> zipcodes = new set<string>(); 
        Id CnRecordTypeId2 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Consumers').getRecordTypeId(); 
        Id prescriberNoAccountId = getPrescriberNoAccountId();
        for(Account accnt: Accounts) {
            //Proceed if it is only Consumer account
            if(accnt.RecordTypeId == CnRecordTypeId2)
            {
                //For Consumers collect the Zip Code to look at the alignment
				if(accnt.ShippingPostalCode != null && accnt.ShippingPostalCode.length() >= 5){
					zipcodes.add(accnt.ShippingPostalCode.substring(0,5));
					accountList.add(accnt);
                }
				//Find US Consumer Accounts where the Prescriber is associated then the Territory needs to be evaluated
                if(accnt.Prescribers__c != null && accnt.Prescribers__c != prescriberNoAccountId){
                    accountList.add(accnt); 
                    prescribers.add(accnt.Prescribers__c);
                }
            }   
        }
        if(accountList.size()>0 || zipcodes.size()>0){
			ProcessAccountAlignment(accountList, prescribers, zipcodes);
        }
    }   
    
    //Method groups the Consumer accounts, which needs to be updated with Territory information
    public static void ConsumersTerritoryBeforeUpdate(List<Account> Accounts,Map <Id,Account> AccountsOldMap){
        List<Account> accountList = new List<Account>();
        Set<String> zipcodes = new set<string>();  
        Set<Id> prescribers = new Set<Id>();
        Id CnRecordTypeId2 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Consumers').getRecordTypeId(); 
        Id prescriberNoAccountId = getPrescriberNoAccountId();
        for(Account accnt: Accounts) {
            //Proceed if it is only Consumer account
            if(accnt.RecordTypeId == CnRecordTypeId2)
            {
                if(accnt.ShippingPostalCode != null && accnt.ShippingPostalCode.length() >= 5){
                    zipcodes.add(accnt.ShippingPostalCode.substring(0,5));
                }
                if(accnt.Prescribers__c != null && accnt.Prescribers__c != prescriberNoAccountId){
                    prescribers.add(accnt.Prescribers__c);
                }
                Account OldAccount = AccountsOldMap.get(accnt.ID);
                //Find US Consumer Accounts where the Prescribers is changed or the Shipping zip is changed
                if(accnt.Prescribers__c != OldAccount.Prescribers__c || accnt.ShippingPostalCode != OldAccount.ShippingPostalCode || (accnt.Territory_Admin_Flag__c != OldAccount.Territory_Admin_Flag__c && accnt.Territory_Admin_Flag__c == true)){
                    accountList.add(accnt); 
                }
            }   
        }
        system.debug('# Prescribers: ' + accountList.size());
        if(accountList.size()>0){
            ProcessAccountAlignment(accountList, prescribers, zipcodes);
        }
    }   
    
    //Method will set the Territory Name field on the Account
    public static void  ProcessAccountAlignment (List<Account> accountList, Set<Id> prescribers, Set<String> zipcodes){
        system.debug('Entering the process of ProcessAccountAlignment');
        Map<String, Id> terrIdLookupMap = new Map<String, Id> ();
        Map<Id, String> prescribersToTerrMap = new Map<Id, String>();
        Map<Id, String> prescribersToTerrSFMap = new Map<Id, String>();
        List<String> terrNames = new List<String>();
        Id prescriberNoAccountId = getPrescriberNoAccountId();
        
        for(Account prescriber : [Select Id, Territory_Code__c, Territory_ID_SF__c From Account Where Id In : prescribers])
        {
            prescribersToTerrMap.put(prescriber.Id, prescriber.Territory_Code__c);
            prescribersToTerrSFMap.put(prescriber.Id, prescriber.Territory_ID_SF__c);
            terrNames.add(prescriber.Territory_Code__c);
        }
        
        //Query the zip_Ter object to get the zipcode (Name) and zone from the zip ter object  
        Map<string,string> territoryMap = new Map<string,string>();  
        Map<string,string> territorySFMap = new Map<string,string>(); 
        for(Zip_Territory__c z : [Select name, Territory_Code__c, Territory_Code_SF__c from Zip_Territory__c WHERE name IN :zipcodes]) {
            territoryMap.put (z.name, z.Territory_Code__c);
            territorySFMap.put (z.name, z.Territory_Code_SF__c );
            terrNames.add(z.Territory_Code__c);
        }
        //Find the TerritoryId Lookup entries  
        if(!terrNames.isEmpty())
        {
            terrIdLookupMap = getTerritoryLookupId(terrNames);
        }
        //Assign Territory value for Consumers 
        for (Account accnt: accountList) {
            if(accnt.Prescribers__c != null && prescribersToTerrMap.get(accnt.Prescribers__c) != null && accnt.Prescribers__c != prescriberNoAccountId)//Take the Prescribers territory
            {
                String prescriberTerritory = prescribersToTerrMap.get(accnt.Prescribers__c);
                String prescriberTerritorySF = prescribersToTerrSFMap.get(accnt.Prescribers__c);
                system.debug('prescriberTerritory - ' + prescriberTerritory);
                system.debug('prescriberTerritorySF - ' + prescriberTerritorySF);
                accnt.Territory_Code__c = prescriberTerritory;
                accnt.Territory_ID_SF__c = prescriberTerritorySF;
                accnt.Territory_ID_Lookup__c = terrIdLookupMap.get(prescriberTerritory);
                
            }
            else if (accnt.ShippingPostalCode != null && accnt.ShippingPostalCode.length() >= 5)//Take the Territory aligned to shipping zip
            {
				String territory = territoryMap.get(accnt.ShippingPostalCode.substring(0,5)); 
                String territorySF = territorySFMap.get(accnt.ShippingPostalCode.substring(0,5));
                if(territory != null) {
                    accnt.Territory_Code__c = territory;
                    accnt.Territory_ID_SF__c = territorySF;
                    accnt.Territory_ID_Lookup__c = terrIdLookupMap.get(territory);
					system.debug('****Territory_ID_Lookup__c is ' + accnt.Territory_ID_Lookup__c);
                }
            }
            else accnt.Territory_Code__c = null;//Remove the territory
            
            
        }
    }
    
    public static void ConsumersOwnerupdate(List<Account> accountsNew){
        List<String> terCodes = new List<String>();  
        Set<ID> Accset = new Set<ID>();    
        Id CnRecordTypeId1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Consumers').getRecordTypeId(); 
        for (Account A: accountsNew) {
            if(A.Territory_Code__c != null && A.RecordTypeId == CnRecordTypeId1 ) {
                terCodes.add(a.Territory_Code__c);
            }
        }
        map<string,ID> territoryalignmentmap =  new map<string,ID>();  
        for(Territory_Alignment__c Tz: [Select id,name,RSD__c,Territory_Rep__c from Territory_Alignment__c WHERE Name IN :terCodes]) {
            territoryalignmentmap.put (Tz.name, Tz.Territory_Rep__c);
        }
        for (Account Acc: accountsNew) {
            if(Acc.Territory_Code__c != null){
                if(!territoryalignmentmap.isEmpty() &&   Acc.Territory_Code__c !=null){
                    ID   territory1  = territoryalignmentmap.get(Acc.Territory_Code__c);
                    if(territory1  != null) {
                        Acc.OwnerID = territory1;
                    }
                }    
                
            }      
        }
    }
    
    /*  public static void EMEAConsumerTerritoryUpdate(List<Account> accountsNew){
Id CnRecordTypeIdE1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('GB Consumer').getRecordTypeId(); 
Id CnRecordTypeIdE2 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IE Consumer').getRecordTypeId();
for (Account A: accountsNew) {
if(A.RecordTypeId == CnRecordTypeIdE1){
A.Territory_Code__c = '20110101';
}else if(A.RecordTypeId == CnRecordTypeIdE2){
A.Territory_Code__c = '20210101'; 
}  
} 

} */
    Public static void TaskFieldUpdate (List<Account> accountsNew3,Map<Id, Account> oldMap,Map<ID, Account> mapIdAcc){
        Set<id> setOfAccountId = new Set<Id>();
        Id CnRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prescriber').getRecordTypeId();
        List<Task> toBeUpdated = new List<task>();
        
        //create list of account ids (only active and prescriber record type)
        for(Account a : accountsNew3){
            if( (a.RecordTypeId == CnRecordTypeId)  &&(a.Territory_Code__c != oldMap.get(a.Id).Territory_Code__c ||a.firstname != oldMap.get(a.Id).firstname
                                                       ||a.LastName != oldMap.get(a.Id).LastName ||a.Middle_Name__c != oldMap.get(a.Id).Middle_Name__c
                                                       ||a.Middle_Name__c != oldMap.get(a.Id).Middle_Name__c ||a.IMS_ID__pc != oldMap.get(a.Id).IMS_ID__pc
                                                       ||a.Middle_Name__c != oldMap.get(a.Id).Middle_Name__c ||a.Suffix__pc != oldMap.get(a.Id).Suffix__pc
                                                       ||a.CCV_ID__c != oldMap.get(a.Id).CCV_ID__c ||a.IMS_ID__pc != oldMap.get(a.Id).IMS_ID__pc
                                                       ||a.Party_ID__c != oldMap.get(a.Id).Party_ID__c ||a.New_Address_ID__c != oldMap.get(a.Id).New_Address_ID__c)){ 
                                                           setOfAccountId.add(a.id);
                                                       }     
        }
        //get task records for set of accounts
        if(setOfAccountId.size()>0){
            List<Task> AllTasks = [SELECT Id, Whatid,First_Name__c,LastName__c,Middle_Name__c,IMS_ID__c,Address_Line_1__c
                                   ,CCV_ID__c,City__c,Zip_Code__c,State__c,Suffix__c,Prescriber_Territory_Code__c,
                                   Prescribers_SFDC_ID__c,Prescriber_s_RecordType__c,Party_ID__c,Territory__c,New_Address_ID__c
                                   FROM Task WHERE Whatid IN: setOfAccountId];
            
            //set the account fields on the task from the account value
            
            for(Task t : AllTasks) {   
                Account a = mapIdAcc.get(t.Whatid); 
                t.First_Name__c = a.firstname;
                t.LastName__c = a.LastName;
                t.Middle_Name__c = a.Middle_Name__c;
                t.IMS_ID__c =a.IMS_ID__pc;
                t.Address_Line_1__c = a.PersonMailingStreet;
                t.CCV_ID__c = a.CCV_ID__c; 
                t.City__c = a.PersonMailingCity;
                t.Zip_Code__c = a.PersonMailingPostalCode;
                t.State__c = a.PersonMailingState;
                t.Suffix__c =a.Suffix__pc;
                t.Prescriber_Territory_Code__c = a.Territory_Code__c;
                t.Prescribers_SFDC_ID__c = a.id;
                t.Prescriber_s_RecordType__c = a.Account_Record_Type__c;
                t.Party_ID__c =a.Party_ID__c;
                t.Territory__c = a.Territory_Code__c;
                t.New_Address_ID__c = a.New_Address_ID__c; 
                toBeUpdated.add(t);
            }
        }
        //update the task records    
        if(!toBeUpdated.isEmpty()){
            update toBeUpdated;  
        }
    }
    public static void CAConsumerTerritoryUpdate(List<Account> accountsNew){  
        Id CAConsumer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('CA Consumer').getRecordTypeId();
        set<string> Zipcodes = new set<string>();  
        for (Account A: accountsNew){
            if(A.Int_LN_Shipping_postcode__c != null && A.RecordTypeId == CAConsumer){
                if(A.Int_LN_Shipping_postcode__c.substringBefore(' ').length() == 3 ){
                    System.debug('AP##'+A.Int_LN_Shipping_postcode__c.substringBefore(' '));
                    Zipcodes.add(A.Int_LN_Shipping_postcode__c.substringBefore(' '));             
                }
            }
        }    
        System.debug('total$$'+Zipcodes.size());
        
        If(Zipcodes.size()>0){    
            //query the zip_Ter object to get the zipcode (Name) and zone from the zip ter object  
            map<string,string> territoryMap=new map<string,string>();  
            for(Zip_Territory__c z : [Select name, Territory_Code__c from Zip_Territory__c WHERE name IN :Zipcodes]) {
                territoryMap.put (z.name, z.Territory_Code__c);
            }
            
            for (Account Acc: accountsNew) {
                if(Acc.Territory_Code__c == null && Acc.Prescribers__c == null && 
                   Acc.RecordTypeId == CAConsumer && Acc.Int_LN_Shipping_postcode__c != null){
                       String territory = territoryMap.get(Acc.Int_LN_Shipping_postcode__c.substringBefore(' ')); 
                       System.debug('ter###'+territory);
                       if(territory != null) {
                           Acc.Territory_Code__c = territory;
                       }
                   }  
            } 
        }  
    }
    public static void CAConsumerOwnerUpdate(List<Account> accountsNew){
        List<String> terCodes = new List<String>();  
        Set<ID> Accset = new Set<ID>();
        Id CAConsumer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('CA Consumer').getRecordTypeId(); 
        for (Account A: accountsNew) {
            if(A.Territory_Code__c != null  && A.RecordTypeId == CAConsumer){ 
                terCodes.add(a.Territory_Code__c);
            }
        }
        IF(terCodes.size()>0){  
            map<string,ID> territoryalignmentmap =  new map<string,ID>();  
            for(Territory_Alignment__c Tz: [Select id,name,RSD__c,Territory_Rep__c from Territory_Alignment__c WHERE Name IN :terCodes]) {
                territoryalignmentmap.put (Tz.name, Tz.Territory_Rep__c);
            }
            for (Account Acc: accountsNew) {
                if(Acc.Territory_Code__c != null){
                    if(!territoryalignmentmap.isEmpty() && Acc.Territory_Code__c !=null){
                        ID territory1  = territoryalignmentmap.get(Acc.Territory_Code__c);
                        if(territory1  != null) {
                            Acc.OwnerID = territory1;
                        }
                    }    
                }
            }      
        }
    }   
    public static void EMEAConsumerTerritoryUpdate(List<Account> accountsNew){      
        set<string> Zipcodes = new set<string>();  
        set<ID> Accset = new Set<ID>(); 
        Id GBConsumer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('GB Consumer').getRecordTypeId(); 
        Id GBMedicalFacility = Schema.SObjectType.Account.getRecordTypeInfosByName().get('GB Medical Facility').getRecordTypeId(); 
        Id IEMedicalFacility = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IE Medical Facility').getRecordTypeId(); 
        Id GBPrescriber = Schema.SObjectType.Account.getRecordTypeInfosByName().get('GB Prescriber').getRecordTypeId(); 
        Id IEConsumer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IE Consumer').getRecordTypeId(); 
        Id IEPrescriber = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IE Prescriber').getRecordTypeId();
        Id CHConsumer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('EMEA CH Consumer').getRecordTypeId();
        for (Account A: accountsNew) {
            if(A.Int_LN_Shipping_postcode__c    != null  && (A.RecordTypeId == GBConsumer ||
                                                             A.RecordTypeId == GBMedicalFacility ||
                                                             A.RecordTypeId == IEMedicalFacility ||
                                                             A.RecordTypeId == GBPrescriber ||
                                                             A.RecordTypeId == IEConsumer ||
                                                             A.RecordTypeId == IEPrescriber )){
                                                                 if(A.Int_LN_Shipping_postcode__c.substringBefore(' ').length() == 3 ){
                                                                     System.debug('AP##'+A.Int_LN_Shipping_postcode__c.substringBefore(' '));
                                                                     Zipcodes.add(A.Int_LN_Shipping_postcode__c.substringBefore(' '));
                                                                 }else if(A.Int_LN_Shipping_postcode__c.substringBefore(' ').length() == 4 ){
                                                                     Zipcodes.add(A.Int_LN_Shipping_postcode__c.substringBefore(' '));
                                                                 }
                                                             }else if(A.Int_LN_Shipping_postcode__c != null  && A.RecordTypeId == CHConsumer){
                                                                 if(A.Int_LN_Shipping_postcode__c.length() == 4){
                                                                     Zipcodes.add(A.Int_LN_Shipping_postcode__c);
                                                                 }
                                                             }
        }
        System.debug('total$$'+Zipcodes.size());
        //    System.debug('CHValue'+Zipcodes.values());
        If(Zipcodes.size()>0){    
            //query the zip_Ter object to get the zipcode (Name) and zone from the zip ter object  
            map<string,string> territoryMap=new map<string,string>();  
            for(Zip_Territory__c z : [Select name, Territory_Code__c from Zip_Territory__c WHERE name IN :Zipcodes]) {
                territoryMap.put (z.name, z.Territory_Code__c);
            }
            
            for (Account Acc: accountsNew) {
                if(Acc.Territory_Code__c == null && (Acc.RecordTypeId == IEConsumer || Acc.RecordTypeId == GBConsumer)){
                    if(Acc.Medical_Facility__c ==null && Acc.Int_LN_Shipping_postcode__c     != null){ 
                        if(Acc.Int_LN_Shipping_postcode__c   != null){
                            String territory = territoryMap.get(Acc.Int_LN_Shipping_postcode__c.substringBefore(' ')); 
                            System.debug('ter###'+territory);
                            if(territory != null) {
                                Acc.Territory_Code__c = territory;
                            }
                        }       
                    }
                }else if(Acc.Territory_Code__c == null && Acc.Prescribers__c == null && 
                         Acc.RecordTypeId == CHConsumer && Acc.Int_LN_Shipping_postcode__c != null){
                             String territory = territoryMap.get(Acc.Int_LN_Shipping_postcode__c.substringBefore(' ')); 
                             System.debug('ter###'+territory);
                             if(territory != null) {
                                 Acc.Territory_Code__c = territory;
                             }
                         }  
            }
        }
        
    }
    public static void EMEAConsumerOwnerUpdate(List<Account> accountsNew){
        List<String> terCodes = new List<String>();  
        Set<ID> Accset = new Set<ID>();
        Id GBConsumer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('GB Consumer').getRecordTypeId(); 
        Id GBMedicalFacility = Schema.SObjectType.Account.getRecordTypeInfosByName().get('GB Medical Facility').getRecordTypeId(); 
        Id IEMedicalFacility = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IE Medical Facility').getRecordTypeId(); 
        Id GBPrescriber = Schema.SObjectType.Account.getRecordTypeInfosByName().get('GB Prescriber').getRecordTypeId(); 
        Id IEConsumer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IE Consumer').getRecordTypeId(); 
        Id IEPrescriber = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IE Prescriber').getRecordTypeId();
        Id CHConsumer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('EMEA CH Consumer').getRecordTypeId(); 
        for (Account A: accountsNew) {
            if(A.Territory_Code__c != null  && (A.RecordTypeId == GBConsumer ||
                                                A.RecordTypeId == GBMedicalFacility ||
                                                A.RecordTypeId == IEMedicalFacility ||
                                                A.RecordTypeId == GBPrescriber ||
                                                A.RecordTypeId == IEConsumer ||
                                                A.RecordTypeId == IEPrescriber ||
                                                A.RecordTypeId == CHConsumer)){  
                                                    
                                                    terCodes.add(a.Territory_Code__c);
                                                }
        }
        IF(terCodes.size()>0){  
            map<string,ID> territoryalignmentmap =  new map<string,ID>();  
            for(Territory_Alignment__c Tz: [Select id,name,RSD__c,Territory_Rep__c from Territory_Alignment__c WHERE Name IN :terCodes]) {
                territoryalignmentmap.put (Tz.name, Tz.Territory_Rep__c);
            }
            for (Account Acc: accountsNew) {
                if(Acc.Territory_Code__c != null){
                    if(!territoryalignmentmap.isEmpty() &&   Acc.Territory_Code__c !=null){
                        ID territory1  = territoryalignmentmap.get(Acc.Territory_Code__c);
                        if(territory1  != null) {
                            Acc.OwnerID = territory1;
                        }
                    }    
                }
            }      
        }
    }
    
}