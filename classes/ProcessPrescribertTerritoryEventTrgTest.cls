@isTest(seealldata = false)
public class ProcessPrescribertTerritoryEventTrgTest {

    @testSetup public static void dataSetUp(){
        Id consumerRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Consumers').getRecordTypeId();
        Id prescriberRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prescriber').getRecordTypeId();
        //create prescriber account
        Account prescriberAccnt = new Account();
        prescriberAccnt.FirstName = 'Med Adv FirstName';
        prescriberAccnt.LastName = 'Med Adv LastName';
        prescriberAccnt.RecordtypeId = prescriberRecordTypeId;
        prescriberAccnt.DOB__c =Date.newInstance(1992, 04, 23);
        insert prescriberAccnt;
        system.debug('prescriberAccnt inserted ::::'+prescriberAccnt);
        //create consumer account
        Account consumerAccnt = new Account();
        consumerAccnt.FirstName = 'Consumer FirstName';
        consumerAccnt.LastName = 'consumer LastName';
        consumerAccnt.Party_Id__c = '78666567';
        consumerAccnt.G6_Program__c = 'Limited Launch Participant';
        consumerAccnt.Prescribers__c = prescriberAccnt.Id;
        consumerAccnt.DOB__c =Date.newInstance(1992, 04, 23);
        consumerAccnt.RecordtypeId = consumerRecordTypeId;
        insert consumerAccnt;
        
        //create new addresses
        ClsAccountAddressTriggerStaticClass.addressChangeInvokedFromAddress = TRUE;
        ClsAccountAddressTriggerStaticClass.addressChangeInvokedFromAccount = FALSE;
        List<Address__c> addrList = new List<Address__c>();
        
        Address__c addrs = TestDataBuilder.getAddressList(prescriberAccnt.Id, true,'SHIP_TO', 1)[0];
        addrs.Oracle_Address_ID__c = '23145687';
        addrList.add(addrs);
        
        Test.startTest();
        if(!addrList.isEmpty()){
            database.insert(addrList);
        }
        
        prescriberAccnt.Primary_Bill_To_Address__c = addrs.Id;
        Update prescriberAccnt;
        
        Test.stopTest();
        
    }
    
    @isTest
    public static void updateAddrOnPrescriber(){
        Account accRec = [Select Id, Primary_Bill_To_Address__c, Primary_Ship_To_Address__c from Account where FirstName = 'Med Adv FirstName' and recordType.Name = 'Prescriber'];
        List<Address__c> addrList = new List<Address__c>();
        
        Address__c addrs = new Address__c();
        addrs.Account__c = accRec.Id;
        addrs.City__c = 'Test City';
        addrs.County__c = 'Test County';
        addrs.State__c = 'AK';
        addrs.Zip_Postal_Code__c = '34567';
        addrs.Street_Address_1__c = System.Now() + ' Test Address';
        addrs.Primary_Flag__c = true;
        addrs.Address_Type__c = 'SHIP_TO';
        addrs.Oracle_Address_ID__c = '10987';
        addrList.add(addrs);
        if(!addrList.isEmpty()){
            database.insert(addrList);
        }
        
        Test.startTest();
        accRec.Primary_Bill_To_Address__c = addrs.Id;
        update accRec;
        Prescriber_Territory_Event__e prescriberTerritoryEvent = new Prescriber_Territory_Event__e(Prescriber_Id__c = accRec.Id, Territory_Name__c = 'Terr Name',Territory_Name_SF__c= 'Territory name');
        Database.SaveResult generationChangeEventResult = EventBus.publish(prescriberTerritoryEvent);   
        Test.stopTest();
    }
    
}