@isTest(seeAllData = false)
public class ClsOpenOpptyDetailControllerTest {

    public static Account testAccount;
    public static User dataIntgUser;
    public static List<Opportunity> opptyList;
    
    @testSetup static void setup() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Data Integrator'];
        dataIntgUser = new User(Alias = 'newUser', Email='newDataIntgUser@testorg.com',
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', ProfileId = p.Id,
                                TimeZoneSidKey='America/Los_Angeles', UserName='newDataIntgUser@testorg.com');
        
        Id payorRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
        
        Account payorAccount=new Account();
        payorAccount.name = 'Test Payor';
        payorAccount.Phone = '123456789';
        payorAccount.DOB__c = Date.newInstance(1992, 04, 23);
        payorAccount.Days_For_Sensor_2_Box_Shipment__c= 2;
        payorAccount.recordtypeid= payorRecordType;
        insert payorAccount;
        
        opptyList = TestDataBuilder.getOpportunityList(1, payorAccount.Id, payorAccount.Id);
        insert opptyList;
    }
    
    @isTest
    public static void getOpenOpptyDetailsTest(){
        testAccount = [Select Id, Name from Account where name ='Test Payor' and phone = '123456789' limit 1];
        List<Opportunity> opptyList = new List<Opportunity>();
        test.startTest();
        opptyList = ClsOpenOpptyDetailController.getOpenOpptyDetails(testAccount.Id);
        system.assert(opptyList !=null);
        test.stopTest();
    }
    
    @isTest
    public static void updateOpptyRecordsTest(){
        opptyList = [Select Id, Name,StageName from Opportunity where AccountId in : ([Select Id from Account where name ='Test Payor' and phone = '123456789' limit 1])];
        test.startTest();
        ClsOpenOpptyDetailController.updateOpptyRecords(opptyList);
        test.stopTest();
        if(opptyList !=null){
            system.assertEquals(ClsApexConstants.OPPTY_STAGE_QUOTE_APPROVED, opptyList[0].StageName);
        }
    }
}