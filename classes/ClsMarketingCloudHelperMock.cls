/**
* MarketingCloudHelperMock
* Mock class for tests hitting the queueMO api on Marketing Cloud
* @author Katie Wilson(Sundog)
* @date 09/28/18
*/
global class ClsMarketingCloudHelperMock  implements HttpCalloutMock {
    global HTTPResponse respond(HttpRequest request) {
        //Mock response for sending sms
        if(request.getEndpoint()=='https://www.exacttargetapis.com/sms/v1/queueMO'){
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody('{"results": [{"identifier": "cTQ4TEI5NHpHa0daYmM2bEdQakI2QTo3Njow","mobileNumber": "17014290378","result": "OK"}]}');
            response.setStatusCode(202);
            return  response;
        }
        //Mock response for get the opt statuses
        if(request.getHeader('SOAPAction')=='Retrieve'){
            HttpResponse response = new HttpResponse();
            
            response.setBody('<?xml version="1.0" encoding="UTF-8"?>'+
                        '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wsa="http://schemas.xmlsoap.org/ws/2004/08/addressing" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">'+
                        '<soap:Header><wsa:Action>RetrieveResponse</wsa:Action><wsa:MessageID>urn:uuid:01017950-4517-4eb2-be26-1fd6bbce6e1a</wsa:MessageID>'+
                        '<wsa:RelatesTo>urn:uuid:ea9732e7-345f-471b-8bf0-1684f4cfd233</wsa:RelatesTo>'+
                        '<wsa:To>http://schemas.xmlsoap.org/ws/2004/08/addressing/role/anonymous</wsa:To><wsse:Security>'+
                        '<wsu:Timestamp wsu:Id="Timestamp-89bea94f-5ecb-4005-8be2-d3226a653b85">   <wsu:Created>2018-10-12T20:23:16Z</wsu:Created>'+
                        '<wsu:Expires>2018-10-12T20:28:16Z</wsu:Expires>     </wsu:Timestamp>   </wsse:Security>  </soap:Header>   <soap:Body>'+
                        '<RetrieveResponseMsg xmlns="http://exacttarget.com/wsdl/partnerAPI">    <OverallStatus>OK</OverallStatus>'+
                        '<RequestID>e775af55-a9d4-439b-ba51-d528a0e97e53</RequestID>   <Results xsi:type="DataExtensionObject"> <PartnerKey xsi:nil="true" />'+
                        '<ObjectID xsi:nil="true" /><Type>DataExtensionObject</Type> <Properties> <Property><Name>MobileNumber</Name><Value>9234567890</Value>'+
                        '</Property> <Property><Name>OptInStatusID</Name><Value>0</Value> </Property><Property><Name>OptOutStatusID</Name><Value>1</Value>'+
                        '</Property></Properties></Results><Results xsi:type="DataExtensionObject"> <PartnerKey xsi:nil="true" /><ObjectID xsi:nil="true" />'+
                        '<Type>DataExtensionObject</Type><Properties><Property><Name>MobileNumber</Name><Value>0987654321</Value></Property><Property><Name>OptInStatusID</Name>'+
                        '<Value>2</Value></Property><Property><Name>OptOutStatusID</Name><Value>0</Value></Property></Properties></Results><Results xsi:type="DataExtensionObject">'+
                        '<PartnerKey xsi:nil="true" /><ObjectID xsi:nil="true" />'+
                        '<Type>DataExtensionObject</Type><Properties><Property><Name>MobileNumber</Name><Value>9234567894</Value></Property><Property><Name>OptInStatusID</Name>'+
                        '<Value>1</Value></Property><Property><Name>OptOutStatusID</Name><Value>0</Value></Property></Properties></Results>'+
                        '</RetrieveResponseMsg></soap:Body></soap:Envelope>');
            response.setStatusCode(200);
            return  response;
        }
        
        //Mock response for the auth call
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{"accessToken": "7Tq6M8U8LHDyurIZ4Q9gTPtg","expiresIn": 3479}');
        response.setStatusCode(200);
        return  response;
    }
    
}