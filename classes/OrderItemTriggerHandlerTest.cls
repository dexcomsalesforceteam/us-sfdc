@isTest
public class OrderItemTriggerHandlerTest {
    
    @testSetup
    public static void testSetUp(){
        
        //create PriceBooks
        // Cash Pricebook for account
        List<Pricebook2> pbInsertList = new List<Pricebook2>();
        
        Pricebook2 newPricebookCash = new Pricebook2(Name = 'Cash Price List',IsActive = true,Cash_Price_Book__c = TRUE,Customer_Type__c = 'Cash'  );
        pbInsertList.add(newPricebookCash);
        
        Pricebook2 updatePricebookCash = new Pricebook2(Name = 'MT24475',IsActive = true,Customer_Type__c = 'Cash',Dual_Pricebook_Eligible__c = TRUE   );
        pbInsertList.add(updatePricebookCash);
        
        insert pbInsertList;
        
        
        ID standardPBID = Test.getStandardPricebookId(); 
        list<PricebookEntry> pbeList = new list<PricebookEntry>();
        
        Product2 prod = new Product2(Name = 'STS-OR-003',IsActive = true, Generation__c = 'G4');
        insert prod;
        PricebookEntry standardPriceEntry = new PricebookEntry(UseStandardPrice = false,Pricebook2Id=standardPBID,Product2Id=prod.id,IsActive=true,UnitPrice=50.0);
        insert standardPriceEntry;
        PricebookEntry pbe = new PricebookEntry(UseStandardPrice = false,Pricebook2Id=newPricebookCash.Id,Product2Id=prod.id,IsActive=true,UnitPrice=100.0);
        pbeList.add(pbe);
        
        // Pricebook update on Line Item
        PricebookEntry pbe2 = new PricebookEntry(UseStandardPrice = false,Pricebook2Id=updatePricebookCash.Id,Product2Id=prod.id,IsActive=true,UnitPrice=200.0);
        pbeList.add(pbe2);
        insert pbeList;
           
       Rules_Matrix__c rm = new Rules_Matrix__c();
       rm.Generation__c = 'G5';
       rm.Consumer_Payor_Code__c = 'K';
       rm.Quantity_Boxes__c = 2;
       rm.Rule_Criteria__c = 'Quarterly Limit';
       rm.Is_True_or_False__c = true;
       insert rm;
                                           
        Account prescriberAccount=new account();
        prescriberAccount.lastname='prescriber Account';
        prescriberAccount.recordtypeId=ClsApexUtility.getRecordTypeIdByDeveloperName('Account', 'Prescriber');
        prescriberAccount.Inactive__c =false;    
        insert prescriberAccount;
        Test.startTest();
        //create Consumer Account
        Account testAccount=TestDataBuilder.testAccount();
        testAccount.Default_Price_Book__c=newPricebookCash.Id;
        testAccount.PersonEmail = 'Test@gmail.comOrderItemTriggerHandler';
        testAccount.BillingStreet='abc';
        testAccount.BillingCity ='Bang';
        testAccount.BillingState ='KN';
        testAccount.BillingCountry ='US';
        testAccount.BillingPostalCode='56003';
        testAccount.ShippingStreet  ='Bang';
        testAccount.ShippingCity='Bang';
        testAccount.ShippingState='KN';
        testAccount.ShippingCountry='US';
        testAccount.ShippingPostalCode='56003';
        testAccount.Party_ID__c ='123344';
        testAccount.Prescribers__c=prescriberAccount.id;
        testAccount.Receiver_Quantity_Prescribed__c = 20;
        testAccount.Sensor_Quantity_Prescribed__c = 20;
        testAccount.Transmitter_Quantity_Prescribed__c = 20;
        testAccount.RecordTypeId = ClsApexUtility.getRecordTypeIdByDeveloperName('Account', 'Consumers');
        testAccount.Customer_Type__c='Commercial';
        
        insert testAccount;
        
        //create Primary address
        Address__c addrs2 = TestDataBuilder.getAddressList(testAccount.Id, true,'SHIP_TO', 1)[0];
        addrs2.Oracle_Address_ID__c ='123455';
        insert addrs2;
        
        testAccount.Primary_Ship_To_Address__c =addrs2.Id;
        testAccount.Sensor_Quantity_Prescribed__c = 10;
        testAccount.Transmitter_Quantity_Prescribed__c = 10;
        testAccount.Receiver_Quantity_Prescribed__c = 10;
        update testAccount;
        Test.stopTest();
        system.debug('-------------------------------testAccount'+testAccount.Primary_Ship_To_Address__c);
        system.debug('------------------------------- addrs2.Oracle_Address_ID__c'+addrs2.Oracle_Address_ID__c);
        
    }
    
    @isTest
    Public static void orderCreationTest(){
        Account accountRec = [Select Id, Default_Price_Book__c, Primary_Ship_To_Address__c from account where PersonEmail = 'Test@gmail.comOrderItemTriggerHandler' limit 1];
        PricebookEntry pricebookStandardEntry = [Select Id, Product2Id,pricebook2Id from PricebookEntry where Pricebook2.Name = 'Cash Price List'];
        Pricebook2 pricebookUpdate = [Select id from Pricebook2 where Name = 'MT24475' limit 1];
        
        
        Order o = new Order();
        o.Name = 'PriceList Test Order ';
        o.Status = 'Draft';
        o.type = 'Standard Sales Order';
        o.EffectiveDate = system.today();
        o.EndDate = system.today() + 4;
        o.AccountId = accountRec.id;
        o.Price_Book__c =  accountRec.Default_Price_Book__c ;
        o.Shipping_Address__c = accountRec.Primary_Ship_To_Address__c;
        
        Insert o;
        Test.startTest();
        List<OrderItem> orderItemList = new List<OrderItem>();
        OrderItem oi1 = new OrderItem();
        oi1.OrderId = o.id;
        oi1.Quantity = 2;
        oi1.Product2id = pricebookStandardEntry.Product2Id;
        oi1.PricebookEntryId=pricebookStandardEntry.id;
        oi1.unitPrice = 50.0;
        //insert oi1;
        orderItemList.add(oi1);
        
        OrderItem oi2 = new OrderItem();
        oi2.OrderId = o.id;
        oi2.Quantity = 1;
        oi2.Product2id = pricebookStandardEntry.Product2Id;
        oi2.PricebookEntryId=pricebookStandardEntry.id;
        oi2.unitPrice = 50.0;
        //insert oi2;
        orderItemList.add(oi2);
        
        
        insert orderItemList;
        
        oi1.Price_Book__c = pricebookUpdate.Id; //get updatePricebookCash
        update oi1;
        
        //oi1.Price_Book__c = null;
       // update oi1;
        
        delete oi2;
        
        Test.stopTest();    
    }
    
    @isTest
    Public static void BundleCheck(){
        String consumerRecordTypeId = ClsApexUtility.getRecordTypeIdByDeveloperName('Account','Consumers');        
        String payorRecordTypeId = ClsApexUtility.getRecordTypeIdByDeveloperName('Account','Payor');
        
        //Create Pricebook
        Map<String, Id> customPricebookMap = ClsTestDataFactory.createCustomPricebook(new List <String>{'Aetna Health Plan'});
        String customPricebookId = customPricebookMap.get('Aetna Health Plan');
        
        Account mdcrPayor = new Account();
        mdcrPayor.RecordTypeId = payorRecordTypeId;
        mdcrPayor.Name = 'Med Adv Test Payor';
       // mdcrPayor.Payor_Code_Commercial__c = 'K';
        mdcrPayor.Default_Price_Book_K__c = customPricebookId;
        mdcrPayor.Claim_Address__c = '1 Main Street, San Diego, CA - 92127';
        mdcrPayor.DOB__c = Date.newInstance(2016, 12, 9);
        insert mdcrPayor;
        
        Account mdcrAccnt = new Account();
        mdcrAccnt.FirstName = 'Med Adv FirstName';
        mdcrAccnt.LastName = 'Med Adv LastName';
        mdcrAccnt.RecordtypeId = consumerRecordTypeId;
        mdcrAccnt.Customer_Type__c = 'Commercial';
        mdcrAccnt.DOB__c = Date.newInstance(2016, 12, 9);
        mdcrAccnt.Payor__c=mdcrPayor.Id;
        Test.startTest();
        insert mdcrAccnt;
        
        //Create New Sensor Product
        Map<String, Id> newProduct = ClsTestDataFactory.createProducts(new List<String> {'STK-MC-001', 'STK-SD-001', 'BUN-GF-003', 'STS-GL-041', 'MT-MC-SUB'});
        //Create Pricebook EntryPair
        Map<Id, Decimal> newProductIdToPriceMap = new Map<Id, Decimal>();
        for(String productName : newProduct.keySet())
            newProductIdToPriceMap.put(newProduct.get(productName), 125.00);
        //Add new pricebookentry item
        Map<Id, Id> newProductIdToPbeId = ClsTestDataFactory.createCustomPricebookEntries(newProductIdToPriceMap, customPricebookId);
        Test.stopTest();
        System.Debug('**** TPS:EH 1.1 Account.Customer_Type__c =' + mdcrAccnt.Customer_Type__c);
        System.Debug('**** TPS:EH 1.2 Account.Consumer_Payor_Code__c =' + mdcrAccnt.Consumer_Payor_Code__c);
        System.Debug('**** TPS:EH 1.3 Account.G4_Exception_Flag__c =' + mdcrAccnt.G4_Exception_Flag__c);
        
    }    
   
}