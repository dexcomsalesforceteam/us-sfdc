@isTest
public class Test_ProcessEligibleDateForSensorOrder {
    public static Id accountId;
    public static void testDataBuilder(){
        //Insert test Account
        id recId = ClsApexUtility.getRecordTypeIdByDeveloperName('Account','Consumers');
        id payorRecId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
        Account consumer = TestDataBuilder.getAccountListConsumer(1, recId)[0];
        
        
        Account acc = TestDataBuilder.getAccountList(1,payorRecId)[0];
        
        insert new list <Account>{consumer, acc};
            consumer.Payor__c = acc.id;
        update consumer;
        
        accountId = consumer.id;
        //Insert Test Order Headers
        List<Order_Header__c> thisOHList = new List<Order_Header__c>();
        Order_Header__c orderHeaderSTS = new Order_Header__c(Account__c = consumer.Id,
                                                             Order_Id__c = '12340',
                                                             Order_Type__c = 'MC Standard Sales Order'
                                                            );
        thisOHList.add(orderHeaderSTS);
        Order_Header__c orderHeaderSTT = new Order_Header__c(Account__c = consumer.Id,
                                                             Order_Id__c = '12341',
                                                             Order_Type__c = 'MC Standard Sales Order'
                                                            );
        
        thisOHList.add(orderHeaderSTT);
        insert thisOHList;  
        //Insert Test OLI
        List<Order_Item_Detail__c> thisOLIList = new List<Order_Item_Detail__c>();
        Order_Item_Detail__c newOLISTS = new Order_Item_Detail__c(Order_Header__c = orderHeaderSTS.Id, 
                                                                  Item_Number__c = 'STS-OR-003',
                                                                  Shipping_Date__c = System.today(), Quantity_Shipped__c = 3);
        thisOLIList.add(newOLISTS);
        Order_Item_Detail__c newOLISTT = new Order_Item_Detail__c(Order_Header__c = orderHeaderSTT.Id, 
                                                                  Item_Number__c = 'STT-OR-001',
                                                                  Shipping_Date__c = System.today()-1 , Quantity_Shipped__c = 3);
        thisOLIList.add(newOLISTT);
        
        insert thisOLIList;
        
        //Insert Rules Matrix
        List<Rules_Matrix__c> rulesMatrixList = new List<Rules_Matrix__c>();
        Rules_Matrix__c thisRule = new Rules_Matrix__c();
        thisRule.Account__c = acc.id; 
        thisRule.Quantity_Boxes__c = 3;
        thisRule.Rule_Criteria__c = 'Days For Next Order';
        thisRule.Is_True_or_False__c = true;
        thisRule.Duration_In_Days__c = 23;
        thisRule.Product_Type__c = 'Sensor';
        thisRule.Generation__c = 'G6';
        thisRule.Consumer_Payor_Code__c = 'A';
        rulesMatrixList.add(thisRule);
        
        Rules_Matrix__c thisRule1 = new Rules_Matrix__c();
        thisRule1.Account__c = acc.id; 
        thisRule1.Quantity_Boxes__c = 3;
        thisRule1.Rule_Criteria__c = 'Days For Next Order';
        thisRule1.Is_True_or_False__c = true;
        thisRule1.Duration_In_Days__c = 23;
        thisRule1.Product_Type__c = 'Sensor';
        thisRule1.Generation__c = 'G6';
        thisRule1.Consumer_Payor_Code__c = 'K';
        rulesMatrixList.add(thisRule1);
        insert rulesMatrixList;
        
    }
    
    @isTest
    public static void testCase1(){
        test.startTest();
        testDataBuilder();
        Account_Next_Date_For_Sensor_Order_Event__e thisEvent = new Account_Next_Date_For_Sensor_Order_Event__e();
        
        List<Account> thisAccounts = new List<Account> ([Select id, Payor__c, Latest_Sensors_Ship_Date__c, Latest_Qty_Of_Sensors_Shipped__c, Latest_Transmitter_Generation_Shipped__c,
                                                         Latest_Sensors_Order_Number__c, Next_Possible_Date_For_Sensors_Order__c, Next_Possible_Date_For_Sensor_Order__c from Account
                                                         where id = : accountId limit 1]);
        System.debug('thisAccounts >> ' + thisAccounts );
        thisEvent.Account_Id__c = thisAccounts[0].Id;
        thisEvent.Payor_Id__c = thisAccounts[0].Payor__c;
        // thisEvent.ReplayId = String.valueOf(thisAccounts[0].id);
        Database.SaveResult sr = EventBus.publish(thisEvent);
        test.stopTest();
    } 
    
    @isTest
    public static void testSkipLogicTest(){
        test.startTest();
        testDataBuilder();
        Account_Next_Date_For_Sensor_Order_Event__e thisEvent = new Account_Next_Date_For_Sensor_Order_Event__e();
        
        List<Account> thisAccounts = new List<Account> ([Select id, Payor__c, Latest_Sensors_Ship_Date__c, Latest_Qty_Of_Sensors_Shipped__c, Latest_Transmitter_Generation_Shipped__c,
                                                         Latest_Sensors_Order_Number__c, Next_Possible_Date_For_Sensors_Order__c, Next_Possible_Date_For_Sensor_Order__c from Account
                                                         where id = : accountId limit 1]);
        
        List<Rules_Matrix__c> rulesMatrixList = [Select Id from Rules_Matrix__c where Account__c = : thisAccounts[0].Payor__c];
        delete rulesMatrixList;
        System.debug('thisAccounts >> ' + thisAccounts );
        thisEvent.Account_Id__c = thisAccounts[0].Id;
        thisEvent.Payor_Id__c = thisAccounts[0].Payor__c;
        // thisEvent.ReplayId = String.valueOf(thisAccounts[0].id);
        Database.SaveResult sr = EventBus.publish(thisEvent);
        test.stopTest();
    }
}