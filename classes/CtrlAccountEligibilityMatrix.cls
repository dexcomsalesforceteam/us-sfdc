public  with sharing class CtrlAccountEligibilityMatrix {
    
    @auraEnabled
    public static  Map<String, String> getAccountEligibilityMatrixDetails(String accountId) {
		Map<String, String> fieldValueMap = new Map<String, String>();
        List<Account> accntList = new List<Account>([SELECT Latest_Transmitter_Generation_Shipped__c,Num_Of_Days_Left_For_Sensors_Order__c,
                                                     		Num_Of_Days_Left_For_Transmitter_Order__c,	Last_Transmitter_Ship_Date__c, 
                                                     		Last_Receiver_Ship_Date__c, Latest_Receiver_Generation_Shipped__c,Receiver_OOW__c,
                                                     		Latest_Insurance_Transmitter_Order__c,Latest_Insurance_Receiver_Order__c,
                                                     		Latest_Insurance_Sensor_Order__c,Transmitter_OOW__c,Sensor_Qty_Remaining_With_Current_CMN__c,
                                                     		Tx_Qty_Remaining_With_Current_CMN__c,Rx_Qty_Remaining_With_Current_CMN__c,
                                                     		Latest_Sensor_Order__c,Latest_Receiver_Order__c,Latest_Transmitter_Order__c
                                                     		FROM ACCOUNT WHERE Id = :accountId]);
        
        List<Eligibility_Upgrade_Matrix__mdt> eumList = new List<Eligibility_Upgrade_Matrix__mdt>();
        
        if(!accntList.isEmpty()) {
            Account accnt = accntList[0];
            
            //Get Order Header Details
            Map<String,String> orderMap = getOrderHeaderMapping(accnt);
            
            String currentReceiverGen = orderMap.containsKey('currentReceiverGen')?orderMap.get('currentReceiverGen'):'';
            String currentTransmitterGen = orderMap.containsKey('currentTransmitterGen')?orderMap.get('currentTransmitterGen'):'';
            
            //Generation
            fieldValueMap.put('currentTransmitterGen', orderMap.containsKey('currentTransmitterGen')?orderMap.get('currentTransmitterGen'):'');
            fieldValueMap.put('currentReceiverGen', orderMap.containsKey('currentReceiverGen')?orderMap.get('currentReceiverGen'):'');
            fieldValueMap.put('currentSensorGen', orderMap.containsKey('currentSensorGen')?orderMap.get('currentSensorGen'):'');
            
            //Shipping Quantity
            /*fieldValueMap.put('QtyOfTransmitterShipped', orderMap.containsKey('QtyOfTransmitterShipped')?orderMap.get('QtyOfTransmitterShipped'):'');
            fieldValueMap.put('QtyOfReceiverShipped', orderMap.containsKey('QtyOfReceiverShipped')?orderMap.get('QtyOfReceiverShipped'):'');
            fieldValueMap.put('QtyOfSensorShipped', orderMap.containsKey('QtyOfSensorShipped')?orderMap.get('QtyOfSensorShipped'):'');*/
            
            //Add Quantity Remaining
            fieldValueMap.put('sensorQtyRemaining',String.ValueOf(accntList[0].Sensor_Qty_Remaining_With_Current_CMN__c));
			fieldValueMap.put('transmitterQtyRemaining',String.ValueOf(accntList[0].Tx_Qty_Remaining_With_Current_CMN__c));
			fieldValueMap.put('receiverQtyRemaining',String.ValueOf(accntList[0].Rx_Qty_Remaining_With_Current_CMN__c));
            
            String receiverInW = accnt.Receiver_OOW__c;
            String transmitterInW = accnt.Transmitter_OOW__c;
            String numOfDaysRemainingForNextSensorsOrder;
            numOfDaysRemainingForNextSensorsOrder = String.ValueOf(accnt.Num_Of_Days_Left_For_Sensors_Order__c); 
            Date TodayDate = Date.today();
            
			if(accnt.Last_Transmitter_Ship_Date__c != null) {
           		Integer warrentyLeftOverDaysTransmitterInt = 182- accnt.Last_Transmitter_Ship_Date__c.daysBetween(TodayDate);
                String warrentyLeftOverDaysTransmitter =   String.valueOf(warrentyLeftOverDaysTransmitterInt);
                fieldValueMap.put('warrentyLeftOverDaysTransmitter',warrentyLeftOverDaysTransmitter);
            } else {
                String warrentyLeftOverDaysTransmitter =   null;
                fieldValueMap.put('warrentyLeftOverDaysTransmitter',warrentyLeftOverDaysTransmitter);
            }
            
            if( accnt.Last_Receiver_Ship_Date__c != null) {
            	Integer warrentyLeftOverDaysReceiverInt = 365 - accnt.Last_Receiver_Ship_Date__c.daysBetween(TodayDate);
                String warrentyLeftOverDaysReceiver= String.valueOf(warrentyLeftOverDaysReceiverInt)  ;
                fieldValueMap.put('warrentyLeftOverDaysReceiver',warrentyLeftOverDaysReceiver);
            } else{
                String warrentyLeftOverDaysReceiver= null ;
                fieldValueMap.put('warrentyLeftOverDaysReceiver',warrentyLeftOverDaysReceiver);
            }
          
            eumList = [SELECT Current_Receiver__c, Current_Transmitter__c,G6_Force_Order_Auth_Code__c,G6_Force_Purchase_R_TX_S__c, G6_Force_Purchase_TX_S__c,Receiver_in_Warranty__c, Receiver_Options__c, Transmitter_in_Warranty__c,Transmitter_Options__c 
                      FROM Eligibility_Upgrade_Matrix__mdt WHERE Current_Receiver__c = :currentReceiverGen AND Current_Transmitter__c = :currentTransmitterGen AND Receiver_in_Warranty__c = :receiverInW  AND Transmitter_in_Warranty__c = :transmitterInW ];
            

            String transmitterOption;
            String receiverOption;
            String G6ForceOrderAuthCode;
            String G6ForcePurchaseRTxS;
            String G6ForcePurchaseTxS;
           
           if(!eumList.isEmpty()) {
               Eligibility_Upgrade_Matrix__mdt eum = eumList[0]; 
               transmitterOption = eum.Transmitter_Options__c;
               receiverOption = eum.Receiver_Options__c;
               G6ForceOrderAuthCode = eum.G6_Force_Order_Auth_Code__c;
               G6ForcePurchaseRTxS = eum.G6_Force_Purchase_R_TX_S__c;
               G6ForcePurchaseTxS  = eum.G6_Force_Purchase_TX_S__c;
           }
           
           //fieldValueMap.put('currentReceiverGen',currentReceiverGen);
           //fieldValueMap.put('currentTransmitterGen',currentTransmitterGen);
		   fieldValueMap.put('receiverInW',receiverInW);
           fieldValueMap.put('transmitterInW',transmitterInW);          
           fieldValueMap.put('transmitterOption', transmitterOption);
           fieldValueMap.put('receiverOption', receiverOption);
           fieldValueMap.put('G6ForceOrderAuthCode',G6ForceOrderAuthCode);
           fieldValueMap.put('G6ForcePurchaseRTxS',G6ForcePurchaseRTxS);
           fieldValueMap.put('G6ForcePurchaseTxS',G6ForcePurchaseTxS);
           fieldValueMap.put('numOfDaysRemainingForNextSensorsOrder', numOfDaysRemainingForNextSensorsOrder); 
       }
       return fieldValueMap;
    }
    
    private static Map<String,String> getOrderHeaderMapping(Account acc) {
    	Map<String,String> orderMap = new Map<String,String>();
        
        Set<Id> orderIds = new Set<Id>();
        
        if(String.isNotBlank(acc.Latest_Insurance_Transmitter_Order__c)) { 
        	orderIds.add(acc.Latest_Insurance_Transmitter_Order__c);
        }
        
        if(String.isNotBlank(acc.Latest_Insurance_Receiver_Order__c)) { 
        	orderIds.add(acc.Latest_Insurance_Receiver_Order__c);
        }
        
        if(String.isNotBlank(acc.Latest_Insurance_Sensor_Order__c)) { 
        	orderIds.add(acc.Latest_Insurance_Sensor_Order__c);
        }
        
        if(String.isNotBlank(acc.Latest_Transmitter_Order__c)) { 
        	orderIds.add(acc.Latest_Transmitter_Order__c);
        }
        
        if(String.isNotBlank(acc.Latest_Receiver_Order__c)) { 
        	orderIds.add(acc.Latest_Receiver_Order__c);
        }
        
        if(String.isNotBlank(acc.Latest_Sensor_Order__c)) { 
        	orderIds.add(acc.Latest_Sensor_Order__c);
        }
        
        if(!orderIds.isEmpty()) {
            Map<Id,Order_Header__c> tempOrderMap = new Map<Id,Order_Header__c>([Select Id,Latest_Transmitter_Generation__c,Price_List__c,
                                                                                	   Latest_Sensor_Generation__c,Latest_Receiver_Generation__c,
                                                                                	   Latest_Transmitter_Ship_Date__c,Latest_Receiver_Ship_Date__c,
                                                                                	   Order_Type__c,Latest_Sensors_Ship_Date__c,Latest_Qty_Of_Sensors_Shipped__c,
                                                                                	   Price_List_Oracle_ID__c,Latest_Qty_Of_Receiver_Shipped__c,
                                                                                	   Latest_Qty_Of_Transmitter_Shipped__c,Terms__c,Effective_Transmitter_Quantity__c	
                                                                               		   from Order_Header__c
                                                                              		   where Id IN :orderIds]);
    		
            /*if(tempOrderMap.containsKey(acc.Latest_Insurance_Transmitter_Order__c)) {
                Order_Header__c tempOrder = tempOrderMap.get(acc.Latest_Insurance_Transmitter_Order__c);
                
                //orderMap.put('currentTransmitterGen',tempOrder.Latest_Transmitter_Generation__c);
                
                if(tempOrder.Effective_Transmitter_Quantity__c != null) {
                    orderMap.put('QtyOfTransmitterShipped',String.valueOf(tempOrder.Effective_Transmitter_Quantity__c));
                }
            }*/
            
            if(tempOrderMap.containsKey(acc.Latest_Transmitter_Order__c)) {
                Order_Header__c tempOrder = tempOrderMap.get(acc.Latest_Transmitter_Order__c);
                
                orderMap.put('currentTransmitterGen',tempOrder.Latest_Transmitter_Generation__c);
            }
            
            /*if(tempOrderMap.containsKey(acc.Latest_Insurance_Receiver_Order__c)) {
                Order_Header__c tempOrder = tempOrderMap.get(acc.Latest_Insurance_Receiver_Order__c);
                
                //orderMap.put('currentReceiverGen',tempOrder.Latest_Receiver_Generation__c);
                
                if(tempOrder.Latest_Qty_Of_Receiver_Shipped__c != null) {
                    orderMap.put('QtyOfReceiverShipped',String.valueOf(tempOrder.Latest_Qty_Of_Receiver_Shipped__c));
                }
            }*/
            
            if(tempOrderMap.containsKey(acc.Latest_Receiver_Order__c)) {
                Order_Header__c tempOrder = tempOrderMap.get(acc.Latest_Receiver_Order__c);
                
                orderMap.put('currentReceiverGen',tempOrder.Latest_Receiver_Generation__c);
            }
            
            /*if(tempOrderMap.containsKey(acc.Latest_Insurance_Sensor_Order__c)) {
                Order_Header__c tempOrder = tempOrderMap.get(acc.Latest_Insurance_Sensor_Order__c);
                
                //orderMap.put('currentSensorGen',tempOrder.Latest_Sensor_Generation__c);
                
                if(tempOrder.Latest_Qty_Of_Sensors_Shipped__c != null) {
                    orderMap.put('QtyOfSensorShipped',String.valueOf(tempOrder.Latest_Qty_Of_Sensors_Shipped__c));
                }
            }*/
            
            if(tempOrderMap.containsKey(acc.Latest_Sensor_Order__c)) {
                Order_Header__c tempOrder = tempOrderMap.get(acc.Latest_Sensor_Order__c);
                
                orderMap.put('currentSensorGen',tempOrder.Latest_Sensor_Generation__c);
            }    
        }
        system.debug('<<<orderMap>>>>>'+orderMap);
        return orderMap;
	}
}