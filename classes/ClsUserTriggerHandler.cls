/*******************************************************************************************************************
@Author          : Jagan Periyakaruppan
@Date Created   : 03/06/2018
@Description    : User trigger handler to process User updates
********************************************************************************************************************/   
public class ClsUserTriggerHandler{
    
    //Method will group the Order by Id and updates the tracking number
    public static void CheckFieldUpdates (Map<Id, User> newTriggerUsers, Map<Id, User> oldTriggerUsers)
    {
        
        List<User> updateRecords = new List<User>();
        //Get profile of currently logged in user
        String profileName = [Select Id, Name from Profile where Id = :UserInfo.getProfileId()].Name;
        
        //profile check
        if(profileName != 'Data Integrator' && profileName != 'System Administrator'){
                
                //get all managed package fields on user
                sObjectType UserType = Schema.getGlobalDescribe().get('User');
                Map<String,Schema.SObjectField> mfields = UserType.getDescribe().fields.getMap();
                for(User newUser : newTriggerUsers.values())
                {
                    User oldUser =   oldTriggerUsers.get(newUser.Id);
                    for(Schema.SObjectField us: mfields.values()){
                        Schema.DescribeFieldResult field = us.getDescribe();
                        if(field.isUpdateable()){
                            string fieldName = field.getName();
                            Pattern regEx = Pattern.compile('^\\w*?__\\B\\w*__');
                             Matcher filter = regEx.matcher(fieldName);
                            if(!filter.find() ){ // removed managed package updates as they contain namespace                               
                                if(profileName == 'Inside Sales Mgmt' && fieldName != 'Title' && fieldName != 'ManagerId' && fieldName != 'User_Location__c' && String.valueOf(newUser.get(fieldName)) != String.valueOf(oldUser.get(fieldName))){
                                        system.debug('profileName>>' + profileName);
                                        system.debug('fieldName>>' + fieldName);
                                        system.debug('String.valueOf(newUser.get(fieldName))>>' + String.valueOf(newUser.get(fieldName)));
                                        system.debug('String.valueOf(oldUser.get(fieldName))>>' + String.valueOf(oldUser.get(fieldName)));
                                        newUser.addError('You can update only the User Title, Manager and User Location.');
                                }                                
                            }
                        }
                    }
                }
        }
    }
}