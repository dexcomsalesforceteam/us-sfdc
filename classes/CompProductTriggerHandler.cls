/***************************************************************************************************************
@author Abhishek Parghi
@date 1/27/2017
@description: Apex Class for Comp Product Requests Workflow.

@author Jagan Periyakaruppan
@date 2/17/2017
@description: Modified the code to pull only the fields on the Comp Product Request, which have values in them          
******************************************************************************************************************/
public class CompProductTriggerHandler {
    public static void NewDXNotesAfterUpdate(List<Comp_Product_Request__c> CompsNew){
        List<Dx_notes__c> dxNotesList = new List<Dx_notes__c>();
        List<Comp_Product_Request__c> filteredCompReqList = new List<Comp_Product_Request__c>();
        Map<Id, Map<String, String>> compReqNonNullFieldsMap = new Map<Id, Map<String, String>>();
        Map<Id, Id> compProdReqIdToAccntIdMap = new Map<Id, Id> ();
        
        //Process records only Approved and Rejected records and the ones, which have Account tied with Party Id populated
        for(Comp_Product_Request__c compProd: CompsNew) 
        {
            system.debug('******Entering the first for loop'+compProd);
            if(compProd.Customer_Account_No__c != null && compProd.Party_Id__c != null &&(compProd.Approval_Status__c =='Approved' || compProd.Approval_Status__c =='Rejected'))
            {
                filteredCompReqList.add(compProd);
                compProdReqIdToAccntIdMap.put(compProd.Id, compProd.Customer_Account_No__c);
                system.debug('******Comp Req Id is ' + compProd.Id);
                
            } 
            system.debug('******size of  compProdReqIdToAccntIdMap is ' + compProdReqIdToAccntIdMap.size());
            system.debug('******size of  filteredCompReqList is ' + filteredCompReqList.size());
        }
        
        //Proceed further only when there are any approved or rejected Comp product request records
        if(filteredCompReqList.size() > 0)
        {
            
            system.debug('******Entering the filtered loop');
            //For each Comp Request prepare a map, which includes only populated fields
            for(Comp_Product_Request__c compProdReq : CompsNew)
            {
                Map<String, String> nonNullFieldsMap = new Map<String, String>();
                if(!String.isBlank(compProdReq.G4_Receiver__c)) 
                    nonNullFieldsMap.put('G4 Receiver ($599.00) Qty - ', compProdReq.G4_Receiver__c);
                if(!String.isBlank(compProdReq.G5_Receiver__c)) 
                    nonNullFieldsMap.put('G5 Receiver ($599.00) Qty - ', compProdReq.G5_Receiver__c);
                if(!String.isBlank(compProdReq.G6_Receiver__c)) 
                    nonNullFieldsMap.put('G6 Receiver ($365) Qty - ', compProdReq.G6_Receiver__c);
                if(!String.isBlank(compProdReq.G4_Transmitter__c))  
                    nonNullFieldsMap.put('G4 Transmitter ($299.00) Qty - ', compProdReq.G4_Transmitter__c);
                if(!String.isBlank(compProdReq.Single_G5_Transmitter__c))   
                    nonNullFieldsMap.put('Single G5 Transmitter ($299) - ', compProdReq.Single_G5_Transmitter__c);
                if(!String.isBlank(compProdReq.G5_Transmitter__c))  
                    nonNullFieldsMap.put('G5 Transmitter Bundle ($599) - ', compProdReq.G5_Transmitter__c);
                if(!String.isBlank(compProdReq.Single_G6_Transmitter__c))   
                    nonNullFieldsMap.put('Single G6 Transmitter ($237.50) - ', compProdReq.Single_G6_Transmitter__c);
                if(!String.isBlank(compProdReq.G6_Transmitter_Bundle__c))  
                    nonNullFieldsMap.put('G6 Transmitter Bundle ($475) - ', compProdReq.G6_Transmitter_Bundle__c);
                if(!String.isBlank(compProdReq.Box_of_Sensors__c))  
                    nonNullFieldsMap.put('Box of Sensors ($349.00) Qty - ', compProdReq.Box_of_Sensors__c);
                if(!String.isBlank(compProdReq.Single_Sensor__c))   
                    nonNullFieldsMap.put('Single Sensor ($89.00) Qty - ', compProdReq.Single_Sensor__c);
                if(!String.isBlank(compProdReq.G6_Box_of_Sensors__c))  
                    nonNullFieldsMap.put('G6 Box of Sensors ($349) - ', compProdReq.G6_Box_of_Sensors__c);
                if(!String.isBlank(compProdReq.G6_Single_Sensor__c))   
                    nonNullFieldsMap.put('G6 Single Sensor ($89) - ', compProdReq.G6_Single_Sensor__c);
                
                //Added additional products per request CRMSF1154
                if(!String.isBlank(compProdReq.Carrying_Case__c))   
                    nonNullFieldsMap.put('Carrying Case ($18.00) Qty - ', compProdReq.Carrying_Case__c);
                if(!String.isBlank(compProdReq.USB_wire__c))   
                    nonNullFieldsMap.put('USB wire ($5.00) Qty - ', compProdReq.USB_wire__c);             
                if(!String.isBlank(compProdReq.Power_Supply_Charger__c))   
                    nonNullFieldsMap.put('Power Supply Charter ($18.00) Qty - ', compProdReq.Power_Supply_Charger__c);           
                
                if(!String.isBlank(compProdReq.Box_of_Lancets__c))   
                    nonNullFieldsMap.put('Box of Lancets ($2.25) Qty - ', compProdReq.Box_of_Lancets__c);           
                if(!String.isBlank(compProdReq.Box_of_Test_Strips__c))   
                    nonNullFieldsMap.put('Box of Test Strips ($7.00) Qty - ', compProdReq.Box_of_Test_Strips__c);           
                if(!String.isBlank(compProdReq.Control_Solution__c))   
                    nonNullFieldsMap.put('Control Solution ($2.25) Qty - ', compProdReq.Control_Solution__c);           
                if(!String.isBlank(compProdReq.Glucose_Monitoring_System__c))   
                    nonNullFieldsMap.put('Glucose Monitoring System ($11.25) Qty - ', compProdReq.Glucose_Monitoring_System__c);           
                
                
                if(compProdReq.Estimated_Amount__c > 0)
                {
                    String currencyAmt = '$' + compProdReq.Estimated_Amount__c;
                    nonNullFieldsMap.put('Estimated Amount - ', currencyAmt);
                }
                if(!(nonNullFieldsMap.isEmpty()))
                {
                    nonNullFieldsMap.put('Approval Status - ', compProdReq.Approval_Status__c);
                    nonNullFieldsMap.put(compProdReq.Approval_Status__c + ' by - ', compProdReq.Approved_Or_Rejected_By__c);
                    compReqNonNullFieldsMap.put(compProdReq.Id,nonNullFieldsMap); 
                }
            }
            system.debug('******compReqNonNullFieldsMap size is ' + compReqNonNullFieldsMap.size());
            
            if(!compReqNonNullFieldsMap.isEmpty())
            {
                //For each of the approved/rejected Comp Prod Request, which have fields populated, we are creating the DxNotes
                system.debug('******Entering the final if loop');
                for(Id compProdReqId : compReqNonNullFieldsMap.keySet())
                {
                    DX_Notes__c dxNote = new DX_Notes__c();
                    String commentString = 'Comp Product Request'+ '\r\n';
                    for (string key:compReqNonNullFieldsMap.get(compProdReqId).keySet())
                    {
                        commentString = commentString + Key + compReqNonNullFieldsMap.get(compProdReqId).get(key)+ '\r\n';
                    }  
                    
                    dxNote.Comments__c = commentString;
                    dxNote.Consumer__c = compProdReqIdToAccntIdMap.get(compProdReqId);
                    dxNotesList.add(dxNote);
                    system.debug('******commentstring is ' + commentString);
                    system.debug('******Consumer__c is ' + compProdReqIdToAccntIdMap.get(compProdReqId));
                }
                if (dxNotesList.size() > 0) 
                {
                    insert dxNotesList;
                }
            }
            
        }
    }
    /**START: Comp Product Order Generation **/
    @future
    public static void generateCompProductOrder(Set<Id> approvedCompProductRequestIds){
        
        //Initialize Collections
        Map<Id, Order> newOrderCreatedMap = new Map<Id, Order>();//Map holds the newly created order for each Comp Product
        List<Comp_Product_Request__c> productListToBeUpdated = new List<Comp_Product_Request__c>();//List of Comp Products to be updated with Order information
        //List<Order> ordersToBeUpdated = new List<Order>();//List holds the list of orders to be updated with Product information
        List<ClsOrderWrapper> orderWrapperList = new List<ClsOrderWrapper>();//List holds the Order wrapper class that are to be created
        Map<String, Comp_Product_Request_O__mdt> compProductDetailsMap = new Map<String, Comp_Product_Request_O__mdt>();// Map of Metadata Type
        List<Comp_Product_Request__c> approvedCompProductRequests = new List<Comp_Product_Request__c>(); // List of Comp Product Req
        //Get the record type id for "US Sales Orders"
        Id orderRecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('US Sales Orders').getRecordTypeId(); 
        
        Map<ID,Comp_Product_Request_O__mdt> compProductDetails = new Map<ID,Comp_Product_Request_O__mdt>(
            [SELECT MasterLabel, Product_Code__c 
             from Comp_Product_Request_O__mdt]);
        
        
        //get the Comp Products 
        String queryString = 'SELECT ID, Customer_Account_No__c, Estimated_Amount__c, Signature_Required__c, Shipping_Address__c, Default_Shipping_Address__c, Shipping_Method__c, Rep_Making_Request_Email__c';
        
        for(Comp_Product_Request_O__mdt mdtRecord : compProductDetails.values()){
            compProductDetailsMap.put(mdtRecord.MasterLabel , mdtRecord);
            queryString = queryString + ', ' + mdtRecord.MasterLabel;
        }
        
        queryString = queryString + ' FROM Comp_Product_Request__c where ID IN :approvedCompProductRequestIds';
        System.debug('queryString >> ' + queryString);
        
        approvedCompProductRequests = Database.Query(queryString);
        system.debug('fetched comp product requests after approval:::'+approvedCompProductRequests.size());
        
        
        if(!approvedCompProductRequests.isEmpty()){
            List<Pricebook2> noChargePriceBookList = new List<Pricebook2>(); //list for No charge Pricebook
            noChargePriceBookList = [SELECT Id,NAME from Pricebook2 where Name LIKE 'No Charge' limit 1];
            
            System.debug('Processing Order Creation');
            
            for(Comp_Product_Request__c thisCompProduct : approvedCompProductRequests){
                ClsOrderWrapper thisOrderWrapper = new ClsOrderWrapper();
                thisOrderWrapper.productQuantityMap = new Map<String,Integer>(); //to save multiple products
                thisOrderWrapper.uniqueIdentifier = thisCompProduct.id; //this will be treated as identifier for the order
                thisOrderWrapper.accountId = thisCompProduct.Customer_Account_No__c;
                thisOrderWrapper.type =  'Sample Order';
                thisOrderWrapper.reasonForApproval = thisCompProduct.Estimated_Amount__c > 134 ? 'VP Approved':'Goodwill';
                thisOrderWrapper.recordTypeId = orderRecordTypeId;
                thisOrderWrapper.cmnVerification = true;
                thisOrderWrapper.signatureRequired = thisCompProduct.Signature_Required__c;
                thisOrderWrapper.status = 'Draft';
                thisOrderWrapper.subType = 'Comp Product Order';
                thisOrderWrapper.effectiveDate = System.Today();
                thisOrderWrapper.scheduledShipDate = System.Today();
                thisOrderWrapper.WaiveShippingCharges = true;
                thisOrderWrapper.shippingAddress = String.isBlank(thisCompProduct.Shipping_Address__c) ? thisCompProduct.Default_Shipping_Address__c : thisCompProduct.Shipping_Address__c;
                thisOrderWrapper.shippingMethod = thisCompProduct.Shipping_Method__c;
                thisOrderWrapper.sendErrorNotificationTo = thisCompProduct.Rep_Making_Request_Email__c;
                if (Test.isRunningTest()) {
                    thisOrderWrapper.priceBook = Test.getStandardPricebookId();
                } 
                else{
                    thisOrderWrapper.priceBook = noChargePriceBookList[0].id; //No charge Pricebook
                }
                
                //add products
                Map<String, Schema.SObjectField> compProductFieldsLabel = Schema.SObjectType.Comp_Product_Request__c.fields.getMap();
                
                for(String fieldAPIName : compProductDetailsMap.keySet()){
                    System.debug('fieldAPIName>> '+ fieldAPIName);
                    Integer quantity =  Integer.valueOf(thisCompProduct.get(fieldAPIName));
                    System.debug('quantity >> '+ quantity);
                    if(compProductDetailsMap.get(fieldAPIName).Product_Code__c != null && 
                       (quantity != Null && quantity != 0)){ //if Product Quantity Selected On Comp Prod Request
                           String productName = compProductDetailsMap.get(fieldAPIName).Product_Code__c;
                           thisOrderWrapper.productQuantityMap.put(productName, quantity);  //Add Product and Quantity in Map
                       }
                }   
                
                orderWrapperList.add(thisOrderWrapper);
            }       
            if(!orderWrapperList.isEmpty()){
                System.debug('list1>> '+ orderWrapperList);
                system.debug('order wrapper list size before creating order:::'+orderWrapperList.size());
                
                //Create the orders for Comp Products
                newOrderCreatedMap = ClsCreateOrder.createOrder(orderWrapperList);
                system.debug('newOrderCreatedMap::::'+newOrderCreatedMap);
                
                //----------------Link the newly created Order record with each Comp Product record Start-------------------------
                for(ClsOrderWrapper orderWrapperRecord :orderWrapperList){
                    Comp_Product_Request__c compProductRecord = new Comp_Product_Request__c(Id = orderWrapperRecord.uniqueIdentifier);
                    if(!newOrderCreatedMap.isEmpty() && newOrderCreatedMap.containsKey(orderWrapperRecord.uniqueIdentifier)){
                        Order newlyCreatedOrder = newOrderCreatedMap.get(orderWrapperRecord.uniqueIdentifier);
                        compProductRecord.order__c = newlyCreatedOrder.id;//Link Order on the Comp Product Record
                        // newlyCreatedOrder.Comp_Product_Request__c = orderWrapperRecord.uniqueIdentifier;//Link Product on the Order record
                        productListToBeUpdated.add(compProductRecord);
                        //  ordersToBeUpdated.add(newlyCreatedOrder);
                    }
                }
                
                
                //try{
                //Update the Products with Order Information
                if(!productListToBeUpdated.isEmpty()){ 
                    Database.update(productListToBeUpdated,false);
                }
                
                //Update Orders with Schedule Information
                //  if(!ordersToBeUpdated.isEmpty()){Database.update(ordersToBeUpdated,false);}
                /*}catch (DmlException de) {
Integer numErrors = de.getNumDml();
System.debug('getNumDml=' + numErrors);
for(Integer i=0;i<numErrors;i++) {
System.debug('getDmlFieldNames=' + de.getDmlFieldNames(i));
System.debug('getDmlMessage=' + de.getDmlMessage(i));
}
}*/
                
            }
        }    
    }
    /**END: Comp Product Order Generation **/
    
    //CRMSF-5295 : Validate testing suplies if the account consumer code is K , Generation is G6 and payor is opt out from rules matrix.
    public static void validateTestingSuplies(List<Comp_Product_Request__c> compReqList, Map<Id,Comp_Product_Request__c> compProdOldMap){
        Map<Id,Boolean> isTestingSupplyOptIn = new Map<Id,Boolean>();
        Set<Id> payorAccountIds = new Set<Id>();
        Set<Id> accountIds = new Set<Id>();
        
        for(Comp_Product_Request__c compReq : compReqList){
            if(compProdOldMap.isEmpty() || 
               (compReq.Box_of_Lancets__c != compProdOldMap.get(compReq.Id).Box_of_Lancets__c || compReq.Box_of_Test_Strips__c != compProdOldMap.get(compReq.Id).Box_of_Test_Strips__c 
                || compReq.Control_Solution__c != compProdOldMap.get(compReq.Id).Control_Solution__c || compReq.Glucose_Monitoring_System__c != compProdOldMap.get(compReq.Id).Glucose_Monitoring_System__c )){
                    accountIds.add(compReq.Customer_Account_No__c);
                }
        }
        if(accountIds.size() > 0){
            Map<Id,Account> accountMap =new Map<Id,Account>([select id,name,Payor__c,Customer_Type__c,Consumer_Payor_Code__c,Latest_Transmitter_Generation_Shipped__c from account where id in:accountIds and Latest_Transmitter_Generation_Shipped__c = 'G6' ]);
            
            for(Comp_Product_Request__c compReq : compReqList){
                if(accountMap.containsKey(compReq.Customer_Account_No__c)){
                    payorAccountIds.add(accountMap.get(compReq.Customer_Account_No__c).Payor__c);
                }
            }
            
            for(Rules_Matrix__c ruleMtx : [SELECT Account__c, Is_True_or_False__c from Rules_Matrix__c where Account__c In : payorAccountIds and Rule_Criteria__c = 'Testing Supplies Opt In']){
                isTestingSupplyOptIn.put(ruleMtx.Account__c, ruleMtx.Is_True_or_False__c);
            }
            // system.debug('isTestingSupplyOptIn..'+isTestingSupplyOptIn);
            
            for(Comp_Product_Request__c compReq : compReqList){
                if((accountMap.containsKey(compReq.Customer_Account_No__c) && accountMap.get(compReq.Customer_Account_No__c).Customer_Type__c.equalsIgnoreCase(ClsApexConstants.CUSTOMER_TYPE) &&  accountMap.get(compReq.Customer_Account_No__c).Consumer_Payor_Code__c.equalsIgnoreCase(ClsApexConstants.CONSUMER_PAYOR_CODE)  ) 
                   && (!string.isblank(compReq.Box_of_Lancets__c) || !string.isblank(compReq.Box_of_Test_Strips__c)|| !string.isblank(compReq.Control_Solution__c) || !string.isblank(compReq.Glucose_Monitoring_System__c))){
                       if(!isTestingSupplyOptIn.containsKey(accountMap.get(compReq.Customer_Account_No__c).Payor__c) || (isTestingSupplyOptIn.containsKey(accountMap.get(compReq.Customer_Account_No__c).Payor__c) && !isTestingSupplyOptIn.get(accountMap.get(compReq.Customer_Account_No__c).Payor__c) ) ){
                           compReq.addError('Payor has opted out for Testing Supplies');   
                       } 
                   }
            }
        }      
    }
}