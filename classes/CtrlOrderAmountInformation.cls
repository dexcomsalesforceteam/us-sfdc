public with sharing class CtrlOrderAmountInformation {
/*******************************************************************************************************************
@Author        	: Jagan Periyakaruppan
@Date Created   : 02/26/2018
@Description    : This controller is used in the Lightning component in Order page layout, which will display
					Order Amount related information
********************************************************************************************************************/ 
    @AuraEnabled
    public static Map<String, String> getOrderAmountDetails  (String orderId) {
        Map<String, String> fieldValueMap = new Map<String, String>();
        Order currentOrder = [SELECT Id, Type, Tax__c, Integration_Message__c, TotalAmount, Total_Order_Amount__c, Shipping_Charges__c, CoPay__c, Is_Cash_Order__c, Price_Book__r.Name FROM Order WHERE Id = : orderId];
        fieldValueMap.put('orderType', String.valueOf(currentOrder.Type));
        fieldValueMap.put('orderAmount', String.valueOf(currentOrder.TotalAmount));
        fieldValueMap.put('taxAmount', String.valueOf(currentOrder.Tax__c));
        fieldValueMap.put('shippingCharges', String.valueOf(currentOrder.Shipping_Charges__c));
        fieldValueMap.put('copayAmount', String.valueOf(currentOrder.CoPay__c));
        fieldValueMap.put('totalAmount', String.valueOf(currentOrder.Total_Order_Amount__c));
        fieldValueMap.put('priceBookName', String.valueOf(currentOrder.Price_Book__r.Name));
        fieldValueMap.put('errorMessage', String.valueOf(currentOrder.Integration_Message__c));

        if(currentOrder.Is_Cash_Order__c)
            fieldValueMap.put('isCashOrder', 'TRUE');
        else
            fieldValueMap.put('isCashOrder', 'FALSE');
        
		return fieldValueMap;
	}
}