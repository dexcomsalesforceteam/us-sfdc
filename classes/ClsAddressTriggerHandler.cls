/*******************************************************************************************************************
@Author        : Jagan Periyakaruppan
@Date Created    : 4/30/2017
@Description    : Class used in to process all Address related logic for Address object upon inserting and updating
********************************************************************************************************************/
public class ClsAddressTriggerHandler
{ 
    
    static List<Id> accountIdsToProcess = new List<Id>();//List of Accounts to be processed
    static List<Address__c> addressToProcess = new List<Address__c>();//List of Addresses to be processed
	static Map<Id, Account> accountsToProcess = new Map<Id, Account>();//Map Accounts to be updated with BILL_TO or SHIP_TO address information
    static Boolean billToExist = false;//Check if BILL_TO Address exist
    static Boolean shipToExist = false;//Check if SHIP_TO Address exist
    
    //Method unsets the primary flag for other BILL_TO or SHIP_TO addresses provided the inserted address record has primary flag checked
    public static void ProcessAddressInsert(List<Address__c> addresses)
    {
        //Find the addresses, which have the primary flag set
        for(Address__c addr : addresses)
        {
            system.debug('----Address Id to Process ' + addr.Id);
            system.debug('----Account Id to Process ' + addr.Account__c);
            
            if(addr.Primary_Flag__c)
            {
                accountIdsToProcess.add(addr.Account__c);
                addressToProcess.add(addr);
                if(addr.Address_Type__c == 'BILL_TO')
                    billToExist = true;
                else
                    if(addr.Address_Type__c == 'SHIP_TO')
                    shipToExist = true;
            }
        }
        if(!addressToProcess.isEmpty())
            ProcessPrimaryAddress();
    }
    
    //Method unsets the primary flag for other BILL_TO or SHIP_TO addresses provided the updated address record has primary flag checked
    public static void ProcessAddressUpdate(Map<Id, Address__c> newTriggerAddresses, Map<Id, Address__c> oldTriggerAddresses)
    {
        addressToProcess.clear();
        
		//Check if any address got updated with primary flag and then proceeed
        for(Address__c newAddr : newTriggerAddresses.values())
        {
            Address__c oldAddr = oldTriggerAddresses.get(newAddr.Id);
            //Check if the primary flag is changed with a true value
            if((oldAddr.Primary_Flag__c != newAddr.Primary_Flag__c) && newAddr.Primary_Flag__c)
            {
                system.debug('----Primary flag was updated on the address, so entering the block');
                accountIdsToProcess.add(newAddr.Account__c);
                addressToProcess.add(newAddr);
                if(newAddr.Address_Type__c == 'BILL_TO')
                    billToExist = true;
                else
                    if(newAddr.Address_Type__c == 'SHIP_TO')
                    shipToExist = true;
            }
			//Check for existing Primary address any of the address related information has been changed
			if((oldAddr.Primary_Flag__c == newAddr.Primary_Flag__c) && newAddr.Primary_Flag__c &&
				(oldAddr.Street_Address_1__c != newAddr.Street_Address_1__c || oldAddr.City__c != newAddr.City__c || oldAddr.State__c != newAddr.State__c || oldAddr.Zip_Postal_Code__c != newAddr.Zip_Postal_Code__c || oldAddr.County__c != newAddr.County__c || oldAddr.Country__c != newAddr.Country__c))
				
			{
				Account accntToBeUpdated = new Account(Id = newAddr.Account__c);
				system.debug('----Found mismatch in address information');
				if(newAddr.Address_Type__c == 'BILL_TO')
				{
					accntToBeUpdated.BillingStreet = newAddr.Street_Address_1__c;
					accntToBeUpdated.BillingCity = newAddr.City__c;
					accntToBeUpdated.BillingState = newAddr.State__c;
					accntToBeUpdated.BillingPostalCode = newAddr.Zip_Postal_Code__c;
					accntToBeUpdated.BillingCountry = newAddr.Country__c;
					accountsToProcess.put(newAddr.Account__c, accntToBeUpdated);
					system.debug('----Account, which will be updated is ' + newAddr.Account__c);
				}
				else
				if(newAddr.Address_Type__c == 'SHIP_TO')
				{
					accntToBeUpdated.ShippingStreet = newAddr.Street_Address_1__c;
					accntToBeUpdated.ShippingCity = newAddr.City__c;
					accntToBeUpdated.ShippingState = newAddr.State__c;
					accntToBeUpdated.ShippingPostalCode = newAddr.Zip_Postal_Code__c;
					accntToBeUpdated.ShippingCountry = newAddr.Country__c;
					accntToBeUpdated.Primary_Ship_To_Address__c = newAddr.Id;
					accountsToProcess.put(newAddr.Account__c, accntToBeUpdated);
					system.debug('----Account, which will be updated is ' + newAddr.Account__c);
				}
				
			}
        }
		//If there are changes to Primary address then account's primary should be updated accordingly
        if(!addressToProcess.isEmpty())
        {
            ProcessPrimaryAddress();
        }
		//If there are changes to any address attributes on the Primary addresss then corresponding account's primary address should be updated accordingly
		if(!accountsToProcess.isEmpty())
        {
			UpdateAccounts(accountsToProcess.values());
		}
        
    }
    
    //Helper class, which will process the address records for primary flag reset
    public static void ProcessPrimaryAddress()
    {
        Map<Id, List<Address__c>> accntIdBillToPrimaryAddressMap = new Map<Id, List<Address__c>>();//Map holds the List of BILL_TO address tied to an account 
        Map<Id, List<Address__c>> accntIdShipToPrimaryAddressMap = new Map<Id, List<Address__c>>();//Map holds the List of SHIP_TO address tied to an account 
        List<Address__c> removePrimaryAddressList = new List<Address__c>();//List holds the address records to be updated to remove the primary flag
		
        if(!addressToProcess.isEmpty())
        {
            //Get the List of Primary BILL_TO and SHIP_TO address tied to the account record
            if(billToExist)
			{
                accntIdBillToPrimaryAddressMap = ClsAddressTriggerHandlerHelper.GetPrimaryAddressesTiedToAccount('BILL_TO', accountIdsToProcess, addressToProcess);
				system.debug('----The size of accntIdBillToPrimaryAddressMap is '+accntIdBillToPrimaryAddressMap.values().size());
            }
			if(shipToExist)
			{
                accntIdShipToPrimaryAddressMap = ClsAddressTriggerHandlerHelper.GetPrimaryAddressesTiedToAccount('SHIP_TO', accountIdsToProcess, addressToProcess);
				system.debug('----The size of accntIdShipToPrimaryAddressMap is '+accntIdShipToPrimaryAddressMap.values().size());
			}
        }
        for(Address__c addrToProcess : addressToProcess)
        {
            system.debug('----Address processing ' + addrToProcess.Id);
            Account accntToBeUpdated = new Account(Id = addrToProcess.Account__c);
            List<Address__c> tempAddrList;
            if(addrToProcess.Address_Type__c == 'BILL_TO')
            {
                tempAddrList = (!accntIdBillToPrimaryAddressMap.isEmpty()) ? accntIdBillToPrimaryAddressMap.get(addrToProcess.Account__c) : null;
                accntToBeUpdated.BillingStreet = addrToProcess.Street_Address_1__c;
                accntToBeUpdated.BillingCity = addrToProcess.City__c;
                accntToBeUpdated.BillingState = addrToProcess.State__c;
                accntToBeUpdated.BillingPostalCode = addrToProcess.Zip_Postal_Code__c;
                accntToBeUpdated.BillingCountry = addrToProcess.Country__c;
                accountsToProcess.put(addrToProcess.Account__c, accntToBeUpdated);
				system.debug('----Account, which will be updated is ' + addrToProcess.Account__c);
                
            }
            else if (addrToProcess.Address_Type__c == 'SHIP_TO')
            {
                tempAddrList = (!accntIdShipToPrimaryAddressMap.isEmpty()) ? accntIdShipToPrimaryAddressMap.get(addrToProcess.Account__c) : null;
                accntToBeUpdated.ShippingStreet = addrToProcess.Street_Address_1__c;
                accntToBeUpdated.ShippingCity = addrToProcess.City__c;
                accntToBeUpdated.ShippingState = addrToProcess.State__c;
                accntToBeUpdated.ShippingPostalCode = addrToProcess.Zip_Postal_Code__c;
                accntToBeUpdated.ShippingCountry = addrToProcess.Country__c;
				accntToBeUpdated.Primary_Ship_To_Address__c = addrToProcess.Id;
                accountsToProcess.put(addrToProcess.Account__c, accntToBeUpdated);
				system.debug('----Account, which will be updated is ' + addrToProcess.Account__c);
            }
            if(tempAddrList != null) //This is to avoid null pointer exception otherwise just checking isEmpty is ok
            {
                if(!tempAddrList.isEmpty())
                    removePrimaryAddressList.addall(tempAddrList);
            }    
            system.debug('----Number of primary address to be inactivated ' + removePrimaryAddressList.size());
        }
        //Update Address Records to reset the primary flag
        if(!removePrimaryAddressList.isEmpty())
        {
            system.debug('-----Update list entered for Addresses');
            try{update removePrimaryAddressList;}
            catch (DmlException de) {
                Integer numErrors = de.getNumDml();
                System.debug('getNumDml=' + numErrors);
                for(Integer i=0;i<numErrors;i++) {
                    System.debug('getDmlFieldNames=' + de.getDmlFieldNames(i));
                    System.debug('getDmlMessage=' + de.getDmlMessage(i));
                }
            }
        }
        
        //Update Account Records to set Billing and Shipping address
        if(!accountsToProcess.isEmpty())
        {
            UpdateAccounts(accountsToProcess.values());
        }
    }
	//Method updates accounts
    public static void UpdateAccounts(List<Account> accountsToBeUpdated)
    {
		system.debug('-----Update list entered for Accounts');
		ClsAccountAddressTriggerStaticClass.addressChangeInvokedFromAccount = false;
		try{update accountsToProcess.values();}
		catch (DmlException de) {
			Integer numErrors = de.getNumDml();
			System.debug('getNumDml=' + numErrors);
			for(Integer i=0;i<numErrors;i++) {
				System.debug('getDmlFieldNames=' + de.getDmlFieldNames(i));
				System.debug('getDmlMessage=' + de.getDmlMessage(i));
			}
		}
	}
    
    /*
     * @Description : This method is created to publish CRM_Account_Address_Event_Out__e
     * on change of Phone_1__c, Phone_2__c, Fax__c
     * If none of the address details are changed then we publish a confirmed event
     * Created for CRMSF-5189
     * @Created Date : May 20,2020
     * @Author : Tanay, LTI
	*/
    public static void publishAddressOutEvent(List<Address__c> newAddressList, Map<Id, Address__c> oldAddressMap){
        system.debug('inside publishAddressOutEvent::::');
        List<Address__c> processAddressList = new List<Address__c>();
        Map<Id, Address__c> confirmedAddressMap = new Map<Id, Address__c>();
        Set<Id> accountIdConfimedSet = new Set<Id>();
        Map<string, Schema.SobjectField> addressFieldsMap = Schema.SObjectType.Address__c.fields.getMap();
        Set<Id> accountIdSet = new Set<Id>(); //this collection holds value of account Ids from US SF
        Set<Id> addressIdSet = new Set<Id>(); //set of SF US Address Ids
        addressIdSet.addAll(oldAddressMap.keySet());
        for(Address__c addressObj : newAddressList){
            if(oldAddressMap.containsKey(addressObj.Id) && oldAddressMap.get(addressObj.Id) != null ){
                if(((addressObj.Phone_1__c != oldAddressMap.get(addressObj.Id).Phone_1__c) || (addressObj.Phone_2__c != oldAddressMap.get(addressObj.Id).Phone_2__c) 
                    || (addressObj.Fax__c != oldAddressMap.get(addressObj.Id).Fax__c)) && !ClsApexUtility.isExecutingFromOCE){//not executing updates from OCE
                        processAddressList.add(addressObj);
                        accountIdSet.add(addressObj.Account__c);
                    }else if(ClsApexUtility.isExecutingFromOCE && !ClsApexUtility.addressUpdConfPublished){
                        confirmedAddressMap.put(addressObj.id, addressObj);
                        accountIdConfimedSet.add(addressObj.Account__c);
                    }
            }
        }
        system.debug('processAddressList::::'+processAddressList);
        //publish pending address event
        if(!processAddressList.isEmpty()){
            publishAccountAddressOutEvent(processAddressList, Label.Event_Stage_Pending, accountIdSet, addressIdSet, Label.Operation_Update);
        }
        
        if(!confirmedAddressMap.values().isEmpty()){
            ClsApexUtility.addressUpdConfPublished = ClsApexConstants.BOOLEAN_TRUE;
            publishAccountAddressOutEvent(confirmedAddressMap.values(), Label.Event_Stage_Confimed, accountIdConfimedSet, addressIdSet, Label.Operation_Update);
        }
    }
    
    /*
     * This method will publish CRM_Account_Address_Event_Out__e based on the address list that we have received
     * it will accept the list of address and stage of the event to be published
     * This method publishes update pending/confirmed and create confirmed events
     * Created for CRMSF-5189
	*/
    public static void publishAccountAddressOutEvent(List<Address__c> addressListToPublish, String stageType, Set<Id> accountIdSet, Set<Id> addressIdSet, String operation){
        system.debug('inside publishing address out pending confirmed event:::');
        List<CRM_Account_Address_Event_Out__e> addressOutEventList = new List<CRM_Account_Address_Event_Out__e>();
        List<CRM_Account_Address_Event_Out__e> skippedAddressOutEventList = new List<CRM_Account_Address_Event_Out__e>();
        List<object> phoneContactDetailsList = new List<object>();
        List<CRM_Error_Event__e> errorOutEventList = new List<CRM_Error_Event__e>();
        List<Map<String, object>> phoneContactWrapList = new List<Map<String, object>>();
        Map<Id, Account> accountDetailsMap = new Map<Id, Account>();
        Map<Id, Address__c> addressDetailsMap = new Map<Id, Address__c>();
        if(!accountIdSet.isEmpty()){
            accountDetailsMap = ClsApexUtility.getExistingAccountMap(accountIdSet);
        }
        
        if(!addressIdSet.isEmpty()){
            addressDetailsMap = ClsApexUtility.getExistingAddressMap(addressIdSet);
        }
        
        for(Address__c addressObj : addressListToPublish){
            if(accountDetailsMap.containsKey(addressObj.Account__c) && accountDetailsMap.get(addressObj.Account__c) != null &&
               ClsApexConstants.PRESCRIBER.equalsIgnoreCase(accountDetailsMap.get(addressObj.Account__c).RecordType.DeveloperName)){ //run for only prescriber accounts
                   CRM_Account_Address_Event_Out__e addressEventOutObj = new CRM_Account_Address_Event_Out__e();
                   phoneContactDetailsList.clear();
                   phoneContactWrapList.clear();
                   addressEventOutObj.version__c = Label.Canonical_Version;
                   addressEventOutObj.eventDate__c = addressDetailsMap.containsKey(addressObj.Id)&& addressDetailsMap.get(addressObj.Id)!=null ?String.valueOf(addressDetailsMap.get(addressObj.Id).LastModifiedDate.formatGmt(ClsApexConstants.GMT_FORMAT)) : String.valueOf(system.now().formatGmt(ClsApexConstants.GMT_FORMAT));
                   addressEventOutObj.entityType__c = Label.Entity_Type_Address;
                   addressEventOutObj.eventSource__c = Label.Event_Source_CRM;
                   addressEventOutObj.eventSourceID__c = addressObj.Id;
                   addressEventOutObj.operation__c = operation;
                   addressEventOutObj.stage__c = stageType;
                   addressEventOutObj.systemOfOrigin__c = String.isNotBlank(addressObj.System_Of_Origin__c) ? addressObj.System_Of_Origin__c : Label.Event_Source_Hcrm;
                   addressEventOutObj.systemOfOriginID__c = addressObj.System_Of_Origin_Id__c;
                   //if system origin id of account is not found then we are passing empty string
                   addressEventOutObj.accountID__c = accountDetailsMap.containsKey(addressObj.Account__c)? accountDetailsMap.get(addressObj.Account__c).System_Of_Origin_Id__c : ClsApexConstants.EMPTY_STRING;
                   addressEventOutObj.parentSystemOfOriginID__c = accountDetailsMap.containsKey(addressObj.Account__c)? accountDetailsMap.get(addressObj.Account__c).System_Of_Origin_Id__c : ClsApexConstants.EMPTY_STRING;
                   addressEventOutObj.isActive__c = !addressObj.Inactive__c;
                   addressEventOutObj.isPrimary__c = addressObj.Primary_Flag__c;
                   addressEventOutObj.addressType__c = ClsApexConstants.ADDRESS_TYPE_SHIPPING;
                   addressEventOutObj.address1__c = addressObj.Street_Address_1__c;
                   addressEventOutObj.address2__c = addressObj.Street_Address_2__c;
                   addressEventOutObj.address3__c = addressObj.Street_Address_3__c;
                   addressEventOutObj.city__c = addressObj.City__c;
                   addressEventOutObj.stateProvince__c = addressObj.State__c;
                   addressEventOutObj.postalCode__c = addressObj.Zip_Postal_Code__c;
                   addressEventOutObj.addressID__c = addressDetailsMap.containsKey(addressObj.Id)? addressDetailsMap.get(addressObj.Id).System_Of_Origin_Id__c : null;
                   addressEventOutObj.countryCode__c = ClsApexConstants.US;
                   addressEventOutObj.country__c = ClsApexConstants.UNITED_STATES;
                   //addressEventOutObj.phoneContacts__c map contacts
                   if(addressObj.Phone_1__c != null || addressObj.Phone_2__c != null || addressObj.Fax__c != null){
                       //Phone_1__c mapping
                       if(addressObj.Phone_1__c != null){
                           phoneContactDetailsList.addAll(new List<object>{Label.Type_of_Contact_Mobile, addressObj.Phone_1__c, Integer.valueOf(Label.Priority_Zero)});
                           Map<String, object> phone1Map = getPhoneContactsMap(phoneContactDetailsList);
                           phoneContactWrapList.add(phone1Map);
                           phoneContactDetailsList.clear();//clear previous contents
                       }
                       //Phone_2__c mapping
                       if(addressObj.Phone_2__c != null){
                           phoneContactDetailsList.addAll(new List<object>{Label.Type_of_Contact_Mobile, addressObj.Phone_2__c, Integer.valueOf(Label.Priority_One)});
                           Map<String, object> phone2Map = getPhoneContactsMap(phoneContactDetailsList);
                           phoneContactWrapList.add(phone2Map);
                           phoneContactDetailsList.clear();//clear previous contents
                       }
                       //Fax__c mapping
                       if(addressObj.Fax__c != null){
                           phoneContactDetailsList.addAll(new List<object>{Label.Type_of_Contact_Fax, addressObj.Fax__c, Integer.valueOf(Label.Priority_Zero)});
                           Map<String, object> faxMap = getPhoneContactsMap(phoneContactDetailsList);
                           phoneContactWrapList.add(faxMap);
                           phoneContactDetailsList.clear();//clear previous contents
                       }
                       
                       if(!phoneContactWrapList.isEmpty())
                           addressEventOutObj.phoneContacts__c = JSON.serialize(phoneContactWrapList);
                   }
                   addressEventOutObj.author__c = UserInfo.getName();
                   if(!String.isEmpty(addressEventOutObj.accountID__c)){
                       addressOutEventList.add(addressEventOutObj);
                   }else{
                       skippedAddressOutEventList.add(addressEventOutObj);
                   }
               }

        }
        
        system.debug('addressOutEventList::::'+addressOutEventList);
        if(!addressOutEventList.isEmpty()){
            Database.SaveResult[] saveResultList =  EventBus.publish(addressOutEventList);
            system.debug('saveResultList::::'+saveResultList);
            
        }
        
        if(!skippedAddressOutEventList.isEmpty()){
            for(CRM_Account_Address_Event_Out__e addressOutObj : skippedAddressOutEventList){
                errorOutEventList.add(ClsApexUtility.createErrorOutObjRecord(Label.Error_Code, Label.Entity_Type_Address+ClsApexConstants.HYPHEN+'update', String.valueOf(system.now().formatGmt(ClsApexConstants.GMT_FORMAT)), Label.Event_Source_CRM, addressOutObj.systemOfOriginID__c, 'Please check record in CRM with Unique Id '+addressOutObj.systemOfOriginID__c, JSON.serialize(addressOutObj)));
            }
            //publish out event
            if(!errorOutEventList.isEmpty()){
                Eventbus.publish(errorOutEventList);
            }
        }
    }
    
    
    /*
     * This method will return a map of phone contact values
	*/
    public static Map<String, object> getPhoneContactsMap(List<object> valuesList){
        Map<String, object> phoneContactMap = new Map<String, object>();
        final Map<Integer, String> sequenceKeyMap = new Map<Integer, String>();
        sequenceKeyMap.put(Integer.valueOf(system.label.Priority_Zero), Label.Type_Key);
        sequenceKeyMap.put(Integer.valueOf(Label.Priority_One), Label.Number_Key);
        sequenceKeyMap.put(Integer.valueOf(Label.Priority_Two), Label.Priority_Key);
        if(valuesList != null && !valuesList.isEmpty()){
            for(Integer itr=ClsApexConstants.ZERO_INTEGER ; itr<valuesList.size(); itr++){
                phoneContactMap.put(sequenceKeyMap.get(itr), valuesList[itr]);
            }
        }
        return phoneContactMap;
    }
    
    
    /*
     * @Description : This method formats the incoming phone and fax in the format as (xxx) xxx-xxxx
     * Created for CRMSF-5189. This method runs for before insert and before update
	*/
    public static void formatPhone(List<Address__c> newAddressList, Map<Id, Address__c> oldAddressMap){
        
        
        for(Address__c newAddressObj : newAddressList){
            if(oldAddressMap != null && oldAddressMap.containsKey(newAddressObj.Id) && oldAddressMap.get(newAddressObj.Id) != null){//run for update
                if(newAddressObj.Phone_1__c != oldAddressMap.get(newAddressObj.Id).Phone_1__c && String.isNotBlank(newAddressObj.Phone_1__c)){
                    newAddressObj.Phone_1__c = ClsApexUtility.formatPhone(newAddressObj.Phone_1__c);
                }
                
                if(newAddressObj.Phone_2__c != oldAddressMap.get(newAddressObj.Id).Phone_2__c && String.isNotBlank(newAddressObj.Phone_2__c)){
                    newAddressObj.Phone_2__c = ClsApexUtility.formatPhone(newAddressObj.Phone_2__c);
                }
                
                if(newAddressObj.Fax__c != oldAddressMap.get(newAddressObj.Id).Fax__c && String.isNotBlank(newAddressObj.Fax__c)){
                    newAddressObj.Fax__c = ClsApexUtility.formatPhone(newAddressObj.Fax__c);
                }
                
            }else{//run for created
                if(String.isNotBlank(newAddressObj.Phone_1__c)){// format Phone1__c
                    newAddressObj.Phone_1__c = ClsApexUtility.formatPhone(newAddressObj.Phone_1__c);
                }
                
                if(String.isNotBlank(newAddressObj.Phone_2__c)){// format Phone1__c
                    newAddressObj.Phone_1__c = ClsApexUtility.formatPhone(newAddressObj.Phone_1__c);
                }
                
                if(String.isNotBlank(newAddressObj.Phone_2__c)){// format Phone1__c
                    newAddressObj.Phone_1__c = ClsApexUtility.formatPhone(newAddressObj.Phone_1__c);
                }
            }
        }
    }
    
    
}