/*******************************************************************************************************************
@Author         : Sudheer Vemula
@Date Created   : 05/24/2019
@Description    : Order Audit Trigger handler to process Order Audit Trigger
********************************************************************************************************************/    

public class OrderAuditTriggerHandler {
    
    public static void updateOrderForQcholdChecks(Map<Id,Order_Audit__c> newAudits){
        List<order> updateOrderList=new List<order>();
        List<string> ids=new List<string>();
        
        for(Order_Audit__c order:newAudits.values()){
            ids.add(order.Order_Number__c); 
        }
        List<Order_Audit__c> orderAuditList=[select id,name,Audit_Field__c,Audit_Area__c,Audit_Exception__c,Audit_Verified__c,Order_Number__c from Order_Audit__c where Order_Number__c in:ids];
        
        boolean biCheck =true;
        boolean payorCheck=true ;
        boolean documentCheck =true;
        boolean orderCheck=true;
        
        boolean biBypassCheck =false;
        boolean payorBypassCheck =false;
        boolean documentBypassCheck =false;
        boolean orderBypassCheck =false;
        Map<string ,string> orderMap=new Map<string ,string>();
       
        for(Order_Audit__c audit:orderAuditList){
            order order=new order();
            if(!orderMap.containskey(audit.Order_Number__c)){
                biBypassCheck =false;
                payorBypassCheck =false;
                documentBypassCheck =false;
                orderBypassCheck =false;
            }
             system.debug('payorCheck..out..'+payorCheck );
            if(audit.Audit_Area__c == 'Benefit'){
                if((audit.Audit_Verified__c == false  && audit.Audit_Field__c != 'Bypass Check') && (!orderMap.containskey(audit.Order_Number__c) || biBypassCheck ==false)){
                    biCheck =  false;
                }
                
                if((audit.Audit_Exception__c == true && audit.Audit_Field__c == 'Bypass Check')){
                    biBypassCheck = true;
                }
                biCheck = getOrderHoldCheckResult(biCheck,biBypassCheck);
                
            }
           
            if(audit.Audit_Area__c == 'Payor'){
                system.debug('audit.Audit_Verified__c ..p..'+audit.Audit_Verified__c );
                if((audit.Audit_Verified__c == false  && audit.Audit_Field__c != 'Bypass Check') && ( !orderMap.containskey(audit.Order_Number__c) || payorBypassCheck ==false)){
                    payorCheck =  false;
                     system.debug('audit.Audit_Verified__c ..p.in.'+payorCheck );
                     system.debug('audit.Audit_Verified__c ..audit name.in.'+audit.name );
                }
                
                if((audit.Audit_Exception__c == true && audit.Audit_Field__c == 'Bypass Check')){
                    payorBypassCheck = true;
                }
                payorCheck = getOrderHoldCheckResult(payorCheck,payorBypassCheck);
                
                system.debug('payorCheck..'+payorCheck);
            }
            
            if(audit.Audit_Area__c == 'Document'){
                if((audit.Audit_Verified__c == false  && audit.Audit_Field__c != 'Bypass Check') && (!orderMap.containskey(audit.Order_Number__c) || documentBypassCheck ==false)){
                    documentCheck =  false;
                }
                
                if((audit.Audit_Exception__c == true && audit.Audit_Field__c == 'Bypass Check')){
                    documentBypassCheck = true;
                }
                documentCheck = getOrderHoldCheckResult(documentCheck,documentBypassCheck);
            }
            
            if(audit.Audit_Area__c == 'Order'){
                if((audit.Audit_Verified__c == false  && audit.Audit_Field__c != 'Bypass Check') && (!orderMap.containskey(audit.Order_Number__c) || orderBypassCheck ==false)){
                    orderCheck =  false;
                }
                
                if((audit.Audit_Exception__c == true && audit.Audit_Field__c == 'Bypass Check')){
                    orderBypassCheck = true;
                }
                orderCheck = getOrderHoldCheckResult(orderCheck,orderBypassCheck);
            }
            
            orderMap.put(audit.Order_Number__c,biCheck+':'+payorCheck+':'+documentCheck+':'+orderCheck);
            
            
        }   
        
        //START for #CRMSF-4614
        
        Set<Id> unSuccessTaskOrderAccounts = new Set<Id>();
        Set<Id> whatIds = new Set<Id>();
        Set<Id> orderIdTask = new Set<Id>();
        Set<Id> benefitAccountIdTask = new Set<Id>();
        Set<Id> accounttaskIds = new Set<Id>();
        
        Set<Id> completeAccountsTaskIds = new Set<Id>();
        
        Map<string,Order> orderRecordsMap = new Map<string,Order>([
            select Id,Name,Scheduled_Ship_Date__c,AccountId,Account.Territory_RSS__c,Account.Territory_Supervisor__c, 
            (SELECT Id,Description,WhatId,Subject FROM Tasks WHERE Type =:ClsApexConstants.AUTOQC and Status !=:ClsApexConstants.STATUS_COMPLETED)
            from Order 
            where Id IN:orderMap.keySet()
        ]);
        //END for #CRMSF-4614

        
        System.debug('====system batch====='+System.isBatch());
        for(string ord:orderMap.keySet()){
            system.debug('orderResMap..'+orderMap);
            order order=new order();
            string value=orderMap.get(ord);
            list<string> strList=value.split(':');
            order.id =ord ;
            order.Is_Benefit_Check_Completed__c =boolean.valueOf(strList[0]);
            order.Is_Payor_Check_Completed__c = boolean.valueOf(strList[1]);
            order.Is_Document_Check_Completed__c =boolean.valueOf(strList[2]) ;
            order.Is_Order_Check_Completed__c =boolean.valueOf(strList[3]) ;
            
            //START for #CRMSF-4614
            Order orderRec = (orderRecordsMap.containskey(ord) && orderRecordsMap.get(ord) != null ) ? orderRecordsMap.get(ord) : new Order();
            system.debug('====orderRec====='+orderRec);
            accounttaskIds.add(orderRec.AccountId);
            //Creating Task for If any one of the services are fail
            if(!(order.Is_Payor_Check_Completed__c && order.Is_Document_Check_Completed__c && order.Is_Order_Check_Completed__c) && 
                orderRec != null && orderRec.Scheduled_Ship_Date__c != null
                && orderRec.Scheduled_Ship_Date__c >=System.today().addDays(-4) && orderRec.Scheduled_Ship_Date__c <=System.today().addDays(3) 
            ){
                system.debug('====unsuccess ord====='+ord); 

                unSuccessTaskOrderAccounts.add(orderRec.AccountId);
            }
            //The status activation based on whether all services are success and if it is triggered from batch
            if(order.Is_Benefit_Check_Completed__c && order.Is_Payor_Check_Completed__c && order.Is_Document_Check_Completed__c && 
                order.Is_Order_Check_Completed__c 
                && System.isBatch()
            ){
                system.debug('====coming inside====='+orderRec.Id);
                order.Status = ClsApexConstants.STATUS_ACTIVATED;
                orderIdTask.add(orderRec.id);
                benefitAccountIdTask.add(orderRec.AccountId);
                completeAccountsTaskIds.add(orderRec.AccountId);
                Id WhatIdRec = (orderRec.Account.Territory_RSS__c != null) ? orderRec.Account.Territory_RSS__c : orderRec.Account.Territory_Supervisor__c;
                system.debug('====WhatIdRec====='+WhatIdRec);
                if(WhatIdRec != null)whatIds.add(WhatIdRec);
            }
            //End for #CRMSF-4614
            
            updateOrderList.add(order);
        }
        system.debug('====updateOrderList====='+updateOrderList); 
        
         Map<string,Account> accountRecordsMap = new Map<string,Account>([
            select Id,Name,Territory_RSS__c,Territory_Supervisor__c, 
            (SELECT Id,Description,WhatId,Subject FROM Tasks WHERE Type =:ClsApexConstants.AUTOQC and Status !=:ClsApexConstants.STATUS_COMPLETED)
            from Account 
            where Id IN:completeAccountsTaskIds
        ]);
        
        List<Task> tasksUpdateList = new List<Task>();
        //Set<string> successOrderId = new Set<string>();
        Set<Id> dmlFailuresRecAccountsId = new Set<Id>();
       
        if(updateOrderList.size() >0 && updateOrderList!= null){
        
            Database.SaveResult[] updateOrderList1 = Database.update(updateOrderList, false);
            system.debug('====updateOrderList1====='+updateOrderList1);
            for(Integer i=0;i<updateOrderList1.size();i++){
                if (updateOrderList1.get(i).isSuccess()){
                     system.debug('====success order Id====='+updateOrderList1.get(i).getId());
                     //successOrderId.add(updateOrderList1.get(i).getId());
                }else if (!updateOrderList1.get(i).isSuccess()){
                    // DML operation failed
                    system.debug('====failure=====');
                    Database.Error error = updateOrderList1.get(i).getErrors().get(0);
                    String failedDML = error.getMessage();
                    system.debug('Failed ID'+updateOrderList.get(i).Id);
                    Order orderRec = (orderRecordsMap.containskey(updateOrderList.get(i).Id) && orderRecordsMap.get(updateOrderList.get(i).Id) != null ) ? orderRecordsMap.get(updateOrderList.get(i).Id) : new Order(); 
                    Account acc = (accountRecordsMap.containskey(orderRec.AccountId) && accountRecordsMap.get(orderRec.AccountId) != null ) ? accountRecordsMap.get(orderRec.AccountId) : new Account();
                 
                    dmlFailuresRecAccountsId.add(acc.Id);
                    //Task tas = OrderAuditTriggerHandler.createTaskForDMLFailuresOfOrders(acc,label.Task_Description_for_any_check_fail);
                    //system.debug('====failure trigger===task====='+tas);      
                    //tasksUpdateList.add(tas);
                }
            }
        } 
        
        if(dmlFailuresRecAccountsId.size() >0)tasksUpdateList = OrderAuditTriggerHandler.TaskWrapperCreation(dmlFailuresRecAccountsId,label.Task_Description_for_any_check_fail);
        
        //Start for #CRMSF-4614
        if(unSuccessTaskOrderAccounts.size() > 0)OrderAuditTriggerHandler.afterUpdateCreateTask(unSuccessTaskOrderAccounts,label.Task_Description_for_any_check_fail);
        
        /*
        Map<Id,Benefits__c> benefitsListMap = new Map<Id,Benefits__c>([SELECT Id, Name,Account__c FROM Benefits__c WHERE Benefit_Hierarchy__c =:ClsApexConstants.BENEFIT_PRIMARY  AND 
            Account__c IN : benefitAccountIdTask]);  
        system.debug('=========benefitsListMap==================='+benefitsListMap);    
        //Closing Primary benefits task when Order activated
        
        if(benefitsListMap.size() >0){
            for(task ta : [select Id,WhatId,Status  from Task where ownerid IN:whatIds and Status !=:ClsApexConstants.STATUS_COMPLETED and type =:ClsApexConstants.AUTOQC and WhatId in: benefitsListMap.keyset()]){
                ta.Status = ClsApexConstants.STATUS_COMPLETED ;
                tasksUpdateList.add(ta);
            }
        }
        */
        
        system.debug('====whatIds====='+whatIds);    
        system.debug('====completeAccountsTaskIds====='+completeAccountsTaskIds);    

        if(whatIds.size() >0 ){ //Closing Order  task when Order activated  //ownerid IN:whatIds and 
                
            for(task ta : [select Id,WhatId,Status  from Task where Status !=:ClsApexConstants.STATUS_COMPLETED and type =:ClsApexConstants.AUTOQC and 
                WhatId in: completeAccountsTaskIds] ){
                ta.Status = ClsApexConstants.STATUS_COMPLETED ;
                tasksUpdateList.add(ta);
            }
        }
        system.debug('====tasksUpdateList====='+tasksUpdateList);    
        if(tasksUpdateList.size() >0 && tasksUpdateList != null) upsert tasksUpdateList;
        system.debug('====22==tasksUpdateList====='+tasksUpdateList);    
        //End for #CRMSF-4614
    }
    
    public static boolean getOrderHoldCheckResult(boolean checkValue,boolean byPasscheckValue ){
        
        if(checkValue || byPasscheckValue){
            checkValue = true; 
        } 
        else{
            checkValue = false; 
        }
        return checkValue;
    }
    
    public static List<Task> TaskWrapperCreation(Set<Id> accountIds,string description){
        
        Map<Id,Account> accountMap= new Map<Id,Account>([
            select Id,Territory_RSS__c,Territory_Supervisor__c
            from Account where Id IN:accountIds
        ]);
        List<Task>taskListQCList = [SELECT Id,WhatId,Subject FROM Task WHERE Type =:ClsApexConstants.AUTOQC and Status !=:ClsApexConstants.STATUS_COMPLETED
            and WhatId IN:accountIds
        ];
        Map<Id,boolean>TaskMap = new Map<Id,boolean>();
        for(task ta : taskListQCList){
            system.debug('====ta.Subject ===='+ta.Subject);
            system.debug('====description===='+description);

            if(ta.Subject==description){
               TaskMap.put(ta.WhatId,true);
            }
        } 
        List<Task> taskList = new List<Task>();
        for(Id order : accountIds){
               Task newNotificationTask = new Task();
               //Order orderRec =  (orderMap.containskey(order) && orderMap.get(order) != null ) ? orderMap.get(order) : new order();
               Account accountRec =  (accountMap.containskey(order) && accountMap.get(order) != null ) ? accountMap.get(order) : new Account();
                
                Boolean isAccountTaskAvailable =false;
                if(TaskMap.containskey(order) && TaskMap.get(order) != null) isAccountTaskAvailable = (TaskMap.get(order)) ? true : false;
                if(isAccountTaskAvailable == false){
                   newNotificationTask.ownerid = (accountRec.Territory_RSS__c != null) ? accountRec.Territory_RSS__c : accountRec.Territory_Supervisor__c;
                   newNotificationTask.WhatId = order;
                   newNotificationTask.subject = description;
                   newNotificationTask.ActivityDate = system.Today();
                   newNotificationTask.Type = ClsApexConstants.AUTOQC;
                   newNotificationTask.Status = ClsApexConstants.STATUS_NOT_STARTED;
                   newNotificationTask.Priority = ClsApexConstants.TASK_PRIORITY;
                   taskList.add(newNotificationTask);
               }
            }
        
        return taskList;
    }
    
    public static void afterUpdateCreateTask(Set<Id> accountIds,string description){
        
        List<Task> taskList = OrderAuditTriggerHandler.TaskWrapperCreation(accountIds,description);
        if(!taskList.isEmpty()){
            database.insert(taskList, ClsApexConstants.BOOLEAN_FALSE);
            system.debug('===afterUpdateCreateTask=====taskList==='+taskList);
        }
    }
    
   /* public static Task createTaskForDMLFailuresOfOrders(Account account,string description){

        Map<Id,boolean>TaskMap = new Map<Id,boolean>();
            
        for(task ta : account.Tasks){
            system.debug('====ta.Subject ===='+ta.Subject);
            system.debug('====description===='+description);
            if(ta.Subject==description){
               TaskMap.put(ta.WhatId,true);
            }
        }
    
        Task newNotificationTask = new Task();
                
        Boolean IsBenefitTaskAvailable =false;
        if(TaskMap.containskey(account.Id) && TaskMap.get(account.Id) != null) IsBenefitTaskAvailable = (TaskMap.get(account.Id)) ? true : false;
        if(IsBenefitTaskAvailable == false){
                   
           newNotificationTask.ownerid = (account.Territory_RSS__c != null) ? account.Territory_RSS__c : account.Territory_Supervisor__c;
           newNotificationTask.WhatId = account.Id;
           newNotificationTask.subject = description;
           newNotificationTask.ActivityDate = system.Today();
           newNotificationTask.Type = ClsApexConstants.AUTOQC;
           newNotificationTask.Status = ClsApexConstants.STATUS_NOT_STARTED;
           newNotificationTask.Priority = ClsApexConstants.TASK_PRIORITY;
           //newNotificationTask.RecordTypeId = ClsApexUtility.getRecordTypeIdByDeveloperName(ClsApexConstants.TASK_SOBJECT_API, ClsApexConstants.RECORD_TYPE_DEVELOPER_API_NAME);
        }
        return newNotificationTask;
    }*/
}