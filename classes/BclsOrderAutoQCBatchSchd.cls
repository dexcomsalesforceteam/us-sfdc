/*
 * @Description : This scheduler class is implemented story CRMSF-4577
 * @Author : LTI
*/
global class BclsOrderAutoQCBatchSchd implements Schedulable{
    global void execute(SchedulableContext sc){
        BclsOrderAutoQCBatch b = new BclsOrderAutoQCBatch ();
       
        database.executebatch(b,25);
    }
}