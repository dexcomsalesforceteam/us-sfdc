/***
*@Author        : Sundog
*@Date Created  : 04-10-2019
*@Description   : Schedulable Apex for Batch Class : BclsPendingAuthUpdateBatch 
***/
global class BclsPendingAuthUpdateSched implements Schedulable{
    public final static String CRON_EXP = '0 0 3 ? * THU *';
    global void execute(SchedulableContext sc) {
        BclsPendingAuthUpdateBatch  bCls = new BclsPendingAuthUpdateBatch(); 
        database.executebatch(bCls);
    }
    
    // to run this batch, copy and execute in dev console:
    // System.Schedule('BclsPendingAuthUpdateBatch', BclsPendingAuthUpdateSched.CRON_EXP, new BclsPendingAuthUpdateSched());

}