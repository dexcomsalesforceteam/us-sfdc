/*
 * Escalation Trigger handler Class
 * @Author: Tanay Kulkarni LTI
 * @Date Created : July 31, 2019
 * @Description : This is a hanlder class for Escalation Trigger;
*/
public without sharing class EscalationTriggerHandler {

    /*
     * @Author: Tanay Kulkarni LTI
     * @param: List<Escaltion__c>
     * created as a part of CRMSF-4352
     * Method is called on after insert trigger event
	*/
    public static void handleAfterInsert(List<Escalation__c> newEscalationList){
        createEscalationTaskRecords(newEscalationList);
    }
    
    /*
     * @Author: Tanay Kulkarni LTI
     * @param: List<Escaltion__c>
     * created as a part of CRMSF-4352
     * This method is called on after update event
	*/
    public static void handleAfterUpdate(Map<Id, Escalation__c> oldRecordMap, List<Escalation__c> newEscalationList){
        List<Escalation__c> processEscalationList = new List<Escalation__c>();
        for(Escalation__c escalationObj : newEscalationList){
            if(oldRecordMap.containsKey(escalationObj.id)){
                if(String.isNotBlank(escalationObj.Assigned_To_user__c) &&
                    (!escalationObj.Assigned_To_user__c.equalsIgnoreCase(oldRecordMap.get(escalationObj.Id).Assigned_To_user__c) ||
                    (!escalationObj.Status__c.equalsIgnoreCase(oldRecordMap.get(escalationObj.Id).Status__c) &&
                    ClsApexConstants.STATUS_OPEN.equalsIgnoreCase(escalationObj.Status__c) && ClsApexConstants.STATUS_RESOLVED.equalsIgnoreCase(oldRecordMap.get(escalationObj.Id).Status__c)))){
                    processEscalationList.add(escalationObj);
                }
            }
        }
        if(!processEscalationList.isEmpty())
        createEscalationTaskRecords(processEscalationList);
    }

    /*
     * @Author: Tanay Kulkarni LTI
     * @param: List<Escaltion__c>
     * created as a part of CRMSF-4352
     * This method is to create Task Records of Escalation
     * record type
	*/    
    public static void createEscalationTaskRecords(List<Escalation__c> escalationList){
        List<Task> taskList = new List<Task>();
        List<Id> accountIdList = new List<Id>();
        Map<Id, Account> accountMap = new Map<Id, Account>();
        for(Escalation__c escObj : escalationList){
            accountIdList.add(escObj.Account__c);
        }
        accountMap = new Map<Id, Account>([Select id,PersonContactId from account where id in : accountIdList]);
        try{
            Id escalationRecordType = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get(ClsApexConstants.ESCALATION_TASK).getRecordTypeId();
            for(Escalation__c escalationObj : escalationList){
                if(String.isNotBlank(escalationObj.Assigned_To_user__c)){
                    Task taskObj = new Task();
                    taskObj.WhatId = escalationObj.id;
                    taskObj.OwnerID = escalationObj.Assigned_To_user__c;
                    taskObj.WhoId = accountMap.containsKey(escalationObj.Account__c) ? accountMap.get(escalationObj.Account__c).PersonContactId : null;
                    taskObj.ActivityDate = System.today();
                    taskObj.Priority = String.isBlank(escalationObj.Priority__c) ? ClsApexConstants.PRIORITY_LOW : escalationObj.Priority__c;
                    taskObj.Subject = escalationObj.Subject__c;
                    taskObj.Status = ClsApexConstants.STATUS_NOT_STARTED;
                    taskObj.RecordTypeId = escalationRecordType;
                    taskObj.Escalation_Details__c = 'Channel : '+escalationObj.Channel__c+'\r\n'+
                        'Escalation Reasons : '+escalationObj.Escalation_reasons__c;
                    taskList.add(taskObj);
                }
            }
            if(!taskList.isEmpty())
                insert taskList;
        }catch(Exception e){
            system.debug('insert into Task object failed::'+e.getMessage());
        }
    }
    
    /*
     * Description : This method will be called by before update routine
     * Created for CRMSF-4623
     * @Author : LTI
	*/
    public static void handleBeforeUpdate(List<Escalation__c> newEscalationList, Map<Id, Escalation__c> oldEscalationMap, Map<Id, Escalation__c> newEscalationMap){
        Map<Id, List<Task>> escalationTaskMap = new Map<Id, List<Task>>();
        Map<Id, Escalation__c> noTaskWithEscalationMap = new Map<Id, Escalation__c>();
        noTaskWithEscalationMap.putAll(newEscalationMap);
        for(Task taskObj : [Select Id, WhatId, Status, RecordType.DeveloperName, IsClosed FROM Task WHERE WhatId IN : newEscalationList AND RecordType.DeveloperName = : ClsApexConstants.ESCALATION_TASK]){
            if(escalationTaskMap.containsKey(taskObj.WhatId)){
                List<Task> taskList = escalationTaskMap.get(taskObj.WhatId);
                taskList.add(taskObj);
            }else{
                escalationTaskMap.put(taskObj.WhatId, new List<Task>{taskObj});
            }
            
            //check if the escalation has associated task
            //if escalation task present then remove escalation id from map
            if(noTaskWithEscalationMap.containsKey(taskObj.WhatId)){
                noTaskWithEscalationMap.remove(taskObj.WhatId);
            }
            
            
        }
        
        validateStatusChange(newEscalationList, oldEscalationMap, escalationTaskMap, noTaskWithEscalationMap);
    }
    
    /*
     * Description : This method will validate the status change of escalation record
     * created for CRMSF-4623
     * @Author : LTI
	*/
    public static void validateStatusChange(List<Escalation__c> newEscalationList, Map<Id, Escalation__c> oldEscalationMap, Map<Id, List<Task>> escalationTaskMap, Map<Id, Escalation__c> noTaskWithEscalationMap){
        
        for(Escalation__c newEscalationObj : newEscalationList){
            if(oldEscalationMap.containsKey(newEscalationObj.Id) &&
              (oldEscalationMap.get(newEscalationObj.Id).Status__c != newEscalationObj.Status__c) &&
               ClsApexConstants.STATUS_RESOLVED.equalsIgnoreCase(newEscalationObj.Status__c)){
                   if(noTaskWithEscalationMap.containskey(newEscalationObj.Id)){ //if no escalation tasks are associated
                          newEscalationObj.addError(ClsApexConstants.ESCALATION_STATUS_CHANGE_ERR_MSG);
                      }else if(escalationTaskMap.containsKey(newEscalationObj.Id)){
                          List<Task> allAssociatedTaskList = escalationTaskMap.get(newEscalationObj.Id);
                          List<Task> nonCompletedTaskList = new List<Task>();
                          for(Task taskObj : allAssociatedTaskList){
                              if(!taskObj.isClosed){
                                  //if an associated task is not closed then add error
                                  newEscalationObj.addError(ClsApexConstants.ESCALATION_STATUS_CHANGE_ERR_MSG);
                              }else{
                                  if(!ClsApexConstants.STATUS_COMPLETED.equalsIgnoreCase(taskObj.Status)){
                                      nonCompletedTaskList.add(taskObj);
                                  }
                              }
                          }
                          //if all associated tasks are closed but not in Completed status
                          if(nonCompletedTaskList.size() == allAssociatedTaskList.size()){
                              newEscalationObj.addError(ClsApexConstants.ESCALATION_STATUS_CHANGE_ERR_MSG);
                          }
                      }
               }
        }
    }
}