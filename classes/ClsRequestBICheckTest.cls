@isTest
private class ClsRequestBICheckTest {
        @testSetup static void setup() {
        Account accConsumer = new Account();
        Account accPayor = new Account();
        Benefits__c bnf = new Benefits__c();
        Id recIdConsumer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Consumers').getRecordTypeId();
        accConsumer = TestDataBuilder.getAccountListConsumer(1, recIdConsumer)[0];
        accConsumer.Territory_RSS__c = userinfo.getuserid();
        insert accConsumer;
        
        Id recIdPayor = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
        accPayor = TestDataBuilder.getAccountList(1, recIdPayor)[0];
        insert accPayor;
        
        Id recId = Schema.SObjectType.Benefits__c.getRecordTypeInfosByName().get('Benefits').getRecordTypeId();
        bnf = TestDataBuilder.getBenefits(accConsumer.Id, accPayor.Id, 1, 'Primary', recId)[0];
        insert bnf;
        }
        @isTest static void testQuadaxAPICallOut(){
            test.startTest();
            Test.setMock(HttpCalloutMock.class, new ClsMockQuadaxBIResponseGenerator());
            Benefits__c bnf = [SELECT Id, Payor__c, Account__c FROM Benefits__c WHERE Benefit_Hierarchy__c =: 'Primary' LIMIT 1];
            ClsRequestBICheck.requestBICheckWebService(bnf.Id, 'SSIP');
            ClsRequestBICheck.invokeQuadaxAPI(bnf.Id, 'SSIP');
            
            Test.stopTest();
        }
        
        @isTest static void testQuadaxAPICallOut1(){
            test.startTest();
            Test.setMock(HttpCalloutMock.class, new ClsMockQuadaxBIResponseGenerator());
            Benefits__c bnf = [SELECT Id, Payor__c, Account__c FROM Benefits__c WHERE Benefit_Hierarchy__c =: 'Primary' LIMIT 1];
            ClsRequestBICheck.requestBICheckWebService(bnf.Id, 'order');
            ClsRequestBICheck.invokeQuadaxAPI(bnf.Id, 'order');
            Set<Id>benefitsId = new Set<Id>();
            benefitsId.add(bnf.Id);
            //ClsRequestBICheck.BenefitsCreateTask(benefitsId,'tttt');     
            Test.stopTest();
        }
}