/****************************************************************************************************************
@Author        : Jagan Periyakaruppan
@Date Created    : 4/15/2019
@Description    : Class used in as a helper class for ClsAddressTriggerHandler
****************************************************************************************************************/
public class ClsAddressTriggerHandlerHelper{
    
    public static Map<Id, List<Address__c>> GetPrimaryAddressesTiedToAccount(String addressType, List<Id> accountIdsToProcess, List<Address__c> primaryAddresses)
    {
        Map<Id, List<Address__c>> accntIdPrimaryAddressMap = new Map<Id, List<Address__c>>();//Map holds the List of primary addresses tied to an account for a given type
        
        //Create a map between account id and list of primary addresses
        for(Address__c addr : [SELECT ID, Account__c, Street_Address_1__c, City__c, State__c, Zip_Postal_Code__c, Country__c, Primary_Flag__c, County__c FROM Address__c WHERE Account__c IN : accountIdsToProcess AND Id NOT IN : primaryAddresses AND Primary_Flag__c = true AND Address_Type__c = :addressType])
        {
            system.debug('----Entering the process of GetPrimaryAddressesTiedToAccount');
            system.debug('----Address Id is ' + addr.Id);
			//Remove the primary flag
			addr.Primary_Flag__c = false;
			//Map the account id to its possible primary addresses, which are to be reset
			if(accntIdPrimaryAddressMap.containsKey(addr.Account__c)) 
			{
				List<Address__c> addrList = accntIdPrimaryAddressMap.get(addr.Account__c);
				addrList.add(addr);
				accntIdPrimaryAddressMap.put(addr.Account__c, addrList);
			}
			else
			{
				accntIdPrimaryAddressMap.put(addr.Account__c, new List<Address__c> {addr});
			}
        }
        system.debug('----Ending the process of GetPrimaryAddressesTiedToAccount');
		return(accntIdPrimaryAddressMap);
    }
}