//Scheduling Class Creates Medicare Follow Up Records based
global class BclsUpdateAccntInfoOnForumlaUpdateSched implements Schedulable {
    // Execute at regular intervals
    global void execute(SchedulableContext ctx){
      BclsUpdateAccntInfoOnForumlaUpdate batch = new BclsUpdateAccntInfoOnForumlaUpdate ();
      Database.executebatch(batch, 100);
    }
}