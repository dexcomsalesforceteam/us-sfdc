@isTest(seeAlldata=false)
public class BclsProcessSSIPSchedulesTest {
    
      public static List<Account> thisAccount;
      public static List<Order_Header__c> thisOrder;
      public static SSIP_Rule__c ssipRule;
      public static Benefits__c bnf = new Benefits__c();    
    
    
    @testSetup public static void ssipBatchProcessTest(){
        Id recTypePayorId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
        Id recTypeConsumerId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Consumers').getRecordTypeId();
        Account payorAccount=new Account();
        payorAccount.name = 'Test Payor';
        payorAccount.Phone = '123456789';
        payorAccount.DOB__c = Date.newInstance(1992, 04, 23);
        payorAccount.Days_For_Sensor_2_Box_Shipment__c= 2;
        payorAccount.recordtypeid= recTypePayorId;
        payorAccount.BillingState = 'CA';
        payorAccount.BillingCountry = 'US';
        payorAccount.Default_Price_Book__c = Test.getStandardPricebookId();
        payorAccount.CHC_Approved__c = TRUE;
        insert payorAccount;
        
                
        Account testAccount=TestDataBuilder.testAccount();
        testAccount.Payor__c = payorAccount.id;
        testAccount.Customer_Type__c = 'Commercial';
        testAccount.BillingState = 'CA';
        testAccount.BillingCountry = 'US';
        testAccount.recordtypeid = recTypeConsumerId;        
        testAccount.Default_Price_Book__c = Test.getStandardPricebookId();
        //testAccount.Primary_Benefit__c = bnf.Id;
        insert testAccount;
        system.debug('testAccount:::'+testAccount);
        system.debug('testAccount pricebook'+testAccount.Default_Price_Book__c);
        
        //insert benefit
        Id recId = Schema.SObjectType.Benefits__c.getRecordTypeInfosByName().get('Benefits').getRecordTypeId();
        bnf = TestDataBuilder.getBenefits(testAccount.Id, payorAccount.Id, 1, 'Primary', recId)[0];
        bnf.MEMBER_ID__c = '12343';
        bnf.Payor__c = payorAccount.Id;
        insert bnf;

        Test.startTest();
        Order_Header__c orderHeader=new Order_Header__c();
        orderHeader.Account__c =testAccount.id;
        orderHeader.Order_Type__c = 'Standard Sales Order';
        insert orderHeader;
        
        Order_Item_Detail__c oid1  = new Order_Item_Detail__c(Item_Number__c = 'BUN-GF-003',Generation__c='G6', Order_Header__c = orderHeader.Id,Quantity__c =1,Quantity_Shipped__c =1,Shipping_Date__c =system.today()-185);
        insert oid1;
        
        //pull data : to get roll up summary fields 
        thisOrder = new List<Order_Header__c>([SELECT Id, Latest_Transmitter_Ship_Date__c from Order_Header__c where Id =:orderHeader.id]);
        thisAccount = new List<Account>([SELECT id, Latest_Transmitter_Ship_Date__c, Next_Possible_Date_For_Transmitter_Order__c, Default_Price_Book__c FROM Account where Id =:testAccount.Id]);
        system.debug('thisAccount[0]'+thisAccount[0]);
        system.debug('pricebook on account::'+testAccount.Default_Price_Book__c);
        //create SSIP Rule
        ssipRule = createSSIPRule(thisAccount[0].Id, System.today(), System.today()+360);
        system.debug('ssip rule'+ssipRule);
        insert ssipRule;
        
        ID ruleId = ssipRule.Id;
        List<SSIP_Schedule__c> newSsipSceduleList = new List<SSIP_Schedule__c>();
        newSsipSceduleList = [SELECT ID,Status__c,Eligibility_Check_Date__c,SSIP_Rule__r.Frequency_In_Days_Number__c from SSIP_Schedule__c WHERE SSIP_Rule__c =: ruleId]; 
       
        system.debug('*********************************newSsipSceduleList[0].SSIP_Rule__r.Frequency_In_Days__c 0   --' +newSsipSceduleList[0].SSIP_Rule__r.Frequency_In_Days_Number__c);
        newSsipSceduleList[1].Status__c = 'Eligibility In Review';
        newSsipSceduleList[0].Scheduled_Shipment_Date__c = Date.today();
        system.debug('*********************************newSsipSceduleList[0].Scheduled_Shipment_Date__c 0   --' +newSsipSceduleList[0].Scheduled_Shipment_Date__c);
        system.debug('*********************************Eligibility_Check_Date__c 0' +newSsipSceduleList[0].Eligibility_Check_Date__c);
        newSsipSceduleList[0].Scheduled_Shipment_Date__c = Date.today()+20;        
        system.debug('*********************************newSsipSceduleList[0].Scheduled_Shipment_Date__c 0   --' +newSsipSceduleList[0].Scheduled_Shipment_Date__c);
        system.debug('*********************************Eligibility_Check_Date__c 0' +newSsipSceduleList[0].Eligibility_Check_Date__c);
        
        
        
        Update newSsipSceduleList;
        Test.stopTest();
        system.debug('*********************************Update Statment is done');
        system.debug('*********************************newSsipSceduleList[0].Scheduled_Shipment_Date__c 0   --' +newSsipSceduleList[0].Scheduled_Shipment_Date__c);
        system.debug('*********************************Eligibility_Check_Date__c 0' +newSsipSceduleList[0].Eligibility_Check_Date__c);
    }
    
    @isTest
    public static void bclsG6OrderHearderCreateBatchTest(){
        test.startTest();
        BclsG6OrderHeaderCreationBatch obj =  new BclsG6OrderHeaderCreationBatch();
        Database.executeBatch(Obj);
        test.stopTest();
    }
    @isTest public static void runBatch() {
        System.debug('************************** in Run Batch method' );
        test.startTest();
        BclsProcessSSIPSchedules obj =  new BclsProcessSSIPSchedules();
        Database.executeBatch(Obj);
        test.stopTest();
        
        
    }
     public static SSIP_Rule__c createSSIPRule(string accountId, Date startDate, Date endDate){
        
        SSIP_Rule__c ssipRule=new SSIP_Rule__c();
        ssipRule.Account__c = accountId;
        ssipRule.Rule_Start_Date__c = startDate;
        ssipRule.Rule_End_Date__c = endDate;
        ssipRule.Product__c = 'G6 | BUN-OR-TX6 | 1';
        ssipRule.First_Shipment_Date__c= startDate.addDays(1);
        ssipRule.Disclaimer__c =true;
        ssipRule.Frequency_In_Days_Number__c = 180;
        //ssipRule.Price_Book_New__c = 'Account Default Pricebook';
        
        return ssipRule;
    }  
    
    @isTest
    public static void processSSIPSchdTest(){
        Test.startTest();
        BclsProcessSSIPSchedulesSched scheduler = new BclsProcessSSIPSchedulesSched();
        String sch = '0 0 23 * * ?';
        system.schedule('Process SSIP Scheduler', sch, scheduler);
        Test.stopTest();
    }
}