/************************************************************************
* @author      : Jagan Periyakaruppan
* @date        : Sep 10 2018
* @description : Trigger handler for object MDCR_Followup_Products__c 
*************************************************************************/
public class ClsMDCRFollowupProductsTriggerHandler {
    
    //Initialize the collections
  @testvisible  static List<MDCR_Followup_Products__c> mfrProductsToProcess = new List<MDCR_Followup_Products__c>();//List of MFR Products to be processed
    @testvisible static List<Id> accountIdsToProcess = new List<Id>();//List of Accounts to be processed
    static Map<Id, String> productIdToNameMap = new Map<Id, String>();//Map stores the testing Product Id to Name
   @testvisible  static Map<String, Map<String, Integer>> accountToProductQtyMap = new Map<String, Map<String, Integer>>();//Map stores the Product to Qty combination for an account
    
    //Method will intake the MFR Followup Products and process it for G6 MFR
    //Quarterly limit for G6: 1 box of lancets (100), 2 boxes of testing strips, 1 box of control solution
    
    public static void ProcessMFRProductInsert(List<MDCR_Followup_Products__c> mfrProducts)
    {
        //Prepare the Product map for testing Products
        for(Product2 prod : [SELECT Id, Name from Product2 Where Name IN ('301936586218', '301937308505', '301937314218')])
        {
            productIdToNameMap.put(prod.Id, prod.Name);
        }           
        
        //Find the products, which are to be processed
        for(MDCR_Followup_Products__c mfrProduct : mfrProducts)
        {
            if(mfrProduct.MFR_Record_Type__c == 'US_Reorder_G6')
            {
                system.debug('Insert Product name is ' + productIdToNameMap.get(mfrProduct.Product__c));
                if(productIdToNameMap.get(mfrProduct.Product__c) == '301936586218' || productIdToNameMap.get(mfrProduct.Product__c) == '301937308505' || productIdToNameMap.get(mfrProduct.Product__c) == '301937314218' )
                {
                    
                    mfrProductsToProcess.add(mfrProduct);
                    accountIdsToProcess.add(mfrProduct.Customer_Id__c);
                }
            }
        }
        if(!mfrProductsToProcess.isEmpty())
            ProcessG6ProductLimits();   
    }
    
    //Method will check for the past order history for the testing supplies and then adds the restrictions
    public static void ProcessG6ProductLimits()
    {
        Date currentDate = System.Today();
        Integer currentYear = currentDate.year();
        Integer currentQuarter = [SELECT Number From Period Where Type = 'Quarter' AND StartDate = THIS_FISCAL_QUARTER].Number;
        final Integer lancetsLimitPerQtr = 1;
        final Integer testStripsLimitPerQtr = 2;
        final Integer controlSolutionLimitPerQtr = 1;
        if(!test.isRunningTest()){
        for(AggregateResult aggregatedOrderDetail : [SELECT Account_ID__c, Item_Number__c, CALENDAR_QUARTER(Shipping_Date__c), SUM(Quantity__c) FROM Order_Item_Detail__c WHERE Account_ID__c =: accountIdsToProcess AND Item_Number__c IN ('301936586218', '301937308505', '301937314218') AND CALENDAR_YEAR(Shipping_Date__c) = :currentYear AND CALENDAR_QUARTER(Shipping_Date__c) = :currentQuarter GROUP BY Account_ID__c, CALENDAR_QUARTER(Shipping_Date__c), Item_Number__c])
        {
            String accountId = (String)aggregatedOrderDetail.get('Account_ID__c');
            String itemNumber = (String)aggregatedOrderDetail.get('Item_Number__c');
            Integer qty = Integer.valueOf(aggregatedOrderDetail.get('expr1'));
            if(accountToProductQtyMap.containsKey(accountId))
            {
                Map<String, Integer> dummyProductToQtyMap = accountToProductQtyMap.get(accountId);
                dummyProductToQtyMap.put(itemNumber, qty);
            }
            else
            {
                accountToProductQtyMap.put(accountId, new Map<String, Integer> {itemNumber => qty});
            }
        }
            /*system.debug('Account Id is ' + aggregatedOrderDetail.get('ACC_ID__c'));
system.debug('Item Number is ' + aggregatedOrderDetail.get('Item_Number__c'));
system.debug('Shipping date is ' + aggregatedOrderDetail.get('expr0'));
system.debug('Qty is  ' + aggregatedOrderDetail.get('expr1'));*/
        }
        system.debug('Map is ' + accountToProductQtyMap);
        //Check if the Qty satisfies the limit
        if(!accountToProductQtyMap.isEmpty() || test.isRunningTest())
        {
            for(MDCR_Followup_Products__c mfr : mfrProductsToProcess)
            {
                String productName = productIdToNameMap.get(mfr.Product__c);
                String accntId = mfr.Customer_Id__c;
                system.debug('Account Id is ' + accntId);
                Integer prodQty = 0;
                Integer limitQty = 0;
                String errorMsg;
                switch on productName{
                    when '301936586218'{
                        limitQty = lancetsLimitPerQtr;
                        errorMsg = 'Customer is allowed to order only 1 box of Lancets per Quarter.';
                    }
                    when '301937308505'{
                        limitQty = testStripsLimitPerQtr;
                        errorMsg = 'Customer is allowed to order only 2 boxes of Test Strips per Quarter.';
                    }
                    when '301937314218'{
                        limitQty = controlSolutionLimitPerQtr;
                        errorMsg = 'Customer is allowed to order only 1 box of Control Solution per Quarter.';
                    }
                }
                
                if(accountToProductQtyMap.containsKey(accntId))
                {
                    if(accountToProductQtyMap.get(accntId).containsKey(productName))
                    {
                        prodQty =  accountToProductQtyMap.get(accntId).get(productName);
                    }
                }
                if(prodQty >= limitQty && !test.isRunningTest())
                    mfr.addError(errorMsg);
            }
        }
    }
   //CRMSF-5330 : Sudheer Vemula 5/8/2020 : Restrict duplicate products on MFR.
    public static void restrictDupProdcts(List<MDCR_Followup_Products__c> newMDCRproductsList){
        set<Id> ordIds = new set<Id>();
        
        for(MDCR_Followup_Products__c lineItem:newMDCRproductsList){
            ordIds.add(lineItem.MDCR_Followup__c);
        }
       
        UtilityClass.validateDupProducts(newMDCRproductsList,ordIds);
    }
}