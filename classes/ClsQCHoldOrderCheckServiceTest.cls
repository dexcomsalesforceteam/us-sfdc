/*
 * Test Class to cover ClsQCHoldOrderCheckService
 * created By LTI
*/
@isTest(seeAllData = false)
public class ClsQCHoldOrderCheckServiceTest {

    @testSetup
    public static void setUpData(){
        List<Account> accountInsertList = new List<Account>();
        Account prescriberAccount=new Account();
        prescriberAccount.lastname='prescriber Account';
        prescriberAccount.recordtypeId= ClsApexUtility.getRecordTypeIdByDeveloperName('Account', 'Prescriber');
        prescriberAccount.Inactive__c =false;        
        //insert prescriberAccount;
        accountInsertList.add(prescriberAccount);
        
        Account payorAccount = new Account();
        payorAccount.Name = 'payor1';
        payorAccount.recordtypeId= ClsApexUtility.getRecordTypeIdByDeveloperName('Account', 'Payor');
        payorAccount.Inactive__c =false;
        payorAccount.Is_Medicare_Payor__c = false;
        accountInsertList.add(payorAccount);
        database.insert(accountInsertList);
        
        
        Account testAccount=TestDataBuilder.testAccount();
        testAccount.Default_Price_Book__c=Test.getStandardPricebookId();
        testAccount.PersonEmail = 'Test@gmail.comQCHoldOrdChk';
        testAccount.BillingStreet='abc';
        testAccount.BillingCity ='Bang';
        testAccount.BillingState ='KN';
        testAccount.BillingPostalCode='560037';
        testAccount.ShippingStreet  ='Bang';
        testAccount.ShippingCity='Bang';
        testAccount.ShippingState='KN';
        testAccount.ShippingCountry='US';
        testAccount.ShippingPostalCode='560037';
        testAccount.Party_ID__c ='123344';
        testAccount.Last_Order_Pricebook__c = 'New Test';
        testAccount.Prescribers__c = prescriberAccount.Id;
        testAccount.RecordTypeId = ClsApexUtility.getRecordTypeIdByDeveloperName('Account', 'Consumers');
        insert testAccount;
        
        
        //Primary_Benefit__c
        Benefits__c benefitObj = new Benefits__c();
        benefitObj.PRIOR_AUTH_REQUIRED__c = 'Y';
        benefitObj.New_Order_Auth_Products__c = 'Receiver;Transmitter;Sensor';
        benefitObj.Reorder_Auth_Products__c = 'Receiver;Transmitter;Sensor';
        benefitObj.Account__c = testAccount.Id;
        benefitObj.Payor__c = payorAccount.Id;
        insert benefitObj;
        
        //create address and update account details
        List<Address__c> addrList = new List<Address__c>();
        Address__c addrs1 = TestDataBuilder.getAddressList(testAccount.Id, true,'SHIP_TO', 1)[0];
        addrs1.Oracle_Address_ID__c ='123455';
        addrs1.Address_Verified__c = 'Yes';
        addrList.add(addrs1);
        Address__c addrs2 = TestDataBuilder.getAddressList(testAccount.Id, true,'BILL_TO', 1)[0];
        addrs2.Oracle_Address_ID__c ='123465';
        addrs2.Address_Verified__c = 'Yes';
        addrList.add(addrs2);
        Test.startTest();
        Database.insert(addrList);
        
        testAccount.Primary_Benefit__c = benefitObj.Id;
        testAccount.Primary_Bill_To_Address__c = addrs2.Id;
        testAccount.Primary_Ship_To_Address__c = addrs1.Id;
        update testAccount;
        
        Order orderObj = new Order();
        orderObj.AccountId = testAccount.Id;
        orderObj.Type = 'Standard Sales Order';
        orderObj.Shipping_Address__c = addrs1.Id;
        orderObj.Status = 'Draft';
        orderObj.EffectiveDate = system.today()-10;
        insert orderObj;
        Test.stopTest();
    }
    
    @isTest
    public static void getOrderDetailsTest1(){
        Order orderRec = [Select Id,AccountId from Order where Account.PersonEmail = 'Test@gmail.comQCHoldOrdChk' and Type = 'Standard Sales Order' limit 1];
        Test.startTest();
        ClsQCHoldOrderCheckService.getOrderDetails(String.valueOf(orderRec.id), String.valueOf(orderRec.AccountId), 'New Order', true);
        Test.stopTest();
        
    }
    /*
     * when prior auth is set to N
    */
    @isTest
    public static void getOrderDetailsTest2(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Data Integrator'];
        User dataIntgUser = new User(Alias = 'newUser', Email='newDataIntgUser@qcHoldTest.com',
                                     EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                                     LocaleSidKey='en_US', ProfileId = p.Id,
                                     TimeZoneSidKey='America/Los_Angeles', UserName='newDataIntgUser@qcHoldTest.com');
        
        Order orderRec = [Select Id,AccountId from Order where Account.PersonEmail = 'Test@gmail.comQCHoldOrdChk' and Type = 'Standard Sales Order' limit 1];
        Benefits__c benefitObj = [Select Id,PRIOR_AUTH_REQUIRED__c from Benefits__c where Account__c = : orderRec.AccountId limit 1];
        benefitObj.PRIOR_AUTH_REQUIRED__c = 'N';
        system.runAs(dataIntgUser){
            update benefitObj;
        }
        
        
        Test.startTest();
        ClsQCHoldOrderCheckService.getOrderDetails(String.valueOf(orderRec.id), String.valueOf(orderRec.AccountId), 'New', false);
        Test.stopTest();
    }
}