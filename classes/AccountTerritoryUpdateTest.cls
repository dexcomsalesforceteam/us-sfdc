/********************************************************************************
@author Abhishek Parghi
@date 10/08/2015
@description: Test class for PCS Call List.
*******************************************************************************/
/********************************************************************************
@author Jagan Periyakaruppan
@date 10/23/2017
@description: Test class update to accomodate roster changes.
*******************************************************************************/
/********************************************************************************
@author Jagan Periyakaruppan
@date 12/18/2017
@description: Added methods and converted the insert/update statements to List
*******************************************************************************/
/********************************************************************************
@author Jagan Periyakaruppan
@date 02/08/2017
@description: Added methods and for Prescriber Territory Changes
*******************************************************************************/

@istest
public class AccountTerritoryUpdateTest{
    Static testmethod void AccountTerritoryUpdate_test(){ 
        
        User Ap = TestDataBuilder.testUser();
        insert AP;  
        User u1 = [Select id, lastname from USer where lastname= 'TUser' ];
        
        Territory_Alignment__c z4 = new Territory_Alignment__c ();
        z4.name = '05442';
        z4.Territory_Rep__c= u1.id;
        //Jagan added below lines
        z4.Fax__c = '8899987789';
        z4.SA__c = u1.Id;
        z4.PSS_RingDNA_Number__c = '8899987780';
        z4.PCS__c = u1.Id;
        z4.RSS__c = u1.Id;
        z4.RSS_Supervisor__c = u1.Id;
        z4.Supervisor__c = u1.Id;
        insert z4;  
        
        Territory_Alignment__c Tz = [Select id,name,RSD__c,Territory_Rep__c from Territory_Alignment__c WHERE Name =: '05442'];
        
        Zip_Territory__c z3 = new Zip_Territory__c ();
        z3.name = '95112';
        z3.Territory_Code__c ='05442' ;
        insert z3;        
        map<string,string> territoryMap1=new map<string,string>();  
        for(Zip_Territory__c z : [Select name, Territory_Code__c from Zip_Territory__c WHERE name =: '95112']) {
            territoryMap1.put (z.name, z.Territory_Code__c);
        }
        //Accounts that are to be inserted
        List<Account> accountsToBeInserted = new List<Account>();
        List<Account> accountsToBeUpdated = new List<Account>();
        
        //Insert US Prescriber record
        Id prescriberRecordTypeId = [select Id from RecordType where (Name='Prescriber') and (SobjectType='Account')].Id;
        Account usPrescriber = new Account();
        usPrescriber.RecordTypeId = prescriberRecordTypeId;
        usPrescriber.FirstName = 'PrescriberFirstName';
        usPrescriber.LastName = 'PrescriberLastName';
        usPrescriber.PersonEmail = 'PrescriberTest@gmail.com';
        usPrescriber.Phone = '12345678911';
        usPrescriber.BillingState = 'AP';
        usPrescriber.BillingCity = 'APTest City';
        usPrescriber.BillingStreet = 'APTest Street';
        usPrescriber.ShippingPostalCode =  '05442';
        usPrescriber.BillingCountry = 'United States';
        usPrescriber.Territory_Code__c = 'US051710';
        insert usPrescriber;
        
        
         // Adding Prescriber #AJP 02/12/2018 
       Id prescRecordTypeID=[select Id from RecordType where (Name='Prescriber') and (SobjectType='Account')].Id;
       Account PresAcc = new Account();
       PresAcc.RecordTypeId = prescRecordTypeID;
       PresAcc.FirstName = 'APTestfirstname523';
       PresAcc.LastName = 'APTestlastname5';
       PresAcc.Phone = '12345678900';
       PresAcc.BillingState = 'CA';
       PresAcc.BillingCity = 'APTest City';
       PresAcc.BillingStreet = 'APTest Street';
       PresAcc.ShippingPostalCode =  '951120';
       PresAcc.BillingCountry = 'United States';
       PresAcc.territory_code__C = null;
       insert PresAcc; 
       accountsToBeUpdated.add(PresAcc); 
     //  accountsToBeInserted.add(PresAcc); 
         
       // Creating a task on Prescriber   #AJP 02/12/2018 
       
         Task T = new Task();
         T.WhatId = PresAcc.ID;
         T.Prescriber_Territory_Code__c = null;
         insert T;
        
        

        Id personRecordTypeID=[select Id from RecordType where (Name='Consumers') and (SobjectType='Account')].Id;
        Account acc5 = new Account();
        acc5.RecordTypeId = personRecordTypeID;
        acc5.FirstName = 'APTestfirstname';
        acc5.LastName = 'APTestlastname';
        acc5.PersonEmail = 'APTestAP@gmail.com';
        acc5.Phone = '12345678900';
        acc5.BillingState = 'CA';
        acc5.BillingCity = 'APTest City';
        acc5.BillingStreet = 'APTest Street';
        acc5.ShippingPostalCode =  '95112';
        acc5.BillingCountry = 'United States';
        //insert acc5;
        accountsToBeUpdated.add(acc5);
        accountsToBeInserted.add(acc5);
        
        Account acc15 = new Account();
       acc15.RecordTypeId = personRecordTypeID;
       acc15.FirstName = 'APTestfirstname';
       acc15.LastName = 'APTestlastname';
       acc15.PersonEmail = 'APTestAP@gmail.com';
       acc15.Phone = '12345678900';
       acc15.BillingState = 'CA';
       acc15.BillingCity = 'APTest City';
       acc15.BillingStreet = 'APTest Street';
       acc15.ShippingPostalCode =  '95112';
       acc15.BillingCountry = 'United States';
       acc15.Prescribers__c = PresAcc.Id;
       acc15.Party_ID__c = '00000';
       //insert acc15;
       accountsToBeUpdated.add(acc15);
        accountsToBeInserted.add(acc15);
        
        

        Account acc6 = new Account();
        acc6 .RecordTypeId = personRecordTypeID;
        acc6 .FirstName = 'APTestfirstname';
        acc6 .LastName = 'APTestlastname';
        acc6.territory_code__C = '05442';
        Acc6.Prescriber__c = null;
        //insert acc6;
        accountsToBeUpdated.add(acc6);
        accountsToBeInserted.add(acc6);
        
         
        
        
        //Insert EMEA CH Consumer account
        Account acc7 = new Account();
        acc7.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('EMEA CH Consumer').getRecordTypeId(); 
        acc7.FirstName = 'APTestfirstname - CH';
        acc7.LastName = 'APTestlastname - CH';
        acc7.PersonEmail = 'APTestAPCH@gmail.com';
        acc7.Phone = '12345678901';
        acc7.Territory_Code__c = '05442';
        acc7.Int_LN_Shipping_postcode__c = '5441';
        acc7.Prescriber__c = null;
        //insert acc7;
        accountsToBeUpdated.add(acc7);
        accountsToBeInserted.add(acc7);
        // Consumer without Prescriber   #AJP 02/12/2018
       Account acc17 = new Account();
       acc17.RecordTypeId = personRecordTypeID;
       acc17.FirstName = 'APTestfirstname';
       acc17.LastName = 'APTestlastname';
       acc17.PersonEmail = 'APTestAP@gmail.com';
       acc17.Phone = '12345678900';
       acc17.BillingState = 'CA';
       acc17.BillingCity = 'APTest City';
       acc17.BillingStreet = 'APTest Street';
       acc17.ShippingPostalCode =  '951120';
       acc17.BillingCountry = 'United States';
       acc17.Prescriber__c = null;
       //insert acc17;  
       accountsToBeUpdated.add(acc17);
       accountsToBeInserted.add(acc17); 
        
        //Insert IE Consumer account
        Account acc8 = new Account();
        Id IEConsumerRecordTypeID=[select Id from RecordType where (Name='IE Consumer') and (SobjectType='Account')].Id;
        acc8.RecordTypeId = IEConsumerRecordTypeID; 
        acc8.FirstName = 'APTestfirstname - IE';
        acc8.LastName = 'APTestlastname - IE';
        acc8.PersonEmail = 'APTestAPIE@gmail.com';
        acc8.Phone = '12345678902';
        acc8.Int_LN_Shipping_postcode__c = '05441';
        acc8.Prescriber__c = null;
        //insert acc8;
        accountsToBeUpdated.add(acc8);
        accountsToBeInserted.add(acc8);
        
        //Insert CA Consumer account
        Account acc9 = new Account();
        acc9.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('CA Consumer').getRecordTypeId(); 
        acc9.FirstName = 'APTestfirstname - CA';
        acc9.LastName = 'APTestlastname - CA';
        acc9.PersonEmail = 'APTestAPCA@gmail.com';
        acc9.Phone = '12345678909';
        acc9.Territory_Code__c = '05442';
        acc9.Int_LN_Shipping_postcode__c = 'V2N 4B6';
        acc9.Prescriber__c = null;
        //insert acc9;
        accountsToBeUpdated.add(acc9);
        accountsToBeInserted.add(acc9);
        
        test.startTest();
        insert accountsToBeInserted;
    //  update accountsToBeUpdated;
        System.assertEquals(acc5.ShippingPostalCode , z3.name);  
        z4.RSS_RingDNA_Number__c = '8899987788';
        update z4;
            
        
       PresAcc.FirstName = 'AJTestfirstname523'; 
      // update PresAcc;   
       PresAcc.territory_code__C = '520';
   //    update PresAcc;   
       PresAcc.LastName = 'AJTestlastname5';
   //    update PresAcc; 
       PresAcc.Phone = '963215648';
   //    update PresAcc; 
       PresAcc.BillingState = 'CA';
   //    update PresAcc;   
       PresAcc.BillingCity = 'AJTest City';
  //     update PresAcc;   
       PresAcc.BillingStreet = 'AJTest Street';  
   //    update PresAcc;
       
       usPrescriber.Territory_Code__c = 'US051711';
//     update usPrescriber;
        
        
       System.assertEquals('AJTest City', PresAcc.BillingCity);
       System.assertEquals(null, T.Prescriber_Territory_Code__c);
       System.assertEquals(acc5.ShippingPostalCode , z3.name);  
       System.assertEquals(6, acc17.ShippingPostalCode.length());
       System.assertEquals('05442', acc7.Territory_Code__c);  // 'CH010501'
       System.assertEquals(null,acc8.Territory_Code__c);
       System.assertEquals(null,acc8.ownerID);  //u1.id
       System.assertEquals(null,acc9.ownerID);  //u1.id  
       System.assertEquals(acc15.ShippingPostalCode , z3.name);   
        
       acc15.Prescribers__c = null;
       update acc15; 
        
        
        test.stopTest();
        
    }
}