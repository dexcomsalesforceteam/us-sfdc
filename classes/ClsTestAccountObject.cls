/********************************************************************************
Description    : Test class to test the Account Address Functionality
*********************************************************************************/
@isTest
private class ClsTestAccountObject {
    @isTest static void TestAccountAddress(){
		//Insert 10 account records
        List<Account> accts = ClsTestDataFactory.createAccountsWithBillingAddress(10, 'Consumers', 'US');
        Test.startTest();
        ClsAccountAddressTriggerStaticClass.addressChangeInvokedFromAddress = FALSE;
        ClsAccountAddressTriggerStaticClass.addressChangeInvokedFromAccount = TRUE;	
        //Get the first account, which was inserted
        Account a = accts[0];
        String street = a.BillingStreet;
        String city = a.BillingCity;
        String state = a.BillingState;
        String postalCode = a.BillingPostalCode;
        String country = a.BillingCountry;
        List<Address__c> addrList = [SELECT Id FROM Address__c WHERE Account__c = :a.Id];
        system.assertEquals(2, addrList.size());
        addrList.clear();
        //Change the Billing and Shipping address and save the account
        a.BillingStreet = '1 Main Street';
        a.BillingCity = 'San Diego';
        a.BillingState = 'CA';		
        a.BillingPostalCode = '92121';
        a.BillingCountry = 'US';
        a.ShippingStreet = '2 Main Street';
        a.ShippingCity = 'San Diego';
		a.ShippingState = 'CA';	
        a.ShippingPostalCode = '92121';
        a.ShippingCountry = 'US';
        Update a;
        addrList = [SELECT Id FROM Address__c WHERE Account__c = :a.Id];
        system.assertEquals(4, addrList.size());
        addrList.clear();
        //Insert new address record
        List<Address__c> addrListToBeInserted = new List<Address__c>();
        Address__c newBillToAddr = new Address__c();
        newBillToAddr.Account__c = a.Id;
        newBillToAddr.Street_Address_1__c = '3 Main Street';
        newBillToAddr.City__c = 'San Diego';
		newBillToAddr.State__c = 'CA';
        newBillToAddr.Zip_Postal_Code__c = '92121';
        newBillToAddr.Country__c = 'US';
        newBillToAddr.Address_Type__c = 'BILL_TO';
        newBillToAddr.Primary_Flag__c = TRUE;
        addrListToBeInserted.add(newBillToAddr);
        Address__c newShipToAddr = new Address__c();
        newShipToAddr.Account__c = a.Id;
        newShipToAddr.Street_Address_1__c = '4 Main Street';
        newShipToAddr.City__c = 'San Diego';
		newShipToAddr.State__c = 'CA';
        newShipToAddr.Zip_Postal_Code__c = '92121';
        newShipToAddr.Country__c = 'US';
        newShipToAddr.Address_Type__c = 'SHIP_TO';
        newShipToAddr.Primary_Flag__c = TRUE;
        addrListToBeInserted.add(newShipToAddr);
        insert addrListToBeInserted;
        //Process to make existing address as Primary
        List<Address__c> addrListToBeUpdated = new List<Address__c>();
        //Query for original billing primary address and make it as primary
        Address__c billToAddrToBeMadePrimary = [SELECT Id FROM Address__c WHERE Street_Address_1__c = :street AND Account__c = :a.Id AND Address_Type__c = 'BILL_TO'];
        billToAddrToBeMadePrimary.Primary_Flag__c = TRUE;
        addrListToBeUpdated.add(billToAddrToBeMadePrimary);
        //Query for original shipping primary address and make it as primary
        Address__c shipToAddrToBeMadePrimary = [SELECT Id FROM Address__c WHERE Street_Address_1__c = :street AND Account__c = :a.Id AND Address_Type__c = 'SHIP_TO'];
        shipToAddrToBeMadePrimary.Primary_Flag__c = TRUE;
        addrListToBeUpdated.add(shipToAddrToBeMadePrimary);
        update addrListToBeUpdated;
        //Make existing address as primary
        //Change the Billing and Shipping address and save the account
        a.BillingStreet = '3 Main Street';
        a.BillingCity = 'San Diego';
		a.BillingState = 'CA';
        a.BillingPostalCode = '92121';
        a.BillingCountry = 'US';
        a.ShippingStreet = '4 Main Street';
        a.ShippingCity = 'San Diego';
		a.ShippingState = 'CA';
        a.ShippingPostalCode = '92121';
        a.ShippingCountry = 'US';
        Update a;
        Test.stopTest();
        //Confirm the counts
        addrList = [SELECT Id FROM Address__c WHERE Account__c = :a.Id];
        system.assertEquals(10, accts.size());
        system.assertEquals(6, addrList.size());
    }
}