@isTest(seeAllData = false)
public class ClsPlatformEventsTriggerTest {
    
    @testsetup
    public static void testDataSetup(){
        
        Id recIdPayor = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
        Account accPayor = TestDataBuilder.getAccountList(1, recIdPayor)[0];
        accPayor.Secondary_Plan_Name__c = 'Payor test';
        insert accPayor;

        Account testAccount=TestDataBuilder.testAccount();
        testAccount.Default_Price_Book__c=Test.getStandardPricebookId();
        testAccount.PersonEmail = 'Test@gmail.com1241';
        testAccount.BillingStreet='abc';
        testAccount.BillingCity ='Bang';
        testAccount.BillingState ='KN';
        testAccount.BillingPostalCode='56003';
        testAccount.BillingCountry = 'US';
        testAccount.ShippingStreet  ='Bang';
        testAccount.ShippingCity='Bang';
        testAccount.ShippingState='KN';
        testAccount.ShippingPostalCode='56003';
        testAccount.ShippingCountry = 'US';
        testAccount.Party_ID__c ='123344';
        testAccount.Receiver_Quantity_Prescribed__c = 20;
        testAccount.Sensor_Quantity_Prescribed__c = 20;
        testAccount.Transmitter_Quantity_Prescribed__c = 20;
        testAccount.RecordTypeId = ClsApexUtility.getRecordTypeIdByDeveloperName('Account','Consumers');
        testAccount.Payor__c = accPayor.Id;
        Database.insert(testAccount);

        SSIP_Rule__c ssipRuleRecord = new SSIP_Rule__c();
        ssipRuleRecord.Account__c = testAccount.Id;
        ssipRuleRecord.Rule_Start_Date__c = system.today()-5;
        ssipRuleRecord.Rule_End_Date__c = system.today()+365;
        ssipRuleRecord.Product__c = 'G6 | BUN-OR-TX6 | 1';
        ssipRuleRecord.First_Shipment_Date__c= system.today()-2;
        ssipRuleRecord.Disclaimer__c =true;
        ssipRuleRecord.Frequency_In_Days_Number__c = 180;
        test.startTest();
        Database.insert(ssipRuleRecord);
        
        Address__c addr = new Address__c();
        addr.Account__c = testAccount.Id;
        addr.Address_Type__c = 'SHIP_TO';
        addr.Address_Verified__c = 'Yes';
        addr.City__c = 'San Diego';
        addr.State__c = 'MA';
        addr.Country__c = 'US';
        addr.Street_Address_1__c = 'Test12';
        addr.Zip_Postal_Code__c = '12345';
        insert addr;
        test.stopTest();
    }
    @isTest static void accountPrimaryShipToAddressChangeTest() {
        
        // Create a test event instance
        Account testAccountRec = [Select Id from Account where PersonEmail = 'Test@gmail.com1241'];
        Address__c addrRecord = [Select Id from Address__c where Address_Type__c = 'SHIP_TO' limit 1];
        Account_Primary_ShipTo_Address_Event__e shipToAddressChangeEvent;
        if(testAccountRec !=null && testAccountRec.Id !=null){
            shipToAddressChangeEvent = new Account_Primary_ShipTo_Address_Event__e(Account_Id__c=testAccountRec.Id, 
                                             Primary_Ship_To_Address__c =addrRecord.Id);
        }
        Test.startTest();
        // Publish test event
        Database.SaveResult shipToAddressChangeEventResult = EventBus.publish(shipToAddressChangeEvent);            
        Test.stopTest();     
        
    }
    
    @isTest
    public static void testPayorChangeEvent(){
        Account testAccountRec = [Select Id from Account where PersonEmail = 'Test@gmail.com1241'];
        Account_Payor_Change__e payorChangeEvent;
        if(testAccountRec !=null && testAccountRec.Id != null){
            payorChangeEvent = new Account_Payor_Change__e(Account_Id__c=String.valueOf(testAccountRec.Id));
        }
        Test.startTest();
        Database.SaveResult payorChangeEventResult = EventBus.publish(payorChangeEvent);
        Test.stopTest();
    }
    
    @isTest
    public static void accountGenerationChangeTest(){
        Account testAccountRec = [Select Id from Account where PersonEmail = 'Test@gmail.com1241'];
        Account_Generation_Change__e generationChangeEvent;
        if(testAccountRec !=null && testAccountRec.Id !=null){
            generationChangeEvent= new Account_Generation_Change__e(Account_Id__c=String.valueOf(testAccountRec.Id), 
                                             New_Generation__c ='G6');
        }
        
        Test.startTest();
        Database.SaveResult generationChangeEventResult = EventBus.publish(generationChangeEvent);
        Test.stopTest();
    }
    
    @isTest
    public static void sendEmailToCrmSupportTeamTest(){
        List<Account> accountList = new List<Account>();
        Account accRec = new Account(Name = 'Some name', Party_Id__c = '12345');
        accountList.add(accRec);
        Test.startTest();
        ClsPlatformEvtTriggerHelper.createApexDebugLogRecords(accountList); 
        Test.stopTest();
    }    
}