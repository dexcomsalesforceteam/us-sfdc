/*
 * This class is created to cover ClsQCHoldDocumentsCheckService
 * @Author - LTI
*/
@isTest(seeAllData = false)
public class ClsQCHoldDocumentsCheckServiceTest {
    
    @testSetUp
    public static void setUpTestData(){
        List<Account> accountInsertList = new List<Account>();
        //prescriber
        Account prescriberAccount=new Account();
        prescriberAccount.lastname='prescriber Account';
        prescriberAccount.recordtypeId= ClsApexUtility.getRecordTypeIdByDeveloperName('Account', 'Prescriber');
        prescriberAccount.Inactive__c =false;        
        accountInsertList.add(prescriberAccount);
        
        //Payor
        Account payorAccount = new Account();
        payorAccount.recordtypeId= ClsApexUtility.getRecordTypeIdByDeveloperName('Account', 'Payor');
        payorAccount.Name = 'Payor';
        payorAccount.Skip_Payor_Matrix__c = true;
        accountInsertList.add(payorAccount);
        Database.insert(accountInsertList);              
        
        //patient
        Account testAccount=TestDataBuilder.testAccount();
        testAccount.Default_Price_Book__c=Test.getStandardPricebookId();
        testAccount.PersonEmail = 'Test@gmail.comQCDocChkService';
        testAccount.BillingStreet='abc';
        testAccount.BillingCity ='Bang';
        testAccount.BillingState ='KN';
        testAccount.BillingCountry = 'US';
        testAccount.BillingPostalCode='560037';
        testAccount.ShippingStreet  ='Bang';
        testAccount.ShippingCity='Bang';
        testAccount.ShippingState='KN';
        testAccount.ShippingCountry = 'US';
        testAccount.ShippingPostalCode='560037';
        testAccount.Party_ID__c ='123344';
        testAccount.Prescribers__c = prescriberAccount.Id;
        testAccount.Payor__c = payorAccount.Id;
        testAccount.RecordTypeId = ClsApexUtility.getRecordTypeIdByDeveloperName('Account', 'Consumers');
        insert testAccount;
        system.debug('testAccount:::::'+testAccount);
        system.debug('testAccount::::'+testAccount.Payor__c);
        //Primary_Benefit__c
        Benefits__c benefitObj = new Benefits__c();
        benefitObj.PRIOR_AUTH_REQUIRED__c = 'Y';
        benefitObj.New_Order_Auth_Products__c = 'Receiver;Transmitter;Sensor';
        benefitObj.Reorder_Auth_Products__c = 'Receiver;Transmitter;Sensor';
        benefitObj.Account__c = testAccount.Id;
        benefitObj.Payor__c = payorAccount.Id;
        insert benefitObj;
        List<Address__c> addrList = new List<Address__c>();
        Address__c addrs1 = TestDataBuilder.getAddressList(testAccount.Id, true,'SHIP_TO', 1)[0];
        addrs1.Oracle_Address_ID__c ='123455';
        addrs1.Address_Verified__c = 'Yes';
        addrList.add(addrs1);
        Address__c addrs2 = TestDataBuilder.getAddressList(testAccount.Id, true,'BILL_TO', 1)[0];
        addrs2.Oracle_Address_ID__c ='123465';
        addrs2.Address_Verified__c = 'Yes';
        addrList.add(addrs2);
        Test.startTest();
        Database.insert(addrList);
        
        testAccount.Primary_Benefit__c = benefitObj.Id;
        testAccount.Primary_Bill_To_Address__c = addrs2.Id;
        testAccount.Primary_Ship_To_Address__c = addrs1.Id;
        update testAccount;
        
        Order orderObj = new Order();
        orderObj.AccountId = testAccount.Id;
        orderObj.Type = 'Standard Sales Order';
        orderObj.Shipping_Address__c = addrs1.Id;
        orderObj.Status = 'Draft';
        orderObj.EffectiveDate = system.today()-10;
        insert orderObj;
        
        //insert Document type
        List<DocumentType__c> docTypeList = new List<DocumentType__c>();
        DocumentType__c docType1 = new DocumentType__c();
        docType1.Name = 'Chart Notes';
        docType1.DocumentDescription__c = 'Description';
        docTypeList.add(docType1);
        
        DocumentType__c docType2 = new DocumentType__c();
        docType2.Name = 'Chart Notes';
        docType2.DocumentDescription__c = 'Description2';
        docTypeList.add(docType2);
        
        database.insert(docTypeList);
        
        Rules_PayorToDoc__c rulesPayorToDoc = new Rules_PayorToDoc__c();
        rulesPayorToDoc.Payor__c = payorAccount.Id;
        rulesPayorToDoc.OrderType__c = 'New Order';
        rulesPayorToDoc.Required__c = true;
        rulesPayorToDoc.DocumentType__c = docType1.Id;
        
        insert rulesPayorToDoc;
        Test.stopTest();
        system.debug('rulesPayorToDoc:::::'+rulesPayorToDoc);
    }
    
    @isTest
    public static void getDocumentDetailsTest_Positive(){
        Order orderRec = [Select Id,AccountId from Order where Account.PersonEmail = 'Test@gmail.comQCDocChkService' and Type = 'Standard Sales Order' limit 1];
        
        list<Rules_PayorToDoc__c> rulesPayorToDocList1 = [Select Id,Payor__c,OrderType__c,Required__c from Rules_PayorToDoc__c];
            system.debug('rulesPayorToDocList from test::'+rulesPayorToDocList1);
        
        Test.startTest();
        ClsQCHoldDocumentsCheckService.getDocumentDetails(orderRec.Id, orderRec.AccountId, 'New Order', true);
        Test.stopTest();
        
        list<Rules_PayorToDoc__c> rulesPayorToDocList2 = [Select Id,Payor__c,OrderType__c,Required__c from Rules_PayorToDoc__c];
            system.debug('rulesPayorToDocList from test after test run::'+rulesPayorToDocList2);
    }

}