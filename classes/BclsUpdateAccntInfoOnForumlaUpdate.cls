/*--------------------------------------------------------------------------------------------------
@Author 	 - Jagan Periyakaruppan
@Date 		 - 01/02/2018
@Description - Batch class updates Account record with formula field updates.
@Condition 1 - For Medicare customers update the Subscription flag
--------------------------------------------------------------------------------------------------*/
global class BclsUpdateAccntInfoOnForumlaUpdate implements Database.Batchable<sObject>
{
	//Map holds the Accounts that are to be updated
	Map<Id, Account> accountsUpdateMap = new Map<Id, Account>();
	
    //Start Method
	global Database.QueryLocator start(Database.BatchableContext BC)
	{
		String query = 'SELECT Id, Age__c, Age_Number__c,  Has_Not_Made_Recent_Order__c, Has_Not_Made_Recent_Order_MKT__c,  MDCR_FFS_Subscription_Calculation__c, MDCR_FFS_Subscription__c, MDCR_FFS_Subscription_Update_Check__c FROM Account WHERE Update_From_Formula_Field_Value__c = true';
		return Database.getQueryLocator(query);
	}
	
    //Execute Method
	global void execute(Database.BatchableContext BC, List<Account> scope)
	{
		for(Account accnt : scope)
		{
			system.debug('MDCR_FFS_Subscription_Update_Check__c value is ' + accnt.MDCR_FFS_Subscription_Update_Check__c);
            //Check if this is a medicare account and subscription field to be updated
			if(accnt.MDCR_FFS_Subscription_Update_Check__c)
            {
				accnt.MDCR_FFS_Subscription__c = 'Not Active';
                accnt.Last_MDCR_Reorder_Follow_up_Type__c = 'End Subscription';
                accnt.MDCR_Subscription_Override__c = false ;
                
			}
            if(accnt.Age_Number__c <>  accnt.Age__c)
            {
                accnt.Age_Number__c = accnt.Age__c;
            }
            if(accnt.Has_Not_Made_Recent_Order_MKT__c <> accnt.Has_Not_Made_Recent_Order__c)
            {
                accnt.Has_Not_Made_Recent_Order_MKT__c = accnt.Has_Not_Made_Recent_Order__c;
            }
            accountsUpdateMap.put(accnt.Id, accnt);
		}
		if(!accountsUpdateMap.isEmpty())
		{
			try{
				update accountsUpdateMap.values();
			}
			catch(DMLException de)
			{
                System.debug('getNumDml=' + de.getNumDml());
                for(Integer i=0;i<de.getNumDml();i++) {
                    System.debug('getDmlFieldNames=' + de.getDmlFieldNames(i));
                    System.debug('getDmlMessage=' + de.getDmlMessage(i));
                }
			}
		}
	}
    
    //Finish Method
    global void finish(Database.BatchableContext BC)
    {
      
    }
}