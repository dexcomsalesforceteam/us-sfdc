/****************************************************************************************************************
@Author        : Jagan Periyakaruppan
@Date Created    : 4/15/2019
@Description    : Class used in to process all Address related logic for Account upon inserting and updating
****************************************************************************************************************/
public class ClsAccountAddressTriggerHandler
{
    //Get Non-US Record Type Ids
	static Set<Id> nonUSRecordTypeIds = getNonUSRecordIds();
		
	//Method populates Account Shipping Address from Account Billing Addres if the shipping address information is blank
    public static void ProcessAccountShippingAddressOnAccountInsert(List<Account> Accounts){
        Id consumerRecordTypeId = ClsApexUtility.getRecordTypeIdByDeveloperName(ClsApexConstants.ACCOUNT_API_NAME, ClsApexConstants.ACCOUNT_REC_TYPE_CONSUMERS_LABEL);
        //Loop through all accounts and find if Shipping information is null and then populate the shipping  address values from billing address
        for(Account accnt : Accounts){
            //Process only for US Accounts
            if(!nonUSRecordTypeIds.contains(accnt.RecordtypeId)){//runs for prescriber, payor, medical facility and consumer accounts
                if(String.isNotBlank(accnt.BillingStreet) && (String.isBlank(accnt.BillingCountry) || (String.isNotBlank(accnt.BillingCountry) && !ClsApexConstants.US.equalsIgnoreCase(accnt.BillingCountry)))
                  && (accnt.RecordtypeId == consumerRecordTypeId))
                    accnt.BillingCountry = ClsApexConstants.US; //changed for INC024931
                
                if((!String.isBlank(accnt.BillingStreet) && String.isBlank(accnt.ShippingStreet) && String.isBlank(accnt.ShippingCity) && String.isBlank(accnt.ShippingState) && String.isBlank(accnt.ShippingPostalCode)) || accnt.Copy_Billing_to_Shipping__c){
                    system.debug('----Entering the process to update the Shipping address as it was blank');
                    accnt.ShippingStreet = accnt.BillingStreet;
                    accnt.ShippingCity = accnt.BillingCity;
                    accnt.ShippingState = accnt.BillingState;
                    accnt.ShippingPostalCode = accnt.BillingPostalCode;
                    accnt.ShippingCountry = accnt.BillingCountry; //changed for INC024931
                }else if(!String.isBlank(accnt.BillingStreet) && String.isNotBlank(accnt.ShippingStreet) && (String.isBlank(accnt.BillingCountry) || (String.isNotBlank(accnt.ShippingCountry) && !ClsApexConstants.US.equalsIgnoreCase(accnt.ShippingCountry)))
                        && (accnt.RecordtypeId == consumerRecordTypeId)){
                    accnt.ShippingCountry = ClsApexConstants.US; //changed for INC024931
                }
            }
        }
    }
	//Method will retrun the set of non-us recordtype ids
	public static Set<Id> getNonUSRecordIds()
	{
		Set<Id> recordTypeIds = new Set<Id>();
		for(Recordtype rt : [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND (Name LIKE 'CA%' OR NAME LIKE 'EMEA%' OR NAME LIKE 'GB%' OR NAME LIKE 'IE%')])
			recordTypeIds.add(rt.Id);
		return recordTypeIds;
	}	
    
    //Method will create BILL_TO and SHIP_TO address for account. This will be used when an account is inserted
    public static void ProcessAccountRelatedAddressOnAccountInsert(List<Account> Accounts)
    {
        List<Address__c> addressToCreateList = new List<Address__c>();//List holds the address records to be created
        
        //Loop through all accounts and create a primary BILL_TO and primary SHIP_TO
        for(Account accnt : Accounts)
        {
			//Process only for US Accounts
			if(!nonUSRecordTypeIds.contains(accnt.RecordtypeId))
			{
				if(!String.isBlank(accnt.BillingStreet))
				{
					Address__c billToAddr = ClsAccountAddressTriggerHandlerHelper.CreateAddress(accnt.Id, accnt.BillingStreet, accnt.BillingCity, accnt.BillingState, accnt.BillingPostalCode, accnt.BillingCountry, 'BILL_TO');
					addressToCreateList.add(billToAddr);
				}
				if(!String.isBlank(accnt.ShippingStreet))
				{
					Address__c shipToAddr = ClsAccountAddressTriggerHandlerHelper.CreateAddress(accnt.Id, accnt.ShippingStreet, accnt.ShippingCity, accnt.ShippingState, accnt.ShippingPostalCode, accnt.ShippingCountry, 'SHIP_TO');
					addressToCreateList.add(shipToAddr);
				}
			}
            
        }
        //Insert Address Records
        if(!addressToCreateList.isEmpty())
        {
            system.debug('----Entering the process to create BILL_TO and SHIP_TO Address tied to this account');
            ClsAccountAddressTriggerStaticClass.addressChangeInvokedFromAddress = false; //Set the static variable, so the code in Address object will not fire.
			List<Id> addressIdsToProcessForCountyNames = new List<Id>();//List of Address Ids used to retrieve the county names
			List<Account> accountsToBeUpdatedWithPrimaryShipToList = new List<Account>(); //List used to update the Accounts with Primary Ship To Id
            try{
				insert addressToCreateList;
				for(Address__c addr : addressToCreateList)
				{
					Account accntToBeUpdatedWithPrimaryShipTo = new Account(Id = addr.Account__c);
					if(addr.Address_Type__c == 'SHIP_TO')
					{
						accntToBeUpdatedWithPrimaryShipTo.Primary_Ship_To_Address__c = addr.Id;
						accountsToBeUpdatedWithPrimaryShipToList.add(accntToBeUpdatedWithPrimaryShipTo);
					}
				}
			}
            catch (DmlException de) {
                Integer numErrors = de.getNumDml();
                System.debug('getNumDml=' + numErrors);
                for(Integer i=0;i<numErrors;i++) {
                    System.debug('getDmlFieldNames=' + de.getDmlFieldNames(i));
                    System.debug('getDmlMessage=' + de.getDmlMessage(i));
                }
            }
			if(!accountsToBeUpdatedWithPrimaryShipToList.isEmpty())
				ClsAccountAddressTriggerHandlerHelper.UpdateAccountWithPrimaryShipToDetails(accountsToBeUpdatedWithPrimaryShipToList);
        }
    }
	//Set the country to US as Google populates the country as United States
	//changing method definition to check old and new values
    public static void processAccountAddressBeforeUpdate(List<Account> Accounts, Map<Id, Account>oldAccountMap){
        Id consumerRecordTypeId = ClsApexUtility.getRecordTypeIdByDeveloperName(ClsApexConstants.ACCOUNT_API_NAME, ClsApexConstants.ACCOUNT_REC_TYPE_CONSUMERS_LABEL);
		for(Account accnt : Accounts){
			//Process only for US Accounts
			if(!nonUSRecordTypeIds.contains(accnt.RecordtypeId)){
                //updated for INC024931
                if(oldAccountMap.containsKey(accnt.Id) && oldAccountMap.get(accnt.Id).BillingStreet != accnt.BillingStreet &&
                   String.isNotBlank(accnt.BillingStreet) && (String.isBlank(accnt.BillingCountry) || (String.isNotBlank(accnt.BillingCountry) && !ClsApexConstants.US.equalsIgnoreCase(accnt.BillingCountry)))
                  && (accnt.RecordtypeId == consumerRecordTypeId)){
                    accnt.BillingCountry = ClsApexConstants.US;
                }
                if(accnt.Copy_Billing_to_Shipping__c){
                    system.debug('----Entering the process to update the Shipping address as it was blank');
                    accnt.ShippingStreet = accnt.BillingStreet;
                    accnt.ShippingCity = accnt.BillingCity;
                    accnt.ShippingState = accnt.BillingState;
                    accnt.ShippingPostalCode = accnt.BillingPostalCode;
                    accnt.ShippingCountry = accnt.BillingCountry;
                }
                //updated for INC024931
                if(oldAccountMap.containsKey(accnt.Id) && oldAccountMap.get(accnt.Id).ShippingStreet != accnt.ShippingStreet &&
                   String.isNotBlank(accnt.ShippingStreet) && !ClsApexConstants.US.equalsIgnoreCase(accnt.ShippingCountry)
                  && (accnt.RecordtypeId == consumerRecordTypeId)){
                    accnt.ShippingCountry = ClsApexConstants.US;
                }
                accnt.Copy_Billing_to_Shipping__c = ClsApexConstants.BOOLEAN_FALSE;
            }
		}
		
	}
    //Method will be called from account trigger after the record is updated. This will process the records in address object for the account and sets correct primary BILL_TO and SHIP_TO
    public static void ProcessAccountRelatedAddressOnAccountUpdate(Map<Id, Account> newTriggerAccnts, Map<Id, Account> oldTriggerAccnts)
    {
        //Invoke the below block of code only when the address change happened from the Account rather than the Address object
        if(ClsAccountAddressTriggerStaticClass.addressChangeInvokedFromAccount)
        {
            List<Account> billToAccountsToProcess = new List<Account>();//Holds list of accounts that are to be processed for BILL_TO address change
            List<Account> shipToAccountsToProcess = new List<Account>();//Holds list of accounts that are to be processed for SHIP_TO address change
			List<Id> accountsSyncdToOracle = new List<Id>();//Holds list of accounts that are sync'd to Oracle
			
            //Check if any account's bill to address got updated and then proceeed
            for(Account newAccnt : newTriggerAccnts.values())
            {
				//Process only for US Accounts
				if(!nonUSRecordTypeIds.contains(newAccnt.RecordtypeId))
				{
					Account oldAccnt = oldTriggerAccnts.get(newAccnt.Id);
					if(oldAccnt.BillingStreet != newAccnt.BillingStreet || oldAccnt.BillingCity != newAccnt.BillingCity || oldAccnt.BillingState != newAccnt.BillingState || oldAccnt.BillingPostalCode != newAccnt.BillingPostalCode || oldAccnt.BillingCountry != newAccnt.BillingCountry)
					{
						system.debug('----Found mismatch in Billing address information');
						billToAccountsToProcess.add(newAccnt);
						
					}
					if(oldAccnt.ShippingStreet != newAccnt.ShippingStreet || oldAccnt.ShippingCity != newAccnt.ShippingCity || oldAccnt.ShippingState != newAccnt.ShippingState || oldAccnt.ShippingPostalCode != newAccnt.ShippingPostalCode || oldAccnt.ShippingCountry != newAccnt.ShippingCountry)
					{
						system.debug('----Found mismatch in Shipping address information');
						shipToAccountsToProcess.add(newAccnt);
					}
					if(oldAccnt.AccountNumber == null && newAccnt.AccountNumber != null)
					{
						accountsSyncdToOracle.add(newAccnt.Id);
					}
				}
			}
            
            //If there are records to be processed for BILL_TO change then go in to the loop
            if(!billToAccountsToProcess.isEmpty())
            {
                system.debug('----Entering the process to handle BILL_TO Address changes');
                ClsAccountAddressTriggerStaticClass.addressChangeInvokedFromAddress = false; //Set the static variable, so the code in Address object will not fire.
                ClsAccountAddressTriggerHandlerHelper.ProcessAddressRecords('BILL_TO', billToAccountsToProcess);
            }
            //If there are records to be processed for SHIP_TO change then go in to the loop
            if(!shipToAccountsToProcess.isEmpty())
            {
                system.debug('----Entering the process to handle SHIP_TO Address changes');
                ClsAccountAddressTriggerStaticClass.addressChangeInvokedFromAddress = false;//Set the static variable, so the code in Address object will not fire.
                ClsAccountAddressTriggerHandlerHelper.ProcessAddressRecords('SHIP_TO', shipToAccountsToProcess);
            }
			//If there are accounts recently sync'd then send the associated address to Oracle
            if(!accountsSyncdToOracle.isEmpty())
            {
                ClsAccountAddressTriggerStaticClass.addressChangeInvokedFromAddress = false;//Set the static variable, so the code in Address object will not fire.
                ClsAccountAddressTriggerHandlerHelper.SendAddressToOracle(accountsSyncdToOracle);
            }
        }	
    }  
}