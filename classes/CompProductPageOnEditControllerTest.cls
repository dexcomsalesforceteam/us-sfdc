/********************************************************************************
@author Abhishek Parghi
@date 10/08/2015
@description: Test class for Comp Product Requests

@author Jagan Periyakaruppan
@date 02/24/2017
@description: Added line 35, 6, 37 to populate the Fedex notes
*******************************************************************************/
@istest
public class CompProductPageOnEditControllerTest {
   Static testMethod void TestCompProduct() 
   {
       //create prescriber
        Account testPrescriber = new Account();      
        testPrescriber.RecordTypeId = ClsApexUtility.getRecordTypeIdByDeveloperName('Account', 'Prescriber');
        testPrescriber.FirstName = 'TestPrescriber_FName';
        testPrescriber.LastName = 'TestPrescriber_LName';
        testPrescriber.PersonEmail = 'Fname.LName@gmail.com';
        Insert testPrescriber;
        //create consumer
       Id personRecordTypeID=[select Id from RecordType where (Name='Consumers') and (SobjectType='Account')].Id;
       Account acc5 = new Account();
       acc5.RecordTypeId = personRecordTypeID;
       acc5.FirstName = 'APTestfirstname';
       acc5.LastName = 'APTestlastname';
       acc5.PersonEmail = 'APTestAP@gmail.com';
       acc5.Phone = '12345678900';
       acc5.BillingState = 'CA';
       acc5.BillingCity = 'APTest City';
       acc5.BillingStreet = 'APTest Street';
       acc5.BillingPostalCode =  '95112';
       acc5.BillingCountry = 'United States';
       acc5.ShippingState = 'CA';
       acc5.ShippingCity = 'APTest City';
       acc5.ShippingStreet = 'APTest Street';
       acc5.ShippingPostalCode =  '95112';
       acc5.ShippingCountry = 'United States';
       acc5.AccountNumber = '1234';
       acc5.Party_ID__c = '4444';
       Acc5.Prescribers__c = testPrescriber.Id;
       Acc5.CMN_or_Rx_Expiration_Date__c = Date.newInstance(2099, 1, 1);
       insert acc5;
       
       List<Address__c> ShipToAddressList = TestDataBuilder.getAddressList(acc5.id,true,'SHIP_TO',1);
        insert ShipToAddressList;
        List<Address__c> BillToAddressList = TestDataBuilder.getAddressList(acc5.id,true,'BILL_TO',1);
        insert BillToAddressList;
        id shipToAddressId;
        for(Address__c shipToAddr : ShipToAddressList ){
            shipToAddressId = shipToAddr.id;
        }
       Test.startTest();
       Comp_Product_Request__c Comp = new Comp_Product_Request__c(Approval_Status__c ='Rejected',
                                                                   Customer_Account_No__c = acc5.id,
                                                                   G5_Receiver__c = '1',
                                                                   G4_Receiver__c ='1',
                                                                   Error_Source__c = 'Fedex',
                                                                   Fedex_Reasons__c = 'other',
                                                                   Fedex_Notes__c = 'Failed Shipment',
                                                                    Shipping_Address__c = shipToAddressId,
                                                                  Shipping_Method__c = '000001_FEDEX_L_GND',
                                                                   Repmaking_Request__c = UserInfo.getUserId(),
                                                                   Estimated_Amount__c = 4042);
       insert comp; 
       Comp_Product_Request__c C1 = [Select id, Approval_Status__c,Estimated_Amount__c from Comp_Product_Request__c where Estimated_Amount__c = 4042 ];  
       c1.Approval_Status__c ='Approved';
       Update C1;
       
       // start the test execution context
        

        // set the test's page to your VF page (or pass in a PageReference)
        Test.setCurrentPage(Page.CompProductPageOnEdit);
        Test.setCurrentPage(Page.CompProductPageOnView); 
        // call the constructor
        CompProductPageOnEditController controller = new CompProductPageOnEditController(new ApexPages.StandardController(Comp));
        CompProductPageOnViewController controller2 = new CompProductPageOnViewController(new ApexPages.StandardController(Comp));
      //  controller.save();

        // stop the test
        Test.stopTest(); 
        controller.changePickList();
        controller2.getHistories();
        String theDate;
        String who;
        String action;
        CompProductPageOnViewController.cHistories cp = new CompProductPageOnViewController.cHistories();
   } 
}