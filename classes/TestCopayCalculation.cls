@isTest
public class TestCopayCalculation
{
    @isTest static void TestCopayInvocationFromAccount()
    {
        //Get Consumers record type 
        String consumerRecordTypeId = ClsApexUtility.getRecordTypeIdByDeveloperName('Account','Consumers');
        //Get Payor record type 
        String payorRecordTypeId = ClsApexUtility.getRecordTypeIdByDeveloperName('Account','Payor');
        
        //Create Pricebook
        Map<String, Id> customPricebookMap = ClsTestDataFactory.createCustomPricebook(new List <String>{'Aetna Health Plan'});
        String customPricebookId = customPricebookMap.get('Aetna Health Plan');
        
        //Create a consumer and Payor Account 
        List<Account> accntsToBeInserted = new List<Account>();
        Account mdcrAccnt = new Account();
        mdcrAccnt.FirstName = 'Med Adv FirstName';
        mdcrAccnt.LastName = 'Med Adv LastName';
        mdcrAccnt.RecordtypeId = consumerRecordTypeId;
        mdcrAccnt.Customer_Type__c = 'Commercial';
        mdcrAccnt.DOB__c = Date.newInstance(2016, 12, 9);
        accntsToBeInserted.add(mdcrAccnt);
        
        //Create a Payor record along with the Med Adv details
        Account mdcrPayor = new Account();
        mdcrPayor.RecordTypeId = payorRecordTypeId;
        mdcrPayor.Name = 'Med Adv Test Payor';
        mdcrPayor.Payor_Code__c = 'K';
        mdcrPayor.Default_Price_Book_K__c = customPricebookId;
        mdcrPayor.Claim_Address__c = '1 Main Street, San Diego, CA - 92127';
        mdcrPayor.DOB__c = Date.newInstance(2016, 12, 9);
        accntsToBeInserted.add(mdcrPayor);
        insert accntsToBeInserted;
        
        //Create a new Opportunity
        Opportunity oppty = new Opportunity();
        oppty.Name = 'Med Adv Test Oppty';
        oppty.AccountId = mdcrAccnt.Id;
        oppty.Closedate = Date.today().addDays(30);
        oppty.StageName = '1. New Opportunity';
        oppty.RecordtypeId = ClsApexUtility.getRecordTypeIdByDeveloperName('Opportunity','US_Opportunity');        
        insert oppty;
        
        //Create Benefit Record
        Benefits__c benefit = new Benefits__c();
        benefit.Benefit_Hierarchy__c = 'Primary';
        benefit.Account__c = accntsToBeInserted[0].Id;
        benefit.Payor__c = accntsToBeInserted[1].Id;
        benefit.Coverage__c = 80;
        benefit.CO_PAY__c = 20;
        benefit.Copay_Line_Item__c = 20;
        benefit.INDIVIDUAL_DEDUCTIBLE__c = 500;
        benefit.INDIVIDUAL_MET__c = 200;
        benefit.INDIVIDUAL_OOP_MAX__c = 1500;
        benefit.INDIVIDUAL_OOP_MET__c = 1000;
        benefit.FAMILY_DEDUCT__c = 1000;
        benefit.Family_Met__c = 850;
        benefit.FAMILY_OOP_MAX__c = 3000;
        benefit.FAMILY_OOP_MET__c = 2500;
        benefit.MEMBER_ID__c = '235';
        insert benefit;

        
        //Create New Sensor Product
        Map<String, Id> newProduct = ClsTestDataFactory.createProducts(new List<String> {'STK-MC-001', 'STK-SD-001', 'BUN-GF-003', 'STS-GL-041', 'MT-MC-SUB'});
        //Create Pricebook EntryPair
        Map<Id, Decimal> newProductIdToPriceMap = new Map<Id, Decimal>();
        for(String productName : newProduct.keySet())
            newProductIdToPriceMap.put(newProduct.get(productName), 125.00);
        //Add new pricebookentry item
        Map<Id, Id> newProductIdToPbeId = ClsTestDataFactory.createCustomPricebookEntries(newProductIdToPriceMap, customPricebookId);
        
        Test.startTest();
        //Test converting the Benefit record to Med Advantage 
        benefit.Plan_Type__c = 'MED ADV';
        benefit.Is_Update_In_Progress__c = ClsApexConstants.BOOLEAN_FALSE;
        update benefit;
        benefit.Plan_Type__c = 'Commercial';
        benefit.Is_Update_In_Progress__c = ClsApexConstants.BOOLEAN_FALSE;
        update benefit;
        mdcrAccnt.Default_Price_book__c = customPricebookId;
        update mdcrAccnt;
        Test.stopTest();
    }
}