public class TerritoryAlignmentTriggerHandler{
/***********************************************************************************************
@Author        : Jagan Periyakaruppan
@Date Modified    : 10/18/2017
@Description    : This method will be process the territory alignment updates on Accounts
@Author        : Jagan Periyakaruppan
@Date Modified    : 11/20/2017
@Description    : Altered the logic to invoke Batch apex as we have to update several Account records in one transaction
@Author        : Andrew Akre
@Date Modified    : 9/10/2018
@Description    : Added and removed certain fields grabbed from the Territory Alignment object
**************************************************************************************************/
    public static void afterUpdate(List<Territory_Alignment__c> terrAlignRecords, Map<Id, Territory_Alignment__c> oldMap){
        
        Set<Id> terrAlignIds = new Set<Id>(); //Set holds list of Territory alignment Ids to be processed
      
        for(Territory_Alignment__c terrAlign : terrAlignRecords){
            if(terrAlign.Fax__c != oldMap.get(terrAlign.Id).Fax__c || 
               terrAlign.AS_Email__c != oldMap.get(terrAlign.Id).AS_Email__c || 
               terrAlign.AS_Direct_Line__c != oldMap.get(terrAlign.Id).AS_Direct_Line__c ||
               terrAlign.PCS__c != oldMap.get(terrAlign.Id).PCS__c ||
               terrAlign.RSS__c != oldMap.get(terrAlign.Id).RSS__c ||
               terrAlign.RSS_RingDNA_Number__c != oldMap.get(terrAlign.Id).RSS_RingDNA_Number__c ||
               terrAlign.RSS_Email__c != oldMap.get(terrAlign.Id).RSS_Email__c ||
               terrAlign.Medicare_RSS__c != oldMap.get(terrAlign.Id).Medicare_RSS__c ||
               terrAlign.Medicare_RSS_RingDNA_Number__c != oldMap.get(terrAlign.Id).Medicare_RSS_RingDNA_Number__c ||
               terrAlign.Supervisor__c != oldMap.get(terrAlign.Id).Supervisor__c)
            {
                terrAlignIds.add(terrAlign.Id);
            }   
        }
        //Invoke account updates when there are any changes to Territory alignment values we are interested in
        if(!terrAlignIds.isEmpty())
        {
            //Invoke Batch apex 
            database.executebatch(new BClsProcessTerritoryAlignmentChanges(terrAlignIds),50);
        }
        
    }
}