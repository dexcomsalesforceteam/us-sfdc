/***
    *@Author        : Priyanka Kajawe
    *@Date Created  : 30-July-2018
    *@Description   : generic class to create orders.
    ***/
    Public Class ClsCreateOrder{
        /***
    *@Description : Method to create Order and Order Items
    *@Param  : Wrapper
    *@Return   : Map<Id, Order>
    ***/
        public static Map<Id, Order> createOrder(List<ClsOrderWrapper> orderWrapperList){
            //Initilize collections
            Map<Id, Order> createOrdersMap = new Map<Id, Order>(); //Map to store orders info
            Map<String,Map<String,Integer>> productMap = new Map<String,Map<String,Integer>>(); //Map between the unique identifier and Product Name to Qty map
            Map<String,PricebookEntry> priceBookEntryProductMap = new Map<String,PricebookEntry>();//Map holds the ProductName+Pricebook Id to PricebookentryId
            Map<Id, Order> newOrdersMap = new Map<Id, Order>(); //Map will hold the newly created Order record for each unique identifier. This map will be returned back
            Set<Id> pricebookids = new Set<Id>();//Set holds all the Pricebook Ids included in the process
            Set<String> productList = new Set<String>();//Set holds all Product names included in the process
            List<OrderItem> orderLinesToInsertList = new List<OrderItem>();//List to store the Order Item entries for insert
            List<Order> insertOrderList = new List<Order>();
            Set<Id> setOrdersForCopay= new set<Id>(); // Orders that will need copay calculation.
            //For each order wrapper prepare the Order records that are to be created
            for(ClsOrderWrapper orderWrapper :orderWrapperList){
                Order thisOrder = new order();
                thisOrder.AccountId = orderWrapper.accountId;
                thisOrder.Type = orderWrapper.type;
                thisOrder.RecordTypeId = orderWrapper.recordTypeId;
                thisOrder.Status = orderWrapper.status;
                thisOrder.Sub_Type__c = orderWrapper.subType;
                thisOrder.EffectiveDate = orderWrapper.effectiveDate;
                thisOrder.Scheduled_Ship_Date__c = orderWrapper.scheduledShipDate;
                thisOrder.Shipping_Address__c = orderWrapper.shippingAddress;
                thisOrder.Shipping_Method__c = orderWrapper.shippingMethod;
                thisOrder.Price_Book__c = orderWrapper.priceBook;
                thisOrder.Reason_for_Approval__c = orderWrapper.reasonForApproval;
                thisOrder.CMN_Verification__c = orderWrapper.cmnVerification;
                thisOrder.AOB_Verification__c = orderWrapper.aobVerification;
                thisOrder.Send_Error_Notification_To__c = orderWrapper.sendErrorNotificationTo;
                thisOrder.Signature_Required__c = orderWrapper.signatureRequired;
                thisOrder.Unique_Identifier__c = orderWrapper.uniqueIdentifier;
                thisOrder.Waive_Shipping_Charges__c=orderWrapper.WaiveShippingCharges;
                //implemented for populating SSIP schedule before and stop creation of orphan orders
                if(ClsApexConstants.ORDER_SUB_TYPE_SSIP_ORDER.equalsIgnoreCase(thisOrder.Sub_Type__c)){
                    thisOrder.SSIP_Schedule__c = orderWrapper.uniqueIdentifier;
                }
                insertOrderList.add(thisOrder);
                //Collect product names and price books involved in the process
                productMap.put(orderWrapper.uniqueIdentifier,orderWrapper.productQuantityMap);
                productList.addAll(orderWrapper.productQuantityMap.keySet());
                
                pricebookids.add(orderWrapper.priceBook);
                System.debug('orderWrapper =' + orderWrapper);
            }
            
            //Process Order and OrderItem inserts
            if(!insertOrderList.isEmpty()){
                //------------------Insert Orders Start---------------------------
                System.debug('insertOrderList =' + insertOrderList);
                Database.SaveResult[] SR = Database.insert(insertOrderList, false);
                
                //------------------Insert Orders Fails---------------------------
                Set<Id> insertedUniIdentifiers = new Set<Id>(); //Set Of SSIP Schedules (failed Orders)
                
                System.debug('SR =' + SR);
                for(Integer i=0;i<SR.size();i++){
                    if(!SR[i].isSuccess()){
                      system.debug(SR[i].getErrors());
                      system.debug(insertOrderList[i]); 
                      
                      Id recordId = insertOrderList[i].Unique_Identifier__c; //parent Record Id                     
                      String errorMsg = SR[i].getErrors().get(0).getFields() + ':' + SR[i].getErrors().get(0).getMessage();
                        
                        if(errorMsg.Length() >= ClsApexConstants.TWO_HUNDRED_INTEGER)
                            errorMsg = errorMsg.substring(ClsApexConstants.ZERO_INTEGER,ClsApexConstants.TWO_HUNDRED_INTEGER);  
                        //Write the error log
                        new ClsApexDebugLog().createLog(
                            new ClsApexDebugLog.Error(
                                'ClsCreateOrder',
                                'createOrder',
                                recordId,
                                errorMsg
                            ));
                  
                        
                      system.debug('error while inserting order is:::::'+errorMsg);
                      
                    }else{
                        System.debug('success =' + SR[i].getId());
                        System.debug('success list ID=' + insertOrderList[i].Unique_Identifier__c);
                        insertedUniIdentifiers.add(insertOrderList[i].Unique_Identifier__c);
                    }   
                }
                
                if(!insertedUniIdentifiers.isEmpty()){
                    for(Order thisOrder : insertOrderList){
                        if(insertedUniIdentifiers.contains(thisOrder.Unique_Identifier__c))
                            createOrdersMap.put(thisOrder.Unique_Identifier__c, thisOrder);
                    }
                }  
                //------------------Insert Orders End---------------------------
                
                //------------------Insert Order Item Start---------------------------
                //Get the price book entries for each Product involved in the process       
                if(!productMap.isEmpty() || !pricebookids.isEmpty()){
                    Set<PricebookEntry> priceBookEntrys = new Set<PricebookEntry>([SELECT ID, Pricebook2Id, UnitPrice, Pricebook2.Oracle_Id__c, Product2.Name from PricebookEntry where Product2.Name IN : productList and Pricebook2.Id IN :pricebookids]);
                    System.debug('priceBookEntrys=' + priceBookEntrys);
                    
                    //Populate the map priceBookEntryProductMap (ProductName+Pricebook Id to PricebookentryId)            
                    for(PricebookEntry thispbEntry :priceBookEntrys){
                        priceBookEntryProductMap.put(thispbEntry.Product2.Name+thispbEntry.Pricebook2Id, thispbEntry);
                    }  
                }
                System.debug('priceBookEntryProductMap =' + priceBookEntryProductMap);
                
                //Loop through the inserted Orders and then add the corresponding Order Items
                for(Order createdOrder : createOrdersMap.Values())
                {
                    for(String  productRecord : productMap.get(createdOrder.Unique_Identifier__c).keySet()){
                        if((priceBookEntryProductMap.containsKey(productRecord+createdOrder.Price_Book__c) &&
                            priceBookEntryProductMap.get(productRecord+createdOrder.Price_Book__c) !=null) || Test.isRunningTest()){
                                OrderItem orderLine = new OrderItem();
                                orderLine.OrderId = createdOrder.Id;
                                setOrdersForCopay.Add(createdOrder.Id);
                                if (Test.isRunningTest()) {
                                    orderLine.PricebookEntryId = Test.getStandardPricebookId();
                                    orderLine.UnitPrice = 0;
                                } 
                                else{
                                    orderLine.PricebookEntryId = priceBookEntryProductMap.get(productRecord+createdOrder.Price_Book__c).id;
                                    orderLine.UnitPrice = priceBookEntryProductMap.get(productRecord+createdOrder.Price_Book__c).UnitPrice;
                                }                    
                                orderLine.Quantity = productMap.get(createdOrder.Unique_Identifier__c).get(productRecord);
                                orderLinesToInsertList.add(orderLine);
                            }
                    }
                    newOrdersMap.put(createdOrder.Unique_Identifier__c, createdOrder);
                }
                System.debug('orderLinesToInsertList =' + orderLinesToInsertList);
                //Insert Out of Box Order Lines
                if(!orderLinesToInsertList.isEmpty()){
                    Database.SaveResult[] saveResultList = Database.insert(orderLinesToInsertList,false);  
                    for(Database.SaveResult saveResultObj : saveResultList){
                        if(!saveResultObj.isSuccess()){
                            String errorMsg = saveResultObj.getErrors().get(0).getFields() + ':' + saveResultObj.getErrors().get(0).getMessage();
                            system.debug('####errorMsg - ' + errorMsg);
                            if(errorMsg.Length() >= ClsApexConstants.TWO_HUNDRED_INTEGER)
                                errorMsg = errorMsg.substring(ClsApexConstants.ZERO_INTEGER,ClsApexConstants.TWO_HUNDRED_INTEGER);
                            //Write the error log
                            new ClsApexDebugLog().createLog(
                                new ClsApexDebugLog.Error(
                                    'ClsCreateOrder',
                                    'insertOliRoutineOnOrder',
                                    ClsApexConstants.EMPTY_STRING,
                                    errorMsg
                                )); 
                        }
                    }
                }
                //------------------Insert Order Item End---------------------------
                
                /* ----	Copay calculations ----. Force calculate copay just before order is activated.*/
                if(!setOrdersForCopay.isEmpty()){
                    CalculateCoPay.CalcCoPayByOrder(new List<ID>(setOrdersForCopay));
                }
               
                //Activate the created Orders
                ID jobID = System.enqueueJob(new ClsActivateOrderQueueable(createOrdersMap.Values()));
                System.debug('*** Activate Orders Queueable invoked + Job Id :: ' + jobID );                
            }
            return newOrdersMap;
            
        }
    /***
    *@Description : Class to activate an order
    *@Param  : List of Orders
    *@Return   : void
    ***/
    public class ClsActivateOrderQueueable implements Queueable {
        Set<Id> orderIdSet = new Set<Id>();
        List<Order> ordersActivateList = new List<Order>();
        
        public ClsActivateOrderQueueable (List<Order> ordersList) {
            for(Order aOrder : ordersList){
                orderIdSet.add(aOrder.id);
            }
        }           
            
        
        public void execute(QueueableContext qc) {
            for(Order thisOrder : [Select Id, Total_Quantity_On_Order__c, status from Order where Id IN :orderIdSet]){
                System.debug('***Order ID:: ' + thisOrder.Id + ' :: Total Quantity :: ' + thisOrder.Total_Quantity_On_Order__c);
                if(thisOrder.Total_Quantity_On_Order__c > 0){
                    ordersActivateList.add(thisOrder);
                }
            }
            System.debug('***List Order :: ' + ordersActivateList);
                 
            if(!ordersActivateList.isEmpty()){
                for(Order orderToUpdate : ordersActivateList){
                    orderToUpdate.status = 'Activated';     
                }
                            
                //UtilityClass.runAfterTrigger = true; //to avoid recursive check from Order Trigger
                List<Database.SaveResult> updateResultList = Database.update(ordersActivateList, false);
            
                for(Database.SaveResult updateResultObj : updateResultList){
                    if(!updateResultObj.isSuccess()){
                        String errorMsg = updateResultObj.getErrors().get(0).getFields() + ':' + updateResultObj.getErrors().get(0).getMessage();
                        system.debug('####Order Update to OH errorMsg - ' + errorMsg);
                        if(errorMsg.Length() >= ClsApexConstants.TWO_HUNDRED_INTEGER)
                            errorMsg = errorMsg.substring(ClsApexConstants.ZERO_INTEGER,ClsApexConstants.TWO_HUNDRED_INTEGER);
                        //Write the error log
                        new ClsApexDebugLog().createLog(
                            new ClsApexDebugLog.Error(
                                'ClsCreateOrder',
                                'OrderActivationRoutine',
                                updateResultObj.getId(),
                                errorMsg
                            )); 
                    }
                }
            }
        }
    }
}