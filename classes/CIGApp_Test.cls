/* V1 */
@isTest
public class CIGApp_Test{
    @testSetup static void setup() {
    
        Organization__c org = new Organization__c(Name='OrgDataCon_Test1', Competitor_Stage__c='Established', Organization_Type__c='Distributor');
        insert org;
        Organization_Data__c od= new Organization_Data__c(Organization__c=org.Id, Name='OrgDataCon_Test1');
        insert od;
        Organization_Product__c op= new Organization_Product__c (Organization__c=org.Id, Name='OrgDataCon_Test1');
        insert op;
    }
    
    @isTest static void TestControllers() {
       Organization__c org= [SELECT Id FROM Organization__c WHERE Name='OrgDataCon_Test1' LIMIT 1];
       Organization_Data__c od= [SELECT Id, Organization__c, Product_Note__c FROM Organization_Data__c WHERE Organization__c= :org.Id LIMIT 1];
       Organization_Product__c op=[SELECT Id, Name, Organization__c  FROM Organization_Product__c WHERE Organization__c= :org.Id LIMIT 1];
       List<Id> lstFiles= new List<Id>();
       
       ContentVersion cvIns= new ContentVersion(
            Title = 'OrgDataCon_Test',
            PathOnClient = 'OrgDataCon_Test.jpg',
            VersionData = Blob.valueOf('Test Content Data OrgDataCon_Test'),
            IsMajorVersion = true
       );
       lstFiles.Add(org.Id);
      // insert cvIns;
       //ContentDocument cd= [SELECT id FROM ContentDocument Limit 1];       
      // lstFiles.Add(cd.Id);
       
       /*ContentDocumentLink cdl1=new ContentDocumentLink();        
        cdl1.ShareType= 'V';
        cdl1.LinkedEntityId = org.Id; 
        cdl1.ContentDocumentId=cd.Id;
        cdl1.Visibility = 'AllUsers'; */
        //insert cdl1;
        
       od.RecordTypeId=Schema.SObjectType.Organization_Data__c.getRecordTypeInfosByDeveloperName().get('Clinical').getRecordTypeId();
       od.Trial_URL__c='url1'; od.Product_Note__c ='PN2';
       List<Organization_Data__c> lstOD= new List<Organization_Data__c>(); lstOD.Add(od);
       System.Assert(OrgDataCon.updateClinicalData(lstOD)== true);
       List<Id> files= new List<Id>(); files.Add(org.Id);
       
       System.Assert(OrgDataCon.getClinicalData(org.Id).size()==1);
       OrgDataCon.getOrg(org.Id);       
       
       Organization_Data__c odRI=new Organization_Data__c(Organization__c=org.Id, RecordTypeID=Schema.SObjectType.Organization_Data__c.getRecordTypeInfosByDeveloperName().get('Regulatory_Info').getRecordTypeId());
       OrgDataCon.addODRecord(odRI, lstFiles, UserInfo.GetUserId() + ';');
       OrgDataCon.getRegulatoryData(org.Id);
       
       Organization_Data__c odMarketing=new Organization_Data__c(Organization__c=org.Id, RecordTypeID=Schema.SObjectType.Organization_Data__c.getRecordTypeInfosByDeveloperName().get('Marketing').getRecordTypeId());
       OrgDataCon.addODRecord(odMarketing, lstFiles, UserInfo.GetUserId() + ';');
       OrgDataCon.getMarketingData(org.Id);
       
       Organization_Data__c odFinancials=new Organization_Data__c(Organization__c=org.Id, RecordTypeID=Schema.SObjectType.Organization_Data__c.getRecordTypeInfosByDeveloperName().get('Financials').getRecordTypeId());
       OrgDataCon.addODRecord(odFinancials, lstFiles, UserInfo.GetUserId() + ';');
       OrgDataCon.getFinancialData(org.Id);
       
       Organization_Data__c odPN=new Organization_Data__c(Organization__c=org.Id, RecordTypeID=Schema.SObjectType.Organization_Data__c.getRecordTypeInfosByDeveloperName().get('Product_Note').getRecordTypeId());
       OrgDataCon.addODRecord(odPN, lstFiles, UserInfo.GetUserId() + ';');
       OrgDataCon.getOrgProductNotes(org.Id);       
      
       OrgDataCon.updateRegulatoryData(lstOD);
       
       AddProductCon.getProduct(op.Id);    AddProductCon.getOrg(org.Id);
       AddProductCon.addProductNote(org, op, 'PN1', lstFiles, 'NT1', UserInfo.GetUserId() + ';');
       
       CreateSupportRequestCon.processFeedback('SR1', 'General','Normal', 'FB1', lstFiles);    
    }
    @isTest static void FeedbackForm_Test () {
        CollaborationGroup cg= new CollaborationGroup(Name='OrgDataCon_Test', CollaborationType='Public', OwnerId=UserInfo.GetUserId());
        insert cg;
        feedbackForm.processFeedback('s1', 'fb1',cg.Id, System.URL.getSalesforceBaseURL().toExternalForm());
    }
    
    
}