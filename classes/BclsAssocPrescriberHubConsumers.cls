/*--------------------------------------------------------------------------------------------------
@Author      - Sudheer Vemula
@Modified 	 - June 23, 2020. Tanay Kulkarni - Implemented code review comments, removed few collections and optimized code
@Date        - 05/26/2020
@Description - CRMSF-5496 : Associate Prescribers based on the NPI number to a HUB Consumer Account
This process runs to update already existing consumers with HUB Patient Ids. This also creates opportunity
for qualifying accounts
--------------------------------------------------------------------------------------------------*/
public class BclsAssocPrescriberHubConsumers implements Database.Batchable<Sobject>{
    Public Database.QueryLocator start(database.BatchableContext bc){
        string query = 'SELECT Id, Prescribers__c, Source_Prescriber_NPI__c, System_Of_Origin__c, System_Of_Origin_Id__c, Active_Opportunity__c FROM Account WHERE Recordtype.DeveloperName = \'consumers\' AND Unique_Integration_Id__c != null AND System_Of_Origin__c IN (\'gpo\', \'crm\') AND System_Of_Origin_Id__c != null AND Prescribers__c = null AND Source_Prescriber_NPI__c != null';
        return Database.getQueryLocator(query);
    }
    public void execute(Database.BatchableContext bc,List<Account> accList){
        set<string> prescriberNPInumberSet = new set<string>(); //holds Source NPI Set from Consumer Accounts
        Set<Id> accountIdWithoutOpptySet = new Set<Id>(); //holds acccount Id Set where existing open Oppty is 0
        List<Account> accountUpdateList = new List<Account>();
        Map<String,Account> prescriberAccountsMap = new  Map<String,Account>();
        for(account acc : accList){
            if(acc.Prescribers__c == null)
                prescriberNPInumberSet.add(acc.Source_Prescriber_NPI__c);
        }
        
        for(account prescriberObj : [SELECT Id, NPI_Number__c FROM Account WHERE Recordtype.DeveloperName = : ClsApexConstants.PRESCRIBER AND NPI_Number__c IN : prescriberNPInumberSet]){
            prescriberAccountsMap.put(prescriberObj.NPI_Number__c,prescriberObj);
        }
        
        for(account acc : accList){
            if(String.Isblank(acc.Prescribers__c) && prescriberAccountsMap.containsKey(acc.Source_Prescriber_NPI__c) &&
               prescriberAccountsMap.get(acc.Source_Prescriber_NPI__c) != null){
                   acc.Prescribers__c = prescriberAccountsMap.get(acc.Source_Prescriber_NPI__c).Id; 
                   if(acc.Active_Opportunity__c == ClsApexConstants.ZERO_INTEGER){
                       accountIdWithoutOpptySet.add(acc.Id);
                   }
                   accountUpdateList.add(acc);
               }
        }
        if(!accountUpdateList.isEmpty()){
            Database.SaveResult[] updatedAccounts = Database.update(accountUpdateList,ClsApexConstants.BOOLEAN_FALSE);
            
            if(!accountIdWithoutOpptySet.isEmpty()){
                ClsApexUtility.createUpdateOpptyOnAccount(accountIdWithoutOpptySet);
            }
            dmlErrorLog(updatedAccounts);
        }
    }
    
    public void finish(Database.BatchableContext bc){
        
    }
    
    public void dmlErrorLog(Database.SaveResult[]  saveResultObjList){
        List<Apex_Debug_Log__c> apexDebugLogList = new List<Apex_Debug_Log__c>();
        for(Database.SaveResult saveResultObj : saveResultObjList){
            if (!saveResultObj.isSuccess()){
                for(Database.Error err : saveResultObj.getErrors()){
                    ClsApexUtility.logApexDebugRecord(err.getMessage(), saveResultObj.getId(), 'Execute', err.getMessage(), 'BclsAddPrescribersToAsembiaAccounts', ClsApexConstants.DEBUG_TYPE_ERROR);
                }
            }
        }
        if(!apexDebugLogList.isEmpty()){
            Database.insert(apexDebugLogList, ClsApexConstants.BOOLEAN_FALSE);
        }
    }
}