/*******************************************************************************************************************
@Author         : Jagan Periyakaruppan
@Date Created   : 02/26/2018
@Description    : Order Trigger handler to process order Trigger
********************************************************************************************************************/    

public class ClsOrderAuditHandler
{
    public static Map<String, String> getfieldMappings(String type){
        Map<String, String> fieldMappings = new Map<String, String>();
        for(QUDAX_Field_Mapping__mdt newMdt : [SELECT MasterLabel, DeveloperName from QUDAX_Field_Mapping__mdt where Audit_Area__c = : type]){
                fieldMappings.put(newMdt.MasterLabel, newMdt.DeveloperName);                
        }
        return fieldMappings;
    }
    public static void insertAuditsForCheck(Map<Id,order> orderMap, Map<Id,Map<String, String>> orderCheckMap, Map<String,List<String>> mdtOrderMap, String auditArea){
        List<Order_Audit__c> orderAuditList = new List<Order_Audit__c>();
        List<Order_Audit_Item__c> orderAuditItemsList = new List<Order_Audit_Item__c>(); 
        Map<String, String> fieldMappings = new Map<String, String>();
        fieldMappings = getfieldMappings(auditArea);
        
        //Creating Audit records. 
        for(Id orderRec : orderMap.keySet()){
            //Bypass check Audit 
            Order_Audit__c bypassAuditRec = addAudit(auditArea, false, 'Bypass Check', false ,orderRec);
            orderAuditList.add(bypassAuditRec);
        
            for(String auditName : mdtOrderMap.keySet()){
                if(!orderCheckMap.isEmpty() && orderCheckMap.containsKey(orderRec)){
                    String keyName;
                    if( !fieldMappings.isEmpty() && 
                        orderCheckMap.get(orderRec).containsKey(fieldMappings.get(auditName)) && 
                        orderCheckMap.get(orderRec).get(fieldMappings.get(auditName)) != null){
                        keyName = fieldMappings.get(auditName);
                    }
                    
                    System.debug('Audit KeyName : ' + keyName );
                    if(orderCheckMap.get(orderRec).containsKey(keyName)){
                        System.debug('Name : ' + auditName + ' | Value : ' + orderCheckMap.get(orderRec).get(keyName));
                        Order_Audit__c thisOrderAuditRec = addAudit(auditArea, false, auditName, Boolean.valueOf(orderCheckMap.get(orderRec).get(keyName)),orderRec);
                        orderAuditList.add(thisOrderAuditRec);
                    }
                }   
            }
        }
        if(!orderAuditList.isEmpty()){
            try{
                insert orderAuditList;
                
                for(Order_Audit__c orderAuditRec : orderAuditList){
                    Set<String> auditItemKeySet = new Set<String>();
                    auditItemKeySet.addAll(orderCheckMap.get(orderAuditRec.Order_Number__c).keySet());
                    System.debug('auditItemKeySet >>' +auditItemKeySet);
                    if(orderAuditRec.Audit_Field__c != 'Bypass Check'){
                        for(String auditItemName : mdtOrderMap.get(orderAuditRec.Audit_Field__c)){
                            System.debug('auditItemName >>' +auditItemName);
                            String keyName = '';
                                if(!fieldMappings.isEmpty() &&  auditItemKeySet.contains(fieldMappings.get(auditItemName)))
                                keyName = fieldMappings.get(auditItemName);
                                
                                if(!String.isBlank(keyName) && orderCheckMap.get(orderAuditRec.Order_Number__c).containsKey(keyName)){
                                    Order_Audit_Item__c  orderItemRec = addAuditItem(auditItemName,orderCheckMap.get(orderAuditRec.Order_Number__c).get(keyName),orderAuditRec.id);
                                    System.debug('keyName >> '+keyName + ' << Value >> ' + orderCheckMap.get(orderAuditRec.Order_Number__c).get(keyName));
                                    orderAuditItemsList.add(orderItemRec);
                                }   
                        }
                    }                   
                }               
                
                insert orderAuditItemsList;
                
            }catch(Exception ex){
                System.debug('Exception while inserting Audit Records for >> '  + auditArea + 'Ex : ' + ex);
            }       
        }        
    }
    
    public static String getDocumentName(String docName){
        String documentName;
        Switch on docName {
                    when 'ChartNotes'{
                        documentName = 'Document - Chart Notes';
                    }
                    when 'CMN'{
                        documentName = 'Document - CMN';
                    }   
                    when 'AOB'{
                        documentName = 'Document - AOB';
                    }
                    when 'BGLogs'{
                        documentName = 'Document - BG Logs';
                    }
                    
                    when 'LabResults'{
                        documentName = 'Document - Lab Results';
                    }
                    when 'HourTrial'{
                        documentName = 'Document - 72 Hour Trial';
                    }
                    when 'ABN'{
                        documentName = 'Document - ABN';
                    }
                    when 'OOWLetter'{
                        documentName = 'Document - OOW Letter';
                    }
                    when 'HypoglycemicQuestionnaire'{
                        documentName = 'Document - Hypoglycemic Questionnaire';
                    }
                    when 'OriginalDateofPurchase'{
                        documentName = 'Document - Original Date Of Purchase';
                    }
                    when 'TechSupportNotes'{
                        documentName = 'Document - Tech Support Notes';
                    }//updated below 2 conditions to add new docs
            		when 'RecentNotesWithCompliance'{
                        documentName = 'Document - Recent Notes With Compliance';
                    }
            		when 'LMN'{
                		documentName = 'Document - LMN';
            		}
                    when else {
                        documentName = '';
                    }}
                return documentName;    
    }
    public static void insertAuditsForDocumentCheck(Map<Id,order> documentMap, Map<Id,Map<String, String>> documentCheckMap, Map<String,List<String>> mdtDocumentMap){
        List<Order_Audit__c> orderAuditList = new List<Order_Audit__c>();
        List<Order_Audit_Item__c> orderAuditItemsList = new List<Order_Audit_Item__c>();
        Set<String> documentKeySet = new Set<String>();
        Map<String, String> fieldMappings = new Map<String, String>();
        fieldMappings = getfieldMappings('Document');
        
        System.debug('documentCheckMap>> '+documentCheckMap);
        for(Id orderRec : documentMap.keySet()){
            for(String documentName : documentCheckMap.get(orderRec).keySet()){
                if(documentCheckMap.get(orderRec).get(documentName) != null) 
                    documentKeySet.add(orderRec + '|' +getDocumentName(documentName));
            }
        }  
        System.debug('documentKeySet>> '+documentKeySet);
        System.debug('mdtDocumentMap >> '+mdtDocumentMap);  
        //Creating 'documents' Audit records
        for(Id orderRec : documentMap.keySet()){
            if(!documentMap.get(orderRec).Is_Cash_Order__c && 
                (documentMap.get(orderRec).Price_Book__r.Oracle_Id__c != '6550' &&
                   documentMap.get(orderRec).Price_Book__r.Oracle_Id__c != '6840' &&
                    documentMap.get(orderRec).Price_Book__r.Oracle_Id__c != '13920' )){
                //Bypass Document check Audit 
                Order_Audit__c bypassAuditRec = addAudit('Document', false, 'Bypass Check', false ,orderRec);
                orderAuditList.add(bypassAuditRec);
            }
            for(String auditName : mdtDocumentMap.keySet()){
                Boolean auditVerified = false;
                if(!documentKeySet.isEmpty() && documentKeySet.contains(orderRec + '|' + auditName)){
                    System.debug('auditName >> '+auditName);
                        //if(auditName == 'Document - CMN') auditVerified = documentCheckMap.get(orderRec).containsKey('CMN') ? Boolean.valueOf(documentCheckMap.get(orderRec).get('CMN')) : false;
                       // if(auditName == 'Document - Chart Notes') auditVerified = documentCheckMap.get(orderRec).containsKey('ChartNotes') ? Boolean.valueOf(documentCheckMap.get(orderRec).get('ChartNotes')) : false;
                        
                        Order_Audit__c thisOrderAuditRec = addAudit('Document', false, auditName, auditVerified, orderRec);
                        orderAuditList.add(thisOrderAuditRec);                    
                }   
            }
        }
        
       if(!orderAuditList.isEmpty()){
            try{
                insert orderAuditList;
                 
                for(Order_Audit__c orderAuditRec : orderAuditList){
                    System.debug('orderAuditRec >> '+orderAuditRec);
                    
                    Set<String> auditItemKeySet = new Set<String>();
                    auditItemKeySet.addAll(documentCheckMap.get(orderAuditRec.Order_Number__c).keySet());
                    System.debug('auditItemKeySet >> '+auditItemKeySet);
                    
                    
                    if(mdtDocumentMap.get(orderAuditRec.Audit_Field__c) != null){
                        for(String auditItemName : mdtDocumentMap.get(orderAuditRec.Audit_Field__c)){
                            String keyName = '';
                             System.debug('auditItemName >> '+auditItemName);
                            
                                if( auditItemKeySet.contains(fieldMappings.get(auditItemName)) ){
                                    keyName = fieldMappings.get(auditItemName);
                                
                                    System.debug('keyName >> '+keyName);    
                                }
                                if(!String.isBlank(keyName) && documentCheckMap.get(orderAuditRec.Order_Number__c).containsKey(keyName)){
                                    Order_Audit_Item__c  orderItemRec = addAuditItem(auditItemName,documentCheckMap.get(orderAuditRec.Order_Number__c).get(keyName),orderAuditRec.id);
                                    System.debug('keyName >> '+keyName + ' << Value >> ' + documentCheckMap.get(orderAuditRec.Order_Number__c).get(keyName));
                                    orderAuditItemsList.add(orderItemRec);
                                }   
                        }
                    }                   
                }               
                
                insert orderAuditItemsList;
            }catch(Exception ex){
                    System.debug('Exception while inserting Document Audit Records >> ' + ex);
                }  
        }        
    }

     public static Order_Audit__c addAudit(string audit_Area, Boolean audit_Exception , String audit_Field, boolean audit_Verified, String Order_Number){
        Order_Audit__c orderAudit=new Order_Audit__c();
        orderAudit.Audit_Area__c = audit_Area;
        orderAudit.Audit_Exception__c = audit_Exception;
        orderAudit.Audit_Field__c = audit_Field;
        orderAudit.Audit_Verified__c = audit_Verified;
        orderAudit.Order_Number__c = order_Number;
        return orderAudit;
    }
    
    public static Order_Audit_Item__c addAuditItem(string audit_Item, string audit_Item_Value,string Order_Audit){
        Order_Audit_Item__c orderAuditItem=new Order_Audit_Item__c(); 
        orderAuditItem.Audit_Item_Field__c =audit_Item;
        orderAuditItem.Audit_Item_Value__c =audit_Item_Value;
        orderAuditItem.Order_Audit__c =Order_Audit;
        
        return orderAuditItem;
    }
    
    public static Map<String, String> getAuditDetailsforCheck(String auditArea, String orderId){
        Map<String, String> fieldValueMap = new Map<String, String>();
        Set<Id> auditIdSet = new Set<Id>();
        Map<String, String> fieldMappings = new Map<String, String>();
        fieldMappings = getfieldMappings(auditArea);
        
        System.debug('auditArea >> ' + auditArea + '<< orderId >> ' + orderId);
        
        for(Order_Audit__c thisAudit : [Select Id, Audit_Area__c, Audit_Exception__c, Audit_Field__c, Audit_Verified__c, Order_Number__c,
                                        Audit_Exception_Added_By__c, Audit_Exception_Added_Date__c from Order_Audit__c 
                                        where Order_Number__c =:orderId and Audit_Area__c =: auditArea]){                                           
                if(thisAudit.Audit_Field__c == 'Bypass Check')
                    fieldValueMap.put('bypassCheck' , String.valueOf(thisAudit.Audit_Exception__c));
                else                                
                    fieldValueMap.put(fieldMappings.get(thisAudit.Audit_Field__c) , String.valueOf(thisAudit.Audit_Verified__c));
                    
                auditIdSet.add(thisAudit.Id);
        }
        System.debug('auditIdSet >> ' + auditIdSet);
        if(!auditIdSet.isEmpty()){
             for(Order_Audit_Item__c thisAuditItem : [Select Id, Audit_Item_Field__c, Audit_Item_Value__c, Order_Audit__c from Order_Audit_Item__c
                                                    where Order_Audit__c IN : auditIdSet]){         
                fieldValueMap.put(fieldMappings.get(thisAuditItem.Audit_Item_Field__c), thisAuditItem.Audit_Item_Value__c);            
            }   
        }
        System.debug('fetching fieldValue >> ' + fieldValueMap);
        return fieldValueMap;
    }
}