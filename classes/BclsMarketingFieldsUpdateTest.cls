@isTest(seeAllData = false)
public class BclsMarketingFieldsUpdateTest {

    @isTest
    public static void BclsMarketingFieldsUpdateSchedTest(){
        Test.startTest();
        BclsMarketingFieldsUpdateSched scheduler = new BclsMarketingFieldsUpdateSched();
        String sch = '0 0 23 * * ?';
        system.schedule('Bcls Marketing Fields Update', sch, scheduler);
        Test.stopTest();
    }
}