/**
 * HourlyDoubleOptInBatchTest
 * Tests for BclsHourlyDoubleOptInBatch
 * @author Craig Johnson(Sundog)
 * @date 03/19/19
 */
@isTest()
public class BclsHourlyDoubleOptInBatchTest {
    
    // Test for hourly job Scheduler
    static testMethod void testHourlyEmailScheduledJob() {
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock());
        insert TestDataBuilder.testURLExpiryVal();

        List<Account> accountList= new List<Account>();
        Account testAccount = TestDataBuilder.testAccount();
        testAccount.PersonEmail='jane.smith@sundog.net';
        testAccount.Email_Pending_Opt_In__c = true;
        testAccount.Email_Opt_In_Language__c = 'English';
        testAccount.Email_Opt_In_Method__c = 'Via Email';
        accountList.add(testAccount); 
        
        Account testAccount2 = TestDataBuilder.testAccount();
        testAccount2.PersonEmail='john.smith@sundog.net';
        testAccount2.Email_Pending_Opt_In__c = true;
        testAccount2.Email_Opt_In_Language__c = 'English';
        testAccount2.Email_Opt_In_Method__c = 'Via Email';
        accountList.add(testAccount2);
        insert accountList;
        
        List<Lead> leadList= new List<Lead>();
        Lead testLead = TestDataBuilder.testLead();
        testLead.Email_Opt_In_Language__c = 'English';
        testLead.Email_Opt_In_Method__c = 'Via Email';
        testLead.Email='jane.smith@sundog.net';
        testLead.Email_Pending_Opt_In__c = true;
        leadList.add(testLead); 
        
        Lead testlead2 = TestDataBuilder.testlead();
        testLead2.Email_Opt_In_Language__c = 'English';
        testLead2.Email_Opt_In_Method__c = 'Via Email';
        testlead2.Email='john.smith@sundog.net';
        testlead2.Email_Pending_Opt_In__c = true;
        leadList.add(testlead2);
        insert leadList;
        
        Test.startTest();
        String jobId = System.schedule('testEmailScheduled', '0 0 0 3 9 ? 2052', new SclsHourlyDoubleOptInScheduled());
        Test.stopTest();
        
        System.assertEquals(false, String.isBlank(jobId));
    }
    
    
    
    // Test for batch job on Account from Canada
    static testMethod void testCanadaAccountOptIn() {
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock());
        
        //Query 'CA_Consumer' record type
        RecordType recordTypeAccount = [select Id FROM RecordType 
            where DeveloperName = 'CA_Consumer' and SobjectType = 'Account' limit 1];
        //Create 2 Test Accounts to put trigger in bulk mode
        List<Account> accountList= new List<Account>();
        Account testAccount = TestDataBuilder.testAccount();
        testAccount.PersonEmail = 'jane.smith@sundog.net';
        testAccount.Email_Pending_Opt_In__c = true;
        testAccount.RecordTypeId = recordTypeAccount.Id;
        testAccount.Email_Opt_In_Language__c = 'English';
        testAccount.Email_Opt_In_Method__c = 'Via Email';
        accountList.add(testAccount);
        Account testAccount2 = TestDataBuilder.testAccount();
        testAccount2.Email_Pending_Opt_In__c = false;
        testAccount2.RecordTypeId = recordTypeAccount.Id;
        testAccount2.Email_Opt_In_Language__c = 'English';
        testAccount2.Email_Opt_In_Method__c = 'Via Email';
        accountList.add(testAccount2);
        insert accountList;
        
        Account resultAccount = [SELECT Email_Double_Opt_In_Subscriber__c FROM Account where Email_Pending_Opt_In__c = true];
        System.assertEquals(null, resultAccount.Email_Double_Opt_In_Subscriber__c);

        //Run batch job
        Test.startTest();
        BclsHourlyDoubleOptInBatch emailbatch = new BclsHourlyDoubleOptInBatch(BclsHourlyDoubleOptInBatch.BatchType.ACCOUNT);
        Id batchInstanceId = Database.executeBatch(emailbatch, 1);
        Test.stopTest();

        resultAccount = [SELECT Email_Double_Opt_In_Subscriber__c FROM Account where Email_Pending_Opt_In__c = true];
        
        System.assertNotEquals(null, resultAccount.Email_Double_Opt_In_Subscriber__c);
    }
    
    // Test for batch job on Account from GB
    static testMethod void testGBAccountOptIn() {
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock());
        
        //Query 'GB_Consumer' record type
        RecordType recordTypeAccount = [select Id FROM RecordType 
            where DeveloperName = 'GB_Consumer' and SobjectType = 'Account' limit 1];
        //Create 2 Test Accounts to put trigger in bulk mode
        List<Account> accountList= new List<Account>();
        Account testAccount = TestDataBuilder.testAccount();
        testAccount.PersonEmail = 'jane.smith@sundog.net';
        testAccount.Email_Pending_Opt_In__c = true;
        testAccount.RecordTypeId = recordTypeAccount.Id;
        testAccount.Email_Opt_In_Language__c = 'English';
        testAccount.Email_Opt_In_Method__c = 'Via Email';
        accountList.add(testAccount);
        Account testAccount2 = TestDataBuilder.testAccount();
        testAccount2.Email_Pending_Opt_In__c = false;
        testAccount2.RecordTypeId = recordTypeAccount.Id;
        testAccount2.Email_Opt_In_Language__c = 'English';
        testAccount2.Email_Opt_In_Method__c = 'Via Email';
        accountList.add(testAccount2);
        insert accountList;
        
        Account resultAccount = [SELECT Email_Double_Opt_In_Subscriber__c FROM Account where Email_Pending_Opt_In__c = true];
        System.assertEquals(null, resultAccount.Email_Double_Opt_In_Subscriber__c);

        //Run batch job
        Test.startTest();
        BclsHourlyDoubleOptInBatch emailbatch = new BclsHourlyDoubleOptInBatch(BclsHourlyDoubleOptInBatch.BatchType.ACCOUNT);
        Id batchInstanceId = Database.executeBatch(emailbatch, 1);
        Test.stopTest();

        resultAccount = [SELECT Email_Double_Opt_In_Subscriber__c FROM Account where Email_Pending_Opt_In__c = true];
        
        System.assertNotEquals(null, resultAccount.Email_Double_Opt_In_Subscriber__c);
    }
    
    // Test for batch job on Account from Ireland
    static testMethod void testIrelandAccountOptIn() {
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock());
        
        //Query 'IE_Consumer' record type
        RecordType recordTypeAccount = [select Id FROM RecordType 
            where DeveloperName = 'IE_Consumer' and SobjectType = 'Account' limit 1];
        //Create 2 Test Accounts to put trigger in bulk mode
        List<Account> accountList= new List<Account>();
        Account testAccount = TestDataBuilder.testAccount();
        testAccount.PersonEmail = 'jane.smith@sundog.net';
        testAccount.Email_Pending_Opt_In__c = true;
        testAccount.RecordTypeId = recordTypeAccount.Id;
        testAccount.Email_Opt_In_Language__c = 'English';
        testAccount.Email_Opt_In_Method__c = 'Via Email';
        accountList.add(testAccount);
        Account testAccount2 = TestDataBuilder.testAccount();
        testAccount2.Email_Pending_Opt_In__c = false;
        testAccount2.RecordTypeId = recordTypeAccount.Id;
        testAccount2.Email_Opt_In_Language__c = 'English';
        testAccount2.Email_Opt_In_Method__c = 'Via Email';
        accountList.add(testAccount2);
        insert accountList;
        
        Account resultAccount = [SELECT Email_Double_Opt_In_Subscriber__c FROM Account where Email_Pending_Opt_In__c = true];
        System.assertEquals(null, resultAccount.Email_Double_Opt_In_Subscriber__c);

        //Run batch job
        Test.startTest();
        BclsHourlyDoubleOptInBatch emailbatch = new BclsHourlyDoubleOptInBatch(BclsHourlyDoubleOptInBatch.BatchType.ACCOUNT);
        Id batchInstanceId = Database.executeBatch(emailbatch, 1);
        Test.stopTest();

        resultAccount = [SELECT Email_Double_Opt_In_Subscriber__c FROM Account where Email_Pending_Opt_In__c = true];
        
        System.assertNotEquals(null, resultAccount.Email_Double_Opt_In_Subscriber__c);
    }
    
    static testMethod void testAllAccountsOptIn(){
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock());
        RecordType caRecordType = [select Id FROM RecordType 
            where DeveloperName = 'CA_Consumer' and SobjectType = 'Account' limit 1];
        RecordType gbRecordType = [select Id FROM RecordType 
            where DeveloperName = 'GB_Consumer' and SobjectType = 'Account' limit 1];
        RecordType ieRecordType = [select Id FROM RecordType 
            where DeveloperName = 'IE_Consumer' and SobjectType = 'Account' limit 1];
        
        // create one account from each country
        List<Account> accountList= new List<Account>();
        // Canada
        Account caAccount = TestDataBuilder.testAccount();
        caAccount.PersonEmail = 'joan.smith@sundog.net';
        caAccount.Email_Pending_Opt_In__c = true;
        caAccount.RecordTypeId = caRecordType.Id;
        caAccount.Email_Opt_In_Language__c = 'English';
        caAccount.Email_Opt_In_Method__c = 'Via Email';
        accountList.add(caAccount);
        
        // Ireland
        Account ieAccount = TestDataBuilder.testAccount();
        ieAccount.PersonEmail = 'peggy.smith@sundog.net';
        ieAccount.Email_Pending_Opt_In__c = true;
        ieAccount.RecordTypeId = ieRecordType.Id;
        ieAccount.Email_Opt_In_Language__c = 'English';
        ieAccount.Email_Opt_In_Method__c = 'Via Email';
        accountList.add(ieAccount);
        
        // GB
        Account gbAccount = TestDataBuilder.testAccount();
        gbAccount.PersonEmail = 'jane.smith@sundog.net';
        gbAccount.Email_Pending_Opt_In__c = true;
        gbAccount.RecordTypeId = gbRecordType.Id;
        gbAccount.Email_Opt_In_Language__c = 'English';
        gbAccount.Email_Opt_In_Method__c = 'Via Email';
        accountList.add(gbAccount);
        
        insert accountList;
        
        // verify state is correct before processing records
        for(Account a : [SELECT Email_Double_Opt_In_Subscriber__c FROM Account where Email_Pending_Opt_In__c = true]){
            System.assertEquals(null, a.Email_Double_Opt_In_Subscriber__c);
        }
        
        // run batch in test mode
        Test.startTest();
        BclsHourlyDoubleOptInBatch emailbatch = new BclsHourlyDoubleOptInBatch(BclsHourlyDoubleOptInBatch.BatchType.ACCOUNT);
        Id batchInstanceId = Database.executeBatch(emailbatch, 3);
        Test.stopTest();
        
        // verify state of accounts has changed
        for(Account a : [SELECT Email_Double_Opt_In_Subscriber__c FROM Account where Email_Pending_Opt_In__c = true]){
            System.assertNotEquals(null, a.Email_Double_Opt_In_Subscriber__c);
        }
    }

    static testMethod void testCanadaAccountOptOut() {
        //Query 'CA_Consumer' record type
        RecordType recordTypeAccount = [select Id FROM RecordType 
            where DeveloperName = 'CA_Consumer' and SobjectType = 'Account' limit 1];

        //Create a Test Lead
        List<Lead> leadList= new List<Lead>();
        Lead testLead = TestDataBuilder.testLead();
        testLead.Email = 'jane.smith@sundog.net';
        testLead.Email_Pending_Opt_Out__c = true;
        testLead.Email_Opt_In_List__c = 'True';
        testLead.Email_Double_Opt_In_Subscriber__c = Datetime.now();
        testLead.Email_Opt_In_Language__c = 'English';
        testLead.Email_Opt_In_Method__c = 'Via Web Form';
        leadList.add(testLead); 
        insert TestDataBuilder.testURLExpiryVal();
        insert leadList;

        Database.LeadConvert lc = new database.LeadConvert();
        lc.setLeadId(testLead.Id);
        lc.convertedStatus = 'Send to Customer Ops';
        lc.setDoNotCreateOpportunity(true);
        Database.LeadConvertResult lcr = Database.convertLead(lc, false);
        //System.assertEquals(null, lcr.getErrors());
        System.assert(lcr.isSuccess());
        
        //query account record where double opt in penbding is true
        Lead resultLead1 = [SELECT Id, Email_Opt_In_List__c, ConvertedAccountId FROM Lead where Id = :testLead.Id];
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock(resultLead1.Id));
        
        Account resultAccount = [SELECT RecordTypeId, Email_Pending_Opt_Out__c, PersonContactId, Email_Double_Opt_In_Subscriber__c FROM Account where Id = :resultLead1.ConvertedAccountId];
        System.assertNotEquals(null, resultAccount.Email_Double_Opt_In_Subscriber__c);
        System.assertEquals(true, resultAccount.Email_Pending_Opt_Out__c);

        resultAccount.RecordTypeId = recordTypeAccount.Id;
        update resultAccount;

        //the normal process flow would only result in the customer being in MC 1 time under lead or account
        //but we test to support the case that they are in as both
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock(new List<String>{ testLead.Id, resultAccount.PersonContactId }, 'Active'));

        //Run batch job
        Test.startTest();
        BclsHourlyDoubleOptInBatch emailbatch = new BclsHourlyDoubleOptInBatch(BclsHourlyDoubleOptInBatch.BatchType.ACCOUNT);
        Id batchInstanceId = Database.executeBatch(emailbatch, 1);
        Test.stopTest();

        resultAccount = [SELECT Email_Double_Opt_In_Subscriber__c, Email_Pending_Opt_Out__c, Email_Opt_In_List__c FROM Account where Id = :resultAccount.Id];
        
        System.assertEquals(null, resultAccount.Email_Double_Opt_In_Subscriber__c);
        System.assertEquals(false, resultAccount.Email_Pending_Opt_Out__c);
        System.assertEquals('False', resultAccount.Email_Opt_In_List__c);
    }
    
    static testMethod void testGBAccountOptOut() {
        //Query 'CA_Consumer' record type
        RecordType recordTypeAccount = [select Id FROM RecordType 
            where DeveloperName = 'GB_Consumer' and SobjectType = 'Account' limit 1];

        //Create a Test Lead
        List<Lead> leadList= new List<Lead>();
        Lead testLead = TestDataBuilder.testLead();
        testLead.Email = 'jane.smith@sundog.net';
        testLead.Email_Pending_Opt_Out__c = true;
        testLead.Email_Opt_In_List__c = 'True';
        testLead.Email_Double_Opt_In_Subscriber__c = Datetime.now();
        testLead.Email_Opt_In_Language__c = 'English';
        testLead.Email_Opt_In_Method__c = 'Via Web Form';
        leadList.add(testLead); 
        insert TestDataBuilder.testURLExpiryVal();
        insert leadList;

        Database.LeadConvert lc = new database.LeadConvert();
        lc.setLeadId(testLead.Id);
        lc.convertedStatus = 'Send to Customer Ops';
        lc.setDoNotCreateOpportunity(true);
        Database.LeadConvertResult lcr = Database.convertLead(lc, false);
        //System.assertEquals(null, lcr.getErrors());
        System.assert(lcr.isSuccess());
        
        //query account record where double opt in penbding is true
        Lead resultLead1 = [SELECT Id, Email_Opt_In_List__c, ConvertedAccountId FROM Lead where Id = :testLead.Id];
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock(resultLead1.Id));
        
        Account resultAccount = [SELECT RecordTypeId, Email_Pending_Opt_Out__c, PersonContactId, Email_Double_Opt_In_Subscriber__c FROM Account where Id = :resultLead1.ConvertedAccountId];
        System.assertNotEquals(null, resultAccount.Email_Double_Opt_In_Subscriber__c);
        System.assertEquals(true, resultAccount.Email_Pending_Opt_Out__c);

        resultAccount.RecordTypeId = recordTypeAccount.Id;
        update resultAccount;

        //the normal process flow would only result in the customer being in MC 1 time under lead or account
        //but we test to support the case that they are in as both
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock(new List<String>{ testLead.Id, resultAccount.PersonContactId }, 'Active'));

        //Run batch job
        Test.startTest();
        BclsHourlyDoubleOptInBatch emailbatch = new BclsHourlyDoubleOptInBatch(BclsHourlyDoubleOptInBatch.BatchType.ACCOUNT);
        Id batchInstanceId = Database.executeBatch(emailbatch, 1);
        Test.stopTest();

        resultAccount = [SELECT Email_Double_Opt_In_Subscriber__c, Email_Pending_Opt_Out__c, Email_Opt_In_List__c FROM Account where Id = :resultAccount.Id];
        
        System.assertEquals(null, resultAccount.Email_Double_Opt_In_Subscriber__c);
        System.assertEquals(false, resultAccount.Email_Pending_Opt_Out__c);
        System.assertEquals('False', resultAccount.Email_Opt_In_List__c);
    }
    
    static testMethod void testIrelandAccountOptOut() {
        //Query 'CA_Consumer' record type
        RecordType recordTypeAccount = [select Id FROM RecordType 
            where DeveloperName = 'IE_Consumer' and SobjectType = 'Account' limit 1];

        //Create a Test Lead
        List<Lead> leadList= new List<Lead>();
        Lead testLead = TestDataBuilder.testLead();
        testLead.Email = 'jane.smith@sundog.net';
        testLead.Email_Pending_Opt_Out__c = true;
        testLead.Email_Opt_In_List__c = 'True';
        testLead.Email_Double_Opt_In_Subscriber__c = Datetime.now();
        testLead.Email_Opt_In_Language__c = 'English';
        testLead.Email_Opt_In_Method__c = 'Via Web Form';
        leadList.add(testLead); 
        insert TestDataBuilder.testURLExpiryVal();
        insert leadList;

        Database.LeadConvert lc = new database.LeadConvert();
        lc.setLeadId(testLead.Id);
        lc.convertedStatus = 'Send to Customer Ops';
        lc.setDoNotCreateOpportunity(true);
        Database.LeadConvertResult lcr = Database.convertLead(lc, false);
        //System.assertEquals(null, lcr.getErrors());
        System.assert(lcr.isSuccess());
        
        //query account record where double opt in penbding is true
        Lead resultLead1 = [SELECT Id, Email_Opt_In_List__c, ConvertedAccountId FROM Lead where Id = :testLead.Id];
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock(resultLead1.Id));
        
        Account resultAccount = [SELECT RecordTypeId, Email_Pending_Opt_Out__c, PersonContactId, Email_Double_Opt_In_Subscriber__c FROM Account where Id = :resultLead1.ConvertedAccountId];
        System.assertNotEquals(null, resultAccount.Email_Double_Opt_In_Subscriber__c);
        System.assertEquals(true, resultAccount.Email_Pending_Opt_Out__c);

        resultAccount.RecordTypeId = recordTypeAccount.Id;
        update resultAccount;

        //the normal process flow would only result in the customer being in MC 1 time under lead or account
        //but we test to support the case that they are in as both
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock(new List<String>{ testLead.Id, resultAccount.PersonContactId }, 'Active'));

        //Run batch job
        Test.startTest();
        BclsHourlyDoubleOptInBatch emailbatch = new BclsHourlyDoubleOptInBatch(BclsHourlyDoubleOptInBatch.BatchType.ACCOUNT);
        Id batchInstanceId = Database.executeBatch(emailbatch, 1);
        Test.stopTest();

        resultAccount = [SELECT Email_Double_Opt_In_Subscriber__c, Email_Pending_Opt_Out__c, Email_Opt_In_List__c FROM Account where Id = :resultAccount.Id];
        
        System.assertEquals(null, resultAccount.Email_Double_Opt_In_Subscriber__c);
        System.assertEquals(false, resultAccount.Email_Pending_Opt_Out__c);
        System.assertEquals('False', resultAccount.Email_Opt_In_List__c);
    }
    
    static testMethod void testAllAccountsOptOut(){
        //Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock());
        List<RecordType> allRecordTypes = [select Id FROM RecordType 
            where (DeveloperName = 'CA_Consumer' OR DeveloperName = 'GB_Consumer' OR DeveloperName = 'IE_Consumer') and SobjectType = 'Account'];
        System.debug(allRecordTypes);
        System.assert(allRecordTypes.size() == 3);
        // test leads
        List<Lead> leadList= new List<Lead>();
        // canada
        Lead caLead = TestDataBuilder.testLead();
        caLead.Email = 'jane.smith@sundog.net';
        caLead.Email_Pending_Opt_Out__c = true;
        caLead.Email_Opt_In_List__c = 'True';
        caLead.Email_Double_Opt_In_Subscriber__c = Datetime.now();
        caLead.Email_Opt_In_Language__c = 'English';
        caLead.Email_Opt_In_Method__c = 'Via Web Form';
        leadList.add(caLead); 
        // ireland
        Lead ieLead = TestDataBuilder.testLead();
        ieLead.Email = 'jane.smith@sundog.net';
        ieLead.Email_Pending_Opt_Out__c = true;
        ieLead.Email_Opt_In_List__c = 'True';
        ieLead.Email_Double_Opt_In_Subscriber__c = Datetime.now();
        ieLead.Email_Opt_In_Language__c = 'English';
        ieLead.Email_Opt_In_Method__c = 'Via Web Form';
        leadList.add(ieLead); 
        // gb
        Lead gbLead = TestDataBuilder.testLead();
        gbLead.Email = 'jane.smith@sundog.net';
        gbLead.Email_Pending_Opt_Out__c = true;
        gbLead.Email_Opt_In_List__c = 'True';
        gbLead.Email_Double_Opt_In_Subscriber__c = Datetime.now();
        gbLead.Email_Opt_In_Language__c = 'English';
        gbLead.Email_Opt_In_Method__c = 'Via Web Form';
        leadList.add(gbLead); 
        insert TestDataBuilder.testURLExpiryVal();
        insert leadList;
        
        // convert leads
        Set<Id> newAccountIds = new Set<Id>();
        List<Database.LeadConvert> lcList = new List<Database.LeadConvert>();
        for(Lead l : leadList){
            Database.LeadConvert lc = new database.LeadConvert();
        	lc.setLeadId(l.Id);
        	lc.convertedStatus = 'Send to Customer Ops';
        	lc.setDoNotCreateOpportunity(true);
            lcList.add(lc);
        }
        for(Database.LeadConvertResult lcr : Database.convertLead(lcList, false)){
            System.assert(lcr.isSuccess());
        }
        
        Map<Lead, Account> convertedLeadToAccount = new Map<Lead, Account>();
        List<Lead> convertedLeads = [SELECT Id, Email_Opt_In_List__c, ConvertedAccountId FROM Lead];
        for(Lead cl : convertedLeads){
            Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock(cl.Id));
            newAccountIds.add(cl.ConvertedAccountId);
            convertedLeadToAccount.put(cl, null);
        }
        Integer count = 0;
        List<Account> newAccounts = [SELECT RecordTypeId, Email_Pending_Opt_Out__c, PersonContactId, Email_Double_Opt_In_Subscriber__c FROM Account where Id IN :newAccountIds];
        System.assert(newAccounts.size() == 3);
        for(Account a : newAccounts){
            System.assertNotEquals(null, a.Email_Double_Opt_In_Subscriber__c);
            System.assertEquals(true, a.Email_Pending_Opt_Out__c);
            a.RecordTypeId = allRecordTypes[count].Id;
            count++;
            for(Lead cl : convertedLeadToAccount.keySet()){
                if(cl.ConvertedAccountId == a.Id){
                    convertedLeadToAccount.put(cl, a);
                }
            }
        }
        update newAccounts;
        List<String> subscribers = new List<String>();
        for(Lead cl : convertedLeadToAccount.keySet()){
            subscribers.add(cl.Id);
            subscribers.add(convertedLeadToAccount.get(cl).PersonContactId);
            //Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock(new List<String>{ cl.Id, convertedLeadToAccount.get(cl).PersonContactId }, 'Active'));
        }
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock(subscribers, 'Active'));
        Test.startTest();
        BclsHourlyDoubleOptInBatch emailbatch = new BclsHourlyDoubleOptInBatch(BclsHourlyDoubleOptInBatch.BatchType.ACCOUNT);
        Id batchInstanceId = Database.executeBatch(emailbatch, 3);
        Test.stopTest();
        
        for(Account a : [SELECT RecordTypeId, Email_Pending_Opt_Out__c, Email_Opt_In_List__c, PersonContactId, Email_Double_Opt_In_Subscriber__c FROM Account where Id IN :newAccountIds]){
            System.assertEquals(null, a.Email_Double_Opt_In_Subscriber__c);
            System.assertEquals(false, a.Email_Pending_Opt_Out__c);
            System.assertEquals('False', a.Email_Opt_In_List__c);
        }
        
    }
    
    // Test for batch job on Lead
    static testMethod void testCanadaLeadOptIn() {
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock());

        RecordType recordTypeLead = [SELECT Id FROM RecordType 
            WHERE DeveloperName = 'CA_Lead' and SobjectType = 'Lead' limit 1];

        //Create 2 Test Leads to put trigger in bulk mode
        List<Lead> leadList= new List<Lead>();
        Lead testLead = TestDataBuilder.testLead();
        testLead.Email = 'jane.smith@sundog.net';
        testLead.Email_Opt_In_Language__c = 'English';
        testLead.Email_Opt_In_Method__c = 'Via Email';
        testLead.Email_Pending_Opt_In__c = true;
        testLead.RecordTypeId = recordTypeLead.Id;
        leadList.add(testLead);
        Lead testLead2 = TestDataBuilder.testLead();
        testLead2.Email_Pending_Opt_In__c = false;
        testLead2.RecordTypeId = recordTypeLead.Id;
        leadList.add(testLead2); 
        
        insert TestDataBuilder.testURLExpiryVal();
        
        insert leadList;

        Lead resultLead = [SELECT Email_Double_Opt_In_Subscriber__c FROM Lead where Email_Pending_Opt_In__c = true];
        System.assertEquals(null, resultLead.Email_Double_Opt_In_Subscriber__c);
        
        //Run batch job
        Test.startTest(); 
        BclsHourlyDoubleOptInBatch emailbatch = new BclsHourlyDoubleOptInBatch();
        Id batchInstanceId = Database.executeBatch(emailbatch, 1);
        Test.stopTest();
        
        resultLead = [SELECT Email_Double_Opt_In_Subscriber__c FROM Lead where Email_Pending_Opt_In__c = true];
        
        System.assertNotEquals(null, resultLead.Email_Double_Opt_In_Subscriber__c);
    }
    
    // Test for batch job on Lead
    static testMethod void testGBLeadOptIn() {
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock());

        RecordType recordTypeLead = [SELECT Id FROM RecordType 
            WHERE DeveloperName = 'GB_Leads' and SobjectType = 'Lead' limit 1];

        //Create 2 Test Leads to put trigger in bulk mode
        List<Lead> leadList= new List<Lead>();
        Lead testLead = TestDataBuilder.testLead();
        testLead.Email = 'jane.smith@sundog.net';
        testLead.Email_Opt_In_Language__c = 'English';
        testLead.Email_Opt_In_Method__c = 'Via Email';
        testLead.Email_Pending_Opt_In__c = true;
        testLead.RecordTypeId = recordTypeLead.Id;
        leadList.add(testLead);
        Lead testLead2 = TestDataBuilder.testLead();
        testLead2.Email_Pending_Opt_In__c = false;
        testLead2.RecordTypeId = recordTypeLead.Id;
        leadList.add(testLead2); 
        
        insert TestDataBuilder.testURLExpiryVal();
        
        insert leadList;

        Lead resultLead = [SELECT Email_Double_Opt_In_Subscriber__c FROM Lead where Email_Pending_Opt_In__c = true];
        System.assertEquals(null, resultLead.Email_Double_Opt_In_Subscriber__c);
        
        //Run batch job
        Test.startTest(); 
        BclsHourlyDoubleOptInBatch emailbatch = new BclsHourlyDoubleOptInBatch();
        Id batchInstanceId = Database.executeBatch(emailbatch, 1);
        Test.stopTest();
        
        resultLead = [SELECT Email_Double_Opt_In_Subscriber__c FROM Lead where Email_Pending_Opt_In__c = true];
        
        System.assertNotEquals(null, resultLead.Email_Double_Opt_In_Subscriber__c);
    }
    
    // Test for batch job on Lead
    static testMethod void testIrelandLeadOptIn() {
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock());

        RecordType recordTypeLead = [SELECT Id FROM RecordType 
            WHERE DeveloperName = 'IE_Lead' and SobjectType = 'Lead' limit 1];

        //Create 2 Test Leads to put trigger in bulk mode
        List<Lead> leadList= new List<Lead>();
        Lead testLead = TestDataBuilder.testLead();
        testLead.Email = 'jane.smith@sundog.net';
        testLead.Email_Opt_In_Language__c = 'English';
        testLead.Email_Opt_In_Method__c = 'Via Email';
        testLead.Email_Pending_Opt_In__c = true;
        testLead.RecordTypeId = recordTypeLead.Id;
        leadList.add(testLead);
        Lead testLead2 = TestDataBuilder.testLead();
        testLead2.Email_Pending_Opt_In__c = false;
        testLead2.RecordTypeId = recordTypeLead.Id;
        leadList.add(testLead2); 
        
        insert TestDataBuilder.testURLExpiryVal();
        
        insert leadList;

        Lead resultLead = [SELECT Email_Double_Opt_In_Subscriber__c FROM Lead where Email_Pending_Opt_In__c = true];
        System.assertEquals(null, resultLead.Email_Double_Opt_In_Subscriber__c);
        
        //Run batch job
        Test.startTest(); 
        BclsHourlyDoubleOptInBatch emailbatch = new BclsHourlyDoubleOptInBatch();
        Id batchInstanceId = Database.executeBatch(emailbatch, 1);
        Test.stopTest();
        
        resultLead = [SELECT Email_Double_Opt_In_Subscriber__c FROM Lead where Email_Pending_Opt_In__c = true];
        
        System.assertNotEquals(null, resultLead.Email_Double_Opt_In_Subscriber__c);
    }
    
    static testMethod void testAllLeadsOptIn(){
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock());
        
        RecordType caRecordType = [select Id FROM RecordType 
            where DeveloperName = 'CA_Lead' and SobjectType = 'Lead' limit 1];
        RecordType gbRecordType = [select Id FROM RecordType 
            where DeveloperName = 'GB_Leads' and SobjectType = 'Lead' limit 1];
        RecordType ieRecordType = [select Id FROM RecordType 
            where DeveloperName = 'IE_Lead' and SobjectType = 'Lead' limit 1];
        // test leads
        List<Lead> leadList= new List<Lead>();
        // canada
        Lead caLead = TestDataBuilder.testLead();
        caLead.Email = 'jane.smith@sundog.net';
        caLead.Email_Opt_In_Language__c = 'English';
        caLead.Email_Opt_In_Method__c = 'Via Email';
        caLead.Email_Pending_Opt_In__c = true;
        caLead.RecordTypeId = caRecordType.Id;
        leadList.add(caLead);
        
        //gb
        Lead gbLead = TestDataBuilder.testLead();
        gbLead.Email = 'jane.smith@sundog.net';
        gbLead.Email_Opt_In_Language__c = 'English';
        gbLead.Email_Opt_In_Method__c = 'Via Email';
        gbLead.Email_Pending_Opt_In__c = true;
        gbLead.RecordTypeId = gbRecordType.Id;
        leadList.add(gbLead);
        
        //ireland
        Lead ieLead = TestDataBuilder.testLead();
        ieLead.Email = 'jane.smith@sundog.net';
        ieLead.Email_Opt_In_Language__c = 'English';
        ieLead.Email_Opt_In_Method__c = 'Via Email';
        ieLead.Email_Pending_Opt_In__c = true;
        ieLead.RecordTypeId = ieRecordType.Id;
        leadList.add(ieLead);
        
        insert TestDataBuilder.testURLExpiryVal();
        
        insert leadList;
        
        for(Lead l : [SELECT Email_Double_Opt_In_Subscriber__c FROM Lead where Email_Pending_Opt_In__c = true]){
            System.assertEquals(null, l.Email_Double_Opt_In_Subscriber__c);
        }
        
        
        //Run batch job
        Test.startTest(); 
        BclsHourlyDoubleOptInBatch emailbatch = new BclsHourlyDoubleOptInBatch();
        Id batchInstanceId = Database.executeBatch(emailbatch, 3);
        Test.stopTest();
        
        for(Lead l : [SELECT Email_Double_Opt_In_Subscriber__c FROM Lead where Email_Pending_Opt_In__c = true]){
            System.assertNotEquals(null, l.Email_Double_Opt_In_Subscriber__c);
        }
        
    }
    
    static testMethod void testAllLeadsOptOut(){
        
        RecordType caRecordType = [select Id FROM RecordType 
            where DeveloperName = 'CA_Lead' and SobjectType = 'Lead' limit 1];
        RecordType gbRecordType = [select Id FROM RecordType 
            where DeveloperName = 'GB_Leads' and SobjectType = 'Lead' limit 1];
        RecordType ieRecordType = [select Id FROM RecordType 
            where DeveloperName = 'IE_Lead' and SobjectType = 'Lead' limit 1];
        // test leads
        List<Lead> leadList= new List<Lead>();
        // canada
        Lead caLead = TestDataBuilder.testLead();
        caLead.Email = 'jane.smith@sundog.net';
        caLead.Email_Pending_Opt_Out__c = true;
        caLead.Email_Opt_In_List__c = 'True';
        caLead.Email_Double_Opt_In_Subscriber__c = Datetime.now();
        caLead.Email_Opt_In_Language__c = 'English';
        caLead.Email_Opt_In_Method__c = 'Via Web Form';
        caLead.RecordTypeId = caRecordType.Id;
        leadList.add(caLead);
        
        //gb
        Lead gbLead = TestDataBuilder.testLead();
        gbLead.Email = 'jane.smith@sundog.net';
        gbLead.Email_Pending_Opt_Out__c = true;
        gbLead.Email_Opt_In_List__c = 'True';
        gbLead.Email_Double_Opt_In_Subscriber__c = Datetime.now();
        gbLead.Email_Opt_In_Language__c = 'English';
        gbLead.Email_Opt_In_Method__c = 'Via Web Form';
        gbLead.RecordTypeId = gbRecordType.Id;
        leadList.add(gbLead);
        
        //ireland
        Lead ieLead = TestDataBuilder.testLead();
        ieLead.Email_Pending_Opt_Out__c = true;
        ieLead.Email_Opt_In_List__c = 'True';
        ieLead.Email_Double_Opt_In_Subscriber__c = Datetime.now();
        ieLead.Email_Opt_In_Language__c = 'English';
        ieLead.Email_Opt_In_Method__c = 'Via Web Form';
        ieLead.RecordTypeId = ieRecordType.Id;
        leadList.add(ieLead);
        
        insert TestDataBuilder.testURLExpiryVal();
        
        insert leadList;
        
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock());
        
        //Run batch job
        Test.startTest();
        BclsHourlyDoubleOptInBatch emailbatch = new BclsHourlyDoubleOptInBatch(BclsHourlyDoubleOptInBatch.BatchType.LEAD);
        Id batchInstanceId = Database.executeBatch(emailbatch, 3);
        Test.stopTest();
        
        for(Lead l : [SELECT Email_Double_Opt_In_Subscriber__c, Email_Pending_Opt_Out__c, Email_Opt_In_List__c FROM Lead]){
            System.assertEquals(null, l.Email_Double_Opt_In_Subscriber__c);
            System.assertEquals(false, l.Email_Pending_Opt_Out__c);
            System.assertEquals('False', l.Email_Opt_In_List__c);
        }
    }

    static testMethod void testCanadaLeadOptOut() {
        RecordType recordTypeLead = [SELECT Id FROM RecordType 
            WHERE DeveloperName = 'CA_Lead' and SobjectType = 'Lead' limit 1];

        Lead testLead = TestDataBuilder.testLead();
        testLead.Email = 'jane.smith@sundog.net';
        testLead.Email_Pending_Opt_Out__c = true;
        testLead.Email_Opt_In_List__c = 'True';
        testLead.Email_Double_Opt_In_Subscriber__c = Datetime.now();
        testLead.Email_Opt_In_Language__c = 'English';
        testLead.Email_Opt_In_Method__c = 'Via Web Form';
        testLead.RecordTypeId = recordTypeLead.Id;
        
        insert TestDataBuilder.testURLExpiryVal();
        insert testLead;
        
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock());

        //Run batch job
        Test.startTest();
        BclsHourlyDoubleOptInBatch emailbatch = new BclsHourlyDoubleOptInBatch(BclsHourlyDoubleOptInBatch.BatchType.LEAD);
        Id batchInstanceId = Database.executeBatch(emailbatch, 1);
        Test.stopTest();

        Lead resultLead = [SELECT Email_Double_Opt_In_Subscriber__c, Email_Pending_Opt_Out__c, Email_Opt_In_List__c FROM Lead where Id = :testLead.Id];
        
        System.assertEquals(null, resultLead.Email_Double_Opt_In_Subscriber__c);
        System.assertEquals(false, resultLead.Email_Pending_Opt_Out__c);
        System.assertEquals('False', resultLead.Email_Opt_In_List__c);
    }
    
    static testMethod void testGBLeadOptOut() {
        RecordType recordTypeLead = [SELECT Id FROM RecordType 
            WHERE DeveloperName = 'GB_Leads' and SobjectType = 'Lead' limit 1];

        Lead testLead = TestDataBuilder.testLead();
        testLead.Email = 'jane.smith@sundog.net';
        testLead.Email_Pending_Opt_Out__c = true;
        testLead.Email_Opt_In_List__c = 'True';
        testLead.Email_Double_Opt_In_Subscriber__c = Datetime.now();
        testLead.Email_Opt_In_Language__c = 'English';
        testLead.Email_Opt_In_Method__c = 'Via Web Form';
        testLead.RecordTypeId = recordTypeLead.Id;
        
        insert TestDataBuilder.testURLExpiryVal();
        insert testLead;
        
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock());

        //Run batch job
        Test.startTest();
        BclsHourlyDoubleOptInBatch emailbatch = new BclsHourlyDoubleOptInBatch(BclsHourlyDoubleOptInBatch.BatchType.LEAD);
        Id batchInstanceId = Database.executeBatch(emailbatch, 1);
        Test.stopTest();

        Lead resultLead = [SELECT Email_Double_Opt_In_Subscriber__c, Email_Pending_Opt_Out__c, Email_Opt_In_List__c FROM Lead where Id = :testLead.Id];
        
        System.assertEquals(null, resultLead.Email_Double_Opt_In_Subscriber__c);
        System.assertEquals(false, resultLead.Email_Pending_Opt_Out__c);
        System.assertEquals('False', resultLead.Email_Opt_In_List__c);
    }
    
    static testMethod void testIrelandLeadOptOut() {
        RecordType recordTypeLead = [SELECT Id FROM RecordType 
            WHERE DeveloperName = 'IE_Lead' and SobjectType = 'Lead' limit 1];

        Lead testLead = TestDataBuilder.testLead();
        testLead.Email = 'jane.smith@sundog.net';
        testLead.Email_Pending_Opt_Out__c = true;
        testLead.Email_Opt_In_List__c = 'True';
        testLead.Email_Double_Opt_In_Subscriber__c = Datetime.now();
        testLead.Email_Opt_In_Language__c = 'English';
        testLead.Email_Opt_In_Method__c = 'Via Web Form';
        testLead.RecordTypeId = recordTypeLead.Id;
        
        insert TestDataBuilder.testURLExpiryVal();
        insert testLead;
        
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock());

        //Run batch job
        Test.startTest();
        BclsHourlyDoubleOptInBatch emailbatch = new BclsHourlyDoubleOptInBatch(BclsHourlyDoubleOptInBatch.BatchType.LEAD);
        Id batchInstanceId = Database.executeBatch(emailbatch, 1);
        Test.stopTest();

        Lead resultLead = [SELECT Email_Double_Opt_In_Subscriber__c, Email_Pending_Opt_Out__c, Email_Opt_In_List__c FROM Lead where Id = :testLead.Id];
        
        System.assertEquals(null, resultLead.Email_Double_Opt_In_Subscriber__c);
        System.assertEquals(false, resultLead.Email_Pending_Opt_Out__c);
        System.assertEquals('False', resultLead.Email_Opt_In_List__c);
    }
}