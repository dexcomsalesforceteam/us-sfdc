/***********************************************************************************************************************
@Author        : Jagan Periyakaruppan
@Date Modified    : 12/19/2017
@Description    : Added static methods runBeforeTriggerOnce,  runAfterTriggerOnce to be used in OpportunityTrigger
************************************************************************************************************************/
public class UtilityClass {
    public static Boolean runTrigger = true;
    public static Boolean runOrderHeaderTrigger = true;//Used to control trigger execution for OrderHeader Object to run only once
    public static Boolean runAccountTrigger = true;//Used to control trigger execution for Account Object to run only once
    public static Boolean runBeforeTrigger = true;
    public static Boolean runAfterTrigger = true;
    public static Boolean runBenefitBeforeTrigger = true;//Controls the execution of Before trigger logic in Benefit
    public static Boolean runBenefitAfterTrigger = true;////Controls the execution of After trigger logic in Benefit
    public static Boolean executeAccountTrigger = true;//Used to control trigger execution for Account Object 
    public static Boolean executeAccountBeforeTrigger = true;
    public static Boolean executeAccountAfterTrigger = true;
    public static Boolean executeOrderHeaderBeforeTrigger = true;//Controls the execution of Before trigger logic in Order Header
    public static Boolean executeOrderHeaderAfterTrigger = true;//Controls the execution of After trigger logic in Order Header
    public static Boolean executeOrderHeaderBeforeUpdateTrigger = ClsApexConstants.BOOLEAN_TRUE; // Controls the before update execution in Order header Trigger
    
    
    
    public static Boolean runOnce(){
        if(runTrigger == true){
            runTrigger = false;
            return true;
        }else{
            return runTrigger;
        }
    }
    
    public static Boolean runBeforeTriggerOnce(){
        if(runBeforeTrigger == true){
            runBeforeTrigger = false;
            return true;
        }else{
            return runBeforeTrigger;
        }
    }
    public static Boolean runBenefitBeforeTriggerOnce(){
        if(runBenefitBeforeTrigger == true){
            runBenefitBeforeTrigger = false;
            return true;
        }else{
            return runBenefitBeforeTrigger;
        }
    }
    
    public static Boolean runAfterTriggerOnce(){
        if(runAfterTrigger == true){
            runAfterTrigger = false;
            return true;
        }else{
            return runAfterTrigger;
        }
    }
    
    public static Boolean runBenefitAfterTriggerOnce(){
        if(runBenefitAfterTrigger == true){
            runBenefitAfterTrigger = false;
            return true;
        }else{
            return runBenefitAfterTrigger;
        }
    }
    //Method used in Order Header object to run only once
    public static Boolean runOrderHeaderTriggerOnce(){
        if(runOrderHeaderTrigger == true){
            runOrderHeaderTrigger = false;
            return true;
        }else{
            return runOrderHeaderTrigger;
        }
    }
    //Method used in Account object to run only once
    public static Boolean runAccountTriggerOnce(){
        if(runAccountTrigger == true){
            runAccountTrigger = false;
            return true;
        }else{
            return runAccountTrigger;
        }
    }
    
    //Method used in account trigger to run before trigger once
    public static Boolean runAccountBeforeTriggerOnce(){
        if(executeAccountBeforeTrigger){
            executeAccountBeforeTrigger = false;
            return true;
        }else{
            return executeAccountBeforeTrigger;
        }
    }
    
    //method to run account trigger to run after trigger once
    public static Boolean runAccountAfterTriggerOnce(){
        if(executeAccountAfterTrigger){
            executeAccountAfterTrigger = false;
            return true;
        }else{
            return executeAccountAfterTrigger;
        }
    }
    
    //method to run Order Header trigger to run after trigger once
    public static Boolean runOrderHeaderBeforeTriggerOnce(){
        if(executeOrderHeaderBeforeTrigger){
            executeOrderHeaderBeforeTrigger = false;
            return true;
        }else{
            return executeOrderHeaderBeforeTrigger;
        }
    }
    
    //method to run Order Header trigger before update event only once
    public static Boolean runOrderHeaderBeforeUpdateTriggerOnce(){
        if(executeOrderHeaderBeforeUpdateTrigger){
            executeOrderHeaderBeforeUpdateTrigger = ClsApexConstants.BOOLEAN_FALSE;
            return true;
        }else{
            return executeOrderHeaderBeforeUpdateTrigger;
        }
    }
    
    //method to run Order Header trigger to run after trigger once
    public static Boolean runOrderHeaderAfterTriggerOnce(){
        if(executeOrderHeaderAfterTrigger){
            executeOrderHeaderAfterTrigger = false;
            return true;
        }else{
            return executeOrderHeaderAfterTrigger;
        }
    }
    
    //CRMSF-5330 : Sudheer Vemula 5/8/2020 : Validate Duplicate Products on Order , Opportunity and MFR objects level.
    public static void validateDupProducts(List<SObject> newLineItemsList,set<id> parentIds){
        String mfpObjApiName = ClsApexConstants.MFP_OBJ_API_NAME;
        String orderItemObjApiName = ClsApexConstants.ORD_ITEM_FIELD_API_NAME;
        String opptyItemObjApiName = ClsApexConstants.OPPTY_ITEM_FIELD_API_NAME;
        
        String mdcrFieldApiName = ClsApexConstants.MDCR_FIELD_API_NAME;
        String orderFieldApiName = ClsApexConstants.ORDER_FIELD_API_NAME;
        String opptyFieldApiName = ClsApexConstants.OPPTY_FIELD_API_NAME;
        String mdcrOrOrderProductApiName = ClsApexConstants.MDCR_OR_ORDER_PRODUCT_API_NAME;
        String opptyProductCodeApiName = ClsApexConstants.OPPTY_PRODUCT_API_NAME;
        
        String mfp_Soql_Query_String = ClsApexConstants.MFP_SOQL_QUERY_STRING;
        String order_Item_Soql_Query_String = ClsApexConstants.ORDER_ITEM_SOQL_QUERY_STRING;
        String oppty_Item_Soql_Query_String = ClsApexConstants.OPPTY_ITEM_SOQL_QUERY_STRING;
        String dup_Error_Mssg = ClsApexConstants.dup_ERROR_MSSG;
        Map<string,List<string>> oldProductsMap = new Map<string,List<string>>();
        
        SobjectType objName = newLineItemsList.getSobjectType();
        string objectName = string.valueOf(objName);
        
        string  query = objectName == opptyItemObjApiName? OPPTY_ITEM_SOQL_QUERY_STRING : (objectName == orderItemObjApiName)?ORDER_ITEM_SOQL_QUERY_STRING:(objectName == mfpObjApiName)?MFP_SOQL_QUERY_STRING:'';
         List<sobject> oldLineItemsList = database.query(query);
        
        // system.debug('stype..'+objectName);
       
        for(sobject oldLineItem:oldLineItemsList){
            //system.debug('oldLineItem record...'+oldLineItem);
            // system.debug('oldLineItem.get(opportunityId)...'+oldLineItem.get('opportunityId'));
           string productName = objectName == opptyItemObjApiName?string.valueof(oldLineItem.get(opptyProductCodeApiName)):(objectName == orderItemObjApiName) || (objectName == mfpObjApiName)?string.valueof(oldLineItem.get(mdcrOrOrderProductApiName)):'';
           string parentObjectId = objectName == opptyItemObjApiName?string.valueof(oldLineItem.get(opptyFieldApiName)):(objectName == orderItemObjApiName)?string.valueof(oldLineItem.get(orderFieldApiName)):(objectName == mfpObjApiName)?string.valueof(oldLineItem.get(mdcrFieldApiName)):'';
            
            if(!oldProductsMap.containskey(parentObjectId)){
                oldProductsMap.put(parentObjectId,new list<string>{productName});
            }
            else{
                oldProductsMap.get(parentObjectId).add(productName);
            }
        }
        system.debug('oldProductsMap..'+oldProductsMap);
        
        for(sobject newProduct:newLineItemsList){
            string productName = objectName == opptyItemObjApiName?string.valueof(newProduct.get(opptyProductCodeApiName)):(objectName == orderItemObjApiName) || (objectName == mfpObjApiName)?string.valueof(newProduct.get(mdcrOrOrderProductApiName)):'';
            string parentObjectId = objectName == opptyItemObjApiName?string.valueof(newProduct.get(opptyFieldApiName)):(objectName == orderItemObjApiName)?string.valueof(newProduct.get(orderFieldApiName)):(objectName == mfpObjApiName)?string.valueof(newProduct.get(mdcrFieldApiName)):'';
            
            if(oldProductsMap.containskey(parentObjectId) && oldProductsMap.get(parentObjectId).contains(productName)){
                if(objectName == orderItemObjApiName){
                OrderItem ss= (OrderItem)newProduct;
                if(productName != null && !test.isRunningTest())    
                ss.quantity.addError(dup_Error_Mssg.replace('productName',productName));
                }
                else{
                    if(productName != null && !test.isRunningTest())
                newProduct.addError(dup_Error_Mssg.replace('productName',productName));
                }
            }
        }
        
    }
}