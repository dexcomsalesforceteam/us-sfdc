/**********************************************
 * @author      : Sundog Interactive
 * @date        : JAN 25, 2019
 * @description : Helper class to PrescriberDashboardCtl and FieldSalesDashboardCtl
**********************************************/
public class ClsPrescriberDashboardHelper {
    
    private static Map<String, Datetime> expToDatetimeMap = new Map<String, Datetime>();
    private static Map<String, Date> expToDateMap = new Map<String, Date>();

    private static void setupDatetimes(){
        
        expToDateMap.put('past-3-months', Date.today().addMonths(-3));
        expToDateMap.put('past-7-months', Date.today().addMonths(-7));
        expToDateMap.put('past-12-months', Date.today().addMonths(-12));
        
        expToDatetimeMap.put('past-3-months', Datetime.now().addMonths(-3));
        expToDatetimeMap.put('past-7-months', Datetime.now().addMonths(-7));
        expToDatetimeMap.put('past-12-months', Datetime.now().addMonths(-12));
        
    }
    /**********************************************************
     **Description: Retrieves certain opportunity and order header records under a certain prescriber Account
     **Parameters:  prescriber Account related to running user
     **Returns:     Account information, related opportunities and order headers in form of custom wrapper object PrescriberDashboardHelper.OpportunitiesAndOrders
     **Author:      Sundog Interactive
     **Date Created:   JAN 25, 2019
    **********************************************************/
    public static OpportunitiesAndOrders getOpportunitiesAndOrders(List<Account> prescribers, String dateRange){
        if(expToDatetimeMap.isEmpty()){
            setupDatetimes();
        }
        // create map of dateRange strings to Datetime
        // incorporate new generated Datetime into query
        // modify the client side initialization as needed
        // write dateRangeChange methods for dateRange filter change on client side
        OpportunitiesAndOrders result = new OpportunitiesAndOrders();

        result.opportunities = [SELECT Id, Name, DME_Distributor__c, DME_Distributor__r.Phone, DME_Distributor__r.Name, DME_Distributor__r.Reorder_Phone_Number__c, 
                AccountId, Opp_Territory_ID__c, Patient_First_Name__c, Patient_Last_Name__c, Payor__r.Name, Account.DOB__c, Doc_1_Status__c, Doc_2_Status__c,  
                HasAllDocs__c, StageName, Sales_Channel__c, CreatedDate, Status__c, Close_Reason__c, Type, Order_NUM__c, Prescribers__c 
                FROM Opportunity 
                //WHERE (Sales_Channel__c = 'Dexcom' OR Sales_Channel__c = 'Pharmacy' OR Sales_Channel__c = 'Distributor')
                WHERE (StageName = '1. New Opportunity' OR StageName = '2. Verification' OR StageName = '3. Referred to Channel Partner' OR StageName = '4. Doc Collection'
                     OR StageName = '5. Prior-Authorization' OR StageName = '6. Quote Pending' OR StageName = '13. Pharma Retail' OR StageName = '10. Cancelled' OR StageName = '61. Quote Approved')
                //AND (CreatedDate >= :Datetime.now().addYears(-1))
                AND (CreatedDate >= :expToDatetimeMap.get(dateRange))
                AND Close_Reason__c != 'Duplicate Opportunity'
                AND Prescribers__c = :prescribers];

        //order number will not exist until StageName = '61. Quote Approved'
        Set<String> orderNumbers = new Set<String>();
        for (Opportunity o : result.opportunities) {
            if (String.isNotBlank(o.Order_NUM__c)){
                orderNumbers.add(o.Order_NUM__c);
            }
        }

        for (Order_Header__c oh : [SELECT Id, Order_ID__c, Calculated_Shipping_Date__c
                                   FROM Order_Header__c WHERE Order_ID__c = :orderNumbers and Status__c = 'SHIPPED']) {
                                       result.orderHeaderMap.put(oh.Order_ID__c, oh.Calculated_Shipping_Date__c);
                                       
                                   }
        System.debug('UserInfo.getUserId():' + UserInfo.getUserId());

        //we want to return qualified orders not tied to any opportunity
        Set<Id> prescriberIds = new Set<Id>();
        for (Account a : prescribers) {
            prescriberIds.add(a.Id);
        }
        System.debug('prescriberIds');
        System.debug(prescriberIds);
        System.debug(Date.today().addYears(-1));
        System.debug(orderNumbers);
        //pulled out of Order_Header query for better performance
        Set<Id> patientIds = new Set<Id>();
        for (Account a : [select Id from Account where Prescribers__c = :prescribers]) {
            patientIds.add(a.Id);
        }
        System.debug(patientIds);

        result.orders = [SELECT Id, Name, Status__c, Account__r.Name, Account__r.FirstName, Account__r.LastName, Account__r.DOB__c, Account__r.Payor__r.Name, Booked_Date__c,
            Account__r.Prescribers__c, Calculated_Shipping_Date__c, Order_ID__c
            FROM Order_Header__c WHERE (Booked_Date__c >= :expToDateMap.get(dateRange)) AND Account__c = :patientIds AND Order_ID__c != :orderNumbers AND Order_Type__c != 'Tech Replacement Order'];

        for(Order_Header__c oh : result.orders){
            if(oh.Status__c == 'SHIPPED' && oh.Order_ID__c != null && oh.Calculated_Shipping_Date__c != null){
                result.orderHeaderMap.put(oh.Order_ID__c, oh.Calculated_Shipping_Date__c);
            }
        }

        return result;
    }
    /**********************************************************
     **Description: Wrapper class for user, account, related prescriber, order header records and opportunity records related to the running user
     **Parameters:  None
     **Returns:     Wrapper class object
     **Author:      Sundog Interactive
     **Date Created:   JAN 25, 2019
    **********************************************************/
    public class OpportunitiesAndOrders {
        public OpportunitiesAndOrders(){
            opportunities = new List<Opportunity>();
            orders = new List<Order_Header__c>();
            orderHeaderMap = new Map<String, Date>();
        }

        @AuraEnabled
        public Boolean registered {get;set;}
        @AuraEnabled
        public Boolean inviteSent {get;set;}
        @AuraEnabled
        public Boolean sendInvite {get;set;}
        @AuraEnabled
        public User user {get;set;}
        @AuraEnabled
        public Account account {get;set;}
        @AuraEnabled
        public Map<String, Date> orderHeaderMap {get;set;}
        @AuraEnabled
        public List<Opportunity> opportunities {get;set;}
        @AuraEnabled
        public List<Order_Header__c> orders {get;set;}
    }
}