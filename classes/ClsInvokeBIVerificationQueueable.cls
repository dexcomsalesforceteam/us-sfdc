/*************************************************************************************************
@Author      - LTI
@Date        - 11/04/2019
@Description - QUEUEABLE class invoke BI Verification.

*************************************************************************************************/
global class ClsInvokeBIVerificationQueueable implements Queueable{
    List<Benefits__c> benefitList = new List<Benefits__c>();
    Map<Id, Benefits__c> benefitMap = new Map<id, Benefits__c>();
    private string invokeForm; //Added for #CRMSF-4614
     
    public ClsInvokeBIVerificationQueueable (List<Benefits__c> benefitList,string invokeFormValue) {
            for(Benefits__c thisBenefit : benefitList){
                benefitMap.put(thisBenefit.Id, thisBenefit);
            } 
            
            this.invokeForm = invokeFormValue;
            
        }
    
    public void execute(QueueableContext context) {
             System.debug('***benefitList :: ' + benefitList);
             if(!benefitMap.isEmpty()){
                 try{
                       for(Benefits__c thisBenefit : benefitMap.Values()){
                            thisBenefit.Invoke_BI_Verification__c = true;
                            //thisBenefit.BI_Request_Invoked_From__c = 'SSIP';
                            thisBenefit.BI_Request_Invoked_From__c = invokeForm; //Added for #CRMSF-4614
                            benefitList.add(thisBenefit);
                            }
                        
                        System.debug('*********process BI Verification ****************');
                        if(!benefitList.isEmpty())
                            Database.update(benefitList, false);
                        System.debug('***after*****benefitList****************'+benefitList);

                    }catch(Exception ex){
                        //Integer numErrors = ex.getNumDml();
                       /* System.debug('getNumDml=' + numErrors);
                        for(Integer i=0;i<numErrors;i++) {
                            System.debug('getDmlFieldNames=' + ex.getDmlFieldNames(i));
                            System.debug('getDmlMessage=' + ex.getDmlMessage(i));
                        } */
                        System.debug('************SSIP Queueable failed ****************' + ex);
                    } 
             
             }      
        }  

}