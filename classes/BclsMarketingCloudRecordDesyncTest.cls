/**
 * BclsMarketingCloudRecordDesyncTest
 * Test class for BclsMarketingCloudRecordDesync
 * @author Kristen(Sundog)
 * @date 07/15/2019
 */
@isTest
private with sharing class BclsMarketingCloudRecordDesyncTest {
    
    private static Id prescriberLeadRTId = [SELECT Id FROM RecordType WHERE Name = 'Prescriber' AND SObjectType = 'Lead'].Id;
    private static Id consumersLeadRTId = [SELECT Id FROM RecordType WHERE Name = 'Consumer' AND SObjectType = 'Lead'].Id;
    private static Id prescriberAccountRTId = [SELECT Id FROM RecordType WHERE Name = 'Prescriber' AND SObjectType = 'Account'].Id;
    private static Id consumersAccountRTId = [SELECT Id FROM RecordType WHERE Name = 'Consumers' AND SObjectType = 'Account'].Id;
    
    @testSetup
    private static void setup(){
        insert TestDataBuilder.testURLExpiryVal();
    } 
    
    static testMethod void testScheduledJob() {
        Test.startTest();
        String jobId = System.schedule('SclsMarketingCloudRecordDesyncTest', '0 0 0 3 9 ? 2052', new SclsMarketingCloudRecordDesync());
        Test.stopTest();

        System.assertEquals(false, String.isBlank(jobId));
    }
    
    @isTest
    // Test for lead record that does not meet criteria to be desynchronized with Marketing Cloud.
    // Reason: Record Type must be 'Consumer'.  This will not get desynced because it is converted.
    //  We do not touch converted leads
    private static void negativeLeadTest1(){
        Lead l = new Lead(
            LastName = 'Testerson',
            RecordTypeId = prescriberLeadRTId,
            HasOptedOutOfEmail = false,
            email = 'testEmail@testEmail.com'
        );

        // insert, set CreatedDate to 25 months ago
        insert l;
        Test.setCreatedDate(l.Id, Date.today().addMonths(-25));

        // convert lead
        Database.LeadConvert lc = new Database.LeadConvert();
		lc.setLeadId(l.Id);
        LeadStatus convertStatus = [SELECT MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
        lc.setConvertedStatus(convertStatus.MasterLabel);
        Database.LeadConvertResult lcr = Database.convertLead(lc);
		System.assert(lcr.isSuccess());
        
		// run batch
        Test.startTest();
        Database.executeBatch(new BclsMarketingCloudRecordDesync());
        Test.stopTest();

        // assert record was processed as expected
        l = [SELECT Desynchronize_With_Marketing_Cloud__c FROM Lead WHERE Id = :l.Id];
        System.assertEquals(false, l.Desynchronize_With_Marketing_Cloud__c);

    }
    
    @isTest
    // Test for lead record that does not meet criteria to be desynchronized with Marketing Cloud.
    // Reason: Record Type must be 'Consumer'.  This will now get desynced because it is not converted
    private static void negativeLeadTest1b(){
        Lead l = new Lead(
            LastName = 'Testerson',
            RecordTypeId = prescriberLeadRTId,
            HasOptedOutOfEmail = false,
            email = 'testEmail@testEmail.com'
        );

        // insert, set CreatedDate to 25 months ago
        insert l;
        Test.setCreatedDate(l.Id, Date.today().addMonths(-25));
        
		// run batch
        Test.startTest();
        Database.executeBatch(new BclsMarketingCloudRecordDesync());
        Test.stopTest();

        // assert record was processed as expected
        l = [SELECT Desynchronize_With_Marketing_Cloud__c FROM Lead WHERE Id = :l.Id];
        System.assertEquals(true, l.Desynchronize_With_Marketing_Cloud__c);

    }

    @isTest
    // Test for lead record that does not meet criteria to be desynchronized with Marketing Cloud.
    // Reason: HasOptedOutOfEmail must be false
    private static void negativeLeadTest2(){
        Lead l = new Lead(
            LastName = 'Testerson',
            RecordTypeId = consumersLeadRTId,
            HasOptedOutOfEmail = true,
            email = 'testEmail@testEmail.com'
        );

        // insert, set CreatedDate to 25 months ago
        insert l;
        Test.setCreatedDate(l.Id, Date.today().addMonths(-25));
 
        // convert lead
        Database.LeadConvert lc = new Database.LeadConvert();
		lc.setLeadId(l.Id);
        LeadStatus convertStatus = [SELECT MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
        lc.setConvertedStatus(convertStatus.MasterLabel);
        Database.LeadConvertResult lcr = Database.convertLead(lc);
		System.assert(lcr.isSuccess());
        
		// run batch
        Test.startTest();
        Database.executeBatch(new BclsMarketingCloudRecordDesync());
        Test.stopTest();

        // assert record was processed as expected
        l = [SELECT Desynchronize_With_Marketing_Cloud__c FROM Lead WHERE Id = :l.Id];
        System.assertEquals(false, l.Desynchronize_With_Marketing_Cloud__c);

    }

    @isTest
    // Test for lead record that does not meet criteria to be desynchronized with Marketing Cloud.
    // Reason: Email must be null OR IsConverted more than 31+ days past OR CreatedDate must be more than 25+ months past
    private static void negativeLeadTest3(){
        Lead l = new Lead(
            LastName = 'Testerson',
            RecordTypeId = consumersLeadRTId,
            HasOptedOutOfEmail = false,
            Email = 'testerson@test.com',
            IsConverted = false
        );

        // insert, set CreatedDate to 24 months ago
        insert l;
        Test.setCreatedDate(l.Id, Date.today().addMonths(-24));
        
        // run batch
        Test.startTest();
        Database.executeBatch(new BclsMarketingCloudRecordDesync());
        Test.stopTest();
		
        // assert record was processed as expected
        l = [SELECT Desynchronize_With_Marketing_Cloud__c FROM Lead WHERE Id = :l.Id];
        System.assertEquals(false, l.Desynchronize_With_Marketing_Cloud__c);
    }

    @isTest
    // Test for lead record that meets criteria to be desynchronized with Marketing Cloud.
    // Reason: Record Type is 'Consumer', HasOptedOutOfEmail is false, Email is null
    private static void positiveLeadTest1(){
        Lead l = new Lead(
            LastName = 'Testerson',
            RecordTypeId = consumersLeadRTId,
            HasOptedOutOfEmail = false,
            email = 'testEmail@testEmail.com'
        );

        // insert, set CreatedDate to 25 months ago
        insert l;
        Test.setCreatedDate(l.Id, Datetime.now().addMonths(-25));
        
        // run batch
        Test.startTest();
        Database.executeBatch(new BclsMarketingCloudRecordDesync());
        Test.stopTest();
		
        // assert record was processed as expected
        l = [SELECT Desynchronize_With_Marketing_Cloud__c FROM Lead WHERE Id = :l.Id];
        System.assertEquals(true, l.Desynchronize_With_Marketing_Cloud__c);
    }

    @isTest
    // Test for lead record that meets criteria to be desynchronized with Marketing Cloud.
    // Reason: Record Type is 'Consumer', HasOptedOutOfEmail is false, CreatedDate more than 25+ months past
    private static void positiveLeadTest2(){
        Lead l = new Lead(
            LastName = 'Testerson',
            RecordTypeId = consumersLeadRTId,
            HasOptedOutOfEmail = false,
            Email = 'testerson@test.com'
        );

        // insert, set CreatedDate to 25 months ago
        insert l;
        Test.setCreatedDate(l.Id, Datetime.now().addMonths(-25));

        // convert lead
        Database.LeadConvert lc = new Database.LeadConvert();
		lc.setLeadId(l.Id);
        LeadStatus convertStatus = [SELECT MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
        lc.setConvertedStatus(convertStatus.MasterLabel);
        Database.LeadConvertResult lcr = Database.convertLead(lc);
		System.assert(lcr.isSuccess());
        
		// run batch
        Test.startTest();
        Database.executeBatch(new BclsMarketingCloudRecordDesync());
        Test.stopTest();
		
        // assert record was processed as expected
        l = [SELECT Desynchronize_With_Marketing_Cloud__c FROM Lead WHERE Id = :l.Id];
        System.assertEquals(true, l.Desynchronize_With_Marketing_Cloud__c);
    }


    @isTest
    // Test for account record that does not meet criteria to be desynchronized with Marketing Cloud.
    // Reason: Record Type must be 'Consumers'
    private static void negativeAccountTest1(){
        Account a = new Account(
            FirstName = 'Lucy',
            LastName = 'Testerson',
            RecordTypeId = prescriberAccountRTId,
            PersonHasOptedOutOfEmail = false
            //SMS_Pending_Opt_In__c = false,
            //SMS_Opt_In_List__c = 'False',
            //SMS_Opt_Out_List__c = 'False',
        );
        
        // insert, set CreatedDate to 25 months ago
        insert a;
        Test.setCreatedDate(a.Id, Datetime.now().addMonths(-25));
        
        // create records to populate Shipping_Date__c
        a = [SELECT Id, CreatedDate FROM Account WHERE Id = :a.Id];
        generateShippingDateRecords(a);
        System.assert([SELECT Shipping_Date__c FROM Account WHERE Id = :a.Id].Shipping_Date__c != null);
        
        // run batch 
        Test.startTest();
        Database.executeBatch(new BclsMarketingCloudRecordDesync(BclsMarketingCloudRecordDesync.BatchType.ACCOUNT2));
        Test.stopTest();
        
        // assert record was processed as expected
        a = [SELECT Desynchronize_With_Marketing_Cloud__pc FROM Account WHERE Id = :a.Id];
        //The Account2 batch job will not pick this one up.
        System.assertEquals(false, a.Desynchronize_With_Marketing_Cloud__pc);
    }
    
    @isTest
    // Test for account record that is not 'Consumers' so should be desynchronized with Marketing Cloud.
    // Reason: Record Type must be 'Consumers'
    private static void negativeAccountTestNonConsumer(){
        Account a = new Account(
            FirstName = 'Lucy',
            LastName = 'Testerson',
            RecordTypeId = prescriberAccountRTId,
            PersonHasOptedOutOfEmail = false,
            Desynchronize_With_Marketing_Cloud__pc = false
            //SMS_Pending_Opt_In__c = false,
            //SMS_Opt_In_List__c = 'False',
            //SMS_Opt_Out_List__c = 'False',
        );
        
        // insert, set CreatedDate to 25 months ago
        insert a;
                
        // run batch 
        Test.startTest();
        Database.executeBatch(new BclsMarketingCloudRecordDesync(BclsMarketingCloudRecordDesync.BatchType.ACCOUNT1));
        Test.stopTest();
        
        // assert record was processed as expected
        a = [SELECT Desynchronize_With_Marketing_Cloud__pc FROM Account WHERE Id = :a.Id];
        System.assertEquals(true, a.Desynchronize_With_Marketing_Cloud__pc);
    }

    @isTest
    // Test for account record that does not meet criteria to be desynchronized with Marketing Cloud.
    // Reason: PersonHasOptedOutOfEmail must be false
    private static void negativeAccountTest2(){
        Account a = new Account(
            FirstName = 'Lucy',
            LastName = 'Testerson',
            RecordTypeId = consumersAccountRTId,
            PersonHasOptedOutOfEmail = true
            //SMS_Pending_Opt_In__c = false,
            //SMS_Opt_In_List__c = 'False',
            //SMS_Opt_Out_List__c = 'False',
        );
        
        // insert, set CreatedDate to 25 months ago
        insert a;
        Test.setCreatedDate(a.Id, Datetime.now().addMonths(-25));
        
        // create records to populate Shipping_Date__c
        a = [SELECT Id, CreatedDate FROM Account WHERE Id = :a.Id];
        generateShippingDateRecords(a);
        
        // run batch 
        Test.startTest();
        Database.executeBatch(new BclsMarketingCloudRecordDesync(BclsMarketingCloudRecordDesync.BatchType.ACCOUNT2));
        Test.stopTest();
        
        // assert record was processed as expected
        a = [SELECT Desynchronize_With_Marketing_Cloud__pc FROM Account WHERE Id = :a.Id];
        System.assertEquals(false, a.Desynchronize_With_Marketing_Cloud__pc);
    }

    @isTest
    // Test for account record that does not meet criteria to be desynchronized with Marketing Cloud.
    // Reason: SMS_Pending_Opt_In__c must be false
    private static void negativeAccountTest3(){
        Account a = new Account(
            FirstName = 'Lucy',
            LastName = 'Testerson',
            RecordTypeId = consumersAccountRTId,
            PersonHasOptedOutOfEmail = false,
            SMS_Pending_Opt_In__c = true,
            SMS_Opt_In_List__c = 'False',
            SMS_Opt_Out_List__c = 'False'
        );
        
        // insert, set CreatedDate to 25 months ago
        insert a; //In this insert SMS_Pending_Opt_In__c is being set to false
        Test.setCreatedDate(a.Id, Datetime.now().addMonths(-25));
        
        // create records to populate Shipping_Date__c
        a = [SELECT Id, CreatedDate, SMS_Pending_Opt_In__c FROM Account WHERE Id = :a.Id];
        a.SMS_Pending_Opt_In__c = true;
        update a;
        
        a = [SELECT Id, CreatedDate, SMS_Pending_Opt_In__c FROM Account WHERE Id = :a.Id];
        System.assertEquals(true, a.SMS_Pending_Opt_In__c);
        generateShippingDateRecords(a);
        
        // run batch 
        Test.startTest();
        Database.executeBatch(new BclsMarketingCloudRecordDesync(BclsMarketingCloudRecordDesync.BatchType.ACCOUNT2));
        Test.stopTest();
        
        // assert record was processed as expected
        a = [SELECT Desynchronize_With_Marketing_Cloud__pc FROM Account WHERE Id = :a.Id];
        System.assertEquals(false, a.Desynchronize_With_Marketing_Cloud__pc);
    }

    @isTest
    // Test for account record that does not meet criteria to be desynchronized with Marketing Cloud.
    // Reason: SMS_Opt_In_List__c must not be 'True'
    private static void negativeAccountTest4(){
        Account a = new Account(
            FirstName = 'Lucy',
            LastName = 'Testerson',
            RecordTypeId = consumersAccountRTId,
            PersonHasOptedOutOfEmail = false,
            //SMS_Pending_Opt_In__c = false,
            SMS_Opt_In_List__c = 'True'
            //SMS_Opt_Out_List__c = 'False'
        );
        
        // insert, set CreatedDate to 25 months ago
        insert a;
        Test.setCreatedDate(a.Id, Datetime.now().addMonths(-25));
        
        // create records to populate Shipping_Date__c
        a = [SELECT Id, CreatedDate FROM Account WHERE Id = :a.Id];
        generateShippingDateRecords(a);
        
        // run batch 
        Test.startTest();
        Database.executeBatch(new BclsMarketingCloudRecordDesync(BclsMarketingCloudRecordDesync.BatchType.ACCOUNT2));
        Test.stopTest();
        
        // assert record was processed as expected
        a = [SELECT Desynchronize_With_Marketing_Cloud__pc FROM Account WHERE Id = :a.Id];
        System.assertEquals(false, a.Desynchronize_With_Marketing_Cloud__pc);
    }
    
    @isTest
    // Test for account record that does not meet criteria to be desynchronized with Marketing Cloud.
    // Reason: SMS_Opt_Out_List__c must not be 'True'
    private static void negativeAccountTest5(){
        Account a = new Account(
            FirstName = 'Lucy',
            LastName = 'Testerson',
            RecordTypeId = consumersAccountRTId,
            PersonHasOptedOutOfEmail = false,
            //SMS_Pending_Opt_In__c = false,
            //SMS_Opt_In_List__c = 'False',
            SMS_Opt_Out_List__c = 'True'
        );
        
        // insert, set CreatedDate to 25 months ago
        insert a;
        Test.setCreatedDate(a.Id, Datetime.now().addMonths(-25));
        
        // create records to populate Shipping_Date__c
        a = [SELECT Id, CreatedDate FROM Account WHERE Id = :a.Id];
        generateShippingDateRecords(a);
        
        // run batch 
        Test.startTest();
        Database.executeBatch(new BclsMarketingCloudRecordDesync(BclsMarketingCloudRecordDesync.BatchType.ACCOUNT2));
        Test.stopTest();
        
        // assert record was processed as expected
        a = [SELECT Desynchronize_With_Marketing_Cloud__pc FROM Account WHERE Id = :a.Id];
        System.assertEquals(false, a.Desynchronize_With_Marketing_Cloud__pc);
    }

    @isTest
    // Test for account record that does not meet criteria to be desynchronized with Marketing Cloud.
    // Reason: if Shipping_Date__c is not null and PersonEmail is not null, Shipping_Date__c must be more than 23 months past
    private static void negativeAccountTest6(){
        Account a = new Account(
            FirstName = 'Lucy',
            LastName = 'Testerson',
            RecordTypeId = consumersAccountRTId,
            PersonHasOptedOutOfEmail = false,
            //SMS_Pending_Opt_In__c = false,
            //SMS_Opt_In_List__c = 'False',
            //SMS_Opt_Out_List__c = 'False',
            PersonEmail = 'testerson.lucy@test.net'
        );
        
        // insert, set CreatedDate to 23 months ago
        insert a;
        Test.setCreatedDate(a.Id, Datetime.now().addMonths(-23));
        
        // create records to populate Shipping_Date__c
        a = [SELECT Id, CreatedDate FROM Account WHERE Id = :a.Id];
        Order_Item_Detail__c orderItem = generateShippingDateRecords(a);
        
        a = [SELECT Shipping_Date__c FROM Account WHERE Id = :a.Id];
        System.debug('Shipping_Date__c: ' + a.Shipping_Date__c);
        
        // run batch 
        Test.startTest();
        Database.executeBatch(new BclsMarketingCloudRecordDesync(BclsMarketingCloudRecordDesync.BatchType.ACCOUNT2));
        Test.stopTest();
        
        // assert record was processed as expected
        a = [SELECT Desynchronize_With_Marketing_Cloud__pc FROM Account WHERE Id = :a.Id];
        System.assertEquals(false, a.Desynchronize_With_Marketing_Cloud__pc);
    }

    @isTest
    // Test for account record that does not meet criteria to be desynchronized with Marketing Cloud.
    // Reason: if Shipping_Date__c is null and PersonEmail is not null, CreatedDate must be more than 24 months past
    private static void negativeAccountTest7(){
        Account a = new Account(
            FirstName = 'Lucy',
            LastName = 'Testerson',
            RecordTypeId = consumersAccountRTId,
            PersonHasOptedOutOfEmail = false,
            SMS_Pending_Opt_In__c = false,
            SMS_Opt_In_List__c = 'False',
            SMS_Opt_Out_List__c = 'False',
            PersonEmail = 'testerson.lucy@test.net'
        );
        
        // insert, set CreatedDate to 23 months ago
        insert a;
        Test.setCreatedDate(a.Id, Datetime.now().addMonths(-23));
                
        // run batch 
        Test.startTest();
        Database.executeBatch(new BclsMarketingCloudRecordDesync(BclsMarketingCloudRecordDesync.BatchType.ACCOUNT2));
        Test.stopTest();
        
        // assert record was processed as expected
        a = [SELECT Desynchronize_With_Marketing_Cloud__pc FROM Account WHERE Id = :a.Id];
        System.assertEquals(false, a.Desynchronize_With_Marketing_Cloud__pc);
    }

    @isTest
    // Test for account record that matches criteria to be desynchronized with Marketing Cloud.
    // Reason: RecordType is 'Consumers', PersonHasOptedOutOfEmail is false, SMS_Pending_Opt_In__c is false, 
    //     SMS_Opt_In_List__c is not 'True', SMS_Opt_Out_List__c is not 'True' and PersonEmail is null
    private static void positiveAccountTest1(){
        Date LAST_24_MONTHS_DATE = Date.today().addMonths(-24);
        Account a = new Account(
            FirstName = 'Lucy',
            LastName = 'Testerson',
            PersonEmail = null,
            RecordTypeId = consumersAccountRTId,
            PersonHasOptedOutOfEmail = false
            //SMS_Pending_Opt_In__c = false
            //SMS_Opt_In_List__c = 'False',
            //SMS_Opt_Out_List__c = 'False'
        );

        // insert, set CreatedDate to 23 months ago
        insert a;
        Test.setCreatedDate(a.Id, Datetime.now().addMonths(-23));
        
        // create records to populate Shipping_Date__c
        a = [SELECT Id, CreatedDate FROM Account WHERE Id = :a.Id];
        generateShippingDateRecords(a);
        
        a = [SELECT PersonEmail, Shipping_Date__c, CreatedDate FROM Account WHERE Id = :a.Id];
        System.assert(a.Shipping_Date__c > LAST_24_MONTHS_DATE);
        System.assert(a.CreatedDate > LAST_24_MONTHS_DATE);
        
        // run batch 
        Test.startTest();
        Database.executeBatch(new BclsMarketingCloudRecordDesync(BclsMarketingCloudRecordDesync.BatchType.ACCOUNT2));
        Test.stopTest();

        // assert record was processed as expected
        a = [SELECT Desynchronize_With_Marketing_Cloud__pc FROM Account WHERE Id = :a.Id];
        System.assertEquals(true, a.Desynchronize_With_Marketing_Cloud__pc);
    }

    @isTest
    // Test for account record that matches criteria to be desynchronized with Marketing Cloud.
    // Reason: RecordType is 'Consumers', PersonHasOptedOutOfEmail is false, SMS_Pending_Opt_In__c is false, 
    //     SMS_Opt_In_List__c is not 'True', SMS_Opt_Out_List__c is not 'True' and Shipping_Date__c is more than past 24 months
    private static void positiveAccountTest2(){
        Account a = new Account(
            FirstName = 'Lucy',
            LastName = 'Testerson',
            RecordTypeId = consumersAccountRTId,
            PersonHasOptedOutOfEmail = false,
            //SMS_Pending_Opt_In__c = false,
            //SMS_Opt_In_List__c = 'False',
            //SMS_Opt_Out_List__c = 'False',
            PersonEmail = 'testerson.lucy@test.net'
        );

        // insert, set CreatedDate to 25 months ago
        insert a;
        Test.setCreatedDate(a.Id, Datetime.now().addMonths(-25));
        
        // create records to populate Shipping_Date__c
        a = [SELECT Id, CreatedDate FROM Account WHERE Id = :a.Id];
        generateShippingDateRecords(a);

        // run batch 
        Test.startTest();
        Database.executeBatch(new BclsMarketingCloudRecordDesync(BclsMarketingCloudRecordDesync.BatchType.ACCOUNT2));
        Test.stopTest();

        // assert record was processed as expected
        a = [SELECT Desynchronize_With_Marketing_Cloud__pc FROM Account WHERE Id = :a.Id];
        System.assertEquals(true, a.Desynchronize_With_Marketing_Cloud__pc);
    }

    @isTest
    // Test for account record that matches criteria to be desynchronized with Marketing Cloud.
    // Reason: RecordType is 'Consumers', PersonHasOptedOutOfEmail is false, SMS_Pending_Opt_In__c is false, 
    //     SMS_Opt_In_List__c is not 'True', SMS_Opt_Out_List__c is not 'True' and Shipping_Date__c is null and CreatedDate is more than 24 months past
    private static void positiveAccountTest3(){
        Account a = new Account(
            FirstName = 'Lucy',
            LastName = 'Testerson',
            RecordTypeId = consumersAccountRTId,
            PersonHasOptedOutOfEmail = false,
            //SMS_Pending_Opt_In__c = false,
            //SMS_Opt_In_List__c = 'False',
            //SMS_Opt_Out_List__c = 'False',
            PersonEmail = 'testerson.lucy@test.net'
        );

        // insert, set CreatedDate to 25 months ago
        insert a;
        Test.setCreatedDate(a.Id, Datetime.now().addMonths(-25));

        // run batch 
        Test.startTest();
        Database.executeBatch(new BclsMarketingCloudRecordDesync(BclsMarketingCloudRecordDesync.BatchType.ACCOUNT2));
        Test.stopTest();

        // assert record was processed as expected
        a = [SELECT Desynchronize_With_Marketing_Cloud__pc FROM Account WHERE Id = :a.Id];
        System.assertEquals(true, a.Desynchronize_With_Marketing_Cloud__pc);
    }

    // creates 2 order header records associated to an account
    private static Order_Item_Detail__c generateShippingDateRecords(Account a){
        Order_Header__c oh = new Order_Header__c(
                Account__c = a.Id
        );
        insert oh;
        System.debug(oh);
        Order_Item_Detail__c oDetail = new Order_Item_Detail__c(
            Order_Header__c = oh.Id,
            Shipping_Date__c = Date.newInstance(a.CreatedDate.year(), a.CreatedDate.month(), a.CreatedDate.day()),
            Item_Number__c = 'STT'
        );
        insert oDetail;
        update a;
        System.debug(oDetail);
        return oDetail;
    }
    
    
    @isTest
    // Test for opportunity record that does not meet criteria to be desynchronized with Marketing Cloud.
    // Reason: Opportunity CreatedDate is not more than 24 months past
    private static void negativeOpportunityTest1(){
        Account a = new Account(
            FirstName = 'Lucy',
            LastName = 'Testerson',
            RecordTypeId = consumersAccountRTId,
            PersonHasOptedOutOfEmail = false,
            SMS_Pending_Opt_In__c = false,
            //SMS_Opt_In_List__c = 'False',
            //SMS_Opt_Out_List__c = 'False',
            PersonEmail = 'testerson.lucy@test.net',
            Territory_Code__c = 'abcdef012345'
        );
        
        // insert
        insert a;
                
        Opportunity o = new Opportunity(
        	Name = 'Golden Opportunity',
            AccountId = a.Id,
            CloseDate = date.today(),
            StageName = 'New Opportunity',
            Amount = 123.45
        );
        
        insert o;
        
        // run batch 
        Test.startTest();
        Database.executeBatch(new BclsMarketingCloudRecordDesync(BclsMarketingCloudRecordDesync.BatchType.ACCOUNT2));
        Test.stopTest();
        
        // assert record was processed as expected
        a = [SELECT Desynchronize_With_Marketing_Cloud__pc FROM Account WHERE Id = :a.Id];
        System.assertEquals(false, a.Desynchronize_With_Marketing_Cloud__pc);
    }

    @isTest
    // Test for opportunity record that does not meet criteria to be desynchronized with Marketing Cloud.
    // Reason: Opportunity CreatedDate is less than 24 months old
    private static void positiveOpportunityTest1(){
        Account a = new Account(
            FirstName = 'Lucy',
            LastName = 'Testerson',
            RecordTypeId = consumersAccountRTId,
            PersonHasOptedOutOfEmail = false,
            SMS_Pending_Opt_In__c = false,
            //SMS_Opt_In_List__c = 'False',
            //SMS_Opt_Out_List__c = 'False',
            PersonEmail = 'testerson.lucy@test.net',
            Territory_Code__c = 'abcdef012345'
        );
        
        // insert
        insert a;
        
        Opportunity o = new Opportunity(
        	Name = 'Golden Opportunity',
            AccountId = a.Id,
            CloseDate = date.today(),
            StageName = 'New Opportunity',
            Amount = 123.45
        );
        
        // insert
        insert o;
		
        // run batch 
        Test.startTest();
        Database.executeBatch(new BclsMarketingCloudRecordDesync(BclsMarketingCloudRecordDesync.BatchType.ACCOUNT2));
        Test.stopTest();
        
        // assert record was processed as expected
        a = [SELECT Desynchronize_With_Marketing_Cloud__pc FROM Account WHERE Id = :a.Id];
        //There is a recent opportunity so we would not desync them from Marketing Cloud.
        System.assertEquals(false, a.Desynchronize_With_Marketing_Cloud__pc);
    }

}