@isTest(seealldata = false)
public class ClsApexUtilityTest {

    @testSetup public static void clsApexUtilitySetUp(){
        List<Account> insertAccountList = new List<Account>();
        
        Id consumerRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Consumers').getRecordTypeId();
        Account consumerAccnt = TestDataBuilder.testAccount();
        consumerAccnt.RecordTypeId = consumerRecordTypeId;
        
        insertAccountList.add(consumerAccnt);
        if(!insertAccountList.isEmpty()){
            database.insert(insertAccountList);
        }
        
        MDCR_Followup__c mdcrFollowupRec = new MDCR_Followup__c();
        mdcrFollowupRec.Customer__c = consumerAccnt.Id;
        mdcrFollowupRec.External_Id__c = '12345';
        mdcrFollowupRec.MDCR_Price_Book__c = Test.getStandardPricebookId();
        mdcrFollowupRec.Order_Number__c = '23557547';
        
        insert mdcrFollowupRec;
    }
    
    @isTest
    public static void createOpptyRecordsTest(){
        Account accRec = [SELECT Id, Name from Account where RecordType.Name = 'Consumers' and FirstName = 'Testfirstname'];
        MDCR_Followup__c mdcrFollowUpRecord = [Select Id from MDCR_Followup__c where Customer__c = : accRec.Id];
        Id usOpptyRecTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('US Opportunity').getRecordTypeId();
        
        Opportunity opportunity = ClsApexUtility.createOpptyRecords(accRec.Id, accRec.Name, usOpptyRecTypeId, mdcrFollowUpRecord, 'MDCR Followup');
        system.debug('opportunity::::'+opportunity);
        system.assert(opportunity !=null);
        Test.startTest();
        Opportunity opportunity1= ClsApexUtility.createOpptyRecords(accRec.Id, accRec.Name, usOpptyRecTypeId, mdcrFollowUpRecord, 'Doc Collection');
        Test.stopTest();
    }
}