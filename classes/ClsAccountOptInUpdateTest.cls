/**
* Tests for the call out that updates the opt in/out status 
* @author Katie Wilson(Sundog)
* @date 10/15/2018
*/
@IsTest
public class ClsAccountOptInUpdateTest {
    static testMethod void testSMSScheduledJob() {
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperMock());
        List <Account> accountList= new List<Account>();
        
        Account testAccount = TestDataBuilder.testAccount();
        testAccount.PersonMobilePhone='1234567890';
        testAccount.SMS_Opt_In_List__c='True';
        accountList.add(testAccount);  
        
        Account testAccount2 = TestDataBuilder.testAccount();
        testAccount2.PersonMobilePhone='0987654321';
        testAccount2.SMS_Opt_In_List__c='True'; 
        accountList.add(testAccount2);  
        
        insert accountList;
        Test.startTest();
        String jobId = System.schedule('testSMSScheduled', '0 0 0 3 9 ? 2052',
                                       new SclsAccountOptInScheduled());           
        Test.stopTest();
    }
    
    static testMethod void testSMSOptInStatusJob() {
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperMock());
        List <Account> accountList= new List<Account>();
        
        Account testAccount = TestDataBuilder.testAccount();
        testAccount.PersonMobilePhone='(923) 456-7890';
        testAccount.SMS_Opt_In_List__c='True';
        testAccount.SMS_pending_opt_in__c=true;
        accountList.add(testAccount);  
        
        Account testAccount2 = TestDataBuilder.testAccount();
        testAccount2.PersonMobilePhone='(098) 765-4321';
        testAccount2.SMS_Opt_In_List__c='True'; 
        testAccount.SMS_pending_opt_in__c=true;
        accountList.add(testAccount2);      
        insert accountList;
        
        Test.startTest();
        ClsMarketingCloudHelper.updateOptInValues(false);
        Test.stopTest();
        
        List <Account> result=[SELECT SMS_Opt_In_List__c, SMS_Opt_out_List__c, PersonMobilePhone FROM Account];
        System.assertEquals(result.get(0).PersonMobilePhone, '(923) 456-7890');
        System.assertEquals(result.get(0).SMS_Opt_In_List__c, 'False');
        System.assertEquals(result.get(0).SMS_Opt_Out_List__c, 'True');
        System.assertEquals(result.get(1).PersonMobilePhone, '(098) 765-4321');
        System.assertEquals(result.get(1).SMS_Opt_In_List__c, 'True');
        System.assertEquals(result.get(1).SMS_Opt_Out_List__c, 'False');
    }
}