public with sharing class CtrlAccountEligibilityCountdown {
    
    private static final String CASH = 'Cash';
    private static final String TERM_CREDIT_CARD = 'Credit Card';
    private static final String TERM_INSURANCE = 'Insurance';
    
    //Method fetches the patients information related to Eligibilityinformation and returns as a Field Name to Field Value map. All attribute names are selfexplanatory
    @AuraEnabled
    public static Map<String, String> getAccountEligibilityCountDownDetails  (String accountId) {
        Map<String, String> fieldValueMap = new Map<String, String>();
        
        List<Account> accntList = new List<Account>([SELECT Latest_Transmitter_Generation_Shipped__c,Latest_Receiver_Generation_Shipped__c,
                                                     		Latest_Solution_Ship_Date__c,Next_Possible_Date_For_Solution_Order__c,
                                                     		Num_Of_Days_Left_For_Solution_Order__c, CMN_or_Rx_Expiration_Date__c, 
                                                     		Is_prior_auth_required__c, Benefit_Calendar_Year_End_Date__c, 
                                                     		Latest_Receiver_Ship_Date__c,Latest_Glucometer_Ship_Date__c,
                                                     		Next_Possible_Date_For_Glucometer_Order__c,Num_Of_Days_Left_For_Glucometer_Order__c,
                                                     		Next_Possible_Date_For_Receiver_Order__c, Num_Of_Days_Left_For_Receiver_Order__c, 
                                                     		Latest_Sensors_Ship_Date__c, Latest_Qty_Of_Sensors_Shipped__c, 
                                                     		Next_Possible_Date_For_Sensor_Order__c, Num_Of_Days_Left_For_Sensors_Order__c, 
                                                     		Latest_Transmitter_Ship_Date__c, Next_Possible_Date_For_Transmitter_Order__c, 
                                                     		Num_Of_Days_Left_For_Transmitter_Order__c, Rx_Qty_Remaining_With_Current_CMN__c, 
                                                     		Sensor_Qty_Remaining_With_Current_CMN__c, Tx_Qty_Remaining_With_Current_CMN__c,
                                                     		Payor__c,Consumer_Payor_Code__c,Latest_Insurance_Transmitter_Order__c,
                                                     		Latest_Insurance_Receiver_Order__c,Latest_Insurance_Sensor_Order__c,Customer_Type__c
                                                     		FROM ACCOUNT WHERE Id = :accountId]);
        
        if(!accntList.isEmpty())
        {
            Account accnt = accntList[0];
            
            //Add Quantity Remaining
            /*fieldValueMap.put('sensorQtyRemaining',String.ValueOf(accntList[0].Sensor_Qty_Remaining_With_Current_CMN__c));
			fieldValueMap.put('transmitterQtyRemaining',String.ValueOf(accntList[0].Tx_Qty_Remaining_With_Current_CMN__c));
			fieldValueMap.put('receiverQtyRemaining',String.ValueOf(accntList[0].Rx_Qty_Remaining_With_Current_CMN__c));*/
            
            //CMN Expiration Date
            String formattedCMNExpirationDate = accnt.CMN_or_Rx_Expiration_Date__c == null?'': accnt.CMN_or_Rx_Expiration_Date__c.month() + '/' + accnt.CMN_or_Rx_Expiration_Date__c.day() + '/' + accnt.CMN_or_Rx_Expiration_Date__c.year();
            fieldValueMap.put('CMNExpirationDate', formattedCMNExpirationDate);
            
            //Benifit Completion Date
            String formattedBenefitCompletionDate = accnt.Benefit_Calendar_Year_End_Date__c == null?'': accnt.Benefit_Calendar_Year_End_Date__c.month() + '/' + accnt.Benefit_Calendar_Year_End_Date__c.day() + '/' + accnt.Benefit_Calendar_Year_End_Date__c.year();
            fieldValueMap.put('BenefitCalendarYearEndDate', formattedBenefitCompletionDate);
            
            //Prior Auth
            String isPriorAuthReqd = accnt.Is_prior_auth_required__c == 'Y'?'Yes': accnt.Is_prior_auth_required__c == 'N'?'No':'';
            fieldValueMap.put('IsPriorAuthRequired', isPriorAuthReqd);
            
            // Added AP 01.02.2019 to add Glucometer and Solution in Eligibility countdown
            if(accnt.Latest_Glucometer_Ship_Date__c != null) {
                String formattedNextPossibleGlucometerShipDate;
                String numOfDaysRemainingForNextGlucometerOrder;
                String formattedLatestGlucometerShipDate = accnt.Latest_Glucometer_Ship_Date__c.month() + '/' + accnt.Latest_Glucometer_Ship_Date__c.day() + '/' + accnt.Latest_Glucometer_Ship_Date__c.year();   
                
                if(accnt.Next_Possible_Date_For_Glucometer_Order__c != null)
                {
                	formattedNextPossibleGlucometerShipDate =  accnt.Next_Possible_Date_For_Glucometer_Order__c.month() + '/' + accnt.Next_Possible_Date_For_Glucometer_Order__c.day() + '/' + accnt.Next_Possible_Date_For_Glucometer_Order__c.year();   
                }
                
                numOfDaysRemainingForNextGlucometerOrder = String.ValueOf(accnt.Num_Of_Days_Left_For_Glucometer_Order__c); 
                fieldValueMap.put('LatestGlucometerShipDate', formattedLatestGlucometerShipDate); 
                //fieldValueMap.put('qtyofGlucometerShipped', '1');
                fieldValueMap.put('nextPossibleGlucometerShipDate', formattedNextPossibleGlucometerShipDate); 
                fieldValueMap.put('numOfDaysRemainingForNextGlucometerOrder', numOfDaysRemainingForNextGlucometerOrder); 
            }
            
            if(accnt.Latest_Solution_Ship_Date__c != null) {
                String formattedNextPossibleSolutionShipDate;
                String numOfDaysRemainingForNextSolutionOrder;
                String formattedLatestSolutionShipDate = accnt.Latest_Solution_Ship_Date__c.month() + '/' + accnt.Latest_Solution_Ship_Date__c.day() + '/' + accnt.Latest_Solution_Ship_Date__c.year();   
                
                if(accnt.Next_Possible_Date_For_Solution_Order__c != null) {
                	formattedNextPossibleSolutionShipDate =  accnt.Next_Possible_Date_For_Solution_Order__c.month() + '/' + accnt.Next_Possible_Date_For_Solution_Order__c.day() + '/' + accnt.Next_Possible_Date_For_Solution_Order__c.year();   
                }
                
                numOfDaysRemainingForNextSolutionOrder = String.ValueOf(accnt.Num_Of_Days_Left_For_Solution_Order__c); 
                fieldValueMap.put('latestSolutionShipDate', formattedLatestSolutionShipDate); 
                //fieldValueMap.put('qtyofSolutionShipped', '1');
                fieldValueMap.put('nextPossibleSolutionShipDate', formattedNextPossibleSolutionShipDate); 
                fieldValueMap.put('numOfDaysRemainingForNextSolutionOrder', numOfDaysRemainingForNextSolutionOrder); 
            }
            
            //Get Order Header Details
            Map<String,String> orderMap = getOrderHeaderMapping(accnt);
            
            //Generation Details
            /*fieldValueMap.put('currentReceiverGen',orderMap.containsKey('currentReceiverGen')?orderMap.get('currentReceiverGen'):'');
            fieldValueMap.put('currentSensorGen',orderMap.containsKey('currentSensorGen')?orderMap.get('currentSensorGen'):'');
            fieldValueMap.put('currentTransmitterGen',orderMap.containsKey('currentTransmitterGen')?orderMap.get('currentTransmitterGen'):'');*/
            
            //Ship Date
			fieldValueMap.put('LatestTransmitterShipDate', orderMap.containsKey('latestTransmitterShipDate')?orderMap.get('latestTransmitterShipDate'):'N/A'); 
            fieldValueMap.put('LatestReceiverShipDate', orderMap.containsKey('latestReceiverShipDate')?orderMap.get('latestReceiverShipDate'):'N/A'); 
            fieldValueMap.put('LatestSensorsShipDate', orderMap.containsKey('latestSensorsShipDate')?orderMap.get('latestSensorsShipDate'):'N/A'); 
            
            //Shipping Quantity
            fieldValueMap.put('QtyOfTransmitterShipped', orderMap.containsKey('QtyOfTransmitterShipped')?orderMap.get('QtyOfTransmitterShipped'):'0');
            fieldValueMap.put('QtyOfReceiverShipped', orderMap.containsKey('QtyOfReceiverShipped')?orderMap.get('QtyOfReceiverShipped'):'0');
            fieldValueMap.put('QtyOfSensorsShipped', orderMap.containsKey('QtyOfSensorsShipped')?orderMap.get('QtyOfSensorsShipped'):'0');
            
            //Remaining Days
            fieldValueMap.put('NumOfDaysRemainingForTransmitterOrder', orderMap.containsKey('NumOfDaysRemainingForTransmitterOrder')?orderMap.get('NumOfDaysRemainingForTransmitterOrder'):'0');
            fieldValueMap.put('NumOfDaysRemainingForNextReceiverOrder', orderMap.containsKey('NumOfDaysRemainingForNextReceiverOrder')?orderMap.get('NumOfDaysRemainingForNextReceiverOrder'):'0');
        	fieldValueMap.put('NumOfDaysRemainingForNextSensorsOrder', orderMap.containsKey('NumOfDaysRemainingForNextSensorsOrder')?orderMap.get('NumOfDaysRemainingForNextSensorsOrder'):'0');
        	
            //Eligibility Date
			fieldValueMap.put('NextPossibleTransmitterShipDate',orderMap.containsKey('NextPossibleTransmitterShipDate')?orderMap.get('NextPossibleTransmitterShipDate'):'N/A');
            fieldValueMap.put('NextPossibleReceiverShipDate',orderMap.containsKey('NextPossibleReceiverShipDate')?orderMap.get('NextPossibleReceiverShipDate'):'N/A');
            fieldValueMap.put('NextPossibleSensorsShipDate',orderMap.containsKey('NextPossibleSensorsShipDate')?orderMap.get('NextPossibleSensorsShipDate'):'N/A');
        }    
        return fieldValueMap;
    }
	
   /*
    * Description : This method prepare a map with Account and it's related information
    * Param 	  : Account acc => Account Record for which we need to do the calculation
    */  
    private static Map<String,String> getOrderHeaderMapping(Account acc) {
    	Map<String,String> orderMap = new Map<String,String>();
        Boolean isMedicareFFS = false;
        Boolean isACommercial = false;
        Boolean isKCommercial = false;
        
        if(acc.Customer_Type__c.equalsIgnoreCase('Medicare FFS') && acc.Consumer_Payor_Code__c.equalsIgnoreCase('K')) {
            isMedicareFFS =  true;
        }
        
        if(acc.Customer_Type__c.equalsIgnoreCase('Commercial') && acc.Consumer_Payor_Code__c.equalsIgnoreCase('A')) {
            isACommercial =  true;
        }
        
        if(acc.Customer_Type__c.equalsIgnoreCase('Commercial') && acc.Consumer_Payor_Code__c.equalsIgnoreCase('K')) {
            isKCommercial =  true;
        }
        
        Set<Id> orderIds = new Set<Id>();
        
        if(String.isNotBlank(acc.Latest_Insurance_Transmitter_Order__c)) { 
        	orderIds.add(acc.Latest_Insurance_Transmitter_Order__c);
        }
        
        if(String.isNotBlank(acc.Latest_Insurance_Receiver_Order__c)) { 
        	orderIds.add(acc.Latest_Insurance_Receiver_Order__c);
        }
        
        if(String.isNotBlank(acc.Latest_Insurance_Sensor_Order__c)) { 
        	orderIds.add(acc.Latest_Insurance_Sensor_Order__c);
        }
        
        if(!orderIds.isEmpty()) {
            Map<Id,Order_Header__c> tempOrderMap = new Map<Id,Order_Header__c>([Select Id,Latest_Transmitter_Generation__c,Price_List__c,
                                                                                	   Latest_Sensor_Generation__c,Latest_Receiver_Generation__c,
                                                                                	   Latest_Transmitter_Ship_Date__c,Latest_Receiver_Ship_Date__c,
                                                                                	   Order_Type__c,Latest_Sensors_Ship_Date__c,Latest_Qty_Of_Sensors_Shipped__c,
                                                                                	   Price_List_Oracle_ID__c,Latest_Qty_Of_Receiver_Shipped__c,
                                                                                	   Latest_Qty_Of_Transmitter_Shipped__c,Terms__c,Effective_Transmitter_Quantity__c	
                                                                               		   from Order_Header__c
                                                                               		   where Id IN :orderIds]);
        	
            Map<String,Rules_Matrix__c> ruleMatrixMap = new Map<String,Rules_Matrix__c>();
            
            if(String.isNotBlank(acc.Payor__c) && String.isNotBlank(acc.Consumer_Payor_Code__c)) {
            	ruleMatrixMap = getRuleMatrixMapping(acc);    
            }
            
            if(tempOrderMap.containsKey(acc.Latest_Insurance_Transmitter_Order__c)) {
                Order_Header__c tempOrder = tempOrderMap.get(acc.Latest_Insurance_Transmitter_Order__c);
                String customerType;
                
                //orderMap.put('currentTransmitterGen',tempOrder.Latest_Transmitter_Generation__c);
                
                if(tempOrder.Latest_Transmitter_Ship_Date__c != null) {
                    orderMap.put('latestTransmitterShipDate',tempOrder.Latest_Transmitter_Ship_Date__c.month() + '/' + tempOrder.Latest_Transmitter_Ship_Date__c.day() + '/' + tempOrder.Latest_Transmitter_Ship_Date__c.year());
                }
                
                if(tempOrder.Effective_Transmitter_Quantity__c != null) {
                    orderMap.put('QtyOfTransmitterShipped',String.valueOf(tempOrder.Effective_Transmitter_Quantity__c));
                }
                
                /*if(acc.Num_Of_Days_Left_For_Transmitter_Order__c!=NULL) {
                	orderMap.put('NumOfDaysRemainingForTransmitterOrder',String.ValueOf(acc.Num_Of_Days_Left_For_Transmitter_Order__c));
                }*/
                
                //if(tempOrder.Order_Type__c.equalsIgnoreCase('MC standard Sales Order')) {  /// Instead use if account.customer type = Medicare FFS and Cust Payo code = K
                if(isMedicareFFS) {
                	Date tempDate = tempOrder.Latest_Transmitter_Ship_Date__c.addDays(91);  
                    orderMap.put('NextPossibleTransmitterShipDate',tempDate.month()+'/'+tempDate.day()+'/'+tempDate.year());  
                    
                    //if(tempOrder.Latest_Transmitter_Ship_Date__c!=NULL) {
                    Integer days = System.today().daysBetween(tempdate);
                    orderMap.put('NumOfDaysRemainingForTransmitterOrder',String.valueOf(days>0?days:0));
                    //}
                }
                else if(isACommercial || isKCommercial) {
                    String key = 'Transmitter'+acc.Consumer_Payor_Code__c+String.valueOf(tempOrder.Effective_Transmitter_Quantity__c);
                    
                    if(ruleMatrixMap.containsKey(key) && tempOrder.Latest_Transmitter_Ship_Date__c!=NULL){
                        Date tempDate = tempOrder.Latest_Transmitter_Ship_Date__c+Integer.valueOf(ruleMatrixMap.get(key).Duration_In_Days__c);
                        orderMap.put('NextPossibleTransmitterShipDate',tempDate.month()+'/'+tempDate.day()+'/'+tempDate.year());
                        
                        //if(tempOrder.Latest_Transmitter_Ship_Date__c!=NULL) {
                        Integer days = System.today().daysBetween(tempdate);
                        orderMap.put('NumOfDaysRemainingForTransmitterOrder',String.valueOf(days>0?days:0));
                        //}
                    }
                }
            } 
            
            if(tempOrderMap.containsKey(acc.Latest_Insurance_Receiver_Order__c)) {
                Order_Header__c tempOrder = tempOrderMap.get(acc.Latest_Insurance_Receiver_Order__c);
                String customerType;
                
                //orderMap.put('currentReceiverGen',tempOrder.Latest_Receiver_Generation__c);
                
                if(tempOrder.Latest_Receiver_Ship_Date__c != null) {
                    orderMap.put('latestReceiverShipDate',tempOrder.Latest_Receiver_Ship_Date__c.month() + '/' + tempOrder.Latest_Receiver_Ship_Date__c.day() + '/' + tempOrder.Latest_Receiver_Ship_Date__c.year());
                }
                
                if(tempOrder.Latest_Qty_Of_Receiver_Shipped__c != null) {
                    orderMap.put('QtyOfReceiverShipped',String.valueOf(tempOrder.Latest_Qty_Of_Receiver_Shipped__c));
                }
                
                if(acc.Num_Of_Days_Left_For_Receiver_Order__c!=NULL) {
                	orderMap.put('NumOfDaysRemainingForNextReceiverOrder',String.ValueOf(acc.Num_Of_Days_Left_For_Receiver_Order__c));
                }
                
                //if(tempOrder.Order_Type__c.equalsIgnoreCase('MC standard Sales Order')) {
                if(isMedicareFFS) {
                	Date tempDate = tempOrder.Latest_Receiver_Ship_Date__c.addDays(1825);  
                    orderMap.put('NextPossibleReceiverShipDate',tempDate.month()+'/'+tempDate.day()+'/'+tempDate.year());
                    
                    //if(tempOrder.Latest_Receiver_Ship_Date__c!=NULL) {
                    Integer days = System.today().daysBetween(tempdate);
                    orderMap.put('NumOfDaysRemainingForNextReceiverOrder',String.valueOf(days>0?days:0));
                    //}
                }
                else if(isACommercial || isKCommercial){
                    String key = 'Receiver'+acc.Consumer_Payor_Code__c+String.valueOf(tempOrder.Latest_Qty_Of_Receiver_Shipped__c);
                    
                    System.debug('Key>>>'+key);
                    System.debug('ruleMatrixMap>>>'+ruleMatrixMap);
                    System.debug('tempOrder.Latest_Receiver_Ship_Date__c>>'+tempOrder.Latest_Receiver_Ship_Date__c);
                    
                    if(ruleMatrixMap.containsKey(key) && tempOrder.Latest_Receiver_Ship_Date__c!=NULL){
                        Date tempDate = tempOrder.Latest_Receiver_Ship_Date__c+Integer.valueOf(ruleMatrixMap.get(key).Duration_In_Days__c);
                        orderMap.put('NextPossibleReceiverShipDate',tempDate.month()+'/'+tempDate.day()+'/'+tempDate.year());
                        
                        //if(tempOrder.Latest_Receiver_Ship_Date__c!=NULL) {
                        Integer days = System.today().daysBetween(tempdate);
                        orderMap.put('NumOfDaysRemainingForNextReceiverOrder',String.valueOf(days>0?days:0));
                        //}
                    }
                }
            }
            
            if(tempOrderMap.containsKey(acc.Latest_Insurance_Sensor_Order__c)) {
                Order_Header__c tempOrder = tempOrderMap.get(acc.Latest_Insurance_Sensor_Order__c);
                String customerType;
                
                //orderMap.put('currentSensorGen',tempOrder.Latest_Sensor_Generation__c);
                
                if(tempOrder.Latest_Sensors_Ship_Date__c != null) {
                    orderMap.put('latestSensorsShipDate',tempOrder.Latest_Sensors_Ship_Date__c.month() + '/' + tempOrder.Latest_Sensors_Ship_Date__c.day() + '/' + tempOrder.Latest_Sensors_Ship_Date__c.year());
                }
                
                if(tempOrder.Latest_Qty_Of_Sensors_Shipped__c != null) {
                    orderMap.put('QtyOfSensorsShipped',String.valueOf(tempOrder.Latest_Qty_Of_Sensors_Shipped__c));
                }
                
                if(acc.Num_Of_Days_Left_For_Sensors_Order__c!=NULL) {
                	orderMap.put('NumOfDaysRemainingForNextSensorsOrder',String.ValueOf(acc.Num_Of_Days_Left_For_Sensors_Order__c));
                }
                
                //if(tempOrder.Order_Type__c.equalsIgnoreCase('MC standard Sales Order')) {
                if(isMedicareFFS) {
                	Date tempDate = tempOrder.Latest_Sensors_Ship_Date__c.addDays(31);  
                    orderMap.put('NextPossibleSensorsShipDate',tempDate.month()+'/'+tempDate.day()+'/'+tempDate.year()); 
                    
                    //if(tempOrder.Latest_Sensors_Ship_Date__c!=NULL) {
                    Integer days = System.today().daysBetween(tempdate);
                    orderMap.put('NumOfDaysRemainingForNextSensorsOrder',String.valueOf(days>0?days:0));
                    //}
                }
                else if(isACommercial || isKCommercial) {
                    String key = 'Sensor'+acc.Consumer_Payor_Code__c+String.valueOf(tempOrder.Latest_Qty_Of_Sensors_Shipped__c);
                    
                    
                    
                    if(ruleMatrixMap.containsKey(key) && tempOrder.Latest_Sensors_Ship_Date__c!=NULL){
                        Date tempDate = tempOrder.Latest_Sensors_Ship_Date__c+Integer.valueOf(ruleMatrixMap.get(key).Duration_In_Days__c);
                        orderMap.put('NextPossibleSensorsShipDate',tempDate.month()+'/'+tempDate.day()+'/'+tempDate.year());
                        
                        //if(tempOrder.Latest_Sensors_Ship_Date__c!=NULL) {
                        Integer days = System.today().daysBetween(tempdate);
                        orderMap.put('NumOfDaysRemainingForNextSensorsOrder',String.valueOf(days>0?days:0));
                        //}
                    }
                }
        	}
        }
    	return orderMap;
    }
    
    private static Map<String,Rules_Matrix__c> getRuleMatrixMapping(Account accObj) {
        Set<String> strSet = new Set<String>{'Sensor','Transmitter','Receiver'};
        Map<String,Rules_Matrix__c> matrixMap = new Map<String,Rules_Matrix__c>();
        
        for(Rules_Matrix__c matrixObj : [Select Id,Duration_In_Days__c,Quantity_Boxes__c,
                                         		Consumer_Payor_Code__c,Product_Type__c
                                         		from Rules_Matrix__c 
                                         		where Consumer_Payor_Code__c=:accObj.Consumer_Payor_Code__c AND
                                        			  Account__c=:accObj.Payor__c AND
                                        			  Product_Type__c IN :strSet AND
                                        			  Quantity_Boxes__c!=NULL AND
                                        			  Duration_In_Days__c!=NULL])
        {
            String key = matrixObj.Product_Type__c+matrixObj.Consumer_Payor_Code__c+matrixObj.Quantity_Boxes__c;
            matrixMap.put(key,matrixObj);
        } 
        return matrixMap;
    }
    
    @AuraEnabled
    public static string getAccountId(string recid){
       string accid='';
   		opportunity oOpp= [select id,accountid from opportunity
         where  id =:recid];
       if(oOpp != null){
           accid=oOpp.accountId;
       }
       return accid;
   }
   
}