/*
    Purpose: Abbreviated form of product notes is shown in related list view on organization page.
    This is because product notes can be long and it will too  much space on list view.
    A user may change organization notes.
    When a user changes a product note, its corresponding abbreviated field needs to be changed as well.
    This is handeled in before update trigger.
    Whenever a product note is changed, change its appreviated field as well to be consistent.
    V1

*/
public class OrgDataTriggerHandler {
    public static void onBeforeUpdate(List<Organization_Data__c> lstOrgData, Map<Id, Organization_Data__c> oldMap){
         Id noteRTId= Schema.SObjectType.Organization_Data__c.getRecordTypeInfosByDeveloperName().get('Product_Note').getRecordTypeId();
         
         for(Organization_Data__c orgData : lstOrgData){
             if(orgData.RecordTypeId==noteRTId && oldMap.Get(orgData.Id).Product_Note__c != orgData.Product_Note__c){
                 orgData.Product_Note_Abbr__c=orgData.Product_Note__c.Abbreviate(100);
                 
             }
         }
    }
}