@isTest
global class ClsMockQuadaxBIResponseGenerator implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json;charset=UTF-8');
        res.setBody('{"status": 200,"result": {"invocationReferenceNumber": "162298652","coverage": 80,"copay": "0.0","planType": "MED ADV", "relationshiptoPatient": "SELF","individualDeductible": 185,"individualDeductibleMet": 0,"individualDeductibleRemaining": "0","individualOOPMax": 0,"individualOOPMaxMet": 0,"familyDeductible": 0,"familyDeductibleMet": 0,"familyOOPMax": 0,"familyOOPMaxMet": 0,"clientInsuranceId": "733"}}');
        res.setStatusCode(200);
        return res;
    }
}