/***
*@Author        : Priyanka Kajawe
*@Date Created  : 05-12-2019
*@Description   : Batch class to push marketing interaction entries for SSIP Annually
***/

global with sharing class  BclsSSIPAnnualMarketingInteractions implements Database.Batchable<sObject>, Database.Stateful{
    global String soqlQuery; //Query string
    
    //Start Method
    global Database.QueryLocator start(Database.BatchableContext BC){ 
        System.debug('*** start *** ');
        
        //prepare SOQL 
        soqlQuery = 'SELECT ID, Account__c, Scheduled_Shipment_Date__c, Status__c, Is_Scheduled__c, Skip__c, SSIP_Rule__r.Rule_Status__c, Price_Book__r.Cash_Price_Book__c FROM SSIP_Schedule__c ';
        soqlQuery += 'WHERE ( ';
        
        soqlQuery += 'SSIP_Rule__r.Rule_Status__c = \'Active\' ';
        soqlQuery += 'and Price_Book__r.Cash_Price_Book__c = false';
        
        soqlQuery += ')';
        system.debug('SOQL Query took effect is ' + soqlQuery);
        return Database.getQueryLocator(soqlQuery);
    }
    //Execute Method  
    global void execute(Database.BatchableContext BC, List<SSIP_Schedule__c> scheduleList){
        system.debug('Size of the list is ' + scheduleList.size());
        
        //List of Marketing Interaction Records to insert
        List<Marketing_Interaction__c> marketingInteractionList = new List<Marketing_Interaction__c>();
        if(!scheduleList.isEmpty()){
            for(SSIP_Schedule__c thisSchedule : scheduleList) {                
                //criteria to create MI with correct communication type
                
                Marketing_Interaction__c thisRuleMI = new Marketing_Interaction__c();
                thisRuleMI.Source_Record_Id__c = thisSchedule.SSIP_Rule__c;  // Add Source Record Id as Rule Id
                thisRuleMI.Account__c = thisSchedule.Account__c; //Account Id
                thisRuleMI.Communication_Type__c = 'Active SSIP Rule';
                marketingInteractionList.add(thisRuleMI);
                
            }  
            //Insert Marketing Interaction Records
            if(!marketingInteractionList.isEmpty()){
                try{
                    System.debug('final List >> ' + marketingInteractionList);
                    Database.insert(marketingInteractionList,false);  //Insert Marketing Interaction Records
                }catch (DmlException de) {
                     new ClsApexDebugLog().createLog(
                        new ClsApexDebugLog.Error(
                            'BclsSSIPAnnualMarketingInteractions', 'Execute','','SSIP Annual MI Insert failed: ' + de.getMessage()));
                }
            }   
        }
    }   
    
    //Finish Method
    global void finish(Database.BatchableContext BC) {
         
        //Send an email to the User after your batch completes
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        //String[] toAddresses = new String[] {'itcrmsalesforce@Dexcom.com'};
        String[] toAddresses = new String[] {'priyanka.kajawe@Dexcom.com'};
        mail.setToAddresses(toAddresses);
        mail.setSubject('BclsSSIPAnnualMarketingInteractions Apex Batch Job is complete for ' + System.Today().format() + ' run');
        mail.setPlainTextBody('The batch Apex Job To Insert MI For Active SSIP Rules Processed Successfully');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
      
    } 
}