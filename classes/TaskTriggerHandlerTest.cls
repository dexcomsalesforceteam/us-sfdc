@istest
public class TaskTriggerHandlerTest {
/********************************************************************************
@author Abhishek Parghi
@date 1/29/2016
@description: Test class for TaskTriggerHandler.
*******************************************************************************/
    
    @testSetUp
    public static void testSetupData(){
     
        Account payorAccount = new Account();
        payorAccount.RecordTypeId = ClsApexUtility.getRecordTypeIdByDeveloperName('Account','Prescriber');
        payorAccount.FirstName = 'PrescriberFirstName';
        payorAccount.LastName = 'prescriberLastName';
        payorAccount.Middle_Name__c ='middleName';
        payorAccount.PersonEmail ='TaskPrescriberFirstName@mail.com';
        payorAccount.Phone = 'Call: 1234567890324545678900';
        payorAccount.BillingState = 'OH';
        payorAccount.BillingCity = 'APTest City';
        payorAccount.BillingStreet = 'APTest Street';
        payorAccount.ShippingPostalCode =  '95112';
        payorAccount.BillingCountry = 'United States';
        payorAccount.Prescriber__c = null;
        payorAccount.IMS_ID__pc = '109';
        payorAccount.Party_ID__c = '111';
        payorAccount.CCV_ID__c = '222';
        
        insert payorAccount;
        
        Id recIdConsumer = ClsApexUtility.getRecordTypeIdByDeveloperName('Account','Consumers');
        Account accConsumer = TestDataBuilder.getAccountListConsumer(1, recIdConsumer)[0];
        accConsumer.Secondary_Plan_Name__c = 'Test';
        accConsumer.ShippingCity = 'Test';
        accConsumer.ShippingState = 'OH';
        accConsumer.PersonEmail = 'TaskPersonTest@mail.com';
        accConsumer.County__c = 'Test';
        accConsumer.ShippingPostalCode = '1';
        
        insert accConsumer;

        List<Account> accList = new List<Account>();
        Account prescriberAccount = new Account(
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prescriber').getRecordTypeId(),
            FirstName = 'APTestfirstname',
            LastName = 'APTestlastname',
            Middle_Name__c = 'Test',
            PersonEmail = 'APTestAP@gmail.com',
            Phone = 'Call: 1234567890012345678900',
            BillingState = 'OH',
            BillingCity = 'APTest City',
            BillingStreet = 'APTest Street',
            ShippingPostalCode =  '95112',
            BillingCountry = 'United States',
            Prescriber__c = null,
            IMS_ID__pc = '109',
            Party_ID__c = '111',
            CCV_ID__c = '222'  ); 
        accList.add(prescriberAccount);
        
        
        Id recIdConsumer1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Consumers').getRecordTypeId();
        Account accConsumer2 = TestDataBuilder.getAccountListConsumer(1, recIdConsumer1)[0];
        accConsumer2.Secondary_Plan_Name__c = 'Test';
        accConsumer2.ShippingCity = 'Test';
        accConsumer2.ShippingState = 'OH';
        accConsumer2.PersonEmail = 'consumer2TaskTest@gmail.com';
        accConsumer2.County__c = 'Test';
        accConsumer2.ShippingPostalCode = '1';
        
        //insert accConsumer;
        accList.add(accConsumer2);
        
        Id recIdPayor = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
        Account accPayor = TestDataBuilder.getAccountList(1, recIdPayor)[0];
        accPayor.Secondary_Plan_Name__c = 'Payor test';
        //insert accPayor;
        accList.add(accPayor);
        test.startTest();
        insert accList;
        test.stopTest();
        
    }
    
    @isTest
    Static void TaskTriggerHandler_Test(){ 
        
        Account accConsumer = [Select Id,PersonEmail from Account where PersonEmail = 'consumer2TaskTest@gmail.com' limit 1];
    	Account accPayor = [Select Id, Name from Account where recordType.DeveloperName = 'Payor' and Secondary_Plan_Name__c = 'Payor test' limit 1];
        
        Opportunity opp = TestDataBuilder.getOpportunityList(1, accConsumer.Id, accPayor.Id)[0];
        insert opp;
        
        List<Task> combinedTaskList = new List<Task>();
        List<Task> T = new List<Task>{ new Task(WhatID = accConsumer.id)};
        //Insert t;
        combinedTaskList.addAll(t);
        
        List<Task> Topp = new List<Task>{ new Task(WhatID = opp.id)};
        //insert Topp;
        combinedTaskList.addAll(Topp);
        if(!combinedTaskList.isEmpty()){
            database.insert(combinedTaskList);
        }
        
        List<Task> combinedTaskListToUpdate = new List<Task>();
        List<task> taskstoupdate = New List<task>{ [select id from task where id in :t]};
            for(task tOK:taskstoupdate)
            tOK.Party_ID__c = 'Completed';
        //Update taskstoupdate;
        combinedTaskListToUpdate.addAll(taskstoupdate);
        system.debug('getQueries()::::'+Limits.getQueries());
        Test.startTest();
        List<task> taskstoupdate1 = New List<task>{ [select id from task where id in :Topp]};
            for(task tOK1:taskstoupdate1)
            tOK1.Party_ID__c = 'Completed';
        //Update taskstoupdate1;
        combinedTaskListToUpdate.addAll(taskstoupdate1);
        
        database.update(combinedTaskListToUpdate);
        //Jagan 08/15/2018 Test Task Deletion
        //Create Inside Sales User
        String expectedExceptionThrown;
        Profile p = [SELECT Id FROM Profile WHERE Name='Inside Sales'];
        User insideSalesUser = new User(Alias = 'newUser', Email='newusertaskdeletetest@testorg.com',
                                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                                        LocaleSidKey='en_US', ProfileId = p.Id,
                                        TimeZoneSidKey='America/Los_Angeles', UserName='newusertaskdeletetest@testorg.com');
        //Create Consumer Account and task
		Id consumerRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Consumers').getRecordTypeId();
        account newAccount = new Account(
            RecordTypeId = consumerRecordTypeId,
            FirstName = 'APTestfirstname1',
            LastName = 'APTestlastname1',
            Middle_Name__c = 'Test1',
            PersonEmail = 'APTestAP1@gmail.com',
            Phone = 'Call: 8977789987',
            BillingState = 'OH',
            BillingCity = 'APTest City1',
            BillingStreet = 'APTest Street1',
            ShippingPostalCode =  '95112',
            BillingCountry = 'United States'
        ); 
        insert newAccount;
        
        Task newTask = new Task(WhatID = newAccount.Id, Status='Completed');
        insert newTask;
        Delete newTask;
        //Delete task as Inside Sales User
        System.runAs(insideSalesUser)
        {
            try{
                Delete newTask; 
            }Catch(DMLException e){expectedExceptionThrown =  e.getMessage(); system.debug('Error message ' + e.getMessage());} 
            System.AssertNotEquals(expectedExceptionThrown, null);
        }
        Test.stopTest();
    }
    
    @isTest
    public static void taskTriggerHandlerAfterUpdateTest(){
        Account personAccount = [Select Id, Name from Account where PersonEmail = 'TaskPersonTest@mail.com' limit 1];
        
        Escalation__c escalationRec = new Escalation__c();
        escalationRec.Account__c = personAccount.Id;
        escalationRec.Assigned_To_Group__c = 'Teleperformance';
        escalationRec.Status__c = 'Open';
        escalationRec.Priority__c = 'Low';
        escalationRec.Escalation_reasons__c = 'Delayed Order - Backorder';
        escalationRec.Subject__c = 'testEscalation';
        escalationRec.Channel__c = 'Email';
        insert escalationRec;
        
        test.startTest();
        
        Task taskobj = new Task();
        taskobj.RecordTypeId = ClsApexUtility.getRecordTypeIdByDeveloperName('Task','Escalation_Task');
        taskObj.status = 'Not Started';
        taskObj.Subject = 'testEscalation';
        taskObj.Priority = 'Medium';
        taskObj.WhatId = escalationRec.Id;
        insert taskObj;
        
        taskObj.Status ='Completed';
        update taskObj;
        test.stopTest();
    }
    
    
    @isTest
    public static void taskTriggerHandlerAfterUpdateTest1(){
        Account personAccount = [Select Id, Name from Account where PersonEmail = 'TaskPersonTest@mail.com' limit 1];
        
        Escalation__c escalationRec = new Escalation__c();
        escalationRec.Account__c = personAccount.Id;
        escalationRec.Assigned_To_Group__c = 'Teleperformance';
        escalationRec.Status__c = 'Open';
        escalationRec.Priority__c = 'Low';
        escalationRec.Escalation_reasons__c = 'Delayed Order - Backorder';
        escalationRec.Subject__c = 'testEscalation';
        escalationRec.Channel__c = 'Email';
        insert escalationRec;
        
        test.startTest();
        List<Task> taskRecList = new List<Task>();
        Task taskobj = new Task();
        taskobj.RecordTypeId = ClsApexUtility.getRecordTypeIdByDeveloperName('Task','Escalation_Task');
        taskObj.status = 'Not Started';
        taskObj.Subject = 'testEscalation';
        taskObj.Priority = 'Medium';
        taskObj.WhatId = escalationRec.Id;
        //insert taskObj;
        taskRecList.add(taskObj);
        Task taskobj2 = new Task();
        taskobj2.RecordTypeId = ClsApexUtility.getRecordTypeIdByDeveloperName('Task','Escalation_Task');
        taskObj2.status = 'Not Started';
        taskObj2.Subject = 'testEscalation';
        taskObj2.Priority = 'Medium';
        taskObj2.WhatId = escalationRec.Id;
        taskRecList.add(taskObj2);
        
        Task taskobj3 = new Task();
        taskobj3.RecordTypeId = ClsApexUtility.getRecordTypeIdByDeveloperName('Task','Escalation_Task');
        taskObj3.status = 'Not Started';
        taskObj3.Subject = 'testEscalation';
        taskObj3.Priority = 'Medium';
        taskObj3.WhatId = escalationRec.Id;
        taskRecList.add(taskObj3);
        
        
        insert taskRecList;
        
        taskObj.Status ='Completed';
        update taskObj;
        test.stopTest();
    }
    
    @isTest
    public static void taskReopenTest1(){
        Account personAccount = [Select Id, Name from Account where PersonEmail = 'TaskPersonTest@mail.com' limit 1];
        
        Escalation__c escalationRec = new Escalation__c();
        escalationRec.Account__c = personAccount.Id;
        escalationRec.Assigned_To_Group__c = 'Teleperformance';
        escalationRec.Status__c = 'Open';
        escalationRec.Priority__c = 'Low';
        escalationRec.Escalation_reasons__c = 'Delayed Order - Backorder';
        escalationRec.Subject__c = 'testEscalation';
        escalationRec.Channel__c = 'Email';
        insert escalationRec;
        
        Task taskobj = new Task();
        taskobj.RecordTypeId = ClsApexUtility.getRecordTypeIdByDeveloperName('Task','Escalation_Task');
        taskObj.status = 'Not Started';
        taskObj.Subject = 'testEscalation';
        taskObj.Priority = 'Medium';
        taskObj.WhatId = escalationRec.Id;
       	insert taskObj;
        
        test.startTest();
        taskObj.Status = 'Completed';
        update taskObj;
        
        taskObj.Status = 'Not Started';
        update taskObj;
        
        test.stopTest();
    }
}