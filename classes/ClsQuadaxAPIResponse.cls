/*********************************************************************************
Description - This class will be used as a response wrapper for Quadax API Call
**********************************************************************************/
public class ClsQuadaxAPIResponse {
    public ErrorMsg ErrorMsg {get;}
    
    public class ErrorMsg {
        public String rejectReasonCode;
        public String rejectReasonMessage;
    }
    
    public Integer status;
    public Result result;
	public String quadaxRequest;
	public String quadaxResponse;	
    public List<ErrorMsg> errors;
    
    public class Result {
        public String benefitStatus;
        public String invocationReferenceNumber;
        public String clientInsuranceId;
        public Integer coverage;
        public Double copay;
        public String planName;
        public String planType;
        public String relationshiptoPatient;
        public DateTime policyEndDate;
        public Double individualDeductible;
        public Double individualDeductibleMet;
		public Double individualDeductibleRemaining;
        public Double individualOOPMax;
        public Double individualOOPMaxMet;
		public Double individualOOPMaxRemaining;
        public Double familyDeductible;	
        public Double familyDeductibleMet;
		public Double familyDeductibleRemaining;		
        public Double familyOOPMax;
        public Double familyOOPMaxMet;
		public Double familyOOPMaxRemaining;
    }
    
    public static ClsQuadaxAPIResponse parse(String json) {
        return (ClsQuadaxAPIResponse) System.JSON.deserialize(json, ClsQuadaxAPIResponse.class);
    }
}