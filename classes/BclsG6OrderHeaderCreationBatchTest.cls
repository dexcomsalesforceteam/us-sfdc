@isTest
public class BclsG6OrderHeaderCreationBatchTest {
    
    @testSetup public static void dataSetUp(){
        List<Pricebook2> pricebookList = new List<Pricebook2>();
        // Cash Pricebook for account
        Pricebook2 newPricebookCash = new Pricebook2();
        newPricebookCash.Name = 'Cash Price List';
        newPricebookCash.IsActive = true;
        newPricebookCash.Cash_Price_Book__c = true;
        newPricebookCash.Customer_Type__c = 'Cash';
        
        pricebookList.add(newPricebookCash);
        
        // Adding Pricebook
        Pricebook2 newPricebookG6 = new Pricebook2();
        newPricebookG6.Name = 'G6 Auto Upgrade Program';
        newPricebookG6.IsActive = true;
        newPricebookG6.Cash_Price_Book__c = false;
        newPricebookG6.Customer_Type__c = 'Commercial';
        pricebookList.add(newPricebookG6);
        if(!pricebookList.isEmpty()){
            database.insert(pricebookList);
        }
        
        //Adding Products
        List<String> listProducts = new List<String>();
        listProducts.add('STT-OR-001');
        listProducts.add('STS-OR-003');
        Map<String, Id> productMap = ClsTestDataFactory.createProducts(listProducts);
        
        
        // Prescriber for Accoount
        Id prescriberRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Prescriber').getRecordTypeId(); 
        Account p = new Account();
        p.FirstName = 'Test';
        p.LastName = 'LTest';
        p.RecordTypeId = prescriberRecordTypeId;
        Insert p;
        
        // Account to create orders (consumer account)
        Account a = TestDataBuilder.testAccount();
        a.Default_Price_Book__c = newPricebookCash.Id;
        a.Customer_Type__c = 'Cash' ;
        a.G6_Program__c = 'Upgrade Patient (Transmitter and Sensors Only)' ;
        a.Party_ID__c ='125436987'; 
        a.Prescribers__c = p.Id;
        system.debug('**********INserting Account');
        Id consumerRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Consumers').getRecordTypeId();
        a.RecordTypeId = consumerRecordTypeId;
        insert a;
        
        system.debug('Account' +a);
        
        //Address for account Primary Ship to
        ClsAccountAddressTriggerStaticClass.addressChangeInvokedFromAddress = true;
        ClsAccountAddressTriggerStaticClass.addressChangeInvokedFromAccount = false;
        List<Address__c> addrList = new List<Address__c>();
        Address__c addrs = TestDataBuilder.getAddressList(a.Id, true,'SHIP_TO', 1)[0];
        addrs.Oracle_Address_ID__c = '23145687';
        addrList.add(addrs);
        
        Address__c addrsBT = TestDataBuilder.getAddressList(a.Id, true,'BILL_TO', 1)[0];
        addrsBT.Oracle_Address_ID__c = '523145687';
        addrList.add(addrsBT);
        
        if(!addrList.isEmpty()){
            database.insert(addrList);
        }
        a.Primary_Bill_To_Address__c = addrsBT.Id;
        a.Primary_Ship_To_Address__c = addrs.Id;
        a.BillingStreet = 'abc1';
        a.BillingCity  = 'abc1';
        a.BillingState = 'DE';
        a.BillingPostalCode = '14256';
        a.ShippingStreet = 'abc1';
        a.ShippingCity  = 'abc1';
        a.ShippingState = 'DE';
        a.ShippingPostalCode = '14256';
        a.Sensor_Quantity_Prescribed__c = 10;
        a.Transmitter_Quantity_Prescribed__c = 10;
        a.Receiver_Quantity_Prescribed__c = 10;
        system.debug('CMN Quantities added');
        system.debug('Address addrs.Id' +addrsBT.Id);
        Update a;
        system.debug('a:::::'+a);
        system.debug('Address a' +a.Primary_Ship_To_Address__c);
        Test.startTest();
        // Pricebook Entries for both products
        Id standardPricebookId = Test.getStandardPricebookId();
        List<PricebookEntry> standardPricebookEntries = new List<PricebookEntry>();
        PricebookEntry standardPriceBookEntryT = new PricebookEntry(Pricebook2Id = standardPricebookId, Product2Id = productMap.get('STT-OR-001') ,UnitPrice = 100, IsActive = true);
        Insert  standardPriceBookEntryT;
        PricebookEntry standardPriceBookEntryS = new PricebookEntry(Pricebook2Id = standardPricebookId, Product2Id = productMap.get('STS-OR-003') ,UnitPrice = 100, IsActive = true);
        Insert  standardPriceBookEntryS;
        
        PricebookEntry custompriceBookEntryT = new PricebookEntry(Pricebook2Id = newPricebookG6.Id, Product2Id = productMap.get('STT-OR-001') ,UnitPrice = 100, IsActive = true);
        Insert custompriceBookEntryT;
        PricebookEntry custompriceBookEntryS = new PricebookEntry(Pricebook2Id = newPricebookCash.Id, Product2Id = productMap.get('STS-OR-003') ,UnitPrice = 100, IsActive = true);
        Insert custompriceBookEntryS;
        
        // Order Header and detail to Populate the roll up summery field
        Order_Header__c oh1 = new Order_Header__c(Account__c = a.Id, Order_Type__c = 'Standard Sales Order',Order_Date__c = date.today().adddays(1), Ship_To_Address__c =addrs.Id );
        list <Order_Item_Detail__c> oidList = new list <Order_Item_Detail__c>();
        List <Order_Header__c> orderHeaderList = new List <Order_Header__c>{oh1};
        insert orderHeaderList;
        oidList.add(new Order_Item_Detail__c(Order_Header__c = orderHeaderList[0].Id, Item_Number__c = 'STT-GF-001', Shipping_Date__c = date.today().adddays(-182)));
        Insert oidList;
        
        //order creation
        // Order that can be updated to make call out
        Order o = new Order();
        o.AccountId = a.id;
        o.Order_Header__c = oh1.id;
        o.EffectiveDate = date.today();
        o.Status = 'Draft';
        o.Scheduled_Ship_Date__c = date.today().adddays(4);
        o.Shipping_Address__c = a.Primary_Ship_To_Address__c;
        o.Shipping_Method__c = '000001_FEDEX_A_3DS';
        o.Price_Book__c = newPricebookG6.Id;
        insert o;
        orderitem ot = new orderitem();
        ot.OrderId = o.Id;
        ot.Product2Id = productMap.get('STT-OR-001');
        ot.Quantity = 1; // changing it from 3 to 1
        ot.UnitPrice = custompriceBookEntryT.UnitPrice;
        ot.PricebookEntryId =  custompriceBookEntryT.Id ;
        insert ot;
        Test.stopTest();
    }
    
    @isTest
    Public static void Test(){
        
        /*Account accQuery = [SELECT ID, G6_Upgrade_Initial_Ship_Date__c,G6_Program__c,Customer_Type__c,G6_Orders_Generated__c,Primary_Ship_To_Address__c,G6_Upgrade_Est_Followup_Ship_Date__c,
                            BillingStreet, BillingCity, BillingState, BillingPostalCode, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode,Last_G5_Transmitter_Ship_Date_Upgrades__c FROM Account WHERE Party_ID__c ='125436987' ];
        Order_Header__c ordHdrObj = [SELECT Id,Last_G5_Transmitter_Ship_Date_Upgrades__c from Order_Header__c where Account__c = :accQuery.Id];
        PricebookEntry standardPriceBookEntryRec = [select Id, UnitPrice, Pricebook2Id, Product2Id from PricebookEntry where Pricebook2.Name = 'G6 Auto Upgrade Program'];
        system.debug('accQuery::::'+accQuery);
        system.debug('ordHdrObj:::'+ordHdrObj);*/
        Order orderObj = [Select Id, Status from Order where accountId in (Select Id from Account where Party_ID__c ='125436987')];
        
        //system.debug('oidList' +ordHdrObj.Last_G5_Transmitter_Ship_Date_Upgrades__c);   
        // Account accQuery = [SELECT ID, G6_Upgrade_Initial_Ship_Date__c,G6_Program__c,Customer_Type__c,G6_Orders_Generated__c,Primary_Ship_To_Address__c,G6_Upgrade_Est_Followup_Ship_Date__c FROM Account WHERE ID =: a.Id ];
        //system.debug('******************************************************************Updating OrderList now' +o);
        orderObj.Status = 'Shipped';
        test.startTest();
        BclsG6OrderHeaderCreationBatch obj =  new BclsG6OrderHeaderCreationBatch();
        Database.executeBatch(Obj);
        system.debug('******************************************************************Updating OrderList now');
        Update orderObj;
        system.debug('******************************************************************Updating OrderList now' +orderObj);
        system.assertEquals('Shipped', orderObj.Status);
        
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorTest());
        
        Test.stopTest();
        
    }
    
    @isTest
    public static void bclsG6OrderHeaderSchedTest(){
        Test.startTest();
        BclsG6OrderHeaderCreationBatchSched scheduler = new BclsG6OrderHeaderCreationBatchSched();
        String sch = '0 0 23 * * ?';
        system.schedule('BclsG6 OrderH Creation Batch', sch, scheduler);
        Test.stopTest();
    }

}