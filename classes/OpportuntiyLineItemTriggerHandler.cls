/********************************************************
* @author      : Kingsley Tumaneng
* @date        : OCT 22, 2015
* @description : Trigger handler for OpportunityLineItem
*********************************************************/
public class OpportuntiyLineItemTriggerHandler {
    public static void beforeInsert(List<OpportunityLineItem> oppLineItemList){
        Map<Id, OpportunityLineItem> mapOppLineItem = new Map<Id, OpportunityLineItem>();
        Map<Id, List<OpportunityLineItem>> mapExistingOpptyLineItem = new Map<Id, List<OpportunityLineItem>>();
        Set<Id> productId = new Set<Id>();
        Map<Id, Product2> mapProduct = new Map<Id, Product2>(); //this Map will hold Products with Generation
        Map<Id, Product2> mapAllProducts = new Map<Id, Product2>(); //this Map will hold data for all Products
        Boolean isError = false;
        List<Product2> prodList = new List<Product2>();
        Set<Id> accountIds = new Set<Id>();
        Set<Id> payorAccountIds = new Set<Id>();
        Boolean ignoreProductForGenerationCheck;
        List<String> productsToBeIgnoredForGenerationCheckList = new List<String>();
        List<OpportunityLineItem> filteredOpptyLineItemList = new List<OpportunityLineItem>();
        
        //Get the List of Products to be ignored for Generation Check
        for(Ignore_Generation_Check_For_Products__mdt productsToBeIgnoredMetadataType : [SELECT Label FROM Ignore_Generation_Check_For_Products__mdt])
        {
            productsToBeIgnoredForGenerationCheckList.add(productsToBeIgnoredMetadataType.Label);
            system.debug('-----Metadata label is ' + productsToBeIgnoredMetadataType.Label);
            
        }
        
        system.debug('***oppLineItemList = ' + oppLineItemList);
        //Ignore the products for which the generation check is not required
        for(OpportunityLineItem oppLineItem : oppLineItemList){
            ignoreProductForGenerationCheck = false;
            for(String productToBeIgnored : productsToBeIgnoredForGenerationCheckList)
            {
                if(oppLineItem.ProductCode == productToBeIgnored)
                {
                    ignoreProductForGenerationCheck = true;
                    system.debug('-----Ignored product is ' + oppLineItem.ProductCode);
                }
            }
            if(!ignoreProductForGenerationCheck)
            {
                productId.add(oppLineItem.Product2Id);
                mapOppLineItem.put(oppLineItem.OpportunityId, oppLineItem);
                accountIds.add(oppLineItem.Opportunity.AccountId);
                payorAccountIds.add(oppLineItem.Opportunity.Account.Payor__c);
                filteredOpptyLineItemList.add(oppLineItem);
            }
        }
        
        for(Product2 prod : [SELECT Id, Generation__c, Name FROM Product2 WHERE Id IN : productId]){
            system.debug('***prod.Product2.Generation__c = ' + prod.Generation__c);
            if(prod.Generation__c != null){
                mapProduct.put(prod.Id, prod);
                prodList.add(prod);
            }
            mapAllProducts.put(prod.Id, prod);
            system.debug('***mapProduct = ' + mapAllProducts);
            system.debug('***mapProduct.get(newOppLineItem.Product2Id) = ' + mapProduct);
        }
        system.debug('***INSIDE IF');
        for(OpportunityLineItem existingOpportunityLineItem : [SELECT Id, OpportunityId, Product2.Generation__c, Product2.Name FROM OpportunityLineItem WHERE OpportunityId IN : mapOppLineItem.keySet() AND ProductCode NOT IN :productsToBeIgnoredForGenerationCheckList]){    
            if(!mapExistingOpptyLineItem.containsKey(existingOpportunityLineItem.OpportunityId)){
                mapExistingOpptyLineItem.put(existingOpportunityLineItem.OpportunityId, new List<OpportunityLineItem>{existingOpportunityLineItem});
            }else{
                mapExistingOpptyLineItem.get(existingOpportunityLineItem.OpportunityId).add(existingOpportunityLineItem);
            }
        }
        
        for(OpportunityLineItem newOppLineItem : filteredOpptyLineItemList){
            if(mapProduct.get(newOppLineItem.Product2Id)  != null){
                if(!mapExistingOpptyLineItem.isEmpty()){
                    for(OpportunityLineItem mapOpptyLine : mapExistingOpptyLineItem.get(newOppLineItem.OpportunityId)){
                        //updated not blank check for INC0246017 in below if condition
                        if(String.isNotBlank(mapOpptyLine.Product2.Generation__c) && String.isNotBlank(mapProduct.get(newOppLineItem.Product2Id).Generation__c) &&
                           mapOpptyLine.Product2.Generation__c != mapProduct.get(newOppLineItem.Product2Id).Generation__c){
                            system.debug('inside oppty line item trigger handler'+system.now());
                            newOppLineItem.addError('The product you are trying to add is of a different Generation than products already on the Opportunity');
                        }
                    }
                }else{
                    system.debug('***INSIDE ELSE');
                    if(filteredOpptyLineItemList.size() > 1){
                        system.debug('***mapProduct.get(newOppLineItem.Product2Id) = ' + mapProduct.get(newOppLineItem.Product2Id));
                        system.debug('***prodList = ' + prodList);
                        system.debug('***prodListsize() = ' + prodList.size());
                        for(integer i = 0; i < prodList.size() - 1; i++){
                            system.debug('***i = ' + i);
                            if(String.isNotBlank(prodList[i].Generation__c) && String.isNotBlank(prodList[i + 1].Generation__c) &&
                               prodList[i].Generation__c != prodList[i + 1].Generation__c){
                                newOppLineItem.addError('The product you are trying to add is of a different Generation');
                            }
                        }
                    }
                }
            }
        }
        
        //K Code Validations
        List<OpportunityLineItem> noCashOppLineItemList = new List<OpportunityLineItem>();
        List<String> newOpportunityTypes = new List<String>();// List of Opty Type
        Map<String, Map<String, Integer>> accountProductLimitsMap = new Map<String, Map<String, Integer>>();
        Map<Id,Boolean> isTestingSupplyOptIn = new Map<Id,Boolean>(); //Rule Matrix for  Account => Testing Supply Opted In
        Map<Id, Opportunity> opportunityMap = new Map<Id, Opportunity>(); // Map to fetch Opty related data
        Map<Id,Set<String>> OptyProductNameMap = new Map<Id,Set<String>>(); // Map to hold Opty and all Product Names
        
        Map<String,Integer> productLimitMap = new Map<String,Integer>();
        List<String> testingSuppliesProducts = new List<String>();
        system.debug('###mapOppLineItem ' + mapOppLineItem);
        system.debug('###mapAllProducts - ' + mapAllProducts);
        for(Opportunity thisOpty : [SELECT Id, Type, Account.Account_18_digit_ID__c,Account.Customer_Type__c, Account.Consumer_Payor_Code__c, 
                                    Account.G4_Exception_Flag__c, Account.Latest_Transmitter_Generation_Shipped__c, PriceBook2.Cash_Price_Book__c,
                                    Account.Payor__c from Opportunity where Id IN : mapOppLineItem.keySet()]){
       		opportunityMap.put(thisOpty.id, thisOpty); 
            accountIds.add(thisOpty.Account.Account_18_digit_ID__c);
            payorAccountIds.add(thisOpty.Account.Payor__c);   
        }   
        system.debug('###opportunityMap - ' + opportunityMap);
        system.debug('###accountIds - ' + accountIds);
        
        //only work on non cash price books
        for(OpportunityLineItem newOppLineItem : oppLineItemList){
            if(opportunityMap.containsKey(newOppLineItem.OpportunityId)){
                if(!opportunityMap.get(newOppLineItem.OpportunityId).PriceBook2.Cash_Price_Book__c && 
                   !mapAllProducts.isEmpty() && mapAllProducts.containsKey(newOppLineItem.Product2Id)){
                       noCashOppLineItemList.add(newOppLineItem);  // check for No Cash Opty Products 
                       //Create Map for MST and MTX SKU validation
                       system.debug('###newOppLineItem.Product2Id - ' + newOppLineItem.Product2Id);
                       if(OptyProductNameMap.isEmpty() || !OptyProductNameMap.containskey(newOppLineItem.OpportunityId)){
                           OptyProductNameMap.put(newOppLineItem.OpportunityId, new Set<String> {mapAllProducts.get(newOppLineItem.Product2Id).Name});
                       }else{
                           OptyProductNameMap.get(newOppLineItem.OpportunityId).add(mapAllProducts.get(newOppLineItem.Product2Id).Name);               
                       }
                   }
            }
        }
        if(!noCashOppLineItemList.isEmpty()){
            for(KCode_Setting__mdt newMdt : [SELECT MasterLabel,KCode_Setting_Type__c from KCode_Setting__mdt]){
                if(newMdt.KCode_Setting_Type__c == 'New Opportunity Types') newOpportunityTypes.add(newMdt.MasterLabel);
            }
            
            for(Rules_Matrix__c ruleMtx : [SELECT Generation__c, Product_Type__c, Quantity_Boxes__c from Rules_Matrix__c
                                           where Rule_Criteria__c = 'Quarterly Limit' and 
                                           Is_True_or_False__c = true and 
                                           Consumer_Payor_Code__c = 'K']){ // Production fix: adding genration and payor code checks
                                               productLimitMap.put(ruleMtx.Product_Type__c + ruleMtx.Generation__c, Integer.valueOf(ruleMtx.Quantity_Boxes__c));
                                               testingSuppliesProducts.add(ruleMtx.Product_Type__c); // testing supply Product names
                                           }
            
            for(Rules_Matrix__c ruleMtx : [SELECT Account__c, Is_True_or_False__c from Rules_Matrix__c where Account__c In : payorAccountIds and Rule_Criteria__c = 'Testing Supplies Opt In']){
                isTestingSupplyOptIn.put(ruleMtx.Account__c, ruleMtx.Is_True_or_False__c);
            }
            
            accountProductLimitsMap = ClsApexUtility.getProductQtyOnAccount(accountIds); //get current Quarter Products quantity on Account
            system.debug('###accountProductLimitsMap - '  + accountProductLimitsMap);
            for(OpportunityLineItem newOppLineItem : noCashOppLineItemList){ 
                Integer prodQty = 0;
                Integer limitQty = 0;
                system.debug('###Customer_Type__c -' + opportunityMap.get(newOppLineItem.OpportunityId).Account.Customer_Type__c);
                system.debug('###Consumer_Payor_Code__c - ' + opportunityMap.get(newOppLineItem.OpportunityId).Account.Consumer_Payor_Code__c);
                system.debug('###G4_Exception_Flag__c - '  + opportunityMap.get(newOppLineItem.OpportunityId).Account.G4_Exception_Flag__c);
                
                Id accountId = opportunityMap.get(newOppLineItem.OpportunityId).Account.Account_18_digit_ID__c;
                String accountGeneration = opportunityMap.get(newOppLineItem.OpportunityId).Account.Latest_Transmitter_Generation_Shipped__c;
                
                if(opportunityMap.get(newOppLineItem.OpportunityId).Account.Customer_Type__c == 'Commercial' &&
                   opportunityMap.get(newOppLineItem.OpportunityId).Account.Consumer_Payor_Code__c == 'K' &&
                   opportunityMap.get(newOppLineItem.OpportunityId).Account.G4_Exception_Flag__c == false ){
                       system.debug('mapAllProducts.get(newOppLineItem.Product2Id).Name > ' + mapAllProducts.get(newOppLineItem.Product2Id).Name);
                       //check for Shipment Duration on Payor Account
                       //START: CRMSF-3465 - valiadtion on MST and MTX on Opportunity Level
                       if(!OptyProductNameMap.isEmpty()){
                           for(String existingProductName : OptyProductNameMap.get(newOppLineItem.OpportunityId)){
                               System.debug('existingProductName :: ' + existingProductName);
                               if((existingProductName.contains('MST') && mapAllProducts.get(newOppLineItem.Product2Id).Name.contains('MTX')) ||
                                  (existingProductName.contains('MTX') && mapAllProducts.get(newOppLineItem.Product2Id).Name.contains('MST')))
                                   newOppLineItem.addError('You can\'t add these sku\'s together only one or the other.');                       
                           }   
                       }
                       system.debug('mapExistingOpptyLineItem > '+ mapExistingOpptyLineItem);
                       if(mapExistingOpptyLineItem.containsKey(newOppLineItem.OpportunityId)){
                           for(OpportunityLineItem existingOppLineItem : mapExistingOpptyLineItem.get(newOppLineItem.OpportunityId)){
                               if((mapAllProducts.get(newOppLineItem.Product2Id).Name.contains('MST') && 
                                   existingOppLineItem.Product2.Name.contains('MTX')) || 
                                  (mapAllProducts.get(newOppLineItem.Product2Id).Name.contains('MTX') && 
                                   existingOppLineItem.Product2.Name.contains('MST'))){
                                       newOppLineItem.addError('You can\'t add \'MTX\' and \'MST\' SKUs together on same Opportunity.');
                                   }
                           }
                       }  //END: CRMSF-3465     
                       
                       //if Testing Supplies added
                       if(testingSuppliesProducts.Contains(mapAllProducts.get(newOppLineItem.Product2Id).Name)){
                           //if G6 Customer OR New Opty Types OR no Generation on account: Check Rule Matrix first to allow
                           if((newOpportunityTypes.contains(opportunityMap.get(newOppLineItem.OpportunityId).Type) || 
                               String.isBlank(accountGeneration) || accountGeneration == 'G6') && 
                              (!isTestingSupplyOptIn.containsKey(opportunityMap.get(newOppLineItem.OpportunityId).Account.Payor__c) || 
                               (isTestingSupplyOptIn.containsKey(opportunityMap.get(newOppLineItem.OpportunityId).Account.Payor__c) &&
                                !isTestingSupplyOptIn.get(opportunityMap.get(newOppLineItem.OpportunityId).Account.Payor__c)))){
                                    newOppLineItem.addError('Payor has opted out for Testing Supplies');
                                    break;
                                }
                           if(accountProductLimitsMap.containsKey(opportunityMap.get(newOppLineItem.OpportunityId).AccountId) && 
                              accountProductLimitsMap.get(opportunityMap.get(newOppLineItem.OpportunityId).AccountId).containsKey(mapAllProducts.get(newOppLineItem.Product2Id).Name))
                               prodQty = accountProductLimitsMap.get(opportunityMap.get(newOppLineItem.OpportunityId).AccountId).get(mapAllProducts.get(newOppLineItem.Product2Id).Name); 
                           System.debug('@@prodQty > ' + prodQty);
                           //get current Qty on Account                            
                           limitQty = productLimitMap.get(mapAllProducts.get(newOppLineItem.Product2Id).Name + accountGeneration); // added Generation check
                           if(prodQty >= limitQty){
                               newOppLineItem.addError('Customer Product Order Limit for current Shipment Duration has been reached');
                               break;
                           }   
                       }
                   }   
            }    
        }
    }
    
    //Sudheer : Prevent adding bundle product if bundle already added 
    public static void preventBundleProducts(list<OpportunityLineItem> oppLineItems){
        list<String> oppIdList=new list<String>();
        Map<string,list<OpportunityLineItem>> oppIdWithOppLineItemsMap=new Map<string,list<OpportunityLineItem>>();
        List<Prevent_Adding_Bundle_Product__mdt> preventBundleProctList=[select id,label,Preventing_Products__c from Prevent_Adding_Bundle_Product__mdt];
        Map<String,String> preventBundleProductsMap=new Map<String,String>();
        Map<String,Opportunity> opportunityMap=new Map<String,Opportunity>();
        
        for(Prevent_Adding_Bundle_Product__mdt bundle:preventBundleProctList){
            preventBundleProductsMap.put(bundle.label.trim(),bundle.Preventing_Products__c);
        }
        
        for(OpportunityLineItem oppli:oppLineItems){
            oppIdList.add(oppli.opportunityid) ;
        }
        for(Opportunity opportunity:[select Id,Name,recordtype.name from opportunity where id in:oppIdList]){
            opportunityMap.put(opportunity.Id,opportunity) ;
        }
        
        list<OpportunityLineItem> oldOptylineitemList=[select id,name,opportunity.recordtype.name,opportunity.account.Consumer_Payor_Code__c,opportunity.account.Customer_Type__c,opportunityid,product2.name from OpportunityLineItem where opportunityid in :oppIdList];
        system.debug('####oldOptylineitemList - ' + oldOptylineitemList);
        
        for(OpportunityLineItem oldOptylineitem:oldOptylineitemList){
            if(oppIdWithOppLineItemsMap.isEmpty() || !oppIdWithOppLineItemsMap.containskey(oldOptylineitem.OpportunityId)){
                oppIdWithOppLineItemsMap.put(oldOptylineitem.OpportunityId, new List<OpportunityLineItem> {oldOptylineitem});
            }else{
                oppIdWithOppLineItemsMap.get(oldOptylineitem.OpportunityId).add(oldOptylineitem);               
            } 
            
        }
        
        List<string> newProductsBundleList=new List<string>();
        for(OpportunityLineItem newOptyLineItem:oppLineItems){
            if(preventBundleProductsMap.containsKey(newOptyLineItem.SKU__c)){
                newProductsBundleList.add(newOptyLineItem.SKU__c);   
            } 
        }
        
        for(OpportunityLineItem newOptyLineItem:oppLineItems){
           // system.debug('opp item.. rec type..'+opportunityMap);
            if(!string.isblank(newOptyLineItem.OpportunityId)) {
                if(opportunityMap.get(newOptyLineItem.OpportunityId).recordtype.name == 'US Opportunity'){
                    //Prevent Products if both bundle and related bundle products is adding at a time.
                    if(!newProductsBundleList.isEmpty()){
                        for(string bundle:newProductsBundleList){
                            if(preventBundleProductsMap.get(bundle).contains(newOptyLineItem.SKU__c)){
                                newOptyLineItem.addError('You can\'t add  bundle and the same bundle product at a time i.e,'+bundle +','+newOptyLineItem.SKU__c);
                                break; 
                            }  
                        }
                    }
                    if(!oppIdWithOppLineItemsMap.isEmpty() && oppIdWithOppLineItemsMap.containskey(newOptyLineItem.OpportunityId)){
                        for(OpportunityLineItem oldOptylineitem:oppIdWithOppLineItemsMap.get(newOptyLineItem.OpportunityId)){
                            string productNames = preventBundleProductsMap.get(oldOptylineitem.product2.name);
                            
                            //Prevent Product if related bundle is there on Opp line Items.
                            if(preventBundleProductsMap.containsKey(oldOptylineitem.product2.name) && productNames.contains(newOptyLineItem.SKU__c) ) {
                                newOptyLineItem.addError('You can\'t add this Product beacause this product related bundle placed already '+ oldOptylineitem.product2.name);
                                break;
                            }
                            
                            //Prevent bundle if related products is there on Opp line Items.
                            for(string bundle:preventBundleProductsMap.keyset()){
                                if(newOptyLineItem.SKU__c == bundle && preventBundleProductsMap.get(bundle).contains(oldOptylineitem.product2.name)) {
                                    newOptyLineItem.addError('You can\'t add this bundle beacause this bundle related product Placed already'+ oldOptylineitem.product2.name);
                                    break;
                                }
                            }
                        }
                    }
                }
                
            }
        }
    }
    
    /*
     * Function addded to add a new optional receiver sku
     * when we add a G6 product on an opportunity when latest transmitter
     * on account is G5/G4.
     * CRMSF-3785 Commented changes for auto QC
     */
    public static void addIfuSkuOnOpportunity(List<OpportunityLineItem> newOliList){
        Set<Id> allOpportunityIdSet = new Set<Id>();
        Map<Id, List<OpportunityLineItem>> opptyLineItemMap = new Map<Id, List<OpportunityLineItem>>();
        Map<Id, List<String>> opptyPrdNameMap = new Map<Id, List<String>>();
        Map<Id, boolean> opptyToG6ProdMap = new Map<Id, boolean>();
        Set<Id> opptyIdSetToProcess = new Set<Id>();
        Map<Id, Id> opptyPricebookMap = new Map<Id, Id>();
        Map<Id, Opportunity> opptyAccountMap = new Map<Id, Opportunity>();
        Map<Id, PricebookEntry> priceBookEntryMap = new Map<Id, PricebookEntry>();
        Map<Id, PricebookEntry> priceBookEntryAuthMap = new Map<Id, PricebookEntry>();
        Set<Id> opptyIdToInsertOli = new Set<Id>();
        Set<Id> opptyIdToInsertAuthOli = new Set<Id>();
        List<OpportunityLineItem> oliInsertList = new List<OpportunityLineItem>();
        Set<Id> excludeIFUOpportunityIdSet = new Set<Id>();
        Set<Id> excludeAuthOpportunityIdSet = new Set<Id>();
         
        //iterate to get unique oppty Ids
        for(OpportunityLineItem oli :newOliList){
            allOpportunityIdSet.add(oli.OpportunityId);
        }
        system.debug('all opportunity set is::::'+allOpportunityIdSet);
        //get oppty Id to line items list map
        opptyLineItemMap = getOpportunityLineItemMap(allOpportunityIdSet);
        
        for(OpportunityLineItem oli :newOliList){
            if(!opptyLineItemMap.isEmpty() && opptyLineItemMap.containsKey(oli.OpportunityId) && opptyLineItemMap.get(oli.OpportunityId) !=null){
                List<OpportunityLineItem> opptyLineItemList = opptyLineItemMap.get(oli.OpportunityId);
                for(OpportunityLineItem currentOli :opptyLineItemList){
                    if(opptyPrdNameMap.containsKey(currentOli.OpportunityId)){
                        List<String> prodNameList = opptyPrdNameMap.get(currentOli.OpportunityId);
                        prodNameList.add(currentOli.Product2.Name);
                        opptyPrdNameMap.put(currentOli.OpportunityId, prodNameList);
                    }else{
                        opptyPrdNameMap.put(currentOli.OpportunityId, new List<String>{currentOli.Product2.Name});
                    }
                    
                    //below routine checks for g6 products corresponding to oppty
                    if('BUN-OR-TX6, STT-OR-001, STS-OR-003, BUN-OR-MST, BUN-OR-MTX, BUN-OR-QRT'.containsIgnoreCase(currentOli.Product2.Name)){
                        opptyToG6ProdMap.put(currentOli.OpportunityId,true);
                    }
                }
            }
            System.debug('##opptyPrdNameMap >>> ' + opptyPrdNameMap);
            System.debug('##opptyToG6ProdMap >>> ' + opptyToG6ProdMap);
            if(!opptyPrdNameMap.isEmpty() && opptyPrdNameMap.containsKey(oli.OpportunityId) &&
               null != opptyPrdNameMap.get(oli.OpportunityId)){
                   opptyIdSetToProcess.add(oli.OpportunityId);
           //Uncomment for IFU SKU Change
                   //if(opptyPrdNameMap.get(oli.OpportunityId).contains('MT24475')) excludeIFUOpportunityIdSet.add(oli.OpportunityId);
                   if(opptyPrdNameMap.get(oli.OpportunityId).contains('MT25056')) excludeAuthOpportunityIdSet.add(oli.OpportunityId);
               }
        }
        System.debug('##opptyIdSetToProcess >>> '+ opptyIdSetToProcess);
        //get account details
        for(Opportunity opptyrecord : [SELECT Id, Pricebook2Id, Account.Default_Price_Book__c, Account.RecordType.Name,
                                      Account.Customer_Type__c, Account.Latest_Transmitter_Generation_Shipped__c, 
                                      Account.Latest_Receiver_Generation_Shipped__c, Count_of_G6_Products__c,
                                      Total_Receiver_Auth_SKU__c FROM Opportunity WHERE Id In : opptyIdSetToProcess]){
            opptyAccountMap.put(opptyrecord.Id, opptyrecord);
            opptyPricebookMap.put(opptyrecord.Id , opptyrecord.Pricebook2Id);//cant have account pb
            if (Test.isRunningTest()) opptyPricebookMap.put(opptyrecord.Id , Test.getStandardPricebookID());
        }
        //get pricebook entry Map
        for(PricebookEntry priceBookEntry : [SELECT ID, UnitPrice, Name, Pricebook2Id, Product2Id from PricebookEntry 
                                             where (Name =: 'MT24475' OR Name =: 'MT25056') and Pricebook2Id IN : opptyPricebookMap.values()]){
                                                 System.debug('###priceBookEntry SKU>> ' + priceBookEntry);
                                                 if(priceBookEntry.Name == 'MT24475')
                                                    priceBookEntryMap.put(priceBookEntry.Pricebook2Id, priceBookEntry);
                                                 else
                                                 priceBookEntryAuthMap.put(priceBookEntry.Pricebook2Id, priceBookEntry);
                                             }
        //insert oli if pricebook is not empty
        
        for(Id opptyId : opptyIdSetToProcess){
            if(opptyAccountMap.get(opptyId).Account.RecordType.Name == 'Consumers' &&
               (opptyToG6ProdMap.containsKey(opptyId) && opptyToG6ProdMap.get(opptyId)) &&
               //added below or condition for INC0243533
               (ClsApexConstants.GENERATION_G4.equalsIgnoreCase(opptyAccountMap.get(opptyId).Account.Latest_Transmitter_Generation_Shipped__c) ||
                ClsApexConstants.GENERATION_G5.equalsIgnoreCase(opptyAccountMap.get(opptyId).Account.Latest_Transmitter_Generation_Shipped__c))){
             //Uncomment for IFU SKU Change
                       /*if(!priceBookEntryMap.isEmpty() &&
                        (excludeIFUOpportunityIdSet.isEmpty() || 
                       !excludeIFUOpportunityIdSet.isEmpty() && !excludeIFUOpportunityIdSet.contains(opptyId)) &&
                       opptyAccountMap.get(opptyId).Account.Customer_Type__c == 'Commercial' && 
                       ('G4'.equalsIgnoreCase(opptyAccountMap.get(opptyId).Account.Latest_Transmitter_Generation_Shipped__c) ||
                        'G5'.equalsIgnoreCase(opptyAccountMap.get(opptyId).Account.Latest_Transmitter_Generation_Shipped__c))){
                           opptyIdToInsertOli.add(opptyId);
                       }*/
                       if(!priceBookEntryAuthMap.isEmpty() &&
                       (excludeAuthOpportunityIdSet.isEmpty() || 
                       !excludeAuthOpportunityIdSet.isEmpty() && !excludeAuthOpportunityIdSet.contains(opptyId)) &&
                        'G5 Touch Screen'.equalsIgnoreCase(opptyAccountMap.get(opptyId).Account.Latest_Receiver_Generation_Shipped__c)){
                           //add default SKU
                           opptyIdToInsertAuthOli.add(opptyId);
                       }
                   }
            
            //insert Oli for Oppty Records
            //the new oli will be tagged to opty pb not account pb
      //Uncomment for IFU SKU Change
            /*if(!opptyIdToInsertOli.isEmpty()){
                for(Id iopptyId : opptyIdToInsertOli){
                    if(!priceBookEntryMap.isEmpty() && 
                        priceBookEntryMap.containsKey(opptyAccountMap.get(iopptyId).Pricebook2Id) && 
                        priceBookEntryMap.get(opptyAccountMap.get(iopptyId).Pricebook2Id) != null){
                        OpportunityLineItem oli = new OpportunityLineItem();
                        oli.Quantity = 1;
                        oli.OpportunityId = iopptyId;
                        oli.UnitPrice = priceBookEntryMap.get(opptyAccountMap.get(iopptyId).Pricebook2Id).UnitPrice;
                        oli.PriceBookEntryId = priceBookEntryMap.get(opptyAccountMap.get(iopptyId).Pricebook2Id).Id;
                        oliInsertList.add(oli);
                        system.debug('IFU oli::::' + oli);
                    }
                }
                system.debug('IFU oliInsertList::::' + oliInsertList);
                if(!oliInsertList.isEmpty())
                    insert oliInsertList;
            }*/
            
            if(!opptyIdToInsertAuthOli.isEmpty()){
                for(Id oppId : opptyIdToInsertAuthOli){
                    if(!priceBookEntryAuthMap.isEmpty() && 
                        priceBookEntryAuthMap.containsKey(opptyAccountMap.get(oppId).Pricebook2Id) && 
                        priceBookEntryAuthMap.get(opptyAccountMap.get(oppId).Pricebook2Id) != null){
                        OpportunityLineItem oli = new OpportunityLineItem();
                        oli.Quantity = 1;
                        oli.OpportunityId = oppId;
                        oli.UnitPrice = priceBookEntryAuthMap.get(opptyAccountMap.get(oppId).Pricebook2Id).UnitPrice;
                        oli.PriceBookEntryId = priceBookEntryAuthMap.get(opptyAccountMap.get(oppId).Pricebook2Id).Id;
                        oliInsertList.add(oli);
                        system.debug('AUTH oli::::' + oli);
                    }
                }
                system.debug('AUTH oliInsertList::::' + oliInsertList);
                if(!oliInsertList.isEmpty())
                    insert oliInsertList;
            }
        }
    }
    
    /*
     * method to get oppportunity and line item Map
    */
    public static Map<Id, List<OpportunityLineItem>> getOpportunityLineItemMap(Set<Id> oppportunityIdSet){
        Map<Id, List<OpportunityLineItem>> opptyLineItemMap = new Map<Id,List<OpportunityLineItem>>();
        
        for(OpportunityLineItem oli : [SELECT Id, OpportunityId, Product2.Name, Product2.Generation__c, Product_Code__c FROM OpportunityLineItem WHERE OpportunityId IN : oppportunityIdSet]){
            If(opptyLineItemMap.containsKey(oli.OpportunityId)){
                List<OpportunityLineItem> oliList = opptyLineItemMap.get(oli.OpportunityId);
                oliList.add(oli);
                opptyLineItemMap.put(oli.OpportunityId,oliList);
            }else{
                opptyLineItemMap.put(oli.OpportunityId, new List<OpportunityLineItem>{oli});
            }
        }
        return opptyLineItemMap;
    }
   
   //CRMSF-5330 : Sudheer Vemula 5/8/2020: Restrict duplicate products on opportunity products.
    public static void restrictDupProdcts(List<OpportunityLineItem> newOppLineItemsList){
        set<Id> oppIds = new set<Id>();
        
        for(OpportunityLineItem lineItem:newOppLineItemsList){
            oppIds.add(lineItem.opportunityId);
        }
       
        UtilityClass.validateDupProducts(newOppLineItemsList,oppIds);
    }
}