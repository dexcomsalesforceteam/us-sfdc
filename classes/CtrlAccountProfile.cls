public class CtrlAccountProfile {
    @AuraEnabled
    public static List<Account_Profile__c> getAccountMedicareQuestionnaire(String accountId) {
        if(!String.isBlank(accountId))
        {
            system.debug('Account Id value is ' + accountId);
            //Get Account Profile Questions for the account
            List<Account_Profile__c> accountProfileQns =  [SELECT Id, Question__c, Answer__c, External_Id__c FROM Account_Profile__c WHERE Account__c = :accountId ORDER BY createdDate ASC];
            //If there are no account profiles, create them
            if(accountProfileQns.isEmpty())
            {
                system.debug('Entering the process to create profile records');
                List<Account_Profile__c> accountProfilesToBeCreated = new List<Account_Profile__c>();
                Map<String, String> medicareQuestionsMap = getMedicareProfileQuestions();
                for(String qnId : medicareQuestionsMap.keySet())
                {
                    Account_Profile__c newAccountProfile = new Account_Profile__c();
                    newAccountProfile.Account__c = accountId;
                    newAccountProfile.External_Id__c = accountId+qnId;
                    newAccountProfile.Question__c = medicareQuestionsMap.get(qnId);
                    newAccountProfile.Type__c = 'Medicare';
                    accountProfilesToBeCreated.add(newAccountProfile);
                }
                if(!accountProfilesToBeCreated.isEmpty())
                    insert accountProfilesToBeCreated;
                return accountProfilesToBeCreated;
            }
            else
                return accountProfileQns;
        }
        else
            return null;
    }
    
    @AuraEnabled
    public static String saveAccountProfile( List<SObject> accountProfileList, String accountId ) {
        List<SObject> sObjectsToUpdate = new List<SObject>();
        sObjectsToUpdate.addAll(accountProfileList);
        //Proceess Acount update
        Boolean isMedicareQualified = true;
        Account acc = [SELECT Id, ShippingState, MDCR_FFS_Subscription_Calculation__c, MDCR_FFS_Status__c, MDCR_FFS_Subscription__c, Customer_Type__c FROM Account WHERE Id = :accountId];
        //Find if any questions are not answered made the account as not medicare qualified else make it qualified
        for(Account_Profile__c answeredQn : (List<Account_Profile__c>)accountProfileList)
        {
            if(!answeredQn.Answer__c)
                isMedicareQualified = false;
        }
        acc.MDCR_FFS_Status__c = isMedicareQualified == true ? 'Qualified' : 'Not Qualified';
        acc.MDCR_FFS_Subscription__c = acc.MDCR_FFS_Subscription_Calculation__c == 'Active' ? 'Active' : 'Not Active';
        
        //Jagan 09/13/2017 Altered the way we populate customer type logic to a customer as below
        String customerType = acc.Customer_Type__c == null ? 'Commercial' : acc.Customer_Type__c;
        //if(isMedicareQualified && !acc.Is_Not_Medicare_Eligible__c && acc.Customer_Type__c != 'Cash' && acc.Customer_Type__c != 'Medicare Advantage')
        //    acc.Customer_Type__c = 'Medicare FFS';
        //else
        //    acc.Customer_Type__c = customerType;
        
        //Get the Medicare Pricebook associated to the account
        //if(isMedicareQualified)
        //{
        //    String pricebookId = ClsApexUtility.getMedicarePricebookForAccountShippingState(acc.ShippingState);
        //    acc.Default_Price_Book__c = pricebookId;    
        //}
        sObjectsToUpdate.add(acc);
        //Update Account Profiles and the Account
        try{update sObjectsToUpdate;}
        catch (DmlException de) {
            Integer numErrors = de.getNumDml();
            System.debug('getNumDml=' + numErrors);
            for(Integer i=0;i<numErrors;i++) {
                System.debug('getDmlFieldNames=' + de.getDmlFieldNames(i));
                System.debug('getDmlMessage=' + de.getDmlMessage(i));
            }
        }
        system.debug('Update job completed');
        return 'Success';
    }
    
    //Method will retrieve the Account profile questions for Medicare from Custom Meta Data 'Account Profile Questions'
    public static Map<String, String> getMedicareProfileQuestions()
    {
        Map<String, String> medicareQuestionsMap = new Map<String, String>();
        Account_Profile_Questions__mdt[] accountProfileQuestionsList = [SELECT DeveloperName, Question__c, Active__c, Type__c, Order__c FROM Account_Profile_Questions__mdt WHERE Active__c = TRUE AND Type__c = 'Medicare' ORDER BY Order__c ASC];
        if(!accountProfileQuestionsList.isEmpty())
        {
            for(Account_Profile_Questions__mdt profileQn : accountProfileQuestionsList)
            {
                medicareQuestionsMap.put(profileQn.DeveloperName, profileQn.Question__c);
                
            }
        }
        return medicareQuestionsMap;
    }
    
}