/***
*@Author        : Priyanka Kajawe
*@Date Created  : 04-05-2019
*@Description   : Schedule class to Process SSIP Batch
***/
public class BclsProcessSSIPSchedulesSched implements Schedulable {
    public void execute(SchedulableContext sc) {
        BclsProcessSSIPSchedules schBatch = new BclsProcessSSIPSchedules();   
       
        Id batchInstanceId = Database.executeBatch(schBatch, 50);
    }
}