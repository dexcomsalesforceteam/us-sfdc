/*
* This class is created for scheduling BclsApprovalProcessRemider batch class
* Class Created for CRMSF-4914
*/
global class BclsApprovalProcessRemiderSchd implements Schedulable{
    global void execute(SchedulableContext sc){
        
        BclsApprovalProcessReminder bclsApprovalProcessReminder = new BclsApprovalProcessReminder();
        database.executeBatch(BclsApprovalProcessReminder, 200);
    }
}