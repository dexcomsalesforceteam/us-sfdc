/*
 * This is generic test class created to cover scheduler classes of
 * batches
 * @Author - LTI
*/
@isTest(SeeAllData = false)
public class ClsSchedulerTest {

    //Test class for BclsAccountExpiryUpdateSched
    @isTest
    public static void BclsAccountExpiryUpdateSchedTest(){
        Test.startTest();
        BclsAccountExpiryUpdateSched scheduler = new BclsAccountExpiryUpdateSched();
        String sch = '0 0 23 * * ?';
        system.schedule('AccountExpiryUpdate'+String.valueOf(system.now()), sch, scheduler);
        Test.stopTest();
    }
    
    //Test class for BclsCreateMedicareFollowUpRecordsSched
    @isTest
    public static void BclsCreateMedicareFollowUpRecordsSchedTest(){
        Test.startTest();
        BclsCreateMedicareFollowUpRecordsSched scheduler = new BclsCreateMedicareFollowUpRecordsSched();
        String sch = '0 0 22 * * ?';
        system.schedule('CreateMedicareFollowUpRecords'+String.valueOf(system.now()), sch, scheduler);
        Test.stopTest();
    }
    
    //Test Class for BclsCreateOpenOpportunitySched
    @isTest
    public static void BclsCreateOpenOpportunitySchedTest(){
        Test.startTest();
        BclsCreateOpenOpportunitySched scheduler = new BclsCreateOpenOpportunitySched();
        String sch = '0 0 22 * * ?';
        system.schedule('CreateOpenOpportunity'+String.valueOf(system.now()), sch, scheduler);
        Test.stopTest();
    }
        
    //Test Class For BclsUpdateAccntInfoOnForumlaUpdateSched
    @isTest
    public static void BclsUpdateAccntInfoOnForumlaUpdateSchedTest(){
        Test.startTest();
        BclsUpdateAccntInfoOnForumlaUpdateSched scheduler = new BclsUpdateAccntInfoOnForumlaUpdateSched();
        String sch = '0 0 22 * * ?';
        system.schedule('UpdateAccntInfoOn'+String.valueOf(system.now()), sch, scheduler);
        Test.stopTest();
    }
    
    @isTest
    public static void BclsSSIPOrderCreationBatchSchedTest(){
        Test.startTest();
        BclsSSIPOrderCreationBatchSched schedulerTest = new BclsSSIPOrderCreationBatchSched();
        String sch = '0 0 22 * * ?';
        system.schedule('BclsUpdateAccnt'+String.valueOf(system.now()), sch, schedulerTest);
        Test.stopTest();
    }
    
    @isTest
    public static void BclsSSIPMarketingInteractionsSchedTest(){
        Test.startTest();
        BclsSSIPMarketingInteractionsSched schedulerTest = new BclsSSIPMarketingInteractionsSched();
        String sch = '0 0 22 * * ?';
        system.schedule('SSIPMarketingInteractions'+String.valueOf(system.now()), sch, schedulerTest);
        Test.stopTest();
    }
    
    @isTest
    public static void BclsMarketingFieldsUpdateSchedTest(){
        Test.startTest();
        BclsMarketingFieldsUpdateSched schedulerTest = new BclsMarketingFieldsUpdateSched();
        String sch = '0 0 22 * * ?';
        system.schedule('MarketingFieldsUpdate'+String.valueOf(system.now()), sch, schedulerTest);
        Test.stopTest();
    }
    
    @isTest
    public static void BclsOrderAutoQCBatchSchdTest(){
        Test.startTest();
        BclsOrderAutoQCBatchSchd schedulerTest = new BclsOrderAutoQCBatchSchd();
        String sch = '0 0 22 * * ?';
        system.schedule('OrderAutoQCBatch'+String.valueOf(system.now()), sch, schedulerTest);
        Test.stopTest();
    }

}