/*************************************************
* @author      : Dexcom, LTI
* @date        : April, 2019
* @description : Controller QC Hold : BI Check Service Call
*************************************************/
public class ClsQCHoldOrderCheckService{
    //Method fetches the BI information related to Account and returns as a Field Name to Field Value map.
    @AuraEnabled
    public static Map<String, String> getOrderDetails(String orderId, String accountId, String orderType, Boolean isOrderActivated) {
        Map<String, String> fieldValueMap = new Map<String, String>();
        String sensorEligible = 'false';
        String txEligible = 'false';
        String rxEligible = 'false';
        Boolean productEligible = false;
        Boolean priorAuthCheck = false;
        String priorAuthProductList = '';
        
        System.debug('*** Order Type in Order Check ** ' + orderType);
        
        fieldValueMap = ClsOrderAuditHandler.getAuditDetailsforCheck('Order', orderId);        
        System.debug('Fetched Order Details : '+ fieldValueMap);
        
        if(fieldValueMap!= null && !fieldValueMap.isEmpty() && (isOrderActivated || Boolean.valueOf(fieldValueMap.get('bypassCheck')))){
            System.debug(' returning Order Map : ' + fieldValueMap);
            return fieldValueMap;
        }else{
            
            fieldValueMap.put('bypassCheck', 'false');
            
            List<Order> orderDataList = new List<Order>([SELECT Id, Receiver_Quantity__c, Sensor_Quantity__c, 
                                                         Account_For_Related_List__r.Primary_Benefit__r.PRIOR_AUTH_REQUIRED__c,
                                                         Transmitter_Added__c from Order where Id = :orderId limit 1]);
            
            List<Account> accntList = new List<Account>([SELECT Id, Rx_Qty_Remaining_With_Current_CMN__c, Sensor_Qty_Remaining_With_Current_CMN__c,
                                                         Tx_Qty_Remaining_With_Current_CMN__c, Rx_Qty_Remaining_With_Current_Auth__c, 
                                                         Sensor_Qty_Remaining_With_Current_Auth__c, Tx_Qty_Remaining_With_Current_Auth__c,
                                                         Next_Possible_Date_For_Sensor_Order__c, Next_Possible_Date_For_Receiver_Order__c,
                                                         Next_Possible_Date_For_Transmitter_Order__c, Num_Of_Days_Left_For_Transmitter_Order__c ,
                                                         Primary_Benefit__r.PRIOR_AUTH_REQUIRED__c, Primary_Benefit__r.New_Order_Auth_Products__c, 
                                                         Primary_Benefit__r.Reorder_Auth_Products__c, Customer_Type__c, Consumer_Payor_Code__c,
                                                         G4_Exception_Flag__c, Payor__r.Shipment_Duration__c FROM ACCOUNT WHERE Id = :accountId]);
            
            system.debug('orderDataList>>' + orderDataList);
            system.debug('accntList>>' + accntList);
            if(!accntList.isEmpty() && !orderDataList.isEmpty()){
                Account accnt = accntList[0];
                Order thisOrder = orderDataList[0];
                                
                if(accnt.Primary_Benefit__r.PRIOR_AUTH_REQUIRED__c == 'Y'){
                    priorAuthCheck = true;
                }
                if (orderType == 'New Order'){
                    priorAuthProductList = accnt.Primary_Benefit__r.New_Order_Auth_Products__c;
                }
                else{
                    priorAuthProductList = accnt.Primary_Benefit__r.Reorder_Auth_Products__c;
                }
                
                system.debug('priorAuthProductList>>' + priorAuthProductList);
                   
                //Calculate Product Eligibility
                sensorEligible = checkEligibility(thisOrder.Sensor_Quantity__c, accnt.Next_Possible_Date_For_Sensor_Order__c);
                
                //Specific code handling for K Code Orders
                if(sensorEligible == 'true' && thisOrder.Transmitter_Added__c > 0 &&
                    (accnt.Num_Of_Days_Left_For_Transmitter_Order__c  <= 30 || 
                        accnt.Next_Possible_Date_For_Transmitter_Order__c <= System.Today()) &&
                    accnt.Customer_Type__c == 'Commercial' && 
                    accnt.Consumer_Payor_Code__c == 'K' &&
                    accnt.G4_Exception_Flag__c == false && 
                    accnt.Payor__r.Shipment_Duration__c == 'Monthly'){
                        txEligible = 'true';
                    }else{  
                        txEligible = checkEligibility(thisOrder.Transmitter_Added__c, accnt.Next_Possible_Date_For_Transmitter_Order__c);
                    }
                rxEligible = checkEligibility(thisOrder.Receiver_Quantity__c, accnt.Next_Possible_Date_For_Receiver_Order__c);
                
                if(sensorEligible != 'false' && txEligible != 'false' && rxEligible != 'false') 
                    productEligible = true;
                
                if(sensorEligible == 'N/A' && txEligible == 'N/A' && rxEligible == 'N/A') 
                    productEligible = false;
                
                System.debug('Eligibility >> 1.' + sensorEligible + ' 2.'+ txEligible + ' 3.' + rxEligible + ' Finally.' + productEligible);
                //Param 1 : 
                fieldValueMap.put('productsEligible', String.valueOf(productEligible));
                
                //Param 2 : 
                fieldValueMap.put('sensorEligible', String.valueOf(sensorEligible));
                
                //Param 3 : 
                fieldValueMap.put('transmitterEligible', String.valueOf(txEligible));          
                
                //Param 4 : 
                fieldValueMap.put('receiverEligible', String.valueOf(rxEligible));
                
                //calculate CMN Limits
                
                Boolean productEligibleCMN = false;
                if(Boolean.valueOf(checkSpecificEligibility(accnt.Sensor_Qty_Remaining_With_Current_CMN__c, sensorEligible).get('Eligible')) ||
                   Boolean.valueOf(checkSpecificEligibility(accnt.Tx_Qty_Remaining_With_Current_CMN__c, txEligible).get('Eligible')) ||
                   Boolean.valueOf(checkSpecificEligibility(accnt.Rx_Qty_Remaining_With_Current_CMN__c, rxEligible).get('Eligible')))
                    productEligibleCMN =  True;
                
                System.debug('CMN Limits >> 1.' + productEligibleCMN);
                 System.debug('Sensor CMN Limits >> ' + checkSpecificEligibility(accnt.Sensor_Qty_Remaining_With_Current_CMN__c, sensorEligible).get('Eligible'));
                
                //Param 5 : 
                fieldValueMap.put('productsWithInPrescribedLimits', String.valueOf(productEligibleCMN));
                
                //Param 6 :
                Decimal sensorCMNQty = accnt.Sensor_Qty_Remaining_With_Current_CMN__c - thisOrder.Sensor_Quantity__c;
                fieldValueMap.put('prescribedRemainingSensorUnits', checkSpecificEligibility(sensorCMNQty, sensorEligible).get('Quantity'));
                
                //Param 7 :
                Decimal txCMNQty = accnt.Tx_Qty_Remaining_With_Current_CMN__c - thisOrder.Transmitter_Added__c;
                fieldValueMap.put('prescribedRemainingTransmitterUnits', checkSpecificEligibility(txCMNQty , txEligible).get('Quantity'));
                
                //Param 8 :
                Decimal rxCMNQty = accnt.Rx_Qty_Remaining_With_Current_CMN__c - thisOrder.Receiver_Quantity__c;
                fieldValueMap.put('prescribedRemainingReceiverUnits', checkSpecificEligibility(rxCMNQty , rxEligible).get('Quantity'));
                
                //calculate Auth Limits
                if(priorAuthCheck){
                    Boolean productEligibleAuth = false;
                    if(Boolean.valueOf(checkSpecificEligibility(accnt.Sensor_Qty_Remaining_With_Current_Auth__c, sensorEligible).get('Eligible')) ||
                       Boolean.valueOf(checkSpecificEligibility(accnt.Tx_Qty_Remaining_With_Current_Auth__c, txEligible).get('Eligible')) ||
                       Boolean.valueOf(checkSpecificEligibility(accnt.Rx_Qty_Remaining_With_Current_Auth__c, rxEligible).get('Eligible')))
                        productEligibleAuth=  True;
                    
                    System.debug('Auth Limits >> 1.'  + productEligibleAuth);
                    //Param 9 :
                    fieldValueMap.put('productsWithInAuthorizedLimits', String.valueOf(productEligibleAuth));
                    
                    //Param 10 :
                    if(!string.isBlank(priorAuthProductList) && priorAuthProductList.containsIgnoreCase('sensor')){
                        Decimal sensorAuthQty = accnt.Sensor_Qty_Remaining_With_Current_Auth__c- thisOrder.Sensor_Quantity__c;
                        fieldValueMap.put('authorizedRemainingSensorUnits', checkSpecificEligibility(sensorAuthQty , sensorEligible).get('Quantity'));
                    }else 
                        fieldValueMap.put('authorizedRemainingSensorUnits','N/A');
                        System.debug('Sensor Auth Qty >> '  + fieldValueMap.get('authorizedRemainingSensorUnits'));
                    
                    //Param 11 :
                    if(!string.isBlank(priorAuthProductList)&&priorAuthProductList.containsIgnoreCase('transmitter')){
                        Decimal txAuthQty = accnt.Tx_Qty_Remaining_With_Current_Auth__c - thisOrder.Transmitter_Added__c;
                        fieldValueMap.put('authorizedRemainingTransmitterUnits', checkSpecificEligibility(txAuthQty , txEligible).get('Quantity'));
                    }else
                        fieldValueMap.put('authorizedRemainingTransmitterUnits', 'N/A');
                    
                    System.debug('Tx Auth QTY >> '  + fieldValueMap.get('authorizedRemainingTransmitterUnits'));
                    
                    //Param 12 :
                    if(!string.isBlank(priorAuthProductList)&&priorAuthProductList.containsIgnoreCase('receiver')){
                        Decimal rxAuthQty = accnt.Rx_Qty_Remaining_With_Current_Auth__c - thisOrder.Receiver_Quantity__c;
                        fieldValueMap.put('authorizedRemainingReceiverUnits', checkSpecificEligibility(rxAuthQty , rxEligible).get('Quantity'));
                    }else
                        fieldValueMap.put('authorizedRemainingReceiverUnits', 'N/A');
                        System.debug('Rx Auth Qty >>'  + fieldValueMap.get('authorizedRemainingReceiverUnits'));
                }else{
                    //Param 9 :
                    fieldValueMap.put('productsWithInAuthorizedLimits', String.valueOf(true));
                    
                    //Param 10 :
                    fieldValueMap.put('authorizedRemainingSensorUnits','N/A');
                    
                    //Param 11 :
                    fieldValueMap.put('authorizedRemainingTransmitterUnits', 'N/A');
                    
                    //Param 12 :
                    fieldValueMap.put('authorizedRemainingReceiverUnits', 'N/A');
                }
            }  
            System.debug('fieldValueMap Order>>' + fieldValueMap);
            return fieldValueMap;
        }
    }
    
    public static String checkEligibility(Decimal quantity, Date eligibilityDate){
        String result = 'false';
        
        if(Integer.valueOf(quantity) > 0){            
            if(eligibilityDate == null || eligibilityDate <= System.Today())
                result = 'true';
            else 
                result = 'false';    
        }else 
            result = 'N/A';
            
        return result;
    }
    
    public static Map<String,String> checkSpecificEligibility(Decimal quantity, String eligibility){
        Map<String,String> resultMap = new Map<String,String>{'Eligible' => 'false', 'Quantity' => 'N/A'};
            if(eligibility == 'true'){
                if(Integer.valueOf(quantity) > 0){
                    resultMap.put('Eligible', 'true');
                    resultMap.put('Quantity', String.valueOf(Integer.valueOf(quantity)));
                }else{
                    resultMap.put('Eligible', 'false');
                    resultMap.put('Quantity', String.valueOf(0));
                }
            }else if(eligibility == 'N/A'){
                return resultMap;
            }else{
                resultMap.put('Eligible', 'false');
                resultMap.put('Quantity', String.valueOf(0));
            }
           
       return resultMap;
                
    }
}