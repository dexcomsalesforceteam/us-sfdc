//Scheduling Class Creates Open Opportunities of type "Proactive Medicare Docs" for Medicare Accounts
global class BclsCreateOpenOpportunitySched implements Schedulable {
    // Execute at regular intervals
    global void execute(SchedulableContext ctx){
      BclsCreateOpenOpportunity batch = new BclsCreateOpenOpportunity();
      Database.executebatch(batch, 50);
    }
}