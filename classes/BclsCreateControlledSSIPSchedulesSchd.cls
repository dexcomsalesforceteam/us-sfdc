/*
 * @Description : This scheduler class is implemented story CRMSF-4577
 * @Author : LTI
*/
global class BclsCreateControlledSSIPSchedulesSchd implements Schedulable{
    global void execute(SchedulableContext sc){
        BclsCreateControlledSSIPSchedules b = new BclsCreateControlledSSIPSchedules ();
       
        database.executebatch(b,200);
    }
}