/*
* This is Handler Class for MDCR Follw up object's trigger
* Created By: LTI
*/
public class ClsMdcrFollowupTriggerHandler {
    
    
    /* 
    * This method runs in after insert event of the MDCR Follow up Object
    * @param List<MDCR_Followup__c>
    */
    public static void handleAfterInsert(List<MDCR_Followup__c> newMdrRecList){
        List<MDCR_Followup__c> processG6TransitionFromTouchscreenList = new List<MDCR_Followup__c>();
        List<MDCR_Followup__c> processG6TransitionFromBrickList = new List<MDCR_Followup__c>();
        Set<Id> pricebookIdSet = new Set<Id>();
        Set<Id> accountIdSet = new Set<Id>();
        Set<Id> pricebookIdG6receiverSet = new Set<Id>();
        for(MDCR_Followup__c mdcrFollowupRec : newMdrRecList){
            accountIdSet.add(mdcrFollowupRec.Customer__c);
        }
        
        Boolean isNotAdminUser = getIsNotAdminUser();
        for(MDCR_Followup__c mdcrFollowUpFetchedRec : [SELECT Id, RecordType.DeveloperName, MDCR_Price_Book__c, Customer__r.Latest_Receiver_Generation_Shipped__c, Customer__r.Latest_Transmitter_Generation_Shipped__c, Customer__r.Num_Of_Days_Left_For_Transmitter_Order__c 
                                                       FROM MDCR_Followup__c WHERE Customer__c in : accountIdSet]){
                                                           if(ClsApexConstants.MFR_US_REORDER_G6_REC_TYPE.equalsIgnoreCase(mdcrFollowUpFetchedRec.RecordType.DeveloperName)
                                                              && ClsApexConstants.GENERATION_G5.equalsIgnoreCase(mdcrFollowUpFetchedRec.Customer__r.Latest_Transmitter_Generation_Shipped__c)){
                                                                  if(ClsApexConstants.RECEIVER_G5_TOUCH_SCREEN.equalsIgnoreCase(mdcrFollowUpFetchedRec.Customer__r.Latest_Receiver_Generation_Shipped__c)){
                                                                      processG6TransitionFromTouchscreenList.add(mdcrFollowUpFetchedRec);
                                                                      pricebookIdSet.add(mdcrFollowUpFetchedRec.MDCR_Price_Book__c);
                                                                  }
                                                                  if((isNotAdminUser && ClsApexConstants.GENERATION_G5.equalsIgnoreCase(mdcrFollowUpFetchedRec.Customer__r.Latest_Receiver_Generation_Shipped__c)) || Test.isRunningTest()){
                                                                      processG6TransitionFromBrickList.add(mdcrFollowUpFetchedRec);
                                                                      pricebookIdG6receiverSet.add(mdcrFollowUpFetchedRec.MDCR_Price_Book__c);
                                                                  }
                                                              }
                                                       }
        if(!processG6TransitionFromTouchscreenList.isEmpty()){
            addDefaultSkuToMFR(processG6TransitionFromTouchscreenList, pricebookIdSet);
        }
        if(!processG6TransitionFromBrickList.isEmpty() && !System.isBatch()){
            addG6ReceiverToMdcrRecord(processG6TransitionFromBrickList, pricebookIdG6receiverSet);   
        }
    }
    
    /*
    * This method will add default product corresponding
    * to a qualifying MDCR Record
    * @param List<MDCR_Followup__c>
    */
    public static void addDefaultSkuToMFR(List<MDCR_Followup__c> processMdcrList, Set<Id> pricebookIdSet){
        Map<Id, List<PricebookEntry>> priceBookEntryAuthMap = new Map<Id, List<PricebookEntry>>();
        List<MDCR_Followup_Products__c> mdcrFollowUpProductRecordsToAdd = new List<MDCR_Followup_Products__c>();
        List<PricebookEntry> pricebookEntryListToProcess = new List<PricebookEntry>();
        //get pricebook entry Map - this will have Pricebook Id, List<MT25056 pricebookEntry>, Modified below list to add transmitter kit as well
        priceBookEntryAuthMap = System.isBatch() ? getPbPricebookEntryMap(new List<String>{ClsApexConstants.MT25056_SKU}, pricebookIdSet) : getPbPricebookEntryMap(new List<String>{ClsApexConstants.MT25056_SKU, ClsApexConstants.G6_MDCR_TRANSMITTER_KIT}, pricebookIdSet);
        system.debug('processMdcrList::::'+processMdcrList);
        system.debug('priceBookEntryAuthMap::::'+priceBookEntryAuthMap);
        for(List<PricebookEntry> pbEntryList : priceBookEntryAuthMap.values()){
            pricebookEntryListToProcess.addAll(pbEntryList);
        }
        for(MDCR_Followup__c mdcrFollowupRec : processMdcrList){
            if(!priceBookEntryAuthMap.isEmpty() && priceBookEntryAuthMap.containsKey(mdcrFollowupRec.MDCR_Price_Book__c)
               && priceBookEntryAuthMap.get(mdcrFollowupRec.MDCR_Price_Book__c) != null){
                   for(PricebookEntry pbEntry : pricebookEntryListToProcess){ //this loop is iterated number of times the SKUs to be added
                       MDCR_Followup_Products__c newProduct = new MDCR_Followup_Products__c();
                       newProduct.MDCR_Followup__c = mdcrFollowupRec.Id;
                       newProduct.Product__c = pbEntry.Product2Id;
                       newProduct.Max_Qty__c = 1;
                       newProduct.Product_Added_By_System_User__c = true;
                       newProduct.Unused_Qty__c = 0;
                       newProduct.Order_Qty__c = 1;
                       mdcrFollowUpProductRecordsToAdd.add(newProduct);
                   }
               }
        }
        system.debug('mdcrFollowUpProductRecordsToAdd::::'+mdcrFollowUpProductRecordsToAdd);
        if(!mdcrFollowUpProductRecordsToAdd.isEmpty()){
            Database.SaveResult[] srList =Database.insert(mdcrFollowUpProductRecordsToAdd, ClsApexConstants.BOOLEAN_FALSE);
            
            for(integer i=0;i<srList.size();i++){
                system.debug('********Entering process to create the MFR Products, count is ' + mdcrFollowUpProductRecordsToAdd.size());
                if(!srList[i].isSuccess()){
                    String errorMsg = srList[i].getErrors().get(0).getFields() + ':' + srList[i].getErrors().get(0).getMessage();
                    system.debug('Error message is ' + errorMsg);
                    if(errorMsg.Length() >= 200)
                        errorMsg = errorMsg.substring(0,200);
                    //Write the error log
                    new ClsApexDebugLog().createLog(
                        new ClsApexDebugLog.Error(
                            'mdcrFollowUpProductRecordsToAdd',
                            'mdcrFollowUpRecordsToAdd',
                            '',
                            errorMsg
                        ));
                }
            }
        }
        
    }
    
    /*
    * Method to add G6 Receiver on MDCR follow up record
    * This method is called if the MDCR record is created by non admin user,
    * Record type is Us Reorder G6 and MFR type is G6 Transition from Brick
    * Creates records in MDCR Follow up Products object
    */
    public static void addG6ReceiverToMdcrRecord(List<MDCR_Followup__c> processMdcrList, Set<Id> pricebookIdSet){
        Map<Id, List<PricebookEntry>> pbPricebookEntryMap = new Map<Id, List<PricebookEntry>>();
        List<PricebookEntry> pricebookEntryListToProcess = new List<PricebookEntry>();
        List<MDCR_Followup_Products__c> mdcrFollowUpProductRecordsToAdd = new List<MDCR_Followup_Products__c>();
        //get pricebook map
        pbPricebookEntryMap = getPbPricebookEntryMap(new List<String>{ClsApexConstants.G6_MDCR_TOUCHSCREEN_KIT, ClsApexConstants.G6_MDCR_TRANSMITTER_KIT}, pricebookIdSet);
        for(List<PricebookEntry> pbEntryList : pbPricebookEntryMap.values()){
            pricebookEntryListToProcess.addAll(pbEntryList);
        }
        
        for(MDCR_Followup__c mdcrFollowupRec : processMdcrList){
            if(!pbPricebookEntryMap.isEmpty() && pbPricebookEntryMap.containsKey(mdcrFollowupRec.MDCR_Price_Book__c)
               && pbPricebookEntryMap.get(mdcrFollowupRec.MDCR_Price_Book__c) != null){
                   for(PricebookEntry pbEntry : pricebookEntryListToProcess){ //this loop is to execute times the SKUs being added
                       MDCR_Followup_Products__c newProduct = new MDCR_Followup_Products__c();
                       newProduct.MDCR_Followup__c = mdcrFollowupRec.Id;
                       newProduct.Product__c = pbEntry.Product2Id;
                       newProduct.Max_Qty__c = 1;
                       newProduct.Product_Added_By_System_User__c = true;
                       newProduct.Unused_Qty__c = 0;
                       newProduct.Order_Qty__c = 1;
                       mdcrFollowUpProductRecordsToAdd.add(newProduct);
                   }
               }
        }
        system.debug('records to be created in MDCR follow up Record object:::'+mdcrFollowUpProductRecordsToAdd);
        
        if(!mdcrFollowUpProductRecordsToAdd.isEmpty()){
            Database.SaveResult[] srList =Database.insert(mdcrFollowUpProductRecordsToAdd, ClsApexConstants.BOOLEAN_FALSE);
            
            for(integer i=0;i<srList.size();i++){
                system.debug('********Entering process to create the MFR Products, count is ' + mdcrFollowUpProductRecordsToAdd.size());
                if(!srList[i].isSuccess()){
                    String errorMsg = srList[i].getErrors().get(0).getFields() + ':' + srList[i].getErrors().get(0).getMessage();
                    system.debug('Error message is ' + errorMsg);
                    if(errorMsg.Length() >= 200)
                        errorMsg = errorMsg.substring(0,200);
                    //Write the error log
                    new ClsApexDebugLog().createLog(
                        new ClsApexDebugLog.Error(
                            'addG6ReceiverToMdcrRecord',
                            'mdcrFollowUpRecordsToAdd',
                            '',
                            errorMsg
                        ));
                }
            }
        }
    }
    
    /*
    * Get qualifiying user based on Profile
    * if profile is not system admin then user is qualified to add
    * STK-OM-OO1 sku on MDCR record
    */
    public static Boolean getIsNotAdminUser(){
        Boolean isNotAdminUser = ClsApexConstants.BOOLEAN_False;
        User currentUser = [SELECT Id, Profile.Name FROM User where Id = : UserInfo.getUserId()];
        if(currentUser !=null && !ClsApexConstants.SYSTEM_ADMINISTRATOR_PROFILE.equalsIgnoreCase(currentUser.Profile.Name)){
            isNotAdminUser = ClsApexConstants.BOOLEAN_TRUE;
        }
        return isNotAdminUser;
    }

    /*
    * This is a reusable function created to return a map
    * to be moved in utility as same code is written in order, oppty and
    * line item triggers
    * @return Map<Pricebook2Id, PricebookEntry>
    * @param productName, PricebookIdSet
    */
    public static Map<Id, List<PricebookEntry>> getPbPricebookEntryMap(List<String> productName, Set<Id> pricebookIdSet){
        system.debug('productName::::'+productName);
        system.debug('pricebookIdSet::::'+pricebookIdSet);
        Map<Id, List<PricebookEntry>> pbPricebookEntryMap = new Map<Id, List<PricebookEntry>>();
        for(PricebookEntry priceBookEntry : [SELECT ID, UnitPrice, Name, Pricebook2Id, Product2Id from PricebookEntry 
                                             WHERE Name IN : productName and Pricebook2Id IN : pricebookIdSet]){
                                                 System.debug('###priceBookEntry SKU>> ' + priceBookEntry);
                                                 if(!pbPricebookEntryMap.containskey(priceBookEntry.Pricebook2Id)){
                                                     pbPricebookEntryMap.put(priceBookEntry.Pricebook2Id, new List<PricebookEntry>{priceBookEntry});
                                                 }else{
                                                     List<PricebookEntry> pbEntryList = pbPricebookEntryMap.get(priceBookEntry.Pricebook2Id);
                                                     pbEntryList.add(priceBookEntry);
                                                 }
                                                 
                                             }
        return pbPricebookEntryMap;
    }
    
    
    /* @Description - If any MFR followup record already exists in last 30days without any order number,
     * it will prevent creation of new MFR followup.
     * This method is created for CRMSF-4694, modofied for CRMSF-4697
     * @Author - LTI
     * @Param - List<MDCR_Followup__c>
    */
    public static void handleBeforeInsert(List<MDCR_Followup__c> newMdrRecList){ 
        Map<Id, MDCR_Followup__c> accountToMdcrMap = new Map<Id, MDCR_Followup__c>();
        //maintain the account Id set
        Set<Id> accountIdSet = new Set<Id>();
        for(MDCR_Followup__c mdcrFollowupRec : newMdrRecList){
            accountIdSet.add(mdcrFollowupRec.Customer__c);
        }
        System.debug('accountIdSet'+accountIdSet);
        Boolean notAdminUser = getIsNotAdminUser(); 
        for(MDCR_Followup__c mdcrFollowUpFetchedRec : [SELECT Id,Name,Customer__c,MDCR_Followup_Date__c,Order_Number__c,MDCR_Followup_Status__c, recordTypeId 
                                                       FROM MDCR_Followup__c WHERE Customer__c in : accountIdSet AND MDCR_Followup_Status__c in : ClsApexConstants.MDCR_STATUS_LIST AND Order_Number__c=Null AND MDCR_Followup_Date__c >= : system.today().addDays(-30) AND Subscription_Only_MFR__c = false Order by MDCR_Followup_Date__c Desc]){
                                                           //populate a map of Account Id to MDCR satisfying the above criteria -if limit 1 then map<Id, MDCR_FOllow Up >is sufficient
                                                           if(!accountToMdcrMap.containsKey(mdcrFollowUpFetchedRec.Customer__c)){
                                                               accountToMdcrMap.put(mdcrFollowUpFetchedRec.Customer__c, mdcrFollowUpFetchedRec);
                                                           }
                                                       }
        
        //iterate to validate the records
        for(MDCR_Followup__c mdcrFollowupRec : newMdrRecList){
            if(notAdminUser && accountToMdcrMap.containsKey(mdcrFollowupRec.Customer__c) && !mdcrFollowupRec.Subscription_Only_MFR__c && mdcrFollowupRec.recordTypeId == accountToMdcrMap.get(mdcrFollowupRec.Customer__c).recordTypeId){
                mdcrFollowupRec.addError('Please reopen the MFR '+accountToMdcrMap.get(mdcrFollowupRec.Customer__c).Name+' which does not have order number.');
            }
        }
    }
    
    /*
     * @Description - This function is called from before update
     * when status is updated from Send to Oracle or Closed To all statuses other than Cancelled
     * and if there exists an order header created for MDCR record then, restrict the status change
     * Created for CRMSF-4697
     * @Author - LTI
     * 
    */
    public static void handleBeforeUpdate(Map<Id, MDCR_Followup__c> newMdcrMap, Map<Id, MDCR_Followup__c> oldMdcrMap){
        //map to store list of all associated orders corresponding to mdcr record
        Map<Id, List<Order_Header__c>> mdcrToOrderHeaderMap = new Map<Id, List<Order_Header__c>>();
        //get current user details
        User currentUser = [SELECT Id, Profile.Name FROM User where Id = : UserInfo.getUserId()];
        //get associated order records to all mdcr records and maintain a map
        for(Order_Header__c orderHeaderRecord : [SELECT Id, MDCR_Followup__c FROM Order_Header__c WHERE MDCR_Followup__c IN : newMdcrMap.keySet()]){
            if(mdcrToOrderHeaderMap.containsKey(orderHeaderRecord.MDCR_Followup__c)){
                List<Order_Header__c> associatedOrderList = mdcrToOrderHeaderMap.get(orderHeaderRecord.MDCR_Followup__c);
                associatedOrderList.add(orderHeaderRecord);
            }else{
                mdcrToOrderHeaderMap.put(orderHeaderRecord.MDCR_Followup__c, new List<Order_Header__c>{orderHeaderRecord});
            }
        }
        //iterate over new map values to check change in status
        for(MDCR_Followup__c updatedMdcrRec : newMdcrMap.values()){
            //if profile is not system admin or data integrator and status is changed from Send to oracle or Closed to
            //other status than cancelled and order headers are present on MDCR then add error
            if((!ClsApexConstants.SYSTEM_ADMINISTRATOR_PROFILE.equalsIgnoreCase(currentUser.Profile.Name) ||
                !ClsApexConstants.PROFILE_DATA_INTEGRATOR.equalsIgnoreCase(currentUser.Profile.Name)) &&
               !String.isBlank(updatedMdcrRec.MDCR_Followup_Status__c) && oldMdcrMap.containsKey(updatedMdcrRec.Id) &&
               updatedMdcrRec.MDCR_Followup_Status__c != oldMdcrMap.get(updatedMdcrRec.Id).MDCR_Followup_Status__c &&
               (ClsApexConstants.MDCR_STATUS_SEND_TO_ORACLE.equalsIgnoreCase(oldMdcrMap.get(updatedMdcrRec.Id).MDCR_Followup_Status__c) ||
                ClsApexConstants.STATUS_CLOSED.equalsIgnoreCase(oldMdcrMap.get(updatedMdcrRec.Id).MDCR_Followup_Status__c)) &&
               !ClsApexConstants.MDCR_ORA_ORDER_CANCELLED_STATUS.equalsIgnoreCase(updatedMdcrRec.MDCR_Followup_Status__c)){
                   if(mdcrToOrderHeaderMap.containsKey(updatedMdcrRec.Id) && mdcrToOrderHeaderMap.get(updatedMdcrRec.Id) != null
                      && mdcrToOrderHeaderMap.get(updatedMdcrRec.Id).size() > ClsApexConstants.ZERO_INTEGER){
                          updatedMdcrRec.addError(ClsApexConstants.MDCR_STATUS_CHANGE_ERROR);
                      }
               }
        }
    }
}