@isTest
public class ClsMdcrFollowupTriggerHandlerTest {

    @testsetup
    public static void createTestData(){
        Id consumerRecTypeId = ClsApexUtility.getRecordTypeIdByDeveloperName(ClsApexConstants.ACCOUNT_API_NAME, ClsApexConstants.ACCOUNT_REC_TYPE_CONSUMERS_LABEL);
        Account testAccount=TestDataBuilder.testAccount();
        testAccount.Default_Price_Book__c=Test.getStandardPricebookId();
        testAccount.PersonEmail = 'Test@gmail.com1241';
        testAccount.BillingStreet='abc';
        testAccount.BillingCity ='Bang';
        testAccount.BillingState ='KN';
        testAccount.BillingPostalCode='56003';
        testAccount.BillingCountry ='US';
        testAccount.ShippingStreet  ='Bang';
        testAccount.ShippingCity='Bang';
        testAccount.ShippingState='KN';
        testAccount.ShippingCountry = 'US';
        testAccount.ShippingPostalCode='56007';
        testAccount.Party_ID__c ='123344';
        testAccount.Receiver_Quantity_Prescribed__c = 20;
        testAccount.Sensor_Quantity_Prescribed__c = 20;
        testAccount.Transmitter_Quantity_Prescribed__c = 20;
        Database.insert(testAccount);
        //order header
        Order_Header__c orderHeader=new Order_Header__c();
        orderHeader.Account__c =testAccount.id;
        orderHeader.Order_Type__c = 'Standard Sales Order';
        insert orderHeader;
        //order item detail
        List<Order_Item_Detail__c> orderItemDetailList = new List<Order_Item_Detail__c>();
        Order_Item_Detail__c oid1  = new Order_Item_Detail__c(Item_Number__c = 'STK-SD-001',Generation__c='G5', Order_Header__c = orderHeader.Id,Quantity__c =1,Quantity_Shipped__c =1,Shipping_Date__c =system.today()-185);
        orderItemDetailList.add(oid1);
        Order_Item_Detail__c oid2  = new Order_Item_Detail__c(Item_Number__c = 'STT-GF-001',Generation__c='G5', Order_Header__c = orderHeader.Id,Quantity__c =1,Quantity_Shipped__c =1,Shipping_Date__c =system.today()-185);
        orderItemDetailList.add(oid2);
        Order_Item_Detail__c oid3  = new Order_Item_Detail__c(Item_Number__c = 'STT-OR-001',Generation__c='G5', Order_Header__c = orderHeader.Id,Quantity__c =1,Quantity_Shipped__c =1,Shipping_Date__c =system.today()-185);
        orderItemDetailList.add(oid3);
        Order_Item_Detail__c oid4  = new Order_Item_Detail__c(Item_Number__c = 'STK-GF-001',Generation__c='G5', Order_Header__c = orderHeader.Id,Quantity__c =1,Quantity_Shipped__c =1,Shipping_Date__c =system.today()-185);
        orderItemDetailList.add(oid4);
        
        
        if(!orderItemDetailList.isEmpty())
        insert orderItemDetailList;
        
        List<Product2> prodList = new List<Product2>();
        Product2 productSku = new Product2();
        productSku.Name = ClsApexConstants.MT25056_SKU;
        productSku.Generation__c = 'G6';
        productSku.Oracle_Product_Id__c = '216946';
        productSku.IsActive = ClsApexConstants.BOOLEAN_TRUE;
        productSku.ProductCode = ClsApexConstants.MT25056_SKU;
        prodList.add(productSku);
        Product2 productSku2 = new Product2();
        productSku2.Name = ClsApexConstants.G6_MDCR_TOUCHSCREEN_KIT;
        productSku2.Generation__c = 'G6';
        productSku2.Oracle_Product_Id__c = '216946';
        productSku2.IsActive = ClsApexConstants.BOOLEAN_TRUE;
        productSku2.ProductCode = ClsApexConstants.G6_MDCR_TOUCHSCREEN_KIT;
        prodList.add(productSku2);
        database.insert(prodList);
        
        List<PricebookEntry> pricebookEntryList = new List<PricebookEntry>();
        PricebookEntry pricebookEntryRec = new PricebookEntry();
        pricebookEntryRec.Product2Id = productSku.Id;
        pricebookEntryRec.Pricebook2Id = Test.getStandardPricebookId();
        pricebookEntryRec.UnitPrice = 10;
        pricebookEntryRec.IsActive = ClsApexConstants.BOOLEAN_TRUE;
        pricebookEntryList.add(pricebookEntryRec);
        
        PricebookEntry pricebookEntryRec2 = new PricebookEntry();
        pricebookEntryRec2.Product2Id = productSku2.Id;
        pricebookEntryRec2.Pricebook2Id = Test.getStandardPricebookId();
        pricebookEntryRec2.UnitPrice = 10;
        pricebookEntryRec2.IsActive = ClsApexConstants.BOOLEAN_TRUE;
        pricebookEntryList.add(pricebookEntryRec2);
        database.insert(pricebookEntryList);
        //insert pricebookEntryRec;
    }
    
    @isTest
    public static void testInsertMdcrFollowupRecord(){
        Id usReorderG6RecType = ClsApexUtility.getRecordTypeIdByDeveloperName(ClsApexConstants.MDCR_FOLLOWUP_OBJECT_API, ClsApexConstants.MFR_US_REORDER_G6_REC_TYPE);
        Account accRec = [Select Id, Latest_Receiver_Generation_Shipped__c, Num_Of_Days_Left_For_Transmitter_Order__c, Latest_Transmitter_Generation_Shipped__c from Account WHERE Party_id__c = '123344' limit 1];
        //created MDCR Follow Up Record 
        MDCR_Followup__c mdcrFollowUpRec = new MDCR_Followup__c();
        mdcrFollowUpRec.Customer__c = accRec.Id;
        mdcrFollowUpRec.Communication_Preference__c = 'Email';
        mdcrFollowUpRec.MDCR_Price_Book__c = Test.getStandardPricebookId();
        mdcrFollowUpRec.Shipping_Method__c = '000001_FEDEX_A_3DS';
        mdcrFollowUpRec.MDCR_Followup_Status__c = 'Open';
        mdcrFollowUpRec.RecordTypeId = usReorderG6RecType;
        Test.startTest();
        Database.insert(mdcrFollowUpRec);
        Test.stopTest();
    }
    
    @isTest
    public static void testBeforeInsertMdcrFollowupRecord(){
        Id usReorderG6RecType = ClsApexUtility.getRecordTypeIdByDeveloperName(ClsApexConstants.MDCR_FOLLOWUP_OBJECT_API, ClsApexConstants.MFR_US_REORDER_G6_REC_TYPE);
        
        Id consumerRecTypeId = ClsApexUtility.getRecordTypeIdByDeveloperName(ClsApexConstants.ACCOUNT_API_NAME, ClsApexConstants.ACCOUNT_REC_TYPE_CONSUMERS_LABEL);
        /*Account testAccount=TestDataBuilder.testAccount();
        testAccount.Default_Price_Book__c=Test.getStandardPricebookId();
        testAccount.PersonEmail = 'Test@gmail.com.beforeinsert';
        testAccount.BillingStreet='abc';
        testAccount.BillingCity ='Bang';
        testAccount.BillingState ='KN';
        testAccount.BillingPostalCode='56037';
        testAccount.ShippingStreet  ='Bang';
        testAccount.ShippingCity='Bang';
        testAccount.ShippingState='KN';
        testAccount.ShippingPostalCode='50037';
        testAccount.Party_ID__c ='123357';
        testAccount.Receiver_Quantity_Prescribed__c = 20;
        testAccount.Sensor_Quantity_Prescribed__c = 20;
        testAccount.Transmitter_Quantity_Prescribed__c = 20;*/
        Account testAccount = [Select Id, Latest_Receiver_Generation_Shipped__c, Num_Of_Days_Left_For_Transmitter_Order__c, Latest_Transmitter_Generation_Shipped__c from Account WHERE Party_id__c = '123344' limit 1];
        Test.startTest();
        //Database.insert(testAccount);
        
        //order header
        Order_Header__c orderHeader=new Order_Header__c();
        orderHeader.Account__c =testAccount.id;
        orderHeader.Order_Type__c = 'Standard Sales Order';
        insert orderHeader;
        List<Order_Item_Detail__c> oidList = new List<Order_Item_Detail__c>();
        Order_Item_Detail__c oid1  = new Order_Item_Detail__c(Item_Number__c = 'STR-GF-001',Generation__c='G6', Order_Header__c = orderHeader.Id,Quantity__c =1,Quantity_Shipped__c =1,Shipping_Date__c =system.today()-185);
        Order_Item_Detail__c oid2  = new Order_Item_Detail__c(Item_Number__c = 'STK-GF-001',Generation__c='G6', Order_Header__c = orderHeader.Id,Quantity__c =1,Quantity_Shipped__c =1,Shipping_Date__c =system.today()-185);
        Order_Item_Detail__c oid3  = new Order_Item_Detail__c(Item_Number__c = 'STK-MC-001',Generation__c='G6', Order_Header__c = orderHeader.Id,Quantity__c =1,Quantity_Shipped__c =1,Shipping_Date__c =system.today()-185);
        oidList.add(oid1);
        oidList.add(oid2);
        oidList.add(oid3);
        database.insert(oidList);
        
        
        Profile profile = [Select Id, Name from Profile where Name ='Data Integrator'];
        User insideSalesUser = new User(Alias = 'newUser', Email='newDataIntgUser@testorg.com',
                                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                                        LocaleSidKey='en_US', ProfileId = profile.Id,
                                        TimeZoneSidKey='America/Los_Angeles', UserName='newInsideSalesUser@testorg.com');
        //created MDCR Follow Up Record 
        system.runAs(insideSalesUser){
            MDCR_Followup__c mdcrFollowUpRec = new MDCR_Followup__c();
            mdcrFollowUpRec.Customer__c = testAccount.Id;
            mdcrFollowUpRec.Communication_Preference__c = 'Email';
            mdcrFollowUpRec.MDCR_Price_Book__c = Test.getStandardPricebookId();
            mdcrFollowUpRec.Shipping_Method__c = '000001_FEDEX_A_3DS';
            mdcrFollowUpRec.MDCR_Followup_Status__c = 'Open';
            mdcrFollowUpRec.RecordTypeId = usReorderG6RecType;
            Database.insert(mdcrFollowUpRec);
            Test.stopTest();
        }
    }
    
    @isTest
    public static void testBeforeUpdateRoutine(){
        Account accountRec = [SELECT ID FROM ACCOUNT WHERE PersonEmail = 'Test@gmail.com1241' limit 1];
        
        MDCR_Followup__c mdcrFollowUpRec = new MDCR_Followup__c();
        mdcrFollowUpRec.Customer__c = accountRec.Id;
        mdcrFollowUpRec.Communication_Preference__c = 'Email';
        mdcrFollowUpRec.MDCR_Price_Book__c = Test.getStandardPricebookId();
        mdcrFollowUpRec.Shipping_Method__c = '000001_FEDEX_A_3DS';
        mdcrFollowUpRec.MDCR_Followup_Status__c = 'Closed';
        
        insert mdcrFollowUpRec;
        List<Order_Header__c> orderHeaderList = new List<Order_Header__c>();
        Order_Header__c orderHeader=new Order_Header__c();
        orderHeader.Account__c =accountRec.id;
        orderHeader.Order_Type__c = 'Standard Sales Order';
        orderHeader.MDCR_Followup__c = mdcrFollowUpRec.Id;
        orderHeaderList.add(orderHeader);
        
        Order_Header__c orderHeader1=new Order_Header__c();
        orderHeader1.Account__c =accountRec.id;
        orderHeader1.Order_Type__c = 'Standard Sales Order';
        orderHeader1.MDCR_Followup__c = mdcrFollowUpRec.Id;
        orderHeaderList.add(orderHeader1);
        
        insert orderHeaderList;
        Test.startTest();
        try{
            mdcrFollowUpRec.MDCR_Followup_Status__c = 'Open';
            update mdcrFollowUpRec;
        }catch(Exception e){
            system.debug('Exception occurred');
        }
        Test.stopTest();
    }
    
    @isTest
    public static void testBeforeInsert(){
        Order_Header__c orderHeaderRec = [Select Id, Account__c, MDCR_Followup__c from Order_Header__c where Account__c in (Select Id from Account where PersonEmail = 'Test@gmail.com1241')];
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Inside Sales'];
        User insideSalesUser = new User(Alias = 'newUser', Email='newusertaskdeletetest@testorg.com',
                                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                                        LocaleSidKey='en_US', ProfileId = p.Id,
                                        TimeZoneSidKey='America/Los_Angeles', UserName='newusertaskdeletetest@testorg.com');
        
        MDCR_Followup__c mdcrFollowUpRec = new MDCR_Followup__c();
        mdcrFollowUpRec.Customer__c = orderHeaderRec.Account__c;
        mdcrFollowUpRec.Communication_Preference__c = 'Email';
        mdcrFollowUpRec.MDCR_Price_Book__c = Test.getStandardPricebookId();
        mdcrFollowUpRec.Shipping_Method__c = '000001_FEDEX_A_3DS';
        mdcrFollowUpRec.MDCR_Followup_Status__c = 'Closed';
        mdcrFollowUpRec.Order_Number__c =null;
        mdcrFollowUpRec.MDCR_Followup_Date__c = system.today();
        mdcrFollowUpRec.Subscription_Only_MFR__c = false;
        
        insert mdcrFollowUpRec;
        Test.startTest();
        MDCR_Followup__c mdcrFollowUpRec1 = new MDCR_Followup__c();
        mdcrFollowUpRec1.Customer__c = orderHeaderRec.Account__c;
        mdcrFollowUpRec1.Communication_Preference__c = 'Email';
        mdcrFollowUpRec1.MDCR_Price_Book__c = Test.getStandardPricebookId();
        mdcrFollowUpRec1.Shipping_Method__c = '000001_FEDEX_A_3DS';
        mdcrFollowUpRec1.MDCR_Followup_Status__c = 'Closed';
        try{
            system.runAs(insideSalesUser){
                insert mdcrFollowUpRec1;
            }
        }catch(Exception e){
            system.debug('exception occurred');
        }
        
        ClsMdcrFollowupTriggerHandler.addDefaultSkuToMFR(new list<MDCR_Followup__c>{mdcrFollowUpRec1},new set<id>{Test.getStandardPricebookId()});
        ClsMdcrFollowupTriggerHandler.addG6ReceiverToMdcrRecord(new list<MDCR_Followup__c>{mdcrFollowUpRec1},new set<id>{Test.getStandardPricebookId()});
        Test.stopTest();
    }
    
}