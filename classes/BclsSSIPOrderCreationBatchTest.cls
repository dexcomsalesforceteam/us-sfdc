/*
  @Author         : LTI
  @Date Created   : 09/26/2018
  @Description    : Handles test cases for SSIP Order Batch
*/
@isTest
public class BclsSSIPOrderCreationBatchTest {
    
    @isTest
    Public static void ssipOrderCreationBatchTest(){
        Test.startTest();
        //create PriceBooks
        Id pricebookId = Test.getStandardPricebookId();
        
        Product2 prod = new Product2(Name = 'BUN-GF-003',  Family = 'Hardware', Generation__c = 'G5');
        insert prod;
        
        PricebookEntry standardPrice = new PricebookEntry( Pricebook2Id = pricebookId, Product2Id = prod.Id,
                                                           UnitPrice = 100, IsActive = true, UseStandardPrice = false);
        insert standardPrice;
        
        //create Prescriber Account
        recordtype recTypeId=[select id,name from recordtype where name='prescriber' limit 1];
        account prescriberAccount=new account();
        prescriberAccount.lastname='prescriber Account';
        prescriberAccount.recordtypeId=recTypeId.id;
        prescriberAccount.Inactive__c =false;        
        insert prescriberAccount;
        
        //Create Payor Account
        recordtype recTypeIdPayor = [select id,name from recordtype where name='Payor' limit 1];
        account payorAccount = new account();
        payorAccount.Name = 'Test Payor';
        payorAccount.recordtypeId = recTypeIdPayor.Id;
        insert payorAccount;
        
        //create Consumer Account
        Account testAccount=TestDataBuilder.testAccount();
        testAccount.Payor__c = payorAccount.Id;
        testAccount.Default_Price_Book__c=pricebookId;
        testAccount.PersonEmail = 'Test@gmail.com1241';
        testAccount.BillingStreet='abc';
        testAccount.BillingCity ='Bang';
        testAccount.BillingState ='KN';
        testAccount.BillingCountry ='US';
        testAccount.BillingPostalCode='560037';
        testAccount.ShippingStreet  ='Bang';
        testAccount.ShippingCity='Bang';
        testAccount.ShippingState='KN';
        testAccount.ShippingCountry='US';
        testAccount.ShippingPostalCode='560037';
        testAccount.Party_ID__c ='123344';
        testAccount.Prescribers__c=prescriberAccount.id;     
        insert testAccount;
        
        //create Primary address
        Address__c addrs2 = TestDataBuilder.getAddressList(testAccount.Id, true,'SHIP_TO', 1)[0];
        addrs2.Oracle_Address_ID__c ='123455';
        insert addrs2;
       
        testAccount.Primary_Ship_To_Address__c =addrs2.Id;
        update testAccount;
       
        //create Order shippied to populate Last shipped date
        Order_Header__c orderHeader=new Order_Header__c();
        orderHeader.Account__c =testAccount.id;
        orderHeader.Order_Type__c = 'Standard Sales Order';
        insert orderHeader;
        
        Order_Item_Detail__c oid1  = new Order_Item_Detail__c(Item_Number__c = 'BUN-OR-TX',Generation__c='G6', Order_Header__c = orderHeader.Id,Quantity__c =1,Quantity_Shipped__c =1,Shipping_Date__c =system.today()-185);
        insert oid1;
        
        //query account to get all roll up summary fields populated after order creation
        List<Account> thisAccount = new List<Account>([SELECT id, Latest_Transmitter_Ship_Date__c, Next_Possible_Date_For_Transmitter_Order__c FROM Account where Id =:testAccount.Id]);
        
        //create SSIP Rule
        SSIP_Rule__c ssipRule=createSSIPRule(thisAccount[0].Id, System.today(), System.today()+360);
        insert ssipRule;
        
        //get list of Schedules Created
        List<SSIP_Schedule__c> ScheduleList = new List<SSIP_Schedule__c>([SELECT Id, Scheduled_Shipment_Date__c, Actual_Shipment_Date__c from SSIP_Schedule__c where SSIP_Rule__c =: ssipRule.id]);
        System.debug('ScheduleList >>' + ScheduleList);
        
        //Update Scheduled Shipment Date to satisfy Batch Entry Criteria
        SSIP_Schedule__c thisSchedule = ScheduleList[0];
        thisSchedule.Scheduled_Shipment_Date__c = System.today() + 2;
        thisSchedule.Status__c = 'Eligibility Verified';
        update thisSchedule;
        System.debug('thisSchedule >>' + thisSchedule);        
        
        //execute batch class
        BclsSSIPOrderCreationBatch bclsOrderCretion=new BclsSSIPOrderCreationBatch('BclsSSIPOrderCreationBatchSchedule');
        database.executeBatch(bclsOrderCretion);        
        Test.stopTest();        
    }
     public static SSIP_Rule__c createSSIPRule(string accountId, Date startDate, Date endDate){
        
        SSIP_Rule__c ssipRule=new SSIP_Rule__c();
        ssipRule.Account__c = accountId;
        ssipRule.Rule_Start_Date__c = startDate;
        ssipRule.Rule_End_Date__c = endDate;
        ssipRule.Product__c = 'G6 | BUN-OR-TX6 | 1';
        ssipRule.Price_Book_New__c = 'Account Default Pricebook';
        ssipRule.First_Shipment_Date__c= startDate.addDays(1);
        ssipRule.Disclaimer__c =true;
        ssipRule.Frequency_In_Days_Number__c = 180;
        
        return ssipRule;
    }
    
}