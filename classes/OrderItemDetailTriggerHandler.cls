/**
* @description	: Trigger handler for the OrderItemDetailMasterTrigger
* @author		: Noy De Goma@CSHERPAS
* @date		: 03.24.2016
**/
/*
* @description	: Trigger handler for the OrderItemDetailMasterTrigger. Modified the code to pull Product generation values from product object.
* @author		:Abhishek Parghi
* @date		    : 03.08.2017
*/

public with sharing class OrderItemDetailTriggerHandler {
    /*  public static void onAfterInsert(list <Order_Item_Detail__c> orderItemDetailList){
recalculateOrderHeader(orderItemDetailList);
}
public static void onAfterUpdate(list <Order_Item_Detail__c> orderItemDetailList){
recalculateOrderHeader(orderItemDetailList);
}

private static void recalculateOrderHeader(list <Order_Item_Detail__c> orderItemDetailList){
set <Id> orderHeaderId = new set <Id>();
for(Order_Item_Detail__c oid : orderItemDetailList){
if(!orderHeaderId.contains(oid.Order_Header__c)){
orderHeaderId.add(oid.Order_Header__c);
}
}
Database.executeBatch(new UpdateOrderHeaderBatch(orderHeaderId), 2000);
}*/
    Public Static void UpdateGenerationInfo(list<Order_Item_Detail__c> orderItemDetailList){
        //set to store ItemNumber values
        set<String> ItemNumbers = new set<String>();
        //map to store ProductCode and Generation values  
        map<string,string> GenerationMap=new map<string,string>();  
        for(Order_Item_Detail__c O: orderItemDetailList) {
            if(O.Item_Number__c != null){    
                ItemNumbers.add(O.Item_Number__c);
            }
        }
        if(ItemNumbers.size()>0){
            //SOQL to get ProductCode and Generation values from the product object   
            for(product2 p : [Select id,ProductCode,Generation__c from Product2 WHERE Productcode IN :ItemNumbers]){
                GenerationMap.put(p.ProductCode, P.Generation__c);
            }
            IF(GenerationMap.size()>0){
                for(Order_Item_Detail__c O: orderItemDetailList){
                    if(O.Item_Number__c != null){    
                        String Generation = GenerationMap.get(O.Item_Number__c);        
                        if(Generation != null){
                            O.Generation__c = Generation;
                        }
                    }  
                } 
            }   
        }
    }
    /*
* @description	: Product Generation field update on accounts from OrderItemDetail. 
* @author		:Abhishek Parghi
* @date		    : 03.09.2017
*/
    Public Static void UpdateGenerationOnAccount(list<Order_Item_Detail__c> orderItemDetailList){
        Map<ID, Account> Accts = new Map<ID, Account>();
        Set<Id> listIds = new Set<Id>();
        System.debug('##AP ' + orderItemDetailList.size() + ' ##');
        for(Order_Item_Detail__c OLD : orderItemDetailList){
            listIds.add(OLD.account_id__C);
        }
        if(listIds.size()>0){
            Accts = new Map<Id, Account>([SELECT id,Generation__c FROM Account WHERE ID IN :listIds]);
            if(Accts.size()> 0){
                for(Order_Item_Detail__c OLID : orderItemDetailList){
                    Account Acc = Accts.get(OLID.ACC_ID__c );
                    IF(Acc != Null){
                        Acc.Generation__c  = OLID.generation__C;  
                    }
                } 
            }   
            update Accts.values();   
        }     
    }

    public static void handleUpdateMIGeneration(Map<Id, Order_Item_Detail__c> newOrderItems, Map<Id, Order_Item_Detail__c> oldOrderItems) {
        List<Order_Item_Detail__c> updatedOrderItems = new List<Order_Item_Detail__c>();
        for (Id id : newOrderItems.keySet()) {
            Order_Item_Detail__c newOrderItem = newOrderItems.get(id);
            Order_Item_Detail__c oldOrderItem = oldOrderItems.get(id);

            if (newOrderItem.Tracking_Number__c != oldOrderItem.Tracking_Number__c && newOrderItem.Tracking_Number__c != null) {
                updatedOrderItems.add(newOrderItem);
            }
        }
        if (updatedOrderItems.size() > 0) {
            generateMarketingInteractions(updatedOrderItems);
        }
    }

    public static void generateMarketingInteractions(List<Order_Item_Detail__c> orderItemDetails) {
        Map<String, Marketing_Interaction__c> interactionsByTrackingNumbers = new Map<String, Marketing_Interaction__c>();

        List<Id> orderItemDetailIds = new List<Id>();
        for(Order_Item_Detail__c detail: orderItemDetails) {
            orderItemDetailIds.add(detail.Id);
        }

        Map<Id, Order_Item_Detail__c> detailsWithHeaders = new Map<Id, Order_Item_Detail__c>([
                SELECT Id, Order_Header__r.Order_Type__c
                FROM Order_Item_Detail__c
                WHERE Id IN: orderItemDetailIds
        ]);

        for (Order_Item_Detail__c orderItemDetail : orderItemDetails) {

            if (validateOrderHeaderOrderType(detailsWithHeaders.get(orderItemDetail.Id).Order_Header__r) && orderItemDetail.Tracking_Number__c != null && !interactionsByTrackingNumbers.containsKey(orderItemDetail.Tracking_Number__c)) {

                Marketing_Interaction__c interaction = new Marketing_Interaction__c();
                interaction.Communication_Type__c = 'Shipment Confirmation';
                interaction.Source_Record_Id__c = orderItemDetail.Order_Header__c;
                interaction.Account__c = orderItemDetail.Account_ID__c;
                interaction.Related_Information__c = orderItemDetail.Tracking_Number__c;

                if (orderItemDetail.Tracking_Number__c != null) {
                    interactionsByTrackingNumbers.put(orderItemDetail.Tracking_Number__c, interaction);
                }
            }
        }

        List<Marketing_Interaction__c> filteredMarketingInteractions = MarketingInteractionUtils.filterExistingInteractions(interactionsByTrackingNumbers.values());
        //The reason why we are using false as the 2nd parameter is because we are trying to stop duplicate 'Shipment Confirmation'
        // records from being inserted.  The above method filterExistingInteractions would typically stop duplicates from being inserted but we 
        //  believe that since these Orders are being mass updated and inserted every night with the bulk API that we have concurrent triggers
        //  happening and we are having a race condition so we are ending up with duplicates.  We have added a 'Unique' field called
        //  Unique_Shipment_Confirmation__c that will be populated so that when the database.insert runs below these duplicates will throw an 
        //  error.  But we only want the duplicate errors to not be committed.  The false param will allow the successful inserts to be committed.
        database.insert(filteredMarketingInteractions, false);
    }

    private static Boolean validateOrderHeaderOrderType(Order_Header__c orderHeader) {
        if (orderHeader == null) return false;
        String orderType = orderHeader.Order_Type__c;
        return orderType == 'Standard Sales Order' || orderType == 'MC Standard Sales Order' || orderType == 'Subscription';
    }


}