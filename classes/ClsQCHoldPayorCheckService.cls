/*************************************************
* @author      : Dexcom, LTI
* @date        : April, 2019
* @description : Controller QC Hold : Payor Check Service Call
*************************************************/
public class ClsQCHoldPayorCheckService {
    
    @AuraEnabled
    public static Map<String, String> getPayorDetails(String orderId, String accountId, String orderType, Boolean isOrderActivated) {
        Map<String, String> fieldValueMap = new Map<String, String>();//Field value map returns back to the client controller
        System.debug('*** Order Type in Payor Check ** ' + orderType);
        fieldValueMap = ClsOrderAuditHandler.getAuditDetailsforCheck('Payor', orderId);        
        System.debug('Fetched Payor Details : '+ fieldValueMap);
        
        if(fieldValueMap!= null && !fieldValueMap.isEmpty() && (isOrderActivated || Boolean.valueOf(fieldValueMap.get('bypassCheck')))){
            System.debug(' returning Map : ' + fieldValueMap);
            return fieldValueMap;
        }else{
            fieldValueMap.put('bypassCheck', 'false');
            
            //Get the account details
            Account accnt = [SELECT Id,Age__c,Payor__r.Indirect_contract__c,Payor__r.Name,Dexcom_Employee__c,billingstate,Diagnosis_Code_1_Acc__c,Diagnosis_Type_Formula__c,Type_of_Insulin_Therapy__c,Payor__r.Payor_Contract_End_Date__c,Payor__r.Direct_Contract__c FROM ACCOUNT WHERE Id =:accountId];
            
            //Param 1 :
            String inContractCheck = ((accnt.Payor__r.Name == 'UNITED HEALTHCARE' && accnt.billingstate != 'CA' && accnt.Dexcom_Employee__c == false) || ((accnt.Payor__r.Payor_Contract_End_Date__c == null || accnt.Payor__r.Payor_Contract_End_Date__c < System.Today()) && accnt.Payor__r.Indirect_contract__c == false ) )? 'false' : 'true';
            fieldValueMap.put('inContract', inContractCheck);
            
            //Param 2 : 
            //if(accnt.Payor__r.Payor_Contract_End_Date__c != null){ CRMSF:5338 : Contract End should blank out if payor Contract End date is deleted.
            Datetime Payor_Contract_End_Date = accnt.Payor__r.Payor_Contract_End_Date__c;
            if(Payor_Contract_End_Date != Null)
                fieldValueMap.put('contractEndDate', String.valueOf(Payor_Contract_End_Date.format('MM/dd/yyyy','GMT')));
            else
                fieldValueMap.put('contractEndDate','');
           // }
            //Param 3 : 
            string directOrIndirectContract = (accnt.Payor__r.Direct_Contract__c || accnt.Payor__r.Indirect_contract__c) ? 'true' : 'false';
            fieldValueMap.put('inNetWork', (accnt.Payor__r.Name == 'UNITED HEALTHCARE' && accnt.billingstate != 'CA' && accnt.Dexcom_Employee__c == false)? 'false' : directOrIndirectContract);
            
            //Param 4 :
            //Check for Diagnosis type and age 
            String diagnosisCoveredCheck = 'false';
            String payorDiagnosisType = '';
            String insulinTherapyRequired = '';
            String patientInsulinTherapy;
            String insulinTherapy = '';
            String payorId = accnt.Payor__c;
            String separator = ', ';
            Map<String, String> diagnosisToInsulinTherapyMap = new Map<String, String>();//Map between the diagnosis type and insulin therapy
            Set<String> diagnosisSet = new Set<String>();//Set holds unique value of the Diagnosis type
            List<String> diagnosisList = new List<String>();//List created from the diagnosis set
            
            List<Rules_PayorToDiagnosis__c> payorDiagnosisRulesList = new List<Rules_PayorToDiagnosis__c>([SELECT Diagnosis__c, MinimumAge__c, MaximumAge__c, Covered__c,InsulinTherapyRequired__c,Diagnosis_Code_Covered__c FROM Rules_PayorToDiagnosis__c WHERE Covered__c = true and Payor__c = :payorId]);//Get all the diagnosis payor rules
            
            //Payor rules for diagnosis should exist and account should have diagnosis code selected
            if(!payorDiagnosisRulesList.isEmpty())
            {
                //Get possible Diagnosis type from the Payor matrix
                for(Rules_PayorToDiagnosis__c diagnosisRule : payorDiagnosisRulesList)
                {
                    diagnosisSet.add(diagnosisRule.Diagnosis__c);
                    payorDiagnosisType = payorDiagnosisType + ' ' + diagnosisRule.Diagnosis__c;
                    insulinTherapy = diagnosisRule.InsulinTherapyRequired__c == true ? 'Yes' : 'No';
                    diagnosisToInsulinTherapyMap.put(diagnosisRule.Diagnosis__c, insulinTherapy);
                }
                diagnosisList.addAll(diagnosisSet);//Convert set to List
                payorDiagnosisType = String.join(diagnosisList, separator);
                insulinTherapyRequired = diagnosisToInsulinTherapyMap.get(accnt.Diagnosis_Type_Formula__c);
                
                if(accnt.Diagnosis_Type_Formula__c != '' && accnt.Age__c > 0)
                {
                    for(Rules_PayorToDiagnosis__c diagnosisRule : payorDiagnosisRulesList)
                    {
                        String diagnosisCodeCovered = diagnosisRule.Diagnosis_Code_Covered__c;
                        //If the account age is within the covered range then proceed
                        if(accnt.Age__c >= diagnosisRule.MinimumAge__c && accnt.Age__c <= diagnosisRule.MaximumAge__c)
                        {
                            //If there isn't any diagnosis code requirement then just check the diagnosis type
                            if(diagnosisCodeCovered == null)
                            {
                                if(accnt.Diagnosis_Type_Formula__c == diagnosisRule.Diagnosis__c)
                                {
                                    //Check if the rule requires insulin therapy and correct insulin therapy is selected 
                                    if(!diagnosisRule.InsulinTherapyRequired__c || (diagnosisRule.InsulinTherapyRequired__c && (accnt.Type_of_Insulin_Therapy__c == 'Insulin using Pump therapy' || accnt.Type_of_Insulin_Therapy__c == 'Insulin using multiple daily injections')))
                                        diagnosisCoveredCheck = 'true'; 
                                }                               
                            }
                            else
                            {
                                //Check if the diagnosis code is covered
                                if(!String.isBlank(accnt.Diagnosis_Code_1_Acc__c) && 
                                   diagnosisCodeCovered.contains(accnt.Diagnosis_Code_1_Acc__c) )
                                {
                                    //Check if the rule requires insulin therapy and correct insulin therapy is selected 
                                    if(!diagnosisRule.InsulinTherapyRequired__c || (diagnosisRule.InsulinTherapyRequired__c && (accnt.Type_of_Insulin_Therapy__c == 'Insulin using Pump therapy' || accnt.Type_of_Insulin_Therapy__c == 'Insulin using multiple daily injections')))
                                        diagnosisCoveredCheck = 'true'; 
                                }
                            }
                        }
                    }
                }
            }
            fieldValueMap.put('diagnosisCovered', diagnosisCoveredCheck);
            
            //Param 5 : 
            fieldValueMap.put('diagnosisType', string.valueOf(accnt.Diagnosis_Type_Formula__c));
            //Param 6 : 
            fieldValueMap.put('payorDiagnosisType', string.valueOf(payorDiagnosisType));
            //Param 7 : 
            fieldValueMap.put('insulinTherapyRequired', string.valueOf(insulinTherapyRequired));
            //Param 8 : 
            fieldValueMap.put('patientInsulinTherapy', string.valueOf(accnt.Type_of_Insulin_Therapy__c));
            return fieldValueMap; 
        } 
    }
}