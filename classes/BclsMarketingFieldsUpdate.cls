global class BclsMarketingFieldsUpdate implements Database.Batchable<sObject>
{
    Map<id,Marketing_Account__c> updateMarketingAccMap = new Map<Id,Marketing_Account__c>();
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        String query = 'SELECT Id, Age__c, Age_Formula__c, Has_Not_Made_Recent_Order__c, Has_Not_Made_Recent_Order_Formula__c, Receiver_Eligible__c,Receiver_Eligible_Formula__c, Sensor_Eligible__c,Sensor_Eligible_Formula__c, Transmitter_Eligible__c,Transmitter_Eligible_Formula__c, Receiver_Eligible_Mismatch__c,Sensor_Eligible_Mismatch__c,Trans_Eligible_Mismatch__c   FROM Marketing_Account__c WHERE Account__r.RecordType.Name = \'Consumers\' and Account__r.Date_Of_Death__c = null and (Non_Eligibility_Fields_Mismatch__c = true Or Receiver_Eligible_Mismatch__c = true Or Sensor_Eligible_Mismatch__c = true Or Trans_Eligible_Mismatch__c = true)';
    return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Marketing_Account__c> scope)
    {
        for(Marketing_Account__c markAccnt : scope)
        {
            If (markAccnt.Age__c <> markAccnt.Age_Formula__c)
            markAccnt.Age__c = markAccnt.Age_Formula__c;
            If(markAccnt.Has_Not_Made_Recent_Order__c <> markAccnt.Has_Not_Made_Recent_Order_Formula__c)
            markAccnt.Has_Not_Made_Recent_Order__c = markAccnt.Has_Not_Made_Recent_Order_Formula__c;
            If(markAccnt.Receiver_Eligible_Mismatch__c)
            markAccnt.Receiver_Eligible__c = markAccnt.Receiver_Eligible_Formula__c;
            If(markAccnt.Sensor_Eligible_Mismatch__c)
            markAccnt.Sensor_Eligible__c = markAccnt.Sensor_Eligible_Formula__c;
            If(markAccnt.Trans_Eligible_Mismatch__c)
            markAccnt.Transmitter_Eligible__c = markAccnt.Transmitter_Eligible_Formula__c;
            updateMarketingAccMap.put(markAccnt.Id, markAccnt);
        }
        
        if(!updateMarketingAccMap.isEmpty())
        {
            //try{
        update updateMarketingAccMap.values();
      //}

      //{
            //    System.debug('getNumDml=' + de.getNumDml());
            //    for(Integer i=0;i<de.getNumDml();i++) {
            //        System.debug('getDmlFieldNames=' + de.getDmlFieldNames(i));
            //        System.debug('getDmlMessage=' + de.getDmlMessage(i));
            //    }
      //}
        }
    }
    global void finish(Database.BatchableContext BC)
    {
      
    }

}