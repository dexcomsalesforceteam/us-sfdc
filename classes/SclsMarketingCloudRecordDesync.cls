/**
 * SclsMarketingCloudRecordDesync
 * @author Terry Luschen(Sundog)
 * @date 01/27/2020 
 */
    global class SclsMarketingCloudRecordDesync implements Schedulable{
    public static String CRON_EXP = '0 15 14 * * ?'; //This would run at 2:15 PM daily

    global void execute(SchedulableContext sc) {
        Database.executeBatch(new BclsMarketingCloudRecordDesync(), BclsMarketingCloudRecordDesync.batchSize);
    }

    // to kick off the job without using the UI, execute following in an anonymous window:
    // System.schedule('BclsMarketingCloudRecordDesync', SclsMarketingCloudRecordDesync.CRON_EXP, new SclsMarketingCloudRecordDesync());

}