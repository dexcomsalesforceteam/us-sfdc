public class clsCreateMFRSubscriptionPageExtension {
    
    public final Account acc;
    
    public clsCreateMFRSubscriptionPageExtension(ApexPages.StandardController stdController) {
        this.acc = (Account)stdController.getRecord();
        system.debug('Accunt' +acc);
        
    }
    
    
    public pagereference getretrunURL()
    {
            PageReference retPage = new PageReference('/'+apexPages.CurrentPage().getParameters().get('id')); 

return retPage;

    }

   
    public boolean getCheckAddress ()
    {
      Account  acc = [SELECT ID,Primary_Ship_To_Address__c from Account WHERE Id =: acc.id LIMIT 1 ];
     List<Address__C> addList = new List<Address__c>();
     addList = [SELECT Id,Zip_Postal_Code__c,City__c,State__c FROM Address__c WHERE ID =: acc.Primary_Ship_To_Address__c Limit 1 ];
        
        Boolean result = false;
       
        
        if(addList[0].Zip_Postal_Code__c == null || addList[0].State__c == null || addList[0].City__c == null)
        {
        	result = true;
        }
        
            
        
        return result;
    }

}