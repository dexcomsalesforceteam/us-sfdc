/******************************************************************************************
Description: This class will invoke SmartyStreetsAPI to get the address related information 
******************************************************************************************/
public class ClsAddressVerificationFromExternalSource {
    
    Static Map<String, String> smartyStreetAPIDetailMap = new Map<String, String>(); 
    
    //Method will get the API Login details for SmartyStreetsZipCodeAPI From CustomMetadataTYpe
    public static Map<String, String> getSmartyStreetsZipCodeAPIDetails() 
    {
        
        for(Smarty_Streets_Zip_Code_API_Details__mdt smartyStreetAPIDetail : [SELECT Auth_Id__c, Auth_Token__c, URL__c FROM Smarty_Streets_Zip_Code_API_Details__mdt WHERE Label = 'SmartyStreetsZipCode'])
        {
            smartyStreetAPIDetailMap.put('AuthId', smartyStreetAPIDetail.Auth_Id__c);
            smartyStreetAPIDetailMap.put('AuthToken', smartyStreetAPIDetail.Auth_Token__c);
            smartyStreetAPIDetailMap.put('URL', smartyStreetAPIDetail.URL__c);
        }
        return smartyStreetAPIDetailMap;
    }
    
    @future (callout=true)
    //Method will process the address records to get the county name
    public static void processAddressToGetCountyName(List<Id> addressIds) 
    {
        system.debug('******processAddressToGetCountyName - Invoked future call');
        List<Address__c> updateAddressWithCountyNamesList = new List<Address__c>();
        
        for(Address__c addr : [SELECT AccountNumber__c, City__c, State__c, Zip_Postal_Code__c, County__c FROM Address__c WHERE Id IN : addressIds])
        {
            system.debug('******Entered for loop ' + addr.City__c);
            addr.County__c = getCountyNameFromSmartyStreets(addr.City__c, addr.State__c, addr.Zip_Postal_Code__c);
            addr.Send_To_Oracle__c = true;
            updateAddressWithCountyNamesList.add(addr);
        }
        try{update updateAddressWithCountyNamesList;}
        catch (DmlException de) {
            Integer numErrors = de.getNumDml();
            System.debug('getNumDml=' + numErrors);
            for(Integer i=0;i<numErrors;i++) {
                System.debug('getDmlFieldNames=' + de.getDmlFieldNames(i));
                System.debug('getDmlMessage=' + de.getDmlMessage(i));
            }
        }
    }

    //Method to call SmartyStreetZipCode API and fetch the address details 
    public static String getCountyNameFromSmartyStreets (String city, String state, String zip)
    {
        String countyName;
        //Get the API Details
        getSmartyStreetsZipCodeAPIDetails();
        String smartyStreetZipCodeAPIURL =  smartyStreetAPIDetailMap.get('URL');
        String smartyStreetZipCodeAPIAuthId =  smartyStreetAPIDetailMap.get('AuthId');
        String smartyStreetZipCodeAPIAuthToken =  smartyStreetAPIDetailMap.get('AuthToken');
        //JSON to Apex conversion
        List<ClsSmartyStreetZipCodeAPIResponse> apiResponseMessageInApexList;
        List<ClsSmartyStreetZipCodeAPIResponse.zipcodes> apiResponseMessageZipCodesList;
        
        try{
            String invokeSmartyStreetAPIUrl = smartyStreetZipCodeAPIURL + 'auth-id=' + smartyStreetZipCodeAPIAuthId + '&auth-token=' + smartyStreetZipCodeAPIAuthToken + '&city=' + city.replace(' ', '%20') + '&state=' + state + '&zipcode=' + zip;
            system.debug('APIUrl is ' + invokeSmartyStreetAPIUrl);
            HttpRequest req = new HttpRequest();
            req.setMethod('GET');
            req.setEndpoint(invokeSmartyStreetAPIUrl);
            Http http = new Http();
            HttpResponse res = http.send(req);
            Integer statusCode = res.getStatusCode();
            system.debug('statusCode is ' + statusCode);
            if(statusCode == 200){
                apiResponseMessageInApexList = ClsSmartyStreetZipCodeAPIResponse.parse(res.getBody());
                if(!apiResponseMessageInApexList.isEmpty())
                {
                   apiResponseMessageZipCodesList =  apiResponseMessageInApexList[0].zipcodes;
                   if(!apiResponseMessageZipCodesList.isEmpty())
                   {
                       countyName = apiResponseMessageZipCodesList[0].county_name;
                       system.debug('Value of county is ' + apiResponseMessageZipCodesList[0].county_name);
                   }
                }
            }
        }
        catch(exception e){
            //Handling exception
            system.debug(e.getMessage());
        }
        return countyName;
    }
}