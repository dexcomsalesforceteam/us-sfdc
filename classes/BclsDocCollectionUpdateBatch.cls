/***
*@Author        : Priyanka Kajawe
*@Date Created  : 05-Sept-2018
*@Description   : 
*@Version on 04-10-2019 to refine query criteria by adding opportunity types and record type to WHERE clause by Sundog. Scheduled by BclsDocCollectionUpdateSched
***/

global with sharing class BclsDocCollectionUpdateBatch implements Database.Batchable<sObject> {
    global String soqlQuery; //Query string
    
    //Start Method
    global Database.QueryLocator start(Database.BatchableContext BC){   
        Date daysFromNow = Date.Today().addDays(-14) ;  //to check if Opty is in Step 2 from last 14 days
        
        //prepare SOQL
        soqlQuery = 'SELECT ID, Onboarding_Steps__c, Onboarding_Step_Update_Date__c,AccountId FROM Opportunity ';
        soqlQuery += 'WHERE ';
        soqlQuery += 'Onboarding_Steps__c = \'Document Collection\' AND StageName = \'4. Doc Collection\' AND '; 
        soqlQuery += 'Onboarding_Step_Update_Date__c != Null and Onboarding_Step_Update_Date__c <= :daysFromNow AND';
        soqlQuery += '(Type = \'NEW SYSTEM\' OR Type = \'Medicare – New Patient\' OR Type = \'Physician Referral\' OR Type = \'OOW Receiver Only\' ';
        soqlQuery += 'OR Type = \'OOW SYSTEM\' OR Type = \'OOW Transmitter Only\' OR Type = \'Sensor-Reorder\') AND RecordType.Name = \'US Opportunity\'';
        system.debug('SOQL Query took effect is ' + soqlQuery);
        return Database.getQueryLocator(soqlQuery);
    }
    //Execute Method  
    global void execute(Database.BatchableContext BC, List<Opportunity> optyList){
        system.debug('Size of the list is ' + optyList.size());
        
        //List of Marketing Interaction Records to insert
        List<Marketing_Interaction__c> marketingInteractionList = new List<Marketing_Interaction__c>();
        if(!optyList.isEmpty()){
            for(Opportunity thisOpty : optyList) {
                Marketing_Interaction__c thisMI = new Marketing_Interaction__c();
                thisMI.Source_Record_Id__c = thisOpty.Id;  // Add Source Record Id as Opty Id
                thisMI.Account__c = thisOpty.AccountId; //Account Id
                thisMI.Communication_Type__c = 'Doc Collection Status'; 
                marketingInteractionList.add(thisMI); //add Record to List to Insert
                System.debug(thisMI);
            }  
            //Insert Marketing Interaction Records
            if(!marketingInteractionList.isEmpty()){
                try{
                    Database.insert(marketingInteractionList,false);  //Insert Marketing Interaction Records
                    System.debug(marketingInteractionList);
                }catch (DmlException de) {
                    Integer numErrors = de.getNumDml();
                    System.debug('getNumDml=' + numErrors);
                    for(Integer i=0;i<numErrors;i++) {
                        System.debug('getDmlFieldNames=' + de.getDmlFieldNames(i));
                        System.debug('getDmlMessage=' + de.getDmlMessage(i));
                    }
                }
            }   
        }
    }   
    
    //Finish Method
    global void finish(Database.BatchableContext BC) {
        
    }
}