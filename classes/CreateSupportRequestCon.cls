/* V1 */
public without sharing class CreateSupportRequestCon{
    @AuraEnabled
    public static boolean processFeedback(string subject, string requestType, string priority, string feedback, List<Id> files) {
       Case c=new Case();
       c.Subject=subject;
       c.Request_Type__c=requestType;
       c.Priority=priority;
       c.SR_Description__c=feedback;
       c.Status='SR1 Submitted';
       
       Id srRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Request').getRecordTypeId();
       System.Debug('*** srRecTypeId =' + srRecTypeId);
       c.RecordTypeID=srRecTypeId;
       c.Origin='CIG Console';
       
       insert c;
       
       CaseShare cs = new CaseShare();
       cs.UserOrGroupId=UserInfo.getUserId();
       cs.CaseAccessLevel='Edit';
       cs.CaseId=c.Id;
       cs.RowCause = 'Manual';
       
       //insert cs;
       
       if(files.size() >0){
           List<ContentDocumentLink> lstCDLIns= new List<ContentDocumentLink>();       
           for(ContentDocumentLink cdl : [Select id, ContentDocumentId, LinkedEntityId, ShareType, Visibility 
                           from ContentDocumentLink Where ContentDocumentId IN: files]){
               ContentDocumentLink cdlNew= cdl.Clone();
               cdlNew.LinkedEntityId=c.Id;
               lstCDLIns.Add(cdlNew);       
           }
           insert lstCDLIns;         
       }
       return true;    
    }

}