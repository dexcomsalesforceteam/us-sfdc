/********************************************************************************
Description    : Test class to test the scenarios involved in Address object
*********************************************************************************/
@isTest
private class ClsTestAddressObject {
    @isTest static void TestAccountAddress(){
        //Insert 1 account record
        List<Account> accts = ClsTestDataFactory.createAccountsWithBillingAddress(1, 'Consumers', 'US');
        Test.startTest();
		ClsAccountAddressTriggerStaticClass.addressChangeInvokedFromAddress = TRUE;
		ClsAccountAddressTriggerStaticClass.addressChangeInvokedFromAccount = FALSE;
		//Get the first account, which was inserted
		Account a = accts[0];
		String street = a.BillingStreet;
		
		//Insert new Bill To and Ship To address records
		List<Address__c> addrListToBeInserted = new List<Address__c>();
		Address__c newBillToAddr = new Address__c();
		newBillToAddr.Account__c = a.Id;
		newBillToAddr.Street_Address_1__c = '3 Main Street';
		newBillToAddr.City__c = 'San Diego';
		newBillToAddr.State__c = 'CA';		
		newBillToAddr.Zip_Postal_Code__c = '92121';
		newBillToAddr.Country__c = 'US';
		newBillToAddr.Address_Type__c = 'BILL_TO';
		newBillToAddr.Primary_Flag__c = TRUE;
		addrListToBeInserted.add(newBillToAddr);

        Address__c newShipToAddr = new Address__c();
		newShipToAddr.Account__c = a.Id;
		newShipToAddr.Street_Address_1__c = '4 Main Street';
		newShipToAddr.City__c = 'San Diego';
		newShipToAddr.State__c = 'CA';
		newShipToAddr.Zip_Postal_Code__c = '92123';
		newShipToAddr.Country__c = 'US';
		newShipToAddr.Address_Type__c = 'SHIP_TO';
		newShipToAddr.Primary_Flag__c = TRUE;
		addrListToBeInserted.add(newShipToAddr);
		insert addrListToBeInserted;
				
		//Process to make existing address as Primary
		List<Address__c> addrListToBeUpdated = new List<Address__c>();
		//Query for original billing primary address and make it as primary
		Address__c billToAddrToBeMadePrimary = [SELECT Id FROM Address__c WHERE Street_Address_1__c = :street AND Account__c = :a.Id AND Address_Type__c = 'BILL_TO' LIMIT 1];
		billToAddrToBeMadePrimary.Primary_Flag__c = TRUE;
		addrListToBeUpdated.add(billToAddrToBeMadePrimary);
		//Query for original shipping primary address and make it as primary
		Address__c shipToAddrToBeMadePrimary = [SELECT Id FROM Address__c WHERE Street_Address_1__c = :street AND Account__c = :a.Id AND Address_Type__c = 'SHIP_TO' LIMIT 1];
		shipToAddrToBeMadePrimary.Primary_Flag__c = TRUE;
		addrListToBeUpdated.add(shipToAddrToBeMadePrimary);
		update addrListToBeUpdated;
		addrListToBeUpdated.clear();
		
		//Alter existing primary address attributes
		//Query for original billing primary address and make it as primary
		Address__c billToPrimaryAddr = [SELECT Id FROM Address__c WHERE Account__c = :a.Id AND Address_Type__c = 'BILL_TO' AND Primary_Flag__c = TRUE LIMIT 1];
		billToPrimaryAddr.Street_Address_1__c = 'Street Address Changed';
		addrListToBeUpdated.add(billToPrimaryAddr);
		Address__c shipToPrimaryAddr = [SELECT Id FROM Address__c WHERE Account__c = :a.Id AND Address_Type__c = 'SHIP_TO' AND Primary_Flag__c = TRUE LIMIT 1];
		shipToPrimaryAddr.Street_Address_1__c = 'Street Address Changed';
		addrListToBeUpdated.add(shipToPrimaryAddr);
		Update addrListToBeUpdated;
		Test.stopTest();
	}
}