/**********************************************************
 **Description: Account trigger handler Test Class.
 **Author:      Louis Augusto Del Rosario, CLOUD SHERPAS
 **Date Created:    JULY.14.2015
**********************************************************
**@Author Katie Wilson
**@Date: 10/24/2018
**@Reason Add the SMS logic tests

*********************************************************/
@isTest
private class AccountTriggerHandlerTest {
    /**********************************************************
     **Description: Test Method if the person email gets copied
     **Parameters:  None
     **Returns:     None
     **Author:      CLOUD SHERPAS, Louis Augusto Del Rosario
     **Date Created:    JULY.14.2015
    **********************************************************/
    static testMethod void testCopyEmail(){
        Account testAccount = TestDataBuilder.testAccount();
        insert testAccount;

        List<Account> accountList = [SELECT Id, PersonEmail,PersonEmail__c FROM Account LIMIT 1];

        System.assertEquals(accountList[0].PersonEmail,accountList[0].PersonEmail__C);
    }
    /**********************************************************
     **Description: Test Method the copied person email is also updated
     **Parameters:  None
     **Returns:     None
     **Author:      CLOUD SHERPAS, Louis Augusto Del Rosario
     **Date Created:    JULY.14.2015
    **********************************************************/
    static testMethod void testCopyEmailUpdate(){
        Account testAccount = TestDataBuilder.testAccount();
        insert testAccount;
        testAccount.PersonEmail = 'TestUpdate@gmail.com';
        Update testAccount;

        List<Account> accountList = [SELECT Id, PersonEmail,PersonEmail__c FROM Account LIMIT 1];

        System.assertEquals(accountList[0].PersonEmail,accountList[0].PersonEmail__C);
    }
    
    static testMethod void testIntegrationLastModifiedUpdate(){
        Account testAccount = TestDataBuilder.testAccount();
        insert testAccount;
        testAccount.PersonEmail = 'TestUpdate@gmail.com';
        Update testAccount;

        List<Account> accountList = [SELECT Id, PersonEmail,PersonEmail__c, Integration_Last_Modified_Date__c FROM Account LIMIT 1];
        accountList[0].PersonEmail = 'TestUpdate@testEmail.com';
        update accountList;
        
      //  System.assertEquals(accountList[0].Integration_Last_Modified_Date__c, datetime.now());
    }
    
    static testMethod void testSMSJobInsert() {
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperMock());
        
        Account testAccount = new Account();
        testAccount.LastName='tester';        
        testAccount.PersonMobilePhone='1234567890';
        testAccount.SMS_Opt_In_List__c='True';
        testAccount.SMS_pending_opt_in__c=true;
        testAccount.Phone_Opt_In_List__c='False';
        
        Test.startTest();
        insert testAccount;
        Test.stopTest();
        
        Account resultAccount=[SELECT SMS_Opt_In_Sent__c FROM Account];
        System.assertNotEquals(null, resultAccount.SMS_Opt_In_Sent__c);        
    }
    
    static testMethod void testSMSJobSent2() {
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperMock());
        
        Account testAccount = new Account();
        testAccount.LastName='tester';  
        insert testAccount;
        testAccount.PersonMobilePhone='(923) 456-7894';
        testAccount.SMS_Opt_In_List__c='True';
        AccountTriggerHandler.RUN_TRIGGER_LOGIC = false;
        Test.startTest(); 
        update testAccount;
        
        AccountTriggerHandler.RUN_TRIGGER_LOGIC = true;
        testAccount.SMS_Send_Opt_In_2__c=true;
        
         
        update testAccount;
        Test.stopTest();  
        
        Account resultAccount=[SELECT SMS_Opt_In_2_Sent__c, SMS_Send_Opt_In_2__c FROM Account];
        System.assertNotEquals(null,resultAccount.SMS_Opt_In_2_Sent__c);
        System.assertEquals(false,resultAccount.SMS_Send_Opt_In_2__c);        
    }
    
    static testMethod void testSMSJob() { 
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperMock());
        Account testAccount = new Account();
        testAccount.LastName='tester';            
        insert testAccount;
        
        testAccount.PersonMobilePhone='1234567890';
        testAccount.SMS_Opt_In_List__c='True';
        
        Test.startTest();           
        update testAccount;
        Test.stopTest();
        
        Account resultAccount=[SELECT SMS_Opt_In_Sent__c FROM Account];
        System.assertNotEquals(null,resultAccount.SMS_Opt_In_Sent__c);
    } 

    static testMethod void testSMSJobSent2NotPending() {
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperMock());
        
        Account testAccount = new Account();
        testAccount.LastName='tester'; 
        insert testAccount;
        testAccount.PersonMobilePhone='(123) 456-7888';
        testAccount.SMS_Opt_In_List__c='True';
        AccountTriggerHandler.RUN_TRIGGER_LOGIC = false;
        Test.startTest(); 
        update testAccount;
        
        AccountTriggerHandler.RUN_TRIGGER_LOGIC = true;
        testAccount.SMS_Send_Opt_In_2__c=true;
        
         
        update testAccount;
        Test.stopTest();  
        
        Account resultAccount=[SELECT SMS_Opt_In_2_Sent__c, SMS_Send_Opt_In_2__c FROM Account];
        System.assertEquals(null,resultAccount.SMS_Opt_In_2_Sent__c);   
    }

    static testMethod void testDoubleOptInResetCanada() { 
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock());
        RecordType recordTypeAccount = [select Id 
            FROM RecordType 
            where DeveloperName = 'CA_Consumer' and SobjectType = 'Account' limit 1];

        Account testAccount = TestDataBuilder.testAccount();
        testAccount.Email_Opt_In_Language__c = 'English';
        testAccount.Email_Opt_In_Method__c = 'Via Email';
        testAccount.RecordTypeId = recordTypeAccount.Id;
        testAccount.Email_Double_Opt_In_Subscriber__c=Datetime.now();
        insert testAccount;
        
        //email address change
        testAccount.PersonEmail='Test2@gmail.com';
        
        Test.startTest();
        update testAccount;
        Account resultAccount=[SELECT Email_Pending_Opt_In__c, Email_Double_Opt_In_Subscriber__c, Email_Pending_Opt_In_Reset__c FROM Account];
        System.assertEquals(true,resultAccount.Email_Pending_Opt_In__c);
        System.assertEquals(null,resultAccount.Email_Double_Opt_In_Subscriber__c);
        System.assertEquals(true,resultAccount.Email_Pending_Opt_In_Reset__c);
        Test.stopTest();
        
        resultAccount=[SELECT Email_Pending_Opt_In__c, Email_Double_Opt_In_Subscriber__c, Email_Pending_Opt_In_Reset__c FROM Account];
        System.assertEquals(true,resultAccount.Email_Pending_Opt_In__c);
        System.assertNotEquals(null,resultAccount.Email_Double_Opt_In_Subscriber__c);
        System.assertEquals(false,resultAccount.Email_Pending_Opt_In_Reset__c);
    }
    
    static testMethod void testDoubleOptInResetGB() { 
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock());
        RecordType recordTypeAccount = [select Id 
            FROM RecordType 
            where DeveloperName = 'GB_Consumer' and SobjectType = 'Account' limit 1];

        Account testAccount = TestDataBuilder.testAccount();
        testAccount.Email_Opt_In_Language__c = 'English';
        testAccount.Email_Opt_In_Method__c = 'Via Email';
        testAccount.RecordTypeId = recordTypeAccount.Id;
        testAccount.Email_Double_Opt_In_Subscriber__c=Datetime.now();
        insert testAccount;
        
        //email address change
        testAccount.PersonEmail='Test2@gmail.com';
        
        Test.startTest();
        update testAccount;
        Account resultAccount=[SELECT Email_Pending_Opt_In__c, Email_Double_Opt_In_Subscriber__c, Email_Pending_Opt_In_Reset__c FROM Account];
        System.assertEquals(true,resultAccount.Email_Pending_Opt_In__c);
        System.assertEquals(null,resultAccount.Email_Double_Opt_In_Subscriber__c);
        System.assertEquals(true,resultAccount.Email_Pending_Opt_In_Reset__c);
        Test.stopTest();
        
        resultAccount=[SELECT Email_Pending_Opt_In__c, Email_Double_Opt_In_Subscriber__c, Email_Pending_Opt_In_Reset__c FROM Account];
        System.assertEquals(true,resultAccount.Email_Pending_Opt_In__c);
        System.assertNotEquals(null,resultAccount.Email_Double_Opt_In_Subscriber__c);
        System.assertEquals(false,resultAccount.Email_Pending_Opt_In_Reset__c);
    }
    
    static testMethod void testDoubleOptInResetIE() { 
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperEmailMock());
        RecordType recordTypeAccount = [select Id 
            FROM RecordType 
            where DeveloperName = 'IE_Consumer' and SobjectType = 'Account' limit 1];

        Account testAccount = TestDataBuilder.testAccount();
        testAccount.Email_Opt_In_Language__c = 'English';
        testAccount.Email_Opt_In_Method__c = 'Via Email';
        testAccount.RecordTypeId = recordTypeAccount.Id;
        testAccount.Email_Double_Opt_In_Subscriber__c=Datetime.now();
        insert testAccount;
        
        //email address change
        testAccount.PersonEmail='Test2@gmail.com';
        
        Test.startTest();
        update testAccount;
        Account resultAccount=[SELECT Email_Pending_Opt_In__c, Email_Double_Opt_In_Subscriber__c, Email_Pending_Opt_In_Reset__c FROM Account];
        System.assertEquals(true,resultAccount.Email_Pending_Opt_In__c);
        System.assertEquals(null,resultAccount.Email_Double_Opt_In_Subscriber__c);
        System.assertEquals(true,resultAccount.Email_Pending_Opt_In_Reset__c);
        Test.stopTest();
        
        resultAccount=[SELECT Email_Pending_Opt_In__c, Email_Double_Opt_In_Subscriber__c, Email_Pending_Opt_In_Reset__c FROM Account];
        System.assertEquals(true,resultAccount.Email_Pending_Opt_In__c);
        System.assertNotEquals(null,resultAccount.Email_Double_Opt_In_Subscriber__c);
        System.assertEquals(false,resultAccount.Email_Pending_Opt_In_Reset__c);
    }
    
    @istest
    public static void kCodetest(){
         
        
        //Insert test Account
        id recId = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Consumers'].Id;
        id payorRecId = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Payor'].Id;
         Id pb = Test.getStandardPricebookID();
		Account consumer = TestDataBuilder.getAccountListConsumer(1, recId)[0];
        consumer.Default_Price_Book__c = pb;
        consumer.BillingState = 'AZ';
        consumer.BillingCountry = 'US';
        consumer.ShippingCountry = 'US';
		test.startTest();
        Account acc = TestDataBuilder.getAccountList(1,payorRecId)[0];
        acc.Payor_Code_Commercial__c = 'K';
        acc.Shipment_Duration__c = 'Quarterly';
        acc.Payor_Code__c ='K';
        
        insert new list <Account>{consumer, acc};
        consumer.Payor__c = acc.id;
        consumer.Primary_Plan_Type__c = 'MED ADV';
        consumer.BillingState = 'CA';
		Update consumer;
		
        consumer.G4_Exception_Flag__c = true;
        consumer.BillingState = 'AL';
        update consumer;
		
        consumer.Customer_Type__c = 'Medicare FFS';
        update consumer;

        test.stopTest();
    }
}