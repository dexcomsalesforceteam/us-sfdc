/****************************************************************************************************************
@Author        : Jagan Periyakaruppan
@Date Created    : 4/15/2019
@Description    : Class used in as a helper class for ClsAccountAddressTriggerHandler
****************************************************************************************************************/
public class ClsAccountAddressTriggerHandlerHelper{
    
    public static void ProcessAddressRecords(String addressType, List<Account> accountsToProcess)
    {
        
        system.debug('----The address type to process is ' + addressType);
        Map<Id, List<Address__c>> accntIdToAddressMap = new Map<Id, List<Address__c>>();//Map holds the List of address tied to an account based on the addressType requested
        List<Address__c> addressToCreateList = new List<Address__c>();//List holds the address records to be created
        Map<Id, Address__c> addressIdToAddressMap = new Map<Id, Address__c>();//Map holds the Id to Address to its address object
		List<Account> accountsToBeUpdatedWithPrimaryShipToList = new List<Account>(); //List used to update the Accounts with Primary Ship To Id
		
        //Create a map between account id and list of addresses
        for(Address__c addr : [SELECT ID, Account__c, Street_Address_1__c, City__c, State__c, Zip_Postal_Code__c, Country__c, Primary_Flag__c, Address_Type__c FROM Address__c WHERE Address_Type__c = :addressType AND Account__c IN : accountsToProcess])
        {
            system.debug('----There are address tied the account');
            if(accntIdToAddressMap.containsKey(addr.Account__c)) 
            {
                List<Address__c> addrList = accntIdToAddressMap.get(addr.Account__c);
                addrList.add(addr);
                accntIdToAddressMap.put(addr.Account__c, addrList);
            }
            else
            {
                accntIdToAddressMap.put(addr.Account__c, new List<Address__c> {addr});
            }
            
        }
        for(Account accnt : accountsToProcess)
        {
            Boolean existingAddrFound = false;
            Address__c existingAddrToBeUpdated;
            List<Address__c> tempAddrList = accntIdToAddressMap.get(accnt.Id);//Store all Address for the Account for a given type
            //Process Address logic
            if(tempAddrList != null)
            {
                if(!tempAddrList.isEmpty())
                {
                    //Remove the primary flag for existing addresses
                    for(Address__c primaryAddr : tempAddrList)
                    {
                        
                        if(primaryAddr.Primary_Flag__c)
                        {
                            system.debug('----Primary Address exist and the Id is ' + primaryAddr.Id);
							system.debug('----Set Primary flag to false');
                            primaryAddr.Primary_Flag__c = false;
                            addressIdToAddressMap.put(primaryAddr.Id, primaryAddr);
                        }
                    }
                    
                    //Process the address list tied to the account to add or create address record
                    for(Address__c addrToUpdate : tempAddrList)
                    {
                        
                        //Check if the Address tied to the account already exist in the address object for this account
                        system.debug('----Street addr to check if it matches with the account address -- ' + addrToUpdate.Street_Address_1__c);
                        if(addressType == 'BILL_TO')
                        {   
                            if(accnt.BillingStreet == addrToUpdate.Street_Address_1__c &&
                               accnt.BillingCity == addrToUpdate.City__c &&
                               accnt.BillingState == addrToUpdate.State__c &&
                               String.isNotBlank(accnt.BillingPostalCode) && String.isNotBlank(addrToUpdate.Zip_Postal_Code__c) && //added fix for INC0256119
                               accnt.BillingPostalCode.substring(0,4) == addrToUpdate.Zip_Postal_Code__c.substring(0,4)){
                                system.debug('----Billing address matched');
                                existingAddrFound = true;
                                existingAddrToBeUpdated = addrToUpdate;
                            }
                        }
                        else if(addressType == 'SHIP_TO')
                        {
                            
                            if(accnt.ShippingStreet == addrToUpdate.Street_Address_1__c &&
                               accnt.ShippingCity == addrToUpdate.City__c &&
                               accnt.ShippingState == addrToUpdate.State__c &&
                               String.isNotBlank(accnt.ShippingPostalCode) && String.isNotBlank(addrToUpdate.Zip_Postal_Code__c) && //added fix for INC0256119
                               accnt.ShippingPostalCode.substring(0,4) == addrToUpdate.Zip_Postal_Code__c.substring(0,4))
                            {
                                system.debug('----Shipping address matched');
                                existingAddrFound = true;
                                existingAddrToBeUpdated = addrToUpdate;
                            }
                        }   
                        
                    }
                    //If existing address is found then update the primary flag
                    if(existingAddrFound)
                    {
                        system.debug('----Update the primary for the matched address');
						if(addressIdToAddressMap.containsKey(existingAddrToBeUpdated.Id))
                        {
                            system.debug('-----Entering the process where the address exist in the map');
                            Address__c addr = addressIdToAddressMap.get(existingAddrToBeUpdated.Id);
                            addr.Primary_Flag__c = true;
							addr.Inactive__c = false;
                            addressIdToAddressMap.put(addr.Id, addr);
                        }
                        else
                        {
                            system.debug('-----Entering the process where the address does not exist in the map');
                            existingAddrToBeUpdated.Primary_Flag__c = true;
							existingAddrToBeUpdated.Inactive__c = false;
                            addressIdToAddressMap.put(existingAddrToBeUpdated.Id, existingAddrToBeUpdated);
                        }
                        
                    }
                    //If no address is found create a new address record
                    if(!existingAddrFound)
                    {
                        //Create a new BILL_TO Address record
						system.debug('----Create a new primary address');
                        Address__c addrToCreate;
                        if(addressType == 'BILL_TO')
                            addrToCreate = CreateAddress(accnt.Id, accnt.BillingStreet, accnt.BillingCity, accnt.BillingState, accnt.BillingPostalCode, accnt.BillingCountry, addressType);
                        if(addressType == 'SHIP_TO')
                            addrToCreate = CreateAddress(accnt.Id, accnt.ShippingStreet, accnt.ShippingCity, accnt.ShippingState, accnt.ShippingPostalCode, accnt.ShippingCountry, addressType);
                        addressToCreateList.add(addrToCreate);
                    }   
                    
                }
            }
			//If there isn't any address records in the system already for the account then create new entries
			else
			{
				//Create a new BILL_TO and SHIP TO Address record
				system.debug('----Create a new primary address');
				Address__c billtoAddrToCreate;
				Address__c shiptoAddrToCreate;
				if(addressType == 'BILL_TO' && accnt.BillingStreet != null){
				billtoAddrToCreate = CreateAddress(accnt.Id, accnt.BillingStreet, accnt.BillingCity, accnt.BillingState, accnt.BillingPostalCode, accnt.BillingCountry, addressType);
				system.debug('billtoAddrToCreate is ' + billtoAddrToCreate);
				addressToCreateList.add(billtoAddrToCreate);
				}
				if(addressType == 'SHIP_TO' && accnt.ShippingStreet != null){
					shiptoAddrToCreate = CreateAddress(accnt.Id, accnt.ShippingStreet, accnt.ShippingCity, accnt.ShippingState, accnt.ShippingPostalCode, accnt.ShippingCountry, addressType);
					system.debug('shiptoAddrToCreate is ' + shiptoAddrToCreate);
					addressToCreateList.add(shiptoAddrToCreate);
				}
			} 
        }
        //Update Address Records
        if(!addressIdToAddressMap.values().isEmpty())
        {
            system.debug('-----Update list entered');
            try{
				update addressIdToAddressMap.values();
				//Get all the Ids that got updated
                for(Address__c addr : addressIdToAddressMap.values())
				{	
					Account accntToBeUpdatedWithPrimaryShipTo = new Account(Id = addr.Account__c);
					if(addr.Address_Type__c == 'SHIP_TO' && addr.Primary_Flag__c == true)
					{
						accntToBeUpdatedWithPrimaryShipTo.Primary_Ship_To_Address__c = addr.Id;
						accountsToBeUpdatedWithPrimaryShipToList.add(accntToBeUpdatedWithPrimaryShipTo);
					}
				}
			}
            catch (DmlException de) {
                Integer numErrors = de.getNumDml();
                System.debug('getNumDml=' + numErrors);
                for(Integer i=0;i<numErrors;i++) {
                    System.debug('getDmlFieldNames=' + de.getDmlFieldNames(i));
                    System.debug('getDmlMessage=' + de.getDmlMessage(i));
                }
            }
        }          
        //Insert Address Records
        if(!addressToCreateList.isEmpty())
        {
            system.debug('-----Insert list entered');
            List<Id> addressIdsToProcessForCountyNames = new List<Id>();//List of Address Ids used to retrieve the county names
            try{
                insert addressToCreateList;
				//Get all the Ids that got inserted
                for(Address__c addr : addressToCreateList)
				{	
                    addressIdsToProcessForCountyNames.add(addr.Id);
					Account accntToBeUpdatedWithPrimaryShipTo = new Account(Id = addr.Account__c);
					if(addr.Address_Type__c == 'SHIP_TO')
					{
						accntToBeUpdatedWithPrimaryShipTo.Primary_Ship_To_Address__c = addr.Id;
						accountsToBeUpdatedWithPrimaryShipToList.add(accntToBeUpdatedWithPrimaryShipTo);
					}
				}
                //Invoke external service to get the county name
                ClsAddressVerificationFromExternalSource.processAddressToGetCountyName(addressIdsToProcessForCountyNames);
            }
            catch (DmlException de) {
                Integer numErrors = de.getNumDml();
                System.debug('getNumDml=' + numErrors);
                for(Integer i=0;i<numErrors;i++) {
                    System.debug('getDmlFieldNames=' + de.getDmlFieldNames(i));
                    System.debug('getDmlMessage=' + de.getDmlMessage(i));
                }
            }
		
        }
		//Update the Primary Ship To on the Account
		if(!accountsToBeUpdatedWithPrimaryShipToList.isEmpty())
			ClsAccountAddressTriggerHandlerHelper.UpdateAccountWithPrimaryShipToDetails(accountsToBeUpdatedWithPrimaryShipToList);
    }
    //Method creates a new address for a given type
    public static Address__c CreateAddress (Id accountId, String street, String city, String state, String zip, String country, String type)
    {
        //Get Address record type for the Country__c
		Address__c addressToCreate = new Address__c ();
        addressToCreate.Account__c = accountId;
        addressToCreate.Street_Address_1__c = street;
        addressToCreate.City__c = city;
        addressToCreate.State__c = state;
        addressToCreate.Zip_Postal_Code__c = zip;
        addressToCreate.Country__c = country;
        addressToCreate.Address_Type__c = type;
        addressToCreate.Primary_Flag__c = true;
		addressToCreate.Skip_Apex__c = true;
        return addressToCreate;
    }
	//Process will be invoked to send the address to Oracle
	public static void SendAddressToOracle(List<Id> accountsToProcess)
    {
        Map<Id, List<Address__c>> accntIdToAddressMap = new Map<Id, List<Address__c>>();//Map holds the List of address tied to an account 
        List<Address__c> addressToUpdateList = new List<Address__c>();//List holds the address records to be updated
		List<Id> addressIdsToProcessForCountyNames = new List<Id>();//List of Address Ids used to retrieve the county names
        //Create a map between account id and list of addresses
        for(Address__c addr : [SELECT ID, Send_To_Oracle__c, Account__c FROM Address__c WHERE Account__c IN : accountsToProcess AND Oracle_Address_ID__c = null])
			addressIdsToProcessForCountyNames.add(addr.Id);
			//Invoke external service to get the county name
        ClsAddressVerificationFromExternalSource.processAddressToGetCountyName(addressIdsToProcessForCountyNames);
	}
	
	//Update account records with the Primary Ship To Information
	public static void UpdateAccountWithPrimaryShipToDetails(List<Account> updateAccountList)
	{
		try{
			ClsAccountAddressTriggerStaticClass.addressChangeInvokedFromAddress = false; //Set the static variable, so the code in Address object will not fire.
			update updateAccountList;
		}
		catch (DmlException de) {
			Integer numErrors = de.getNumDml();
			System.debug('getNumDml=' + numErrors);
			for(Integer i=0;i<numErrors;i++) {
				System.debug('getDmlFieldNames=' + de.getDmlFieldNames(i));
				System.debug('getDmlMessage=' + de.getDmlMessage(i));
			}
		}
	}
}