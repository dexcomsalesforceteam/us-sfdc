/***
*@Author        : Priyanka Kajawe
*@Date Created  : 04-05-2019
*@Description   : Schedule class to push Orders for SSIP
***/
public class BclsSSIPOrderCreationBatchSched implements Schedulable {
    public void execute(SchedulableContext sc) {
        BclsSSIPOrderCreationBatch schBatch = new BclsSSIPOrderCreationBatch('BclsSSIPOrderCreationBatchSchedule');   
       
        Id batchInstanceId = Database.executeBatch(schBatch, 5);
    }
}