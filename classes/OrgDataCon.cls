/* V1 */
public class OrgDataCon{

    @AuraEnabled
    public static List<Organization_Data__c> getClinicalData(string orgId){
        return [Select Id, Note_Title__c, Organization__r.Name, Product__r.Name, Trial_Start_Date__c, Trial_End_Date__c, Trial_URL__c, Num_of_Participants__c
        , Results_posted__c, CreatedDate, LastModifiedDate
         FROM Organization_Data__c Where  Organization__c =:orgId AND  RecordType.DeveloperName='Clinical' Order By LastModifiedDate Desc LIMIT 200];
    }

    @AuraEnabled
    public static boolean updateClinicalData(List<Organization_Data__c> lstOrgData){
        try{
            update lstOrgData;
            return true;
        } catch(Exception e){
            return false;
        }
    }
    
    @AuraEnabled
    public static Organization__c getOrg(Id orgId) {
        return [SELECT Id, Name, Type__c, Organization_Type__c FROM Organization__c Where Id=:orgId];
    }    

    
    @AuraEnabled
    public static Organization_Data__c addODRecord(Organization_Data__c data, List<Id> files, string usersToFollow) {       
       //Id regulatoryRecTypeId = Schema.SObjectType.Organization_Data__c.getRecordTypeInfosByDeveloperName().get('Regulatory_Info').getRecordTypeId();
       set<Id> setFollowersId=new set<Id>();
       
       if(usersToFollow != null){
           System.Debug('**** usersToFollow =' + usersToFollow);
           List<String> lstUsers=usersToFollow.split(';');
           for(string uid : lstUsers){               
               setFollowersId.Add(uid);            
           }
           data.Followers__c='';
           for(User u : [Select Id, Name from User where IsActive=true AND ID IN :setFollowersId]){
               data.Followers__c += u.Name + ' \r\n';
           }
           
           System.Debug('**** data.Followers__c=' + data.Followers__c);
       }
       
       if(data != null){           
           insert data;
       }
       
       System.Debug('**** data='+data);
       
        if(files.size() >0){
           List<ContentDocumentLink> lstCDLIns= new List<ContentDocumentLink>();       
           for(ContentDocumentLink cdl : [Select id, ContentDocumentId, LinkedEntityId, ShareType, Visibility 
                           from ContentDocumentLink Where ContentDocumentId IN: files]){
               ContentDocumentLink cdlNew= cdl.Clone();
               cdlNew.LinkedEntityId=data.Id;
               lstCDLIns.Add(cdlNew);       
           }
           insert lstCDLIns;
       }
       
       if(usersToFollow != null && setFollowersId.size()>0){
           System.Debug('**** usersToFollow =' + usersToFollow);
           List<String> lstUsers=usersToFollow.split(';');
           System.Debug('**** lstUsers=' + lstUsers);
           
           List<EntitySubscription> lstES= new List<EntitySubscription>();
           for(Id uid : setFollowersId){
               lstES.Add(new EntitySubscription(SubscriberId = uid, ParentId = data.Id));             
           }
           System.Debug('**** lstES=' + lstES);
           insert lstES;
       }
       
       return data;      
    }
    
    @AuraEnabled
    public static List<Organization_Data__c> getRegulatoryData(string orgId){
        System.Debug('**** Org Id=' + orgId);
        return [Select Id, Note_Title__c, Organization__r.Name, Product__r.Name, Regulatory_Agency__c, Filling_Country__c, Filing_Status__c, Num_Files_Attached__c
        , Followers__c,Filed_Date__c 
         FROM Organization_Data__c Where Organization__c =:orgId AND RecordType.DeveloperName='Regulatory_Info' Order By LastModifiedDate Desc LIMIT 200];
    }
    
    @AuraEnabled
    public static List<Organization_Data__c> getMarketingData(string orgId){
        System.Debug('**** Org Id=' + orgId);
        return [Select Id, Note_Title__c, Organization__r.Name, Product__r.Name, Product_Claim__c, Collateral_Source__c, Collateral_Target__c, Collateral_URL__c,Date_first_observed__c,
        Num_Files_Attached__c, Followers__c,CreatedDate, LastModifiedDate
         FROM Organization_Data__c Where Organization__c =:orgId AND RecordType.DeveloperName='Marketing' Order By LastModifiedDate Desc LIMIT 200];
    }
    
     @AuraEnabled
    public static List<Organization_Data__c> getFinancialData(string orgId){
        System.Debug('**** Org Id=' + orgId);
        return [Select Id, Note_Title__c, Organization__r.Name, Form_Number__c, Analyst__c, Num_Files_Attached__c
        , Followers__c,CreatedDate, Filed_Date__c 
         FROM Organization_Data__c Where Organization__c =:orgId AND RecordType.DeveloperName='Financials' Order By LastModifiedDate Desc LIMIT 200];
    }
    
    @AuraEnabled
    public static boolean updateRegulatoryData(List<Organization_Data__c> lstOrgData){
        try{
            update lstOrgData;
            return true;
        } catch(Exception e){
            return false;
        }
    }
    
    @AuraEnabled
    public static List<Organization_Data__c> getOrgProductNotes(string orgId){
        return [Select Id, Name, Product__r.Name, Product_Note_Abbr__c, CreatedBy.Name, CreatedDate
         FROM Organization_Data__c Where  Organization__c =:orgId AND  RecordType.DeveloperName='Product_Note' Order By LastModifiedDate Desc LIMIT 200];
    }
    
    @AuraEnabled
    public static List<Organization_Data__c> getOrgWikiHistData(string orgId){
        System.Debug('**** Org Id=' + orgId);
        return [Select Id, Name, Field_Changed__c, LastModifiedBy.Name, LastModifiedDate
         FROM Organization_Data__c Where Organization__c =:orgId AND RecordType.DeveloperName='History' Order By LastModifiedDate Desc LIMIT 200];
    }
}