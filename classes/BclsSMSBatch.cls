/**
 * SMSBatch
 * Batch to resend any missed SMS messages
 * @Author: Katie Wilson(Sundog)
 * @Date Modified: 09/28/18
 * @Author: Craig Johnson
 * @Date Modified: 2/6/2019
 * @Description: Restrict 2nd opt-in SMS query to only results returned from MarketingCloud
 */
global class BclsSMSBatch implements database.Batchable<SObject>, Database.Stateful, Database.AllowsCallouts  {
    //We assume in this code that the batch size will be 1.  If that changes then the code will need to be refactored
    Set<String> pendingNumbers= new Set<String>();
    global Database.QueryLocator start(Database.BatchableContext bc){
		//This gets the numbers that are still pending so we can compare to make sure we aren't sending people messages that are no longer pending
        pendingNumbers=ClsMarketingCloudHelper.updateOptInValues(true);
        //Get the numbers that are good to receive SMS
        return Database.getQueryLocator([SELECT Id, PersonMobilePhone, SMS_Send_Opt_In_2__c, PersonContactId 
            FROM Account 
            WHERE (PersonMobilePhone != null AND SMS_pending_opt_in__c = true AND Send_SMS_OK__c = true AND SMS_Opt_In_List__c = 'True' AND SMS_Opt_out_List__c != 'True')
            AND (SMS_Opt_In_Sent__c = null OR (PersonMobilePhone = :pendingNumbers AND SMS_Send_Opt_In_2__c = true AND SMS_Opt_In_2_Sent__c = null))]);
    }
    
    global void execute(Database.BatchableContext bc, List<Account> scope){
        
        for(Account accToSendSMS : scope){
            //Call num denotes if it is the first sms(1) or the second sms we are sending(2)
            Integer callNumber=1;
            if(accToSendSMS.SMS_Send_Opt_In_2__c==true){
                callNumber=2;
                //If the number is no longer pending, we don't want to send them an SMS so we break out
                if(!pendingNumbers.contains(accToSendSMS.PersonMobilePhone)){
                    callNumber=0;
                    break;
                }
            }
            
            //Format the phone number that the SMS is being sent to
            String phoneNumber = accToSendSMS.PersonMobilePhone.replaceAll(' ','');
            phoneNumber = phoneNumber.replace('(','');
            phoneNumber = phoneNumber.replace(')','');
            phoneNumber = phoneNumber.replaceAll('-','');
            if(phoneNumber.length()==10&& callNumber>0){
                ClsMarketingCloudHelper.sendSMSMessage('1'+phoneNumber, accToSendSMS.Id, callNumber, accToSendSMS.PersonContactId );                    
            }
        }
    }
    
    global void finish(Database.BatchableContext bc){
    }    
}