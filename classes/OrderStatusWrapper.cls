/****************************************************************************
* Author       : Sudheer Vemula
* Date         : March 12 2019
* Description  : For Store the Order status Data 
/****************************************************************************/
public class OrderStatusWrapper{
	public class OrderStatus {
		@AuraEnabled public String status;
		@AuraEnabled public String Hold_name;
		@AuraEnabled public String Hold_Release_Flag;
		@AuraEnabled public String Hold_Applied_date;
		@AuraEnabled public String Hold_Applied_by;
        @AuraEnabled public String Hold_Release_Comment;
		@AuraEnabled public Integer Invoice_Number;
		@AuraEnabled public String Invoice_date;
		@AuraEnabled public Integer Amount;
		@AuraEnabled public String Balance;
		@AuraEnabled public String Payment_required;
        @AuraEnabled public String Hold_Released_by;
        @AuraEnabled public String Hold_Release_date;
        @AuraEnabled public String CC_Approval_Code;
        
	}

	@AuraEnabled public Integer Order_Number;
	@AuraEnabled public List<OrderlineStatus> OrderlineStatus;
	@AuraEnabled public List<OrderStatus> OrderStatus;

	public class OrderlineStatus {
		@AuraEnabled public String Line_Number;
		@AuraEnabled public String Ordered_item;
		@AuraEnabled public String Ordered_Qty;
		@AuraEnabled public String Source_Line_Number;
		@AuraEnabled public String Delivery;
		@AuraEnabled public String Delivery_Status;
		@AuraEnabled public String Pick_Status;
		@AuraEnabled public String Lot_Number;
		@AuraEnabled public String Serial_From;
		@AuraEnabled public String Serial_To;
		@AuraEnabled public String Revision;
		@AuraEnabled public String Tracing_Number;
	}	
}