/********************************************************************************
@author Anuj Patel
@date 02/13/2018
@description: To cover the trigger ContactMerge_Trigger written on Contact_Merge__c  
Removing an older test class during this update. #AJP
********************************************************************************/

@isTest
public class ContactMerge_TriggerTest {
    static testMethod void testContactMerge(){
        List<Account> ls = new List<Account>{new Account(Firstname='Test1', Lastname = 'Dex', Party_ID__c = '12125', Dexcom_Rating__pc = '3-Potential', Call_Goal__pc = decimal.valueof('2')),
                                                         new Account(Firstname='Test2', Lastname = 'Dex', Party_ID__c = '12126', Dexcom_Rating__pc = '2-Lion', Call_Goal__pc =decimal.valueof('3')),
            											 new Account(Firstname='Test2', Lastname = 'Dex', Party_ID__c = '12131', Dexcom_Rating__pc = ' ', Call_Goal__pc =decimal.valueof('1')),
           												 new Account(Firstname='Test2', Lastname = 'Dex', Party_ID__c = '12132', Dexcom_Rating__pc = '2-Lion', Call_Goal__pc =decimal.valueof('2')),
            											 new Account(Lastname = 'Dex', Party_ID__c = '12137', Dexcom_Rating__pc = '2-Lion', Call_Goal__pc =decimal.valueof('2'))};                                                     
        insert ls;
      
        Account mastercnt = [SELECT Id, Name, Dexcom_Rating__pc, Call_Goal__pc FROM Account WHERE Party_ID__c  ='12125' LIMIT 1];
        Account Losecnt = [SELECT Id, Name, Dexcom_Rating__pc, Call_Goal__pc FROM Account WHERE Party_ID__c    = '12126' LIMIT 1];
        List<Contact_Merge__c> Cntmerge = new List<Contact_Merge__c>{new Contact_Merge__c(Contact_Merge__c = Losecnt.Id, name = '12125')};
        Account mastercnt2 = [SELECT Id, Name, Dexcom_Rating__pc, Call_Goal__pc FROM Account WHERE  Party_ID__c='12131' LIMIT 1];  ///HCP_Party_ID__c
        Account Losecnt2 = [SELECT Id, Name, Dexcom_Rating__pc, Call_Goal__pc FROM Account WHERE Party_ID__c    = '12132' LIMIT 1];
        List<Contact_Merge__c> Cntmerge2 = new List<Contact_Merge__c>{new Contact_Merge__c(Contact_Merge__c = Losecnt2.Id, name = '12131')};    
        List<Contact_Merge__c> Cntmerge3 = new List<Contact_Merge__c>{new Contact_Merge__c(Contact_Merge__c = Losecnt2.Id, name = '12132')};    
    	 System.debug('Name' + Cntmerge2[0].name);
         System.debug('Cont Merge ID ' + Cntmerge2[0].Contact_Merge__C);
        try{
           Test.startTest();
           insert Cntmerge;   
           insert Cntmerge2; 
           insert Cntmerge3;    
			}catch (Exception e) {
          System.debug('Contact merge failed: ' + e.getMessage());
          }
          Test.stopTest();
    }
}