//Batch Class Creates Medicare Follow Up Records based
//03/12/2018 - Amol - Changed the healthcare logic to look at field Is_BI_Active__c rather than Is_Last_Benefit_Check_Valid__c
//05/07/2018 - Jagan - Added logic to prevent Subscription sku to be created within 30 days mark
//07/31/2018 - Added logic for Medicare
global class BclsCreateMedicareFollowUpRecords implements Database.Batchable<sObject>
{
    
    //List of Medicare Follow Up Records, which are to be added
    List<MDCR_Followup__c> mdcrFollowUpRecordsToAdd = new List<MDCR_Followup__c>();
    //List of Medicare Follow Up Records, which are to be added
    List<MDCR_Followup__c> mdcrSubscriptionOnlyFollowUpRecordsToAdd = new List<MDCR_Followup__c>();
    //List of Medicare Follow Up Product Records, which are to be added
    List<MDCR_Followup_Products__c> mdcrFollowUpProductRecordsToAdd = new List<MDCR_Followup_Products__c>();
    //List of primary Benefit records to be updated
    List<Benefits__c> primaryBenefitsToBeSentToChangeHealthCare = new List<Benefits__c>();
    //List of Opportunities to be added
    List<Opportunity> opptyRecordsToBeAdded = new List<Opportunity>();
    //List of Opportunities to be added
    List<Opportunity> opptyRecordsToBeAddedForInactiveChartNotesList = new List<Opportunity>();
    //List of Opportunities to be added
    List<Account> accountIdWithInactiveChartNotesList = new List<Account>();
    //Map holds the map between the Product name and the product record
    Map<String, Product2> activeProductMap = new Map<String, Product2>();
    //Map holds the map between the Account Id and Primary Ship To Id
    Map<Id, Id> accountToPrimaryShipToId = new Map<Id, Id>();
    //Set holds all external Ids of MDCR Followup Product records
    Set<String> mdcrFollowupExternalIdsSet = new Set<String>();
    //List of Order Headers to be added
    List<Order_Header__c> orderHeadersToBeAdded = new List<Order_Header__c>();
    //List of Order Lines to be added
    List<Order_Item_Detail__c> orderItemsToBeAdded = new List<Order_Item_Detail__c>();
    
    //Set holds all the account Id to process
    Set<Id> accountIdSet = new Set<Id>();
    //Set holds all possible Pricelist names we will be using for Reorder
    Set<String> mdcrReorderPricelistsSet = new Set<String>();
    //Set of Ids for which Oppty record needs to be created
    Set<Id> accountIdsForOpptyCreationSet = new Set<Id>();
    
    //Map holds all external Ids of MDCR Followup Product recordsMax_Open_MFR_Followup_Date__c
    Map<Id, MDCR_Followup__c> accountIdToOpenMCRFollowup = new Map<Id, MDCR_Followup__c>();
    //Map holds the map between the Pricebook name to its Id
    Map<String, Id> priceBookNameToIdMap = new Map<String, Id>();
    //Map holds the map between the Pricebook Id to OracleId
    Map<Id, String> priceBookIdToOracleIdMap = new Map<Id, String>();
    //Map Account with inactive ChartNotes to Open Opportunity records
    Map<Id, Opportunity> openOpptyTiedToAccountsWithInactiveChartNotesMap = new Map<Id, Opportunity>();
    //Map between the account Id and Territory Id lookup
    Map<Id, Id> accountIdToTerrAlignIdMap = new Map<Id, Id>();  
    //Map between the MFR External Id and SUB SKU (G5/G6)
    Map<String, String> mfrExternalIdToSubSKUMap = new Map<String, String>();  
    
    
    private String query;
    //Constructors
    global BclsCreateMedicareFollowUpRecords (String soql){
        query = soql;
    }
    global BclsCreateMedicareFollowUpRecords (){
        query = 'SELECT Id, Name, MDCR_Communication_Preference__c, Next_MDCR_Reorder_Follow_up_Date__c, MDCR_Sensors_Count__c, MDCR_Test_Strips_Count__c, MDCR_Lancets_Count__c,Is_BI_Active__c, MDCR_Reorder_Pricebook__c, Is_Chart_Notes_Actve__c, Is_CMN_Active__c, Default_Price_Book__c, Next_Possible_Date_For_Glucometer_Order__c, Next_Possible_Date_For_Receiver_Order__c, Next_Possible_Date_For_Sensors_Order__c, Next_Possible_Date_For_Solution_Order__c, Next_Possible_Date_For_Transmitter_Order__c, Num_Of_Days_Left_For_Glucometer_Order__c, Num_Of_Days_Left_For_Receiver_Order__c, Num_Of_Days_Left_For_Sensors_Order__c, Num_Of_Days_Left_For_Solution_Order__c, Num_Of_Days_Left_For_Transmitter_Order__c, Num_Of_Open_MDCR_Followup_Records__c, Max_MFR_Followup_Date__c, Max_Non_Sub_MFR_Followup_Date__c, Latest_Receiver_Generation_Shipped__c, G6_Program__c, Latest_Transmitter_Generation_Shipped__c, Territory_ID_Lookup__c FROM ACCOUNT WHERE MDCR_Subscription_Exists_For_Curr_Month__c = FALSE AND Next_MDCR_Reorder_Follow_up_Date__c =: today AND Is_MDCR_Patient__c = TRUE AND MDCR_FFS_Subscription__c = \'Active\'';
        //query = 'SELECT Id, Name, MDCR_Communication_Preference__c, Next_MDCR_Reorder_Follow_up_Date__c, MDCR_Sensors_Count__c, MDCR_Test_Strips_Count__c, MDCR_Lancets_Count__c,Is_BI_Active__c, MDCR_Reorder_Pricebook__c, Is_Chart_Notes_Actve__c, Is_CMN_Active__c, Default_Price_Book__c, Next_Possible_Date_For_Glucometer_Order__c, Next_Possible_Date_For_Receiver_Order__c, Next_Possible_Date_For_Sensors_Order__c, Next_Possible_Date_For_Solution_Order__c, Next_Possible_Date_For_Transmitter_Order__c, Num_Of_Days_Left_For_Glucometer_Order__c, Num_Of_Days_Left_For_Receiver_Order__c, Num_Of_Days_Left_For_Sensors_Order__c, Num_Of_Days_Left_For_Solution_Order__c, Num_Of_Days_Left_For_Transmitter_Order__c, Num_Of_Open_MDCR_Followup_Records__c, Max_MFR_Followup_Date__c, Max_Non_Sub_MFR_Followup_Date__c, Latest_Receiver_Generation_Shipped__c, G6_Program__c, Latest_Transmitter_Generation_Shipped__c, Territory_ID_Lookup__c FROM ACCOUNT WHERE Id = \'0014B00000eYd3L\'';
    }
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        Date today = Date.today();
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Account> scope)
    {
        system.debug('Entering the Execute Method');
        //Get RecordtypeId for US Reorder - G5 and US Reorder - G6
        Id G5mdcrFollowupRecordTypeId = Schema.SObjectType.MDCR_Followup__c.getRecordTypeInfosByName().get('US Reorder - G5').getRecordTypeId();
        Id G6mdcrFollowupRecordTypeId = Schema.SObjectType.MDCR_Followup__c.getRecordTypeInfosByName().get('US Reorder - G6').getRecordTypeId();
        
        //Get RecordtypeId for US Reorder
        Id opptyRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('US Opportunity').getRecordTypeId();
        
        //Prepare active product map
        for(Product2 prod : [SELECT Id, Name, Description FROM PRODUCT2 WHERE IsActive = TRUE])
        {
            activeProductMap.put(prod.Name, prod);
        }
        
        //Get all the account Ids to be processed
        for(Account accnt : scope)
        {
            accountIdSet.add(accnt.Id);
            mdcrReorderPricelistsSet.add(accnt.MDCR_Reorder_Pricebook__c);          
        }
        
        //Get the map of Account Id to its Open MFRs 
        accountIdToOpenMCRFollowup = ClsApexUtility.getOpenMFRsForAccount(accountIdSet);
        
        //Get Pricebook map 
        for(Pricebook2 pb : [SELECT Name, Id, Oracle_Id__c FROM Pricebook2 WHERE Name IN : mdcrReorderPricelistsSet])
        {
            priceBookNameToIdMap.put(pb.Name, pb.Id);
            priceBookIdToOracleIdMap.put(pb.Id, pb.Oracle_Id__c);
        }
        
        //Get all the primary Benefit records tied to the accounts to be processed and update the BI Request Date
        for(Benefits__c benefit : [Select Id, BI_Request_Date__c FROM Benefits__c WHERE Benefit_Hierarchy__c = 'Primary' AND account__r.Is_BI_Active__c = FALSE AND Account__c IN: accountIdSet])
        {
            benefit.BI_Request_Date__c = datetime.now();
            primaryBenefitsToBeSentToChangeHealthCare.add(benefit);
        }
        //Get Primary Ship to address of Account
        for(Address__c addrs : [SELECT Id, Account__c FROM Address__c WHERE Address_Type__c =: 'SHIP_TO'  AND Primary_Flag__c = TRUE AND                       Account__c IN : accountIdSet]){
            accountToPrimaryShipToId.put(addrs.Account__c, addrs.Id);
        }  
        
        //For each account, create a Medicare Followup records
        for(Account accnt : scope)
        {
            system.debug('Account currently being processed ' + accnt.Id);
            Boolean canCreateMFR = false;
            Boolean addOnlySubSKU = false;
            Boolean skipProcessing = false;
            
            String mdcrRecordTypeId = null;
            String receiverGeneration = accnt.Latest_Receiver_Generation_Shipped__c;
            String transmitterGeneration = accnt.Latest_Transmitter_Generation_Shipped__c;
            String productGeneration;
            //CRMSF-4112 : The default SKU to be added on MFR for G6 or G5 MFR
            String subSKUProduct = 'MT-MC-SUB';          
            
            //Confirm if the Product Generation to be shipped and record type based on the 
            //Receiver Generation and the Number of days of Transmitter left to be shipped
            if(transmitterGeneration == 'G6')
            {
                productGeneration = 'G6';
                mdcrRecordTypeId = G6mdcrFollowupRecordTypeId;
                subSKUProduct = 'MT-OM-SUB';
            }
            else if((receiverGeneration == 'G5' || receiverGeneration == 'G5 Touch Screen') && accnt.Num_Of_Days_Left_For_Transmitter_Order__c > 15)
            {
                productGeneration = 'G5';
                mdcrRecordTypeId = G5mdcrFollowupRecordTypeId;
            }       
            else if(receiverGeneration == 'G5' && accnt.Num_Of_Days_Left_For_Transmitter_Order__c <= 15)
                
            {   
                productGeneration = 'G6 Transition from Brick';
                mdcrRecordTypeId = G6mdcrFollowupRecordTypeId;
                subSKUProduct = 'MT-OM-SUB';
            }
            else if(receiverGeneration == 'G5 Touch Screen' && accnt.Num_Of_Days_Left_For_Transmitter_Order__c <= 15)
            {   
                productGeneration = 'G6 Transition from Touchscreen';
                mdcrRecordTypeId = G6mdcrFollowupRecordTypeId;
                subSKUProduct = 'MT-OM-SUB';
            }
            else if(receiverGeneration == 'G6 Touch Screen')
            {   
                productGeneration = 'G6';
                mdcrRecordTypeId = G6mdcrFollowupRecordTypeId;
                subSKUProduct = 'MT-OM-SUB';
            }
            else
            {
                productGeneration = 'G5'; 
                mdcrRecordTypeId = G5mdcrFollowupRecordTypeId;
            }
            
            
            
            system.debug('Recordtype evaluation is complete and the value is ' + mdcrRecordTypeId);
            if(accnt.Max_Non_Sub_MFR_Followup_Date__c != null)
            {
                system.debug('********Entering the evalutation for Sub only SKU');
                Date lastMFRDate = accnt.Max_Non_Sub_MFR_Followup_Date__c;
                Date currentDate = Date.Today();
                if(lastMFRDate.Year() == currentDate.Year() && lastMFRDate.Month() == currentDate.Month())
                {
                    if(accnt.Is_Chart_Notes_Actve__c)
                    {
                        system.debug('********Entering the evalutation for Sub only SKU -- Success');
                        addOnlySubSKU = true;
                    }
                    else
                    {
                        system.debug('********Counting the account for Opp creation');
                        accountIdWithInactiveChartNotesList.add(accnt);
                        skipProcessing = true;
                    }
                }
            }   
            
            Date todayDate = system.today();
            Date restictG6ProductsFromdate = Date.valueOf('2099-01-01');
            //Handle logic for handling records with existing Open MFR record
            if(!addOnlySubSKU && accountIdToOpenMCRFollowup.containsKey(accnt.Id) && !skipProcessing)
            {
                system.debug('********Entering the process of handling existing MFR record');
                system.debug('Max_MFR_Followup_Date__c is ' + accnt.Max_MFR_Followup_Date__c);
                //Get the latest open MFR record tied to the customer
                MDCR_Followup__c latestOpenMFRRecord = accountIdToOpenMCRFollowup.get(accnt.Id);
                
                //Check if there are any open MFRs, which was created since the last Followup that had subscription SKU in it
                if(accnt.Num_Of_Open_MDCR_Followup_Records__c > 0 && (latestOpenMFRRecord.MDCR_Followup_Date__c >= accnt.Max_MFR_Followup_Date__c || accnt.Max_MFR_Followup_Date__c == null))
                {
                    system.debug('********Entering the process to add to existing MFR record ' + latestOpenMFRRecord.Name);
                    system.debug('Follow up date is ' + latestOpenMFRRecord.MDCR_Followup_Date__c);
                    system.debug('Max_MFR_Followup_Date__c is ' + accnt.Max_MFR_Followup_Date__c);
                    //Add Subscription SKU to the existing MFR record if there isn't o  ne already
                    if(latestOpenMFRRecord.Subscription_Product_Exist__c == 0)
                    {
                        system.debug('********Entering the process to add to existing record -- Success');
                        MDCR_Followup_Products__c subscription = ClsApexUtility.createProduct(activeProductMap.get(subSKUProduct),1, latestOpenMFRRecord, true);
                        subscription.MDCR_Followup__c = latestOpenMFRRecord.Id;
                        subscription.Unused_Qty__c = 0;
                        subscription.Order_Qty__c = 1;
                        mdcrFollowUpProductRecordsToAdd.add(subscription);
                        subscription = null;                        
                    }
                }
                else
                    canCreateMFR = true;
            }
            system.debug('Tracking the canCreateMFR Value ' + canCreateMFR);
            //accnt.Num_Of_Open_MDCR_Followup_Records__c == 0 is included in below if block is because we want to avoid the case where we have an open MFR with SUB sku in it 
            if((!skipProcessing && accnt.Num_Of_Open_MDCR_Followup_Records__c == 0 && !accountIdToOpenMCRFollowup.containsKey(accnt.Id)) || canCreateMFR == true)
            {
                system.debug('********Entering the process to handle new MFR creation');
                MDCR_Followup__c mdcrFollowUpRecord = new MDCR_Followup__c();
                String externalId = accnt.Id + '-' + accnt.Next_MDCR_Reorder_Follow_up_Date__c.format() + '-Admin';
                mdcrFollowUpRecord.Customer__c = accnt.Id;
                mdcrFollowUpRecord.MDCR_Followup_Status__c = addOnlySubSKU == TRUE ? 'Send Order to Oracle' : 'Open';
                mdcrFollowUpRecord.Subscription_Only_MFR__c = addOnlySubSKU == TRUE ? true : false;
                mdcrFollowUpRecord.External_Id__c = externalId;
                mdcrFollowUpRecord.Communication_Preference__c = accnt.MDCR_Communication_Preference__c;
                mdcrFollowUpRecord.MDCR_Followup_Date__c = accnt.Next_MDCR_Reorder_Follow_up_Date__c;
                mdcrFollowUpRecord.Is_BI_Active__c = accnt.Is_BI_Active__c;
                mdcrFollowUpRecord.Is_Chart_Notes_Actve__c = accnt.Is_Chart_Notes_Actve__c;
                mdcrFollowUpRecord.Is_CMN_Active__c = accnt.Is_CMN_Active__c;
                mdcrFollowUpRecord.MDCR_Price_Book__c = priceBookNameToIdMap.get(accnt.MDCR_Reorder_Pricebook__c);
                mdcrFollowUpRecord.Next_Possible_Date_For_Glucometer_Order__c = accnt.Next_Possible_Date_For_Glucometer_Order__c;
                mdcrFollowUpRecord.Next_Possible_Date_For_Receiver_Order__c = accnt.Next_Possible_Date_For_Receiver_Order__c;
                mdcrFollowUpRecord.Next_Possible_Date_For_Sensors_Order__c = accnt.Next_Possible_Date_For_Sensors_Order__c;
                mdcrFollowUpRecord.Next_Possible_Date_For_Solution_Order__c = accnt.Next_Possible_Date_For_Solution_Order__c;
                mdcrFollowUpRecord.Next_Possible_Date_For_Transmitter_Order__c = accnt.Next_Possible_Date_For_Transmitter_Order__c;
                mdcrFollowUpRecord.Num_Of_Days_Left_For_Glucometer_Order__c = accnt.Num_Of_Days_Left_For_Glucometer_Order__c;
                mdcrFollowUpRecord.Num_Of_Days_Left_For_Receiver_Order__c = accnt.Num_Of_Days_Left_For_Receiver_Order__c;
                mdcrFollowUpRecord.Num_Of_Days_Left_For_Sensors_Order__c = accnt.Num_Of_Days_Left_For_Sensors_Order__c;
                mdcrFollowUpRecord.Num_Of_Days_Left_For_Solution_Order__c = accnt.Num_Of_Days_Left_For_Solution_Order__c;
                mdcrFollowUpRecord.Num_Of_Days_Left_For_Transmitter_Order__c = accnt.Num_Of_Days_Left_For_Transmitter_Order__c;
                mdcrFollowUpRecord.Latest_Receiver_Generation_Shipped__c = accnt.Latest_Receiver_Generation_Shipped__c;
                mdcrFollowUpRecord.ShipTo_Address__c = accountToPrimaryShipToId.get(accnt.Id);
                mdcrFollowUpRecord.RecordTypeId = mdcrRecordTypeId;
                if(!addOnlySubSKU)
                    mdcrFollowUpRecordsToAdd.add(mdcrFollowUpRecord);
                else
                    mdcrSubscriptionOnlyFollowUpRecordsToAdd.add(mdcrFollowUpRecord);           
                MDCR_Followup__c parentReference = new MDCR_Followup__c(External_Id__c = externalId); 
                //If there is no follow up made since last time when we billed the Subscription then create a normal MFR
                if(!addOnlySubSKU && productGeneration == 'G5')
                {
                    system.debug('********Entering the process to add G5 Products');
                    //Add Sensor Product
                    system.debug('********Adding Sensor, parent reference is ' + parentReference);
                    MDCR_Followup_Products__c sensorProduct = ClsApexUtility.createProduct(activeProductMap.get('STS-MC-001'),accnt.MDCR_Sensors_Count__c, parentReference, false);
                    mdcrFollowUpProductRecordsToAdd.add(sensorProduct);
                    //Add Lancets Product
                    system.debug('********Adding Lancet');
                    MDCR_Followup_Products__c lancetsProduct = ClsApexUtility.createProduct(activeProductMap.get('301936586218'),accnt.MDCR_Lancets_Count__c, parentReference, false);
                    mdcrFollowUpProductRecordsToAdd.add(lancetsProduct);
                    //Add Test Strips Product
                    system.debug('********Adding TestStrips');
                    MDCR_Followup_Products__c testStripsProduct = ClsApexUtility.createProduct(activeProductMap.get('301937308505'),accnt.MDCR_Test_Strips_Count__c, parentReference, false);
                    mdcrFollowUpProductRecordsToAdd.add(testStripsProduct);
                    //Add Transmitter Order
                    
                    
                    if(accnt.Num_Of_Days_Left_For_Transmitter_Order__c == 0 &&  todayDate <= restictG6ProductsFromdate)
                    {
                        system.debug('********Adding Xmer');
                        MDCR_Followup_Products__c transmitter = ClsApexUtility.createProduct(activeProductMap.get('STT-MC-001'),1, parentReference, false);
                        transmitter.Unused_Qty__c = 0;
                        transmitter.Order_Qty__c = 1;
                        mdcrFollowUpProductRecordsToAdd.add(transmitter);
                    }
                    system.debug('********Adding Sub');
                    //Add subscription SKU
                    MDCR_Followup_Products__c subscription = ClsApexUtility.createProduct(activeProductMap.get('MT-MC-SUB'),1, parentReference, true);
                    subscription.Unused_Qty__c = 0;
                    subscription.Order_Qty__c = 1;
                    mdcrFollowUpProductRecordsToAdd.add(subscription);
                    
                    //Save the MFR G5 SKU
                    mfrExternalIdToSubSKUMap.put(externalId, 'G5');
                    //Add Opportunity record for followup for Phone followup type
                    /*  if(accnt.MDCR_Communication_Preference__c == 'Phone')
{
system.debug('********Adding Opp');
Opportunity newOppty = ClsApexUtility.createOpptyRecords(accnt.Id, accnt.Name, opptyRecordTypeId, parentReference);
opptyRecordsToBeAdded.add(newOppty);
}*/
                }
                //Add G6 related Products
                else if(!addOnlySubSKU && productGeneration.contains('G6'))
                {
                    //Add Sensor Product
                    system.debug('Product value STS-OM‌-003 is ' + activeProductMap.get('STS-OM-003'));
                    MDCR_Followup_Products__c sensorProduct = ClsApexUtility.createProduct(activeProductMap.get('STS-OM-003'),1, parentReference, true);
                    mdcrFollowUpProductRecordsToAdd.add(sensorProduct);
                    
                    //Add Transmitter SKU
                    if(accnt.Num_Of_Days_Left_For_Transmitter_Order__c <= 15 && todayDate <= restictG6ProductsFromdate )
                    {
                        MDCR_Followup_Products__c transmitter = ClsApexUtility.createProduct(activeProductMap.get('STT-OM-001'),1, parentReference, true);
                        transmitter.Unused_Qty__c = 0;
                        transmitter.Order_Qty__c = 1;
                        mdcrFollowUpProductRecordsToAdd.add(transmitter);
                    }
                    //Add Receiver SKU
                    if(productGeneration == 'G6 Transition from Brick')
                    {
                        MDCR_Followup_Products__c receiver = ClsApexUtility.createProduct(activeProductMap.get('STK-OM-001'),1, parentReference, true);
                        receiver.Unused_Qty__c = 0;
                        receiver.Order_Qty__c = 1;
                        mdcrFollowUpProductRecordsToAdd.add(receiver);
                    }
                    //Add subscription SKU
                    MDCR_Followup_Products__c subscription = ClsApexUtility.createProduct(activeProductMap.get('MT-OM-SUB'),1, parentReference, true);
                    subscription.Unused_Qty__c = 0;
                    subscription.Order_Qty__c = 1;
                    mdcrFollowUpProductRecordsToAdd.add(subscription);
                    
                    //Save the MFR G6 SKU
                    mfrExternalIdToSubSKUMap.put(externalId, 'G6');
                    
                    //Add Opportunity record for followup for Phone followup type
                    if(accnt.MDCR_Communication_Preference__c == 'Phone')
                    {
                        Opportunity newOppty = ClsApexUtility.createOpptyRecords(accnt.Id, accnt.Name, opptyRecordTypeId, parentReference, 'MDCR Followup');
                        opptyRecordsToBeAdded.add(newOppty);
                    }
                    
                }
                //If there is a follow up made since last time when we billed the Subscription then create a MFR with only Subscription SKU in it
                else if(addOnlySubSKU)
                {
                    //Add subscription SKU
                    MDCR_Followup_Products__c subscription = ClsApexUtility.createProduct(activeProductMap.get(subSKUProduct),1, parentReference, true);
                    subscription.Unused_Qty__c = 0;
                    subscription.Order_Qty__c = 1;
                    mdcrFollowUpProductRecordsToAdd.add(subscription);
                    if(subSKUProduct == 'MT-MC-SUB'){
                        //Save the MFR G5 SKU
                        mfrExternalIdToSubSKUMap.put(externalId, 'G5');
                    }else{
                        //Save the MFR G6 SKU
                        mfrExternalIdToSubSKUMap.put(externalId, 'G6');
                    }
                }       
            }
        }
        
        //Update Primary Benefit Records
        if(!primaryBenefitsToBeSentToChangeHealthCare.isEmpty())
        {
            try{
                List<Benefits__c> uniqueBenefitsToBeUpdated = new List<Benefits__c>();
                Map<Id, Benefits__c> benFitsToBeUpdatedMap = new Map<Id, Benefits__c>(primaryBenefitsToBeSentToChangeHealthCare);
                for(Benefits__c benefit: benFitsToBeUpdatedMap.values()){
                    uniqueBenefitsToBeUpdated.add(benFitsToBeUpdatedMap.get(benefit.Id));
                }
                update uniqueBenefitsToBeUpdated;
            }
            catch (DmlException de) {
                Integer numErrors = de.getNumDml();
                System.debug('getNumDml=' + numErrors);
                for(Integer i=0;i<numErrors;i++) {
                    System.debug('getDmlFieldNames=' + de.getDmlFieldNames(i));
                    System.debug('getDmlMessage=' + de.getDmlMessage(i));
                }
            }
        }
        
        //Insert Medicare Followup Records
        if(!mdcrFollowUpRecordsToAdd.isEmpty())
        {
            system.debug('********Entering process to create the MFR Header Records, count is ' + mdcrFollowUpRecordsToAdd.size());
            Database.SaveResult[] srList = Database.insert(mdcrFollowUpRecordsToAdd, false);
            // Iterate through each returned result
            for(integer i=0;i<srList.size();i++){
                system.debug('********mdcrFollowUpRecordsToAdd Status is sr[i].isSuccess()');
                if(!srList[i].isSuccess()){
                    String errorMsg = srList[i].getErrors().get(0).getFields() + ':' + srList[i].getErrors().get(0).getMessage();
                    system.debug('Error message is ' + errorMsg);
                    if(errorMsg.Length() >= 200)
                        errorMsg = errorMsg.substring(0,200);
                    //Write the error log
                    new ClsApexDebugLog().createLog(
                        new ClsApexDebugLog.Error(
                            'BclsCreateMedicareFollowUpRecords',
                            'mdcrFollowUpRecordsToAdd',
                            mdcrFollowUpRecordsToAdd[0].Customer__c,
                            errorMsg
                        ));
                }
            }
        }
        //Insert Medicare Followup Records, which has only Subscription
        if(!mdcrSubscriptionOnlyFollowUpRecordsToAdd.isEmpty())
        {
            system.debug('********Entering process to create the sub only');
            Database.SaveResult[] srList = Database.insert(mdcrSubscriptionOnlyFollowUpRecordsToAdd, false);
            // Iterate through each returned result
            for(integer i=0;i<srList.size();i++){
                system.debug('********Entering process to create the Sub only, count is ' + mdcrSubscriptionOnlyFollowUpRecordsToAdd.size());
                if(!srList[i].isSuccess()){
                    String errorMsg = srList[i].getErrors().get(0).getFields() + ':' + srList[i].getErrors().get(0).getMessage();
                    system.debug('Error message is ' + errorMsg);
                    if(errorMsg.Length() >= 200)
                        errorMsg = errorMsg.substring(0,200);
                    //Write the error log
                    new ClsApexDebugLog().createLog(
                        new ClsApexDebugLog.Error(
                            'mdcrSubscriptionOnlyFollowUpRecordsToAdd',
                            'mdcrFollowUpRecordsToAdd',
                            mdcrFollowUpRecordsToAdd[0].Customer__c,
                            errorMsg
                        ));
                }
            }
        }
        //Insert Medicare Followup Product Records
        if(!mdcrFollowUpProductRecordsToAdd.isEmpty())
        {
            system.debug('********Entering process to create the MFR Header Products Records');
            Database.SaveResult[] srList = Database.insert(mdcrFollowUpProductRecordsToAdd, false);
            // Iterate through each returned result
            for(integer i=0;i<srList.size();i++){
                system.debug('********Entering process to create the MFR Products, count is ' + mdcrFollowUpProductRecordsToAdd.size());
                if(!srList[i].isSuccess()){
                    String errorMsg = srList[i].getErrors().get(0).getFields() + ':' + srList[i].getErrors().get(0).getMessage();
                    system.debug('Error message is ' + errorMsg);
                    if(errorMsg.Length() >= 200)
                        errorMsg = errorMsg.substring(0,200);
                    //Write the error log
                    new ClsApexDebugLog().createLog(
                        new ClsApexDebugLog.Error(
                            'mdcrFollowUpProductRecordsToAdd',
                            'mdcrFollowUpRecordsToAdd',
                            mdcrFollowUpRecordsToAdd[0].Customer__c,
                            errorMsg
                        ));
                }
            }
        }
        
        //Insert Medicare Followup Opportunity Records
        if(!opptyRecordsToBeAdded.isEmpty())
        {
            system.debug('********Entering process to create Opp');
            try{
                ClsApexUtility.skipExecutionInOpptyTrigger = true;
                insert opptyRecordsToBeAdded;}
            catch (DmlException de) {
                Integer numErrors = de.getNumDml();
                System.debug('getNumDml=' + numErrors);
                for(Integer i=0;i<numErrors;i++) {
                    System.debug('getDmlFieldNames=' + de.getDmlFieldNames(i));
                    System.debug('getDmlMessage=' + de.getDmlMessage(i));
                }
            }
        }
        
        //Process Accounts with inactive Chart Notes
        if(!accountIdWithInactiveChartNotesList.isEmpty())
        {
            system.debug('********Entering process of Chart Notes expiration Opp creation');
            //Get the Open Opportunities tied to the accounts that are to be processed and prepare the map
            for(Opportunity openOpp : [SELECT Id, AccountId FROM Opportunity WHERE Stagename NOT IN ('61. Quote Approved', '10. Cancelled') AND AccountId IN : accountIdWithInactiveChartNotesList])
                openOpptyTiedToAccountsWithInactiveChartNotesMap.put(openOpp.AccountId, openOpp);
            system.debug('********Count of existing Opp is ' + openOpptyTiedToAccountsWithInactiveChartNotesMap.size());
            //Loop through the account Ids for which we need to create the follow up Opportunity
            for(Account inactiveChartNotesAccount : accountIdWithInactiveChartNotesList)
            {
                system.debug('**********Terr Id value is ' + inactiveChartNotesAccount.Territory_ID_Lookup__c);
                accountIdToTerrAlignIdMap.put(inactiveChartNotesAccount.Id, inactiveChartNotesAccount.Territory_ID_Lookup__c);
                //If there isn't any open Chart Notes then create one
                if((!openOpptyTiedToAccountsWithInactiveChartNotesMap.isEmpty() && !openOpptyTiedToAccountsWithInactiveChartNotesMap.containsKey(inactiveChartNotesAccount.Id)) || openOpptyTiedToAccountsWithInactiveChartNotesMap.isEmpty())
                {
                    Opportunity newOppty = ClsApexUtility.createOpptyRecords(inactiveChartNotesAccount.Id, inactiveChartNotesAccount.Name, opptyRecordTypeId, null, 'Doc Collection');
                    opptyRecordsToBeAddedForInactiveChartNotesList.add(newOppty);
                }
            }
            //Insert Opportunity records
            if(!opptyRecordsToBeAddedForInactiveChartNotesList.isEmpty())
            {
                system.debug('********Entering process to create Opp records');
                try{
                    ClsApexUtility.skipExecutionInOpptyTrigger = true;
                    insert opptyRecordsToBeAddedForInactiveChartNotesList;}
                catch (DmlException de) {
                    Integer numErrors = de.getNumDml();
                    System.debug('getNumDml=' + numErrors);
                    for(Integer i=0;i<numErrors;i++) {
                        System.debug('getDmlFieldNames=' + de.getDmlFieldNames(i));
                        System.debug('getDmlMessage=' + de.getDmlMessage(i));
                    }
                }
            }
            //Group the Opportunities for which the task record need to be created
            if(!openOpptyTiedToAccountsWithInactiveChartNotesMap.isEmpty())
                opptyRecordsToBeAddedForInactiveChartNotesList.addAll(openOpptyTiedToAccountsWithInactiveChartNotesMap.values());
            
            //Create task record to track the Chart Notes Inactive details
            if(!opptyRecordsToBeAddedForInactiveChartNotesList.isEmpty())
            {
                system.debug('********Entering process to create task records');
                Map<Id, String> terrIdToRSSMap = ClsApexUtility.getMedicareRSSForTerritory(accountIdToTerrAlignIdMap.values());
                List<Task> tasksToBeCreated = ClsApexUtility.createTaskRecords(opptyRecordsToBeAddedForInactiveChartNotesList, accountIdToTerrAlignIdMap, terrIdToRSSMap);
                try{insert tasksToBeCreated;}
                catch (DmlException de) {
                    Integer numErrors = de.getNumDml();
                    System.debug('getNumDml=' + numErrors);
                    for(Integer i=0;i<numErrors;i++) {
                        System.debug('getDmlFieldNames=' + de.getDmlFieldNames(i));
                        System.debug('getDmlMessage=' + de.getDmlMessage(i));
                    }
                }
            }
        }
        
        //Process Order Creation for Subscription only MFRs
        if(!mdcrSubscriptionOnlyFollowUpRecordsToAdd.isEmpty())
        {
            //MAP for MFR Id and MFR External Id
            Map<Id, String> mfrToExternalIdMap = new Map<Id, String>();
            
            for(MDCR_Followup__c mfr : mdcrSubscriptionOnlyFollowUpRecordsToAdd)
            {
                Order_Header__c newOrder = new Order_Header__c();
                newOrder.Account__c = mfr.Customer__c;
                newOrder.MDCR_Followup__c = mfr.Id;
                newOrder.Order_Date__c = system.Today();
                newOrder.Order_Type__c = 'MC Standard Sales Order';
                newOrder.Price_List__c = priceBookIdToOracleIdMap.get(mfr.MDCR_Price_Book__c);
                newOrder.Shipping_Method__c = '000001_FEDEX_A_3DS';
                newOrder.Status__c = 'Open';
                mfrToExternalIdMap.put(mfr.Id, mfr.External_Id__c);
                orderHeadersToBeAdded.add(newOrder);
            }
            //Insert Order Header Records and Process the order lines
            if(!orderHeadersToBeAdded.isEmpty())
            {
                try{
                    insert orderHeadersToBeAdded;
                }
                catch (DmlException de) {
                    Integer numErrors = de.getNumDml();
                    System.debug('getNumDml=' + numErrors);
                    for(Integer i=0;i<numErrors;i++) {
                        System.debug('getDmlFieldNames=' + de.getDmlFieldNames(i));
                        System.debug('getDmlMessage=' + de.getDmlMessage(i));
                    }
                }
                //Insert Subscription SKU Order lines
                for(Order_Header__c orderHeader:orderHeadersToBeAdded)
                {
                    String subSKUProduct = 'MT-MC-SUB';
                    if(!mfrExternalIdToSubSKUMap.isEmpty() && !mfrToExternalIdMap.isEmpty()&&                        
                       mfrToExternalIdMap.containsKey(orderHeader.MDCR_Followup__c) && 
                       mfrExternalIdToSubSKUMap.containsKey(mfrToExternalIdMap.get(orderHeader.MDCR_Followup__c)) &&
                       mfrExternalIdToSubSKUMap.get(mfrToExternalIdMap.get(orderHeader.MDCR_Followup__c)) == 'G6'){
                           subSKUProduct = 'MT-OM-SUB';
                       }   
                    Order_Item_Detail__c oli = new Order_Item_Detail__c();
                    oli.Order_Header__c = orderHeader.Id;
                    oli.Item_Name__c = subSKUProduct;
                    oli.Item_Number__c = subSKUProduct;
                    oli.Quantity__c = 1;
                    orderItemsToBeAdded.add(oli);
                }
                if(!orderItemsToBeAdded.isEmpty())
                {
                    try{
                        insert orderItemsToBeAdded;
                    }
                    catch (DmlException de) {
                        Integer numErrors = de.getNumDml();
                        System.debug('getNumDml=' + numErrors);
                        for(Integer i=0;i<numErrors;i++) {
                            System.debug('getDmlFieldNames=' + de.getDmlFieldNames(i));
                            System.debug('getDmlMessage=' + de.getDmlMessage(i));
                        }
                    }
                }
            }
        }
    }
    //Finish Method
    global void finish(Database.BatchableContext BC)
    {
        AsyncApexJob a = [Select Id, Status,ExtendedStatus,NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email    from AsyncApexJob where Id =:BC.getJobId()];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'itcrmsalesforce@dexcom.com'};
            mail.setToAddresses(toAddresses);
        mail.setSubject('MFR Records for  ' + system.today().format() + ' ' + a.Status);
        mail.setPlainTextBody('MFR Records were processed. Success :  ' + a.TotalJobItems +   ' batches. Failure : '+ a.NumberOfErrors + ' batches.');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });      
    }
}