global class clsLeadTriggerHelper {
/* Purpose: Checks if an accountassociated with a potential lead has any past orders or opportunities associated with it for last x years    
       Input: Lead that is being process and if there si an existing accoutn associated with it.
              numYrsOrders: # of years to check for orders associated with this account.
              numYrsOpp:  # of years to check for opportunities associated with this account.
       Output: map of lead id and a boolean flag if it is a high priority or not.
*/
public static map<string, boolean> AccHighPriorityCheck(List<Account> lstAcc, boolean ordersInPast2yrsCheck, boolean OppInPast2yrsCheck){
    map<string, boolean> mapReturn = new map<string, boolean>();
    if(lstAcc.IsEmpty()){
        return mapReturn;
    }
    // Check for orders
    Map<id, boolean> mapAccAccountChk= new map<Id, boolean>();
    Map<id, boolean> mapAccOrderChk= new map<Id, boolean>();
    Map<id, boolean> mapAccOppChk= new map<Id, boolean>();
    set<Id> setAccId= new set<Id>();
    for(Account a : lstAcc){
        mapAccAccountChk.put(a.id, true);
        mapAccOrderChk.put(a.Id, true); // Initialize to true;
        mapAccOppChk.put(a.Id, true); // Initialize to true;
        setAccId.Add(a.Id);
    }
    System.Debug('**** TPS:EH 1.1 lstAcc=' + lstAcc);
    System.Debug('**** TPS:EH 1.1 ordersInPast2yrsCheck=' + ordersInPast2yrsCheck);
    System.Debug('**** TPS:EH 1.1 OppInPast2yrsCheck=' + OppInPast2yrsCheck);
    
    Date dToday=Date.today();    
	Date dLast2Years=dToday.AddDays(-730);
    Date dLast2Months=dToday.AddDays(-61);
    
    /*Map<Id, Account> mapOldAcc= new Map<Id, Account>([Select Id from  Account Where CreatedDate > LAST_N_DAYS:60 AND Id IN :setAccId LIMIT 5000]);
    System.Debug('**** TPS:EH 2.1 mapOldAcc=' + mapOldAcc);
                 
    for(Account a:lstAcc){
        if(mapOldAcc.containsKey(a.Id)){
            mapAccAccountChk.put(a.Id, false);
        } 
    }*/
    for(Account accNew : [Select Id from  Account Where CreatedDate > :dLast2Months AND Id IN :setAccId LIMIT 5000]){
    	mapAccAccountChk.put(accNew.Id, false); 
    }
    System.Debug('**** TPS:EH 2.2 mapAccAccountChk=' + mapAccAccountChk);
                 
    /*for(Account acc : [Select Id from  Account Where CreatedDate > LAST_N_DAYS:60 AND Id IN :setAccId LIMIT 5000]){
        mapAccOrderChk.put(acc.Id, false);
    }*/
    // If an order is found in last 2 years, set it to false. 
    if(ordersInPast2yrsCheck){
        Map<Id, Order_Header__c> mapAccIdOH= new Map<Id, Order_Header__c>();
        
        for(Order_Header__c oh : [Select Id, Account__c from  Order_Header__c Where Account__c IN :setAccId AND Order_Date__c > :dLast2Years LIMIT 5000]){
            mapAccOrderChk.put(oh.Account__c, false);
            mapAccIdOH.put(oh.Account__c, oh);
        }
    }
    System.Debug('**** TPS:EH 2.3 mapAccOrderChk=' + mapAccOrderChk);
    
    if(OppInPast2yrsCheck){
        Map<Id, Opportunity> mapAccIdOrd= new Map<Id, Opportunity>();
        // Check for opportunities
        for(Opportunity o: [Select Id, AccountId from Opportunity Where AccountId IN :setAccId AND CreatedDate >LAST_N_YEARS:2 AND StageName !='10. Cancelled' LIMIT 5000]){
            mapAccOppChk.put(o.AccountId, false);
            mapAccIdOrd.put(o.AccountId, o);
        }    
    }
    System.Debug('**** TPS:EH 2.4 mapAccOppChk=' + mapAccOppChk);
    
    for(Account a : lstAcc){
        mapReturn.put(a.Id, mapAccAccountChk.get(a.Id) && mapAccOrderChk.get(a.Id) && mapAccOppChk.get(a.Id));        
    }    
    return mapReturn;
}
}