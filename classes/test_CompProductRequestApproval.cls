@isTest
Private class test_CompProductRequestApproval{
    
    /*static testMethod void validateCompProduct(){
        Test.startTest();
        system.debug('******');
        list<Id> groupId = new List<Id>();
        List<Id> ids = new List<Id> ();
        boolean renderTable;
        List<Id> compIds = new List<Id>();
        List<Id> userIdAndGroupId = new List<Id>();
        //Id userId = UserInfo.getUserId();
        
        
        User u = new user();
        u.LastName = 'SubimitterUser';
        u.Email = 'testNew' + Math.random() + '@test.com';
        u.Alias = 'UserApp';
        u.Username = 'SubmitterUser' + Math.random() + '@test.com';
        u.CommunityNickname = 'test123';
        u.LocaleSidKey = 'en_US';
        u.TimeZoneSidKey = 'GMT';
        u.ProfileID = '00e400000013uap';
        u.LanguageLocaleKey = 'en_US';
        u.EmailEncodingKey = 'UTF-8';
        insert u;
        
        User u1 = new user();
        u1.LastName = 'ApproverUser';
        u1.Email = 'test' + Math.random() + '@test.com';
        u1.Alias = 'App';
        u1.Username = 'ApproverUser' + Math.random() + '@test.com';
        u1.CommunityNickname = 'test12';
        u1.LocaleSidKey = 'en_US';
        u1.TimeZoneSidKey = 'GMT';
        u1.ProfileID = '00e400000013uap';
        u1.LanguageLocaleKey = 'en_US';
        u1.EmailEncodingKey = 'UTF-8';
        insert u1;
        Id userId = u1.Id;
        
        
        Group newGroup = new Group();
        newGroup.Name = 'groupName';
        newGroup.Type='Queue';
        insert newGroup;
        
        
        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
            GroupMember GM = new GroupMember();
            GM.GroupId = newGroup.id;
            GM.UserOrGroupId = thisUser.id;
            //Test.startTest();
            insert GM;
            // Test.stopTest();
        }
        
        
        GroupMember groupId22 =[SELECT Group.Id FROM GroupMember WHERE UserOrGroupId = :thisUser.Id AND Group.Type = 'Queue'];
        system.debug('groupId22...'+groupId22);
        system.debug('***thisUser***'+thisUser.id);
        system.debug('***uUser***'+u.id);
        system.debug('***u1User***'+u1.id);
        
        list<Account>ac = new list<Account>();
        List<Comp_Product_Request__c> compPr1 = new list<Comp_Product_Request__c>();
        List<Id> objId = new List<Id>();
        
        Account acc= new Account();
        acc.FirstName = 'FirstName';
        acc.LastName = 'LastName';
        acc.CMN_or_Rx_Expiration_Date__c = Date.newInstance(2099, 1, 1);
            
          
        acc.Party_Id__c = String.valueOf(Math.random());
        //Test.startTest();
        insert acc;
        ac.add(acc);
        //Test.stopTest();
        Address__c addrs = new Address__c();
        addrs.Account__c = acc.id;
        addrs.County__c = 'Test County';
        addrs.State__c = 'AZ';
        addrs.Zip_Postal_Code__c = '12345';
        addrs.Street_Address_1__c = 'Test Address';
        addrs.Primary_Flag__c = True;
        addrs.Address_Type__c ='SHIP_TO';
        insert addrs;
        
        Comp_Product_Request__c compPr = new Comp_Product_Request__c();
        compPr.Customer_Account_No__c = ac[0].id;
        compPr.Estimated_Amount__c = 12.00;
        compPr.Error_Source__c = 'Dexcom';
        compPr.Repmaking_Request__c = thisUser.Id;
        compPr.Shipping_Method__c = '000001_FEDEX_L_GND';
        compPr.Shipping_Address__c = addrs.id;
        insert compPr;
        objId.add(compPr.Id);
        system.debug('***compPr***'+compPr.id);
        //selectedCopm.add(compPr);
        // }
        
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(objId[0]);
         req1.setSubmitterId(thisUser.Id);
        req1.setNextApproverIds(new Id[] {thisUser.id});
       // req1.setProcessDefinitionNameOrId('Comp Product Requests Workflow');
        req1.setSkipEntryCriteria(true);
        Approval.ProcessResult result = Approval.process(req1);
      
        
        List<Id> newWorkItemIds = result.getNewWorkitemIds();
        system.debug('***getInstanceStatus***'+result.getInstanceStatus());
        list<ProcessInstanceWorkItem> item22 =[SELECT ProcessInstance.Status,ActorId,ProcessInstance.TargetObjectId FROM ProcessInstanceWorkItem ];
        
        system.debug('ProcessInstanceWorkItem..'+item22);
       
        //System.assert(result.isSuccess());
        //System.assertEquals('Pending', result.getInstanceStatus(),'Instance Status'+result.getInstanceStatus());
        for(GroupMember gropId : [SELECT Group.Id FROM GroupMember WHERE UserOrGroupId = :userId AND Group.Type = 'Queue']){
            userIdAndGroupId.add(gropId.Group.Id);
        }
        userIdAndGroupId.add(userId);
        userIdAndGroupId.add(userInfo.getUserId());
        system.debug('***userIdAndGroupId***'+userIdAndGroupId);
        //Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
        // for (ProcessInstanceWorkItem item : [SELECT ProcessInstance.TargetObjectId FROM ProcessInstanceWorkItem WHERE 
        // ProcessInstance.Status = 'Pending' AND ActorId IN:userIdAndGroupId])
        for (ProcessInstance item : [SELECT Id,TargetObjectId FROM ProcessInstance where Status = 'Pending' AND TargetObjectId = :compPr.id])
        {
            compIds.add(item.TargetObjectId);
            system.debug('***compIds***'+compIds);
        }
        
        List<Comp_Product_Request__c> compListToCheckVal = new List<Comp_Product_Request__c>();
        List<Comp_Product_Request__c> selectedCopms = new List<Comp_Product_Request__c>();
        
        //List<wrapperClas> compList = new List<wrapperClas>();
        compListToCheckVal = [SELECT Id, Name,Customer_Account_No__c,Error_Source__c,Estimated_Amount__c from Comp_Product_Request__c WHERE Id IN:compIds];
        system.debug('--compListToCheckVal--'+compListToCheckVal);
        //CompProductRequestApprovalController compProd= new CompProductRequestApprovalController ();
        List<CompProductRequestApprovalController.wrapperClas> listWrap = new List<CompProductRequestApprovalController.wrapperClas>();
        If(compListToCheckVal.Size()>0){                                             
            for(Comp_Product_Request__c comp : compListToCheckVal){
                renderTable = True;
                listWrap.add(new CompProductRequestApprovalController.wrapperClas(comp));
                //CompProductRequestApprovalController.wrapperClas wrpp = new CompProductRequestApprovalController.wrapperClas();
                //compIds.add(comp.id);
            }
        }
        
        Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
        //if(selectedCopms.size()>0){   //ActorId IN :userIdAndGroupId
        for(ProcessInstanceWorkitem workItem  : [SELECT id, ActorId FROM ProcessInstanceWorkItem WHERE ProcessInstance.TargetObjectId in:compListToCheckVal AND ProcessInstance.Status = 'Pending']){
            System.debug('workItem>>' + workItem);
            if(workItem != NULL){
                req2.setAction('Approve'); 
                req2.setWorkitemId(workItem.id); 
                req2.setComments('Approving request.'); 
                Approval.ProcessResult result2 =  Approval.process(req2);
                System.debug('result2>>' + result2);
            }
        }
        for(ProcessInstanceWorkitem workItem  : [SELECT id FROM ProcessInstanceWorkItem WHERE ProcessInstance.Status = 'Pending' AND ProcessInstance.TargetObjectId in:compListToCheckVal AND ActorId IN :userIdAndGroupId]){
            System.debug('workItem>>' + workItem);    
            if(workItem != NULL){
                req2.setAction('Rejected');
                req2.setWorkitemId(workItem.id); 
                req2.setComments('Rejecting'); 
                Approval.ProcessResult result2 =  Approval.process(req2);
                
            }
        }
        //    System.runAs(u1) {
        CompProductRequestApprovalController compProd= new CompProductRequestApprovalController ();
        compProd.processApproved();
        compProd.processRejected();*/
        //   }
        //}
        
        /*list<Id> userId = new List<Id>(); // Commenting here
//list<user> runUser = new List<user>();
user runUser = new User();
list<GroupMember> GroupMemberId = new List<GroupMember>();
GroupMemberId = [SELECT userOrGroupId FROM groupmember WHERE group.name='Comp Product Request Appovers'];
for(GroupMember gm:GroupMemberId){
userId.add(gm.userOrGroupId);
}

runUser=[select id,ProfileId from user where id in:userId limit 1];
System.runAs(runUser) {

CompProductRequestApprovalController compProd= new CompProductRequestApprovalController ();
compProd.renderTable = True;
CompProductRequestApprovalController.wrapperClas wrp = new CompProductRequestApprovalController.wrapperClas(compPr);
wrp.selected = True;
if(selected == True){

compProd.processApproved();
//wrp.selected = True;
}

if(selected == True){
compProd.processRejected();
} 
compProd.processRejected();
}*/
      /*  Test.stopTest();
    }*/
    
}