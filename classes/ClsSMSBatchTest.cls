/**
 * SMSBatchTest
 * Test Batch method to send SMS messages
 * @author Katie Wilson(Sundog)
 * @date 09/2818
 */
@IsTest
public class ClsSMSBatchTest {
    
    static testMethod void testSMSBatch() {
        
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperMock());
        List <Account> accountList= new List<Account>();
        
        Account testAccount = TestDataBuilder.testAccount();
        testAccount.PersonMobilePhone='1234567890';
        testAccount.SMS_Opt_In_List__c='True';
        accountList.add(testAccount);  
        
        Account testAccount2 = TestDataBuilder.testAccount();
        testAccount2.PersonMobilePhone='1234567891';
        testAccount2.SMS_Opt_In_List__c='True'; 
        accountList.add(testAccount2);
        
        insert accountList;
        Account deleteAccount=[SELECT SMS_Opt_In_Sent__c FROM Account].get(0);
        List <Marketing_Account__c> deleteMAccount= new list<Marketing_Account__c>();
        deleteMAccount=[SELECT id FROM Marketing_Account__c];
        delete deleteMAccount;
        delete deleteAccount;       
        
        Test.startTest(); 
        BclsSMSBatch smsBatch = new BclsSMSBatch();         
        Id batchInstanceId = Database.executeBatch(smsBatch, 1);
        Test.stopTest();
        
        Account resultAccount=[SELECT SMS_Opt_In_Sent__c FROM Account].get(0);
        System.assertNotEquals(null,resultAccount.SMS_Opt_In_Sent__c);
    }

    static testMethod void testSMSBatch2() {
        
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperMock());
        List <Account> accountList= new List<Account>();
        
        Account testAccount = TestDataBuilder.testAccount();
        testAccount.PersonMobilePhone='(123) 456-7893';
        testAccount.SMS_Opt_In_List__c='True';
        testAccount.SMS_Opt_In_Sent__c=datetime.now(); 
        testAccount.SMS_Send_Opt_In_2__c=true;
        accountList.add(testAccount);  
        
        Account testAccount2 = TestDataBuilder.testAccount();
        testAccount2.PersonMobilePhone='(923) 456-7894';
        testAccount2.SMS_Opt_In_List__c='True';        
        testAccount2.SMS_Opt_In_Sent__c=datetime.now(); 
        testAccount2.SMS_Send_Opt_In_2__c=true;
        accountList.add(testAccount2); 
        
        insert accountList;
        Account deleteAccount=[SELECT SMS_Opt_In_Sent__c FROM Account].get(0);
        List <Marketing_Account__c> deleteMAccount= new list<Marketing_Account__c>();
        deleteMAccount=[SELECT id FROM Marketing_Account__c];
        delete deleteMAccount;
        delete deleteAccount;       
        
        Test.startTest(); 
        BclsSMSBatch smsBatch = new BclsSMSBatch();   
        Id batchInstanceId = Database.executeBatch(smsBatch, 1);
        Test.stopTest();
        
        Account resultAccount=[SELECT SMS_Opt_In_2_Sent__c, SMS_Send_Opt_In_2__c FROM Account].get(0);
        System.assertNotEquals(null,resultAccount.SMS_Opt_In_2_Sent__c);
		System.assertEquals(false,resultAccount.SMS_Send_Opt_In_2__c); 
    }
    
    static testMethod void testNoSMSBatch() {
        
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperMock());
        List <Account> accountList= new List<Account>();
        
        Account testAccount = TestDataBuilder.testAccount();
        testAccount.Phone='1234567890';
        testAccount.RecordTypeId='01233000000MIqL';
        testAccount.SMS_Opt_In_List__c='True';
        accountList.add(testAccount);  
        
        Account testAccount2 = TestDataBuilder.testAccount();
        testAccount2.Phone='1234567891';
        testAccount2.SMS_Opt_In_List__c='True'; 
        testAccount2.RecordTypeId='01233000000MIqL';
        accountList.add(testAccount2); 
        
        insert accountList;
        Account deleteAccount=[SELECT SMS_Opt_In_Sent__c FROM Account].get(0);
        List <Marketing_Account__c> deleteMAccount= new list<Marketing_Account__c>();
        deleteMAccount=[SELECT id FROM Marketing_Account__c];
        delete deleteMAccount;
        delete deleteAccount;       
        
        Test.startTest(); 
        BclsSMSBatch smsBatch = new BclsSMSBatch();   
        Id batchInstanceId = Database.executeBatch(smsBatch, 1);
        Test.stopTest();
        
        Account resultAccount=[SELECT SMS_Opt_In_Sent__c FROM Account].get(0);
        System.assertEquals(null,resultAccount.SMS_Opt_In_Sent__c);
    }
    
    static testMethod void testSMSBatch2NotPending() {
        
        Test.setMock(HttpCalloutMock.class, new ClsMarketingCloudHelperMock());
        List <Account> accountList= new List<Account>();
        
        Account testAccount = TestDataBuilder.testAccount();
        testAccount.PersonMobilePhone='(123) 456-7893';
        testAccount.SMS_Opt_In_List__c='True';
        testAccount.SMS_Opt_In_Sent__c=datetime.now(); 
        testAccount.SMS_Send_Opt_In_2__c=true;
        accountList.add(testAccount);  
        
        Account testAccount2 = TestDataBuilder.testAccount();
        testAccount2.PersonMobilePhone='(123) 456-7888';
        testAccount2.SMS_Opt_In_List__c='True';        
        testAccount2.SMS_Opt_In_Sent__c=datetime.now(); 
        testAccount2.SMS_Send_Opt_In_2__c=true;
        accountList.add(testAccount2); 
        
        insert accountList;
        Account deleteAccount=[SELECT SMS_Opt_In_Sent__c FROM Account].get(0);
        List <Marketing_Account__c> deleteMAccount= new list<Marketing_Account__c>();
        deleteMAccount=[SELECT id FROM Marketing_Account__c];
        delete deleteMAccount;
        delete deleteAccount;       
        
        Test.startTest(); 
        BclsSMSBatch smsBatch = new BclsSMSBatch();   
        Id batchInstanceId = Database.executeBatch(smsBatch, 1);
        Test.stopTest();
        
        Account resultAccount=[SELECT SMS_Opt_In_2_Sent__c, SMS_Send_Opt_In_2__c FROM Account].get(0);
        System.assertEquals(null,resultAccount.SMS_Opt_In_2_Sent__c);
    }
}