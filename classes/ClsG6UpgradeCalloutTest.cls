@isTest
public class ClsG6UpgradeCalloutTest {
  	
    @isTest static void testCallout() {
        // Set mock callout class 
		List<ID> accList = new List<ID>();
        Account a = new Account(FirstName ='fname',LastName ='lname' );
        accList.add(a.Id);
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorTest());
        
            
        Test.startTest();
  		 		 

		ClsG6UpgradeCallout.sendNotification(accList); 
        Test.stopTest();

}    
}