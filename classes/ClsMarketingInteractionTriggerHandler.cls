/***************************************************
*@Author        : Priyanka Kajawe(LTI)
*@Date Created  : 03-09-2020
*@Description   : Apex class to handle marketing interaction Trigger
*****************************************************/
Public class ClsMarketingInteractionTriggerHandler{

/* Method to update Marketing Account on Marketing Interactions */
    public static void populateMarketingAccount(List<Marketing_Interaction__c> newTrigger){
        Set<Id> accountIdSet = new Set<Id>(); //Set of Account Ids
        Map<Id, Id> acntToMAmap = new Map<Id, Id>(); // Account to Marketing Account Map
        List<Marketing_Interaction__c> populateMAList = new List<Marketing_Interaction__c>(); // Marketing Interaction List
        
        for(Marketing_Interaction__c thisMI : newTrigger){
            if(String.isBlank(thisMI.Marketing_Account__c)){
                populateMAList.add(thisMI);
            }
        }
        if(!populateMAList.isEmpty()){
            //fetch Marketing Account 
            for(Marketing_Interaction__c thisInteraction : populateMAList){
                accountIdSet.add(thisInteraction.Account__c);
            }   
            
            for(Marketing_Account__c mAccount : [SELECT id, Account__c from Marketing_Account__c where Account__c IN : accountIdSet]){
                acntToMAmap.put(mAccount.Account__c , mAccount.Id);
            } 
             
            //populate Marketing Account on Interaction
            if(!acntToMAmap.isEmpty()){
                for(Marketing_Interaction__c thisMInteraction : populateMAList){
                    if(acntToMAmap.containsKey(thisMInteraction.Account__c) && acntToMAmap.get(thisMInteraction.Account__c) != null)
                        thisMInteraction.Marketing_Account__c  = acntToMAmap.get(thisMInteraction.Account__c);
                }
            } 
        }
    }
}