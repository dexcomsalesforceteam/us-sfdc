/***
*@Author        : Kristen (Sundog)
*@Date Created  : 05-06-2019
*@Description   : Schedule class for BclsAccountExpiry
***/
global class BclsAccountExpiryUpdateSched implements Schedulable{
    global static String CRON_EXP = '0 0 6 1/1 * ? *'; // every day at 6 in the morning
    
    global void execute(SchedulableContext sc) {
        BclsAccountExpiryUpdate schBatch = new BclsAccountExpiryUpdate();   
       
        Id batchInstanceId = Database.executeBatch(schBatch);
    }
    
    /*
     * To begin running this batch on a regular schedule, paste and execute the following line of Apex in an anonymous window:
     * System.schedule('Expiration Updates Batch', BclsAccountExpiryUpdatedSched.CRON_EXP, new BclsAccountExpiryUpdatedSched());
     */ 

}