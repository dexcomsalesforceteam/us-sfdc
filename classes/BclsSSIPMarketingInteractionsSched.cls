/***
*@Author        : Priyanka Kajawe
*@Date Created  : 04-05-2019
*@Description   : Schedule class to Push Marketing Interactions for SSIP
***/
public class BclsSSIPMarketingInteractionsSched implements Schedulable {
    public void execute(SchedulableContext sc) {
        BclsSSIPMarketingInteractions schBatch = new BclsSSIPMarketingInteractions();   
       
        Id batchInstanceId = Database.executeBatch(schBatch, 150);
    }
}