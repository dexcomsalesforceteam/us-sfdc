<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>true</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Account__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <externalId>false</externalId>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Marketing Accounts</relationshipLabel>
        <relationshipName>Marketing_Accounts</relationshipName>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Age_Formula__c</fullName>
        <externalId>false</externalId>
        <formula>Account__r.Age__c</formula>
        <label>Age (Formula)</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Age__c</fullName>
        <externalId>false</externalId>
        <label>Age</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Current_Generation__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Current Generation</label>
        <length>8</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>First_Order_Ship_Date__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>First Order Ship Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Has_Not_Made_Recent_Order_Formula__c</fullName>
        <externalId>false</externalId>
        <formula>Account__r.Has_Not_Made_Recent_Order__c</formula>
        <label>Has Not Made Recent Order (Formula)</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Has_Not_Made_Recent_Order__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Has Not Made Recent Order</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Medical_Facility_Name_Rollup__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Medical Facility Name Rollup</label>
        <length>100</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Medical_Facility__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Medical Facility</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Non_Eligibility_Fields_Mismatch__c</fullName>
        <externalId>false</externalId>
        <formula>IF  ( (Age__c &lt;&gt; Age_Formula__c) || (Has_Not_Made_Recent_Order__c  &lt;&gt;  Has_Not_Made_Recent_Order_Formula__c) , true, false)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Non Eligibility Fields Mismatch?</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Payor_Name_Rollup__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Payor Name Rollup</label>
        <length>100</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Payor__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Payor</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Receiver_Eligible_Formula__c</fullName>
        <externalId>false</externalId>
        <formula>Account__r.Num_Of_Days_Left_For_Receiver_Order__c &lt;1</formula>
        <label>Receiver Eligible (Formula)</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Receiver_Eligible_Mismatch__c</fullName>
        <externalId>false</externalId>
        <formula>Receiver_Eligible__c &lt;&gt; Receiver_Eligible_Formula__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Receiver Eligible Mismatch?</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Receiver_Eligible__c</fullName>
        <defaultValue>true</defaultValue>
        <externalId>false</externalId>
        <label>Receiver Eligible</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Sales_Channel__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Sales Channel</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Sensor_Eligible_Formula__c</fullName>
        <externalId>false</externalId>
        <formula>Account__r.Num_Of_Days_Left_For_Sensors_Order__c &lt;1</formula>
        <label>Sensor Eligible (Formula)</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Sensor_Eligible_Mismatch__c</fullName>
        <externalId>false</externalId>
        <formula>Sensor_Eligible__c &lt;&gt; Sensor_Eligible_Formula__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Sensor Eligible Mismatch?</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Sensor_Eligible__c</fullName>
        <defaultValue>true</defaultValue>
        <externalId>false</externalId>
        <label>Sensor Eligible</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Total_Orders__c</fullName>
        <externalId>false</externalId>
        <label>Total Orders</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Trans_Eligible_Mismatch__c</fullName>
        <externalId>false</externalId>
        <formula>Transmitter_Eligible__c &lt;&gt; Transmitter_Eligible_Formula__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Trans Eligible Mismatch?</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Transmitter_Eligible_Formula__c</fullName>
        <externalId>false</externalId>
        <formula>Account__r.Num_Of_Days_Left_For_Transmitter_Order__c &lt;1</formula>
        <label>Transmitter Eligible (Formula)</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Transmitter_Eligible__c</fullName>
        <defaultValue>true</defaultValue>
        <externalId>false</externalId>
        <label>Transmitter Eligible</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>Marketing Account</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>MA-{0000000}</displayFormat>
        <label>Marketing Account Number</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Marketing Accounts</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
