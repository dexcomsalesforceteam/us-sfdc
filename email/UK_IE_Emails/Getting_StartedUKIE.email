<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html style="width:100%;margin:0px;font-size:12px;line-height:1.2em;font-weight:400;font-style:normal;font-family:'Open Sans',Arial,'Helvetica Neue',Helvetica,sans-serif;color:#353535;">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;">
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />

    <style type="text/css">
      @import url('https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i');
      /* Some resets and issue fixes */
      #outlook a { padding:0; }
      body{ width:100% !important; -webkit-text; size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0; }
      .ReadMsgBody { width: 100%; }
      .ExternalClass {width:100%;}
      .backgroundTable {margin:0 auto; padding:0; width:100% !important;}
      table td {border-collapse: collapse;}
      .ExternalClass * {line-height: 115%;}
      /* End reset */
      @media screen and (max-width: 630px) {
        html {
          box-sizing: border-box;
        }
        *, *:before, *:after {
          box-sizing: inherit;
        }
        sub, sup {
          /* Specified in % so that the sup/sup is the
             right size relative to the surrounding text */
          font-size: 75%;

          /* Zero out the line-height so that it doesn't
             interfere with the positioning that follows */
          line-height: 0;

          /* Where the magic happens: makes all browsers position
             the sup/sup properly, relative to the surrounding text */
          position: relative;

          /* Note that if you're using Eric Meyer's reset.css, this
             is already set and you can remove this rule */
          vertical-align: baseline;
        }

        sup {
          /* Move the superscripted text up */
          top: -0.5em;
        }

        sub {
          /* Move the subscripted text down, but only
             half as far down as the superscript moved up */
          bottom: -0.25em;
        }
        /* Hide stuff */
        *[class="hide"] {display:none !important;}
        /* responsive images */
        *[class="rsp-img"] {width:100% !important; height:auto !important;}
        /* This sets elements to 100% width and fixes the height issues too */
        *[class="100p"] {display:block !important;width:100% !important; height:auto !important;border:0 !important;}
        *[class="90p"] {display:block !important;width:100% !important; height:auto !important;border:0 !important;}
        *[class="90plrb"] {display:block !important;width:100% !important; height:auto !important;border:0 !important;padding:0 20px 25px 20px;}
        *[class="90ptlrb"] {display:block !important;width:100% !important; height:auto !important;border:0 !important;padding:25px 20px 25px 20px;}
        *[class="90plrbl"] {display:block !important;width:100% !important; height:auto !important;border:0 !important;padding:0 20px 55px 20px;}
        *[class="100pb"] {display:block !important;width:100% !important; height:auto !important;border:0 !important;padding:0 0 25px 0;}
        *[class="np"] {padding:0 !important;}
      }
    </style>
  </head>
  <body style="padding:0; margin:0">
    <table border="0" cellpadding="0" cellspacing="0" style="margin: 0; padding: 0" width="100%">
      <tr>
        <td align="center" valign="top">
          <table width="600" cellspacing="0" cellpadding="0" class="100p"><!-- start content table -->
            <tr>
              <td>
                <img src="https://s3-us-west-2.amazonaws.com/dexcompdf/OUS+Specific+PDFs/img_UK_IST/img_PS_V2/PS_header.jpg" atl="" width="600" height="128" class="rsp-img" style="display:block;" />
              </td>
            </tr>
            <tr>
              <td>
                <table width="600" cellspacing="0" cellpadding="0" class="100p">
                  <td class="100p" width="35">&nbsp;</td>
                  <td class="90plrb" width="330">
                    <p style="margin-top:0px;margin-bottom:10px; font-size: 13px; line-height:1.4;color: #878787">We are delighted you have chosen the Dexcom G6<sup>&reg;</sup> and
wish you all the best getting started. We have summarised below some information that could be useful to you during your trial period:</p>
                    <p style="margin:0;">&nbsp;</p>
                    <h2 style="font-family: 'Arial Narrow', Arial, sans-serif;margin-top:0px;margin-bottom:5px; font-size: 15px;font-weight:bold;line-height:1.2;color: #FF9417">DELIVERY OF YOUR PARCEL</h2>
                    <p style="margin-top:0px;margin-bottom:0; font-size: 13px; line-height:1.2;color: #878787">You will receive an email with your parcel tracking information 24 hours
before your scheduled delivery.</p>
                  </td>
                  <td class="90plrb" width="235"><img src="https://s3-us-west-2.amazonaws.com/dexcompdf/OUS+Specific+PDFs/img_UK_IST/img_PS_V2/PS_half-lady-1.jpg?2" width="235" height="209" class="rsp-img" style="display:block;" /></td>
                </table>
              </td>
            </tr>
            <tr>
              <td>
                <table width="600" cellspacing="0" cellpadding="0" class="100p">
                  <td class="90plrb" width="300" bgcolor="#f3eee8"><img src="https://s3-us-west-2.amazonaws.com/dexcompdf/OUS+Specific+PDFs/img_UK_IST/img_PS_V2/PS_half-lady-2.jpg?2" width="300" height="277" class="rsp-img" style="display:block;" /></td>
                  <td class="90plrb" bgcolor="#f3eee8" width="290">
                    <h2 style="font-family: 'Arial Narrow', Arial, sans-serif;margin-top:0px;margin-bottom:5px; font-size: 15px;font-weight:bold;line-height:1.2;color: #FF9417">DEXCOM ON-BOARDING RESOURCES</h2>
                    <p style="margin-top:0px;margin-bottom:10px; font-size: 13px; line-height:1.2;color: #878787">Along with your order, you should receive an information bundle that contains some excellent training resources.
In the meantime, here’s some other useful information:</p>
                    <ul style="margin-left:15px; padding-left: 0;">
                      <li style="margin-top:0px;margin-bottom:5px; font-size: 13px; line-height:1.2;color: #878787">Attached to this email, you will find two documents that give you useful tips and guidance on setting up and using your device.</li>
                      <li style="margin-top:0px;margin-bottom:5px; font-size: 13px; line-height:1.2;color: #878787">We run <a href="https://rebrand.ly/G6WebinarPSemail" target="_blank" style="">regular live webinars</a> in which you can have all your questions answered live.</li>
                      <li style="margin-top:0px;margin-bottom:0; font-size: 13px; line-height:1.2;color: #878787">If you can’t make the webinar, don’t worry – we have a range of <a href="https://rebrand.ly/G6OnDVideosPSemail" target="_blank" style="">on-demand training videos.</a></li>
                    </ul>
                    </td>
                  <td class="hide" width="10" bgcolor="#f3eee8">&nbsp;</td>
                </table>
              </td>
            </tr>
            <tr>
              <td>
                <table width="600" cellspacing="0" cellpadding="0" class="100p">
                  <td class="hide" width="35">&nbsp;</td>
                  <td class="90ptlrb" width="350">
                    <h2 style="font-family: 'Arial Narrow', Arial, sans-serif;margin-top:0px;margin-bottom:5px; font-size: 15px;font-weight:bold;line-height:1.2;color: #FF9417">AFTER YOUR TRIAL PERIOD</h2>
                    <p style="margin-top:0px;margin-bottom:10px; font-size: 13px; line-height:1.2;color: #878787">You will soon receive a form via email giving you the option to enroll in our Subscribe and Save plan for your Dexcom G6 supplies. Alternatively, you can order via phone at our number below, or go through our website (using the same login details as you do for the Dexcom app).</p>
                    <p style="margin:0;">&nbsp;</p>
                    <h2 style="font-family: 'Arial Narrow', Arial, sans-serif;margin-top:0px;margin-bottom:5px; font-size: 15px;font-weight:bold;line-height:1.2;color: #FF9417">TECHNICAL SUPPORT</h2>
                    <p style="margin-top:0px;margin-bottom:0px; font-size: 13px; line-height:1.2;color: #878787">If you have any questions or issues with your product, please contact our Technical Support Team directly – their details can be found below.</p>
                  </td>
                  <td class="90p" width="215" align="center"><img src="https://s3-us-west-2.amazonaws.com/dexcompdf/OUS+Specific+PDFs/img_UK_IST/img_PS_V2/PS_phones.png?2" atl="" style="display:block;margin:10px 0 10px 0;" class="rsp-img" width="215" height="217" /></td>
                </table>
              </td>
            </tr>
            <tr>
              <td>
                <table width="600" cellspacing="0" cellpadding="0" class="100p">
                  <tr>
                    <td style="padding:20px 35px 20px 35px;" bgcolor="#75AF27">
                     <p style="margin-top:0px;margin-bottom:10px;color:#ffffff;">Good luck! We hope you enjoy life with ZERO* fingersitcks!
                      <br/>
                      <br/>
                      Kind regards,</p>
                    <div>
                      <img src="https://s3-us-west-2.amazonaws.com/dexcompdf/OUS+Specific+PDFs/img_UK_IST/img_PS_V2/PS_dexcom_small.jpg?2" atl="" style="display:block;max-width:100%;height:auto;border:0;">
                    </div>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td height="40">&nbsp;</td>
            </tr>
            <tr>
              <td align="center">
                <table width="600" cellspacing="0" cellpadding="0" class="90ptlrb">
                  <tr>
                    <td width="240" valign="center" align="center" style="border-right: solid 1px #878787;">
                    </td>
                    <td width="180" valign="center" align="center" style="border-right: solid 1px #878787;">
                      <h2 style="font-family: 'Arial Narrow', Arial, sans-serif;margin-top:5px;margin-bottom:5px; font-size: 15px;font-weight:bold;line-height:1.2;color: #FF9417">UK</h2>
                    </td>
                    <td width="180" valign="center" align="center">
                      <h2 style="font-family: 'Arial Narrow', Arial, sans-serif;margin-top:5px;margin-bottom:5px; font-size: 15px;font-weight:bold;line-height:1.2;color: #FF9417">ROI</h2>
                    </td>
                  </tr>
                  <tr>
                    <td width="240" valign="center" align="center" style="border-right: solid 1px #878787;">
                       <p style="margin-top:5px;margin-bottom:5px; font-size: 10px; line-height:1.2;color: #878787"><strong>Sales &amp; ordering</strong><br/>(Monday to Thursday 09:00 – 17:30<br/>and Friday 09:00 – 16:00)</p>
                    </td>
                    <td width="180" valign="center" align="center" style="border-right: solid 1px #878787;">
                      <p style="margin-top:5px;margin-bottom:5px; font-size: 10px; line-height:1.2;color: #878787"><a href="tel:08000315761" style="color:#878787;text-decoration: none;">0800 031 5761</a></p>
                    </td>
                    <td width="180" valign="center" align="center">
                      <p style="margin-top:5px;margin-bottom:5px; font-size: 10px; line-height:1.2;color: #878787"><a href="tel:1800827602" style="color:#878787;text-decoration: none;">1800 827 602</a></p>
                    </td>
                  </tr>
                  <tr>
                    <td width="240" valign="center" align="center" style="border-right: solid 1px #878787;">
                       <p style="margin-top:5px;margin-bottom:5px; font-size: 10px; line-height:1.2;color: #878787"><strong>Email (for orders)</strong></p>
                    </td>
                    <td width="180" valign="center" align="center" style="border-right: solid 1px #878787;">
                      <p style="margin-top:5px;margin-bottom:5px; font-size: 10px; line-height:1.2;color: #878787"><a href="maito:gb.sales@dexcom.com" style="color:#878787;text-decoration: none;">gb.sales@dexcom.com</a></p>
                    </td>
                    <td width="180" valign="center" align="center">
                      <p style="margin-top:5px;margin-bottom:5px; font-size: 10px; line-height:1.2;color: #878787"><a href="maito:ie.sales@dexcom.com" style="color:#878787;text-decoration: none;">ie.sales@dexcom.com</a></p>
                    </td>
                  </tr>
                  <tr>
                    <td width="240" valign="center" align="center" style="border-right: solid 1px #878787;">
                       <p style="margin-top:5px;margin-bottom:5px; font-size: 10px; line-height:1.2;color: #878787"><strong>Technical support</strong><br/>(Monday to Friday 07:30 – 17:30)</p>
                    </td>
                    <td width="180" valign="center" align="center" style="border-right: solid 1px #878787;">
                      <p style="margin-top:5px;margin-bottom:5px; font-size: 10px; line-height:1.2;color: #878787"><a href="tel:08000315763" style="color:#878787;text-decoration: none;">0800 031 5763</a></p>
                    </td>
                    <td width="180" valign="center" align="center">
                      <p style="margin-top:5px;margin-bottom:5px; font-size: 10px; line-height:1.2;color: #878787"><a href="tel:1800827603" style="color:#878787;text-decoration: none;">1800 827 603</a></p>
                    </td>
                  </tr>
                  <tr align="center">
                    <td colspan="3" style="padding-top:20px;padding-bottom:20px;line-height: 1.5em;font-size: 10px;color: #878787" class="np">Find us on:
                      <br>
                      <a href="https://www.facebook.com/DexcomUK/" target="_blank" rel="nofollow" style="display:inline-block;text-decoration:none;color:#000;margin-bottom: 5px;max-width: 60px; margin: 0 auto;">
                        <img src="https://s3-us-west-2.amazonaws.com/dexcompdf/OUS+Specific+PDFs/img_UK_IST/img_PS_V2/PS_fb-icon.png?2" width="22" height="22" atl="facebook link" style="display:block;max-width:100%;height:auto;border:0;width: 22px; height: 22px;"><p style="margin: 0;color: #878787">@DexcomUK

                        </p></a>
                        <br />
                      <a href="http://www.dexcom.com/en-GB" target="_blank" style="display:inline-block;text-decoration:none;color: #878787;font-size: 10px; line-height: 1.2em;">www.dexcom.com/en-GB</a>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td align="center" colspan="2" bgcolor="#e1e1e1" style="width:50%;padding-top:20px;padding-bottom:20px;padding-left:35px;padding-right:35px;width: 100%;">
                <table style="font-size:6px;line-height:1.2em;font-weight:400;font-style:normal;border-collapse:collapse;font-family:'Open Sans',Arial,&quot;Helvetica Neue&quot;,Helvetica,sans-serif;color:#353535;font-size: 9px; line-height: 1.3em;">
                  <tr align="center">
                    <td style="width:50%;padding-top:20px;padding-bottom:20px;padding-left:35px;padding-right:35px;padding-top: 0px; padding-bottom: 0px; padding-left: 0px; padding-right: 0px;">
                      <h3 style="font-weight:bold;font-size:12px;line-height:1.4em;margin-top:0px;margin-bottom:10px;font-size: 9px; line-height: 1.2; margin-bottom: 3px;"> <strong style="font-weight:700;">&copy;2018 Dexcom (UK) Distribution Limited</strong>
                      </h3>
                      <p style="margin-top:0px;margin-bottom:10px;max-width: 500px; margin-bottom: 10px; font-size: 10px; line-height: 1.2;">This email is for your eyes only, so please don’t pass this on to anyone else. If you’ve received this email by accident, let us know and feel free to delete this message.<br/>LBL016791 Rev001</p>
                    </td>
                  </tr>
                  <tr align="center">
                    <td style="width:50%;padding-top:20px;padding-bottom:20px;padding-left:35px;padding-right:35px;padding-top: 0px; padding-bottom: 0px; padding-left: 0px; padding-right: 0px;">
                      <p style="margin-top:0px;margin-bottom:5px; font-size: 10px; line-height: 1.2;">
                        All rights reserved. Watchmoor Park, Camberley, GU153YL (10040080)
                      </p>
                      <p style="margin-top:0px;margin-bottom:10px;margin-bottom: 0px; font-size: 10px; line-height: 1.2;">*If your glucose alerts and readings from the G6 do not match symptoms or expectations, use a blood glucose meter to make diabetes treatment decisions.</p>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table><!-- end content table -->
        </td>
      </tr>
    </table>
  </body>
</html>